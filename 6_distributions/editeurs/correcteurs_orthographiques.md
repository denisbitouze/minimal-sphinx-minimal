# Quels sont les correcteurs orthographiques utilisables pour TeX/LaTeX ?

```{eval-rst}
.. todo:: *Page sans doute à reprendre posément car les différentes solutions (programmes autonomes, correcteurs intégrables à des éditeurs...) s'y mélangent.*
```

Nous pouvons distinguer deux types de correcteurs orthographiques dans ce contexte :

- les correcteurs orthographiques généraux, qui permettent de vérifier des textes non LaTeX ;
- les correcteurs orthographiques qui permettent de vérifier l'orthographe dans un source LaTeX.

Il est toujours possible de saisir le texte avec un éditeur quelconque, comme `Open-Office`, `Word`, `WordPerfect` ou autre sans introduire de balises (mais ce n'est possible que si l'on n'a pas de formules mathématiques) et de faire la vérification orthographique avec le correcteur fourni avec le logiciel. Ensuite, il faut sauvegarder en mode texte, et saisir les balises TeX ou LaTeX. L'intérêt de cette méthode dépend donc du type de document à réaliser.

Une approche alternative à cette première méthode prend la sortie TeX ou LaTeX et la vérifie. Cela consiste par exemple à produire une sortie PDF et à la traiter avec `pdftotext`, en utilisant n'importe quel vérificateur de texte brut sur le résultat. Pour que cela fonctionne bien, l'utilisateur devrait désactiver la césure avant de créer la sortie PDF.

Les correcteurs orthographiques qui permettent de contrôler directement le source LaTeX sont soit des programmes indépendants, soit intégrés dans les environnements de travail. Voyez la partie dévolue aux environnements intégrés pour les détails.

## Sous Unix ou Linux

### Abiword

[Abiword](http://www.abisource.com/) permet de sauvegarder les textes en format LaTeX, et comme tout traitement de texte, permet la correction orthographique, et ceci dans de nombreuses langues. En tapant son texte d'une façon {doc}`wysiwig </1_generalites/bases/wysiwyg>` et en sauvegardant son texte en format LaTeX, la correction orthographique peut être faite au moment de l'édition.

:::{important}
Résultat non testé.
:::

### Hunspell

L'offre récente [Hunspell](http://hunspell.sourceforge.net/) est largement utilisée dans d'autres projets de logiciels open source. Elle est également disponible pour d'autres architectures.

### ispell et aspell

> {ctanpkg}`ispell` ([également disponible ici](ftp://ftp.lip6.fr/pub/gnu/)) fut pendant longtemps l'outil principal sur ce sujet. Vous pouvez lui associer le [dictionnaire Français-GUTenberg](http://www.unil.ch/ling/cp/frgut.html). Il peut être associé à `Emacs`, `XEmacs` ou, `NeXT`. Ainsi, il existe le mode `ispell-minor-mode` de `Emacs`, qui est capable de vérifier l'orthographe en ligne sans prendre en compte les commandes LaTeX.

De son côté, [aspell](http://aspell.sourceforge.net/) est plus ou moins le [remplaçant](https://fr.wikipedia.org/wiki/GNU_Aspell) de `ispell`. Il est utilisable directement ou comme librairie et est utilisé par de nombreux programmes (éditeurs, logiciel de courrier...). Il fonctionne parfaitement avec des sources TeX ou LaTeX. Il peut être appelé directement depuis `Emacs` ou `XEmacs`.

### Le dictionnaire de René Cougnenc

Le dictionnaire de [René Cougnenc](https://fr.wikipedia.org/wiki/René_Cougnenc) peut s'utiliser en mode shell sous Linux. Il contient 95 000 mots et 39 000 codes postaux, et permet de vérifier très rapidement une orthographe. Ce dictionnaire a longtemps été disponible sur la [page web de David Trystram](http://dtrystram.free.fr/). Maintenant, le plus simple est de le récupérer depuis les [sources Debian](http://snapshot.debian.org/package/le-dico-de-rene-cougnenc/).

### Texspell

[TeXspell](https://github.com/jeertmans/texspell) est un projet prometteur de Jérome Eertmans. Il s'appuie sur [LanguageTool](https://dev.languagetool.org/http-server.html) pour la vérification de la grammaire et de l'orthographe, et sur [OpenDetex](https://github.com/pkubowicz/opendetex) pour l'analyse du document TeX. Il n'est pas encore entièrement utilisable, mais pourrait bientôt devenir le vérificateur de langue le plus complet pour les documents LaTeX.

## Sous Mac OS

### Excalibur

{ctanpkg}`Excalibur <excalibur>` propose plusieurs dictionnaires.

(hunspell-1)=

### Hunspell

Voir ci-dessus.

### Programmes de correction

Il existe des correcteurs orthographiques disponibles et utilisables en ligne (internet) et d'autres utilisables directement dans les programmes utilisés :

- [Antidote](http://www.druide.com/) ;
- [Prolexis](http://www.prolexis.com/).

## Sous Windows

### amspell

> {ctanpkg}`amspell` peut être appelé depuis un éditeur.

### jspell

> {ctanpkg}`jspell`, pour MS-DOS, est basé sur `ispell`.

(le-dictionnaire-de-rene-cougnenc-1)=

### Le dictionnaire de René Cougnenc

Ce dictionnaire est utilisable sous MS-DOS. Voir ci-dessus.

### Microspell

[Microspell](http://www.microspell.com/) (commercial) peut être appelé depuis PCTeX pour Windows.

### textpad

L'éditeur [textpad](http://www.textpad.com/) sous Windows est capable de supporter des textes LaTeX et comprend de nombreux dictionnaires.

### L'Orthophile

[L'Orthophile](http://jeannoel.saillet.free.fr/Orthophile/Orthophile.htm), écrit en `logo`, est un programme GNU. Il permet la correction orthographique, mais aussi grammaticale... Il ne traite que les fichiers purement texte.

### Correcteur101

Il existe aussi le programme [correcteur101](http://www.mysoft.fr/correcteur.htm). Il existe en version bilingue, en version normale et professionnelle. Il permet la correction syntaxique (français, anglais), orthographique, grammaticale, et typographique. Il s'intègre parfaitement aux applications existantes sous Windows (et plus sous Linux, malheureusement).

(programmes-de-correction-1)=

### Programmes de correction

Il existe des correcteurs orthographiques disponibles et utilisables en ligne (internet) et d'autres utilisables directement dans les programmes utilisés :

- [Antidote](http://www.druide.com/) ;
- [Cordial](http://www.synapse-fr.com/) ;
- [Prolexis](http://www.prolexis.com/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,dictionnaire GUTenberg,correcteur orthographique pour LaTeX,correcteur grammatical,grammaire,spell checking
```

## Autres systèmes

Le programme Pascal pour VMS {ctanpkg}`vmspell` traite des particularités de fonctionnalités importantes de la syntaxe LaTeX. Il dispose d'un [dépôt GitHub](https://github.com/rf-latex/vmspell) mais est en fait figé depuis 1994.

## Des solutions directement avec LaTeX

L'extension (expérimentale) {ctanpkg}`spelling` pour `LuaTeX` et `LuaLaTeX` va plus loin : elle utilise le code `lua` pour extraire les mots *pendant que la composition est en cours* mais avant que la césure ne soit appliquée. Chaque mot est recherché dans une liste de fautes d'orthographe connues, et le mot est mis en surbrillance s'il y apparaît. En parallèle, un fichier texte est créé, traitable par un correcteur orthographique classique pour produire une liste des orthographes impropres. La documentation de l'extension montre le résultat final.

______________________________________________________________________

*Source :* {faquk}`Spelling checkers for work with TeX <FAQ-spell>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,correcteur d'orthographe pour LaTeX,correcteur de grammaire pour LaTeX
```

