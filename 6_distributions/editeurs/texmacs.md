# Puis-je utiliser l'éditeur TeXmacs ?

- Oui, si vous le souhaitez, mais TeXmacs n'est pas relié à TeX ou LaTeX, malgré son nom.

[GNU TeXmacs](https://fr.wikipedia.org/wiki/GNU_TeXmacs) est un logiciel libre pour la composition de documents scientifiques incluant un support pour les formules mathématiques, les figures, une gestion bibliographique, etc. Cependant TeXmacs est un logiciel indépendant de TeX ou LaTeX, très différent de ceux-ci et ne faisant même pas appel à eux en arrière-plan. Il utilise ses propres algorithmes de typographie [^footnote-1].

:::{tip}
GNU TeXmacs peut être utilisé comme [tableau interactif pour des cours de maths en visioconférence](https://www.youtube.com/watch?v=usL0hNIz8ng).
:::

______________________________________________________________________

*Source :* [GNU TeXmacs](https://fr.wikipedia.org/wiki/GNU_TeXmacs).

```{eval-rst}
.. meta::
   :keywords: éditeur de texte, éditeur scientifique, formules mathématiques
```

[^footnote-1]: [TeXmacs documentation : Mathematical typesetting](http://www.texmacs.org/tmdoc/devel/source/maths.en.html)

