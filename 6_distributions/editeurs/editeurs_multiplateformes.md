# Quels sont les éditeurs utilisables sous les autres systèmes d'exploitation ?

## Eclipse

Très récemment, des plugins pour l'environnement de développement `Eclipse` ont été écrits. Certains proposent la gestion de projets évolués (la comptabilité de ces plugins n'est pas assurée). Voici les URL :

- <http://sourceforge.net/projects/eclipse-latex/>,
- <http://sourceforge.net/projects/etex/>,

Ces programmes utilisent `java` et sont donc utilisables partout où il y a une `jvm` disponible.

## Eddi4tex

{ctanpkg}`Eddi4tex <eddi4tex>`, sous MS-DOS, est un éditeur spécifiquement conçu pour TeX, il offre la couleur, vérifie la syntaxe. <https://www.ctan.org/tex-archive/systems/os2/epmtex>.

## GNU Emacs

`GNU Emacs` avec son module `AUCTeX` peut également être utilisé sous MS-DOS ou OS/2.

## JED

[JED](http://www.jedsoft.org/jed/) est un clone multiplate-forme proche d'emacs. `JED` est en fait un `Emacs` allégé qui offre des facilités dans l'édition de fichier TeX et LaTeX. Il tourne sous Unix, VMS, MS-DOS et Windows.

## NetBeans

Il existe aussi une extension à [NetBeans](http://freshmeat.net/projects/nblatex/) qui permet d'éditer du LaTeX. Ce programme utilise `java` et est donc utilisable partout où il y a une `jvm` disponible.

## Quelques compléments

Les utilisateurs de LaTeX à la recherche d'installations se basant sur des « makefiles » consulteront la question « {doc}`Comment écrire un « Makefile » pour mes documents LaTeX ? </2_programmation/compilation/ecrire_un_makefile_pour_compiler_mon_document_latex>` ».

Alors que de nombreux éditeurs orientés TeX et LaTeX peuvent prendre en charge le travail sur les fichiers `BibTeX`, il existe de nombreux systèmes qui fournissent un accès spécifique « façon base de données » à vos fichiers `BibTeX` --- voir « {doc}`Comment créer un fichier bibliographique BibTeX ? </3_composition/annexes/bibliographie/construire_un_fichier_bibtex>` ».

______________________________________________________________________

*Source :* {faquk}`TeX-friendly editors and shells <FAQ-editors>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeurs de texte,éditeurs pour LaTeX
```

