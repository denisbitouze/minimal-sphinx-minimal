# Quels sont les logiciels permettant de créer une bibliographie ?

- L'outil standard est BibTeX. C'est un programme extrêmement (mais
  difficilement) configurable, qui prend en entrée une base de données
  bibliographiques, un style bibliographique, et une liste de références citées
  dans un document, et retourne les caractéristiques complètes des références
  citées, ces caractéristiques étant extraites de la base de données. Plus de
  détails dans la partie~\\vref\{bibliographie}, qui est consacrée aux
  bibliographies.
- BibTeX8 est une version « 8-bits » de BibTeX, écrite en C. Les auteurs, Niel
  Kempson et Alejandro Aguilar-Sierra, ont transformé les sources de BibTeX en
  C, puis ont ajouté de nombreuses extensions :
  - passage de 16 bits à 32 bits, et augmentation des limites de BibTeX, dont
    certaines étaient franchement basses ;
  - certaines de ces limites sont maintenant contrôlables depuis la ligne de
      commande, et donc virtuellement non bornées ;
  - et surtout, gestion d'alphabets plus généraux, et possibilité de spécifier
    l'ordre alphabétique voulu.

C'est essentiellement ce dernier point qui constitue une grande avancée, et qui
a d'ailleurs valu son nom au logiciel. Il est en effet possible, lorsqu'on
utilise BibTeX8, d'entrer directement les caractères « 8-bits », plutôt que de
mettre le codage LaTeX pour ces caractères. L'intérêt peut sembler faible, mais
c'est néanmoins la source de nombreux problèmes de tri et de calculs de labels
par BibTeX. BibTeX8 fournit par ailleurs une solution au problème suivant : en
Suédois, certains caractères accentués font partie intégrante de l'alphabet. Par
exemple, `\"o`, qui donne « ö », n'est pas la 15{sup}`e` lettre de l'alphabet,
mais la 29{sup}`e`. Obtenir un classement satisfaisant cette condition avec
BibTeX nécessite des acrobaties inextricables qui rendent le fichier `.bib` très
peu portable. BibTeX8 permet de spécifier un fichier indiquant l'ordre des
caractères dans l'alphabet, ce qui fournit une façon élégante de résoudre ce
problème.

S'il n'avait que des avantages, BibTeX8 aurait été adopté très
rapidement. Cependant, il fait une distinction entre `\"o` et `ö`, alors qu'on
voudrait, au final, que les deux soient traités de la même manière. Par
ailleurs, il ne fournit qu'une extension 8-bits, alors que le problème est bien
plus général. De l'aveu même des auteurs, c'est une extension un peu trop
particulière, et un vrai BibTeX, propre et général comme promet de l'être BibTeX
1.0, serait nettement préférable.

Plusieurs programmes de remplacement de BibTeX sont disponibles sur
Internet. Ils sont généralement basés sur des langages de scripts, comme
`perl`. Cependant, aucun d'eux n'a été réellement développé dans le but de
remplacer définitivement BibTeX, et tous sont restés à l'état de prototype.

:::{todo}
Mettre à jour en parlant de BibLaTeX.
:::

```{eval-rst}
.. meta::
   :keywords: BibLaTeX,LaTeX,bibliographie
```

