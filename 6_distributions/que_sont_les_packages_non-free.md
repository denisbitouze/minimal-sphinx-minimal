# Qu'est-ce que l'arborescence « non libre » du CTAN ?

Lorsque le CTAN a été fondé, dans les années 1990, il était relativement rare de publier les conditions dans lesquelles une extension TeX était distribuée (ou, en tout cas, de publier ces conditions de façon formelle).

Cependant, avec l'avènement des *distributions* TeX, les gens ont commencé à réaliser que ces informations étaient indispensables pour protéger ceux qui créent, distribuent ou vendent les disques contenant les extensions en question. Les distributeurs ont besoin d'avoir ces informations sur les licences pour décider quelles extensions peuvent être redistribués, et lesquelles ne le doivent pas.

L'équipe du CTAN a décidé qu'il serait utile pour les utilisateurs (et les distributeurs, sans parler des auteurs de paquets) de séparer les paquets candidats à la distribution de ceux qui, d'une certaine manière, ne sont pas « libres ». C'est ainsi qu'est né le répertoire `nonfree` (= *non libre*).

Dès le départ, cette arborescence a été controversée : les raisons pour lesquelles un paquet pouvait y être placé étaient très contestées, et l'équipe du CTAN n'a pu remplir cette arborescence que lentement. Il est devenu évident pour l'équipe que le projet ne serait jamais terminé.

## Et maintenant ?

Maintenant, le catalogue CTAN enregistre la nature de la licence d'une bonne partie des paquets qu'il décrit (bien qu'il en reste encore pour lesquels la licence est inconnue, ce qui équivaut pour les distributeurs à une licence interdisant la distribution). Comme la couverture du CTAN par le catalogue est plutôt bonne (et s'améliore encore, lentement), la règle générale pour les distributeurs est devenue « si le paquet est listé dans le catalogue, vérifiez si vous pouvez le distribuer ; si le paquet n'est pas listé dans le catalogue, ne pensez même pas à le distribuer ».

:::{note}
Le catalogue n'a qu'une [liste modeste de licences](https://ctan.org/license/), mais elle couvre l'ensemble utilisé par les paquets sur le CTAN, avec un joker `other-free` qui couvre les paquets que les administrateurs du CTAN pensent être libres même si les auteurs n'ont pas utilisé une licence standard.
:::

Il y a un corollaire à la « règle générale » : si vous remarquez quelque chose qui devrait être dans les distributions, mais pour lequel il n'y a pas d'entrée dans le catalogue, veuillez le faire savoir [à l'équipe du CTAN](https://ctan.org/contact). Il se peut que le paquet ait juste été oublié. Certains ne sont pas catalogués parce qu'il n'y a pas de documentation et que l'équipe ne comprend tout simplement pas le paquet.

L'arborescence *nonfree* est donc en cours de démantèlement et son contenu est déplacé (ou ramené) dans l'arborescence principale du CTAN. La réponse à la question est devenue : « l'arborescence *nonfree* était une partie du CTAN dont le contenu est maintenant dans l'arborescence principale ».

______________________________________________________________________

*Sources :*

- {faquk}`What was the CTAN « nonfree » tree? <FAQ-nonfree>`
- [How can a package be listed on CTAN but not be available in Tex Live?](https://tex.stackexchange.com/questions/185495/how-can-a-package-be-listed-on-ctan-but-not-be-available-in-tex-live)

```{eval-rst}
.. meta::
   :keywords: LaTeX,non-free,nonfree,extensions propriétaires,packages propriétaires,licences,LPPL
```

