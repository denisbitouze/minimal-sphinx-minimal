# Comment installer des extensions avec le gestionnaire MiKTeX ?

Les extensions destinées à être utilisés avec MiKTeX sont maintenues avec une grande efficacité par les responsables du projet (les nouvelles extensions et les mises à jour sur le CTAN sont généralement transférées dans le dépôt d'extensions de MiKTeX en moins d'une semaine). Il est donc logique pour l'utilisateur de MiKTeX de profiter du système plutôt que de passer par toutes les étapes d'une installation manuelle.

MiKTeX maintient une base de données des extensions qu'il « connaît », ainsi que des instructions d'installation (codées) qui lui permettent d'installer les extensions avec très peu d'intervention de votre part; vous pouvez mettre à jour la base de données par Internet.

Donc si MiKTeX connaît une extension dont vous avez besoin, utilisez ce système : tout d'abord, ouvrez la fenêtre du gestionnaire d'extensions de MiKTeX : cliquez sur `Start` \$\\rightarrow\$ `Programs` \$\\rightarrow\$ `MiKTeX` \$\\rightarrow\$ `MiKTeX Options`, et sélectionnez l'onglet `Packages`.

On the tab, there is an Explorer-style display of packages. Right-click on the root of the tree, `MiKTeX Packages`, and select `Search` : enter the name of the package you're interested in, and press the `Search` button. If MiKTeX knows about your package, it will open up the tree to show you a tick box for your package : check that box.

Dans l'onglet, les extensions sont affichées à la manière de l'explorateur Windows. Cliquez avec le bouton droit de la souris sur la racine de l'arbre, *MiKTeX Packages*, et sélectionnez *Search*; entrez le nom de l'extension qui vous intéresse, puis appuyez sur le bouton *Search*. Si MiKTeX connaît votre paquet, il ouvrira l'arborescence pour vous montrer une case à cocher pour votre extension : cochez cette case. Et voilà!

Si vous préférez un utilitaire en ligne de commande, il y a mpm. Ouvrez une fenêtre de commande, et tapez :

```bash
mpm --install=⟨package⟩
```

(ce qui suppose bien sûr que vous connaissiez le nom par lequel MiKTeX se réfère à votre extension).

______________________________________________________________________

*Source :* {faquk}`Installation using MiKTeX package manager <FAQ-inst-miktexstar>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer LaTeX sous Windows,MiKTeX,distribution LaTeX pour Windows,installer des packages,installer des extensions,installer des paquets,mettre à jour les packages,mettre à jour les extensions,gérer les packages
```

