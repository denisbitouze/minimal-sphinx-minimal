# Qu'est devenue la distribution gwTeX, pour MacOS ?

De 2000 à 2006, Gerben Wierda a maintenu une distribution TeX pour [Mac OS](https://fr.wikipedia.org/wiki/MacOS), **gwTeX**, et offert du support à ses utilisateurs par e-mail. Il a ainsi immensément aidé les utilisateurs de TeX sur Mac à faire la transition d'OS 9 à Mac OS X de façon simple. Au départ, la distribution était basée sur teTeX et nécessitait la compilation des programmes à partir des sources. Elle est rapidement devenue un mélange de TeX Live, teTeX et des ajouts spécifiques de Gerben, et elle est devenue facile à installer pour les utilisateurs non expérimentés. En 2007, la distribution est devenue presque entièrement basée sur TeX Live. Pour cette distribution (et la redistribution de toutes une série d'autres programmes associés : Ghostscript, ImageMagick, FontForge...), Gerben a écrit et maintenu un programme d'installation interactif, passant le réseau : *i-Installer*.

- Gerben a annoncé en 2006 qu'il arrêterait au 1{sup}`er` janvier 2007 le support par e-mail et le développement continu gwTeX et de *i-Installer*. Il continue cependant à maintenir les logiciels pour son usage personnel et contribue à TeX Live en mettant le résultat de son travail à disposition de la communauté TeX.

La distribution de référence pour Mac OS est maintenant [MacTeX](https://www.tug.org/mactex/).

______________________________________________________________________

*Sources :*

- [Brief History of gwTeX](https://www.tug.org/twg/mactex/award/2007/gerben/aboutgwtex.html),
- [Latex distributions. What are their main differences?](https://tex.stackexchange.com/questions/239199/latex-distributions-what-are-their-main-differences)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installing
```

