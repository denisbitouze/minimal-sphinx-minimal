# Quelles sont les distributions TeX pour les systèmes Unix et Linux ?

:::{warning}
Notez que Mac OS/X, bien qu'il s'agisse d'un système basé sur Unix, dispose de distributions dédiées décrites à la question « {doc}`Quelles sont les distributions TeX pour les systèmes Mac ? </6_distributions/installation/latex_sur_mac>` ».
:::

## TeX Live

[TeX Live](https://www.tug.org/texlive/) est la distribution TeX par excellence pour les systèmes Unix (y compris GNU/Linux et la plupart des autres systèmes libres de type Unix).

Cette distribution peut être installée :

- en ligne avec un [programme dédié](https://tug.org/texlive/acquire-netinstall.html) ;
- hors ligne avec le DVD {doc}`TeX collection </6_distributions/installation/dvd_texcollection>`.

Accessoirement, vous pouvez aussi en faire une [version portable](http://www.tug.org/texlive/portable.html) sur clé USB pour l'utiliser sur n'importe quel autre ordinateur.

Une fois que vous avez un système (qu'il soit installé en ligne ou à partir d'un DVD), un gestionnaire (`tlmgr`) peut à la fois maintenir votre installation à jour et ajouter des extensions en fonction de vos besoins.

## TeX-FPC

{ctanpkg}`TeX-FPC <tex-fpc>` (anciennement {ctanpkg}`TeX-GPC <tex-gpc>`) est une distribution « retour aux sources » des seuls utilitaires TeX (contrairement à TeX Live, aucun ensemble d'extension « sur mesure » n'est fourni). Elle est distribuée en tant que source et se compile avec Free Pascal, se rapprochant ainsi le plus possible de la distribution originale de {doc}`Knuth </1_generalites/glossaire/qu_est_ce_que_tex>`. Elle est réputée bien fonctionner mais l'omission de \$\\epsilon\$-TeX et `pdfTeX` lui donne un public assez limité.

