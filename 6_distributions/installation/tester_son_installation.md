# Comment tester mon installation de LaTeX ?

Vous venez d'installer une distribution LaTeX sur votre ordinateur. Comment savoir si tout fonctionne ?

- Vous pouvez ouvrir un terminal et taper :

```bash
pdflatex sample2e
```

après avoir appuyé sur la touche ⏎, vous devriez voir défiler quelques dizaines de lignes de texte ressemblant à ça :

```
This is pdfTeX, Version 3.14159265-2.6-1.40.21 (TeX Live 2020) (preloaded format=pdflatex)
  [...]
Output written on sample2e.pdf (3 pages, 142114 bytes).
Transcript written on sample2e.log.
```

et vous aurez dans le répertoire en cours un fichier `sample2e.pdf`. Si vous l'ouvrez avec un visualisateur PDF, il devrait [ressembler à ceci](http://mirror.ctan.org/macros/latex/base/sample2e.pdf).

Si ce n'est pas le cas, c'est qu'il y a un problème...

## Qu'est-ce que ça teste ?

Le document `sample2e.tex` est un fichier d'exemple faisant partie de toute distribution LaTeX. Il est dans l'arborescence de votre installation, et indexé par le logiciel chargé de localiser les fichiers (`kpsewhere`).

Donc en exécutant `pdflatex sample2e`, vous testez :

- que la commande `pdflatex` est accessible,
- que les fichiers de votre distribution sont bien indexés,
- que les fichiers des fontes, formats et extensions sont aux bons endroits, avec les bons droits...

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation,distribution,tester une installation,vérifier son installation,fichier d'exemple
```

