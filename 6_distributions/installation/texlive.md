# Comment installer des extensions avec le gestionnaire TeX Live ?

Le *TeX Live manager* (`tlmgr`) est le gestionnaire d'extensions de TeX Live. Il s'agit d'abord d'un outil en ligne de commande (également utilisable dans une fenêtre terminal sous Windows), mais il fournit également une interface graphique. Pour consulter sa (volumineuse) {texdoc}`documentation <tlmgr>`, utilisez la commande :

```bash
tldoc tlmgr
```

mais son fonctionnement de base est assez simple. Le gestionnaire doit savoir depuis où télécharger les fichiers à installer ; le plus simple pour réaliser cette configuration est d'exécuter :

```bash
tlmgr option repository http://mirror.ctan.org/systems/texlive/tlnet
```

qui utilise le sélecteur de miroir du CTAN pour choisir à votre place. Vous pouvez (bien sûr) spécifier un miroir particulier auquel vous faites confiance (par exemple celui de votre université ou votre fournisseur d'accès à internet).

Pour mettre à jour une seule extension, utilisez :

```bash
tlmgr update ⟨extension⟩
```

Pour mettre à jour tout ce qui est déjà installé sur votre système, y compris `tlmgr` lui-même, utilisez :

```bash
tlmgr update --self -all
```

## Comment utiliser l'interface graphique ?

Sous Linux (et MacOS), ça se fait avec cette commande :

```bash
tlmgr gui
```

Sous Windows, cette interface graphique ne fonctionnera probablement pas (car elle nécessite la bibliothèque [Tcl/Tk](<https://fr.wikipedia.org/wiki/Tk_(informatique)>), qui n'est généralement pas disponible sous WIndows, et non fournie par TeX Live).

D'autres interfaces ont été développées, utilisables sur toutes les plateformes :

- `tlshell`, qui semble être installée par défaut par TeX Live sous Windows et MacOS X;
- {ctanpkg}`tlcockpit`, écrite en Java (il vous faudra donc Java installé sur votre machine).

De façon intuitive, ces interface se lanceront avec les commandes

```bash
tlshell
```

ou

```bash
tlcockpit
```

respectivement.

## Comment réinstaller des extensions « forcibly removed » par tlmgr ?

Parfois, lors d'une mise à jour complète de TeX Live, quelques extensions sont supprimées par `tlmgr`, avec un message du genre :

```text
skipping forcibly removed package babel
```

Pour réinstaller automatiquement les extensions supprimées sans votre avis, vous pouvez exécuter la commande :

```bash
tlmgr update --all --reinstall-forcibly-removed
```

______________________________________________________________________

*Source :* {faquk}`Installation using TeXLive manager <FAQ-inst-texlive>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation,mise à jour,TeXlive,distribution LaTeX,gérer les extensions LaTeX,gérer les packages,mettre à jour les extensions,mettre à jour les packages
```

