# Quelles sont les versions commerciales de TeX ?

Il existe de nombreuses implémentations commerciales de TeX. La première est apparue peu de temps après l'apparition de TeX lui-même. Ce qui suit est probablement une liste incomplète et il n'y a ici aucune garantie de qualité sur ces implémentations.

En général, une implémentation commerciale est livrée avec des prévisualiseurs et des pilotes d'imprimante appropriés. Elle contient également une documentation complète et propose un service d'assistance. Dans certains cas, il s'agit d'un numéro gratuit (probablement applicable uniquement aux États-Unis et/ou au Canada), dans d'autres il s'agit d'une assistance par courrier électronique ou par téléphone.

## AmigaTeX

Cette implémentation complète pour le Commodore Amiga [n'est plus disponible](http://www.radicaleye.com/) selon son auteur, Tom Rokicki. Elle permettait une prise en charge complète à l'écran et à l'impression de tous les graphiques et polices PostScript, des graphiques raster IFF, de la génération automatique de polices et de toutes les macros et utilitaires standard.

## PCTeX

[PCTeX](https://pctex.com/) est une implémentation pour Windows, inactive depuis 2013.

## Scientific Word

[Scientific Word](https://www.sciword.co.uk/)'' et ''[Scientific Workplace](https://www.sciword.co.uk/scientificworkplace.htm) proposent un mécanisme de saisie presque {doc}`WYSIWYG </1_generalites/bases/wysiwyg>` des documents LaTeX. Les demandes faites au Royaume-Uni et en Irlande doivent être adressées à Scientific Word Ltd., les autres devant être adressées directement à l'éditeur, [MacKichan Software Inc](http://www.mackichan.com).

## Truetex

[TrueTeX](http://www.truetex.com/) fonctionne sur des versions de Windows allant jusqu'à NT. Les développements semblent s'être arrêtés en 2014.

## Y&Y TeX

[Y&Y TeX](http://www.tug.org/yandy/) (et son support) n'est plus disponible : la société Y&Y a en effet fait faillite. Les utilisateurs des systèmes `Y&Y` peuvent utiliser l'auto-assistance [mailing list](https://tug.org/pipermail/yandytex/) qui a été établie en 2003 ; le reste du contenu utilisable du site web de Y&Y est disponible sur <http://www.tug.org/yandy/>.

______________________________________________________________________

*Source :* {faquk}`Commercial TeX implementations <FAQ-commercial>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer,acheter,distribution commerciale
```

