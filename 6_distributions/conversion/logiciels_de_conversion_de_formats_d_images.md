# Logiciels de conversion de formats graphiques

Les formats graphiques sont très nombreux. Ils ont tous leurs qualités et leurs défauts, mais il est souvent nécessaire de les transformer pour les utiliser avec un logiciel particulier, ou pour les utiliser avec LaTeX.

Il faut aussi distinguer 2 grandes catégories de formats graphiques :

- Les formats bitmaps : ce sont des formats qui enregistrent les points graphiques, point par point. Ils ne se prêtent que rarement à un agrandissement correct (ils deviennent flous ou pixellisés). Ils ont produits avec des programmes tels `Gimp`, `Photoshop` ainsi que les outils basiques fournis avec les systèmes d'exploitation, comme « Paint »,. Les modeleurs 3D comme `blender`, `povray` (pour en citer deux...) peuvent aussi produire des images bitmaps. La numérisation d'images (avec un scanner ou un appareil photo) produit des images bitmaps, et il faut pouvoir les intégrer dans les documents.
- Les formats vectoriels (Postscript, `svg`, `xfig`...) sont produits par des logiciels spécialisés comme *Inkscape*, *Illustrator*, *Sketch*, *xfig*, *tgif*... Les dessins ou images sont redimensionnables sans perte de qualité, et sont les formats de prédilection à utiliser en vue d'une édition professionnelle.

Il nous faut encore distinguer deux types de convertisseurs :

- Les convertisseurs de formats de la même catégorie (bitmap        $\leftrightarrow$ bitmap)
- Les convertisseurs de format entre catégories (bitmap        $\leftrightarrow$ vectoriel)

Tous les logiciels de retouche comme *The Gimp* ou *Photoshop* permettent de charger une quantité impressionnante de formats de fichiers de type bitmap. *The Gimp* permet même maintenant de charger des fichiers vectoriels de type `svg` et de les transformer à la volée en bitmap pour pouvoir être édité immédiatement. Les fichiers Postscript sont aussi chargés automatiquement et transformés à la volée en bitmaps à la résolution voulue. Je suppose que *Photoshop* le permet aussi, avec encore en plus des formats propriétaires de type Windows (`swf`). Ils les sauvegardent sous tous les formats imaginables, souvent compressés, avec ou sans perte d'information.

Certains formats compriment les images sans perte d'information, et après décompression, on retrouve l'image d'origine. D'autres font subir une compression avec perte, parfois réglable comme dans les formats `jpeg`. Il n'est pas possible de retrouver la photo originale avec toutes ses informations. La compression peut venir d'une diminution du nombre de couleurs, ou d'autres approximations sur les détails.

Les autres convertisseurs font soit partie de logiciels plus importants comme *Illustrator*, soit sont sous formes de programmes indépendants comme *autotrace*. Ils permettent une vectorisation d'images bitmap et les sauvent ensuite sous des formats vectoriels variés : `svg`, `xfig` ou autres. Il est ensuite possible de les agrandir sans perte de qualité et de les travailler avec des logiciels de traitement d'images vectoriels.

## Où trouver des informations sur les fichiers graphiques en français ?

- Voici quelques sites qui permettront de vous familiariser avec les différents formats graphiques, de voir leurs qualités et défauts.
- <http://www.commentcamarche.net/video/formats.php3>,
- <http://www.lookimage.com/formats/>,
- <http://www.laltruiste.com/document.php?url=http://www.laltruiste.com/courshtml/imgformat.html>,
- <http://amssoft.free.fr/gifjpg.htm>,
- <http://www.net6tm.com/article.php3?id_article=6>,
- <http://vcampus.u-strasbg.fr/public/faerber/traitements/formats_graph.html>,
- <http://www.iptsos.com/formation/internet/format/graphi.php>,
- <http://www.cri.univ-rennes1.fr/documentations/Xwindow/guide/chapter2.12.html>,
- <http://www.ac-grenoble.fr/crt/national/tic2002/formgrap.htm>,
- <http://www.01net.com/article/192268.html>.

Ces différentes URL ont été obtenues par une première recherche avec Google et en affinant la recherche, il sera certainement possible d'avoir d'autres sites dédiés à certains type particuliers de formats. Ils vous expliqueront les détails de chaque format, la différence entre image bitmap et vectorielle, et vous donneront des liens vers les programmes permettant de gérer ces différents formats ainsi que des outils de conversion.

## Existe-t-il un site dédié à la conversion des formats graphiques utilisés sur Macintosh et PC ?

- Voici un site qui peut déjà vous donner des indications : <http://www.macdisk.com/graphfr.php3>.

## Comment convertir une image en police METAFONT ?

- Le programme {ctanpkg}`bm2font` permet de faire cela. Sa documentation est aussi disponible sur le [LaTeX Navigator](http://www.loria.fr/services/tex/).

Pour plus de détails sur METAFONT, il existe la liste [metafont@ens.fr](mailto:metafont@ens.fr). Pour plus d'informations sur cette liste : <https://www.gutenberg.eu.org/listes>.

## Quels sont les utilitaires de conversion de formats graphiques ?

- Le logiciel [ImageMagick](https://fr.wikipedia.org/wiki/ImageMagick) peut être vu comme un couteau suisse moderne pour traiter les images en ligne de commande : <https://imagemagick.org/>.

```{eval-rst}
.. todo:: ajouter des exemples de lignes de commande.
```

- [Netpbm](https://en.wikipedia.org/wiki/Netpbm) est plus ancien, mais est disponible sur tous les systèmes d'exploitation, dont tous les Unix. Il se compose de 200 programmes divers qui permettent de changer de format, réduire la résolution, composer plusieurs images en une seule de différentes façons. `netpbm` utilise un format intermédiaire pour ses traitements : pbm(5), pgm(5), ppm(5) et pam(5) : ces formats sont documentés dans les manpages (Unix) et [sur Wikipedia](https://fr.wikipedia.org/wiki/Portable_pixmap).

Ces programmes sont des programmes en ligne de commande et sous Unix sont utilisables enchaînés (pipe) les uns aux autres pour effectuer des tâches complexes. (C'est le principe d'Unix : chaque programme fait une chose simple, remplit sa tâche correctement et est utilisable en combinaison avec d'autres programmes de même type.)

Voici l'exemple d'une chaîne de traitement :

```bash
for i in *.png; do pngtopnm \$i | ppmtojpeg >`basename \$i .png`.jpg; done
```

`Netpbm` est dérivé du travail de Jef Poskanzer (`PbmPlus`), et a été amélioré, étendu par de très nombreux collaborateurs.

[Sur sa page d'accueil,](http://netpbm.sourceforge.net/) vous trouverez la documentation et un descriptif des possibilités des différents programmes fournis dans la suite.

## Existe-t-il un programme qui permet de connaître le format d'un fichier graphique ?

- Le programme `file` qui tourne sous Unix reconnaît la majorité des formats de fichier, en analysant leurs signature (et non pas l'extension...). Il est disponible à l'URL : <ftp://ftp.astron.com/pub/file>.

## Pouvez-vous nous donner différents noms de formats, de convertisseurs ou de librairies de traitement/conversion de formats de fichiers graphiques ?

- Voici sans ordre particulier une liste de programmes, librairies, formats, tous tirés de la manpage des outils `netpbm`.
- `ImageMagick` est une suite de programmes comparables à ceux de la suite `netpbm` mais accompagnée d'un visualiseur qui permet d'afficher les images et de voir l'effet des transformations effectuées. `ImageMagick` tourne sur quasiment tous les systèmes.
- `The Utah Raster Toolkit` a les mêmes fonctionnalités que `netpbm`. Ce package est basé sur le format RLE qui est convertible avec le format `pbm`. Vous pouvez obtenir des informations complémentaires à l'adresse suivante : <http://www.cs.utah.edu/research/projects/urt/>.
- `Ilib` est une librairie de fonctions C qui permet d'ajouter du texte à une image. Elle travaille avec le format de base de `netpbm`. Ses possibilités en matière de traitement du texte sont supérieures à celle de `netpbm`.
- `GD` est une librairie graphique que l'on peut appeler à partir de nombreux langages comme `perl`, `python`, `php` et qui permet de traiter et de convertir différents types d'image.
- `pnm2ppa` convertit vers le format de HP "Winprinter". C'est un sur-ensemble du programme `pbmtoppa` qui permet de traiter la couleur. [Il est disponible ici.](http://pnm2ppa.sourceforge.net/)
- Une autre version du couple `pnmtopng/pngtopnm` est disponible à l'URL : <http://www.schaik.com/png/pnmtopng.html>. Cette version est plus actuelle que la version fournie avec `netpbm`.
- `jpegtran` fait le même travail que certains programmes de `netpbm`, mais est spécialisé dans la gestion des fichiers `jpeg` qu'il fait sans perte de qualité et d'informations. Il n'y a pas besoin de décompresser l'image pour travailler avec ces utilitaires. À aller chercher à l'URL : <http://www.ijg.org/>.

## Comment extraire une image d'un `gif` animé ?

- Pour créer un `gif` animé, ou extraire une image d'un `gif` animé, il est possible d'utiliser `gifsicle`. Il est possible ensuite de convertir de et vers le format `gif` avec les outils `ppmtogif` et `giftopnm`. La page d'accueil est à l'URL~ : \\url{<http://www.lcdf.org/gifsicle>}.

## Y a-t-il des programmes qui permettent de travailler avec les fichiers `EXIF` ?

- Certains programmes permettent de travailler sur les fichiers graphiques d'extension `EXIF`. Voici deux programmes intéressants : `Exifdump` pour interpréter les en-têtes du fichier, ou `Jhead` à l'URL : <http://www.sentex.net/~mwandel/jhead>.
- La librairie `python` `EXIF` ainsi qu'un « dumper » est disponible à <http://pyexif.sourceforge.net/>.

## Comment faire des conversions image -> texte ?

- Il faut utiliser un scanner qui sauvegardera une image du texte sous forme d'image. Ensuite, des logiciels (OCR) de reconnaissance de caractères se chargeront d'extraire les parties texte. Nous pouvons citer `clara`, `gocr`, `kooka` sous Unix et gratuits, `finereader` sous Windows et Unix, mais payant, `textbridge` ou `omnipage`.

## Qu'en est-il des autres formats graphiques ?

- Le format `cal` qui vient de l'« US Department Of Defense ». Voir : <http://netghost.narod.ru/gff/graphics/summary/cals.htm>.
- Les formats « tableaux » : `dx`, `general`, `netcdf`, `CDF`, `hdf`, `cm` et `CGM+`.
- Le format « Windows Meta File » `wmf`. La librairie `Libwmf` convertit le format `wmf` vers des formats comme LaTeX, `pdf`, `png`.

## Qu'en est-il du format `dxf` (AutoCAD) ?

- Des programmes externes permettent maintenant d'interpréter ces formats. D'autres comme des logiciels `CAD` peuvent lire ce format et éventuellement le sauvegarder en autre chose.

## Comment convertir une image bitmap en vectorielle ?

- `autotrace` permet de convertir une image bitmap en image vectorielle. Il est possible de choisir son format de sortie. L'URL est : <http://autotrace.sourceforge.net/>.
- *Photoshop* permet également cette transformation.
- `potrace` est un utilitaire de transformation disponible à : <http://potrace.sourceforge.net/> .
- Enfin, `flash MX` permet aussi cette transformation.

```{eval-rst}
.. meta::
   :keywords: Association GUTenberg,LaTeX,ImageMagick,formats d'images,conversion,images bitmap,photos,convertir des fichiers,JPEG,PNG,BMP
```

