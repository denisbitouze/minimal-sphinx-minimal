# Logiciels de conversion de formats de texte

Cette partie traitera des logiciels qui permettent de convertir les formats de fichiers « texte », propriétaires ou non, vers LaTeX et vice versa. Il est souvent nécessaire de récupérer des documents pour les traiter et ceux-ci arrivent dans tous les formats imaginables. Chaque éditeur de logiciel utilise « son » format, et il n'est pas toujours documenté. Il faut parfois utiliser des textes composés avec des programmes anciens dont la récupération n'est même plus possible avec les versions modernes de ces programmes... et certains de ces programmes n'existent plus ou n'ont pas été mis à jour depuis des années.

- En 2020, le couteau suisse le plus abouti pour les conversions entre formats de document [est Pandoc](https://pandoc.org/), disponible pour Linux, MacOS, Windows et quelques autres OS. Cet outil, écrit en Haskell, est activement maintenu par John MacFarlane, et réalise des conversions de très bonne qualité.

Les réponses données ci-dessous sont anciennes. Certaines sont peut-être encore valables, mais la plupart sont obsolètes, ou ne répondent qu'à des besoins ponctuels.

(logiciels-de-conversion-de-formats-de-texte-1)=

# Logiciels de conversion de formats de texte

## Comment convertir du LaTeX en Word ?

- `TeXport`, de K-Talk permet de convertir vos fichier TeX et LaTeX en documents `WordPerfect` ou Microsoft Word 97, 2000, XP. Il traite les styles de fontes, les notes de bas de page, les caractères grecs, les mathématiques, les tableaux simples, les accents européens, ainsi que les commandes `\def` et `\renewcommand`.

Ce logiciel est payant. Pour plus de détails, voir : <http://www.ktalk.com/>.

## Comment convertir du Word en LaTeX ?

- Il y a l'outil `Publishing Companion` de K-Talk (voir <http://www.ktalk.com/>).
- `Wd2latex`, pour MS-DOS, est disponible sur <https://ctan.org/tex-archive/support/wd2latex>. Cet utilitaire paraît rudimentaire, et date de 1991.
- `Word2x` disponible sur {ctanpkg}`word2x` permet de convertir du `Word 6.0` en texte ou en LaTeX. Sa page d'accueil est : <http://word2x.sourceforge.net/>.
- Voir également `texcnven` dans <http://tug.org/utilities/texconv/index.html>.
- `WINW2LTX`, de A. Cottrell, est disponible sur <https://www.ctan.org/support/winw2ltx/english/ww2/> pour `WinWord2`, et <https://www.ctan.org/support/winw2ltx/english/ww6/> pour `WinWord6`. Il ne traite ni les équations, ni les graphiques.
- Le programme `catdoc` (<http://www.45.free.net/~vitus/ice/catdoc>) est à la base prévu pour convertir un document Word en texte brut, mais grâce à l'option `-t`, il peut générer un document TeX.
- Le projet [wvWare](http://wvware.sourceforge.net/) fournit des utilitaires qui permettent de convertir un document Word en divers formats. Il comprend notamment le programme `wvLatex` qui permet de convertir un document word en LaTeX.

## Comment convertir du `scribe` en LaTeX ?

- `s2latex`, de V. Jacobson, fait partie de la distribution GUTenberg pour Unix. Celle-ci est disponible par FTP sur <http://mirror.gutenberg.eu.org/pub/GUTenberg/distribunix/>.

## Comment convertir du `WordPerfect` en LaTeX ?

- `wp2latex`, de R.C. Houtepen, est disponible sur <https://www.ctan.org/obsolete/support/wp2latex/>. `wp2latex` a été écrit pour PC en Turbo Pascal. Il travaille à partir d'un document `WordPerfect5.0`. On note quelques limitations concernant les indices, la table des matières, les marges et les graphiques.
- `wp2latex`, de [Glenn Geers](mailto:dgeers@bigpond.net.au), a été écrit en C. Il devrait être étendu à *WordPerfect* 5.1 et 6. La version la plus récente est disponible en consultant la page d'accueil à l'URL : <http://cmp.felk.cvut.cz/~fojtik/wp2latex/wp2latex.htm> ou sur <https://www.ctan.org/obsolete/support/wp2latex/>.
- `texperf` est disponible sur <https://www.ctan.org/support/texperf/>.

## Comment convertir du LaTeX en `rtf` ?

- Le format `RTF` (Rich Text Format) correspond à un format `ASCII` contenant des informations de formatage lisibles (entre autres) par `Word` pour MS-DOS, `Word` pour Windows et `Word` pour MacOS. Pour plus d'informations sur ce format, voir : <http://msdn.microsoft.com/library/en-us/dnrtfspec/html/rtfspec.asp>.
- `TeX2RTF`, est disponible sur <https://www.ctan.org/support/tex2rtf/> ou à l'URL : <http://www.wxwindows.org/tex2rtf/index.htm>. Il tourne sous SPARC Open Look, SPARC Motif et Windows. Il permet de sauvegarder sous différents formats comme le format `help` de Windows, `rtf`, `html`. `TeX2RTF` ne gère pas les formules mathématiques ni les tableaux compliqués.
- `LaTeX2rtf`, de F. Dorner et A. Granzer, est disponible sur <https://www.ctan.org/support/latex2rtf/>. Une version est également disponible sur <http://latex2rtf.sourceforge.net/>. Il existe une version Windows avec GUI depuis 2004. L'exécutable pour l'architecture 386 est fourni ; il existe aussi un Makefile compilant le tout pour Unix.

`LaTeX2rtf` produit du `rtf` avec la simple commande (sous MS-DOS) :

```bash
ltx2rtf srcltx.tex
```

qui produit `srcltx.rtf`.

Ce programme traite (sauf erreur) les commandes de changements de polices de LaTeX du genre `\bfseries`, `\ttfamily`, `\sffamily`, `\slshape` et les tailles de polices, mais aussi les guillemets sous la forme « et ». La numérotation automatique pour les sections, chapitres, etc. est aussi prise en compte mais compte tenu de certains bugs de *MS Word 6.0*, leur présentation peut être aléatoire.

:::{warning}
Ce programme ne traite pas toutes les formules mathématiques mais traduit du texte LaTeX pouvant être relu en *MS Word*.

Les lettres accentuées au codage 850 ne sont pas traduites correctement : il faut utiliser le codage 7 bits (`\'e`, `\^i`, etc.).
:::

## Comment convertir du `rtf` en AllTeX ?

- `rtf2TeX` (Unix), de [R. Lupton](mailto:rhl@astro.princeton.edu), est disponible à {ctanpkg}`rtf2tex`.
- `rtf2latex` (Unix), de E. Wechtl, est disponible à {ctanpkg}`rtf2latex`.
- `rtf2latex2e` (MacOS, Unix, Windows), d'Ujwal Sathyam, est disponible sur {ctanpkg}`rtf2latex2e`.

:::{note}
Il n'y a pas eu de mise à jour depuis 2001 et La page d'accueil a disparu.
:::

- `w2latex`, de J. Garmendia et J.-L. Maltret, est disponible sur <https://www.ctan.org/support/w2latex/>. `w2latex` est un outil de conversion entre deux formats descripteurs de texte, permettant de traduire un document rédigé sous certaines versions de *MS Word* pour obtenir du code LaTeX.
- `rtflatex`, de Daniel Taupin, est capable de transcrire un fichier `rtf` en LaTeX et de lui associer des fichiers de macros (`sty`). Toutefois, il ne traite pas les formules ni les tables. `rtflatex` est disponible sur {ctanpkg}`rtflatex`.

## Comment convertir du *Excel* en LaTeX ?

- Le fichier `tabular.xla`, de A. Gjestland, est disponible sur <http://tabular.hsh.no/>.

:::{note}
```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.Il existe deux versions ``Tabular4.xla`` pour ``Excel4`` et ``Tabular5.xla`` pour ``Excel5``. Une documentation est également disponible : ``docu-tab.ps.Z``. Vous pouvez aussi consulter la page WEB http://www.nak.mech.tut.ac.jp/sugi/vtab.asp.
```

Pour tous renseignements, contacter l'auteur par e-mail : [ag@hsh.no](mailto:ag@hsh.no). Une documentation assez récente y est également disponible.
:::

- Il existe également l'utilitaire `Excel2LaTeX` disponible à {ctanpkg}`excel2latex`. La version 2.0 est compatible avec Excel 95 et 97.

## Comment convertir du `HTML` en LaTeX ?

- L'URL <http://www.w3.org/hypertext/WWW/Tools/html2things.html> propose un certain nombre d'outils de conversion à partir d'un document HTML.
- {ctanpkg}`html2latex`, de N. Torkington, également disponible à <http://www.w3.org/hypertext/WWW/Tools/html2latex.html>. Cet outil semble assez limité. Il est basé sur le parser HTML de [Mosaic (NCSA)](https://fr.wikipedia.org/wiki/NCSA_Mosaic).
- `h2l`, de J. Korpela, est disponible à [http://www.cs.tut.fi/\\%7Ejkorpela/](http://www.cs.tut.fi/%7Ejkorpela/). Il est un peu plus complet que le précédent mais doit encore être complété. Une documentation est également prévue.

:::{note}
Le langage HTML étant à la base une application de de SGML, une autre solution (non testée) serait d'utiliser un traducteur SGML       $\rightarrow$ LaTeX. Le système `linuxdoc-sgml` devrait pouvoir faire cela si on lui donne la DTD de la version de HTML concernée et les règles de traduction. Il est disponible à <ftp://tsx-11.mit.edu/pub/linux/docs/linuxdoc-sgml-1.5.tar.gz>.
:::

- `techexplorer` disponible sur <http://www-306.ibm.com/software/network/techexplorer/> est un navigateur Hypermedia qui met en forme des documents scientifiques écrits en LaTeX pour les présenter sous forme `html`. Il gère notamment les expressions mathématiques. Il est compatible avec les navigateurs *Netscape*, *Mozilla* et avec *Microsoft Internet Explorer*. Pour plus de détails voir la liste de discussion [techexplorer@listserv.nodak.edu](mailto:techexplorer@listserv.nodak.edu).
- Le package {ctanpkg}`typehtml` de David Carlisle permet de lire des fichiers `HTML2` et les mathématiques dans `HTML3.0`.
- `Html2tex` est disponible à l'adresse <http://home.planet.nl/~faase009/html2tex_c.txt>. Vous obtiendrez directement le source C à cette adresse ainsi que des commentaires inclus. Une documentation complète se trouve ici en anglais : <http://home.planet.nl/~faase009/html2tex.html>.
- `vulcanize` disponible à <http://www.cis.upenn.edu/~mjd/bin/vulcanize>. La page de manuel est disponible à <http://www.plover.com/vulcanize/>.
- `QWERTZ` disponible à <http://www.megginson.com/Software/psgmlxml-19980218.zip>.

```{eval-rst}
.. todo:: Apparemment plus disponible.
```

## Comment convertir du LaTeX en HTML ?

- `IDVI` est un outil qui permet de présenter des documents `html` à partir de sources LaTeX. `IDVI` est une implémentation `Java` d'un visualiseur DVI qui permet d'afficher des pages LaTeX sur le WEB et non plus un document HTML. Pour plus de détails, consulter : <http://www.geom.uiuc.edu/java/idvi/>.
- `LaTeX2HTML` pour Windows 95, 98, NT et Unix, de N. Drakos, écrit en Perl, est disponible sur {ctanpkg}`latex2html`. Pour plus de détails, consulter : <http://www.latex2html.org/>.

:::{note}
Le package {ctanpkg}`html` permet d'insérer dans le source LaTeX des commandes traitées de manière spéciale par le convertisseur `latex2html`.
:::

Il existe également une liste de discussion `latex2html` à laquelle on peut s'abonner en envoyant un mail contenant `subscribe latex2html [⟨adresse⟩]` à [majordomo@mcs.anl.gov](mailto:majordomo@mcs.anl.gov).

La nouvelle version de `LaTeX2HTML` est la version 2002-2.1.

- Il est possible également d'utiliser {ctanpkg}`hyperlatex`, qui produit des documents HTML plus jolis pour peu que votre source LaTeX suive certaines conventions. Pour plus de détails, consulter : <http://www.cs.uu.nl/~otfried/Hyperlatex/>.

:::{warning}
Cet utilitaire nécessite l'utilisation de LaTeX et de `Emacs`.
:::

- À voir également : `OmniMark` sur <http://www.omnimark.com/>.
- Il existe également {ctanpkg}`ltx2x`.
- `TeX4ht` est un environnement configurable qui permet de générer des documents hypertextes. Pour plus de détails, voir : <http://www.cis.ohio-state.edu/~gurari/TeX4ht/mn.html> ou <http://www.leps.de/tex4ht.html> (en allemand).
- `HeVeA` est un programme gratuit destiné à transformer un source LaTeX en `HTML 4.0`. Ce programme est disponible à l'URL <http://pauillac.inria.fr/~maranget/hevea/> C'est un interpréteur du langage TeX. Donc toutes les nouvelles macros, tous les environnements que l'on peut définir sont correctement interprétés, que l'on peut charger des fichiers de style exotiques, etc. si l'on désire obtenir un résultat particulier dans la sortie `HTML`. `HeVeA` transforme les formules mathématiques en `HTML 4.0` « pur » (sans graphique) en utilisant la fonte symbole, des tags de positionnements fins et des tables. (Contribution due à Éric Brunet).
- `ltoh` est convertisseur entièrement écrit en Perl (donc indépendant de la plate-forme), très paramétrable. Il sait gérer différents types de macros, les tables, la plupart des caractères spéciaux, etc. Il est possible de programmer la transformation à effectuer quand telle ou telle balise LaTeX est rencontrée. Pour plus de détails (version 2000b), voir : <http://www.best.com/~quong/ltoh/>.
- `mn` de E. Gurari est un convertisseur Plain TeX ou LaTeX vers `html`. Il est hautement configurable. Cet outil est disponible sur : <http://www.cis.ohio-state.edu/~gurari/TeX4ht/mn.html>.
- `Webbuilder` de MicroPress, est un éditeur TeX qui permet de générer du code `html`. Il gère aussi bien les commandes TeX que les macros utilisateur. Tous les environnements mathématiques, tables, notes de bas de page, table des matières, références, listes, etc, sont gérés. La plupart des documents LaTeX peuvent facilement être convertis. `Webbuilder` est disponible sous Windows 95, 98, NT, et 3.1 (Win32 nécessaire). Pour plus de détails et quelques exemples, consulter : <http://www.micropress-inc.com/>. Pour toutes questions écrire [au support de Micropress](mailto:support@micropress-inc.com).
- `TTH` est un convertisseur TeX        $\rightarrow$ `html` disponible à <http://hutchinson.belmont.ma.us/tth/>. Il gère les mathématiques (il convertit les formules mathématiques au lieu d'en faire des images) et reconnaît les structures de document Plain TeX et LaTeX qu'il convertit en hyperliens. `TTH` est rapide et portable.
- Le package {ctanpkg}`typehtml` de David Carlisle permet de gérer des documents HTML directement depuis le source LaTeX.
- `Texpider` est un moteur TeX qui produit directement du HTML. Il est possible de l'acheter ici : <http://www.micropress-inc.com/webb/wbstart.htm>. Il ne produit pas de fichier DVI mais directement du HTML.
- `Tex converter` est un programme intégrateur de plusieurs convertisseurs de LaTeX vers autre chose, dont du HTML (sous tous les Windows). Il est disponible à <http://www.mayer.dial.pipex.com/tex.htm>.

## Existe-t-il un programme qui transforme les formules mathématiques et les tables LaTeX en HTML ?

- `math2html` convertit des tables et des formules mathématiques LaTeX vers du HTML. Les formules mathématiques sont transformées en images bitmap s'il n'est pas possible de les transformer en HTML.

## Comment convertir du WEB en LaTeX ?

- `SchemeWEB`, de J. Ramsdell, est disponible sur <https://www.ctan.org/web/schemeweb/>.
- Il existe également des programmes pour de nombreux autres langages. Ainsi, `ocamlweb` (<http://www.lri.fr/~filliatr/ocamlweb/index.fr.html>) permet de faire de la programmation littéraire en CaML.

## Comment convertir du SGML en AllTeX ?

:::{note}
SGML = *Standard Generic Markup Language.*

Il s'agit d'une norme de balisage de documents structurés. Pour plus de détails, consulter : <http://www.sil.org/sgml/sgml.html>.
:::

- `sgmlspm`, de D. Megginson, est disponible à <http://www.garshol.priv.no/download/xmltools/prod/SGMLSpm.html>. Cet outil nécessite Perl.
- `stil`, de J. Schrod et C. Detig, est disponible sur <ftp://ftp.th-darmstadt.de/pub/text/sgml/stil/>.

Ces deux outils s'appuient sur le parser `nsgmls` de J. Clark qui traduit du `SGML` en `ESIS`.

- `linuxdoc-sgml` s'appuie aussi sur `sgmls` de J. Clark.
- {ctanpkg}`sgml2tex`, de Peter Flynn, disponible sur le CTAN.

## Comment convertir un fichier 8 bits en fichier 7 bits ?

- Bernd Raichle fournit un tel convertisseur parfaitement portable qui se présente sous forme d'un programme TeX appelé `convert.tex`. Celui-ci est disponible sur <https://www.ctan.org/language/typingtex/>.

```{eval-rst}
.. todo:: Non disponible sur le CTAN au 19/09/2020.
```

- Il en existe également une version modifiée par L. Siebenmann dans <https://www.ctan.org/language/typingtex/Convert-RaichleHacked.dir/> (interface modifiée, ajout de quelques tables de transformation dont `mac2dek.tbl` ou `pc2dek.tbl` pour la conversion du Macintosh ou du PC vers la syntaxe classique de Knuth (`\'e`, `\c{c}`, etc.)).
- L'outil `Tower of Babel` (avec option TeX activée), permet également de réaliser de telles conversions. Il est disponible pour MacOS sur <http://www0.univ-rennes1.fr/pub/mac/editeur/tower-of-babel-13.sit.hqx>.
- `recode` de F. Pinard est un programme Unix (GNU) qui convertit tous types de fichiers texte. De nombreux formats sont supportés (ASCII, EBCDIC, CDC, LaTeX, etc.).

Exemple :

```bash
recode latin1:applemac ⟨fichier⟩
```

- Dans `Emacs`, le mode `iso-cvt.el`, peut convertir automatiquement un fichier codé 7 bits en fichier codé 8 bits (et réciproquement) lors de la lecture et de l'écriture. Pour l'utiliser, ajouter `(require iso-cvt)` dans le `.emacs`. Le fichier sera alors en 7 bits sur le disque.
- De même, il y a le mode `x-symbol.el` pour `(X)Emacs`.
- Il existe également les utilitaires `kb7to8/8to7` distribués avec {ctanpkg}`french`.

## Comment convertir un fichier `ChiWriter` en TeX ?

- `chi2tex` est disponible sur <https://www.ctan.org/support/chi2tex/>. Il y a plusieurs versions du convertisseur, selon la version de `ChiWriter` à convertir. À partir des versions 2 ou 3 cela ne marchait que pour des fichiers très simples (pas plus d'un niveau d'exposant et d'indice, etc.). Il est probable que le convertisseur de la version 4 marche beaucoup mieux : il fait une analyse syntaxique des formules, alors que les versions 2 et 3 étaient purement graphiques.

```{eval-rst}
.. todo:: Non disponible sur le CTAN au 19/09/2020.
```

## Où trouver une FAQ de convertisseurs TeX / Traitement de texte ?

- La FAQ de W. Hennings est disponible à {ctanpkg}`wp-conv`.

## Comment convertir un fichier Postscript en ASCII ?

- `pstotext` est un outil Unix freeware qui permet d'extraire du texte ascii d'un fichier Postscript. Il utilise `Ghostscript`. `pstotext` est disponible à <http://www.research.digital.com/SRC/virtualpaper/pstotext.html>.

Sous Windows et OS/2, `pstotext` est accessible via l'outil `GSview` de R. Lang (version 2.0 minimum). Voir <http://www.cs.wisc.edu/~ghost/gsview/>.

Pour MS-DOS, `pstotext` est disponible à <http://www.cs.uu.nl/pub/TEX/MSDOS/pstotext.zip>.

## Comment convertir un fichier PDF en ASCII ?

- L'outil `pstotext` présenté à la question précédente permet également d'extraire du texte ASCII d'un fichier PDF. `pdftotext` est un autre programme spécifique au format PDF.

## Comment convertir du LaTeX en PDF ?

- L'utilitaire `ps2pdf` de `ghostscript` (version 4.01 au moins) permet de convertir des fichiers Postscript au format PDF d'Adobe. La dernière version de `ghostscript` est disponible sur <http://www.ghostscript.com/>.
- Sur MacOS, `ps2pdf` livré avec `cmactex`, nécessite l'installation de `psview`. Voir <http://www.kiffe.com/cmactex.html>.
- Les moteurs pdfTeX, luaLaTeX, etc., permettent de produire directement des fichiers PDF sans passer par un DVI. Ils supportent nativement l'inclusion d'objets graphiques et de liens hypertextes.
- `Adobe Acrobat Distiller` permet avec des fontes simples de générer des fichiers PDF à partir de PS. Selon votre version d'Acrobat Reader, il vaut mieux utiliser des fontes de type1 car les bitmaps de TeX peuvent être très mal rendus. Pour plus de détails consulter <http://www.adobe.com/>.

:::{note}
Il vaut nettement mieux demander à LaTeX de travailler avec des polices Postscript, si l'on compte transformer le document en PDF par la suite. Si vous utilisez des polices à résolution fixe, le texte aura une apparence on ne peut plus laide sur votre écran.
:::

## Comment définir son propre format de sortie ?

- {ctanpkg}`ltx2x` permet de remplacer des commandes LaTeX par des commandes définies par l'utilisateur.

```{eval-rst}
.. meta::
   :keywords: Pandoc,conversion,convertion,formats de documents,format universel,XML,SGML,HTML,LaTeX,tex4ht,Association GUTenberg,word,vieux documents
```

