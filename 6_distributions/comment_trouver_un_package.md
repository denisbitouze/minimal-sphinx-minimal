# Où télécharger cette extension ?

Tout d'abord, où avez-vous entendu parler de cette extension ?

Si l'information provient de cette FAQ, vous devriez avoir un lien vers le fichier. Cliquez sur les liens de la page en question, et vous devriez rapidement trouver les fichiers que vous voulez (soit un fichier unique, soit une archive...).

Si vous avez entendu parler du fichier ailleurs, il est aussi possible qu'on vous ait dit où chercher; sinon, essayez le moteur de recherche intégré au CTAN : <https://ctan.org/search>. Cet outil plutôt simple renvoie les résultats d'une recherche dans le catalogue du CTAN (qui couvre la plupart des paquets utiles), mais aussi les résultats d'une recherche sur les noms de fichiers contenus dans toutes les extensions stockées sur le CTAN.

En dernier recours, essayez les moteurs de recherche génériques : [Google](https://www.google.fr/), [Qwant](https://www.qwant.com/), [Exalead](https://www.exalead.com/), [Bing](http://www.bing.com/)... Ceci vous permettra de trouver des extensions qui sont par exemple sur [GitHub](https://github.com/) ou autre site distinct du CTAN.

Les extensions se présentent sous différentes formes : les plus simples consistent en un simple fichier `extension.sty` --- dans ce cas, il suffit de télécharger le fichier et de le copier {doc}`là où vous voulez l'installer </5_fichiers/installer_des_fichiers_pour_latex>`.

Autre cas fréquent : le fichier que vous voulez (par exemple, `truc.sty`) est distribué dans un *fichier source documenté*, `truc.dtx` (`dtx` veut dire **D***ocumented (La)***T***e***X**); vous devez donc chercher quelque chose qui s'appelle `truc`, car `truc.sty` ne sera visible nulle part.

La plupart des paquets sont distribués sous cette forme, avec des fichiers `dtx` et `ins`, mais ils ont leur propre répertoire (soit sur le CTAN, soit ailleurs). Même si ce répertoire contient d'autres fichiers, ou semble contenir plusieurs extensions, vous avez intérêt à télécharger tout ce qui s'y trouve : le plus souvent, les extensions regroupées de cette façon dépendent les unes des autres, de sorte que vous avez *vraiment* besoin de l'ensemble des fichiers pour que ça fonctionne.

Maintenant que vous avez récupéré les fichiers, il ne vous reste plus qu'à « {doc}`les installer </5_fichiers/installer_des_fichiers_pour_latex>` »!

______________________________________________________________________

*Sources :*

- {faquk}`Finding packages to install <FAQ-install-find>`,
- {faquk}`unpacking LaTeX packages <FAQ-install-unpack>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation,trouver un package,trouver un paquet,télécharger un package
```

