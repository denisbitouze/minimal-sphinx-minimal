Tests de migration de la [FAQ LaTeX francophone de l'association
GUTenberg](https://faq.gutenberg.eu.org/) de
[DokuWiki](https://www.dokuwiki.org/) vers
[Sphinx-doc](https://www.sphinx-doc.org/).
