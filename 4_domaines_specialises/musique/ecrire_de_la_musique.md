# Comment écrire de la musique sous LaTeX ?

## Avec l'éditeur « Lilypond »

`LilyPond` est un outil permettant, entre autres, de produire du code TeX/LaTeX. `LilyPond` permet d'écrire de la polyphonie, d'ajouter des paroles à la musique, de générer des fichiers midi et offre une large gamme de notations musicales : du grégorien (neumes et ligatures) au contemporain (clusters) en passant par la renaissance (notes carrées) et le baroque (basse chiffrée). Sa syntaxe d'entrée est moins cryptique que celle de {ctanpkg}`MusiXTeX <musixtex>`, à niveau de qualité équivalent.

Disponible sur [son site officiel](https://lilypond.org/), `Lilypond` accepte en particulier une [liste d'éditeurs](http://lilypond.org/easier-editing.fr.html) permettant de travailler plus facilement avec lui. En matière d'exemples, le site du [projet Mutopia](http://www.mutopiaproject.org/) propose plusieurs centaines de partitions mises en pages à l'aide de `LilyPond`.

### L'extension « lilyglyphs »

Pour les références musicales occasionnelles (signes dièses et bémols, notes, clefs, etc.), il existe une extension LaTeX appelé {ctanpkg}`lilyglyphs`. Elle utilise les polices `lilypond` (incluses dans l'extension) et fournit également les moyens d'ajouter des éléments provenant d'autres sources.

## Avec l'extension « MusiXTeX »

L'extension {ctanpkg}`MusiXTeX <musixtex>` de Daniel Taupin, Ross Mitchell et Andreas Egler permet d'écrire des partitions pour orchestres ou de la musique polyphonique. Bien sûr, {ctanpkg}`MusiXTeX <musixtex>` requiert des polices musicales et celles-ci sont disponibles dans une extension séparée. {ctanpkg}`MusiXTeX <musixtex>` fonctionne avec trois compilation :

- une basée sur TeX ;
- une passe de traitement effectuée par `musixflx` qui optimise l'espacement et les liaisons ;
- une autre passe TeX.

Une difficulté de {ctanpkg}`MusiXTeX <Musixtex>` réside dans sa syntaxe, quelque peu abrupte. Des solutions, présentées ci-après, permettent de simplifier cette syntaxe.

Voici quelques sources documentaires complémentaires :

- le [Cahier GUTenberg n°21](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1995___21) ;
- le [site web de Werner Icking](http://icking-music-archive.org/), mine d'informations, propose en particulier des partitions générées avec {ctanpkg}`MusiXTeX <musixtex>` ;
- la liste de discussion « [Tex-music](https://tug.org/mailman/listinfo/tex-music) » (en anglais) sur ce sujet.

### Le programme « PMX »

`PMX` est un programme d'aide à l'utilisation de MusiXTeX disponible à l'URL : <http://icking-music-archive.org/software/pmx/>.

### Le programme « M-Tx »

Le programme [M-Tx](https://www.icking-music-archive.org/software/mtx/mtx060.pdf) (Music-from-TeXt) est un autre pré-processeur pour {ctanpkg}`MusixTeX <musixtex>` qui facilite la saisie des chœurs. La sortie de `M-Tx` est transmise à `pmx` et, de là, à {ctanpkg}`MusixTeX <musixtex>`.

### Le programme « abc2mtex »

Une autre alternative pour écrire de la musique sous forme de texte est [notation ABC](https://abcnotation.com/), développée pour la musique traditionnelle d'Europe occidentale (qui peut être écrite sur une seule portée), bien qu'elle puisse être utilisée beaucoup plus largement. Un frontal à {ctanpkg}`MusiXTeX <Musixtex>`, `abc2mtex`, rend la composition ABC possible.

### Le programme « midi2tex »

Cas particulier, le programme `midi2tex` peut également générer une sortie {ctanpkg}`MusiXTeX <Musixtex>` à partir de fichiers MIDI.

## Avec le programme « gregoriotex »

Pour le chant grégorien, il existe {ctanpkg}`gregoriotex`, un logiciel d'écriture de partitions de chant grégorien.

