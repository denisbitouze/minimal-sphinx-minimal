# Comment convertir des fichiers « MIDI » en MusiXTeX ?

- Le programme [NoteEdit](https://www.berlios.de/software/noteedit/) permet d'effectuer une telle conversion.
- {ctanpkg}`midi2tex` permet également de traduire des fichiers de données [MIDI](https://fr.wikipedia.org/wiki/Musical_Instrument_Digital_Interface).
- Notons qu'il existe aussi des macros permettant de générer un fichier midi à partir d'une partition MusiXTeX (<http://icking-music-archive.org/software/musixtex/add-ons/mxtx2mml.zip>).

```{eval-rst}
.. meta::
   :keywords: LaTeX,musique,conversion de format de musique,fichiers MIDI,séquenceur
```

