# Existe-t-il une liste de discussion de musique ?

- La liste de discussion TeX-music (anciennement mutex) est consacrée à l'écriture de musique en TeX. On peut s'y inscrire à la page suivante : <http://icking-music-archive.org/mailman/listinfo/tex-music>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

