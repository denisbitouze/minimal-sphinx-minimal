# LaTeX peut-il faire le café ?

- LaTeX est sans doute capable de faire le café et même de le boire, puisqu'il sait faire des taches de café sur vos documents !

L'extension [coffee](http://legacy.hanno-rein.de/hanno-rein.de/archives/349), de [Hanno Rein](http://hanno-rein.de/), utilise de véritables taches de café (marques de tasses et éclaboussures), qui ont été photographiées puis retouchées sous Gimp et redessinées avec {ctanpkg}`PStricks`, pour orner vos pages.

La version actuelle a été réimplémentée en {ctanpkg}`TikZ <PGF>` par [Evan Sultanik](https://www.sultanik.com/), pour fonctionner avec pdfTeX et LuaTeX. La syntaxe de base est :

```latex
% !TEX noedit
\coffeestainA{⟨alpha⟩}{⟨scale⟩}{⟨angle⟩}{⟨xoff⟩}{⟨yoff⟩}
```

où

- ⟨alpha⟩ est la transparence, soit un nombre entre 0 et 1 (1 = la tache de café est complètement opaque; 0 = la tache est complètement transparente et invisible);
- ⟨scale⟩ est l'échelle (valeur standard : 1);
- ⟨angle⟩ est l'angle de rotation de la tache, en degrés d'angle;
- ⟨xoff⟩ et ⟨yoff⟩ sont les coordonnées horizontale et verticale par rapport au centre de la page, passées comme longueurs.

Quatre commandes sont disponibles, pour dessiner quatre formes de taches :

| Commande        | Apparence                                                  |
|-----------------|------------------------------------------------------------|
| `\coffeestainA` | Grand arc de cercle avec deux petites gouttes              |
| `\coffeestainB` | Petit arc de cercle                                        |
| `\coffeestainC` | Deux grosses éclaboussures de couleur claire               |
| `\coffeestainD` | Grosse tache colorée, avec des éclaboussures en périphérie |

```{eval-rst}
.. todo:: L'exemple n'est pas bien rendu ici, car il nécessite deux compilations pour que les nœuds Ti\ *k*\ Z soient positionnés de façon absolue sur la page.
```

```latex
\documentclass{article}
  \usepackage[width=9cm,height=9cm]{geometry}
  \usepackage{coffeestains}
  \usepackage{lipsum}
  \pagestyle{empty}

\begin{document}
\lipsum[1]

\coffeestainA{0.6}{0.8}{0}{0cm}{5cm}

%\coffeestainC{0.4}{0.6}{0}{-1cm}{-3cm}

\end{document}
```

:::{important}
L'extension [coffee](http://legacy.hanno-rein.de/hanno-rein.de/archives/349) n'est pas disponible sur CTAN, mais elle est très simple à installer :

1. Téléchargez le fichier `coffeestains.tar.gz` depuis <http://legacy.hanno-rein.de/hanno-rein.de/archives/349>,
2. Décompressez-le,
3. Copiez le fichier `coffeestains.sty` dans le répertoire de votre document.

Si vous souhaitez vous en servir régulièrement, copiez ce fichier dans le répertoire 

`texmf`

 de votre 

`HOME`

 ou de votre système.

Depuis novembre 2018, le développement se poursuit sur GitHub, sous le nom 

[latex-coffee-stains](https://github.com/barak/latex-coffee-stains)

, avec des contributions de Barak A. Pearlmutter et Sebastian Schmittner.
:::

## Comment avoir des taches sur chaque page ?

Avec l'extension {ctanpkg}`everypage`, il est possible d'[ajouter des taches aléatoires sur toutes les pages](https://tex.stackexchange.com/questions/237557/coffee-and-wine-and-nicotine-stains-on-an-entire-document) du document :

```latex
% !TEX noedit
\documentclass[a4paper]{article}
  \usepackage{coffeestains}
  \usepackage{everypage}
  \usepackage{lipsum}


\pgfmathsetseed{\pdfuniformdeviate 10000000}

\pgfmathdeclarerandomlist{scales}{{0.5}{0.7}{1.0}{1.4}}
\pgfmathdeclarerandomlist{stains}{{\coffeestainA}{\coffeestainB}{\coffeestainC}{\coffeestainD}}

\AddEverypageHook{%
  \pgfmathrandominteger{\angle}{15}{350}%
  \pgfmathparse{rand/2.4}\xdef\xoffset{\pgfmathresult}%
  \pgfmathparse{rand/2.4}\xdef\yoffset{\pgfmathresult}%
  \pgfmathparse{(0.1 + rnd/3)}\xdef\trans{\pgfmathresult}%
  \pgfmathrandomitem{\scale}{scales}%
  \pgfmathrandomitem{\stain}{stains}%
  \stain{\trans}\scale\angle{\xoffset\textwidth}{\yoffset\textheight}%
}

\begin{document}
\lipsum[1-120]
\end{document}
```

______________________________________________________________________

*Sources :*

- [Are there other « fun » packages like the « coffee stains » package?](https://tex.stackexchange.com/questions/67656/are-there-other-fun-packages-like-the-coffee-stains-package)
- [LaTeX Coffee Stains](http://legacy.hanno-rein.de/hanno-rein.de/archives/349),
- [latex-coffee-stains](https://github.com/barak/latex-coffee-stains) sur GitHub.

```{eval-rst}
.. meta::
   :keywords: LaTeX,taches de café,crasse,salir un document,maculer un document LaTeX,amusement,jeux,fun
```

