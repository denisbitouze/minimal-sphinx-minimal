# Comment constuire un arbre généalogique ?

Les arbres généalogiques sont bien utiles pour présenter les connexions génétiques et relationnelles entre les individus, dans un but clinique, sociologique ou légal. La dénomination « arbre » dérive des diagrammes de familles historiques. Cependant, même la plus petite entité familiale composée de deux parents et de quelques enfants n'est pas un arbre au sens mathématique, mais un graphe plus général.

- Le package {ctanpkg}`genealogytree` fournit un ensemble d'outils permettant de composer des arbres généalogiques (c'est-à-dire de dessiner un ensemble de graphes adaptés à la description de structures familiales). Il utilise un algorithme d'extraction automatique qui peut être personnalisé, par exemple, pour prioriser certaines arêtes.

```{eval-rst}
.. todo:: Ajouter un exemple.
```

## Diagrammes de parenté

Les diagrammes de parenté sont un outil courant en anthropologie pour représenter les systèmes de parenté. En effet selon les cultures et les langues, les relations de parenté ne sont pas conceptualisées et nommées de la même manière. Par exemple, dans les langues ryukyu, les frères et sœurs cadets son désignés par le même terme, sans distinction de sexe. Et il existe des systèmes de parenté très différents, dont certains peuvent nous sembler « bizarres ». À l’inverse, le fait qu’en français on ne distingue pas les grands-parents, oncles et tantes paternels de ceux du côté maternel paraitrait terriblement étrange à certaines cultures.

Pour réaliser un diagramme de parenté avec LaTeX, il ne semble pas exister de solution « clef en main ». Ce qui s’en rapprocherait le plus est {ctanpkg}`genealogytree`, mais celui-ci ne prend pas en compte les spécificités des diagrammes de parenté.

[Voir la solution de Thomas Pellard.](https://cipanglo.hypotheses.org/309)

______________________________________________________________________

*Source :*

- [Diagramme de parenté avec LaTeX](https://cipanglo.hypotheses.org/309).

```{eval-rst}
.. meta::
   :keywords: LaTeX,arbre généalogique,mes ancêtres en LaTeX,génétique humaine,diagramme de parenté
```

