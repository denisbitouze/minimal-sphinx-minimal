# Par où commencer une présentation avec « Beamer » ?

Vous devez préparer une présentation orale, avec des diapositives (*slides*) comme support ? On vous a {doc}`vanté les mérites </4_domaines_specialises/diaporama/classes_pour_des_presentations>` de {ctanpkg}`Beamer <beamer>`, mais vous ne savez pas trop par où commencer ? Voici quelques instructions simples.

## Comment mettre en forme ma première diapo ?

### La diapo de titre

```latex
\documentclass{beamer}
  \usepackage[french]{babel}

  \title{Faut-il se coucher de bonne heure?}
  \author{Marcel \textsc{Proust}}
  \institute{Institut de recherche du Temps Perdu --- Paris, France.}
  \date{14 novembre 1913}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\end{document}
```

### Les diapositives suivantes

```{eval-rst}
.. todo:: Ajouter un exemple.
```

## Comment structurer le contenu d'une diapo ?

```{eval-rst}
.. todo:: Ajouter un exemple avec des colonnes.
```

:::{tip}
LaTeX est un excellent outil pour écrire des articles ou des livres. Mais il faut reconnaître que ses atouts sont moins impressionnants lorsqu'il s'agit de préparer une présentation. En effet, sur chaque diapositive, les élements doivent être placés de façon plutôt visuelle, ce qui vous obligera souvent à compiler plusieurs fois votre présentation en ajoutant des `\vspace{}` par-ci par-là pour que l'ensemble soit harmonieux, voire à utiliser {ctanpkg}`TikZ <pgf>` pour placer librement les éléments sur la diapo.

Donc l'usage de LaTeX pour préparer une présentation orale est surtout recommandé :

- si vous utilisez couramment LaTeX pour d'autres travaux (dans ce cas, vos habitudes seront une force),
- si vous avez déjà du contenu formaté avec LaTeX, que vous récupèrerez pour vos diapos : formules, graphiques, illustrations...
:::

## Comment aller plus loin ?

Vous n'échapperez pas à la lecture [d'un tutoriel plus complet](https://pub.phyks.me/sdz/sdz/creez-vos-diaporamas-en-latex-avec-beamer.html).

Notez que la {texdoc}`documentation de Beamer <beamer>` (en anglais) est très bien écrite, et peut faire elle-même office de tutoriel.

Vous pouvez aussi consulter [une galerie d'exemples](http://deic.uab.es/~iblanes/beamer_gallery/) pour choisir un thème pour vos diapositives.

______________________________________________________________________

*Sources :*

- [Tutoriel : Créez vos diaporamas en LaTeX avec Beamer !](https://pub.phyks.me/sdz/sdz/creez-vos-diaporamas-en-latex-avec-beamer.html)
- [Beamer](https://fr.wikipedia.org/wiki/Beamer).

```{eval-rst}
.. meta::
   :keywords: LaTeX,PowerPoint,Impress,Prezi,keynotes,slides,diapositives,transparents,diaporamas,présentations,support de présentation
```

