# Comment présenter des séquences nucléiques ou protéiques ?

## Alignements de séquences

- Le package {ctanpkg}`TeXshade <texshade>`, d'Eric Beitz, permet de présenter joliment des alignments multiples de séquences nucléiques ou protéiques. Vous devez avoir réalisé l'alignement auparavant, et préparer un fichier ALN, MSA ou Fasta (aligné). Si les séquence sont longues, ce package peut les représenter de façon graphique, par des motifs colorés (*fingerprints*) au lieu d'afficher chaque résidu.

```{eval-rst}
.. todo:: Donner un exemple concret, y compris la procédure d'alignement.
```

:::{important}
Si votre alignement est gros, l'utilisation de TeXshade nécessitera que vous augmentiez les paramètres `main_memory` et `stack_size` dans le fichier `texmf.cnf`.

{texdoc}`Voir la FAQ spécifique de TeXshade. <tsfaq>`
:::

## Autres représentations de séquences biologiques

- Le package {ctanpkg}`pgfmolbio` propose de représenter :
- des chromatogrammes (type séquençage Sanger),
- des domaines le long d'une séquence :

```latex
\documentclass{article}
  \usepackage[domains]{pgfmolbio}
  \usetikzlibrary{decorations.pathreplacing}
  \pagestyle{empty}
\begin{document}
\large
\begin{pmbdomains}[name=Bidulase \TeX ique]{101}
  \addfeature[description={\TeX}]{domain}{10}{25}
  \addfeature[description={\LaTeX}]{domain}{40}{70}
  \addfeature[description=Région 1]{range}{15}{30}
  \addfeature[description=Région 2]{range}{25}{60}

  \addfeature[description=Et la fin,%
              style={very thick, draw=red},%
              range font=\footnotesize\textcolor{red}]{range}{68}{86}
\end{pmbdomains}
\end{document}
```

- Enfin, {ctanpkg}`TeXtopo <textopo>` permet de dessiner la structure secondaire d'une protéine, et [son repliement](https://fr.wikipedia.org/wiki/Repliement_des_protéines) en structures transmembranaires.

______________________________________________________________________

*Sources :*

- <https://pubmed.ncbi.nlm.nih.gov/10842735/>
- <https://pubmed.ncbi.nlm.nih.gov/11159320/>
- <https://tex.stackexchange.com/questions/400703/highlight-cpg-sites-with-texshade>

```{eval-rst}
.. meta::
   :keywords: LaTeX,protéines,ADN,acides nucléiques,alignement multiple,Clustal,biologie,génomique,électrophorégramme,électrophérogramme,structure des protéines
```

