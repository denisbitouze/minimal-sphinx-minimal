# Comment mettre en forme un courrier interne (newletter) ?

- Le package {ctanpkg}`newsletr` offre un ensemble de macros pour mettre en page des *newsletters*. Il est destiné à TeX (et pas LaTeX). Le résultat est très satisfaisant (multicolonnages, cadres simples, etc.).
- Des canevas au style moderne sont également proposés ici : <https://www.latextemplates.com/cat/newsletters>

```{eval-rst}
.. meta::
   :keywords: LaTeX,mailing,liste de diffusion,courrier,communication d'entreprise
```

