# Comment ajuster la taille des polices mathématiques ?

Dans `Plain` TeX, lorsque vous introduisez une nouvelle taille de police, vous devez également déclarer la taille des polices à utiliser en mathématiques. Cela se fait en déclarant `\textfont`, `\scriptfont` et `\scriptscriptfont` pour les familles mathématiques que vous utilisez ; toutes ces choses sont décrites dans le chapitre 17 du TeX​[book](/1_generalites/documentation/livres/documents_sur_tex), d'autres livres et des [documents en ligne](1_generalites/documentation/documents/documents_sur_tex) qui traitent de `Plain` TeX de manière suffisamment détaillée.

Dans LaTeX, bien sûr, tout cela est automatisé : il existe des règles qui, pour chaque taille de police (texte), déterminent quelles tailles de police mathématique doivent être utilisées. L'automatisation vérifie d'abord la présence d'un ensemble de tailles de texte connues, pour chacune desquelles des tailles mathématiques sont déclarées à l'avance. Si la taille du texte n'est pas connue, les tailles « script » et « scriptscript » sont calculées en tant que ratios fixes de la taille de la police TeX (les valeurs utilisées sont `\defaultscriptratio`=0,7 et `\defaultscriptscriptratio`=0,5.).

La formule à rapport fixe est capable de produire des résultats parfois peu esthétiques (en particulier si vous utilisez des polices qui, selon LaTeX, ne sont disponibles que dans un ensemble fixe de tailles). Vous pouvez également remplacer les idées de LaTeX, par exemple en définissant des polices mathématiques sensiblement plus grandes ou plus petites que le texte qui les entoure. À cet effet, la commande LaTeX suivante peut être utilisée :

```latex
% !TEX noedit
\DeclareMathSizes{⟨tfs⟩}{⟨ts⟩}{⟨ss⟩}{⟨sss⟩}
```

C'est la commande que LaTeX utilise pour définir son propre ensemble de tailles. Ceci établit (ou rétablit) les tailles de polices mathématiques à utiliser lorsque la taille de police du texte environnant est `⟨tfs⟩`, `⟨ts⟩` étant la taille utilisée pour `\textfont`, `⟨ss⟩` pour `\scriptfont` et `⟨sss⟩` pour `\scriptscriptfont`. Par exemple, vous souhaiterez peut-être utiliser une police avec une hauteur de corps plus petite que *Computer Modern*, tout en préférant les maths *Computer Modern* à l'une des alternatives. Dans ce cas, vous pouvez utiliser la commande suivante pour obtenir des mathématiques de 9 pt lorsque le corps du texte environnant a une taille de 10 pt :

```latex
% !TEX noedit
\DeclareMathSizes{10}{9}{7}{5}
```

La commande `\DeclareMathSizes` ne peut être utilisée que dans le préambule du document : une seule association est disponible pour chaque taille de police de texte pour l'ensemble du document. Les paramètres par défaut sont spécifiés dans le fichier `fontdef.dtx` dans la distribution LaTeX et sont compilés dans `fontmath.ltx`. Les arguments de la commande ne sont que des nombres (le `pt` est supposé), mais certains d'entre eux sont écrits en utilisant des [abréviations internes](/2_programmation/macros/abreviations_internes_de_latex) pour les tailles de police standard. Méfiez-vous donc lorsque vous copiez tout ou partie de ces définitions LaTeX : puisqu'elles contiennent ces abréviations internes, elles doivent être traitées comme des [commandes internes](2_programmation/macros/makeatletter_et_makeatother).

______________________________________________________________________

*Source :* {faquk}`Adjusting maths font sizes <FAQ-mathsize>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

