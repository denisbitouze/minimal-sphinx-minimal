# Comment changer de police en mode mathématique ?

## Avec les extensions amsmath et amssymb

Pour les opérations suivantes, il faut charger les extensions {ctanpkg}`amsmath` et {ctanpkg}`amssymb`.

### Les polices classiques

Il existe un certain nombre de commandes de changement de police pour LaTeX. Il s'agit de :

- `\mathrm` pour du roman ;
- `\mathbf` pour du gras (*bold font*) ;
- `\mathsf` pour du sans empattement (*sans serif*) ;
- `\mathtt` pour du typewriter (machine à écrire) ;
- `\mathit` pour de l'italique ;
- `\mathnormal` pour revenir à la fonte par défaut.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath,amssymb}
\begin{document}
\[ \mathrm{ABcd} \neq \mathbf{ABcd} \neq \mathsf{ABcd} \neq \mathtt{ABcd} \neq \mathit{ABcd} \]
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath,amssymb}
\pagestyle{empty}
\begin{document}
\[ \mathrm{ABcd} \neq \mathbf{ABcd} \neq \mathsf{ABcd} \neq \mathtt{ABcd} \neq \mathit{ABcd} \]
\end{document}
```

### Une police gothique

Cette police est accessible par la commande `\mathfrak{⟨texte⟩}`. Il existe également une commande `\frak{⟨texte⟩}` mais elle est considérée comme obsolète par l'extension (elle est conservée pour raison de compatibilité). Le gras pour cette police s'obtient avec la combinaison `\boldsymbol{\mathcal{⟨texte⟩}}`. En voici un exemple d'utilisation.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath,amssymb}
\begin{document}
\[ \mathfrak{ABcd} \neq ABcd \]
\[ \boldsymbol{\mathfrak{ABcd}} \neq ABcd \]
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath,amssymb}
\pagestyle{empty}
\begin{document}
\[ \mathfrak{ABcd} \neq ABcd \]
\[ \boldsymbol{\mathfrak{ABcd}} \neq ABcd \]
\end{document}
```

### Une police calligraphique

Cette police s'obtient avec la commande `\mathcal{⟨texte⟩}` avec une limitation importante : ne sont accessibles que les lettres majuscules. Le gras s'obtient avec la même technique que précédemment : `\boldsymbol{\mathcal{⟨texte⟩}}`. En voici un exemple d'utilisation.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath,amssymb}
\begin{document}
\[ \mathcal{ABCD} \neq ABCD \]
\[ \boldsymbol{\mathcal{ABCD}} \neq ABCD \]
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath,amssymb}
\pagestyle{empty}
\begin{document}
\[ \mathcal{ABCD} \neq ABCD \]
\[ \boldsymbol{\mathcal{ABCD}} \neq ABCD \]
\end{document}
```

On peut aussi générer tout l'alphabet (majuscule) correspondant et créer une commande `\mathbfcal` avec les définitions suivantes :

```latex
% !TEX noedit
\DeclareSymbolFont{boldsymbols}{OMS}{cmsy}{b}{n}
\DeclareSymbolFontAlphabet
        {\mathbfcal}{boldsymbols}
```

### Une police gras italique

Ce cas demande de déclarer les commandes allant chercher les bons caractères. En voici deux variantes :

```latex
% !TEX noedit
\DeclareMathAlphabet\mbi{OML}{cmm}{b}{it}
```

```latex
% !TEX noedit
\DeclareSymbolFont{mathbold}{OML}{cmm}{b}{it}
\DeclareMathSymbol
        {\biGamma}{\mathord}{mathbold}{0}
```

### Des symboles gras

Comme vu plus haut, ils s'obtiennent avec la commande `\boldsymbol{⟨symbole⟩}` mais peuvent être aussi obtenus par des définitions comme celle-ci pour le symbole «        $\alpha$ » :

```latex
% !TEX noedit
\DeclareSymbolFont{mathbold}{OML}{cmm}{b}{it}
\DeclareMathSymbol
        {\balpha}{\mathord}{mathbold}{11}
```

:::{note}
Si cela ne marche pas, c'est que les caractères gras correspondants n'existent pas. Il faut alors soit utiliser la commande `\pmb{...}` qui permet de « graisser » un symbole mathématique, soit utiliser une extension supplémentaire telle que {ctanpkg}`amsbsy`. Il existe par ailleurs des versions postscript de polices AMS disponibles sur le CTAN.
:::

## D'autres solutions pour le gras

### Avec la commande \\mathversion

Pour écrire toute une formule en gras, on peut aussi utiliser la commande `\mathversion{⟨argument⟩}` de LaTeX. Ici, `⟨argument⟩` peut valoir `bold` ou `normal` comme dans l'exemple suivant.

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
Une formule importante~ :
\mathversion{bold}
\[\sum_{i=0}^n u_i + v_i\]

Une formule moins importante~ :
\mathversion{normal}
\[\sum_{i=0}^n u_i + v_i\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath,amssymb}
\pagestyle{empty}
\begin{document}
Une formule importante~ :
\mathversion{bold}
\[\sum_{i=0}^n u_i + v_i\]

Une formule moins importante~ :
\mathversion{normal}
\[\sum_{i=0}^n u_i + v_i\]
\end{document}
```

### Avec l'extension bm

L'extension {ctanpkg}`bm` permet de bien gérer les symboles mathématiques en gras.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{bm}
\begin{document}
Une formule importante~ :
\[\bm{\sum_{i=0}^n u_i + v_i}\]

Une formule moins importante~ :
\[\sum_{i=0}^n u_i + v_i\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{bm}
\pagestyle{empty}
\begin{document}
Une formule importante~ :
\[\bm{\sum_{i=0}^n u_i + v_i}\]

Une formule moins importante~ :
\[\sum_{i=0}^n u_i + v_i\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

