# Comment écrire les symboles d'ensembles ?

Jadis, dans les livres, les symboles d'ensemble étaient imprimés en gras pour qu'on les voie bien :

> $\mathbf{R}$ est l'ensemble des réels,        $\mathbf{N}$ celui des entiers.

**Problème :** comment obtenir ce type de distinction lorsque l'on écrit sur un tableau noir ? En fait, au lieu d'écraser la craie avec force sur le tableau pour élargir le trait, on a pris l'habitude de doubler certaines parties des caractères :        $\mathbb{R}$,        $\mathbb{N}$. En retour, les livres se sont mis à copier l'usage des enseignants, et ont distingué le « gras standard » (*bold*), du « gras de tableau » (*blackboard bold*) qui est une police de caractères à part (dessin normal à barres redoublées).

## Avec les polices AMS

Les symboles mathématiques d'ensemble et bien d'autres sont disponibles par défaut dans les polices de l {doc}`AMS </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>` avec les familles `msam` (*e.g.* `msam10` pour 10pt) et `msbm` (disponibles en Type 1 dans les distributions actuelles). Pour y avoir accès, il faut utiliser les extensions {ctanpkg}`amsfonts` et {ctanpkg}`amssymb`.

Voici un exemple d'utilisation, avec un cas, pour l'ensemble des réels, de définition de commande pour simplifier ce type de saisie :

```latex
% !TEX noedit
\newcommand{\R}{\mathbb{R}}
$\mathbb{N}, \mathbb{Z}, \R, \mathbb{C}$
```

```latex
\newcommand{\R}{\mathbb{R}}
$\mathbb{N}, \mathbb{Z}, \R, \mathbb{C}$
```

La commande `\mathbb` ne fonctionne que pour les lettres majuscules. Par ailleurs, la forme de ces caractères est considérée comme plutôt austère (bien qu'elle ressemble à ce qu'on obtiendrait avec une craie) et rend ces lettres parfois peu appréciées.

## Avec l'extension « mathbbold » ou l'extension « bbold »

Les extensions {ctanpkg}`mathbbol` et {ctanpkg}`bbold` permettent d'obtenir des lettres minuscules, et même d'autres symboles. La police est cependant visuellement différente des polices mathématiques usuelles : elle ressemble à une *Futura* vraiment doublée par endroit, pas détourée. L'exemple ci-dessous illustre ce point et montre également qu'il faut utiliser la commande `\mathbb` (comme pour les fontes AMS). C'est d'ailleurs le cas pour {ctanpkg}`mathbbol` comme pour {ctanpkg}`mathbbol`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathbbol}
\begin{document}
\[\mathbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{mathbbol}
\pagestyle{empty}
\begin{document}
\[\mathbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\end{document}
```

Cette police, créée avec MetaFont, n'est pas disponible en format T1.

## Avec l'extension « bbm »

L'extension {ctanpkg}`bbm` propose certaines variantes pour les polices *Computer Modern*. Ces symboles correspondent davantage à ceux utilisés au tableau en France. Les caractères s'obtiennent avec la commande `\mathbbm` :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{bbm}
\begin{document}
\[\mathbbm{N, Z, R, C, a, b, c}\]
\end{document}
```

```{eval-rst}
.. todo:: \* : la restitution LaTeX ne se fait pas.\*
```

Cette police, créée avec MetaFont, n'est pas disponible en format T1.

## Avec l'extension « doublestroke »

L'extension {ctanpkg}`doublestroke` propose des symboles correspondant à ceux utilisés en France au format T1 et est donc bien adapté pour la création de fichier PDF de qualité. Toutes les majuscules sont présentes ainsi que le « 1 », le « h » et le « k ».

```latex
% !TEX noedit
\documentclass{article}
\usepackage{dsfont}
\begin{document}
\[\mathds{N, Z, R, C}\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{dsfont}
\pagestyle{empty}
\begin{document}
\[\mathds{N, Z, R, C}\]
\end{document}
```

Un document permet de comparer l'apparence des différentes polices citées ci-dessus : {ctanpkg}`blackboard`.

## Avec l'extension « boondox »

L'ensemble de polices {ctanpkg}`boondox` consiste en des formats T1 des [polices STIX mathématiques](https://en.wikipedia.org/wiki/STIX_Fonts_project). Cet ensemble contient une fonte *BOONDOXDoubleStruck-Regular* et sa version grasse (cette dernière s'obtenant avec `\mathbbb`).

```latex
% !TEX noedit
\documentclass{article}
\usepackage{BOONDOX-ds}
\begin{document}
\[\mathbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\[\mathbbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{BOONDOX-ds}
\pagestyle{empty}
\begin{document}
\[\mathbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\[\mathbbb{N, Z, R, C, a, b, c, 1, 2, 3}\]
\end{document}
```

## Avec des polices gratuites

Certaines des polices évoquées dans la question « {doc}`Quelles sont les fontes T1 disponibles pour les mathématiques ? </5_fichiers/fontes/fontes_t1_pour_les_mathematiques>` » répondent à la demande :

- les familles `txfonts` et `pxfonts` sont fournies avec des répliques de `msam` et `msbm` mais, comme indiqué par ailleurs, il y a plusieurs raisons pour ne pas utiliser ces fontes. Les versions révisées de ces fontes, `newtx` et `newpx` sont toutefois mieux ajustées ;
- la famille `mathpazo` propose des caractères « gras de tableau » ;
- les fontes `fourier` proposent des caractères « gras de tableau » pour les majuscules, le chiffre « 1 » et le « k ».

## Avec des polices commerciales

Les polices *Dextor outline* et *Mathematical Pi* (une sorte d'*Helvetica* doublée par endroit) donnent accès à des caractères « doublés ».

______________________________________________________________________

*Sources :*

- [Écrire les ensembles classiques en Latex : \\mathbb, amsfonts et \\mathbf](https://www.math-linux.com/latex-4/faq/latex-faq/article/ecrire-les-ensembles-classiques-en-latex-mathbb-amsfonts-et-mathbf)
- {faquk}`Symbols for the number sets <FAQ-numbersets>`
- [STIX font project](https://www.stixfonts.org/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,blackboard bold,gras de tableau,ensembles,symbole des réels,symbole des entiers,lettres avec barres doubles
```

