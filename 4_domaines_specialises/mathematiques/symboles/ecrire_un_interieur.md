# Comment obtenir la notation d'intérieur ?

Entre autres fonctionnalités, l'extension {ctanpkg}`yhmath` permet d'obtenir la notation d'un [intérieur](<https://fr.wikipedia.org/wiki/Int%C3%A9rieur_(topologie)>) suivant les habitudes mathématiques françaises. Elle définit pour cela les commandes `\ring` et `\windering`. En voici un exemple :

```latex
% !TEX noedit
\usepackage{yhmath}
...
\[
\ring{A}=\widering{\bigcup\limits_{x\in A} \{x\}}
\]
```

```latex
\documentclass[french]{article}
\usepackage{amsmath}
\usepackage{yhmath}
\pagestyle{empty}
\begin{document}
\[
\ring{A}=\widering{\bigcup\limits_{x\in A} \{x\}}
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématique,notation,intérieur
```

