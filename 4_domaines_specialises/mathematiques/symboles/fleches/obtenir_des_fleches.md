# Comment obtenir des flèches ?

Ces symboles sont listés dans le document {texdoc}`The comprehensive LaTeX symbol list <symbols-a4>`.

## Avec les commandes de base

De nombreux symboles de flèches sont disponibles par défaut. Pour vous aider à vous en rappeler, les 6 premières lignes du tableau suivant respectent une logique simple :

- la commande finit par `arrow` ;
- si la flèche va vers la gauche, la commande contient `left`. Si elle va vers la droite, la commande contient `right` ;
- si la flèche est longue, la commande commence par `long` ;
- si la flèche est double, la commande commence par une lettre majuscule.

| Symbole                 | Commandes            |     | Symbole                  | Commandes             |
| ----------------------- | -------------------- | --- | ------------------------ | --------------------- |
| \$\\gets\$              | `\gets`,`\leftarrow` |     | \$\\longleftarrow\$      | `\longleftarrow`      |
| \$\\Leftarrow\$         | `\Leftarrow`         |     | \$\\Longleftarrow\$      | `\Longleftarrow`      |
| \$\\to\$                | `\to`,`\rightarrow`  |     | \$\\longrightarrow\$     | `\longrightarrow`     |
| \$\\Rightarrow\$        | `\Rightarrow`        |     | \$\\Longrightarrow\$     | `\Longrightarrow`     |
| \$\\leftrightarrow\$    | `\leftrightarrow`    |     | \$\\longleftrightarrow\$ | `\longleftrightarrow` |
| \$\\Leftrightarrow\$    | `\Leftrightarrow`    |     | \$\\Longleftrightarrow\$ | `\Longleftrightarrow` |
| \$\\uparrow\$           | `\uparrow`           |     | \$ \\Uparrow\$           | `\Uparrow`            |
| \$\\downarrow\$         | `\downarrow`         |     | \$\\Downarrow\$          | `\Downarrow`          |
| \$\\updownarrow\$       | `\updownarrow`       |     | \$\\Updownarrow\$        | `\Updownarrow`        |
| \$\\nearrow\$           | `\nearrow`           |     | \$\\searrow\$            | `\searrow`            |
| \$\\swarrow\$           | `\swarrow`           |     | \$\\nwarrow\$            | `\nwarrow`            |
| \$\\mapsto\$            | `\mapsto`            |     | \$\\longmapsto\$         | `\longmapsto`         |
| \$\\hookleftarrow\$     | `\hookleftarrow`     |     | \$\\hookrightarrow\$     | `\hookrightarrow`     |
| \$\\leftharpoonup\$     | `\leftharpoonup`     |     | \$\\rightharpoonup\$     | `\rightharpoonup`     |
| \$\\leftharpoondown\$   | `\leftharpoondown`   |     | \$\\rightharpoondown\$   | `\rightharpoondown`   |
| \$\\rightleftharpoons\$ | `\rightleftharpoons` |     | \$\\leadsto\$            | `\leadsto`            |

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` propose des compléments aux commandes de base.

| Symbole                   | Commandes              |     | Symbole                 | Commandes             |
| ------------------------- | ---------------------- | --- | ----------------------- | --------------------- |
| \$\\leftleftarrows\$      | `\leftleftarrows`      |     | \$\\rightrightarrows\$  | `\rightrightarrows`   |
| \$\\leftrightarrows\$     | `\leftrightarrows`     |     | \$\\rightleftarrows\$   | `\rightleftarrows`    |
| \$\\upuparrows\$          | `\upuparrows`          |     | \$\\downdownarrows\$    | `\downdownarrows`     |
| \$\\Lleftarrow\$          | `\Lleftarrow`          |     | \$\\Rrightarrow\$       | `\Rrightarrow`        |
| \$\\dashleftarrow\$       | `\dashleftarrow`       |     | \$\\dashrightarrow\$    | ''\\dashrightarrow '' |
| \$\\leftarrowtail\$       | `\leftarrowtail`       |     | \$\\rightarrowtail\$    | `\rightarrowtail`     |
| \$\\twoheadleftarrow\$    | `\twoheadleftarrow`    |     | \$\\twoheadrightarrow\$ | `\twoheadrightarrow`  |
| \$\\Lsh\$                 | `\Lsh`                 |     | \$\\Rsh\$               | `\Rsh`                |
| \$\\leftrightsquigarrow\$ | `\leftrightsquigarrow` |     | \$\\rightsquigarrow\$   | `\rightsquigarrow`    |
| \$\\circlearrowleft\$     | `\circlearrowleft`     |     | \$\\circlearrowright\$  | `\circlearrowright`   |
| \$\\curvearrowleft\$      | `\curvearrowleft`      |     | \$\\curvearrowright\$   | `\curvearrowright`    |

```{eval-rst}
.. todo:: // À compléter avec ``\looparrowleft`` et ``\looparrowright`` qui ne s'affichent pas.//
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,symboles mathématiques,flèches
```

