# Comment obtenir les lettres grecques ?

En général, pour obtenir une lettre de l'[alphabet grec](https://fr.wikipedia.org/wiki/Alphabet_grec) dans des éléments mathématiques, il suffit de faire précéder son nom en toutes lettres d'une contre-oblique, ce qui donne par exemple la commande `\alpha`. Cependant, il faut garder en tête que ce sont des commandes mathématiques : au sein d'un paragraphe, il faut donc écrire `$\alpha$`.

La table ci-dessous liste les différentes commandes en supposant que vous êtes en mode mathématique. Il amène à trois remarques :

- notre alphabet classique restitue parfois directement la lettre grecque majuscule (par exemple pour la lettre « A »), ce qui explique l'absence de commande ;
- il existe des variantes de graphie pour une même lettre, comme `\epsilon` et `\varepsilon` ;
- les mathématiques sont écrites par défaut en italique tandis que les commandes de majuscules grecques sont par défaut en romain. Cela conduit à utiliser les commandes `\mathrm` et `\mathit` pour être cohérent.

| Nom     | Minuscule                       | Commande minuscule        | Majuscule romaine | Commande majuscule romaine | Majuscule italique      | Commande majuscule italique |
| ------- | ------------------------------- | ------------------------- | ----------------- | -------------------------- | ----------------------- | --------------------------- |
| alpha   | \$\\alpha\$                     | `\alpha`                  | \$\\mathrm\{A}\$  | `\mathrm{A}`               | \$A\$                   | `A`                         |
| bêta    | \$\\beta\$                      | `\beta`                   | \$\\mathrm\{B}\$  | `\mathrm{B}`               | \$B\$                   | `B`                         |
| gamma   | \$\\gamma\$                     | `\gamma`                  | \$\\Gamma\$       | `\Gamma`                   | \$\\mathit{\\Gamma}\$   | `\mathit{\Gamma}`           |
| delta   | \$\\delta\$                     | `\delta`                  | \$\\Delta\$       | `\Delta`                   | \$\\mathit{\\Delta}\$   | `\mathit{\Delta}`           |
| epsilon | \$\\epsilon\$, \$\\varepsilon\$ | `\epsilon`, `\varepsilon` | \$\\mathrm\{E}\$  | `\mathrm{E}`               | \$E\$                   | `E`                         |
| zêta    | \$\\zeta\$                      | `\zeta`                   | \$\\mathrm\{Z}\$  | `\mathrm{Z}`               | \$Z\$                   | `Z`                         |
| êta     | \$\\eta\$                       | `\eta`                    | \$\\mathrm\{H}\$  | `\mathrm{N}`               | \$H\$                   | `H`                         |
| thêta   | \$\\theta\$, \$\\vartheta\$     | `\theta`                  | \$\\Theta\$       | `\Theta`                   | \$\\mathit{\\Theta}\$   | `\mathit{\Theta}`           |
| iota    | \$\\iota\$                      | `\iota`                   | \$\\mathrm\{I}\$  | `\mathrm{I}`               | \$I\$                   | `I`                         |
| kappa   | \$\\kappa\$                     | `\kappa`                  | \$\\mathrm\{K}\$  | `\mathrm{K}`               | \$K\$                   | `K`                         |
| lambda  | \$\\lambda\$                    | `\lambda`                 | \$\\Lambda\$      | `\Lambda`                  | \$\\mathit{\\Lambda}\$  | `\mathit{\Lambda}`          |
| mu      | \$\\mu\$                        | `\mu`                     | \$\\mathrm\{M}\$  | `\mathrm{M}`               | \$M\$                   | `M`                         |
| nu      | \$\\nu\$                        | `\nu`                     | \$\\mathrm\{N}\$  | `\mathrm{N}`               | \$N\$                   | `N`                         |
| xi      | \$\\xi\$                        | `\xi`                     | \$\\Xi\$          | `\Xi`                      | \$\\mathit{\\Xi}\$      | `\mathit{\Xi}`              |
| pi      | \$\\pi\$, \$\\varpi\$           | `\pi`, `\varpi`           | \$\\Pi\$          | `\Pi`                      | \$\\mathit{\\Pi}\$      | `\mathit{\Pi}`              |
| rho     | \$\\rho\$, \$\\varrho\$         | `\rho`, `\varrho`         | \$\\mathrm\{P}\$  | `\mathrm{P}`               | \$P\$                   | `P`                         |
| sigma   | \$\\sigma\$, \$\\varsigma\$     | `\sigma`, `\varsigma`     | \$\\Sigma\$       | `\Sigma`                   | \$\\mathit{\\Sigma}\$   | `\mathit{\Sigma}`           |
| tau     | \$\\tau\$                       | `\tau`                    | \$\\mathrm\{T}\$  | `\mathrm{T}`               | \$T\$                   | `T`                         |
| upsilon | \$\\upsilon\$                   | `\upsilon`                | \$\\Upsilon\$     | `\Upsilon`                 | \$\\mathit{\\Upsilon}\$ | `\mathit{\Upsilon}`         |
| phi     | \$\\phi\$, \$\\varphi\$         | `\phi`                    | \$\\Phi\$         | `\Phi`                     | \$\\mathit{\\Phi}\$     | `\mathit{\Phi}`             |
| khi/chi | \$\\chi\$                       | `\chi`                    | \$\\mathrm\{X}\$  | `\mathrm{X}`               | \$X\$                   | `X`                         |
| psi     | \$\\psi\$                       | `\psi`                    | \$\\Psi\$         | `\Psi`                     | \$\\mathit{\\Psi}\$     | `\mathit{\Psi}`             |
| omega   | \$\\omega\$                     | `\omega`                  | \$\\Omega\$       | `\Omega`                   | \$\\mathit{\\Omega}\$   | `\mathit{\Omega}`           |

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,grecques
```

