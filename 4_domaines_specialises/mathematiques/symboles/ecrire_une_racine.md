# Comment écrire une racine (carrée ou autre) ?

La commande `\sqrt[⟨n⟩]{⟨arg⟩}` représente la racine \$n\\textsuperscript\{e}\$ de `⟨arg⟩`. `⟨n⟩` est un paramètre optionnel; s'il est absent, vous obtiendrez une racine carrée.

```latex
% !TEX noedit
\[ \sqrt[3]{\sqrt{\sqrt[\gamma]{x+y}}} \]
```

```latex
\[ \sqrt[3]{\sqrt{\sqrt[\gamma]{x+y}}} \]
```

Notez que la taille du symbole racine s'ajuste automatiquement au contenu.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équation,racine carrée,racine cubique,symbole racine
```

