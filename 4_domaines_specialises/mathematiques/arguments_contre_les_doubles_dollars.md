# Quels sont les arguments contre l'utilisation de « `$$...$$` » ?

TeX définit des commandes mathématiques en mode texte et en mode hors-texte, dupliquant apparemment les séquences mathématiques primitives TeX qui entourent les commandes mathématiques avec des « `$` » ou des « `$$` ».

De fait, les commandes « `\( ... \)` » ont presque le même effet que la version primitive TeX « `$ ... $` » : la version LaTeX vérifie juste que vous ne mettez pas « `\(` » et « `\)` » dans le mauvais sens. Ceci fait parfois penser qu'il y a, de la même manière, équivalence entre « `\[ ... \]` » et « `$$ ... $$` ». Et ce n'est pas le cas ! L'utilisation de « `$$...$$` » est déconseillée pour plusieurs raisons.

1. Elle ne respecte pas les mécanismes de LaTeX, comme par exemple l'option `fleqn` de la classe standard article : cette dernière doit avoir pour effet d'aligner à gauche (au lieu de centrer) les équations hors-texte, mais les équations délimitées par « `$$` » restent obstinément centrées.
2. L'espacement vertical autour de l'équation peut poser problème dans certains cas. Ainsi, il peut avoir des comportements étranges quand l'équation est précédée ou suivie de changements de paragraphes ou autres objets complexes.
3. Enfin, toutes les extensions faites pour LaTeX supposent que vous utilisez les constructions standard de LaTeX, et risquent donc de ne pas fonctionner avec « `$$` ». C'est le cas d' {ctanpkg}`amsmath`, et par exemple, de sa commande `\tag`.

Pour être complet, les environnements initialement prévus par LaTeX pour les mathématiques hors-texte sont `displaymath`, `equation*` et « `\[ ... \]` » : le dernier n'est guère plus long à taper que « `$$` » et rend par ailleurs le source plus lisible.

______________________________________________________________________

*Sources :*

- {faquk}`Why use \[ ... \] in place of $$...$$ <FAQ-dolldoll>`
- <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#dollars>

```{eval-rst}
.. meta::
   :keywords: LaTeX,composition des mathématiques,bonnes pratiques,double dollar
```

