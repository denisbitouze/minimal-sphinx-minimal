# Comment définir un nouvel opérateur ou symbole ?

- Il existe pour cela la commande `\DeclareMathOperator` du package {ctanpkg}`amsmath` {doc}`explications sur AMS-TeX </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>`). Elle s'utilise dans le préambule comme ici :

```latex
\documentclass{article}
  \usepackage{amsmath}
  \DeclareMathOperator{\init}{init}
  \pagestyle{empty}

\begin{document}
\[
\init f = 0 \mathrm{\ au \ lieu \ de :} init f = 0
\]
\end{document}
```

- On peut également utiliser `\newcommand`, plutôt dans le préambule du document, mais ce n'est pas obligatoire comme le montre cet exemple :

```latex
\newcommand{\affecte}{\mathrel{:=}}

$x \affecte 0$

$y \affecte 1$
```

- On peut utiliser `\def` comme ceci :

```latex
\makeatletter
\def\log{\mathop{\operator@font log}\nolimits}
\makeatother

$\log 28 = 1.447158$
```

- Pour définir un nouveau symbole de plusieurs caractères, il faut utiliser la commande `\mathit`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,symboles mathématiques,opérateurs mathématiques
```

