# Comment ajuster la taille de délimiteurs ?

## Avec une méthode automatique

Pour ajuster à leur contenu la taille de {doc}`délimiteurs </4_domaines_specialises/mathematiques/structures/delimiteurs/delimiteurs_mathematiques>` comme les parenthèses, les accolades, il suffit d'utiliser les commandes `\left` et `\right` suivies du symbole du délimiteur à redimensionner. En voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Comparez les parenthèses~ :
\[
(\frac{1}{2}) =
    \left( \frac{2}{4} \right)
\]
\end{document}
```

Les commandes `\left` et `\right` doivent *toujours être appelées ensemble* : oublier l'une ou l'autre génère une erreur. Toutefois, il existe des cas où vous pourrez souhaiter n'ajuster que la taille d'un unique délimiteur. Ces cas sont présentés à la question « {doc}`Comment gérer des délimiteurs non équilibrés ? </4_domaines_specialises/mathematiques/structures/delimiteurs/ne_pas_equilibrer_les_delimiteurs>` ».

À ces commandes s'ajoute (par le biais de 𝜀-TeX) la commande `middle` qui permet d'ajuster la taille d'un autre délimiteur compris entre les commandes `\left` et `\right`. En voici un exemple courant :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{amssymb} % pour \mathbb
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Soit l'ensemble $A_x$ défini par~ :
\[
A_x = \left\{ \frac{x^k}{k} \middle|
      k \in \mathbb{N}^* \right\}
\]
\end{document}
```

### Un peu plus de personnalisation

La taille des délimiteurs gérés avec les commandes `\left` et `\right` doit respecter deux règles fixant la taille minimale et utilisant les valeurs des paramètres `\delimitershortfall` et `\delimiterfactor`.

Ainsi, *la taille des délimiteurs est au moins celle de leur contenu diminuée par la valeur du paramètre `\delimitershortfall`*. Il est donc possible d'ajuster des délimiteurs d'une certaine longueur, par exemple pour obtenir des délimiteurs plus grands que leur contenu à l'aide d'une valeur négative à `\delimitershortfall`. La taille par défaut est `5pt`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{amssymb} % pour \mathbb
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Avant et après modification du
paramètre~ :
\[
A_x = \left\{ \frac{x^k}{k} \middle|
      k \in \mathbb{N}^* \right\}
\]
\[
\setlength{\delimitershortfall}{%
     -10pt}
A_x = \left\{ \frac{x^k}{k} \middle|
      k \in \mathbb{N}^* \right\}
\]
\end{document}
```

*La taille des délimiteurs rapportée à la taille du contenu doit valoir au moins la valeur du paramètre `\delimiterfactor` divisé par 1000*. Il est donc possible de faire un ajustement proportionnel des délimiteurs (avec une syntaxe ancienne car ce paramètre provient de TeX). La valeur par défaut est de `901` (un entier).

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{amssymb} % pour \mathbb
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Avant et après modification du
paramètre~ :
\[
A_x = \left\{ \frac{x^k}{k} \middle|
      k \in \mathbb{N}^* \right\}
\]
\[
\delimiterfactor=1300
A_x = \left\{ \frac{x^k}{k} \middle|
      k \in \mathbb{N}^* \right\}
\]
\end{document}
```

Ces mécanismes internes de TeX sont décrits plus précisément par le document {texdoc}`TeX by Topic <texbytopic>` dans sa section 21.2.

## Avec une méthode manuelle

Pour imposer son propre choix de tailles de délimiteurs notamment pour des délimiteurs imbriqués, sont disponibles plusieurs commandes :

- `\big` donnant un délimiteur de la hauteur d'une ligne ;
- `\Big` donnant un délimiteur de la hauteur d'une ligne et demi ;
- `\bigg` donnant un délimiteur de la hauteur de deux lignes ;
- et `\Bigg` donnant un délimiteur de la hauteur de deux lignes et demi.

Les exemples ci-dessus illustrent ces commandes. La dernière équation montre d'ailleurs ce qu'aurait donné `\left` et `\right` sur la même équation (pas d'ajustement) et sur le fait que ces commandes manuelles peuvent améliorer la présentation de vos équations.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici les différentes tailles
possibles~ :
\[
\Bigg\{ \bigg\{ \Big\{ \big\{ \{x\}
    \big\} \Big\} \bigg\} \Bigg\}
\]
Voici un exemple d'utilisation~ :
\[
f(x) = \Big( (x+1) (x-1) \Big)^{2}
\]
Et ce même exemple avec les commandes
vues plus haut~ :
\[
f(x) = \left( (x+1) (x-1) \right)^{2}
\]
\end{document}
```

Contrairement aux commandes `\left` et `\right`, les commandes de la famille de `\bigg` n'ont pas de contrainte d'utilisation. Dès lors, LaTeX ne s'occupant pas du sens mathématique de vos équations, vous pouvez par cette méthode obtenir des petits monstres typographiques : par exemple des parenthèses englobantes plus petites que leur contenu, des parenthèses beaucoup trop grandes ou des parenthèses non équilibrées.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
L'abus de parenthèses manuelles est
dangereux pour la santé mathématique
de vos documents~ :
\[
f(x) = ( \bigg(x+1) \Big(x-1)
       \Bigg)^{2}
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,délimiteurs,ajustement,ajustement de taille,paramètres
```

