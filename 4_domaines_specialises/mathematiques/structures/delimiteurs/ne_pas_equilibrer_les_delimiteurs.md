# Comment gérer des délimiteurs non équilibrés ?

LaTeX propose les commandes bien pratiques `\left` et `\right` pour ajuster automatiquement la taille des {doc}`délimiteurs </4_domaines_specialises/mathematiques/structures/delimiteurs/delimiteurs_mathematiques>` (comme les parenthèses) en fonction du contenu, comme le montre la question « {doc}`Comment ajuster la taille de délimiteurs ? </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>` ». La seule contrainte est que les délimiteurs doivent être équilibrés par paire. Voici deux méthodes autour de cette contrainte.

## Cas du délimiteur isolé

Vous pouvez parfois avoir besoin d'un délimiteur unique telle l'accolade ouvrante dans un système d'équation. Ici, LaTeX propose à cet effet le délimiteur invisible « `.` » pour compléter la paire de délimiteurs. Par exemple, le cas suivant met en face de la commande `\left\{` la commande `\right.` :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\[ (S) \left\{ % ici on ouvre...
\begin{aligned}
  ax + by + cz &= 0 \\
  ey + fz &= 0 \\
  gz &= 0
\end{aligned}
\right. % ...et là on ferme !
\]
\end{document}
```

Pour présenter un système d'équation, vous pouvez aussi consulter la question « {doc}`Comment mettre en page un système d'équations ? </4_domaines_specialises/mathematiques/equations/systemes_d_equations>` ».

## Cas de délimiteurs trop éloignés

Il est parfois impossible d'équilibrer les délimiteurs, par exemple parce qu'ils sont rejettés sur deux lignes différentes faute de place. Il faut alors ruser en employant conjointement des délimiteurs invisibles, et si besoin des {doc}`fantômes </3_composition/texte/exploiter_les_fantomes>` avec la commande `\vphantom` pour ajuster la taille des délimiteurs à un contenu agrandi par le fantôme. Par exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\begin{align*}
a &= \left( \frac{1}{2} + b + c
     + \ldots \right. \\
  & \qquad \left.
     \vphantom{\frac{1}{2}}
     + \ldots + x + y + z \right)^2
\end{align*}
\end{document}
```

______________________________________________________________________

*Source* : <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#lrdelim>

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,délimiteurs,ajustement,fantômes,système
```

