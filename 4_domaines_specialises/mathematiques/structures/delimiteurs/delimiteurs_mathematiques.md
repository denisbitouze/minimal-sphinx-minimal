# Quels sont les délimiteurs disponibles ?

De nombreux symboles peuvent servir de délimiteurs mathématiques. L'avantage de ces symboles est qu'ils sont normalement redimensionnables comme le montre la question « {doc}`Comment ajuster la taille de délimiteurs ? </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>` ».

## Avec les commandes de base

LaTeX dispose d'un certain nombre de délimiteurs :

| Nom                       | Exemple                                    | Commande LaTeX        |
| ------------------------- | ------------------------------------------ | ---------------------------------- |
| Parenthèses               | $( x )$                                    | `( )`                              |
| Crochets                  | $\lbrack x \rbrack$                        | `[ ]` ou `\lbrack \rbrack`         |
| Accolades                 | $\lbrace x \rbrace$                        | `\{ \}` ou `\lbrace \rbrace`       |
| Chevrons mathématiques    | $\langle x \rangle$                        | `\langle \rangle`                  |
| Planchers                 | $\lfloor x \rfloor$                        | `\lfloor \rfloor`                  |
| Plafonds                  | $\lceil x \rceil$                          | `\lceil \rceil`                    |
| Barres verticales         | $\vert x \vert$                            | `\|` ou `\vert`                    |
| Barres verticales doubles | $\Vert x \Vert$                            | `\\|` ou `\Vert`                   |
| Flèches                   | $w \uparrow x \downarrow y \updownarrow z$ | `\uparrow \downarrow \updownarrow` |
| Flèches doubles           | $w \Uparrow x \Downarrow y \Updownarrow z$ | `\Uparrow \Downarrow \Updownarrow` |
| Obliques                  | $/ x /$                                    | `/`                                |
| Contre-obliques           | $\backslash x \backslash$                  | `\backslash`                       |

## Avec l'extension « stmaryrd »

L'extension {ctanpkg}`stmarydr` permet d'obtenir en particulier d'obtenir les crochets mathématiques blancs (parfois nommés doubles crochets) :

| Nom                           | Symbole | Commande LaTeX |
| ----------------------------- | ------- | --------------------------- |
| Crochets mathématiques blancs | ⟦ ⟧     | `\llbracket \rrbracket`     |

## Avec d'autres extensions

D'autres délimiteurs sont présentés dans {texdoc}`The comprehensive LaTeX symbol list <symbols-a4>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

