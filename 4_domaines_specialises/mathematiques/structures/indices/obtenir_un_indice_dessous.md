# Comment écrire au-dessous d'un symbole ou d'une flèche ?

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` dispose de la commande `\underset` permettant de placer des « indices centrés ».

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}
  \usepackage{amsmath}
  \pagestyle{empty}

\begin{document}
\[
\underset{a}{X} =
    \sum_{k=a}^{\infty} X^k
\]
\end{document}
```

Ce principe se généralise avec la commande `\overunderset` qui permet de placer un exposant au-dessus du symbole et un indice au-dessous le symbole.

L'extension fournit également avec des commandes dédiées un moyen de placer des éléments au-dessous d'une flèche extensible. Ce sujet est évoqué à la question « {doc}`Comment ajuster la longueur d'une flèche par rapport à celle d'un texte ? </4_domaines_specialises/mathematiques/symboles/fleches/ajuster_la_longueur_d_une_fleche_par_rapport_a_un_texte>` ».

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#indices>

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,écrire sous une flèche,indice sous une flèche,indice sous une relation,écrire sous un symbole
```

