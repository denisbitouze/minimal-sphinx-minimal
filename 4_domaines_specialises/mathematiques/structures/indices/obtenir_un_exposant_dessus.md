# Comment écrire au-dessus d'un symbole ou d'une flèche ?

## Avec les commandes de base

La commande `\stackrel` permet de placer des éléments au-dessus d'une flèche ou d'une relation :

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}

\pagestyle{empty}
\begin{document}
\[
x \stackrel{f}{\mapsto} f(x)
\]
\end{document}
```

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` dispose de la commande `\overset` permettant de placer des « exposants centrés ».

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}
  \usepackage{amsmath}
  \pagestyle{empty}

\begin{document}
\[
\overset{a}{X} =
    \sum_{k=0}^{a} X^k
\]
\end{document}
```

Cette commande suit la logique de la commande `\underset` décrite à la question « {doc}`Comment obtenir des indices au-dessous des symboles ? </4_domaines_specialises/mathematiques/structures/indices/obtenir_un_indice_dessous>` ». Son principe se généralise avec la commande `\overunderset` qui permet de placer un exposant au-dessus du symbole et un indice au-dessous le symbole.

L'extension fournit également avec des commandes dédiées un moyen de placer des éléments au-dessus d'une flèche extensible. Ce sujet est évoqué à la question « {doc}`Comment ajuster la longueur d'une flèche par rapport à celle d'un texte ? </4_domaines_specialises/mathematiques/symboles/fleches/ajuster_la_longueur_d_une_fleche_par_rapport_a_un_texte>` ».

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#indices>

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,exposant au-dessus des symboles,texte sur un symbole,écrire sur les flèches,texte au-dessus d'une flèche
```

