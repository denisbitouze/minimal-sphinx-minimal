# Comment empiler des indices sous les opérateurs ?

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` dispose d'une commande `\substack` qui permet de placer plusieurs lignes d {doc}`indices </4_domaines_specialises/mathematiques/structures/indices/start>` sous un opérateur en les séparant par la commande `\\`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\[
S_I = \sum_{%
    \substack{(i,j) \in I^2 \\
    i \neq j}}
    x_i + x_j
\]
\end{document}
```

Si le centrage des indices ne vous satisfait pas, l'extension propose une autre solution avec l'environnement `subarray` où vous pouvez déterminer l'alignement souhaité (avec les choix classiques propres aux {doc}`environnements de tableau </3_composition/tableaux/construire_un_tableau>`) par exemple un alignement à gauche avec le « `l` » dans l'exemple ci-dessous :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\[
S_I = \sum_{%
    \begin{subarray}{l}
      (i,j) \in I^2 \\
      i \neq j}}
    \end{subarray}
    }
  x_i + x_j
\]
\end{document}
```

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#indices>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

