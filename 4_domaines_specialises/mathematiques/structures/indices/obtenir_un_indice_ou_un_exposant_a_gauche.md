# Comment obtenir des indices ou exposants à gauche ?

Il est parfois nécessaire de placer des {doc}`indices ou exposants </4_domaines_specialises/mathematiques/structures/indices/start>` devant un symbole, par exemple pour la transposée d'une matrice. Voici quelques méthodes sur ce sujet.

## Avec des commandes de base

La solution de base consiste à placer ceux-ci sur un groupe vide, afin d'éviter qu'ils n'aillent se coller au symbole précédent. Dans certains cas, il sera intéressant d'ajuster l'espace manuellement, par exemple avec la commande `\!`. Voici un exemple de cette méthode :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\LARGE
$A {}^t M \neq A${}^t\!M$
\end{document}
```

L'inconvénient est que cette méthode n'est pas générale car elle ne positionne pas bien l'élement mis en indice ou en exposant en ne considèrant pas la taille de l'élément qui sert de référence à l'indice ou l'exposant. En voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\[
{}^t
\begin{bmatrix}
   a & b \\  c & d \\
\end{bmatrix}
=
\begin{bmatrix}
   a & c \\  b & d \\
\end{bmatrix}
\]
\end{document}
```

Pour contourner cette difficulté, il est possible de définir une commande `\transposee` utilisant la commande `\vphantom`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\newcommand{\transposee}[1]%
{{\vphantom{#1}}^{\mathit t}{#1}}
\begin{document}
\[
\transposee{
\begin{bmatrix}
   a & b \\ c & d \\
\end{bmatrix}
}
=
\begin{bmatrix}
   a & c \\  b & d \\
\end{bmatrix}
\]
\end{document}
```

## Avec l'extension « mathtools » (ou « amsmath »)

Pour des symboles qui ne sont pas des grands opérateurs, l'extension {ctanpkg}`mathtools` met à disposition la commande `\prescript` qui fournit un alignement sur la droite, plus satisfaisant que celui de la solution du groupe vide.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\LARGE
${}^{12}_3 C \neq \prescript{12}{3}{C}$
\end{document}
```

Dans le cas des grands opérateurs (le symbole de sommation ou de produit par exemple), {ctanpkg}`mathtools` propose la commande `\sideset` dont voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
$\sideset{_a^b}{_c^d}\prod$
\end{document}
```

La {texdoc}`documentation <amsldoc>` de l'extension {ctanpkg}`amsmath` (appelée par {ctanpkg}`mathtools`) donne d'ailleurs un exemple compliqué à traiter si `\sideset` n'est pas utilisé (mettre un prime sur le symbole de sommation alors que ce dernier a des éléments sous lui) :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\[
\sideset{}{'}\sum_{n<k,\;%
   \text{$n$ impair}} nE_n
\]
\end{document}
```

Cette commande peut être utilisée dans la définition de la commande `\transposee` vue plus haut. Il faut cependant rester ici prudent, les auteurs de l'extension indiquant que la commande `\sideset` devrait être limitée aux seuls grands opérateurs.

```latex
% !TEX noedit
\newcommand{\transposee}[1]%
      {\sideset{^{\mathit{t}}}{}{#1}}
```

## Avec l'extension « chemsym »

Dans un domaine un peu différent, celui de la {doc}`chimie </4_domaines_specialises/chimie/start>`, l'extension {ctanpkg}`chemsym` permet de définir et d'utiliser des symboles chimiques plus facilement. Elle permet en particulier de définir des commandes simplifiant la saisie de quelques symboles qui ont des exposants à gauche (avec des méthodes similaires à celles présentées en début de cette page). L'exemple ci-dessous est repris de la {texdoc}`documentation de l'extension <chemsym>`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
%\usepackage{chemsym}
%\newcommand{\hH}{\kemtkn{{}^2H}}
\pagestyle{empty}
\begin{document}
Voici une des notations du
deutérium~ : ${}^2\textrm{H}$.
\end{document}
```

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#indices>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

