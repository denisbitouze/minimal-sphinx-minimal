# Comment bien aligner les indices et exposants ?

Toutes choses étant égales par ailleurs, TeX s'efforcera de positionner les {doc}`indices et les exposants </4_domaines_specialises/mathematiques/structures/indices/start>` à des endroits corrects. Malheureusement, il le fait séparément pour les indices et les exposants de chaque terme de l'équation. Voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\LARGE
$ X^{1}_{2} X_{2} $
\end{document}
```

Ici, le second indice semble trop haut, puisque le premier a été légèrement déplacé vers le bas pour éviter de toucher l'exposant.

## Avec des commandes de base

Vous pouvez éviter ce problème, au sein d'une équation, en utilisant la méthode suivante :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\LARGE
$ X^{1}_{2} X^{}_{2} $
\end{document}
```

Ici, l'exposant vide a pour effet nécessaire de « pousser l'indice vers le bas », ce qui restaure l'alignement visuel des deux indices.

De façon plus poussée, la commande `\vphantom` définit une boîte invisible dont la hauteur est celle de son argument. Elle peut être utilisée pour aligner des indices, comme dans l'exemple qui suit.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\[
f_d = f_{\widehat{ef}}
\mathrm{\ devient\ }
f_{\vphantom{\widehat{ef}} d}
    = f_{\widehat{ef}}.
\]
\end{document}
```

## Avec l'extension « subdepth »

Si la technique ci-dessus fonctionne, elle est fastidieuse et potentiellement source d'erreurs. Ainsi, pour plus d'une ou deux équations dans un document, il est conseillé d'utiliser l'extension {ctanpkg}`subdepth`, qui force tous les indices à apparaître à leur position basse, indépendamment de la présence d'un exposant.

### Cas de lualatex

Il faut noter que cette extension ne fonctionne pas avec `lualatex`. Dans ce cas, il faudra utiliser la solution suivante (clairement pas idéale car nécessitant des tests d'ajustement) :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Avant modification, $X_2 X_2^\dagger
X_2^+$ s'affiche ainsi hors ligne~ :
\[X_2 X_2^\dagger X_2^+\]

\Umathsubshiftdown\textstyle=3pt
\Umathsubshiftdown\displaystyle=2.5pt

Après modification, $X_2 X_2^\dagger
X_2^+$ s'affiche ainsi hors ligne~ :
\[X_2 X_2^\dagger X_2^+\]
\end{document}
```

______________________________________________________________________

*Sources :*

- {faquk}`Even subscript height <FAQ-subheight>`
- [Re : \[luatex\] sub and superscripts in Lua(La)TeX with tfm/otf](https://www.mail-archive.com/luatex@tug.org/msg05849.html)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,positionnement des indices,positionnement des exposants,indices décalés,alignement des indices
```

