# Comment obtenir des indices ou exposants ?

En mathématiques, les {doc}`indices et exposants </4_domaines_specialises/mathematiques/structures/indices/start>` se placent traditionnellement à droite du symbole qui reçoit l'indice ou l'exposant. C'est ce sujet que traite cette page. Si vous cherchez à placer  :

- des indices ou exposants à gauche, consultez la question « {doc}`Comment obtenir des indices ou exposants à gauche ? </4_domaines_specialises/mathematiques/structures/indices/obtenir_un_indice_ou_un_exposant_a_gauche>` » ;
- des indices au-dessous, consultez la question « {doc}`Comment obtenir des indices au-dessous des symboles ? </4_domaines_specialises/mathematiques/structures/indices/obtenir_un_indice_dessous>` » et la question « {doc}`Comment empiler des indices sous les opérateurs ? </4_domaines_specialises/mathematiques/structures/indices/empiler_des_indices>` » ;
- des exposants au-dessus, consultez la question « {doc}`Comment obtenir des exposants au-dessus des symboles ? </4_domaines_specialises/mathematiques/structures/indices/obtenir_un_exposant_dessus>` ».

## Le principe

En {doc}`mode </4_domaines_specialises/mathematiques/passer_en_mode_mathematique>` mathématique, deux symboles servent à traiter ce sujet  :

- `_` indique la mise en indice du groupe de caractère qui suit, encadré par des accolades ;
- `^` indique la mise en exposant du groupe de caractère qui suit, encadré par une accolade.

En l'absence d'accolades, la mise en indice ou en exposant porte uniquement sur le premier caractère qui suit `_` ou `^`.

Voici un exemple de leur utilisation :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Une suite géométrique $(u_n)$ est
définie par la valeur de son premier
terme~ :
\[
u_0 = a
\]
et par la relation de récurrence
suivante faisant intervenir un
nombre $q$ pour tout $n>0$~ :
\[
u_n = q u_{n-1}.
\]
Il peut être démontré que ce
terme peut également s'écrire~ :
\[
u_n = a q^n.
\]
\end{document}
```

Comme le montre cet exemple, les accolades étaient nécessaires pour mettre en indice le bloc « n-1 ». Sans cela, LaTeX aurait composé le seul « n » en indice et aurait mis le « -1 » au même niveau que le « u », changeant ainsi le sens de l'équation. L'exemple suivant illustre ce point :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Soit une suite $(v_n)$, avec
$v_0=1$, telle que~
\[
v_n-1 = v_{n-1}.
\]
\end{document}
```

Bien entendu, au sein d'un élément mis en indice ou en exposant peuvent être mis des éléments eux-mêmes ayant des indices ou exposants :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Une suite géométrique $(u_n)$ peut
être définie à partir d'un rang
$n_0$ tel que :
\[
u_n = u_{n_0} q^{n-n_0}.
\]
\end{document}
```

Enfin, la taille des éléments en indice ou exposant peut être modifiée (à vos risques et périls), comme présenté en question « {doc}`Comment modifier la taille des indices et exposants ? </4_domaines_specialises/mathematiques/structures/indices/reduire_la_taille_des_indices_en_mode_mathematique>` ».

## Indices et exposants sur un même élément

La mise en indice et en exposant sur un même élément se fait en plaçant à la suite la mise en indice et la mise en exposant. L'ordre importe peu car il est possible de faire la mise en exposant suivie de la mise en indice, comme le montre cet exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Notez bien que nous aurons
$x_2^3 \neq x^2_3$.
\end{document}
```

Ceci peut toutefois provoquer des alignements d'indices parfois peu esthétiques, comme le montre la question « {doc}`Comment bien aligner les indices et exposants ? </4_domaines_specialises/mathematiques/structures/indices/harmoniser_la_hauteur_des_indices>` ».

## Indices et exposants sur des grands opérateurs

Certains commandes de LaTeX conduisent à disposer les indices et exposants de façon particulière : au lieu d'être mis à droite après le symbole souhaité, les indices et exposants se placents au-dessous et au-dessus du symbole si l'équation est en mode hors ligne. Voici un exemple classique illustrant cette différence de mise en forme :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
On définit ici la notation
$\sum_{k=1}^n x_k$ ainsi~ :
\[
\sum_{k=1}^n x_k
    = x_1 + x_2 + \dots + x_n.
\]\end{document}
```

Cette disposition est détaillée aux questions « {doc}`Quels sont les huit styles mathématiques ? </4_domaines_specialises/mathematiques/les_huit_styles_mathematiques>` » et « [À quoi sert la commande « \\displaystyle » ?](/4_domaines_specialises;mathematiques/a_quoi_sert_displaystyle) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,structures mathématiques,indices,exposants
```

