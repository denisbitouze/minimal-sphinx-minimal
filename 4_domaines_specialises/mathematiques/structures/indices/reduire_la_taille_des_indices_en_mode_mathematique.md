# Comment modifier la taille des indices et exposants ?

Il peut arriver que vous souhaitiez modifier la taille des {doc}`indices et exposants </4_domaines_specialises/mathematiques/structures/indices/start>` dans un document. Voici plusieurs méthodes pour y arriver, classée de la plus recommandée à la moins recommandée.

## Avec les commandes de style mathématiques

Les commandes `\scriptstyle` ou `\scriptscriptstyle` permettent de gérer ces tailles. La première donne aux éléments mathématiques la taille d'un indice ou exposant de premier ordre, la seconde la taille d'un indice ou exposant de second ordre (autrement dit, par exemple, la taille de l'indice d'un indice).

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Au premier ordre~ :
\[
X_{AB} = X_{\scriptstyle AB}
    \neq X_{\scriptscriptstyle AB}.
\]

Au second ordre~ :
\[
X_{AB_{CD}}
    = X_{AB_{\scriptscriptstyle CD}}
    \neq X_{AB_{\scriptstyle CD}.
\]
\end{document}
```

## Avec des commandes de taille de texte classique

La question « {doc}`Comment placer du texte dans des mathématiques ? </4_domaines_specialises/mathematiques/composer_du_texte_en_mode_mathematique>` » permet d'imaginer une autre solution : changer les indices et exposants en texte (ci-dessous avec la commande `\textit`) pour alors appliquer toute la palette des commandes vues à la question « {doc}`Comment changer la taille d'une police ? </3_composition/texte/symboles/polices/changer_la_taille_d_une_fonte>` ». Cependant, il s'agit là d'une logique assez contradictoire avec celle d'utiliser un environnement mathématique et il peut devenir rapidement fastidieux de garder une uniformité sur un document assez long. Cette utilisation devrait donc rester exceptionnelle.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Un exemple peu subtil~ :
\[
X_{AB_{CD}} \neq
    X_{\textit{\footnotesize
    AB}}_{\textit{\normalsize
    CD}}
\]
\end{document}
```

## La commande \\DeclareMathSizes

La commande `\DeclareMathSizes` permet d'effectuer des réglages généraux sur la taille des polices. Elle est présentée à la question « {doc}`Comment ajuster la taille des polices mathématiques ? </4_domaines_specialises/mathematiques/symboles/polices/tailles_de_fonte_en_mode_mathematique>` ». Elle n'est cependant pas recommandée car LaTeX fournit normalement des dimensions bien adaptées. [David Carlisle](https://davidcarlisle.github.io/dcarlisle/david/) détaille d'ailleurs ce cas sur le site de [LaTeX Stack Exchange](https://tex.stackexchange.com/questions/99981/when-should-we-use-declaremathsizes-xxxstyle-and-font-size-changing-macros) et précise que cette commande ne devrait servir que lors de la définition de {doc}`nouvelles classes </2_programmation/extensions/creer_sa_propre_classe>`).

Il ne faut pas ici oublier que le premier paramètre de cette commande est la taille du texte environnant. Notre document ci-dessous ayant une fonte de `10pt` par défaut, il faut bien entamer la commande `\DeclareMathSizes` avec un argument valant `10`. Les arguments suivants donnent la taille de `\textfont` (10pt), de `\scriptfont` (9pt) et de `\scriptscriptfont` (8pt). Ces deux derniers réglages vont donner des indices assez grands et pas forcément très agréables visuellement.

```latex
\documentclass[10pt]{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\DeclareMathSizes{10}{10}{9}{8}
\begin{document}
Voici un exemple de taille augmentée
à comparer avec le résultat des
exemples précédents~ :
\[
X_{AB_{CD}}
\]
\end{document}
```

______________________________________________________________________

*Source :* <https://tex.stackexchange.com/questions/99981/when-should-we-use-declaremathsizes-xxxstyle-and-font-size-changing-macros>.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

