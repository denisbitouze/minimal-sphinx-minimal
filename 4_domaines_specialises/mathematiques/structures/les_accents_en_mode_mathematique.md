# Quels sont les accents mathématique standards ?

- Il en existe dix pour des lettres uniques :

```latex
% !TEX noedit
  $\hat{a}$
+ $\check{a}$
+ $\breve{a}$
+ $\acute{a}$
+ $\grave{a}$
+ $\tilde{a}$
+ $\bar{a}$
+ $\vec{a}$
+ $\dot{a}$
+ $\ddot{a}$
```

```latex
$\hat{a}$
+ $\check{a}$
+ $\breve{a}$
+ $\acute{a}$
+ $\grave{a}$
+ $\tilde{a}$
+ $\bar{a}$
+ $\vec{a}$
+ $\dot{a}$
+ $\ddot{a}$
```

:::{note}
Pour placer un accent sur un **i** ou un **j**, il faut utiliser les commandes `\imath` et `\jmath` qui permettent de supprimer le point par défaut :

```latex
i $\rightarrow$ \imath
```

```latex
j $\rightarrow$ \jmath
```
:::

- Il existe également des accents extensibles, qui peuvent couvrir des ensembles de lettres :

```latex
% !TEX noedit
  $\widehat{abc}$
+ $\widetilde{def}$
+ $\overrightarrow{ghi}$
+ $\overline{jkl}$
+ $\underline{mno}$
+ $\overbrace{pqr}$
+ $\underbrace{stu}$
```

```latex
$\widehat{abc}$
+ $\widetilde{def}$
+ $\overrightarrow{ghi}$
+ $\overline{jkl}$
+ $\underline{mno}$
+ $\overbrace{pqr}$
+ $\underbrace{stu}$
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mode mathématique,accents mathématiques,grands accents,chapeau,accolade horizontale,barre horizontale
```

