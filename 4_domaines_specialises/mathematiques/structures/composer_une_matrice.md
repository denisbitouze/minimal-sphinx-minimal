# Comment composer une matrice ?

- Il suffit de construire un tableau sans lignes avec l'environnement `array` comme dans cet exemple :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{array}
  \pagestyle{empty}

\begin{document}
\Large
\[
\begin{array}{ccc}
   x_{11} & \cdots & x_{1p} \\
   \vdots & \ddots & \vdots \\
   x_{n1} & \cdots & x_{np}
\end{array}
\]
\end{document}
```

Pour encadrer cette matrice avec des délimiteurs, voir la question « {doc}`Comment ajuster la taille de délimiteurs? </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>` ».

- Le package {ctanpkg}`amsmath` (voir la question « {doc}`Que sont AMS-TeX et AMS-LaTeX? </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>` ») permet de définir une matrice de manière plus rapide qu'avec l'environnement `array`. Les environnements disponibles sont :
- `matrix`,
- `pmatrix` pour une matrice encadrée par des parenthèses,
- `bmatrix` pour une matrice encadrée par des crochets,
- `vmatrix` pour une matrice encadrée par des lignes verticales,
- `Vmatrix` pour une matrice encadrée par des doubles lignes verticales.

Utilisation de {ctanpkg}`amsmath` pour les matrices :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{amsmath}
  \pagestyle{empty}

\begin{document}
\Large
\[
\begin{pmatrix}
   a & b \\
   c & d
\end{pmatrix}
\]
\end{document}
```

- Le package {ctanpkg}`easymat` aide également à l'écriture de matrices.
- Le package {ctanpkg}`easybmat` fournit des facilités pour écrire des matrices par bloc.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,matrice,tableau
```

