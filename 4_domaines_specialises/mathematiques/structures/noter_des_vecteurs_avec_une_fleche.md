# Comment noter un vecteur ?

## Avec l'extension « amsmath »

Il suffit d'utiliser l'extension {ctanpkg}`amsmath` et d'appeler la commande `\overrightarrow`. En voici un exemple :

```latex
Le vecteur $\overrightarrow{u}$
```

On peut également définir ses propres flèches notamment pour régler leur hauteur par rapport aux lettres qu'elles surmontent. En voici un exemple :

```latex
\documentclass{article}
  \usepackage{amsmath}
  \usepackage[matrix,arrow]{xy}

\newcommand{\xyflecheverladroite}%
           {\mbox{\xymatrix{%
            *{\hphantom{OM}}\ar[]+L;[]+R%
           }}}

\newcommand{\ra}[1]{%
  \mathchoice%
    {\overset{\mbox{\xymatrix{%
     *{\hphantom{\displaystyle #1}}
     \ar[]+L;[]+R}}}{\displaystyle #1}%
     }%
    {\overset{\mbox{\xymatrix{%
     *{\hphantom{\textstyle #1}}
     \ar[]+L;[]+R}}}{\textstyle #1}%
     }%
    {\overset{\mbox{\xymatrix{%
     *{\hphantom{\scriptstyle #1}}
     \ar[]+L;[]+R}}}{\scriptstyle #1}%
     }%
    {\overset{\mbox{\xymatrix{%
     *{\hphantom{\scriptscriptstyle #1}}
     \ar[]+L;[]+R}}}{\scriptscriptstyle #1}%
     }%
}

% Pour changer la distance de la flèche,
% on peut procéder ainsi :
% \renewcommand{\ra}[1]
% {\overset{\raisebox{-1pt}{\mbox{\xymatrix{*{%
% \hphantom{#1}} \ar[]+L;[]+R}}}}{#1}}
\begin{document}
\[ \ra{OM} \]
\(\ra{OM}\)
\(\ra{OM_i}\)
\[ \ra{OM}_{\ra{OM}_{\ra{OM}}} \]
\end{document}
```

## Avec l'extension « vector »

L'extension {ctanpkg}`vector`, de Nick Efford, offre notamment un certain nombre de vecteurs utiles aux physiciens et des commandes automatiques d'énumération des coordonnées. Par exemple :

```latex
\documentclass{article}
  \usepackage{vector}
  \pagestyle{empty}

\begin{document}
$(\irvec[4]x)$

$\bvec{y} = \left[ \cvec{y}{0}{3} \right]$
\end{document}
```

```{eval-rst}
.. todo:: L'exemple ne compile pas (pour l'instant), car l'extension :ctanpkg:`vector` doit être installée manuellement.
```

## Avec l'extension « easyvector »

L'extension {ctanpkg}`easyvector` permet de définir des vecteurs suivant une syntaxe de type langage C.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,notation des vecteur,flèches
```

