# Comment tracer un tableau de variations ?

- La méthode actuellement conseillée utilise le package {ctanpkg}`tkz-tab`, écrit par Alain Matthes et basé sur Ti*k*Z, pour composer tableaux de signes et tableaux de variations. La richesse de sa syntaxe permet toutes les fantaisies (utilisation de la couleur, par exemple) et {texdoc}`sa documentation, très complète, est écrite en français. <tkz-tab>`

```latex
\documentclass{article}
\usepackage{tkz-tab}
\pagestyle{empty}

\begin{document}
\begin{tikzpicture}
\tikzset{h style/.style = {pattern=north west lines}}
\tkzTabInit[lgt=1,espcl=2]{$x$ /1,  $f$ /2}{$0$,$1$,$2$,$3$}%
\tkzTabVar{+/ $1$  / , -CH/ $-2$ / , +C/  $5$, -/ $0$  /  }
\end{tikzpicture}
\end{document}
```

- Pour un tableau simple, c'est également possible (et facile) avec l'environnement `array` du mode mathématique standard de LaTeX comme ici :

```latex
$
\begin{array}{|c|ccccr|}
\hline
x     & -\infty &  & 0 & & +\infty \\ \hline
f'(x) & 5 & + & 0  & - & -10       \\ \hline
      &   &   & 10 &   &           \\ % ligne des valeurs "max"
f(x) &    &\nearrow & &\searrow  & \\ % flèches
     & -\infty &  &  & & -10       \\ % ligne des valeurs "min"
\hline
\end{array}
$
```

Les flèches sont nommées d'après les points cardinaux : `\nearrow` est pour *north-east*, `\searrow` pour *south-east* :

| Commande   | Résultat   |
| ---------- | ---------- |
| `\nearrow` | $\nearrow$ |
| `\searrow` | $\searrow$ |
| `\nwarrow` | $\nwarrow$ |
| `\swarrow` | $\swarrow$ |

On peut faire de plus jolies flèches (redimensonnables) avec les commandes graphiques, mais le code est moins lisible.

Pour cela, il faut choisir une unité de longueur avant par exemple `\unitlength=1cm` et remplacer par exemple `nearrow` par des flèches de 2cm de large et de haut, comme dans cet exemple :

```latex
\unitlength=1cm
$
\begin{array}{|c|ccccr|}
\hline
x & -\infty & & 0 & & +\infty \\ \hline
f'(x) & 5 & + & 0 & - & -10 \\ \hline
 & & & 10 & & \\          % ligne des valeurs "max"
f(x) & &
  \begin{minipage}{1cm}
  \begin{picture}(1,1)
    \put(0,0){\vector(1,1){1}}
  \end{picture}
  \end{minipage}
 & &
  \begin{minipage}{1cm}
  \begin{picture}(1,1)
     \put(0,1){\vector(1,-1){1}}
  \end{picture}
  \end{minipage}
& \\                                      % flèches
& -\infty & & & & -10 \\  % ligne des valeurs "min"
\hline
\end{array}
$
```

- Enfin, le package {ctanpkg}`tableaux`, également téléchargeable à l'adresse <http://melusine.eu.org/syracuse/exemples/kisselhoff/>, permet d'effectuer des mises en page plus complexes grâce à {ctanpkg}`PStricks <pstricks>`. Par exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{ifthen,minimum,tableau}
\newpsstyle{hachured}
           {fillstyle=hlines,
            hatchwidth=0.2pt,
            hatchsep=2pt}
\begin{document}

\setlength{\TabTitreL}{1cm}
\begin{MonTableau}{1}{9}{1.5}
\psframe[style=hachured,%
         linestyle=none](.3,1)(.45,0)

\TabNewCol{0}
\rTabPut{Br}{-.15}{.25}{$x$}
\rTabPut{Br}{-.15}{.40}{$f(x)$}

\TabNewCol{0}
\rTabPut{Bl}{.15}{.25}{$0$}
\rTabPut{Bl}{.15}{.60}{$\frac{3}{2}$}

\TabNewCol{.30}
\rTabPut{B}{0}{.25}{$1$}
\rTabPut{Br}{-.15}{.80}{$+\infty$}
\psline[style=TabDblBarre](.3,0)(.3,1)

\TabNewCol{.45}
\rTabPut{B}{0}{.25}{$\frac{3}{2}$}
\rTabPut[2]{B}{0}{.10}{\TabZ}

\TabNewCol{1}
\rTabPut{Br}{-.15}{.25}{$+\infty$}
\rTabPut{Br}{-.15}{.40}{$1$}

\TabFleche{B1}{C1}
\TabFleche{D1}{E1}
\end{MonTableau}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonctions mathématiques,TikZ,PStricks
```

