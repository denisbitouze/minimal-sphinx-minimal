# Comment écrire une fraction ?

LaTeX propose la commande mathématique `\frac{`*arg1*`}{`*arg2*`}` dont l'argument *arg1* est le numérateur et *arg2* le dénominateur. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
Une fraction en formule :
\[\frac{\alpha}{6 + \sqrt{2}} = \frac{1+\sqrt{2}}{1+\frac{\beta-1}{4}}\]
ou dans le texte $\frac{\alpha}{6 + \sqrt{2}}$.
\end{document}
```

```latex
Une fraction en formule :
\[\frac{\alpha}{6 + \sqrt{2}} = \frac{1+\sqrt{2}}{1+\frac{\beta-1}{4}}\]
ou dans le texte $\frac{\alpha}{6 + \sqrt{2}}$.
```

## Comment écrire une division en ligne ?

La notation fractionnaire est la plus courante, mais n'oublions pas que l'on apprend d'abord à écrire les divisions « en ligne », avec un [Obèle](https://fr.wikipedia.org/wiki/Obèle) :

```latex
\large
$6 \div 3 = 2$
```

______________________________________________________________________

*Source :*

- [Is there a way to produce this division symbol ? ÷](https://tex.stackexchange.com/questions/108035/is-there-a-way-to-produce-this-division-symbol-%C3%B7)

```{eval-rst}
.. meta::
   :keywords: LaTeX,composition des mathématiques,symbole de fraction,barre de fraction,symbole de division,opérateurs mathématiques,Obélix
```

