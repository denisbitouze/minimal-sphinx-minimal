# Comment déclarer de nouveaux opérateurs mathématiques ?

Pour cela, vous pouvez utiliser la commande `\mathop`. Par exemple :

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\begin{document}
\newcommand{\diag}{\mathop{\mathrm{diag}}}
\[\diag_1^n \]
\end{document}
```

Les indices et les exposants associés à `\diag` seront placés en dessous et au-dessus du nom de la fonction, comme ils le sont pour la commande `\sum`. Si vous voulez que vos indices et exposants soient toujours placés à droite, utilisez la commande `\nolimits` (Pour plus d'information, voir la question « {doc}`Comment positionner les limites des grands opérateurs ? </4_domaines_specialises/mathematiques/structures/operateurs/changer_la_position_des_limites_autour_des_sommes_et_integrales>` »).  :

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\begin{document}
\newcommand{\diag}{\mathop{\mathrm{diag}}\nolimits}
\[\diag_1^n \]
\end{document}
```

L'ensemble {ctanpkg}`amsmath <latex-amsmath>` (dans son extension {ctanpkg}`amsopn` automatiquement chargée par l'extension {ctanpkg}`amsmath`) fournit une commande `\DeclareMathOperator` qui effectue le même travail que la deuxième définition ci-dessus :

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\DeclareMathOperator{\diag}{diag}
\begin{document}
\[\diag_1^n \]
\end{document}
```

Pour créer notre commande `\diag` originale, il faut utiliser la commande étoilée `\DeclareMathOperator*` qui génère un opérateur qui a toujours ses exposants et exposants.

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\DeclareMathOperator*{\diag}{diag}
\begin{document}
\[\diag_1^n \]
\end{document}
```

La commande `\operatorname` de l'extension {ctanpkg}`amsopn` vous permet d'introduire des opérateurs *ad hoc* directement dans vos mathématiques. Vous pouvez donc saisir :

```latex
% !TEX noedit
\[ \operatorname{foo}(bar) \]
```

Ceci sera équivalent à :

```latex
% !TEX noedit
% Dans le préambule
\DeclareMathOperator{\foo}{foo}
% (...)
% Dans le document
\[ \foo(bar) \]
```

Comme pour `\DeclareMathOperator`, il existe une version étoilée `\operatorname*` pour gérer la position des indices et exposants.

