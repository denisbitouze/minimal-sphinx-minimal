# Comment obtenir des accolades horizontales ?

- Les commandes `\overbrace` et `\underbrace` permettent respectivement d'obtenir des accolades horizontales au-dessus ou au-dessous de leur argument :

```latex
% !TEX noedit
\[
\overbrace{f(x)}^{=0} + \underbrace{g(y)}_{=0} = 0
\]
```

```latex
\[
\overbrace{f(x)}^{=0} + \underbrace{g(y)}_{=0} = 0
\]
```

- L'extension {ctanpkg}`oubraces` permet d'entrelacer des accolades `overbrace` et `underbrace` :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{oubraces}
\begin{document}
\[
\overunderbraces{&\br{2}{x}& &\br{2}{y}}%
  {a + b +&c + d +&e + f&+&g + h&+ i + j&+ k + l}%
  {& &\br{3}{z}}
= \pi r^2
\]
\end{document}
```

```latex
\documentclass{article}
\usepackage{oubraces}
\pagestyle{empty}
\begin{document}
\[
\overunderbraces{&\br{2}{x}& &\br{2}{y}}%
  {a + b +&c + d +&e + f&+&g + h&+ i + j&+ k + l}%
  {& &\br{3}{z}}
= \pi r^2
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

