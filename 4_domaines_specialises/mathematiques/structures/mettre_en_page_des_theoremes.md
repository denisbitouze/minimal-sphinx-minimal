# Comment mettre en page des théorèmes ?

Pour présenter des théorèmes, lemmes, propositions ou bien encore axiomes, il existe plusieurs possibilités, présentées ici.

Si vous cherchez à ajouter un carré en fin de texte du théorème, vous pouvez consulter la question « [Comment ajouter un carré en fin de démonstration ?](/4_domaines_specialises/mathematiques/symboles/symbole_cqfd) ».

## Avec les commandes de base

LaTeX dispose de la commande `\newtheorem` qui existe sous deux formes :

```latex
% !TEX noedit
\newtheorem{nom}[compteur]{texte}[section]
\newtheorem{nom}[compteur]{texte}
```

Dans cette définition :

- *nom* est une clé pour identifier le théorème ;
- *texte* le titre du théorème qui sera imprimé ;
- *compteur* permet de référencer un autre type de théorème en suivant le numérotation des théorèmes désignés dont le *nom* est *compteur*. La deuxième forme est donc adaptée pour créer des éléments qui incrémentent ;
- *section* permet de préciser le niveau de numérotation voulu pour la numérotation des théorèmes (cela peut donc valoir `chapter`, `section`, `subsection`.

En voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\newtheorem{th-imp}%
           {Théorème important}[section]
\newtheorem{th-u}%
           {Théorème}[subsection]
\pagestyle{empty}
\begin{document}
\section{Introduction}
\begin{th-imp} \label{th-AA}
    BLA BLA
\end{th-imp}
\subsection{Bases}

\begin{th-u} \label{th-aaa}
   bla bla bla
\end{th-u}

\begin{th-u}
   bla bla
\end{th-u}

\section{Conclusion}

Le théorème important~1.1
est à apprendre par c\oe{}ur,
le théorème~1.1.2
peut être utile.
\end{document}
```

## Avec l'extension « ntheorem »

L'extension {ctanpkg}`ntheorem` peut générer une liste des théorèmes de la même manière que `listoffigures`, est compatible avec {ctanpkg}`mathtools` (et donc {ctanpkg}`amsmath`), permet d'ajouter des symboles à la fin d'un théorème (carré, q.e.d.), et permet également la référence à d'autres théorèmes. Pour plus de précisions, voir la {texdoc}`documentation de l'extension <ntheorem>`. Voici un exemple d'utilisation.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage[thmmarks]{ntheorem}

{\theoremstyle{changebreak}
\theoremindent0.5cm
\theoremnumbering{greek}
\newtheorem{Lemma}{Lemme}}

{\theoremheaderfont{\normalfont%
    \bfseries}
\theorembodyfont{\slshape}
\theoremsymbol{\ensuremath{%
    \diamondsuit}}
\theoremseparator{:}
\newtheorem{Theorem}{Théorème}}

{\theoremheaderfont{\sc}%
    \theorembodyfont{\upshape}
\theoremstyle{nonumberplain}
\theoremseparator{}
\theoremsymbol{\rule{1ex}{1ex}}
\newtheorem{Proof}{Preuve}}
\pagestyle{empty}
\begin{document}

\begin{Theorem}[Théorème complexe]
Voici l'énoncé de mon théorème.

\begin{Lemma}[Equation]
\label{lem=equ}
   Un lemme important~ :
\begin{equation}
  0+0=\mbox{la t\^ete à toto}
\end{equation}
\end{Lemma}

\begin{Proof}[de rien]
  D'après le lemme $\alpha$,
  le théorème est évident.
\end{Proof}
Fin du théorème.
\end{Theorem}
\end{document}
```

## Avec l'extension « theorem »

L'extension {ctanpkg}`theorem` augmente les possibilités de présentation de l'environnement pour les théorèmes que propose LaTeX par défaut.

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` (chargeant {ctanpkg}`amsmath`) définit l'environnement `proof` insérant un carré.

```{eval-rst}
.. todo:: // Présenter amsthm. //
```

## Avec l'extension « shadethm »

L'extension {ctanpkg}`shadethm` permet d'écrire des théorèmes sur fond ombré.

## Avec l'extension « nccthm »

L'extension {ctanpkg}`nccthm` permet de faire à peu près la même chose que {ctanpkg}`ntheorem`. Il dispose de compteurs dynamiques, de différents styles, de symboles de fin de preuve, etc. Se reporter à la documentation de l'extension pour plus d'informations.

## Récapitulatif

| Extension                                                                                                                                                        | amsthm | theorem | ntheorem  |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ | ------- | --------- |
| Retour à la ligne après le titre Pas de numérotation d'un théorème Position du numéro du théorème Style de la numérotation Ponctuation après le titre (:,.,etc.) | x xx   | xx      | x x x x x |
| Police du titre ou du corps Indentation du titre                                                                                                                 | x x    | x       | x         |
| Espace vertical autour du théorème Symbole de fin de preuve                                                                                                      | x x    | x       | x x       |

```{eval-rst}
.. todo:: *Revoir le tableau du fait des autres extensions évoquées.*
```

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```

