# Comment tracer des diagrammes commutatifs ?

- \\Xypic est un outil simple et puissant qui permet de réaliser de tels diagrammes comme dans ici :

```latex
% !TEX noedit
\documentclass[a4paper,12pt]{article}
\usepackage[all]{xy}
\begin{document}
\[\xymatrix{
  A \ar[d] \ar[r] \ar@{=}[rd] & B \ar[d] \\
  C \ar[r] & D }
\]
\end{document}
```

```latex
\documentclass[a4paper,12pt]{article}
\usepackage[all]{xy}
\begin{document}
\[\xymatrix{
  A \ar[d] \ar[r] \ar@{=}[rd] & B \ar[d] \\
  C \ar[r] & D }
\]
\end{document}
```

- On peut également utiliser le package {ctanpkg}`amscd` disponible sur <https://www.ctan.org/%7Bmacros/latex/required/amslatex/math/%7D> et l'environnement `CD` comme dans l'exemple~\\vref\{maths-commut-amscd} ou plus généralement le package {ctanpkg}`amsmath` comme dans l'exemple~\\vref\{maths-commut-amsmath} mais son offre est plus limitée.

Utilisation de {ctanpkg}`amscd`\\label\{maths-commut-amscd}

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath,amscd}
\begin{document}
\[
\begin{CD}
   \mathcal{F} @>\otimes>> T\\
   @VdrVlfV @ViVjV\\
   \mathtt{f} @= t
\end{CD}
\]
\[
\begin{CD}
d @>>> e @>>> f \\
@AAA @. @AAA \\
a @>>> b @>>> c
\end{CD}
\]
\end{document}
```

Exemple d'utilisation d' {ctanpkg}`amsmath` pour un diagramme commutatif\\label\{maths-commut-amsmath}} :

```latex
% !TEX noedit
\[
\begin{array}{ccccc}
d & \longrightarrow & e & \longrightarrow &f \\
\uparrow &&&& \uparrow \\
a & \longrightarrow & b &\longrightarrow& c\\
\end{array}
\]
```

:::{note}
Dans la dernière version de {ctanpkg}`amscd`, la syntaxe@>Exp1>Exp2>'' utilisée pour les flèches extensibles a été abandonnée au profit des commandes `xleftarrow` et `xrightarrow`.

Pour plus de détails, consulter~ :

- *User's Guide for the amsmath Package*, dans le fichier `amsldoc.pdf`
- *Differences between various versions of the amsmath package*, dans le fichier `diff-m.txt`.
:::

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```

