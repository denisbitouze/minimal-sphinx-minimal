# Comment forcer le style d'un tableau à « displaystyle » ?

Le style par défaut dans un `array` est *textstyle*. Pour éviter d'avoir à écrire `\displaystyle` dans chaque cellule, on peut déclarer un tableau entièrement {doc}`en mode </4_domaines_specialises/mathematiques/a_quoi_sert_displaystyle>` {doc}`displaystyle </4_domaines_specialises/mathematiques/a_quoi_sert_displaystyle>` grâce à cet environnement :

```latex
% !TEX noedit
\newenvironment{disarray}%
 {\everymath{\displaystyle\everymath{}}\array}%
 {\endarray}
```

**Avec `disarray`:**

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{array}
  \pagestyle{empty}

\newenvironment{disarray}{%
 \everymath{\displaystyle\everymath{}}\array%
}{%
 \endarray%
}

\begin{document}
\(
\begin{disarray}{|l|}
 y =\sum_{i=0}^{n} i^2 \\
 y =\sum_{i=0}^{n} (i+1)^2 \\
 y =\sum_{i=0}^{n} \bigl(i^2+i+1\bigr)^2 \\
\end{disarray}
\)
\end{document}
```

**Sans:**

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{array}
  \pagestyle{empty}

\begin{document}
\(
\begin{array}{|l|}
 y =\sum_{i=0}^{n} i^2 \\
 y =\sum_{i=0}^{n} (i+1)^2 \\
 y =\sum_{i=0}^{n} \bigl(i^2+i+1\bigr)^2 \\
\end{array}
\)
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,tableau en mode hors-texte,tableaux en mode mathématique
```

