# Comment gérer les sauts de ligne dans des mathématiques placées dans le texte ?

TeX, par défaut, vous permet de diviser une expression mathématique en fin de ligne : il permet des coupures au niveau des opérateurs relationnels (comme « `=` », « `<` », etc.) et des opérateurs binaires (comme «+'' », « `-` », etc.). Dans le cas de grandes expressions, cela peut être très pratique. Toutefois, dans le cas d'expressions simples comme « a=b+c », une coupure peut être très gênante pour le lecteur et l'éviter est souhaitable. Heureusement, ces coupures sont contrôlables.

## Avec les paramètres de TeX

Il existe en effet des pénalités associées à chaque type d'opérateur : chaque pénalité indique avec quelle intensité une rupture doit être évitée. Les valeurs par défaut sont :

```latex
% !TEX noedit
\relpenalty   = 500
\binoppenalty = 700
```

Vous rendez la coupure moins activable en augmentant ces valeurs. Vous pouvez d'ailleurs interdire toutes les coupures avec la valeur `10000` qui est la valeur maximale autorisée :

```latex
% !TEX noedit
\relpenalty   = 10000
\binoppenalty = 10000
```

Si vous voulez juste éviter la coupure dans une seule expression, écrivez :

```latex
% !TEX noedit
{%
  \relpenalty   = 10000
  \binoppenalty = 10000
  $a=b+c$
}
```

Les valeurs d'origine resteront inchangées en dehors des accolades.

## Avec les accolades

La méthode détaillée ci-dessus peut devenir rapidement fastidieuse... et il existe une approche alternative, dans laquelle vous dites quelles parties de l'expression peuvent ne pas être coupées quoi qu'il arrive. Supposons que nous voulions reporter une coupure jusqu'après l'égalité, nous pourrions écrire :

```latex
% !TEX noedit
${a+b+c+d} = z+y+x+w$
```

Les accolades indiquent ici qu'il faut traiter la sous-formule comme un unique élément non sécable (en TeX au moins).

______________________________________________________________________

*Source :* {faquk}`Line-breaking in in-line maths <FAQ-brkinline>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

