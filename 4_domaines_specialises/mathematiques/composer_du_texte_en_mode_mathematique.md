# Comment placer du texte dans des mathématiques ?

Lorsque nous saisissons des mathématiques dans TeX ou LaTeX, les lettres à partir desquelles nous composons du texte ordinaire prennent une signification particulière : elles deviennent toutes des noms de variables à une seule lettre. Si les lettres apparaissent en italique, il ne s'agit pas du même italique que celui du texte ordinaire : une série de lettres mathématiques (par exemple "ici") semble étrangement maladroite par rapport au mot écrit en italique texte. En effet, le texte en italique est créné pour que les lettres s'emboîtent bien, tandis que les lettres mathématiques sont configurées pour donner l'impression que vous multipliez *i* par *c* par *i*. L'autre fait notable réside dans l'absence de prise en compte des espaces dans le mode mathématique : au mieux pouvons-nous écrire de simples mots isolés.

Aussi, si nous voulons avoir un beau texte au sein des mathématiques que nous écrivons, nous devons prendre des précautions particulières. Si vous utilisez LaTeX, ce qui suit devrait vous aider.

## Avec des commandes de base

Le plus simple est d'utiliser `\mbox` ou `\textrm` (ou `\textit` si vous souhaitez un texte en italique) :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
$e = mc^2 \mbox{ et hop !}$
\end{document}
```

Le problème est que, avec ces commandes, la taille du texte reste celle du texte environnant, de sorte qu'il est tout à fait possible de tomber sur ce résultat disgracieux. Ce problème n'apparaît pas avec `\textrm` :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
$z = a_{\mbox{de l'autre bout}}$

$z = a_{\mbox{de l'autre bout}}$ % Du fait des chargements d'extensions dans le moteur du site.
\end{document}
```

Vous pouvez corriger ce problèmes avec les [sélecteurs de taille](/3_composition/texte/symboles/polices/changer_la_taille_d_une_fonte) dans le texte, comme :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
$z = a_{\mbox{\scriptsize de l'autre bout}}$
\end{document}
```

Ceci fonctionne si votre texte environnant est à la taille de document par défaut, mais vous donne la mauvaise taille dans le cas contraire. La question « {doc}`Comment modifier la taille des indices ? </4_domaines_specialises/mathematiques/structures/indices/reduire_la_taille_des_indices_en_mode_mathematique>` » montre une autre utilisation de cette méthode.

## Avec l'extension « mathtools » (ou « amsmath »)

Recourir à la commande `\mbox` est (à peu près) raisonnable pour une utilisation occasionnelle, mais des textes mathématiques longs demandent une technique plus efficace pour éviter au rédacteur d'incessantes corrections. L'extension {ctanpkg}`mathtools` fournit ici le nécessaire avec la commande `\text`. Pour être exact, la commande est en fait fournie par l'extension {ctanpkg}`amstext`, mais l'extension {ctanpkg}`mathtools` charge {ctanpkg}`amsmath` qui, elle-même, charge {ctanpkg}`amstext`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
$z = a_{\text{de l'autre bout}}$
\end{document}
```

Le texte sera à la bonne taille et dans la même police que le texte environnant. L'extension {ctanpkg}`amstext` corrige également la commande `\textrm`... mais `\text` est plus facile à taper que `\textrm` !

## Cas des textes intercalés dans des formules

L'extension {ctanpkg}`mathtools`, par le biais {ctanpkg}`amsmath`, prévoit également des commentaires positionnés au milieu d'une de ses structures d'affichage multi-lignes, via la commande `\intertext`. Par exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\begin{align}
  A//1&=N//0(\lambda;\Omega')-\phi(\lambda;\Omega'),\\
  A_2&=\phi(\lambda;\Omega')-\phi(\lambda;\Omega),\\
  \intertext{et}
  A_3&=
      \mathcal{N}(\lambda;\omega).
\end{align}
\end{document}
```

Cette commande place le texte « et » sur une ligne séparée avant la dernière ligne de l'affichage. Si le texte intercalé est court ou si les équations elles-mêmes sont légères, vous constaterez peut-être que `\intertext` laisse trop d'espace. La commande `\shortintertext` (propre à {ctanpkg}`mathtools`) est un peu moins gourmande en espace :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\begin{align}
  a =& b
  \shortintertext{ou}
  c =& b
\end{align}
\end{document}
```

Pour avoir le texte sur la même ligne que la deuxième équation, on peut utiliser l'environnement `flalign` ( de {ctanpkg}`mathtools` et d' {ctanpkg}`amsmath`) avec plein d'équations factices (représentées par le double « `&&` ») :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\begin{flalign}
            && a =& b && \\
  \text{ou} && c =& b &&
\end{flalign}
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Text inside maths <FAQ-mathstext>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,texte
```

