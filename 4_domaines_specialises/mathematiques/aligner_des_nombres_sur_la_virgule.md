# Comment aligner des nombres sur le séparateur décimal ?

## L'extension « dcolumn »

L'extension {ctanpkg}`dcolumn` permet d'aligner les nombres d'un tableau par rapport à leur séparateur décimal paramétrable (la virgule en français, le point en anglais).

## L'utilisation d'un tableau

On peut également utiliser un tableau avec une virgule comme séparateur de colonnes. Voici un exemple de cette méthode :

```latex
\Large
\[
\begin{array}{r @{,} l}
   1&2 \\
   233&456 \\
   x&y \\
\end{array}
\]
```

## L'extension « siunitx »

Toujours dans un tableau, en dehors du mode mathématique, l'extension {ctanpkg}`siunitx` (bien utile dés qu'on gère des nombre et des unités) met à disposition le style de colonne "S", aligné sur le marqueur décimal mais aussi les milliers (espace en français, virgule en anglais), comme le montre l'exemple suivant.(

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *L'exemple devrait être traité avec la virgule. Cependant l'option de siunitx « output-decimal-marker={,} » ne fonctionne pas pour le moment.*) »
```

```latex
\documentclass{article}
  \usepackage{siunitx}
  \pagestyle{empty}
\begin{document}
\Large
\begin{tabular}{lS}
 A & 1,23          \\
 B & 123456,123456 \\
\end{tabular}
\end{document}
```

## L'extension « numprint »

L'extension {ctanpkg}`numprint` fournit des types de colonnes `n` et `N` qui permettent d'aligner les valeurs sur le séparateur décimal.

```{eval-rst}
.. todo:: Ajouter un exemple.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,alignement,aligner sur la virgule,aligner sur le point,aligner des nombres
```

