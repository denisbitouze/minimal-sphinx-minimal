# Comment biffer des termes dans une expression mathématique ?

Barrer (ou « biffer ») des termes permet d'expliquer le comportement d'expressions ou d'équations (souvent à des fins pédagogiques). Depuis 2013, l'extension {ctanpkg}`cancel` de Donald Arseneau fournit plusieurs commandes pour le faire :

- `\cancel{`*expression*`}` trace un trait diagonal sur l'*expression* (de gauche à droite et de bas en haut);
- `\bcancel{`*expression*`}` trace un trait diagonal inversé sur l'*expression* (de gauche à droite et de haut en bas);
- `\xcancel{`*expression*`}` coche l'*expression* en cumulant les effets des deux commandes précédentes;
- `\cancelto{`*valeur*`}{`*expression*`}` barre l'*expression* d'une flèche pointant vers la *valeur* de cette expression;

Voici des exemples d'applications :

```latex
\documentclass{article}
  \usepackage{amsmath}
  \usepackage{cancel}
  \pagestyle{empty}

\begin{document}
\begin{align*}
(a-b)(a+b) &= a^2 + \cancel{ab} - \cancel{ba} - b^2   \\
           &= a^2 + \bcancel{ab} - \bcancel{ba} - b^2 \\
           &= a^2 + \xcancel{ab} - \xcancel{ba} - b^2 \\
           &= a^2 + \xcancel{ab - ba} - b^2           \\
           &= a^2 + \cancelto{0}{ab - ba} - b^2       \\
           &= a^2 - b^2
\end{align*}
\end{document}
```

La {texdoc}`documentation de l'extension « cancel » <cancel>` propose quelques options supplémentaires pour régler un peu plus finement ces marques.

:::{note}
N'oubliez pas que vous pouvez aussi égayer vos formules avec des couleurs.

En combinant l'extension {ctanpkg}`cancel` avec {ctanpkg}`xcolor`, vous pouvez aider votre lecteur à suivre les termes d'une étape à l'autre :

```latex
\documentclass[12pt]{article}
  \usepackage{cancel}
  \usepackage{color}
  \pagestyle{empty}

\newcommand{\red}{\color{red}}

\begin{document}
\[
\frac{ {\red a} b }{ b } =
\frac{ {\red a} \cancel{b} }{ \cancel{b} }
= {\red a}
\]
\end{document}
```
:::

______________________________________________________________________

*Source :*

- {faquk}`Cancelling terms in maths expressions <FAQ-cancellation>`,
- [LaTeX pour les enseignants](https://www.editions-ellipses.fr/accueil/378-latex-pour-les-enseignants-9782340036710.html), Nicolas Poulain, Ellipses (2020), 226 pages. ISBN : 9782340036710.

```{eval-rst}
.. meta::
   :keywords: LaTeX,biffer,annuler,barrer,mathématiques,simplifier une équation,enseignement des mathématiques
```

