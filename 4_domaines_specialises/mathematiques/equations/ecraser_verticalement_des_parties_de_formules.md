# Comment écraser verticalement des parties d'une équation ?

À l'inverse de l'astuce « {doc}`Comment utiliser les fantômes? </3_composition/texte/exploiter_les_fantomes>` », qui vise à réserver de la place pour des éléments invisibles, vous pouvez souhaiter que des éléments visibles n'occupent pas (ou moins) de place du point de vue de TeX, leur représentation graphique restant inchangée.

Ceci s'obtient grâce à la commande `\smash`, qui insère son argument comme un objet de hauteur et profondeur nulles (voir « {doc}`Quelles sont les dimensions des boîtes ? </2_programmation/syntaxe/boites/comprendre_le_modele_de_boites>` »). Les variantes `\smash[b]` et `\smash[t]` (fournies par {ctanpkg}`mathtools` ou {ctanpkg}`amsmath`) n'annulent respectivement que la profondeur ou la hauteur.

Voici un exemple suivant illustrant ce point. Observez bien la profondeur des racines du premier et dernier terme par rapport à celui du milieu :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\[
\sqrt{y} + \sqrt{x} + \sqrt{\smash[b]{y}}
\]
\end{document}
```

Il convient d'utiliser ce genre de commande avec prudence car, en mentant à TeX sur la taille des objets qu'il manipule, des risques de chevauchement visuels apparaissent.

______________________________________________________________________

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#smash>

```{eval-rst}
.. meta::
   :keywords: LaTeX,équations,formules mathématiques,taille d'une formule,espace vertical
```

