# Comment numéroter les équations ?

L'environnement le plus courant pour numéroter des équations simples est `equation`. La numérotation est d'ailleurs une fonctionnalité par défaut de nombreux environnements traitant des équations et des {doc}`groupes d'équation </4_domaines_specialises/mathematiques/equations/aligner_des_equations>`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici un exemple de numérotation
classique~ :
\begin{equation}
   x + 4 = 0
\end{equation}
\begin{equation}
   y - 7 = 0
\end{equation}
\end{document}
```

## Placer la numérotation à gauche

Sous LaTeX, les options de classe `leqno` et `fleqn` permettent d'indiquer si les numéros doivent apparaître à droite ou à gauche des équations.

```latex
\documentclass[leqno]{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici un exemple de numérotation
placée à gauche~ :
\begin{equation}
   x + 4 = 0
\end{equation}
\end{document}
```

## Réinitialiser le compteur de numérotation des équations

Pour réinitialiser le compteur d'équations dans chaque section, il faut inclure dans le préambule :

```latex
% !TEX noedit
\makeatletter
\renewcommand\theequation%
{\thesection.\arabic{equation}}
\@addtoreset{equation}{section}
\makeatother
```

:::{important}
Dans ce cas, il ne faut pas mettre d'équation dans un chapitre avant la première section numérotée sous peine d'avoir un numéro du style 3.0.1.
:::

## Répéter une équation et sa numérotation

Lorsqu'une équation est répétée, il est souvent souhaitable que sa numérotation soit la même que lors de sa première apparition. Sous LaTeX, l'extension {ctanpkg}`amsmath` fournit la commande `\tag` permettant de gérer ce point :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathtools}
\begin{document}
Nous posons :
\begin{equation}
  a=b
  \label{eq1}
\end{equation}
(...) Et comme nous l'avons vu :
\begin{equation}
  a=b
  \tag{\ref{eq1}}
\end{equation}
Ces deux équations sont numérotées
par \eqref{equa}.
\end{document}
```

La commande `\tag` permet initialement de personnaliser la numérotation des équations en mettant son argument comme numérotation. Ici, elle remplace donc la numérotation de la deuxième équation par celle de la première équation récupérée avec la commande `\label` et restituée avec la commande `\ref`.

:::{note}
`eqref` remplace `ref` pour les équations : la police utilisée est toujours la même (c'est plus beau).
:::

______________________________________________________________________

*Source :* [FAQ-reuseq](https://www.texfaq.org/FAQ-reuseq)

```{eval-rst}
.. meta::
   :keywords: LaTeX, numérotation, équations, numéroter, référence, label, ref
```

