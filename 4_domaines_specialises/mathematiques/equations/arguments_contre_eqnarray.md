# Quels sont les arguments contre l'utilisation d'« eqnarray » ?

L'environnement `eqnarray` paraît attractif pour les utilisateurs occasionnels de mathématiques dans des documents LaTeX : il semble permettre d'alignement de systèmes d'équations.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\begin{eqnarray}
  a & = & b + c \\
  x & = & y - z
\end{eqnarray}
\end{document}
```

Et, en effet, il *fournit* cette fonctionnalité d'alignement... mais en gérant l'espacement de façon douteuse.

Ce sujet est discuté en détail dans un article en anglais du [PracTeX journal paper](https://tug.org/pracjourn/2006-4/madsen/madsen.pdf) de Lars Madsen; Stefan Kottwitz a également rédigé un [billet du TeX blog](http://texblog.net/latex-archive/maths/eqnarray-align-environment/) en anglais qui inclut des copies d'écran illustrant clairement les problèmes associés à `eqnarray`. La présente page se limite à trois arguments importants.

## L'espacement autour du symbole de relation

Typographiquement, `eqnarray` rajoute beaucoup d'espacement autour du symbole de relation, de façon injustifiée et incohérente avec les autres environnements mathématiques (ci-dessous `equation`, `eqnarray` puis `align`).

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\begin{equation*}
  x & = & y
\end{equation*}
\vspace*{-0.8cm}
\begin{eqnarray*}
  x & = & y
\end{eqnarray*}
\vspace*{-1cm}
\begin{align*}
  x & = y
\end{align*}
\end{document}
```

De fait, dans l'environnement `eqnarray`, les espaces autour du symbole «        $=$ » ne sont pas ceux définis par les métriques de la fonte. Ils sont définis par `\arraycolsep`, qui peut être arbitrairement fixé à des valeurs utiles aux véritables tables présentes dans le document.

## La gestion du numéro d'équation

Quand l'équation occupe toute la largeur de la page, `eqnarray` ne s'en rend pas compte et place le numéro d'équation en surimpression sur le texte. Les autres environnements standard, comme equation, ne présentent pas ce problème et placent le tag en-dessous (pour les deux premières lignes ci-dessous, est utilisé `eqnarray` avec `\setlength\arraycolsep{2pt}`; pour les deux suivantes, `align`).

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\setlength\arraycolsep{2pt}
\begin{eqnarray}
  a & = & b \\
    & = & cdefghijklmnopqrst
          uvwxyzcdefghijklmno
\end{eqnarray}
\vspace*{-0.5cm}
\begin{align}
  a & = b \\
    & = cdefghijklmnopqrst
        uvwxyzcdefghijklmno
\end{align}
\end{document}
```

## Le manque de comptabilité avec « amsmath »

Par ailleurs, `eqnarray` ne fonctionne pas correctement avec les commandes de l'extension {ctanpkg}`amsmath`, incontournable pour composer les mathématiques. Par exemple, les commandes `\tag` et `\intertext` fonctionnent avec tous les environnements sauf `eqnarray`.

## Ce qu'il existe pour la remplacer

### L'environnement « align »

Pour ces raisons, `eqnarray` doit être considéré comme obsolète et ne plus être utiliser. Il est avantageusement remplacé par les environnements de {ctanpkg}`mathtools` (ou de {ctanpkg}`amsmath`), notamment `align` défini pour répondre aux besoins des mathématiciens dont voici la syntaxe pour corriger l'exemple ci-dessus :

```latex
\begin{align}
  a & = b + c \\
  x & = y - z
\end{align}
```

### Un correctif ancien et partiel

