# Comment mettre en page un système d'équations ?

## Avec des commandes de base

Pour cela, vous pouvez utiliser les commandes `\left` et `\right` qui permettent d'obtenir des {doc}`délimiteurs extensibles </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>` et de les positionner autour d'un tableau, comme ici :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
%\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\[
\left \{
\begin{array}{c @{=} c}
    x & \sin a \cos b \\
    y & \sin a \sin b
\end{array}
\right.
\]
\end{document}
```

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` permet de placer des choix conditionnels, comme pour cet exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\begin{equation*}
  \|x\|=
     \begin{cases}
        -x & \text{si } x < 0 \\
        x  & \text{si } x \geq 0
     \end{cases}
\end{equation*}
\end{document}
```

## Avec l'extension « delarray »

L'extension {ctanpkg}`delarray` peut également être utilisée dans ce but car elle permet d'indiquer les délimiteurs entourant un tableau directement au niveau de la déclaration du format des colonnes. Dans l'exemple, ces délimiteurs sont respectivement « `\{` » (accolade ouvrante) et « `.` » (absence de délimiteur), à l'image de ce qu'il aurait fallu passer comme argument aux commandes `\left` et `\right` (pour avoir des {doc}`délimiteurs extensibles </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>`).

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{delarray}
\pagestyle{empty}
\begin{document}
\[
\|x\| =
\begin{array}\{ {cc}.
   -x & \textrm{si } x < 0 \\
    x & \textrm{si } x \geq 0
\end{array}
\]
\end{document}
```

## Avec l'extension « envmath »

L'extension {ctanpkg}`envmath` permet de numéroter le système ou chaque équation du système, le compteur pouvant alors être personnalisé. L'aspect esthétique n'est toutefois pas très bon dans certains cas.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{envmath}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
% numérotation du système
\begin{System}
  2x + y = 6 \\
  3x + y = 7
\end{System}

%numérotation de chaque équation du système
\begin{EqSystem}
  2x + y = 6 \\
  3x + y = 7
\end{EqSystem}
\end{document}
```

## Avec l'extension « cases »

L'extension {ctanpkg}`cases` gère les systèmes d'équations. Voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{cases}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\begin{numcases}{\|x\|=}
    -x, & si $x < 0$ \\
     x, & si $x \geq 0$
\end{numcases}
\end{document}
```

Le fichier `xtdeqnra.sty` est une adaptation de cette extension par Jérôme Laurens.

```latex
%  fichier xtdeqnra.sty             ver 1.1    Decembre 2004
%
%  Copyright (C) 2004 by Jerome LAURENS
%
%  Ces macros peuvent e^tre librement transmises, sans modification aucune
%  tant que cette notice est intacte.
%  Base' sur le paquetage cases.sty de D.Arseneau
%  et la commande \eqnarray de Leslie Lamport et de l'e'quipe LaTeX3.
%
%  On rajoute un parame`tre optionnel a` l'environnement eqnarray qui
%  est un delimiteur gauche
%
%  Exemple : source
%
%  \begin{eqnarray}[\{]
%   x_1 & = & valeur_1 \\
%   x_2 & = & valeur_2 \\
%   x_3 & = & valeur_3
%  \end{eqnarray}
%
%  Re'sultat
%
%               /  x_1 = valeur_1           (1)
%               |
%               <  x_2 = valeur_2           (2)
%               |
%               \  x_3 = valeur_3           (3)
%
%  Pour utiliser ce paquetage,
%  inclure  "\usepackage{xtdeqnra}"  apre`s \documentclass.
%
%  On peut utiliser l'environnement subequations pour avoir (1a), (1b) et (1c)
% - - - - -
%
\ProvidesPackage{xtdeqnra}[2004/12/10 version 1.1]
%  - sauvegarde de l'environnement eqnarray
\let\x@eqnarray\eqnarray
\let\endx@eqnarray\endeqnarray
%  - environnement de remplacement
\def\eqnarray{\@ifnextchar[%
  {\let\endeqnarray\endy@eqnarray\y@eqnarray}
  {\let\endeqnarray\endx@eqnarray\x@eqnarray}}
%
%  - nouvel environnement avec un de'limiteur a` gauche
\def\y@eqnarray[#1]%
{\gdef\@leftdelimiter{#1}
 \displaymath
  \setbox\tw@\vbox
    \bgroup
      \stepcounter{equation}%
      \def\@currentlabel{\p@equation\theequation}%
      \global\@eqnswtrue
      \m@th
      \everycr{}%
      \tabskip\@centering
      \let\\\@eqncr
      \halign to\displaywidth
        \bgroup
          \hskip \@ne\arraycolsep
          \hfil$\displaystyle
          \tabskip\z@skip{##}$%
          \@eqnsel
          &\global\@eqcnt\@ne
          \hskip \tw@\arraycolsep
          \hfil${##}$\hfil
          &\global\@eqcnt\tw@
          \hskip\tw@\arraycolsep
          $\displaystyle{##}$\hfil
          \unskip\hfil
          \tabskip\@centering% \unskip removes space if no explanations
                  &\global\@eqcnt\thr@@
          \hb@xt@\z@\bgroup\hss##\egroup
          \tabskip\z@skip
          \cr
}
\def\endy@eqnarray{%
          \@@eqncr
        \egroup % end \halign, which does not contain brace
      \global\advance\c@equation\m@ne
      \unskip\unpenalty
      \unskip\unpenalty
      \setbox\z@\lastbox % grab last line
      \nointerlineskip
      \copy\z@ % then put it back
      \setbox\z@\hbox{\unhbox\z@}%
      \global\dimen@i\wd\z@
    \egroup% end \vbox (box\tw@, box\z@ is restored to LHS)
  \hbox to\displaywidth{%
    \m@th % assemble the whole equation
    \hskip\@centering
    \hbox to\dimen@i{%
      $\displaystyle%
      \dimen@\ht\tw@
      \advance\dimen@\dp\tw@ % get size of brace
      \setbox\z@\hbox{$\mathord{\left\@leftdelimiter\vcenter
to\dimen@{\vfil}\right.}$}%
      \dimen@\wd\z@
      \kern-\dimen@
      \box\z@%
      \n@space % make brace
      $\hfil}
    \hskip\@centering % finished first part (filled whole line)
    \kern-\displaywidth$%
    \vcenter{\box\tw@}$% overlay the alignment
  }% end the \hbox
  \ifx\notag\undefined\else\notag\fi% fixed on 12/10/2004 to support amsmath
  \enddisplaymath
  \global\@ignoretrue
}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équations,système d'équations,délimiteurs
```

