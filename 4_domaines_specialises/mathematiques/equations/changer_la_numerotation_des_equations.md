# Comment changer le style de numérotation des équations ?

Pour modifier le style de numérotation, on peut redéfinir la commande `\theequation`. L'exemple qui suit numérote les équations avec des lettres majuscules :

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}
  \usepackage{mathtools}
  \pagestyle{empty}

\renewcommand{\theequation}{%
    \thesection \Alph{equation}%
}

\begin{document}
\section{Systèmes à une variable}
\begin{align}
x + 4 & = 0 \\
x & = 3
\end{align}

\section{Systèmes à deux variables}
\begin{align}
 x + 4 & = 0 \\
 8 - y & = 0
\end{align}
\end{document}
```

Ce second exemple permet de placer la numérotation entre crochets :

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}
  \pagestyle{empty}

\makeatletter
\renewcommand{\@eqnnum}{%
    {\normalfont\normalcolor%
    \theequation}}
\renewcommand{\theequation}{%
    [\arabic{equation}]}
\makeatother

\begin{document}
Voici un exemple de numérotation
placée entre crochets~ :
 \begin{equation}
    y - 2 = 0
 \end{equation}
\end{document}
```

## Afficher une numérotation liée aux paragraphes

Avec l'extension {ctanpkg}`amsmath`, la commande `\numberwithin` permet de numéroter les équations suivant le paragraphe auquel elles appartiennent. Il faut alors appeler : `\numberwithin{equation}{section}` dans le préambule.

## Afficher une numérotation liée aux sections ou aux annexes

Les extensions {ctanpkg}`seceqn` et {ctanpkg}`apeqnum` permettent pour la première de numéroter les équations par section et pour la seconde de numéroter individuellement les équations dans les annexes.

## Afficher un second niveau de numérotation

L'extension {ctanpkg}`deleq` permet de définir un label par groupe d'équations (4) en plus des labels individuels de type (4a), (4b), etc.

Par ailleurs, les environnements `subequations` (de l'extension {ctanpkg}`amsmath`) et `subeqnarray` (de l'extension {ctanpkg}`subeqnarray`) permettent de référencer différentes lignes d'un même groupe d'équations par des indices (3.a), (3.b), etc.

- avec {ctanpkg}`subeqnarray` une référence à une ligne particulière se fait alors par `slabel` au lieu de `label` ;
- avec `subequations`, une référence à une ligne particulière s'obtient alors par un `label` placé sur cette ligne alors qu'une référence au système d'équation s'obtient par un `label` placé immédiatement après `subequations`, comme le montre l'exemple suivant :

```latex
\documentclass{article}
  \usepackage[body={8cm,8cm}]{geometry}
  \usepackage{lmodern}
  \usepackage{mathtools}
  \pagestyle{empty}

\begin{document}
\begin{subequations} \label{E+gp}
  \begin{gather}
    x_1 x_2 + x_1^2 x_2^2 + x_3
    \label{E+gp1} \\
    x_1 x_3 + x_1^2 x_3^2 + x_2
    \label{E+gp2} \\
    x_1 x_2 x_3 \label{E+gp3}
  \end{gather}
\end{subequations}

Voir le système~1 et en particulier l'équation~1b.
\end{document}
```

:::{note}
C'est le `\\` qui incrémente le compteur des sous-références.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équations,numérotation,référence,label,ref
```

