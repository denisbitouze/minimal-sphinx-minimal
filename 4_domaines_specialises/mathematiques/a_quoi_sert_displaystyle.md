# À quoi sert la commande `\displaystyle` ?

La commande `\displaystyle` permet de forcer LaTeX à placer les {doc}`indices et les exposants </4_domaines_specialises/mathematiques/structures/indices/start>` comme s'il était en mode mathématique isolé. Cela peut alors induire une modification locale de l'interligne courant comme dans cet exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Elle permet de forcer \LaTeX{} à
gérer les indices et les exposants
comme s'il était en mode
mathématique isolé $\displaystyle
\sum_{i=0}^n x_i$. Cela peut alors
induire une modification locale de
l'interligne courant. Ce texte
contient un exemple probant. Le
changement d'interligne est bien
visible.
\end{document}
```

Vous pouvez comparer l'exemple ci-dessus avec l'exemple **sans `\displaystyle`** :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Elle permet de forcer \LaTeX{} à
gérer les indices et les exposants
comme s'il était en mode
mathématique isolé
$\sum_{i=0}^n x_i$. Cela peut alors
induire une modification locale de
l'interligne courant. Ce texte
contient un exemple probant. Le
changement d'interligne est bien
visible.
\end{document}
```

______________________________________________________________________

*Source :* [I have a question about the « \\displaystyle » command](https://tex.stackexchange.com/questions/323367/i-have-a-question-about-the-displaystyle-command).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mode mathématique hors texte,équation hors texte,mathématiques dans un paragraphe,mathématiques et lignes
```

