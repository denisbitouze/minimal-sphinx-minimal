# Comment insérer des mathématiques ?

LaTeX, comme TeX, propose deux façons d'insérer des mathématiques nommées les **modes mathématiques** : le mode hors-ligne et le mode en ligne.

Dans ces deux modes, LaTeX traite des formules mathématiques et change alors la présentation des éléments : par exemple, les caractères apparaissent en italique car ils sont considérés comme des variables. De même, la gestion des espaces et des retours à la ligne devient entièrement automatique. Si cette gestion automatique pose des difficultés, vous pouvez voir les questions :

- « {doc}`Comment gérer les espaces en mode mathématique ? </4_domaines_specialises/mathematiques/espaces_en_mode_mathematique>` » ;
- « {doc}`Comment placer du texte dans des mathématiques ? </4_domaines_specialises/mathematiques/composer_du_texte_en_mode_mathematique>` » ;
- « {doc}`Comment gérer les sauts de ligne dans des mathématiques placées dans le texte ? </4_domaines_specialises/mathematiques/coupures_de_lignes_en_mode_mathematique>` » (uniquement pour le mode en ligne comme le montre la suite de cette page).

Notez bien que LaTeX ne fait que présenter des mathématiques. Il n'a pas pour vocation de comprendre ce que vous écrivez : vous pouvez donc écrire des formules fausses sans que LaTeX y trouve matière à erreur.

## Avec le mode hors-ligne

Ce mode hors-ligne, appelé *displaystyle* en anglais, permet d'isoler une formule du texte qui l’entoure. En LaTeX, ce mode s'ouvre puis se ferme avec les commandes suivantes :

- soit `\[` et `\]` ;
- soit `\begin{displaymath}` et `\end{displaymath}`.

En `Plain` TeX, il existe une autre solution, à savoir utiliser `$$` et `$$`. Cependant, cette solution ne doit pas être employée avec LaTeX pour les raisons évoquées à la question « [Quels sont les arguments contre l'utilisation de « \$\$...\$\$ » ?](/4_domaines_specialises/mathematiques/arguments_contre_les_doubles_dollars) ».

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\pagestyle{empty}
\begin{document}
Résoudre l'équation~ : \[ 3 + 4 x = 0\]
Puis résoudre l'équation~ :
\begin{displaymath}
\sum_{k=0}^\infty y^k = 2
\end{displaymath}
\end{document}
```

## Avec le mode en ligne

Le mode en ligne sert à placer de courts morceaux de mathématiques dans le texte courant. En LaTeX, ce mode s'ouvre puis se ferme avec les commandes suivantes :

- `$` et `$` (solution pour `Plain` TeX mais qui fonctionne en LaTeX) ;
- `\(` et `\)` ;
- `\begin{math}` et `\end{math}`.

Voici un exemple d'insertion de mathématiques en ligne :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\pagestyle{empty}
\begin{document}
Résoudre l'équation~ : $2x-4 = 0$. Puis
l'équation~ : \(\sum_{k=0}^\infty y^k = 2 \). Enfin calculer
la valeur de \begin{math}x+y\end{math}.
\end{document}
```

La comparaison avec l'exemple du mode hors-ligne montre que les équations elles-même ne sont pas présentées de la même manière. Dans le cas du mode en ligne, LaTeX fait en sorte de respecter l'interligne et compacte la présentation de certaines éléments, dans le cas présent les éléments en indice et en exposant du symbole de sommation « Σ ». Ce comportement peut être modifié comme le montrent les questions :

- « [À quoi sert la commande « \\displaystyle » ?](/4_domaines_specialises/mathematiques/a_quoi_sert_displaystyle) » ;
- « {doc}`Comment positionner les limites des grands opérateurs ? </4_domaines_specialises/mathematiques/structures/operateurs/changer_la_position_des_limites_autour_des_sommes_et_integrales>` ».

## Sans oublier l'extension « mathtools »

Pour écrire des mathématiques sous LaTeX, l'extension {ctanpkg}`mathtools` est incontournable car elle va permettre d'obtenir beaucoup de fonctionnalités usuelles pour saisir des mathématiques, illustrées dans cette FAQ. Comme l'indique sa {texdoc}`documentation <mathtools>`, {ctanpkg}`mathtools` charge l'extension de référence sur le sujet des mathématiques, à savoir {ctanpkg}`amsmath` (évoquée à la question « {doc}`Que sont AMS-TeX et AMS-LaTeX ? </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>` »). Mieux, {ctanpkg}`mathtools` va encore un peu plus loin en apportant des correctifs à {ctanpkg}`amsmath`. De fait, vous pourrez trouver fréquemment des références à {ctanpkg}`amsmath` dans la littérature existante : remplacez-la par {ctanpkg}`mathtools` pour être sûr de bénéficier des fonctionnalités les plus à jour.

En LaTeX 2.09, vous pouvez utiliser {ctanpkg}`amstex`, {ctanpkg}`amsbsy` et {ctanpkg}`amsopn` mais il faut savoir que {ctanpkg}`amstex` est obsolète.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,mode mathématique,hors-ligne,en ligne
```

