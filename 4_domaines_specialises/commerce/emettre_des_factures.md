# Comment émettre des factures ?

- {ctanpkg}`facture`
- <https://github.com/guyzmo/facturation_latex>

## Mentions obligatoires sur une facture en France

:::{warning}
Les différents packages ont pu être écrits par leurs auteurs en fonction de leurs besoins propres. Les factures suivent des règles nationales, et certaines mentions sont obligatoires, en fonction du pays, pour que la facture soit recevable.
:::

En France, une facture doit obligatoirement comporter :

[Mentions obligatoires d'une facture.](https://www.economie.gouv.fr/entreprises/factures-mentions-obligatoires)

## QR-factures (pour la Suisse)

Le package {ctanpkg}`qrbill` permet de générer les [QR-factures utilisées en Suisse depuis 2020](https://fr.wikipedia.org/wiki/QR-facture), qui incluent un QR-code particulier, avec le dessin d'une croix suisse en leur centre. Il s'utilise avec `lualatex` ou `xelatex` (versions de 2020 et ultérieures), car il appelle {ctanpkg}`fontspec` [^footnote-1].

______________________________________________________________________

*Sources :*

- [Factures : les mentions obligatoires](https://www.economie.gouv.fr/cedef/facture-mentions-obligatoires),
- [Style Guide QR-bill](https://www.paymentstandards.ch/dam/downloads/style-guide-en.pdf).

```{eval-rst}
.. meta::
   :keywords: LaTeX,commerce,factures,documents administratifs
```

[^footnote-1]: Sa documentation indique cependant une façon de l'utiliser avec `pdflatex`, en redéfinissant la commande `\qrbillfont`.

