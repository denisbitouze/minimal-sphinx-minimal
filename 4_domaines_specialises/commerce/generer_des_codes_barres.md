# Comment générer des codes à barres ?

- Le package {ctanpkg}`ean` permet de créer des codes-barres à 8 ou 13 chiffres. Il marche aussi bien avec TeX qu'avec LaTeX, et utilise METAFONT.
- Le package {ctanpkg}`code128` permet également de faire des codes-barres sous TeX comme sous LaTeX. Il ne nécessite cependant pas l'utilisation de fontes particulières.
- Il existe également un package {ctanpkg}`barcodes`, qui permet, comme son nom l'indique, de faire des codes-barres.

## Pour les livres

- Le package {ctanpkg}`ean13isbn` est spécialement adapté pour l'édition de livres : il permet de configurer [l'ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number) du livre comme option du package, puis de le faire apparaître à différents endroits, y compris sous forme de code-barre sur la quatrième de couverture :

```latex
\documentclass{article}
  \usepackage[ISBN=978-2-7117-8662-6]{ean13isbn}
  \pagestyle{empty}

\begin{document}
\ISBN

\EANisbn

\EANisbn[SC5b]
\end{document}
```

Dans le cas où la couverture est préparée dans un fichier séparé du reste du livre, il est intéressant de mettre l'ISBN du livre à un unique endroit, dans un fichier `ean13isbn.cfg`, qui contiendra les deux lignes :

```latex
% !TEX noedit
\ProvidesFile{ean13isbn.cfg}
\setkeys{zwean}{ISBN=978-2-7117-8662-6}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,code à barres,gencode,CAB,EAN13
```

