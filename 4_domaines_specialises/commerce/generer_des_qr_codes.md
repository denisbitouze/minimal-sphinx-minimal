# Comment générer des QR codes ?

Les [QR codes](https://fr.wikipedia.org/wiki/Code_QR) (pour *Quick Response Code*) sont un type de code-barres en deux dimensions constitué de points noirs disposés dans un carré à fond blanc. L'agencement de ces points décrit l'information que contient le code. Ils peuvent être lus par un lecteur de code-barre ou un smartphone.

Le package {ctanpkg}`qrcode` permet de générer des QR-codes directement depuis LaTeX :

```latex
% !TEX noedit
\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{qrcode}

\begin{document}
  \qrcode{Données à coder}
\end{document}
```

qui donne :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{qrcode}
\pagestyle{empty}
\begin{document}
  \qrcode{Données à coder}
\end{document}
```

Dans la plupart des cas, c’est une URL que vous voudrez inclure dans le QR code. Par exemple :

```latex
% !TEX noedit
  \qrcode{https://www.gutenberg.eu.org/}
```

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{qrcode}
\pagestyle{empty}
\begin{document}
  \qrcode{https://www.gutenberg.eu.org/}
\end{document}
```

Note : si vous utilisez {ctanpkg}`hyperref`, `qrcode` mettra automatiquement un lien cliquable vers l'adresse en question, à moins de spécifier l’option `nolink` à la commande.

Il est possible de spécifier la hauteur du QR code, avec l’option `height` (sachant que les QR codes sont obligatoirement carrés...).

## Versions des QR codes

Vous noterez la différence de résolution entre les deux QR codes précédents. Ceci s’explique par le fait que la chaîne de caractère à coder dans le second exemple est plus longue que dans le premier. Les QR codes sont définis suivant différentes tailles de grille, chaque taille correspondant à une « version » : de 21×21 points (version 1) jusqu’à 177×177 points (version 40).

Ainsi, la version 1 permet de coder au maximum 25 caractères alphanumériques tandis que la version 40 peut en coder 4296.

Le package en question calcule automatiquement la version minimale à utiliser pour chaque chaîne (ça se voit dans le log de compilation), mais il est possible de forcer cette valeur (pour augmenter la version, évidemment), par exemple pour un aspect esthétique (si vous avez deux QR codes côte à côte) :

```latex
% !TEX noedit
  \qrcode[version=3]{Données à coder}
```

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{qrcode}
\pagestyle{empty}
\begin{document}
  \qrcode[version=3]{Données à coder}
\end{document}
```

## QR-factures

Le package {ctanpkg}`qrbill` permet de générer les [QR-factures utilisées en Suisse depuis 2020](https://fr.wikipedia.org/wiki/QR-facture), qui incluent un QR-code particulier, avec le dessin d'une croix suisse en leur centre. Il s'utilise avec `lualatex` ou `xelatex` (versions de 2020 et ultérieures), car il appelle {ctanpkg}`fontspec` [^footnote-1].

______________________________________________________________________

*Sources :*

- [Générer des QR codes directement dans LaTeX](https://blog.dorian-depriester.fr/latex/generer-des-qr-codes-directement-dans-latex),
- [Style Guide QR-bill](https://www.paymentstandards.ch/dam/downloads/style-guide-en.pdf).

```{eval-rst}
.. meta::
   :keywords: LaTeX,code-barre,QR code,URL,smartphone,QR-facture,QR-Rechnung
```

[^footnote-1]: Sa documentation indique cependant une façon de l'utiliser avec `pdflatex`, en redéfinissant la commande `\qrbillfont`.

