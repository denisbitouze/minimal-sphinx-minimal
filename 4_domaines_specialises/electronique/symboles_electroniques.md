# Comment obtenir des symboles de composants électroniques ?

Un symbole de composant électronique étant un cas particulier de circuit électronique, il est recommandé de voir aussi la question « {doc}`Comment dessiner des circuits électroniques ? </4_domaines_specialises/electronique/dessiner_des_circuits_electroniques>` ».

## Avec l'extension lcircuit

L'extension {ctanpkg}`lcircuit` définit, dans l'environnement `picture` de LaTeX, un certain nombre de symboles de circuits électroniques.

```{eval-rst}
.. meta::
   :keywords: LaTeX,schéma,condensateur,transistor,résistance,bobine
```

