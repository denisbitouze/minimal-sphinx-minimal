# Comment mettre en page un manuel de référence ?

## Avec la classe « refman »

La classe {ctanpkg}`refman` est faite pour ça. Vous pouvez ici consulter le {texdoc}`manuel de la classe refman <refman>` et l'{texdoc}`annexe concernant les classes refart et refrep <layout_e>`.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir`, et son {texdoc}`excellente documentation <memman>`, peut aussi être utilisée. Voir notamment {texdoc}`sa documentation sur les en-têtes de chapitres. <MemoirChapStyle>`

______________________________________________________________________

*Sources :*

- <https://tex.stackexchange.com/questions/3852/latex-template-for-a-technical-reference-manual-user-guide>

```{eval-rst}
.. meta::
   :keywords: LaTeX,classe,documentation technique
```

