# Comment créer des unités de mesure ?

- L'extension {ctanpkg}`siunitx` permet de mettre en forme de manière cohérente et unifiée les unités de mesure, que ce soit en mode texte ou en mode mathématique. L'extension utilise la police mathématique droite courante (par défaut). La façon d'afficher les nombres et les unités est largement configurable.

```latex
\documentclass{article}
\usepackage{siunitx}
\pagestyle{empty}
\begin{document}
\noindent\SI{12}{\kilogram}\\
\SI{12}{\kilogram}\\
\SI{5.8}{\meter\per\second}\\
\SI{1.23}{\joule\per\mole\per\Kelvin}\\
\SI{15}{\electronvolt}\\
\SI{89}{\giga\pascal}\\
\SI{658.4}{\tera\hertz}\\
\SI{25}{\degreeCelsius}
\end{document}
```

Les nombres sont traités en tenant compte des exposants, des nombres complexes et des multiplications :

```latex
\documentclass{article}
\usepackage{siunitx}
\pagestyle{empty}
\begin{document}
\noindent\num{12345,67890}\\
\num{1+-2i}\\
\num{.3e45}\\
\num{1.654 x 2.34 x 3.430}
\end{document}
```

L'extension est capable de reconnaître les unités écrites sous forme de texte comme sous forme de macros (dans ce dernier cas, différents formats sont possibles) :

```latex
\documentclass{article}
\usepackage{siunitx}
\pagestyle{empty}
\begin{document}
\noindent\si{kg.m.s^{-1}}\\
\si{\kilogram\metre\per\second}\\
\si[per-mode=symbol]{\kilogram\metre\per\second}\\
\si[per-mode=symbol]{\kilogram\metre\per\ampere\per\second}\\
\SI[per-mode=fraction]{1.345}{\coulomb\per\mole}
\end{document}
```

L'extension peut aussi représenter des gammes de valeurs :

```latex
\documentclass{article}
\usepackage{siunitx}
\pagestyle{empty}
\begin{document}
\noindent\numlist{10;20;30}\\
\SIlist{0.13;0.67;0.80}{\milli\metre}\\
\numrange{10}{20}\\
\SIrange{0.13}{0.67}{\milli\metre}\\
\end{document}
```

{octicon}`alert;1em;sd-text-warning` *Information obsolète. Ce qui suit est informatif.*

- Le problème du mode mathématique est qu'il change automatiquement de police et qu'il utilise une gestion particulière des espaces. Le mieux pour écrire des unités de mesures du type `m/s` sans avoir à taper systématiquement des barbarismes du type `\rm\,m/s\mit` ou `\,\textrm{m}/\textrm{s}` et de définir une commande spéciale dans le préambule du document.

Exemples avec {ctanpkg}`amsmath` :

Unités avec {ctanpkg}`amsmath`

```latex
% !TEX noedit
\newcommand{\units}[2]{#1\textrm{\thinspace #2}}
\units{10}{m/s}
```

Ou plus rapide :

Unités avec {ctanpkg}`amsmath`

```latex
% !TEX noedit
\newcommand{\units}[2]{#1\textrm{\thinspace #2}}
\newcommand{\ms}[1]{\units{#1}{m/s}}
 \ms{10}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

