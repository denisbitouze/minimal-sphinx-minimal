# Comment dessiner des diagrammes de Feynman ?

En physique théorique, un [diagramme de Feynman](https://fr.wikipedia.org/wiki/Diagramme_de_Feynman) est une représentation graphique d'équations mathématiques particulières décrivant les interactions des particules subatomiques dans le cadre de la théorie quantique des champs. Cet outil a été inventé par le physicien américain [Richard Feynman](https://fr.wikipedia.org/wiki/Richard_Feynman) à la fin des années 1940.

L'extension {ctanpkg}`feynman`, de Michael Levine, qui permettait de dessiner ces diagrammes sous LaTeX 2.09 est toujours disponible.

L'extension {ctanpkg}`feynmf`, de Thorsten Ohl, est conçue pour être utilisée avec les versions actuelles de LaTeX, et fonctionne en combinaison avec MetaFont (ou MetaPost, pour sa variante {ctanpkg}`feynmp <feynmf>`, fournie dans le même fichier `.tar.gz`). L'extension `feynmf` (ou `feynmp`) lit une description du diagramme écrite en TeX, et écrit le code. MetaFont (ou MetaPost) peut alors produire une police (ou un fichier PostScript) qui sera utilisée à la prochaine exécution de LaTeX. Si vous commencez un nouveau document et que vous avez accès à MetaPost, vous devriez sans doute préférer la version PostScript, pour des raisons de portabilité des documents, notamment. Mais la version MetaFont est toujours là pour vos vieux documents.

L'extension {ctanpkg}`axodraw`, de Jos Vermaseren, est mentionnée comme une alternative dans la documentation de {ctanpkg}`feynmf`, mais il est entièrement écrit sous forme de commandes `\special` pour `dvips`, et est donc assez peu portable.

Une approche alternative est implémentée par l'extension {ctanpkg}`feyn` de Norman Gray. Plutôt que de créer des diagrammes complets sous forme d'images PostScript, {ctanpkg}`feyn` fournit une police (en différentes tailles) contenant des fragments de diagramme, que vous pouvez assembler pour produire des diagrammes complets. Il propose des diagrammes assez simples qui s'intègrent bien dans les équations, plutôt que des diagrammes compliqués qui seraient plus adaptés dans des figures hors-texte.

______________________________________________________________________

*Sources :*

- {faquk}`Drawing Feynman diagrams in LaTeX <FAQ-drawFeyn>`,
- [Feynman Diagrams with feynMF and feynMP](https://tex.stackexchange.com/questions/181427/feynman-diagrams-with-feynmf-and-feynmp).

```{eval-rst}
.. meta::
   :keywords: LaTeX,physique,Richard Feynman,électrodynamique quantique
```

