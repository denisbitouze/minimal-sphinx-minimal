# Comment mettre en page des algorithmes ou des programmes ?

- Le package {ctanpkg}`algorithm2e` permet comme son nom l'indique d'écrire des algorithmes. Il a l'avantage d'avoir une présentation souple et d'être facilement extensible. En effet, les petites instructions conditionnelles peuvent être présentées sur une ligne et l'on peut facilement ajouter un filet sur le côté. Il dispose également de commandes telles que `\SetKw` qui permettent de rajouter facilement des mots-clés.

Voici un algorithme écrit avec {ctanpkg}`algorithm2e` :

```latex
\documentclass[french]{article}
  \usepackage[width=9cm]{geometry}
  \usepackage{lmodern}
  \usepackage[ruled,lined]{algorithm2e}
  \usepackage{babel}
  \pagestyle{empty}

\begin{document}
\begin{algorithm}
  \caption{Comment utiliser \LaTeX{} ?}
  \Entree{un utilisateur quelconque}
  \Sortie{un utilisateur connaissant \LaTeX{}}

  initialisation \;
  \Tq{pas à la fin de la FAQ}{
    l'utilisateur lit la section courante \;
    \eSi{comprise}{
      aller à la section suivante \;
      la section courante devient cette dernière \;
    }{
      revenir au début de cette section \;
    }
  }
\end{algorithm}
\end{document}
```

- Les packages {ctanpkg}`algorithm` et {ctanpkg}`algorithmic` disponibles dans {ctanpkg}`algorithms`, ont été spécifiquement conçus pour mettre en forme des algorithmes. {ctanpkg}`Algorithmic <algorithmic>` sert à décrire les algorithmes et {ctanpkg}`algorithm` fournit un environnement flottant semblable à `figure` ou `table` (voir {doc}`les pages à propose des flottants </3_composition/flottants/start>`). Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{algorithm,algorithmic}

\begin{document}
\begin{algorithm}
\caption{Un joli algorithme}
\begin{algorithmic}
\REQUIRE{habiter près des montagnes}
\REPEAT
 \IF{il fait beau}
  \STATE faire une randonnée
 \ELSE[il fait moche]
  \STATE résoudre P $\neq$ NP
 \ENDIF
\UNTIL{foulure de cheville}
\ENSURE{bobo}
\end{algorithmic}
\end{algorithm}
\end{document}
```

- Le package {ctanpkg}`frpseudocode`, d'Oliver Irwin, s'appuie sur {ctanpkg}`algorithmicx` (équivalent récent des packages présentés ci-dessus) en le françisant. Son ut est avant tout de fournir une traduction en français de termes utilisés dans les algorithmes, pour permettre leurt intégration dans un document en français. Il suffit de charger {ctanpkg}`frpseudocode`, puis d'utiliser les commandes habituelles de {ctanpkg}`algorithmicx`, puisque leur nom est conservé.
- Le package {ctanpkg}`algpseudocodex`, de Christian Matt, est lui aussi basé sur {ctanpkg}`algorithmicx`, dont il reprend la syntaxe, mais il lui ajoute de nombreuses fonctionnalités.
- Il existe aussi le package {ctanpkg}`alg`.
- Le package {ctanpkg}`newalg` propose un environnement `algorithm` qui, par défaut, utilise le mode mathématique et l'environnement `array` pour les alignements. La commande `\text` est également disponible. Le package gère les instructions : *if-then-else*, *for*, *while*, *repeat*, *switch* et propose un certain nombre de macros telles que *call*, *error*, *algkey*, *return*, *nil*.

Exemple d'utilisation de {ctanpkg}`newalg` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{newalg}

\begin{document}
\begin{algorithm}{StrictSup}{x, y}
  \begin{IF}{x > y}
    \RETURN x
  \ELSE
    \ERROR{x leq y}
  \end{IF}
\end{algorithm}
\end{document}
```

- Le package {ctanpkg}`program` permet de mettre en relief des mots clés, d'utiliser des mathématiques dans des algorithmes, etc.

```latex
\documentclass{article}
  \usepackage{program}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\begin{program}
\mbox{Exponentiation rapide :} \\
\BEGIN
  \FOR i:=1 \TO 10 \STEP 1 \DO
     |afficher|(|exp|(2,i)); \\ |newline|() \OD
\WHERE
\FUNCT |exp|(x,n) \BODY
          \EXP z:=1;
               \WHILE n \ne 0 \DO
                  \WHILE |pair|(n) \DO
                     n:=n/2; x:=x*x \OD;
                  n:=n-1; z:=z*x \OD;
               z \ENDEXP \ENDFUNCT
\END\label{fin}
\end{program}
\end{document}
```

:::{warning}
L'extension {ctanpkg}`program` doit être chargée après {ctanpkg}`amsmath` lors d'une utilisation conjointe de ces deux packages.
:::

- Le style `programs.sty` du package {ctanpkg}`progkeys` permet lui aussi d'utiliser des mathématiques et de mettre des mots-clefs en gras.

______________________________________________________________________

*Sources :*

- [Écrire des algorithmes en LaTeX](http://rouxph.blogspot.com/2014/08/ecrire-des-algorithmes-en-latex.html),
- [Présenter des algorithmes, morceaux de programmes avec LaTeX](http://revue.sesamath.net/spip.php?article1047).

```{eval-rst}
.. meta::
   :keywords: LaTeX,pseudocode,algorithme,programmation,algorithmique,informatique théorique
```

