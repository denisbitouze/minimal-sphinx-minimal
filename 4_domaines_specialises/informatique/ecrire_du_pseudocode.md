# Comment écrire des algorithmes en pseudocode ?

En programmation, le [pseudocode](https://fr.wikipedia.org/wiki/Pseudo-code) est une façon de décrire un algorithme d'une façon proche du langage naturel, sans référence à un langage de programmation en particulier.

Le pseudocode diffère des listings de programmes réels en ce qu'il n'a pas de syntaxe et de sémantique strictes. De plus, comme le pseudocode est censé exprimer un algorithme de manière claire, il peut avoir besoin d'incorporer des notations mathématiques, des figures, des tableaux et d'autres éléments LaTeX qui n'apparaissent normalement pas dans les langages de programmation conventionnels. La {doc}`mise en forme des programmes </4_domaines_specialises/informatique/inserer_du_code_source_informatique>` est décrite ailleurs.

Il n'y a pas une unique façon de mettre en forme du pseudocode, mais de nombreuses. Il existe par conséquent de nombreuses extensions LaTeX pour mettre en forme des algorithmes de façon lisible et jolie.

On pourrait certainement créer son propre environnement de composition de pseudocode en utilisant, par exemple, les environnements `tabbing` {doc}`décrits ici </3_composition/tableaux/tabulations/composer_un_tableau_avec_des_tabulations>`) ou `list` {doc}`décrits ici </3_composition/texte/listes/modifier_les_environnements_de_liste>`) --- ce ne serait pas très difficile pour commencer, mais on arriverait vite à un niveau de complexité rédhibitoire. Cela vaut donc la peine d'essayer les extensions suivantes, toutes conçues spécifiquement pour la mise en forme de pseudocodes.

:::{note}
Aucune de ces solutions n'est parfaite et universelle. Pour choisir entre elles, demandez-vous :

- quel style de sortie vous préférez-vous ;
- dans quelle mesure avez-vous besoin d'étendre ou de modifier l'ensemble des mots-clefs prédéfinis ;
- si vous avezvous besoin que les algorithmes flottent comme des figures ou des tableaux.
:::

## Avec les extensions de « algorithms »

Le package {ctanpkg}`algorithms` (qui contient les extensions {ctanpkg}`algorithm <algorithms>` et {ctanpkg}`algorithmic <algorithms>`, toutes deux nécessaires pour une utilisation normale) a une interface simple et produit des résultats plutôt jolis. Il fournit des primitives pour les déclarations, qui peuvent contenir des commandes LaTeX arbitraires, des commentaires, et diverses structures itératives et conditionnelles. Ces primitives peuvent facilement être redéfinies pour produire une sortie différente. Cependant, il n'y a pas de support pour ajouter de nouvelles primitives.

La mise en forme du pseudocode à proprement parler est effectuée par l'extension {ctanpkg}`algorithmic <algorithms>`; l'extension {ctanpkg}`algorithms` utilise, elle, utilise les fonctionnalités de l'extension {ctanpkg}`float` pour numéroter les algorithmes de manière séquentielle, permettre aux algorithmes de {doc}`flotter </3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux>` comme des figures ou des tableaux, et produire « Liste des algorithmes » en début de document.

Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{algorithm,algorithmic}

\begin{document}
\begin{algorithm}
\caption{Un joli algorithme}
\begin{algorithmic}
\REQUIRE{habiter près des montagnes}
\REPEAT
 \IF{il fait beau}
  \STATE faire une randonnée
 \ELSE[il fait moche]
  \STATE résoudre P $\neq$ NP
 \ENDIF
\UNTIL{foulure de cheville}
\ENSURE{bobo}
\end{algorithmic}
\end{algorithm}
\end{document}
```

## Avec les extensions de « algorithmicx »

Les extensions fournies par le package {ctanpkg}`algorithmicx` sont similaires à {ctanpkg}`algorithmic <algorithms>`, tant par leurs principes de base que par leur résultat final, mais elles permettent d'ajouter de nouveaux mots-clefs et de modifier la mise en forme. Il y a notamment l'extension {ctanpkg}`algpseudocode <algorithmicx>` qui est (presque) un remplacement direct d'{ctanpkg}`algorithmic <algorithms>`. Une autre extension du package, {ctanpkg}`algpascal <algorithms>`, utilise des mots-clefs de type [Pascal](<https://fr.wikipedia.org/wiki/Pascal_(langage)>), indente le code différemment de {ctanpkg}`algpseudocode <algorithmicx>` et formatte les arguments des commandes en mode mathématique plutôt qu'en mode texte. Il n'y a pas d'environnement flottant mais {ctanpkg}`algorithmicx`, comme {ctanpkg}`algorithmic <algorithms>`, est compatible avec {ctanpkg}`algorithm <algorithms>`

:::{warning}
Apparemment, il y a des problèmes pour définir de nouvelles commandes, mais l'extension n'est plus activement maintenue.

Faites quelques tests avant de vous appuyer sur {ctanpkg}`algorithmicx` pour un gros projet.
:::

### Extensions dérivées de « algorithmicx »

- L'extension {ctanpkg}`frpseudocode`, d'Oliver Irwin, s'appuie sur {ctanpkg}`algorithmicx` (équivalent récent des extensions présentées ci-dessus) en la françisant. Son but est avant tout de fournir une traduction en français de termes utilisés dans les algorithmes, pour permettre leur intégration dans un document en français. Il suffit de charger {ctanpkg}`frpseudocode`, puis d'utiliser les commandes habituelles de {ctanpkg}`algorithmicx`, puisque leur nom est conservé.
- L'extension {ctanpkg}`algpseudocodex`, de Christian Matt, est elle aussi basée sur {ctanpkg}`algorithmicx`, dont elle reprend la syntaxe, mais elle lui ajoute de nombreuses fonctionnalités.

## Avec l'extension « algorithm2e »

L'extension {ctanpkg}`algorithm2e` est très ancienne, mais encore largement utilisée et recommandée. Elle a l'avantage d'avoir une présentation souple et d'être facilement extensible. Les petites instructions conditionnelles peuvent être présentées sur une ligne, pour une écriture compacte, et l'on peut facilement ajouter un filet sur le côté. La commande `\SetKw` permet d'ajouter facilement des mots-clefs.

Elle charge l'extension {ctanpkg}`float` pour fournir formatter les algorithmes comme des flottants, mais vous pouvez toujours utiliser l'option `H` de {ctanpkg}`float` pour que l'algorithme apparaisse « là où vous l'avez écrit ».

Voici un algorithme écrit avec {ctanpkg}`algorithm2e` :

```latex
\documentclass[french]{article}
  \usepackage[width=9cm]{geometry}
  \usepackage{lmodern}
  \usepackage[ruled,lined]{algorithm2e}
  \usepackage{babel}
  \pagestyle{empty}

\begin{document}
\begin{algorithm}
  \caption{Comment utiliser \LaTeX{} ?}
  \Entree{un utilisateur quelconque}
  \Sortie{un utilisateur connaissant \LaTeX{}}

  initialisation \;
  \Tq{pas à la fin de la FAQ}{
    l'utilisateur lit la section courante \;
    \eSi{comprise}{
      aller à la section suivante \;
      la section courante devient cette dernière \;
    }{
      revenir au début de cette section \;
    }
  }
\end{algorithm}
\end{document}
```

## Avec l'extension « alg »

L'extension {ctanpkg}`alg` offre, comme {ctanpkg}`algorithms`, un environnement d'algorithmes flottants avec toutes les subtilités qui en découlent. Cependant, {ctanpkg}`alg` peut légender ses flottants dans diverses langues, dont le français. En outre, {ctanpkg}`alg`, contrairement à l'extension {ctanpkg}`algorithms`, permet d'ajouter facilement de nouvelles structures.

## Extensions développées pour des ouvrages : « newalg », « clrscode\[3e\] » & « pseudocode »

L'extension {ctanpkg}`newalg` a une interface assez similaire à celle de {ctanpkg}`algorithms`, mais sa sortie vise à imiter la mise en forme plutôt agréable utilisée dans le livre "{isbn}`Introduction to Algorithms <978-0-072-97054-8>`" de Corman, Leiserson, Rivest et Stein ([le livre est consultable ici](https://edutechlearners.com/download/Introduction_to_algorithms-3rd%20Edition.pdf), en anglais). Son environnement `algorithm` utilise le mode mathématique par défaut et l'environnement `array` pour les alignements; vous pouvez utiliser la commande `\text` pour sortir du mode mathématique.

L'extension connaît les instructions : *if-then-else*, *for*, *while*, *repeat*, *switch* et propose un certain nombre de macros telles que *call*, *error*, *algkey*, *return*, *nil*.

Malheureusement, {ctanpkg}`newalg` ne propose pas d'environnement flottant ni d'options pour réellement personnaliser la mise en forme.

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{newalg}

\begin{document}
\begin{algorithm}{StrictSup}{x, y}
  \begin{IF}{x > y}
    \RETURN x
  \ELSE
    \ERROR{x leq y}
  \end{IF}
\end{algorithm}
\end{document}
```

Si vous vous méfiez des imitations, l'extension LaTeX développée pour "{isbn}`Introduction to Algorithms <978-0-262-03384-8>`" a été publiée par Thomas Cormen :

- {ctanpkg}`clrscode` pour la deuxième édition de l'ouvrage,
- {ctanpkg}`clrscode3e` pour la troisième édition.

De même, le style utilisé dans "{doi}`Combinatorial Algorithms : Generation, Enumeration and Search <10.1145/309739.309744>`" est fourni par l'extension {ctanpkg}`pseudocode <algorithmicx>`, écrite par les auteurs du livre. Elle offre un style courant « de type Pascal », et quelques structures intéressantes supllémentaires pour ce qui ressemblerait à des blocs en Pascal.

## Avec l'extension « program »

L'utilisation de l'extension {ctanpkg}`program` est un peu différente de celle des autres. Elle compose les programmes en mode mathématique plutôt qu'en mode texte, et les sauts de ligne sont significatifs. {ctanpkg}`Program <program>` ne propose pas d'environnement de flottant, mais elle peut numéroter les algorithmes, {ctanpkg}`alg` et {ctanpkg}`algorithms`. Il n'y a pas vraiment de possibilité de personnalisation ou d'ajout de fonctionnalités. La {texdoc}`documentation de « program » <program>` est très succincte, mais un fichier d'exemple, `program-demo.tex`, est fourni avec l'extension et présente de nombreux cas d'usage.

```latex
\documentclass{article}
  \usepackage{program}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\begin{program}
\mbox{Exponentiation rapide :} \\
\BEGIN
  \FOR i:=1 \TO 10 \STEP 1 \DO
     |afficher|(|exp|(2,i)); \\ |newline|() \OD
\WHERE
\FUNCT |exp|(x,n) \BODY
          \EXP z:=1;
               \WHILE n \ne 0 \DO
                  \WHILE |pair|(n) \DO
                     n:=n/2; x:=x*x \OD;
                  n:=n-1; z:=z*x \OD;
               z \ENDEXP \ENDFUNCT
\END\label{fin}
\end{program}
\end{document}
```

:::{warning}
L'extension {ctanpkg}`program` doit être chargée après {ctanpkg}`amsmath` lors d'une utilisation simultanée.
:::

## Avec l'extension « progkeys »

- Le style `programs.sty` du package {ctanpkg}`progkeys` permet lui aussi d'utiliser des mathématiques et de mettre des mots-clefs en gras.

______________________________________________________________________

*Sources :*

- {faquk}`Typesetting pseudocode in LaTeX <FAQ-algorithms>`,
- [Écrire des algorithmes en LaTeX](http://rouxph.blogspot.com/2014/08/ecrire-des-algorithmes-en-latex.html),
- [Présenter des algorithmes, morceaux de programmes avec LaTeX](http://revue.sesamath.net/spip.php?article1047).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,pseudo-code,pseudocode,algorithme,programmation,algorithmique,informatique théorique
```

