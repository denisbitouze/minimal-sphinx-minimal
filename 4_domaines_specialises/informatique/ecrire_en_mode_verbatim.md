# Comment écrire en mode verbatim ?

Dans certains domaines, notamment en informatique, on a besoin de citer du texte comportant beaucoup de {doc}`caractères spéciaux pour LaTeX </3_composition/texte/symboles/caracteres/caracteres_reserves>` (`&`, `_`, `\`...). Le cas extrême se présentera si vous écrivez un document **sur** LaTeX **en** LaTeX. Vous aurez besoin de citer des noms de commandes, de donner des bouts de code...

Dans le jargon LaTeX, citer du texte sans interpréter ce qu'il contient se dit « écrire en verbatim », ce qui veut dire [mot pour mot](https://fr.wikipedia.org/wiki/Verbatim) en latin.

- Les environnements `verbatim` et `verbatim*` de LaTeX permettent de réaliser cela. Cependant, ils sont assez limités, en particulier sur la taille de ce que peuvent contenir ces environnements, et de nombreuses extensions ont été proposées.
- Le package {ctanpkg}`verbatim` redéfinit les environnements `verbatim` et `verbatim*`, afin de ne plus être limité par la taille du contenu. Il fournit également la commande `\verbatiminput`, qui prend en argument un nom de fichier, et inclut ce fichier en mode verbatim.
- L'environnement `alltt` du package du même nom {ctanpkg}`alltt` (package dû à Johannes Braams) est une autre solution. À l'intérieur de cet environnement, le caractère `\` et les accolades conservent leur signification, et il est donc possible d'inclure des commandes LaTeX dans un environnement `alltt`. Cela permet en particulier d'inclure un fichier en verbatim, à condition qu'il ne contienne pas d'accolades.

Exemple d'utilisation de {ctanpkg}`alltt` :

```latex
\documentclass{article}
  \usepackage{alltt}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{alltt}
 Notez la différence subtile entre
 $f(x)$ et \(f(x)\) grâce à
 l'utilisation de la \og{}contre-oblique\fg.
\end{alltt}
\end{document}
```

- L'extension {ctanpkg}`moreverb` va un peu plus loin, en permettant notamment de numéroter les lignes de l'environnement `verbatim`. Elle permet aussi d'inclure un fichier, bien sûr, mais aussi et surtout d'écrire dans un fichier.

Pour inclure un fichier, on saisira par exemple :

```latex
% !TEX noedit
{ \small \listinginput[5]{10}{totor.c} }
```

Les paramètres `5` et `10` indiquent que la numérotation des lignes doit se faire de 5 en 5 en commençant à 10.

- L'extension {ctanpkg}`fancyvrb` permet d'écrire du texte verbatim encadré, en couleur et même dans lequel certaines commandes sont interprétées. {doc}`Voir cette page </3_composition/texte/paragraphes/texte_verbatim>` pour un exemple.
- L'extension {ctanpkg}`sverb` de Mark Wooding, propose un environnement `listing`.
- L'extension {ctanpkg}`verbtext`, permet de numéroter les lignes d'un texte verbatim, et d'interpréter certaines commandes.
- On peut aussi essayer l'extension {ctanpkg}`verbasef` (*verbatim automatic segmentation of external files*). Il utilise l'environnement `figure`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,verbatim,écrire du code,inclure du code,programmation
```

