# Comment écrire des parties d'échecs ?

- L'extension {ctanpkg}`skak` permet à la fois d'afficher des positions d'échecs :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{skak}

\begin{document}
\paragraph{}
 Petit exercice d'entraînement :
 les blancs jouent et font mat en trois coups.
 \fenboard{7k/4K1pp/7N/8/8/8/8/B7 w - - 0 1}
 \begin{center}
   \showboard
 \end{center}

 Vous ne voyez pas ? Voici la solution :
 \mainline{1. Bf6 gxf6 2. Kf8 f5 3. Nf7\mate}

\paragraph{}
 À vous de jouer maintenant !
 Voici un autre problème où les blancs font
 mat en trois coups.
 \fenboard{7B/8/8/8/8/N7/pp1K4/k7 w - - 0 1}
 \begin{center}
   \showinverseboard
 \end{center}
\end{document}
```

- Une autre extension, {ctanpkg}`chess`, est beaucoup plus ancienne et un peu moins souple, mais permet globalement de faire la même chose.

```{eval-rst}
.. meta::
   :keywords: LaTeX et loisirs,jeux d'échecs,partie d'échecs,échiquier
```

