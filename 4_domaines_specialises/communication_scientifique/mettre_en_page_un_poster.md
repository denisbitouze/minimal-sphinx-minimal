# Comment faire un poster ?

Dans la communauté scientifique, un « poster » est un moyen de présenter ses projets et ses résultats lors d'une conférence. C'est une grande feuille de papier (généralement de 1 mètre par 70 cm, parfois A0), sur laquelle on explique ses idées par des dessins, des photos, quelques lignes de texte, éventuellement des équations... Les posters des participants à la conférence sont affichés à proximité de l'amphithéâtre, et pendant les pauses, chacun peut les regarder. C'est une façon d'engager la discussion entre confrères.

Il n'existe pas de « solution toute faite » pour créer un poster avec LaTeX, comme il peut en exister pour préparer des articles ou des présentations. Un poster est constitué d'une unique page, avec des éléments disposés de façon assez libre et, pour être honnête, si LaTeX permet de le faire, ce n'est pas son but premier. Néanmoins, sa qualité typographique reste optimale même en grand format, et si vous avez déjà des textes et des figures au format LaTeX, ça vous fera sans doute gagner du temps de les réutiliser directement pour préparer votre poster.

La solution la plus complète est la classe {ctanpkg}`sciposter`, qui fournit les moyens de produire rapidement de bonnes affiches, dans le style voulu par son auteur (succession de colonnes). Un exemple de poster complet est fourni avec la classe. Son développement s'est arrêté en 2006.

Sinon, il existe toute une série d'outils, dont la plupart sont basés sur la classe {ctanpkg}`a0poster`, qui permet de créer le document à la taille appropriée, de régler la taille des polices de même, vous laisse ensuite à vos propres moyens.

Après avoir utilisé {ctanpkg}`a0poster`, vous pouvez bien sûr aller au plus simple et écrire votre affiche comme un document LaTeX sans ornement (probablement en plusieurs colonnes, avec l'extension {ctanpkg}`multicol`), mais si vous voulez un peu plus de liberté et d'expressivité, essayez d'utiliser {ctanpkg}`textpos` pour positionner les blocs de texte, les tableaux et les figures sur la page.

Plus sophistiqué est le paquet {ctanpkg}`flowfram`, dont le but fondamental est de faire passer le texte d'un bloc à l'autre sur la page. L'un des objectifs de son développement semble justement avoir été la production de posters, et un {texdoc}`exemple complet <sample-poster>` est fourni dans sa documentation. L'auteur de {ctanpkg}`flowfram` propose un outil expérimental appelé [FlowframTk (formerly called JpgfDraw)](https://www.dickimaw-books.com/software.html#flowframtk), qui permet de construire le contour des cadres à utiliser avec {ctanpkg}`flowfram`.

Le package {ctanpkg}`beamerposter` est ajouté à un document {ctanpkg}`beamer` pour permettre à l'utilisateur de travailler comme avec la classe {ctanpkg}`a0poster`. Ainsi, les possibilités puissantes de {ctanpkg}`beamer` pour positionner des éléments sur une page peuvent être utilisées pour préparer un poster. La documentation de {ctanpkg}`beamerposter` est succincte, mais un fichier d'exemple permet à l'utilisateur de se faire une idée de ce qui est disponible.

Malgré le peu d'outils disponibles, on trouve plusieurs pages web qui expliquent bien le processus de création d'un poster avec LaTeX (essentiellement avec {ctanpkg}`a0poster`) :

- par Norman Gray, [Producing posters using LaTeX](https://nxg.me.uk/docs/posters/) ;
- par Nicola Talbot, [Creating technical posters with LaTeX](https://www.dickimaw-books.com/latex/posters/) ;
- par Rob Clark [Advanced LaTeX Posters](https://web.archive.org/web/20190226042657/http://homepages.inf.ed.ac.uk:80/robert/posters/advanced.html) (which has links to code samples) ;
- par Brian Wolven, [LaTeX Poster Macros, Examples, and Accessories (Internet Archive)](https://web.archive.org/web/20040204003003/http://fuse.pha.jhu.edu/~wolven/posters.html) (this page also provides macros and other support suggestions).

______________________________________________________________________

- L'extension {ctanpkg}`poster` permet entre autres d'imprimer un texte s'étalant sur plusieurs pages avec des parties communes de manière à pouvoir recoller les morceaux après.
- Vous pouvez aussi utiliser l'extension {ctanpkg}`modernposter`, évolution de l'extension {ctanpkg}`a0poster`.
- Si vous utilisez déjà le style Beamer, vous pourriez être intéressé par l'extension {ctanpkg}`beamerposter`\].
- Les programmes {ctanpkg}`psresize <psutils>` de *PSutils* et {ctanpkg}`poster` permettent de redimensionner des fichiers Postscript ou de les répartir sur plusieurs pages.

Voir aussi :

- [Les posters sous LaTeX](https://blog.dorian-depriester.fr/latex/les-posters-sous-latex),
- [Gemini : A Modern LaTeX Poster Theme](https://www.anishathalye.com/2018/07/19/gemini-a-modern-beamerposter-theme/)

______________________________________________________________________

*Sources :*

- {faquk}`Creating posters with LaTeX <FAQ-poster>`,
- [Poster scientifique : la bonne formule](https://agentmajeur.fr/poster-scientifique/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,grand format,poster scientifique,affiche,communication scientifique,conférence
```

