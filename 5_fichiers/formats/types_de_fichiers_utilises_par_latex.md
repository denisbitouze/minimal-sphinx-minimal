# C'est quoi ces fichiers sources ?

- `tex` : commençons par le commencement, le fichier `tex` est le fichier qui contient le source du document que l'on crée.
- `web` : bien avant l'invention du World Wide Web, le WEB était un langage de programmation mélangeant, historiquement, du Pascal, pour le code, et du TeX, pour la documentation. Il existe de nos jours du CWeb, contenant du C à la place du Pascal, et quelques autres variantes pour les autres langages. On croise parfois le WEB dans le monde TeX parce que TeX est écrit en WEB, d'une part, et parce que le WEB produit des documentations en TeX, d'autre part.

:::{note}
Mais les fichiers `TEX` ne sont pas les seuls à contenir du code AllTeX. Ainsi, il y a des fichiers qui permettent de faire de la programmation littéraire : on écrit des commandes LaTeX dans les commentaires les programmes, puis on compile avec un outil adapté (Pascal fut le premier langage concerné, C, Scheme, OCaml).
:::

- `sty` : les fichiers de style ou packages contiennent un ensemble de macros que l'on peut utiliser dans les documents AllTeX.
- `cls` : les classes sont des types de documents LaTeX, typiquement les lettres, transparents, livres...
- `ins` : script d'installation pour un nouveau package.
- `dtx` : documentation et code d'un package.

## C'est quoi ces fichiers annexes ?

- `aux` : c'est un fichier texte contenant des informations auxiliaires utiles à la production du résultat, comme par exemple des informations numériques sur les étiquettes et les sections.
- `log` : c'est un compte rendu fait par TeX expliquant comment s'est passée la compilation (objets utilisés, messages d'avertissement...).
- `toc` : «Table Of Contents» est un fichier contenant la tables des matières qui sera affichée par `tableofcontents`.
- `lof` : «List Of Figures» contient la liste des figures du document, permettant de créer une table des figures.
- `lot` : «List Of Tables», contient l'équivalent du `lof` pour les tables.

## C'est quoi ces fichiers de bibliographie ?

- `bib` : c'est l'extension qu'on utilise pour écrire des fichiers contenant la bibliographie.
- `bst` : style de bibliographie, c'est la manière dont la bibliographie sera mise en forme.
- `bbl` : bibliographie triée, produite par BibTeX à partir du fichier `bib` et utilisable par TeX.
- `blg` : fichier `log` pour la bibliographie.

## C'est quoi ces fichiers d'index ?

- `idx` : fichier créé par le biais de la commande `makeindex` de TeX recensant tous les items à mettre dans l'index.
- `ind` : fichier créé par `makeindex` et utilisé par TeX lors de l'affichage de l'index.
- `ilg` : fichier `log` pour les index.
- `ist` : fichier de style d'index.

## C'est quoi ces fichiers de polices ?

- `tfm` : TeX font metric, un fichier dans lequel TeX peut lire les dimensions et les espacements à respecter pour chaque caractère d'une police donnée, ainsi que toutes les infos relatives au positionnement des caractères (ligatures, règles de choix des symboles de grande taille pour les maths, etc.).
- `gf` : «Generic Font», un fichier dans lequel TeX et ses affidés (logiciels de visualisation, d'impression, etc.) trouvent les dessins des caractères en bit à bit, pour un fichier `tfm`, il existe normalement un ou plusieurs fichiers `gf` (un par résolution prévu, classiquement un pour l'écran et un pour l'imprimante).
- `pk` : Packed, version plus compressée du fichier `gf`, le fichier `gf` est en fait tombé en désuétude et n'existe que de manière transitoire sur un système~ : quand le dessin d'une police manque à une résolution donnée, le fichier `gf` est créé à la volée, puis immédiatement converti en fichier `pk` pour économiser de la place.
- `mf`, `vf`, `fd` : Voir la question \\vref\{siglesfontes}.

## C'est quoi ces formats de sortie ?

- `dvi` : «DeVice Independent» est le format de fichier que produit AllTeX de manière naturelle comme le résultat de la compilation d'un document. Ce format est une coquille vide, il indique, pour chaque document, quelle fonte est utilisée (simplement son nom, le `dvi` ne contient pas de police) et, page par page, la position de chaque caractère. Un fichier `dvi` est donc inexploitable si l'on ne dispose pas des polices et des images qui ont été utilisées par le document.
- `ps` : Postscript, format de fichier compréhensible directement par certaines imprimantes, et souvent utilisé comme intermédiaire d'impression : `GhostScript` (un programme) est capable de lire du Postscript et de l'imprimer, il est classique de traduire un `dvi` (avec les polices associées) en Postscript pour imprimer ensuite avec `GhostScript`. Un fichier Postscript peut être vectoriel (*i.e.* indépendant de la résolution de l'imprimante) ou bitmap (\\emph{i.e.} intimement lié à la résolution de l'imprimante). Traditionnellement, TeX produit des fichier Postscript en bitmap en incluant directement dans le fichier Postscript le contenu du `dvi` et des différents `pk` utiles, ainsi que les images. Le fichier Postscript est normalement autonome.
- `pdf` : «Portable Document Format», format définit par Adobe, et de plus en plus répandu, comme le fichier `dvi`, le fichier PDF est indépendant de la résolution du support final (imprimante, écran, etc.), mais, lui, inclut les images et les polices, il est donc autonome. C'est ce que produisent pdfTeX et pdfLaTeX.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,Postscript
```

