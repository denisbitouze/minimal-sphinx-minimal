# À quoi sert la commande « `\special` » ?

TeX fournit des moyens d'exprimer des choses que les pilotes de périphériques comprennent, mais mais pas TeX lui-même. Par exemple, TeX ne sait pas comment inclure une figure PostScript dans un document, ni comment changer la couleur du texte, mais certains pilotes de périphériques le savent.

Ces actions sont décrites dans le document au moyen de commandes `\special`; tout ce que TeX fait avec ces commandes, c'est développer leur argument, puis transmettre le résultat au fichier DVI. Dans la plupart des cas, vous utiliserez des macros fournies par des extensions (souvent avec le pilote), qui s'occuperont elles-mêmes d'appeler `\special`, en plus d'autres tâches, pour avoir un résultat cohérent; par exemple, ça n'aurait guère de sens d'inclure une figure avec `\special` sans laisser de place pour elle dans la page. Donc les extensions {ctanpkg}`graphics` et {ctanpkg}`graphicx` se chargent de réserver de l'espace en plus d'insérer l'image avec `\special`, et peuvent aussi la pivoter ou la mettre à l'échelle. De même, les extensions {ctanpkg}`color` et {ctanpkg}`colorx` s'occupent de tout pour mettre votre texte en couleur, opération particulièrement délicate si vous deviez le faire à la main.

:::{note}
ConTeXt fournit directement les mêmes fonctionnalités, sans qu'il soit nécessaire de charger d'extensions.
:::

Les arguments autorisés pour `\special` dépendent du pilote de périphérique que vous utilisez. En dehors des exemples ci-dessus, il existe des commandes `\special` dans les pilotes emTeX (par exemple, `dvihplj`, `dviscr`, etc.) qui tracent des lignes avec des orientations arbitraires, et des commandes dans `dvitoln03` qui permettent de mettre la page en orientation paysage.

:::{warning}
`\special` se comporte de manière assez différente dans pdfTeX, puisqu'il n'y a pas de notion de « pilote de périphérique ». Il existe un concept similaire pour le format PDF, mais dans la plupart des cas, `\special` provoquera un avertissement lorsqu'il est utilisé dans pdfTeX, et n'aura pas l'effet désiré.
:::

______________________________________________________________________

*Sources :*

- {faquk}`« \\special » commands <FAQ-specials>`,
- [« \\specials » in pdftex](https://tex.stackexchange.com/questions/123301/specials-in-pdftex).

```{eval-rst}
.. meta::
   :keywords: LaTeX,générer du code PostScript,pilote DVI,commandes spéciales
```

