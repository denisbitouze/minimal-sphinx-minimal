# Comment convertir un fichier DVI en PostScript ?

## La solution standard : « dvips »

- Le meilleur logiciel open-source de conversion du format DVI vers PostScript, est {ctanpkg}`dvips`, de Tom Rokicki. Il est écrit en `C`, se compile sur de très nombreuses plate-formes et a l'avantage de savoir gérer les « virtual fonts ».

La page d'accueil officielle de `dvips` était sur le site de [Radical Eye Software](http://www.radicaleye.com/), mais elle renvoie maintenant vers [la documentation actuelle](http://tug.org/dvips/), hébergée sur le site du TUG. Tout le développement actuel est fait dans le cadre de la bibliothèque `kpathsea` de Karl Berry, et ses sources sont disponibles [dans le dépôt TeX Live](http://tug.org/svn/texlive/trunk/Build/source/texk/dvipsk/). Des versions sont proposées pour toutes les distributions TeX qui supportent PostScript.

### Comment utiliser « dvips » ?

Après avoir exécuté la commande `latex` sur votre document (disons `document.tex`), vous avez un fichier `document.dvi`. Il vous suffit maintenant d'exécuter :

```bash
dvips document
```

Cela créera le fichier PostScript `document.ps`.

Si le format ou l'orientation du papier utilisés par le document ne correspondent pas aux valeurs par défaut de votre installation LaTeX, vous pouvez les préciser à `dvips`, avec l'option `-t` :

```bash
# Pour utiliser du A4 :
dvips -t a4 document

# Pour pivoter les pages de 90° :
dvips -t landscape document

# Pour utiliser du A4 au format paysage :
dvips -t a4 -t landscape document
```

:::{important}
Le manuel complet de `dvips` (en anglais) est maintenu {doc}`au format texinfo </1_generalites/glossaire/qu_est_ce_que_texinfo>`. Pour l'afficher sous Linux, tapez :

```bash
info dvips
```

ou plus simplement consultez la version [PDF](http://tug.org/texlive/Contents/live/texmf-dist/doc/dvips/dvips.pdf) ou [HTML](http://tug.org/texinfohtml/dvips.html)

[Une page de manuel](<https://fr.wikipedia.org/wiki/Man_(Unix)>) existe, mais elle est incomplète.
:::

## Autres outils

- {ctanpkg}`dvipsk` a été le nom d'une version dérivée de {ctanpkg}`dvips`, dont le développement a été fusionné avec la version principale. Il n'a plus d'existence réelle. Sous Unix/Linux, `dvipsk` est maintenant un alias pour `dvips`.
- {ctanpkg}`dviout` est conçu pour Microsoft Windows. Il supporte divers systèmes d'impression, la plupart des formats de polices, et une vaste palette de commandes `\special`. Il est toujours développé (en 2021).
- {ctanpkg}`dvitops`, de James Clark, est maintenant considéré comme obsolète, car il ne supporte pas les fontes virtuelles.

______________________________________________________________________

*Sources :*

- {faquk}`DVI to PostScript conversion programs <FAQ-dvips>`,
- [Documentation de « dvips »](http://tug.org/texinfohtml/dvips.html).

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,Postscript,imprimer un fichier DVI,dvi2ps,DeVice Independent
```

