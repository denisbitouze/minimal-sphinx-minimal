# Comment sont gérés les fichiers PostScript dans LaTeX ?

Dans la chaîne de compilation utilisant LaTeX et `dvips`, chaque programme apporte sa contribution à la constitution du fichier {doc}`PostScript </5_fichiers/postscript/start>`.

## La partie effectuée par LaTeX

Lorsque le fichier LaTeX doit intégrer une figure PostScript (EPS), des commandes doivent être mises dans le fichier source (fichier TEX) pour dire à LaTeX que la figure doit être positionnée à telle place avec telles dimensions (en incluant la bonne extension : {ctanpkg}`graphicx`, {ctanpkg}`psfig`, {ctanpkg}`epsfig`).

Lors de la phase de compilation du code LaTeX, ce dernier va consulter dans le fichier externe contenant la figure les seules dimensions/proportions de l'image (les fameuses « bounding box » chères au PostScript encapsulé).

En fin de course, dans le fichier {doc}`DVI </5_fichiers/dvi/start>` généré par LaTeX se retrouvent le nom et les dimensions de l'image mais pas celle-ci (qui est toujours externe, comme précisé par la question « {doc}`Qu'est-ce qu'un fichier DVI ? </5_fichiers/dvi/qu_est_qu_un_fichier_dvi>` »). Aussi, en visualisant le fichier DVI, apparaît l'emplacement de l'image mais pas cette dernière (en particulier la commande `psdraft` génère un cadre similaire à `\fbox{...}` aux dimensions de l'image à l'emplacement de celle-ci).

## La partie effectuée par dvips

Suite au travail de LaTeX, `dvips` transforme le fichier DVI en PostScript et inclut à ce moment les fichiers images EPS. Ainsi, si ce document Postscript est visualisé ou imprimé, le texte et les images y sont bien affichés.

## Cas particuliers

Certains visualiseurs DVI, par exemple `xdvi` sous UNIX pour n'en citer qu'un, détectent dans le fichier DVI des passages tels que :

```latex
% !TEX noedit
PSfile="toto.eps" llx=0 lly=-1 urx=57 ury=29 rwi=4252
```

Ils appellent alors `GhostScript`/`GhostView` pour préparer une image bitmap que le visualiseur DVI va restituer. Mais d'autres visualiseurs, comme `dviwin`, ne savent pas que faire d'un fichier PostScript.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « Est-ce toujours vrai ? »
```

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,PostScript
```

