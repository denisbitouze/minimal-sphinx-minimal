# Qu'est-ce que le « PostScript encapsulé » (« EPS ») ?

PostScript a été pendant de nombreuses années la *lingua franca* des imprimantes professionnelles (bien que les imprimantes modernes haut de gamme tendent maintenant à demander un dialecte du format PDF à la place) ; comme PostScript est également un langage de programmation graphique puissant, il est couramment utilisé comme format de sortie pour les logiciels de dessin (entre autres).

Mais, justement *parce que* PostScript est un langage si puissant, certaines règles doivent être imposées, afin que le dessin de sortie puisse être inclus dans un document en tant que figure sans avoir d'effet de bord qui pourrait altérer le reste du document.

L'annexe H du [Manuel de référence du langage PostScript](https://www.adobe.com/content/dam/acom/en/devnet/actionscript/articles/PLRM.pdf) (deuxième édition et ultérieures), spécifie un ensemble de règles pour que le PostScript puisse être utilisé de cette façon pour des figures incluses dans un document. Voici les principales :

- certains « commentaires structurés » sont requis. Les plus importants sont l'identification du type de fichier et les informations sur la « *bounding box* » de la figure (c'est-à-dire le rectangle minimal qui la contient) ;
- certaines commandes sont interdites. Par exemple, la commande `showpage` ferait disparaître l'image dans la plupart des usages avec TeX ; et
- des « informations de prévisualisation » sont autorisées, ce qui permet aux logiciels qui ne savent pas décoder le PostScript (les traitements de texte, par exemple) de donner quand même un aperçu du résultat --- ces informations de prévisualisation peuvent être sous des formats variés, et tout programme de visualisation peut choisir de les ignorer.

Une figure PostScript conforme à ces règles est dite être au format « *Encapsulated PostScript* » (EPS) ou « PostScript encapsulé ». La plupart des outils (La)TeX permettant d'inclure du PostScript savent utiliser le format *Encapsulated PostScript* ; inversement, il n'est pas forcément facile d'utiliser les fichiers issus de logiciels ne respectant pas ces règles...

:::{tip}
Avec la généralisation des moteurs {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` et {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`, les formats PS et EPS ont perdu leur place de formats d'échange universels.

En effet, avec ces moteurs, si vous souhaitez inclure une image au format EPS, il vous faudra d'abord la {doc}`convertir en PDF </5_fichiers/pdf/generer_un_fichier_pdf_de_qualite>`, par exemple avec les outils `epspdf` ou `epstopdf`, en ligne de commande (sous Linux).
:::

______________________________________________________________________

*Sources :*

- {faquk}`What is "Encapsulated PostScript" ("EPS")? <FAQ-eps>`
- [Encapsulated PostScript](https://fr.wikipedia.org/wiki/Encapsulated_PostScript).

```{eval-rst}
.. meta::
   :keywords: LaTeX,format d'impression,graphiques PostScript,Encapsulated PostScript,inclure des images,inclure du PostScript
```

