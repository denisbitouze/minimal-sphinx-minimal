# Comment installer des fichiers « là où (La)TeX peut les trouver » ?

Beaucoup de documentations d'anciennes extensions demandaient de placer les fichiers «là où LaTeX peut les trouver», ce qui était parfaitement inutile pour le lecteur. S'il savait où c'était, ça ne servait à rien d'écrire ça, mais s'il ne le savait pas, ça ne lui apportait aucune information pour l'aider.

C'est de ce problème qu'est née l'idée de la TDS {doc}`TeX Directory structure </5_fichiers/tds/la_tds>`) : la questions «où mettre?» se résume maintenant à «où est l'arborescence TDS?».

Donc pour répondre à la question, il suffit de savoir :

- {doc}`quel arbre utiliser </5_fichiers/tds/la_tds3>`, et
- {doc}`où, dans l'arbre, mettre les fichiers </5_fichiers/tds/ou_installer_les_packages>`.

Une fois que l'on connaît la réponse à ces deux questions, et que l'on a créé tous les répertoires nécessaires, il suffit de copier les fichiers à leur emplacement exact.

Petit bémol : une fois ceci fait, LaTeX (ou autre) *a accès* aux fichiers, mais, pour qu'il les trouve, il faut aussi mettre à jour un fichier d'index.

Sur un système MiKTeX, ouvrez la fenêtre `Start` \$\\rightarrow\$ `All Programs` \$\\rightarrow\$ `MiKTeX <version>` \$\\rightarrow\$ `Settings`, et cliquez sur `Refresh FNDB`. Le travail peut également être effectué en ligne de commande, en utilisant :

```bash
initexmf --update-fndb
```

La [documentation de MiKTeX](http://docs.miktex.org/manual/initexmf.html) donne davantage de détails à propos de `initexmf` (en anglais).

Sur un système basé sur TeX Live (ou son prédécesseur teTeX), utilisez la commande `texhash` (ou si elle n'est pas disponible, `mktexlsr`; ce sont en fait deux noms différents pour le même programme).

Après avoir fait tout cela, les nouveaux fichiers seront disponibles pour être utilisés.

:::{tip}
Si vous devez installer une extension pour un simple test, il peut être suffisant de copier ses fichiers dans le répertoire courant de votre document.
:::

:::{note}
TeX Live et MiKTeX ont maintenant chacune un outil d'installation des extensions. Si l'extension que vous souhaitez installer est disponible sur CTAN, commencez par regarder si vous pouvez la faire installer automatiquement par votre distribution :

- {doc}`Comment installer une extension avec TeX Live Manager? </6_distributions/installation/texlive>`
- {doc}`Comment installer une extension avec MiKTeX package manager? </6_distributions/installation/miktex>`

Vous n'aurez ainsi pas à vous poser de questions, et les mises à jour seront assurées automatiquement dans le futur.
:::

______________________________________________________________________

*Source :* {faquk}`Installing files "where (La)TeX can find them" <FAQ-inst-wlcf>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation,fichiers
```

