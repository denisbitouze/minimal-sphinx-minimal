# Comment corriger les réglages d'Adobe Reader ?

L'impression depuis [Adobe Reader](https://fr.wikipedia.org/wiki/Adobe_Acrobat) réduit la taille des pages du document à celle du papier, par défaut. Malheureusement, son calcul ne tient pas compte des marges existantes du document, de sorte qu'il réduit ce qu'il croit être votre page entière sur ce qu'il croit être sa page de sortie. L'effet ressemble généralement à une augmentation des marges.

Ce problème se résoud en ajustant la valeur par défaut de `Reader` dans la boîte de dialogue d'impression. Malheureusement, cette boîte de dialogue varie d'une version à l'autre :

- Reader version 7 : sur la mise à l'échelle de la page (par défaut : « Ajuster aux marges de l'imprimante »), passez à « Aucun » et, sur la valeur de cette mise à l'échelle (par défaut 95% de la taille normale), passez à « 100% ».
- Adobe Reader 6 : dans la boîte de dialogue d'impression, dans le volet « copies & pages », vous trouverez une liste déroulante intitulée « Mise à l'échelle de la page ». Changez sa valeur en « Aucun ».
- Acrobat (Reader) 5 : dans la boîte de dialogue d'impression, assurez-vous que la case « Réduire les pages surdimensionnées pour qu'elles s'adaptent » est décochée. Il peut également être utile de décocher la case « Agrandir les petites pages pour s'adapter au format du papier ».

```{eval-rst}
.. todo:: *La traduction utilise sans doute une terminologie incorrecte pour les noms des fenêtres et des boîtes de dialogues.*
```

______________________________________________________________________

*Source :* {faquk}`Adobe Reader messing with print size <FAQ-acroantics>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,Acrobat reader,Adober Reader,imprimer un fichier PDF,taille réduite,imprimer en taille normale
```

