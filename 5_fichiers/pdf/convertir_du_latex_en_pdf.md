# Comment générer un PDF à partir d'un document LaTeX ?

Il existe trois grandes manières pour générer un {doc}`PDF </5_fichiers/pdf/start>` avec LaTeX :

- l'utilisation d'un générateur PDF direct de type TeX tel que {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` ;
- la conversion directe d'un fichier {doc}`DVI </5_fichiers/dvi/start>` ;
- la voie de la « distillation » d'Adobe, qui transforme un document PostScript en document PDF.

Pour les documents simples sans hyperliens, vous pouvez soit :

- traiter le document directement en PDF avec {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>`'', `LuaTeX` ou ' {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>`. Ce sont les méthodes les plus couramment utilisées ;
- sur une machine Windows ou Macintosh avec les outils appropriés installés, envoyer votre sortie imprimable vers un générateur de PDF à la place d'un pilote d'imprimante. Cette solution n'est appropriée que pour les documents simples : les générateurs de PDF ne peuvent pas créer d'hyperliens ;
- traiter le document avec LaTeX pour obtenir un DVI puis générer un PDF directement à partir de ce DVI en utilisant `dvipdfm` ou `dvipdfmx` ;
- traiter le document de manière à générer une sortie PostScript, puis utiliser le programme `Adobe Distiller`. Bien qu'il n'y ait pas d'implémentation gratuite de toutes les fonctionnalités d' `Adobe Distiller`, toutes les versions récentes de [Ghostscript](https://www.ghostscript.com/) fournissent une mécanique approchante assez fiable (en se méfiant des problèmes liés aux {doc}`sorties associée à la chaine DVI-Postscript </5_fichiers/pdf/generer_un_fichier_pdf_de_qualite>`).

## La gestion des références croisées et hyperliens

Pour traduire toutes les références croisées LaTeX en liens PDF, vous avez besoin d'une extension LaTeX pour redéfinir les commandes internes. Il en existe deux pour LaTeX, toutes deux capables de se conformer à la {doc}`spécification HyperTeX </3_composition/document/comment_faire_un_document_hypertexte>` :

- {ctanpkg}`hyperref` de Heiko Oberdiek, que presque tout le monde utilise ;

- {ctanpkg}`hyper` de Michael Mehlich, qui n'a pas été mis à jour depuis 2000.

  {ctanpkg}`Hyperref` peut souvent déterminer comment générer de l'hypertexte à partir de son environnement, mais il existe un large éventail d'options de configuration que vous pouvez donner via `\usepackage`. L'extension peut fonctionner en utilisant les primitives `pdfTeX`, les commandes hyperTeX `\special` ou spécifiques au pilote DVI `\special`. Les programmes `dvips` et `DVIPSONE` peuvent traduire le DVI avec ces commandes `\special` en PostScript acceptable pour `Distiller`. De même, `dvipdfm` et `dvipdfmx` ont des commandes `\special` qui leur sont propres.

## Visualisation du PDF

Pour visualiser (et imprimer) les fichiers résultants, [Acrobat Reader](https://www.adobe.com/fr/acrobat/pdf-reader.html)'' d'Adobe est disponible pour un large éventail de plates-formes. Si ce lecteur n'est pas disponible, `Ghostscript` combiné à `gv` ou ''[gsview](http://www.ghostgum.com%20.au/) peut afficher et imprimer des fichiers PDF, tout comme `xpdf`.

Dans certaines circonstances, une application de visualisation basée sur [ghostscript](https://www.ghostscript.com/)'' est en fait préférable à `Acrobat Reader`. Par exemple, sur Windows, à la différence de ''[gsview](http://www.ghostgum.com.au/), `Acrobat Reader` verrouille le fichier `pdf` qu'il affiche : cela parasite le cycle traditionnel de modification d'un document LaTeX « Édition / Compilation / Visualisation » car le PDF doit être refermé pour être modifié.

## Cas particulier de Plain TeX

Si vous utilisez `Plain TeX`, les commandes d {doc}`Eplain </1_generalites/glossaire/qu_est_ce_que_eplain>` peuvent vous aider à créer des documents PDF avec des hyperliens. Il peut fonctionner en utilisant des primitives `pdfTeX` ou les commandes `\special` pour les pilotes DVI `dvipdfm`/`dvipdfmx`.

______________________________________________________________________

*Source :* {faquk}`Making PDF documents from (La)TeX <FAQ-acrobat>`

```{eval-rst}
.. meta::
   :keywords: LaTeX, compiler un document LaTeX, générer un PDF, produire un fichier PDF
```

