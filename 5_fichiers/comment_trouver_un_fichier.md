# Comment trouver des fichiers (La)TeX ?

Les distributions modernes de TeX contiennent un grand nombre de fichiers de toutes sortes, mais tôt ou tard, la plupart des gens ont besoin de trouver quelque chose qui n'est pas dans leur système actuel (ne serait-ce que parce qu'ils ont entendu dire qu'un bug a été corrigé et qu'ils ne veulent pas attendre la mise à jour à venir).

**Mais comment et où trouver les fichiers désirés?**

Les distributions modernes (TeX Live et MiKTeX, notamment) fournissent des moyens de mettre à jour votre installation par internet, et d'installer facilement des extensions manquantes. C'est la façon la plus simple d'obtenir un nouveau fichier : il suffit de trouver quelle extension de la distribution contient le fichier en question, et de demander à la distribution de le mettre à jour. Les façons de procéder sont différentes (les deux distributions ont évolué différemment), mais aucun n'est difficile --- voir les questions «{doc}`Comment installer MiKTeX? </6_distributions/installation/miktex>` » ou «{doc}`Comment installer TeX Live? </6_distributions/installation/texlive>` ».

Il existe cependant des extensions qui ne sont pas dans la distribution que vous utilisez (ou pour lesquels la distribution n'a pas encore été mise à jour pour proposer la version dont vous avez besoin).

Certaines sources, comme les réponses de cette FAQ, fournissent des liens vers des fichiers : ainsi, si vous avez appris l'existence d'une extension ici, vous devriez être en mesure de la récupérer sans trop de difficultés.

Sinon, le CTAN propose une recherche en texte intégral, sur son [serveur principal](https://ctan.org/), ainsi que des index par sujet et par auteur et un lien pour parcourir les archives elles-mêmes.

Par ailleurs, Google et les autres moteurs de recherche sont des outils à ne pas négliger. Entrez les mots-clefs de votre recherche, et vous pourrez sans doute trouver telle ou telle extension dont l'auteur n'a pas pris la peine de la déposer sur le CTAN.

Avec Google, l'utilisateur peut restreindre la recherche au CTAN en faisant écrivant

```text
site:ctan.org tex-archive ⟨mots clés à chercher⟩
```

dans le champ de recherche de Google. Vous pouvez également filtrer les résultats en utilisant le mécanisme de «recherche avancée» de Google; les autres moteurs de recherche disposent (probablement) de fonctionnalités similaires.

Certains utilisateurs évitent d'avoir à passer par le réseau pour leurs recherches en téléchargeant la liste de fichiers utilisée par les recherches de fichiers sur le web des archives. Ce fichier, `FILES.byname`, présente une liste unifiée des fichiers disponibles (sans les noms de répertoire et les liens croisés). Son compagnon `FILES.last07days` est également utile, pour garder un œil sur les changements sur le CTAN. Comme ces fichiers ne sont mis à jour qu'une fois par 24 heures, ça vaut le coup de les télécharger automatiquement chaque nuit avec [rsync](https://fr.wikipedia.org/wiki/Rsync).

______________________________________________________________________

*Source :* {faquk}`Finding (La)TeX files <FAQ-findfiles>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer,mettre à jour,chercher sur CTAN,moteurs de recherche,index,chercher un package,chercher une extension,chercher un paquetage
```

