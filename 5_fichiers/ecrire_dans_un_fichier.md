# Comment écrire dans un fichier pendant une compilation ?

## Avec des commandes de base

Les commandes `\newwrite`, `\openout`, `\write` et `\closeout`, utilisées dans cet ordre, permettent d'écrire dans un fichier pendant une {doc}`compilation </2_programmation/compilation/start>`. En voici un exemple :

```latex
% !TEX noedit
\newwrite\fichier
\openout\fichier=exemple.tex
\write\fichier{Ceci est un fichier compilable par \noexpand\LaTeX.}
\closeout\fichier
```

Normalement, LaTeX développe les commandes écrites dans le fichier généré (ici, `exemple.tex`). C'est la raison pour laquelle une commande `\noexpand` a été placée dans cet exemple : de cette façon, la commande `\LaTeX` n'est pas développée, elle est écrite telle quelle dans `exemple.tex` et ne sera interprétée qu'à la compilation de ce fichier.

## Avec les extensions « moreverb » ou « sverb »

Les extensions {ctanpkg}`moreverb` et {ctanpkg}`sverb` définissent des environnements nommés respectivement `verbatimwrite` et `verbwrite`. Ils permettent d'écrire dans un fichier du texte qui ne sera pas développé. L'exemple ci-dessous, utilisant l'extension {ctanpkg}`moreverb`, fait la même chose que l'exemple précédent :

```latex
% !TEX noedit
\begin{verbatimwrite}{exemple.tex}
Ceci est un fichier compilable par \LaTeX.
\end{verbatimwrite}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX, compilation, écriture, fichier
```

