# Que représente la TDS ?

- TDS est l'abréviation de « *TeX Directory Structure* » (« arborescence des répertoires de TeX ») ; c'est la façon standard d'organiser l'ensemble des fichiers liés à TeX sur votre ordinateur. L'intérêt de standardiser cette hiérarchie est que cela facilite l'installation et la mise à jour des packages, polices, etc. Cette hiérarchie concerne les parties invariantes de TeX. Elle est commune à la quasi-totalité des systèmes d'exploitation.

La TDS elle-même est publiée comme le résultat d'un {doc}`groupe de travail technique du TUG </1_generalites/gutenberg>`. Vous pouvez consulter le [Cahier GUTenberg n°44-45](http://cahiers.gutenberg.eu.org/fitem?id=CG_2004___44-45_83_0) sur le sujet, ou bien [la version de référence de la norme](https://tug.org/tds/) (en anglais, également disponible {ctanpkg}`sur le CTAN <tds>`).

- Plus concrètement, la plupart des distributions modernes stockent les fichiers utilisés par TeX, LaTeX et consorts dans un ou plusieurs répertoires (dossiers) « `texmf` » [^footnote-1]. Par exemple, si on garde les valeurs proposées par l'installeur de la distribution TeXlive 2020 sur un système Linux :
- `/usr/local/texlive/2020/` : répertoire réservé à la distribution ;
- `/usr/local/texlive/texmf-local/` : pour tous les utilisateurs de la machine, géré par l'administrateur ;
- `/home/alice/texmf` : juste pour l'utilisateur « alice ».

On peut aussi rencontrer des répertoires `texmf-dist` et `texmf-var`. L'idée est la même : séparer une arborescence de fichiers **fournis par la distribution** et une (série d') arborescence(s) de fichiers **locaux**.

Sur d'autres systèmes Unix, ces répertoires peuvent être sous `/usr/share/texmf` ou `/opt/texmf`. Ça ne change en rien le principe de ces arborescences, et la TDS permet d'organiser le contenu de chacun de ces répertoires. Elle dit que les polices de caractères doivent être rangées dans le sous-répertoire `fonts/`, les fichiers d'installation dans `source/`, la documentation dans `doc/`, les packages LaTeX dans `tex/latex`, les bibliographies dans `bibtex` et ainsi de suite. De plus amples détails [sont disponibles ici.](https://tug.org/tds/)

______________________________________________________________________

*Sources :*

- [TDS : une structure de répertoires pour les fichiers TeX](http://cahiers.gutenberg.eu.org/fitem?id=CG_2004___44-45_83_0), Cahier GUTenberg n°44-45 (2004).
- {faquk}`What is the TDS? <FAQ-tds>`
- [Spécification de la TDS](https://tug.org/tds/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX directory structure,concepts,installation de LaTeX,répertoire pour LaTeX
```

[^footnote-1]: Le nom « `texmf` » est recommandé par la TDS; il veut dire *TeX et Metafont*.

