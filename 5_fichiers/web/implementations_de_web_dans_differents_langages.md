# Existe-t-il d'autres implémentations de WEB ?

TeX est écrit dans le langage de programmation WEB qui est un outil mettant en œuvre le concept de {doc}`programmation lettrée </5_fichiers/web/literate_programming>`. L'implémentation originale de Knuth se trouve dans n'importe quelle distribution classique de TeX, mais les sources des deux outils (`tangle` et `weave`), ainsi qu'un manuel décrivant les techniques de programmation, peuvent être obtenus auprès de CTAN.

D'autres implémentations de WEB existent :

- `CWEB`, de Silvio Levy, un programme WEB adapté pour les programmes en C ;
- `FWEB`, de John Krommes, est une version pour le Fortran, Ratfor, C, C++, fonctionnant avec LaTeX. Il se base sur `CWEB` ;
- Spidery WEB, de Norman Ramsey, est compatible avec plusieurs langages incluant Ada, `awk` et C. Bien qu'il soit pas libre, il est utilisable sans frais. Il est désormais remplacé par `noweb` (toujours de Norman Ramsay) qui tient compte des leçons de l'implémentation de spidery WEB et s'avère plus simple tout en restant aussi puissant ;
- `Tweb` est une version de WEB pour les fichiers de commandes `Plain` TeX, utilisant `noweb` ;
- `SchemeWEB`, de John Ramsdell, est un filtre Unix qui traduit du SchemeWEB en une source LaTeX ou une source Scheme ;
- `APLWEB` est une version de WEB pour APL ;
- `FunnelWeb` est une version de WEB indépendante du langage de programmation. Une autre version indépendante du langage est `nuweb` (écrit en ANSI C).

______________________________________________________________________

*Source :* {faquk}`WEB systems for various languages <FAQ-webpkgs>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation lettrée,web
```

