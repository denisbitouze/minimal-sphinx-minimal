# Comment enlever les balises LaTeX d'un document ?

- Les programmes {ctanpkg}`detex` et {ctanpkg}`untex` permettent d'enlever toutes les commandes et séquences de contrôle LaTeX d'un document.

`detex` existe sous Unix, MS-DOS et MacOS mais il semble maintenant obsolète.

:::{warning}
`detex` enlève les appels de macros mais pas leurs arguments, ce qui donne des résultats pas toujours très propres avec des documents LaTeX. Mais `untex` dispose d'options qui permettent d'enlever les arguments et les noms des environnements.
:::

Une autre option est {ctanpkg}`l2a`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,convertir du LaTeX en texte brut,LaTeX vers ASCII
```

