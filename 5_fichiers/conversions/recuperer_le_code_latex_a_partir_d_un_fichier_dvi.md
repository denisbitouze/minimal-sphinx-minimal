# Comment récupérer le code (La)TeX d'un fichier DVI, PDF ou PS ?

Ce travail ne peut tout simplement pas être effectué automatiquement : DVI, PDF et PostScript sont des formats « finaux », censés ne pas être modifiés ultérieurement, et les informations d'origine du document ne sont pas conservées. Donc, si vous avez perdu votre source (La)TeX (ou si vous n'avez jamais eu le code-source d'un document sur lequel vous devez travailler), vous avez du pain sur la planche... Souvent, la meilleure stratégie consiste à retaper l'ensemble du document, ou en récupérer ce qui peut l'être par copier-coller, puis refaire la mise en forme manuellement. Bien entendu, cela dépend de la taille du document et de l'aisance de l'utilisateur.

Même avec une assistance automatique, il est peu probable que l'on puisse faire plus que récupérer du texte ; dans tous les cas, le balisage (La)TeX qui crée les effets typographiques du document devra être recréé à la main.

Si le fichier que vous avez est au format DVI, les techniques de {doc}`conversion de (La)TeX en ASCII </5_fichiers/conversions/convertir_du_latex_en_texte_brut>` sont utilisables. Pensez à {ctanpkg}`dvi2tty`, {ctanpkg}`crudetype` et {ctanpkg}`catdvi`. N'oubliez pas qu'il y aura probablement des problèmes pour récupérer les objets inclus (comme les figures PostScript incluses, qui ne figurent pas dans le fichier DVI lui-même), et que les formules mathématiques ne seront probablement pas converties facilement.

Pour récupérer le texte de fichiers PostScript, les outils `ps2ascii` (qui fait partie de la suite [Ghostscript](https://www.ghostscript.com/)) ou {ctanpkg}`pstotext` (qui appelle `Ghostscript`) sont disponibles. On peut essayer d'appliquer ces outils au PostScript dérivé d'un fichier PDF en utilisant `pdf2ps` (également fourni avec `ghostscript`), ou Acrobat Reader lui-même ; une alternative est `pdftotext`, qui est distribué avec [xpdf](http://www.xpdfreader.com/).

Adobe Acrobat (version 5 ou ultérieure) offre une autre possibilité à ceux qui veulent extraire le contenu d'un fichier PDF : vous pouvez baliser le fichier PDF pour en faire un document structuré, le convertir en XHTML bien formé et importer les résultats dans Microsoft Word (2000 ou ultérieur). De là, vous pouvez convertir le fichier en (La)TeX en utilisant l'une des techniques présentées dans la section « {doc}`Conversion de et vers (La)TeX </5_fichiers/conversions/autres_outils_de_conversion_de_fichiers_latex>` ».

Le résultat sera généralement (au mieux) mal balisé. Des problèmes peuvent également survenir en raison des encodages particuliers des polices TeX (notamment ceux des polices mathématiques), qu'Acrobat ne sait pas relier à leur représentation Unicode standard.

______________________________________________________________________

*Sources :*

- {faquk}`Retrieving (La)TeX from DVI, etc. <FAQ-recovertex>`
- [Can we convert DVI or PostScript into TeX?](https://tex.stackexchange.com/questions/46779/can-we-convert-dvi-or-postscript-into-tex/46786)

```{eval-rst}
.. meta::
   :keywords: LaTeX,retrouver le code d'origine,reconstruire le code-source
```

