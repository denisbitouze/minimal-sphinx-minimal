# Pourquoi des caractères disparaissent de mes figures avec pdfTeX ?

Admettons que vous ayez une figure au format PDF que vous voulez utiliser dans votre document avec pdfLaTeX. Lorsque vous compilez le document, pdfTeX se plaint de « *missing glyphs* » (« glyphes manquants »), et une partie (ou la totalité) du texte de la figure originale n'est plus visible : légendes, symboles...

Voici ce qui s'est passé :

- Votre fichier contenant la figure (disons `fig.pdf`) contient une police `font.pfb` intégrée,
- pdfTeX note qu'il a `font.pfb` sur le disque, et le charge à la place de la copie fournie par `fig.pdf`,
- Il s'avère que la copie contenue dans `fig.pdf` avait des glyphes qui ne sont pas dans le fichier `font.pfb` du disque, de sorte que vous obtenez des erreurs lors de la compilation et que vous voyez que des caractères sont manquants lorsque vous visualisez la sortie.

Le problème est que pdfTeX ne peut pas savoir que les polices sont différentes, puisqu'elles ont le même nom...

pdfTeX fait cela pour réduire la taille des fichiers : si votre document chargeait les figures `fig1.pdf` et `fig2.pdf` qui, toutes deux, utilisaient la police `font.pfb`, si pdfTeX ne faisait rien de particulier, il y aurait deux copies de `font.pfb` dans le fichier de sortie. Plus, peut-être, une troisième copie si votre document utilisait également la police...

:::{note}
Un cas réel est la police URW `NimbusRomNo9L-Regu` (un [clone de Times Roman](https://en.wikipedia.org/wiki/Nimbus_Roman_No._9_L)), qui est disponible dans une version avec des lettres cyrilliques, alors que la version fournie dans les distributions TeX n'a pas ces lettres. Les deux versions, en général, ont le même nom.
:::

La solution facile, « quick and dirty », est d'ajouter la commande

```latex
% !TEX noedit
\pdfinclusioncopyfonts=1
```

au préambule de votre document. De cette façon, pdfTeX conservera toutes les copies des polices, sans chercher à les dédoublonner.

La « vraie » solution est de renommer l'une ou l'autre des polices. Cela nécessite de reconfigurer les tables de polices d'un programme (TeX ou votre logiciel de dessin), ce qui est un évidemment laborieux.

______________________________________________________________________

*Source :* {faquk}`Characters disappear from figures in pdfTeX <FAQ-pdf-fig-chars>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,polices de caractères,caractères manquants,caractères perdus,inclusion de figure,dessin,illustration
```

