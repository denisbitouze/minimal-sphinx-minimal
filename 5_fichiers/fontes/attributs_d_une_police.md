# Quels sont les attributs d'une police ?

Une police possède cinq attributs :

- une famille,
- un codage,
- une série,
- une forme,
- une taille.

## La famille

La famille (*family*) correspond à l'allure générale de la police. Par défaut avec LaTeX, elle vaut `rm` (caractère romain).

Les familles plus courantes sont :

- `rm` : famille par défaut (p. ex. *Computer Modern Roman*),
- `tt` : pour les fontes « machines à écrire » (*typewriter*),
- `ss` : pour les fontes sans empattement (*sans serif*).

La police *Computer modern* vient avec d'autres :

- `vtt` : pour les fontes « machines à écrire » à chasse variable,
- `fr` : police funny,
- `dh` : police dunhill,
- `fib` : police fibonacci.

On peut choisir une famille avec la commande `\fontfamily` :

```latex
% !TEX noedit
{\fontfamily{cmrm}\selectfont Apportez un vieux whisky !}
{\fontfamily{cmtt}\selectfont Apportez un vieux whisky !}
{\fontfamily{cmvtt}\selectfont Apportez un vieux whisky !}
{\fontfamily{cmss}\selectfont Apportez un vieux whisky !}
{\fontfamily{cmdh}\selectfont Apportez un vieux whisky !}
```

{raw-latex}`{\fontfamily{cmrm}\selectfont Apportez un vieux whisky !}`

{raw-latex}`{\fontfamily{cmtt}\selectfont Apportez un vieux whisky !}`

{raw-latex}`{\fontfamily{cmvtt}\selectfont Apportez un vieux whisky !}`

{raw-latex}`{\fontfamily{cmss}\selectfont Apportez un vieux whisky !}`

{raw-latex}`{\fontfamily{cmdh}\selectfont Apportez un vieux whisky !}`

:::{tip}
Pour changer de famille ponctuellement dans votre document, vous avez intérêt à définir une macro, pour éviter d'appeler `\fontfamily` directement. Par exemple :

```latex
% !TEX noedit
\newcommand{\textfib}[1]{{\fontfamily{cmfib}\selectfont#1}}
```

qui vous permettra d'écrire ensuite :

```latex
% !TEX noedit
\textfib{Et maintenant la fameuse suite}
```
:::

## Le codage

[Le codage](/2_programmation/encodage/notion_d_encodage) (*encoding*) vaut `OT1` par défaut.

Autres valeurs possibles : `T1`, `OML`, `OMS`...

## La série

La série (*series*) décrit à la fois l'épaisseur du trait (graisse) et la largeur des caractères (caractères condensés ou étendus).

Elle vaut `m` (*medium*) par défaut, et les autres valeurs sont obtenues par une combinaison de deux attributs :

- un poids (qui correspond à la graisse de la fonte) : `ul` (*ultra-light*), `el` (*extra-light*), `l` (*light*), `sl` (*semi-light*), `sb` (*semi-bold*), `b` (*bold*), `eb` (*extra-bold*), `ub` (*ultra-bold*) ;
- une largeur : `uc` (*ultra-condensed*), `ec` (*extra-condensed*), `c` (*condensed*), `sc` (*semi-condensed*), `m` (*medium*), `sx` (*semi-expanded*), `x` (*expanded*), `ex` (*extra-expanded*), `ux` (*ultra-expanded*).

## La forme

La forme (*shape*) correspond aux différents variantes de la police. Ses valeurs possibles sont :

- `n` : normal (valeur par défaut),
- `it` : italique,
- `sl` : penché (*slanted*),
- `sc` : petites capitales (*small capitals*),
- `ui` : italique droit pas toujours disponible (*upright italics*),
- `ol` : pas toujours disponible (*outline*).

## La taille

La taille (*size*) vaut 10pt (*10 points*) par défaut.

Lorsque deux valeurs sont précisées, la première correspond effectivement à la taille de la fonte et la seconde, généralement plus grande, correspond à la taille de l'interligne.

______________________________________________________________________

*Sources :*

- {texdoc}`Documentation de « cmfonts » <cmfonts>`,
- [Best way of using the full range of full fonts/styles/faces for Latin Modern Roman?](https://tex.stackexchange.com/questions/468795/best-way-of-using-the-full-range-of-full-fonts-styles-faces-for-latin-modern-rom)
- [slifontsexample.pdf](http://texpower.sourceforge.net/doc/slifontsexample.pdf)'' (source : ''[slifontsexample.tex](https://mirror.las.iastate.edu/tex-archive/macros/latex/contrib/texpower/tpslifonts/slifontsexample.tex)).

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,polices,forme des caractères,familles de caractères,gras,italique,penché,slanted,italique droit
```

