# Où sont les polices `msx` et `msy` ?

Les polices `msx` et `msy` ont été conçues par l'American Mathematical Society dans les tout premiers temps du TeX, pour être utilisées dans la composition de documents destinés à des revues mathématiques. Elles ont été conçues en utilisant l'"ancien" MetaFont, qui n'était pas portable et n'est plus disponible ; pendant longtemps, elles n'ont été disponibles qu'en versions 300 dpi qui ne correspondent qu'imparfaitement aux imprimantes modernes. L'AMS a maintenant redessiné les polices, en utilisant la version actuelle de MetaFont, et les nouvelles versions sont appelées les familles `msa` et `msb`.

Néanmoins, les familles `msx` et `msy` continuent d'apparaître. Il peut bien sûr y avoir encore des sites qui n'ont pas eu le temps de se mettre à jour ; mais, même si tout le monde se mettait à jour, il y aurait toujours le problème des anciens documents qui les spécifient.

Si vous disposez d'un code `tex` qui demande `msx` et `msy`, la meilleure technique consiste à la modifier de manière à ce qu'elle demande `msa` et `msb` (il suffit de changer un seul caractère dans les noms de police).

Une réimplantation partielle de la partie « [Gras de tableau
noir](https://fr.wikipedia.org/wiki/Gras_de_tableau_noir) » de la police msy\`
(couvrant uniquement ℂ, ℕ, ℝ, 𝕊 et ℤ) est disponible en format Type 1 ; si vos
besoins mathématiques ne vont pas plus loin, la police pourrait être un bon
choix.

Si vous disposez d'un fichier DVI qui demande ces polices, il existe une extension de {doc}`polices virtuelles </5_fichiers/fontes/que_sont_les_fontes_virtuelles>` capable de faire correspondre l'ancienne et la nouvelle série.

______________________________________________________________________

*Source :* {faquk}`Where are the msx and msy fonts? <FAQ-msxy>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```

