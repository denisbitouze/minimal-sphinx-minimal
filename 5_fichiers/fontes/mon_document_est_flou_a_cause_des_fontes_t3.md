# The wrong type of fonts in PDF

This is far the commonest problem : the symptom is that text in the document looks "fuzzy".

Most people use [Adobe Acrobat Reader](https://fr.wikipedia.org/wiki/Adobe_Acrobat) to view their PDF : `Reader` is distributed free of charge, and is widely available, for all its faults. One of those faults is its failure to deal with bitmap fonts (at least, in all versions earlier than version 6, all of which copies are pretty old, now ... but some are occasionally found).

So we don't want bitmap fonts in our PostScript : with them, characters show up in `Reader`s display as blurred blobs which are often not even recognisable as the original letter, and are often not properly placed on the line. Nevertheless, even now, most TeX systems have `dvips` configured to use {doc}`pk files </5_fichiers/fontes/que_sont_les_fichiers_pk>` in its output. Even pdfTeX will use `pk` files if it can see no alternative for a font in the document it is processing.

Our remedy is to use {doc}`Adobe Type 1 </5_fichiers/fontes/formats_de_polices_adobe>`" versions of the fonts we need. Since Adobe are in the business of selling Type 1 fonts, `Reader` was of course made to deal with them really rather well, from the very beginning.

Of course, if your document uses nothing but fonts that came from Adobe in the first place --- fonts such as [Times](https://fr.wikipedia.org/wiki/Times_New_Roman), that appear in pretty much every PostScript printer, or such as Adobe [Sabon](<https://fr.wikipedia.org/wiki/Sabon_(police_d'écriture)>) that you pay extra for --- then there's no problem.

But most people use [Computer Modern](https://fr.wikipedia.org/wiki/Computer_Modern) to start with, and even those relative sophisticates who use something as exotic as `Sabon` often find themselves using odd characters from CM without really intending to do so. Fortunately, rather good versions of the CM fonts are available from the AMS (who have them courtesy of {doc}`Blue Sky Research </6_distributions/implementations_commerciales>` and [Y&Y](https://www.tug.org/yandy/)).

Most modern systems have the fonts installed ready to use; and any system installed less than 3 years ago has a `dvips` configuration file `pdf` that signals the use of the CM fonts, and also sets a few other parameters to improve `dvips` output. Use this configuration as :

```bash
dvips -Ppdf myfile -o myfile.ps
```

This may produce a warning message about failing to find the configuration file :

```text
dvips : warning : no config file for `pdf'
```

or something similar, or about failing to find a font file :

```text
dvips : ! Couldn't find header file cmr10.pfb
```

Either of these failures signals that your system doesn't have the fonts in the first place.

A way of using the fonts that doesn't involve the sophistication of the `-Ppdf` mechanism is simply to load maps :

```bash
dvips -Pcmz -Pamz myfile -o myfile.ps
```

You may encounter the same warning messages as listed above.

If your system does not have the fonts, it won't have the configuration file either; however, it might have the configuration file without the fonts. In either case, you need to {doc}`install the fonts </5_fichiers/fontes/installer_la_version_t1_de_computer_modern>`.

______________________________________________________________________

*Source :* {faquk}`The wrong type of fonts in PDF <FAQ-fuzzy-type3>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonts
```

