# Fonts at arbitrary sizes

Almost all fonts, nowadays, are provided with LaTeX control (`fd`) files, so the temptation to risk the problems of newfont[FAQ-newfontstar](FAQ-newfontstar) is usually easy to resist.

However, one temptation remains, arising from the way that LaTeX restricts the sizes of fonts. In fact, the restriction only significantly applies to the default (Computer Modern) and the Cork-encoded (T1) EC fonts, but it is widely considered to be anomalous, nowadays. In recognition of this problem, there is a package {ctanpkg}`fix-cm` which will allow you to use the fonts, within LaTeX, at any size you choose. If you're not using scaleable versions of the fonts, most modern distributions will just generate an appropriate bitmap for you.

So, suppose you want to produce a heading in Computer Modern Roman at 30 points, you might be tempted to write :

```latex
% !TEX noedit
\newfont{\bigfont}{cmr10 at 30pt}
\begin{center}
  \bigfont Huge text
\end{center}
```

which will indeed work, but will actually produce a worse result than

```latex
% !TEX noedit
\usepackage{fix-cm}
...
\begin{center}
  \fontsize{30}{36}\selectfont
  Huge text
\end{center}
```

Note that the {ctanpkg}`fix-cm` package was not distributed until the December 2003 edition of LaTeX; if you have an older distribution, the packages {ctanpkg}`type1cm` (for CM fonts) and {ctanpkg}`type1ec` (for EC fonts) are available.

{ctanpkg}`Fix-cm <fix-cm>` has one or two omissions --- fonts the LaTeX team did not consider useful, or something; the CM dunhill fonts (as CM, but with stretched ascenders) and the CM fibonacci font (which is only available in 8-point design size) are certainly missing. If {ctanpkg}`fix-cm` doesn't do the job, try the {ctanpkg}`type1xx <type1cm>` packages, or the {ctanpkg}`anyfontsize` package.

A further alternative might be to switch to the [Latin Modern fonts](FAQ-uselmfonts) (which provide a close simulacrum of the *Computer Modern* set); these fonts were scaleable from their first distribution, and don't therefore need any such trick as the above.

______________________________________________________________________

*Source :* {faquk}`Fonts at arbitrary sizes <FAQ-fontsize>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programming
```

