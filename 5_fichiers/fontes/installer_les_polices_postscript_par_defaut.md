# Installing a PostScript printer built-in font

There is a "standard" set of fonts that has appeared in every PostScript printer since the second generation of the type. These fonts (8 families of four text fonts each, and three special-purpose fonts) are of course widely used, because of their simple availability. The set consists of :

- [Times](https://fr.wikipedia.org/wiki/Times_New_Roman) family (4 fonts),
- [Palatino](https://fr.wikipedia.org/wiki/Palatino) family (4 fonts),
- [New Century Schoolbook](https://fr.wikipedia.org/wiki/Century_Schoolbook) family (4 fonts),
- [Bookman](<https://en.wikipedia.org/wiki/Bookman_(typeface)>) family (4 fonts),
- [Helvetica](https://fr.wikipedia.org/wiki/Helvetica) family (4 fonts),
- [Avant Garde](<https://fr.wikipedia.org/wiki/Avant_Garde_(police_d'écriture)>) (4 fonts),
- [Courier](<https://fr.wikipedia.org/wiki/Courier_(police_d'écriture)>) family (4 fonts),
- [Utopia](<https://en.wikipedia.org/wiki/Utopia_(typeface)>) family (4 fonts),
- [Zapf Chancery](https://en.wikipedia.org/wiki/Zapf_Chancery) (1 font),
- [Zapf Dingbats](https://en.wikipedia.org/wiki/Zapf_Dingbats) (1 font),
- [Symbol](<https://en.wikipedia.org/wiki/Symbol_(typeface)>) (1 font).

All these fonts are supported, for LaTeX users, by the {ctanpkg}`psnfss` set of metrics and support files in the file `lw35nfss.zip` on CTAN. Almost any remotely modern TeX system will have some version of {ctanpkg}`psnfss` installed, but users should note that the most recent version has much improved coverage of maths with `Times` (see package {ctanpkg}`mathptmx`) and with `Palatino` (see package {ctanpkg}`mathpazo`, as well as a more reliable set of font metrics.

The archive `lw35nfss.zip` is laid out according to the TDS, so in principle, installation consists simply of "unzipping" the file at the root of a `texmf` tree.

Documentation of the {ctanpkg}`psnfss` bundle is provided in `psnfss2e.pdf` in the distribution.

______________________________________________________________________

*Source :* {faquk}`Installing a PostScript printer built-in font <FAQ-instprinterfont>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,fontes
```

