# Making outline fonts from MetaFont

[TeXtrace](https://sourceforge.net/projects/textrace/), originally developed by Péter Szabó, is a [bundle of Unix scripts](https://pts.50.hu/textrace/) that use Martin Weber's freeware boundary tracing package [autotrace](http://autotrace.sourceforge.net) to generate Type 1 outline fonts from MetaFont bitmap font outputs. The result is unlikely ever to be of the quality of the commercially-produced Type 1 font, but there's always the [FontForge](http://fontforge.sourceforge.net/) font editor to tidy things. Whatever, there remain fonts which many people find useful and which fail to attract the paid experts, and auto-tracing is providing a useful service here. Notable sets of fonts generated using `TeXtrace` are Péter Szabó's own EC/TC font set `tt2001` and Vladimir Volovich's CM-Super set, which covers the EC, TC, and the Cyrillic LH font sets (for details of both of which sets, see {doc}`"8-bit" type 1 fonts </5_fichiers/fontes/fontes_t1_8bits>`).

Another system, which arrived slightly later, is [mftrace](http://www.cs.uu.nl/~hanwen/mftrace/) : this is a small Python program that does the same job. `Mftrace` may use either `autotrace` (like `TeXtrace`) or Peter Selinger's [potrace](http://potrace.sourceforge.net) to produce the initial outlines to process. `Mftrace` is said to be more flexible, and easier to use, than is `TeXtrace`, but both systems are increasingly being used to provide Type 1 fonts to the public domain.

The `MetaType1` system aims to use MetaFont font sources, by way of MetaPost and a bunch of scripts and so on, to produce high-quality Type 1 fonts. The first results, the [Latin Modern fonts](https://ctan.org/pkg/lm), are now well-established, and a bunch of existing designs have been reworked in MetaType1 format.

> {ctanpkg}`Mf2pt1` is another translator of MetaFont font sources by way of MetaPost; in addition, available, {ctanpkg}`mf2pt1` will use [Fontforge](http://fontforge.sourceforge.net/) (if it's available) to auto-hint the result of its conversion ({ctanpkg}`Mf2pt1` is also written in `perl`).

______________________________________________________________________

*Source :* {faquk}`Making outline fonts from MetaFont <FAQ-textrace>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,dessin des caractères,Metafont
```

