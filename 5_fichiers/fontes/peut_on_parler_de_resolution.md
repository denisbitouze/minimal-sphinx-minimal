# Que sont les « résolutions » ?

Le mot « résolution » est un mot qui s'emploie sans se soucier de ses multiples sens dans le domaine du matériel informatique. Le mot suggère une mesure de ce qu'un observateur (peut-être l'œil humain) peut distinguer ; pourtant, nous voyons régulièrement des publicités pour des imprimantes dont la résolution est de [1200 dpi](https://fr.wikipedia.org/wiki/Point_par_pouce) --- bien plus fine que ce que l'œil humain peut distinguer sans aide. Les publicités parlent ici de la précision avec laquelle l'imprimante peut placer des points sur l'image imprimée, ce qui affecte la finesse de la représentation des polices et la précision du placement des glyphes et autres marques sur la page.

En fait, il existe deux types de « résolution » sur la page imprimée que nous devons prendre en compte pour les besoins de TeX ou LaTeX :

- la précision de positionnement ;
- la qualité des polices.

Dans le cas où la sortie TeX ou LaTeX est envoyée directement à une imprimante, dans le langage « natif » de l'imprimante, il est clair que le processeur DVI doit connaître tous ces détails et doit tenir compte des deux types de résolution.

Dans le cas où la sortie est envoyée vers un format intermédiaire, qui a un potentiel d'impression (ou d'affichage), nous ne savons pas comment le traducteur final, qui se connecte directement à l'imprimante (ou à l'écran), a connaissance des propriétés de l'appareil : le processeur DVI n'a pas besoin de le savoir et ne doit pas prétendre le deviner.

```{eval-rst}
.. todo:: *Traduire*
```

Both PostScript and PDF output are in this category. While PostScript is used less frequently for document distribution nowadays, it is regularly used as the source for distillation into PDF ; and PDF is the workhorse of an enormous explosion of document distribution.

Therefore, we need DVI processors that will produce "resolution independent" PostScript or PDF output ; of course, the independence needs to extend to both forms of independence outlined above.

Resolution-independence of fonts was for a long time forced upon the world by the feebleness of Adobe's `Acrobat Reader` at dealing with bitmap files : a sequence of answers starting with one aiming at the {doc}`quality of PDF from PostScript </5_fichiers/pdf/generer_un_fichier_pdf_de_qualite>` addresses the problems that arise.

Resolution-independence of positioning is more troublesome : `dvips` is somewhat notorious for insisting on positioning to the accuracy of the declared resolution of the printer. One commonly-used approach is to declare a resolution of 8000 ("better than any device"), and this is reasonably successful though it does have {doc}`its problems </2_programmation/erreurs/m/mismatched_mode_ljfour_and_resolution_8000>`.

______________________________________________________________________

*Source :* {faquk}`What are « resolutions »? <FAQ-resolns>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,résolution d'une police,résolution des caractères,polices bitmap
```

