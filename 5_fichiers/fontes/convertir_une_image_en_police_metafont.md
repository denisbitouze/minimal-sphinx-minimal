# Comment convertir une image en police METAFONT ?

Le programme {ctanpkg}`bm2font` permet de faire cela.

Des renseignements plus généraux pourront être trouvés sur la page [logiciels_de_conversion_de_formats_d_images](/6_distributions/conversion/logiciels_de_conversion_de_formats_d_images).

```{eval-rst}
.. meta::
   :keywords: LaTeX,créer une police de caractères,créer une police Metafont,dessin vectoriel
```

