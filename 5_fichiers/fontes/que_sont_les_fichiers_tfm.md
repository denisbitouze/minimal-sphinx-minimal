# Que sont les fichiers TFM ?

TFM est un acronyme pour *TeX Font Metrics* (métriques de fontes TeX). Les fichiers TFM contiennent des informations sur la taille des caractères, les ligatures et les crénages d'une fonte. Un fichier TFM est nécessaire pour chaque fonte utilisée par TeX, c'est-à-dire pour toute combinaison de taille de dessin (en point), de graisse et de famille de la fonte. Chaque fichier TFM sert à tous les grossissements de « sa » fonte, de sorte qu'il y a (généralement) moins de fichiers TFM qu'il n'y a de fichiers {doc}`PK </5_fichiers/fontes/que_sont_les_fichiers_pk>`. TeX, LaTeX, etc., n'ont besoin de connaître que la taille des caractères et leurs interactions les uns avec les autres, mais pas à quoi ressemblent les caractères. En revanche, les fichiers TFM ne sont en principe pas nécessaires au pilote DVI, celui-ci n'ayant besoin de connaître que les glyphes que chaque caractère sélectionné, pour les imprimer ou les afficher.

Notez que les polices TrueType et OpenType contiennent les métriques nécessaires, de sorte que {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` et {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`, avec de telles polices, n'ont pas besoin de fichiers TFM. En conséquence, la configuration des fontes pour ces moteurs est beaucoup *plus facile*.

______________________________________________________________________

*Source :* {faquk}`What are TFM files? <FAQ-tfm>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,TFM
```

