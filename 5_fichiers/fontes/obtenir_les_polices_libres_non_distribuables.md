# Comment obtenir les polices « non-free » ?

## Que sont les polices « non-free » ?

Certaines polices sont libres d'utilisation, mais leur fournisseur interdit de les vendre.

Paradoxalement, cette condition plutôt généreuse empêche que ces polices soient inclusent sur un DVD qui pourrait éventuellement être proposé à la vente (ne serait-ce qu'à prix coûtant, ou vendu avec un livre papier). Donc le choix des mainteneurs des distributions LaTeX libre a été de ne pas inclure ces police dans les distributions, mais d'en proposer le téléchargement séparé.

## Comment installer ces polices ?

Elles sont librement téléchargeables sur le [CTAN](https://www.ctan.org/), avec tous les fichiers nécessaires pour les installer, **mais** si vous utilisez TeX Live, vous ne devriez pas avoir à vous occuper manuellement de leur installation, car un script existe pour s'occuper de ça :

- Sur la page <https://tug.org/fonts/getnonfreefonts/>, téléchargez le fichier d'installation `install-getnonfreefonts`,
- allez dans le répertoire où vous l'avez enregistré,
- puis exécutez `texlua install-getnonfreefonts`.

Si vous êtes sous Linux, vous pouvez utiliser ces deux commande pour réaliser le téléchargement et l'exécution :

```bash
wget https://www.tug.org/fonts/getnonfreefonts/install-getnonfreefonts
texlua install-getnonfreefonts
```

Ceci installe le script `getnonfreefonts` dans votre arborescence LaTeX. Vous allez maintenant pouvoir exécuter ce script pour réaliser l'installation des polices proprement dites.

Vous pouvez lui demander ce qu'il a de disponible en exécutant :

```bash
getnonfreefonts -l
```

et vous pouvez lui demander d'installer une police (dans votre arbre `texmf` personnel) avec :

```bash
getnonfreefonts --user luximono
```

Si vous êtes administrateur de la machine, vous pouvez installer la police pour tous les utilisateurs, avec l'option `--sys` :

```bash
getnonfreefonts --sys luximono
```

Le script s'occupera de télécharger les fichiers de polices appropriés depuis le CTAN, de les extraire de leur fichier `zip`, de les installer et de mettre à jour les index des polices (*fontmaps*). Il va même jusqu'à s'excuser du temps que cela prend !

:::{note}
L'utilisation la plus courante du script demande l'installation de **toutes** les polices *non-free* :

```bash
getnonfreefonts --sys --all
```
:::

## Quelles sont les polices concernées ?

Voici la liste complète. En consultant les pages correspondantes du CTAN, vous aurez un bref historique pour chacune.

| Extension                | Fonte                   |
| ------------------------ | ----------------------- |
| {ctanpkg}`urw-arial`     | URW Arial (A030)        |
| {ctanpkg}`classico`      | URW Classico            |
| {ctanpkg}`dayroman`      | Day Roman               |
| {ctanpkg}`gandhi`        | Gandhi                  |
| {ctanpkg}`urw-garamond`  | URW GaramondNo8         |
| {ctanpkg}`garamondx`     | GaramondNo8 Expert      |
| {ctanpkg}`lettergothic`  | URW LetterGothic        |
| {ctanpkg}`literaturnaya` | Literaturnaya           |
| {ctanpkg}`luximono`      | Luxi Mono               |
| {ctanpkg}`vntex-nonfree` | ClassicoVn & GaramondVn |
| {ctanpkg}`webomints`     | Webomints               |

______________________________________________________________________

*Source :*

- {faquk}`Getting « free » fonts not in your distribution <FAQ-getnff>`,
- [getnonfreefonts](https://tug.org/fonts/getnonfreefonts/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,fontes,fontes non libres,fontes non gratuite,fontes non free, non free fonts
```

