# Comment modifier la police du mode verbatim ?

- <https://www.ctan.org/macros/latex/required/tools/> permet de faire cela. Il est alors conseillé d'utiliser des fontes aux normes T1.
- L'environnement `verbatimcmd` du package {ctanpkg}`moreverb`, disponible sur
  <https://www.ctan.org/macros/latex/contrib/misc/> permet de garder les
  caractères « contre-oblique » (`\`) et les accolades (`{`, et `}`) actifs. On
  peut donc entre autres opérer des changements de police.
- Pour changer la taille de la police du mode `verbatim`, il faut l'encapsuler dans des commandes de modification de taille.

Exemple~ :

```latex
% !TEX noedit
Texte avant.

\begin{small}
\begin{verbatim}
   Texte...
\end{verbatim}
\end{small}

Texte après.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

