# Où trouver une fonte de taille 9pt ?

- La classe de document {ctanpkg}`amsart`, de {doc}`AmSLaTeX </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>` offre l'option `[9pt]` :

**10pt**

```latex
% !TEX noedit
\documentclass[10pt]{article}

\begin{document}
Taille de base (10\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[10pt]{article}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Taille de base (10\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

**9pt**

```latex
% !TEX noedit
\documentclass[9pt]{amsart}

\begin{document}
Taille de base (9\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[9pt]{amsart}
  \usepackage{lmodern}

\begin{document}
  \thispagestyle{empty}

Taille de base (9\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

- De même les classes `extarticle` et `extreport`, fournies par l'extension {ctanpkg}`extsizes`, proposent de composer en taille 9pt, mais aussi 8pt, 14pt, 17pt, et 20pt.

Si vous aimez bricoler, vous pouvez utiliser le fichier `size9.clo` de James Kilfiger, dans les sources de {ctanpkg}`extsizes`.

:::{note}
Pour des exemples avec {ctanpkg}`extsizes`, voir la page « {doc}`Comment avoir d'autres tailles de police de base? </3_composition/texte/symboles/polices/comment_avoir_d_autres_tailles_de_police>` ».
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,police de caractère,taille pluis petite,fonte plus petite,neuf points
```

