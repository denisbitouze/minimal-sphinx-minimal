# Caractères flous : comment corriger le problème de « Ghostscript » ?

:::{note}
Ce problème ne devrait plus arriver avec les versions actuelles des différents logiciels (il a été résolu dès l'année 2001, en fait).
:::

Vous avez fait tout ce que la FAQ vous dit, les bonnes polices sont correctement installées et apparaissent dans la sortie `dvips`, mais vous obtenez *toujours* des caractères flous dans votre document final après avoir l'avoir « distillé » avec [ghostscript](https://fr.wikipedia.org/wiki/Ghostscript).

Le problème peut provenir d'une version trop ancienne de [ghostscript](https://www.ghostscript.com/), que vous utilisez soit directement, soit via un script tel que `ps2pdf` (distribué avec `ghostscript` lui-même), `dvipdf`, ou similaire. Bien que `ghostscript` soit capable de distiller depuis la version 5.50, cette version ne pouvait produire que des sorties bitmap de Type 3 pour toute police autre que les 35 polices de base (`Times`, `Helvetica`, etc.). Les versions ultérieures ont progressivement ajouté la distillation complète, mais ce n'est qu'à partir de la version 6.50 que l'on pouvait s'y fier véritablement.

Donc, si votre sortie PDF semble systématiquement floue dans `Acrobat Reader`, mettez à jour `ghostscript`. La nouvelle version doit être au moins la version 6.50, bien sûr, mais il est généralement préférable de passer à la version la plus récente (la série des versions 8.xx est sortie en 2002, les versions 9.xx en 2010, et on en est maintenant à la version 9.54.0, sortie en mars 2021).

{octicon}`alert;1em;sd-text-warning` Normalement, ce problème est loin derrière nous et si vous rencontrez de nouveau des problèmes de caractères flous, il faut sans doute chercher ailleurs.

______________________________________________________________________

*Source :* {faquk}`Fuzzy fonts because « Ghostscript » too old <FAQ-fuzzy-gs>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,fontes,caractères flous,lettres floues,texte flou,texte pas net,bug de Ghostscript
```

