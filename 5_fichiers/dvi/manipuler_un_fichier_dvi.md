# Comment manipuler un fichier DVI ?

- On peut sélectionner quelques pages d'un fichier DVI à l'aide de la commande `dviselect`.
- `dviconcat` permet de concaténer des fichiers DVI.
- `dvidvi` permet, en plus de selectionner, changer l'ordre et de tourner des pages, de créer des livrets en format A5...
- `dvi2dvi` permet de changer la police.

______________________________________________________________________

*Sources :*

- [Device independent file format](https://en.wikipedia.org/wiki/Device_independent_file_format#DVI_related_software),
- [Is there any reason to compile to DVI rather than PDF these days?](https://tex.stackexchange.com/questions/2811/is-there-any-reason-to-compile-to-dvi-rather-than-pdf-these-days)

```{eval-rst}
.. meta::
   :keywords: Format DVI,Device-independent,convertir un fichier DVI,ouvrir un fichier DVI,visualiser un fichier DVI,imprimer un fichier DVI
```

