# Comment générer des images à partir d'un DVI ?

Tout pilote ou prévisualiseur DVI génère des images matricielles (aussi nommées [bitmap](https://fr.wikipedia.org/wiki/Image_matricielle)) : ils les utilisent pour placer de minuscules points sur du papier via une imprimante ou pour remplir une partie de votre écran. Cependant, il est généralement difficile d'extraire ces images autrement que par capture d'écran et la résolution est alors généralement assez mauvaise.

Pourquoi voudrait-on obtenir ces images ? Le plus souvent, ceci sert à inclure dans un document HTML généré à partir de la source LaTeX tout ce qui ne peut pas être traduit en HTML. Parmi les cas courants se trouvent :

- les mathématiques : en effet, les fontes mathématiques ne sont pas universellement pris en charge par les navigateurs ;
- la génération d'images pour insertion dans l'affichage d'une autre application acceptant les images ou pour prendre en charge une chaîne de traitement de document automatisée.

Dans le passé, la manière la plus courante de générer une image était de générer un fichier PostScript à partir du DVI, puis d'utiliser [ghostscript](https://www.ghostscript.com/) pour produire le format requis (éventuellement au moyen du format PNM ou quelque chose d'approchant). Cette procédure, qui a longtemps servi, s'avère fastidieuse et nécessite deux ou trois étapes qui s'exécutent lentement : elle n'est plus d'actualité.

## Avec dvi2bitmap

L'ancien programme {ctanpkg}`dvi2bitmap` génére à partir d'un fichier DVI les formats XBM, XPM, GIF (obsolète depuis longtemps) et PNG.

## Avec dvipng

Le programme {ctanpkg}`dvipng` était à l'origine un convertisseur PNG. À partir de la version 1.2, il peut également restituer le format GIF. Il est essentiellement pensé pour des conversions rapides dans des environnements qui génèrent un grand nombre de fichiers PNG. Notez que `dvipng` donne un résultat de haute qualité même si ses opérations internes sont optimisées pour la vitesse.

______________________________________________________________________

*Source :* {faquk}`Generating bitmaps from DVI <FAQ-dvi-bmp>`

```{eval-rst}
.. meta::
   :keywords: LaTeX, DVI, image, conversion
```

