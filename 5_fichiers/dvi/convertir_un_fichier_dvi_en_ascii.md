# Comment convertir un fichier DVI en ASCII ?

- {ctanpkg}`catdvi` produit un bon résultat et gère plusieurs codages de caractères. [Voir aussi sa page d'accueil](http://catdvi.sourceforge.net/).
- On peut utiliser {ctanpkg}`dvi2tty`. Ce programme a quelques difficultés avec les ligatures; une solution semble être d'utiliser une police sans ligatures, comme par exemple `courier`.

:::{note}
Vous trouverez aussi {ctanpkg}`dvitty`. Il s'agit en fait de la première version de {ctanpkg}`dvi2tty`, écrite en Pascal (de 1984 à 1986). Le programme a été réécrit en `C` vers 1989, et c'est bien `dvi2tty` qui est actuellement maintenu.
:::

- [dvispell](https://ctan.org/tex-archive/support/dvispell) de la distribution {ctanpkg}`emTeX <emtex>` permettait également de convertir un fichier DVI en fichier texte. Mais cette distribution est largement obsolète (dernière mise à jour en 1998) et ne fonctionne que sous OS/2, MS-DOS, Windows 3.1.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,format texte brut,ouvrir un fichier DVI
```

