# Qu'est-ce qu'un fichier DVI ?

"DVI" est censé être l'acronyme de *DeVice-Independent*. C'est historiquement le format de fichier de sortie de TeX. Il porte ce nom car il est conçu pour pouvoir être converti en vue d'être imprimé ou visualisé sur la plupart des types de périphériques de sortie (écrans, imprimantes...). Ce format de fichier n'a rien à voir avec le type de connexion [Digital visual interface](https://fr.wikipedia.org/wiki/Digital_visual_interface) que vous utilisez pour brancher un écran sur votre ordinateur.

## Où rencontre-t-on les fichiers DVI ?

Lorsque vous compilez votre document avec les commandes `tex` ou `latex`, vous obtenez un fichier dont l'extension est `.dvi` [^footnote-1]. Ce format DVI est le format de fichier de sortie d'origine de TeX, développé par David R. Fuchs en 1982 pour le projet, et implémenté par Donald E. Knuth quand il a écrit le moteur TeX. Contrairement aux fichiers TeX ou LaTeX, qui sont dans un langage balisé fait pour être écrit et lu par des humains, les fichiers DVI ne sont pas destinés à être directement compréhensibles ; ils sont constitués de données binaires décrivant la présentation visuelle d'un document d'une manière qui ne dépend pas d'un format d'image, d'un matériel d'affichage ou d'une imprimante particuliers.

Un fichier TeX doit donner la même sortie DVI quelle que soit l'implémentation de TeX utilisée pour le compiler.

Un fichier DVI est généralement utilisé comme entrée d'un second programme, appelé « {doc}`pilote DVI </5_fichiers/dvi/qu_est_qu_un_pilote_dvi>` »), qui va s'occuper de le traduire en informations graphiques. Par exemple, la plupart des distributions TeX comprennent un programme permettant de prévisualiser les fichiers DVI sur un écran d'ordinateur. D'autres pilotes vont convertir les fichiers DVI en PostScript ou en PDF (des langages de description de pages), ou dans des formats spéciaux pour tel ou tel modèle d'imprimante.

Le DVI diffère du PostScript et du PDF en ce que, s'il contient bien toute les autres informations nécessaires à l'impression ou à l'affichage, il n'intégre pas les polices de caractères (les formats PostScript et PDF peuvent soit intégrer leurs polices à l'intérieur des documents, soit faire référence à des polices externes). Dans le fichier DVI, les caractères (représentant les glyphes pour l'impression ou l'affichage) apparaissent selon un codage décrit dans le document. Donc pour qu'un fichier DVI puisse être imprimé ou même correctement prévisualisé, les polices auxquelles il fait référence doivent être déjà installées.

Autre élément manquant dans le fichier DVI : tout ce qui a été introduit par {doc}`des commandes </5_fichiers/postscript/commandes_special>``\special` (qui sont généralement une façon de contourner volontairement le pilote DVI, pour s'adresser directement au pilote PostScript depuis le document TeX; voir ci-après).

Comme le PDF, et à la différence du PS, le DVI utilise un langage qui n'est pas un langage de programmation complet ([au sens de Turing](https://fr.wikipedia.org/wiki/Turing-complet)). Il est est limité et offre une [garantie de terminaison](https://fr.wikipedia.org/wiki/https://fr.wikipedia.org/wiki/Terminaison_d%27un_algorithme).

Le balisage du document TeX initial peut être en partie retrouvé par ingénierie inverse à partir du fichiers DVI, mais ce processus ne retrouvera pas les constructions de haut niveau, surtout si le balisage d'origine utilisait des extensions complexes (ne serait-ce que LaTeX).

:::{tip}
{doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` (publié quelque temps après pdfTeX) utilise un "format DVI étendu" (XDV, pour *Extended DVI format*) au lieu du format DVI de base et il faut utiliser le {doc}`pilote DVI </5_fichiers/dvi/qu_est_qu_un_pilote_dvi>` adapté : `xdvipdfmx`.
:::

## Spécifications

La référence ultime pour la structure d'un fichier DVI est le code-source du programme {ctanpkg}`dvitype` de Knuth (dont le but initial, comme son nom l'indique, était de visualiser le contenu d'un fichier DVI). Mais ce n'est pas très pratique pour rendre ce format accessible à d'autres développeurs, donc depuis 2004, le [TUG DVI Driver Standards Committee](https://www.tug.org/twg.html) a documenté les {ctanpkg}`spécifications qu'un pilote DVI doit implémenter <dvistd>`. Ces spécifications sont partiellement basées sur un [article de TUGboat de 1992](https://www.tug.org/TUGboat/tb13-1/tb34dvistd.pdf), beaucoup plus court.

Le format DVI a été conçu pour être compact et facilement lisible par une machine. Dans ce but, un fichier DVI est une séquence de commandes qui forment une sorte de « langage machine », selon les termes de Knuth. Chaque commande commence par un opcode de huit bits, suivi de zéro octet ou plus de paramètres. Par exemple, un opcode du groupe `0x00` à `0x7F` (127 en décimal), `set_char_i`, insère un unique caractère et déplace le curseur de la largeur de ce caractère. L'opcode `0xF7` (décimal 247), au contraire, `pre` (le préambule, qui doit être le premier opcode du fichier DVI), prend au moins quatorze octets de paramètres, plus un commentaire facultatif pouvant aller jusqu'à 255 octets [^footnote-2].

De façon plus générale, un fichier DVI se compose d'un préambule, d'une ou plusieurs pages et d'un postambule. Six variables d'état sont maintenues sous forme d'un tuple d'entiers signés de 32 bits : (*h*, *v*, *w*, *x*, *y*, *z*) :

- *h* et *v* sont les décalages horizontaux et verticaux actuels par rapport au coin supérieur gauche (quand *v* augmente le curseur se déplace vers le bas de la page),
- *w* et *x* contiennent les informations d'espacement horizontal,
- *y* et *z* contiennent les informations d'espacement vertical.

Ces variables peuvent être ajoutée ou retirées de la pile (*pushed* et *popped*). En outre, la police actuelle, notée *f*, est stockée sous forme d'un entier, mais elle pas stockées dans la pile avec les autres variables d'état lorsque les opcodes `push` et `pop` sont exécutés. Les informations sur la taille de chaque caractère sont chargées depuis les fichiers TFM. Les polices elles-mêmes ne sont pas incluses dans le fichier DVI, mais seulement référencées par un entier défini par l'opérateur `fnt_defi` (cette opération est effectuée exactement deux fois pour chaque police chargée : une fois avant qu'elle ne soit référencée et une fois dans le postambule). *f* contient une valeur entière d'une longueur maximale de quatre octets, bien qu'en pratique, TeX ne donne que des numéros de polices compris entre 0 et 255.

De même, le format DVI prend en charge les codes de caractères d'une longueur maximale de quatre octets, même si seule la plage 0-255 est couramment utilisée, le format TFM étant limité à cette plage. Les codes de caractères du fichier DVI font référence au codage des caractères dans la police actuelle, et non à celui du système. Cela signifie, par exemple, qu'un système EBCDIC peut traiter un fichier DVI généré sur un système ASCII, à condition que les mêmes polices soient installées.

## L'inclusion de dessins

Le format DVI ne prend pas en charge les dessins, à l'exception des traits en noir et blanc les plus simples. Par contre, le format DVI propose un mécanisme général d'échappement (et d'extension), appelé *specials*, qui permet que le pilote DVI passe simplement les informations à l'étape suivante du traitement. Ainsi, avec la commande TeX `\special`, on pourra stocker des informations de dessin ou de manipulation des couleurs dans le fichier DVI. Le pilote DVI n'y touchera pas, il se contentera de transmettre ces information à des filtres de post-traitement. Il existe de nombreuses commandes spéciales DVI, dont les plus remarquables sont des commandes spéciales PostScript (utilisée intensivement par l'extension {ctanpkg}`PStricks <pstricks>`), mais d'autres programmes comme {ctanpkg}`tpic <eepic>` ont leurs propres commandes.

______________________________________________________________________

*Sources :*

- {faquk}`What is a DVI file? <FAQ-dvi>`
- [Format DVI](<https://fr.wikipedia.org/wiki/DVI_(TeX)>),
- [The DVI Driver Standard, Level 0](http://mirrors.ctan.org/dviware/driv-standard/level-0/dvistd0.pdf),
- [Does DVI file appear same on different DVI viewers?](https://tex.stackexchange.com/questions/330106/does-dvi-file-appear-same-on-different-dvi-viewers)
- [Is the DVI format de facto dead?](https://tex.stackexchange.com/questions/130518/is-the-dvi-format-de-facto-dead)

```{eval-rst}
.. meta::
   :keywords: LaTeX,fichier DVO,format DVI,conversion de fichiers DVI,lecture de fichier DVI,comment ouvrir un fichier DVI,DeVice-Independent file,fichier device-indépendant
```

[^footnote-1]: {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` et {doc}`luaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`, apparus plus tard, peuvent produire d'autres formats, et sont généralement utilisés pour produire directement des fichiers PDF.

[^footnote-2]: Les spécifications du format ne le précisent pas, mais DVI est [gros-boutiste](https://fr.wikipedia.org/wiki/Boutisme) (*big endian*).

