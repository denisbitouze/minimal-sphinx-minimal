# Produire du XML ou du xxML

# Comment convertir du SGML en AllTeX ?

- Veuillez vous reporter à la section \\vref\{sgmllatex}.

# Comment convertir du LaTeX en XML ?

- `ltx2x` est un programme qui permet de convertir du LaTeX en autre chose dont du XML. La translation est faite à partir de tables, et il est possible de programmer des tables pour obtenir des sorties de tout type. Il est disponible sur les sites <https://www.ctan.org/>.
- `Latex2MathMLcontent` disponible à l'URL : <http://mowgli.cs.unibo.it/library/latexconverter.html> , permet de transformer du LaTeX en MathML.
- `TeX4ht` permet, à l'aide de scripts, de faire la transformation LaTeX → XML. Il est disponible sur le CTAN.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

