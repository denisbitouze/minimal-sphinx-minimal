# Comment convertir du code LaTeX en code Plain TeX ?

Malheureusement, aucun processus général, simple et automatique n'est susceptible de réussir dans cette tâche. Voir la question « {doc}`Quel est le lien entre LaTeX et Plain TeX ? </1_generalites/bases/differences_entre_latex_et_tex>` » pour plus d'informations.

En mettant à part le cas des documents triviaux, même les documents qui utilisent des éléments relativement simples, tels que des étiquettes et des références, sont susceptibles de causer des problèmes. De fait, Plain TeX ne prend pas en charge les étiquettes alors qu'il gère bien, par exemple, les images. Aussi, traduire un document conçu pour fonctionner avec LaTeX en un document qui fonctionne avec Plain TeX revient généralement à inclure soigneusement (ou à réimplémenter) toutes ces parties de LaTeX que Plain TeX ne connaît pas et que le document utilise.

Dans un sens, une partie de ce travail a été faite, par exemple dans le portage de l'extension {ctanpkg}`graphics` de LaTeX vers Plain TeX. Cependant, d'autres extensions complexes (notamment {ctanpkg}`hyperref`) n'ont pas bénéficié de ce type de portage. Un volontaire sur ce sujet pourra noter que le système {doc}`Eplain </1_generalites/glossaire/qu_est_ce_que_eplain>` est une source de code utile. En fait, un système léger tel qu'Eplain pourrait raisonnablement être adopté comme cible alternative de conversion, bien qu'il donne sans aucun doute à l'utilisateur plus que le « strict minimum » que Plain TeX est conçu pour offrir.

______________________________________________________________________

*Sources :*

- {faquk}`Translating LaTeX to Plain TeX <FAQ-LaTeXtoPlain>`,
- [Convert from LaTeX to Plain TeX](https://tex.stackexchange.com/questions/21281/convert-from-latex-to-plain-tex),
- [Is there any software that converts latex file to tex file?](https://tex.stackexchange.com/questions/91042/is-there-any-software-that-converts-latex-file-to-tex-file)

```{eval-rst}
.. meta::
   :keywords: LaTeX,Plain TeX,conversion,Eplain
```

