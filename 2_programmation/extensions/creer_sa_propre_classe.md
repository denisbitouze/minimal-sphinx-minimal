# Comment créer une classe ?

Les {doc}`classes </2_programmation/extensions/que_sont_les_classes_et_packages>` LaTeX définissent généralement de grands types de documents : articles, livres, posters. Mais il peut être intéressant d'en créer de nouvelles.

## Avant d'écrire une classe

Si vous souhaitez juste personnaliser le style de votre document (mise en forme des titres, changement de la mise en page...) ou ajouter des fonctionnalités sous forme de commandes, la création d'une classe n'est peut-être pas nécessaire. Mieux vaut écrire une extension (un fichier `.sty`), qui pourra être chargée dans le préambule de votre document en même temps que les autres :

```latex
% !TEX noedit
\documentclass{book}
\usepackage{MonExtension}
\usepackage{hyperref}
```

Néamoins, il peut être justifié de créer une classe :

- soit parce que l'on a besoin d'un type de document qui n'existe pas encore,
- soit parce que l'on veut modifier une classe déjà existante pour l'adapter à ses besoins.

Dans ce cas, vous commencerez par créer votre propre fichier `.cls` dans lequel seront réunies les nouvelles commandes de mise en page. Un exemple simple est proposé ci-dessous.

## Quelques documents de référence

Écrire de bonnes classes n'est pas chose facile. Il est recommandé avant tout d'en lire quelques-unes telles les {ctanpkg}`classes` standard. Les autres classes sont généralement basées sur celles-ci et commencent par charger la classe standard avec `\LoadClass`. Un exemple de cette technique peut être vu dans le document « {ctanpkg}`Class and package programming guide <clsguide>` ». Pour aller plus loin sur cette compréhension des classes, une [version annotée](https://tug.org/TUGboat/Articles/tb28-1/tb88flynn.pdf) de la classe `article`, tel qu'elle apparaît dans `classes.dtx`, a été publiée en anglais dans le [TUGboat 28:1](https://tug.org/TUGboat/Articles/tb28-1/). Cet article, de Peter Flynn, est un bon guide pour comprendre '' classes.dtx''.

Par ailleurs, le document « {ctanpkg}`Class and package programming guide <clsguide>` », tenu par l'équipe du {doc}`Projet LaTeX </1_generalites/histoire/c_est_quoi_latex3>`, décrit les commandes utiles et accessibles dans le cadre de l'écriture d'une nouvelle classe ou d'une nouvelle extension.

Vous aurez sans doute également besoin du document {texdoc}`LaTeX2ε : un manuel de référence officieux <latex2e-fr>` (en français, [également disponible en HTML](https://texdoc.org/serve/latex2e-fr/1)).

## Un exemple

Voici un exemple de Vincent Henn pour définir une classe similaire à {ctanpkg}`article` :

```latex
%%% A sauver dans MonArticle.cls
%%%
%%%  Format de style adapté de article.cls
%%%
%%%  d'après V.H. le 13 avril 1995
%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MonArticle}[1995/06/20 Article personnel]

%%%  Chargement de la classe article, avec transfert d'options
\PassOptionsToClass{a4paper}{article} % format a4paper par défaut
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions

\LoadClass{article}

%%%  Chargement des packages les plus courants
\RequirePackage{frbib}
\RequirePackage{general}
\RequirePackage{fuzzy}
\RequirePackage{bigcenter}
\RequirePackage{traffic}
\RequirePackage[dvips]{epsfig}
\RequirePackage{epic}
\RequirePackage[frenchb]{babel}

%%%  Destination de l'article (proposé pour une conf, une revue...)

\def\Destination#1{\ifx{#1}{}\gdef\@Destination{}%
\else\gdef\@Destination{#1}\fi}

%%%   Destination vide par défaut

\Destination{}

%%%   Auteur par défaut, pour eviter de recopier a chaque fois

\author{Vincent \fsc{Henn}\thanks{Laboratoire d'ingénierie
circulation--transport, (Unité mixte \lsc{INRETS}--\lsc{ENTPE}),
109, Avenue Salvador Allende, Case 24,
F--69675 \fsc{Bron} Cedex, {\tt henn@inrets.fr}.}}
```

En voici un exemple simple d'utilisation :

```latex
% !TEX noedit
\documentclass[a4paper,11pt]{MonArticle}

\title{Exemple d'article}
\Destination{IEEE}
\begin{document}
\maketitle

\chapter{Introduction}

Bla bla

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: développement LaTeX,packages,styles de documents,classe personnalisée en LaTeX
```

