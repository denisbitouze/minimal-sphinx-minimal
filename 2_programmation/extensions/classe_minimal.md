# À quoi sert la classe « minimal » ?

- Dans la vie de tous les jours, cette classe ne doit **pas** être utilisée, même pas pour composer un «{doc}`exemple complet minimal </1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal>` ».

Cette classe a servi de test dans les années 1990, lors du développement de LaTeX2ε, et reste dans les distributions LaTeX pour des raisons historiques, mais elle n'a plus d'usage réel. Elle ne définit même pas les macros pour les différentes tailles de caractère, ni `\parindent`, par exemple. En l'utilisant, vous ne vous mettez pas dans les conditions d'un document réel.

Son code est présenté en réponse à la question «[Que signifie l'erreur : « The font size command \\normalsize is not defined... »?](/2_programmation/erreurs/t/the_font_size_command_normalsize_is_not_defined) »

# Alors quoi utiliser pour un document sans fioritures ?

- Si vous souhaitez produire un document qui ait juste la taille de son contenu (donc « minimal » au sens de «sans marges, sans numéro de page, sans titre...»), vous pouvez utiliser la classe {ctanpkg}`standalone`, de Martin Scharrer :

```latex
\documentclass{article} % 'standalone' ne fonctionne pas sur le wiki, en TL2017.
  \usepackage{tikz}
  \pagestyle{empty}

\begin{document}
\begin{tikzpicture}
  \fill[orange] circle (3ex) ;
  \draw (-3ex,-3ex) rectangle (3ex,3ex) ;
\end{tikzpicture}
\end{document}
```

Cette classe, basée sur la classe `article`, est spécialement conçue pour créer des documents dont le résultat sera inclus dans un autre document avec un `\includegraphics`.

Elle propose de nombreuses options pour ajuster son comportement. {texdoc}`Voir sa documentation <standalone>` (en anglais).

______________________________________________________________________

*Sources :*

- [Why should the minimal class be avoided?](https://tex.stackexchange.com/questions/42114/why-should-the-minimal-class-be-avoided)
- [What is the advantage of using minimal over article when creating a standalone graphic?](https://tex.stackexchange.com/questions/20974/what-is-the-advantage-of-using-minimal-over-article-when-creating-a-standalone-g)

```{eval-rst}
.. meta::
   :keywords: LaTeX,classes LaTeX,exemple complet minimal,déboguer du code LaTeX,simplifier son exemple
```

