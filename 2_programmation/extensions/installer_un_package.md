# Comment installer une extension ?

## Installation semi-automatique

La plupart du temps, il suffit d'utiliser les mécanismes de gestion d'extension de votre distribution LaTeX (et de penser à procéder aux mises à jour).

## Installation manuelle

Il peut cependant arriver que vous ayez à installer à la main une extension, soit pour avoir la version la plus récente, soit parce que vous n'avez pas accès au gestionnaire d'extension (sur une machine où vous n'êtes pas administrateur, par exemple).

### Dossiers cibles de l'installation

Il faut alors savoir qu'existent sur votre machine plusieurs arborescences `texmf` (les répertoires où sont rangés, entre autres, les extensions). La plupart du temps, ce sont les trois suivantes :

- une arborescence de base ;
- une arborescence « locale » ;
- et une arborescence « utilisateur ».

Ne touchez qu'aux deux dernières. Sur une installation TeXlive/linux typique, ces arborescences sont :

```
/usr/share/texmf-texlive/
/usr/local/share/texmf
$HOME/texmf
```

Ces chemins peuvent varier et être modifiés. Leur valeur est déterminée par des variables d'environnement que vous pouvez afficher via la commande suivante sous TeXlive/linux :

```
texconfig conf
```

Pour en savoir plus, lisez la documentation spécifique à votre distribution. En effet, les distributions sont toujours accompagnées de leur propre documentation, décrivant l'organisation des fichiers, les outils de bases, variables d'environnement, etc. Cette documentation se trouve sous plusieurs formes et/ou langues dans vos répertoires d'installation mais aussi, plus simplement, sur le web, que ce soit pour [TeXlive](https://www.tug.org/texlive/doc/texlive-fr/texlive-fr.html) ou [MiKTeX](https://docs.miktex.org/manual/MiKTeX).

### Cas simples

Ces cas sont ceux où il suffit de copier les fichiers (dont le fichier `.sty`) depuis le {doc}`CTAN </1_generalites/glossaire/qu_est_ce_que_le_ctan>` vers l'endroit approprié de votre disque dur. Vous pouvez placer les fichiers au choix dans le même répertoire que votre document en cours (peu recommandé puisque seul les fichiers présents dans le même répertoire trouveront cette extension), ou dans un sous-répertoire adéquat (typiquement `tex/latex/package`) de votre `texmf` personnel ou local.

Dans ce dernier cas, il vous faudra reconstruire la base de données des extensions avec, suivant la distributionn `texhash`, `mktexlsr` ou bien l'une des deux commandes :

```bash
rebuild ls-R filenames databases
# ou
refresh filename database
```

La documentation de votre distribution devrait vous aider à savoir quelles sont les commandes utiles ici. Vous pourrez ensuite vérifier que votre nouveau fichier est bien trouvé par LaTeX avec la commande suivante :

```
kpsewhich extension.sty
```

Elle retourne la version qui sera effectivement utilisée dans le cas où il en existerait plusieurs.

### Cas un peu plus délicats (mais plus formels)

Lorsqu'il s'agit d'une extension conçue pour LaTeX et utilisant les procédures et outils définis dans ce cadre, une extension est fournie avec deux fichiers (ou plus, pour les extensions plus complexes) : `extension.ins` et `extension.dtx` et non le fichier `extension.sty`.

Il est ici conseillé de travailler dans un répertoire temporaire dans lequel on copiera ces fichiers. Il faut commencer par exécuter le script d'installation :

```bash
latex extension.ins
```

Cette action crée le ou les fichiers de commandes (typiquement `extension.sty`). Cette extension est normalement accompagnée d'une documentation qui s'obtient alors en exécutant :

```bash
latex extension.dtx
```

Il peut ici y avoir besoin de construire l'index (si un fichier `extension.idx` existe) :

```bash
makeindex -s gind.ist -o extension.ind extension.idx
```

Il est possible également qu'il puisse y avoir à construire un glossaire (si un fichier `extension.glo` existe) :

```bash
makeindex -s gglo.ist -o extension.gls extension.glo
```

Il faut ensuite déplacer ces fichiers dans les répertoires présentés ci-dessus et, enfin, reconstruire la base de données des extensions comme dans le premier cas présenté ci-dessus.

Pour information, cette méthode est également décrite dans la section 4.6 du document {ctanpkg}`Une courte (?) introduction à LaTeX2ε <lshort-french>` (en français, la {ctanpkg}`version originale <lshort>` étant en anglais).

:::{note}
Il n'est évidemment pas possible de générer la documentation en premier, parce qu'elle aura souvent besoin du fichier des commandes, par exemple pour illustrer le fonctionnement de l'extension.

Si l'on ne veut obtenir que la documentation « utilisateur », il faut ajouter la ligne `OnlyDescription` dans le fichier `extension.dtx`, avant la ligne `\DocInput{extension.dtx}`. Sinon, on obtient aussi le *listing* commenté du code, ce dont on peut ne pas vouloir se soucier et qui peut être volumineux pour les extensions importantes.
:::

### Cas plus exotiques

Le mode d'organisation et de distribution des extensions décrit ici n'est que le mode encouragé par l'équipe du {doc}`Projet LaTeX </1_generalites/histoire/c_est_quoi_latex3>` et n'est en rien une obligation. Un bon nombre d'extensions disponibles ne le suit d'ailleurs pas, pour diverses raisons :

- extensions qui se veulent utilisables aussi bien avec Plain TeX qu'avec LaTeX ;
- « vieilles » extensions écrites pour LaTeX 2.09 ;
- extensions écrites par des personnes qui veulent faire « comme ça leur plaît »...

Dans ces différents cas, il faut aller y voir de plus près pour comprendre l'organisation choisie. Les questions suivantes traitant de la {doc}`documentation des extensions </1_generalites/documentation/documents/documents_extensions/start>` pourront vous donner des pistes :

- « {doc}`Où trouver la documentation des extensions? </1_generalites/documentation/documents/documents_extensions/documentation_des_packages>` » ;
- « {doc}`Comment générer la documentation d'une extension? </1_generalites/documentation/documents/documents_extensions/documentation_des_packages3>` ».

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#packages>

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX directory structure,installation de LaTeX
```

