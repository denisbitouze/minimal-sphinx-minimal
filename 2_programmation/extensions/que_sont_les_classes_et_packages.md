# Que sont les classes et extensions LaTeX ?

LaTeX vise à être un traitement de texte le plus général possible. Toutefois, les concepteurs de LaTeX ont choisi de structurer les documents pour faciliter son utilisation, quitte à le limiter quelque peu au premier abord (et à éviter à l'utilisateur de recourir aux *primitives* de bas niveau de TeX).

## Les classes

LaTeX propose de concevoir des documents par le biais d'une *abstraction* du type du document : la *classe*. Celle-ci décrit un ensemble de règles de mises en forme (cela laisse cependant des possibilités pour des variations entre documents d'une même classe).

Par défaut, LaTeX propose quatre classes générales ({ctanpkg}`book`, {ctanpkg}`report`, {ctanpkg}`article` et {ctanpkg}`letter`) et quelques classes un plus spécialisées telles que {ctanpkg}`slides` et {ctanpkg}`ltnews`. Pour chaque classe, LaTeX fournit un *fichier de classe* que l'utilisateur va utiliser via la commande `\documentclass` en haut du document. Ainsi, un document commençant par `\documentclass{article}` composera un *article*.

Si le principe est bon, il a un défaut flagrant : les conceptions typographiques réelles fournies par les classes LaTeX ne sont pas forcément unanimement appréciées. La solution consiste alors à *affiner* la classe. Dans ce cas, un programmeur peut écrire un nouveau fichier de classe qui charge une classe existante, puis la modifie pour obtenir sa propre mise en page du document.

## Les extensions

Ceci dit, plutôt que de modifier directement un fichier de classe, l'utilisateur peut aussi adapter une classe en chargeant une ou plusieurs *extensions* (*package*, en anglais) :

- certaines vont affiner le comportement d'une classe, comme l'extension {ctanpkg}`babel` qui modifie des réglages typographiques selon la langue souhaitée pour le document ;
- d'autres fournissent de nouvelles fonctionnalités, comme l'extension {ctanpkg}`graphicx` permettant d'insérer des graphiques dans un document ou l'extension {ctanpkg}`hyperref` permet de créer des hyperliens dans un document.

La distribution LaTeX, elle-même, fournit assez peu d'extensions, mais il y en a beaucoup, développées par une grande variété d'auteurs, déposées sur le {doc}`site du CTAN </1_generalites/glossaire/qu_est_ce_que_le_ctan>` ou dans les {doc}`distributions de TeX </1_generalites/glossaire/qu_est_ce_qu_une_distribution>`. D'un point de vue pratique, les fichiers de classe et d'extension ne sont différents que par l'extension de leur nom :

- les fichiers de classe finissent par `.cls`. La classe standard LaTeX {ctanpkg}`article` est définie par un fichier appelé `article.cls` ;
- les fichiers d'extension finissent par `.sty`. L'extension {ctanpkg}`hyperref` est représentée sur le disque par un fichier appelé `hyperref.sty`.

:::{important}
Historiquement, dans LaTeX 2.09, la distinction entre classe et extension n'existait pas : tout était appelé un *style* (un style de document ou une option de style de document").
:::

______________________________________________________________________

*Source :* {faquk}`What are LaTeX classes and packages? <FAQ-clsvpkg>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,concepts,définition,classes,extensions
```

