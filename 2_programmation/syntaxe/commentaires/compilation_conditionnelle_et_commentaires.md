# Comment obtenir des commentaires ou des plages de document compilables sous conditions ?

Bien que LaTeX (ou tout autre dérivé de TeX) ne ressemble pas vraiment à un compilateur, des utilisateurs souhaitent en faire usage ainsi. Leurs besoins sont le plus souvent la « compilation conditionnelle » et les « commentaires en bloc ». Et plusieurs moyens ont été mis à disposition pour LaTeX à cette fin.

Les simples `\newcommand{\commentaire}[1]{}` et `\iffalse ... \fi` ne sont pas vraiment satisfaisants pour faire des commentaires, puisque le bloc de texte ignoré est néanmoins analysé par TeX. La présence de cette analyse impose des restrictions sur ce que vous êtes autorisé à éviter. Si ce n'est pas un problème *aujourd'hui*, cela pourrait devenir une belle épine dans le pied demain. Comme dans le cas suivant :

```latex
% !TEX noedit
\iffalse % ignore ce qui suit
observons ce qui se passe si nous
utilisons \verb+\iftrue+. Oui, une
surprise.
\fi
```

Le `\iftrue` est repéré par TeX pendant qu'il scanne, ignorant la commande `\verb`. Aussi, le `\iffalse` n'est pas terminé par le `\fi` suivant. Pour sa part, la commande `\commentaire` définie plus haut ne se montre pas très efficace pour traiter tout ce qui n'est pas trivial, puisque tout ce qui doit être ignoré est copié dans la pile d'arguments avant d'être ignoré.

## Par l'insertion conditionnelle de fichiers

### Avec les commandes « \\include » et « \\includeonly »

Si vous avez besoin d'un document où vous pouvez escamoter des chapitres entiers (ou portions similaires de texte), essayez le système LaTeX des commandes `\include` et `\includeonly` (en tenant compte des remarques de la question « [Que fait la commande « \\include » ?](/3_composition/document/que_fait_vraiment_include) »).

Si vous utilisez `\include` vos fichiers (plutôt que `\input`), LaTeX constitue une trace de ce qui se passe à la fin de chaque chapitre dans le fichier `aux`. Ensuite, avec la commande `\includeonly`, vous pouvez donner à LaTeX une liste exhaustive des fichiers que vous voulez intégrer dans le document. Les fichiers qui n'obtiennent pas `\include` sont entièrement ignorés, mais le traitement du document continue *comme s'ils étaient là* et les numéros de page, de note de bas de page et autres ne sont pas perturbés. Notez que vous pouvez choisir les sections que vous souhaitez inclure de manière interactive, en utilisant l'extension {ctanpkg}`askinclude`.

### Avec l'extension « stampinclude » (et « pdftexcmds »)

Une variante du mécanisme `\includeonly` est proposée par l'extension {ctanpkg}`stampinclude` qui tire parti de la commande `pdfTeX` `\pdffilemoddate`. Lorsqu'un fichier inséré avec la commande `\include` est traité dans un document LaTeX, un fichier `aux` est créé contenant des données telles que des plages de numéros de page et des numéros de chapitre/section. Lorsque `\stampinclude` est inclus dans un document, il compare les heures de modification pour chaque fichier inséré et son fichier `aux` correspondant. Le fichier n'est compilé dans "cette exécution" du document que si le fichier est plus récent que son fichier `aux` correspondant. L'extension nécessite une version récente de `pdfTeX` et fonctionne également avec `LuaTeX` si l'extension {ctanpkg}`pdftexcmds` est disponible (elle émule les commandes `pdfTeX` requises à l'aide de `lua`. Hormis cet éventuel réglage, {ctanpkg}`stampinclude` nécessite peu d'attention : incluez-le dans votre document et il fait son travail en silence. Lorsque vous voulez une version finale de votre document, supprimez tous les fichiers `aux` fichiers, et {ctanpkg}`stampinclude` n'interférera pas.

### Avec l'extension « excludeonly »

Une fonctionnalité inverse peut être obtenue avec l'extension {ctanpkg}`excludeonly` : cela vous permet d'exclure une liste de fichiers insérés avec `\include`, au moyen d'une commande `\excludeonly`.

### Avec les extensions « pagesel » et « selectp »

Si vous souhaitez sélectionner des pages particulières de votre document, utilisez les extensions {ctanpkg}`pagesel` ou {ctanpkg}`selectp` de Heiko Oberdiek. Vous pouvez faire quelque chose de similaire, à savoir insérer des pages d'un document PDF existant (que vous avez peut-être compilé en utilisant `pdflatex` en premier lieu) dans votre document, en utilisant l'extension {ctanpkg}`pdfpages`. Le travail est ensuite fait avec un document ressemblant à :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[final]{pdfpages}
\begin{document}
\includepdf[pages=30-40]{votresource.pdf}
\end{document}
```

Pour inclure tout, vous pouvez saisir la commande sans indiquer le numéro de première et de dernière page :

```latex
% !TEX noedit
\includepdf[pages=-]{votresource.pdf}
```

## Par l'insertion conditionnelle de morceaux de texte

### Avec l'extension « comment »

L'extension {ctanpkg}`comment` vous permet de déclarer les zones d'un document à inclure ou à exclure. Vous faites ces déclarations dans le préambule de votre dossier. La commande '' \\includecomment\{nom-version} '' déclare un environnement `nom-version` dont le contenu sera inclus dans votre document, tandis que `\excludecomment{nom-version}` définit un environnement dont le contenu sera être exclu du document. L'extension utilise une méthode d'exclusion assez robuste et peut faire face à des groupes de texte mal formés (par exemple, avec des accolades déséquilibrées ou des commandes `\if`.

### Avec les extensions « version » et « versions »

L'extension {ctanpkg}`version` offre des fonctionnalités similaires à celles de {ctanpkg}`comment`, c'est-à-dire les commandes `\includeversion` et `\excludeversion`. Cette extension prend moins de place mais s'avère moins robuste : en particulier, elle ne peut pas traiter de très grandes zones de texte incluses/exclues.

Une évolution de {ctanpkg}`version`, appelée {ctanpkg}`versions`, ajoute une commande `\markversion{nom-version}` qui définit un environnement qui imprime le texte inclus, avec une marque claire autour.

### Avec l'extension « optional »

L'extension {ctanpkg}`optional` définit une commande `\opt` : son premier argument est un « indicateur d'inclusion » et son second est le texte à inclure ou à exclure. Le texte à inclure ou à exclure doit être bien correct (les accolades bien appariées par exemple) et ne doit pas être trop conséquent. Si un texte volumineux est nécessaire, la commande `\input` doit être utilisée dans l'argument. La {texdoc}`documentation de l'extension <optional>` vous indique comment déclarer quelles sections doivent être incluses : cela peut être fait dans le préambule du document mais la documentation suggère également des moyens de le faire dans la ligne de commande appelant LaTeX, ou de manière interactive.

### Avec l'extension « verbatim »

Autre extension, et pas des moindres, {ctanpkg}`verbatim` définit un environnement `comment` qui permet à l'utilisateur de mettre en commentaire facilement (s'il sait bien utiliser son éditeur de texte) des morceaux d'un fichier source LaTeX. La classe {ctanpkg}`memoir` offre le même environnement.

### Avec l'extension « xcomment »

Une variante intéressante est proposée par l'extension {ctanpkg}`xcomment`. Elle définit un environnement dont tout le corps est exclu, à l'exception des environnements nommés dans son argument. Ainsi, par exemple :

```latex
% !TEX noedit
\begin{xcomment}{figure,table}
  Ce texte n'est pas inclus
  \begin{figure}
    Cette figure est incluse
  \end{figure}
  Ce texte n'est pas plus inclus
  \begin{table}
    Cette table est incluse
  \end{table}
\end{xcomment}
```

### Avec l'extension « tagging »

L'extension {ctanpkg}`tagging` met en place une syntaxe qui permet à l'utilisateur d'appliquer des « balises » à des morceaux de texte et d'inclure ou exclure du texte balisé en fonction des balises. Par exemple, l'utilisateur peut « activer » du texte marqué avec certaines balises et « supprimer » du texte marqué avec d'autres :

```latex
% !TEX noedit
\usetag{⟨liste de balises⟩}
\droptag{⟨liste de balises⟩}
```

Dans les listes, les balises sont séparées par des virgules. Les commandes de balisage du texte sont ici :

```latex
% !TEX noedit
\tagged{⟨liste de balises⟩}{⟨texte⟩}
\untagged{⟨liste de balises⟩}{⟨texte⟩}
```

La première commande affiche le ⟨texte⟩ si la ⟨liste de balises⟩ contient une des balises présentes dans la commande `\usetag`. La seconde commande ne produit le ⟨texte⟩ que si la ⟨liste de balises⟩ ne contient aucune des balises mentionnées dans la commande `\droptag`.

D'autres commandes proposent une configuration si-alors-sinon et spécifient des environnements `taggedblock` et `untaggedblock`.

## Par une extraction de code, avec l'extension « extract »

Un autre aspect important du problème est couvert par l'extension {ctanpkg}`extract`. Elle vous permet de produire une « copie partielle » d'un document existant. Elle a été développée pour permettre la production d'un « livre d'exemples » à partir d'un ensemble de notes de cours. La {texdoc}`documentation de l'extension <extract>` montre l'utilisation suivante :

```latex
% !TEX noedit
\usepackage[
  active,
  generate=demonstration,
  extract-env={figure,table},
  extract-cmd={chapter,section}
]{extract}
```

Ceci crée un fichier ''demonstration.tex '' contenant tous les environnements `figure` et `table` ainsi que toutes les commandes `\chapter` et `\section` du document en cours de traitement. Le fichier ''demonstrationr.tex '' est généré au cours d'une compilation ordinaire du document principal. Cette extension fournit un bon nombre d'autres fonctionnalités, y compris des plages (numériques ou étiquetées) d'environnements à extraire et un environnement `extract` que vous pouvez utiliser pour créer des documents LaTeX complets prêts à l'emploi avec les éléments que vous avez extraits.

______________________________________________________________________

*Source :* {faquk}`Conditional compilation and « comments » <FAQ-conditional>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,compilation conditionnelle,différents PDF à partir d'un document LaTeX,compiler des parties d'un document
```

