# Commenter ses fins de lignes

Une autre astuce un peu technique qui peut vous épargner bien des désagréments. Comme vous le savez peut-être, quand TeX lit votre document, il traduit chaque fin de ligne en espace. La plupart du temps (quand vous saisissez le texte d'un paragraphe sur plusieurs lignes par exemple), c'est exactement le comportement qu'il faut pour vous simplifier la vie.

Il y a d'autres circonstances où ce comportement peut être gênant. Notamment, quand vous définissez une macro un peu longue, ou que vous faites quelque chose de compliqué, vous pouvez avoir envie de présenter votre code de façon aérée sur plusieurs lignes (et indenté) pour le rendre plus lisible, comme dans l'exemple suivant.

```latex
% !TEX noedit
\def\@protected@testopt#1{%
\ifx\protect\@typeset@protect
\expandafter\@testopt
\else
\@x@protect#1%
\fi}
```

Dans ces circonstances, vous ne voulez pas que les retours à la ligne purement liés à l'esthétique de votre code introduisent des espaces dans votre document quand votre macro sera utilisée au milieu du texte. Pour cela, prenez l'habitude d'insérer un caractère de commentaire « `%` » à la fin de la ligne (ce qu'on appelle souvent « commenter ses fins de ligne »).

Vous n'êtes pas obligé de commenter les fins de ligne qui suivent un mot de contrôle car, vous le savez (cf noms des macros ), les espaces sont ignorés après un mot de contrôle. Vous n'avez pas non plus besoin de commenter la fin de ligne après un chiffre qui n'est pas une spécification d'argument, car, je l'ai déjà évoqué, il est prudent de faire suivre les nombres (sauf spécification d'argument) d'un espace qui sert à les terminer, et est donc avalé par TeX comme l'espace qui termine un mot de contrôle.

Pour vous convaincre que je ne raconte pas de bêtises, que les fins de lignes introduisent bien des espaces que vous de désirez peut-être pas, et qu'on les évite facilement en commentant ses fins de lignes, je vous propose de regarder l'exemple suivant.

```latex
% !TEX noedit
\fbox{
Du texte.
}
\quad
\fbox{%
Du texte.%
}
```

```{image} https://web.archive.org/web/20140608074142im/https://elzevir.fr/imj/latex/img/eol.png
:alt: https://web.archive.org/web/20140608074142im/https://elzevir.fr/imj/latex/img/eol.png
```

Prenez donc l'habitude de systématiquement commenter les fins de lignes qui le nécessitent quand vous écrivez des macros ou que vous présentez du code sur plusieurs lignes. Ceci vous épargnera des désagréments qui peuvent aller bien au-delà d'un espace en trop dans votre document.

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#eol>

