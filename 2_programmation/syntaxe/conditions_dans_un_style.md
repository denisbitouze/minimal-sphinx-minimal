# Comment utiliser des tests « si... alors... » ?

- Le package {ctanpkg}`ifthen` permet de faire ce genre de choses de façon assez simple, avec en particulier les commandes `\ifthenelse` et `\whiledo` :

```latex
\documentclass[french]{article}
  \usepackage{ifthen}
  \pagestyle{empty}

\newcommand*{\ecrireLettre}[1]{%
\hspace{4ex}%
\ifthenelse{\equal{#1}{M}%
            }{Cher Monsieur,}{Chère Madame,}

J'ai le plaisir de vous inviter\dots
}

\begin{document}

\ecrireLettre{F}

---

\ecrireLettre{M}

\end{document}
```

- TeX fournit également des structures conditionnelles. Notamment les commandes `\if`, `\ifx`, `\ifnum`...
- On peut également définir des variables booléennes en TeX : on procédera de la façon suivante : on déclare un booléen avec la commande `\newif`. On peut ensuite modifier sa valeur, et définir des commandes dont le comportement dépend de cette valeur. Par exemple

```latex
% !TEX noedit
\newif\ifcondition
\conditionfalse

La condition est \ifcondition vraie\else fausse\fi.
```

La commande `\conditionfalse` met la valeur de la variable `condition` à faux. L'inverse eût été réalisé par la commande `\conditiontrue`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,ifthenelse,structures conditionnelles,si...alors,conditions en TeX,conditions en LaTeX
```

