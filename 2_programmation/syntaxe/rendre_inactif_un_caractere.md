# Comment rendre inactif un caractère ?

- La commande `\string` permet de rendre inactif le caractère qui la suit. Plus précisément, si le caractère qui la suit est un caractère « actif » (comme `_` ou `^`), `\string` affiche ce caractère sans l'interpréter, sauf si c'est une contre-oblique, auquel cas elle affiche le nom complet de la commande qui la suit. La nuance n'a pas d'importance dans le cas de `\string\LaTeX` par exemple, mais a une importance dans le cas de `\string\}`.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

