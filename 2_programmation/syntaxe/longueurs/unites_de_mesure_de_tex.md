# Quelles sont les unités de mesure de TeX ?

Pour TeX, il existe 11 unités de longueur, plus une qui n'existe qu'en mode mathématique :

| Unité | Nom                         | Définition                                                                    | Exemple d'une longueur d'une unité                                                                                                                            |
| ----- | --------------------------- | ----------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `sp`  | *scaled point*              | La plus petite unité de TeX. 1 `sp` = 1/65536 `pt` soit 5,4 `nm` | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1sp}`      |
| `pt`  | point d'imprimeur (*point*) | 1 `pt` = 1/72,27 `in`, environ 0,351 `mm`                                     | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1pt}`      |
| `bp`  | gros point (*big point*)    | 1 `bp` = 1/72 `in`                                                            | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1bp}`      |
| `dd`  | didot (*Didot point*)       | 1 `dd` = 1,07 `pt`, environ 0,376 `mm`                                     | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1dd}`      |
| `mm`  | millimètre                  | 1 `mm` = 0,001 `m`                                                            | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1mm}`      |
| `pc`  | pica                        | 1 `pc` = 12 `pt`                                                              | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1pc}`      |
| `cc`  | cicero                      | 1 `cc` = 12 `dd`                                                              | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1cc}`      |
| `cm`  | centimètre                  | 1 `cm` = 0,01 `m`                                                             | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1cm}`      |
| `in`  | pouce (*inch*)              | 1 `in` = 25,4 `mm`                                                            | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1in}`      |
| `ex`  | « hauteur d'x »             | La hauteur d'un « `x` » dans la fonte courante                                | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1ex}`      |
| `em`  | « largeur d'M »             | La largeur d'un « `M` » dans la fonte courante                                | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1em}`      |
| `mu`  | « mu »                      | 1 `mu` = 1/18 `em` (unité valable en mode mathématique uniquement)            | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{0.0555em}` |

:::{note}
C'est le concepteur de la fonte qui décide la largeur `em` et peut tout à fait décider que ce ne sera pas exactement la largeur d'un « M ».
:::

En complément de ces mesures, pdfTeX définit deux autres mesures :

| Unité | Nom          | Définition          | Exemple d'une longueur d'une unité                                                                                                                       |
| ----- | ------------ | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `nd`  | *new didot*  | 1 `nd` = 0,375 `mm` | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1nd}` |
| `nc`  | *new cicero* | 1 `nc` = 12 `nd`    | {raw-latex}`\def\unitl#1{\vrule height 3pt depth 0pt width .4pt\vrule height .4pt depth 0pt width #1 \vrule height 3pt depth 0pt width .4pt}\unitl{1nc}` |

```{eval-rst}
.. meta::
   :keywords: LaTeX,unité,unités en TeX,unités de mesure, unités de longueur, cm, pt, in, ex, em
```

