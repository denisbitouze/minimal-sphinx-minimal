# Comment vérifier la syntaxe de mon code LaTeX ?

Les vérificateurs de syntaxe sont soit des extensions LaTeX, soit des programmes externes, qu'ils soient indépendants ou inclus dans des environnements intégrés de développement (IDE). Dans ce dernier cas, la vérification syntaxique peut se faire en même temps que l'on saisit le code (« en ligne »), ou par appel d'une option de menu.

## Vérifier la syntaxe de son document avec l'extension « syntonly »

- Si vous travaillez sur un gros document et que vous souhaitez savoir rapidement si votre code est correct ou non, vous pouvez utiliser l'extension {ctanpkg}`syntonly`. Elle permet de compiler votre document sans produire de fichier de sortie, ce qui fait gagner du temps.

Ajoutez simplement ces deux lignes à votre préambule :

```latex
% !TEX noedit
\usepackage{syntonly}
\syntaxonly
% suite ...
```

LaTeX ne produira plus aucune page, mais il vérifiera que votre syntaxe est correcte et que votre document compile. Le `log` devrait dire quelque chose comme ça :

```
No pages of output.
Transcript written on mon_document.log.
```

Pour restaurer un fonctionnement normal, mettez juste la commande `\syntaxonly` en commentaire :

```latex
% !TEX noedit
\usepackage{syntonly}
% \syntaxonly
```

:::{tip}
À titre de test, nous avons compilé {texdoc}`la documentation de la classe « memoir » <memman>` avec et sans `\syntaxonly` sur un MacBook pro de 2018, dix fois :

| `\syntaxonly` | Durée de la compilation |
|---------------|-------------------------|
| **Sans**      | 4,33s ± 0,06            |
| **Avec**      | 1,92s ± 0,04            |

soit un gain d'un facteur supérieur à 2 pour un document de 567 pages.
:::

- Le package {ctanpkg}`refcheck` permet, lui, de vérifier les références d'un document. Il suffit de le charger :

```latex
% !TEX noedit
\usepackage{refcheck}
```

puis de compiler deux fois le document. Il signale les étiquettes inutiles, les équations non référencées, les références bibliographiques non appelées, et peut vous aider à maintenir votre document en affichant les noms des étiquettes `\label` dans la marge.

Il n'est pas influencé par les options `final`/`draft`. Il faudra donc bien penser à le retirer manuellement pour produire la version finale de votre document !

:::{important}
Attention à charger {ctanpkg}`refcheck` **après** {ctanpkg}`amslatex` et {ctanpkg}`hyperref`, car il modifie certaines de leurs macros.
:::

- L'extension {ctanpkg}`nag` n'est pas à proprement parler un vérificateur de syntaxe, mais elle vous prévient si vous utilisez des commandes, extensions ou classes considérées comme obsolètes.

## Des vérificateurs externes de syntaxe LaTeX ?

- {ctanpkg}`lacheck`, disponible avec la distribution {ctanpkg}`AUCTeX <auctex>` (mode AllTeX pour `Emacs`), est capable de vérifier la syntaxe LaTeX et de détecter les erreurs les plus fréquentes.
- Le programme {ctanpkg}`chktex` est capable de détecter des erreurs typographiques dans du code LaTeX. Il permet de s'affranchir de certaines constructions LaTeX non intuitives. Des binaires pour Unix, Amiga, MS-DOS et OS/2 sont disponibles. Le programme étant écrit en ANSI C, il devrait aussi fonctionner sur tous les Windows après recompilation.

Quelques exemples de services :

- pas d'espace ou assimilé après/avant une parenthèse;

- vérifie la cohérence du style;

- met en garde en cas d'utilisation de primitives TeX;

- gère les `left` et `right` erronés;

- indique les espaces multiples qui ne sont pas équivalents à un seul;

- gère la ponctuation en mode mathématique;

- indique les espaces avant une note de bas de page;

- gère les fichiers inclus;

- détecte les blancs avant une référence au lieu de `~`;

- vérifie les couples de parenthèses;

- gère l'espace après un passage en italique;

- ...

Cet outil est configurable.

______________________________________________________________________

*Sources :*

- [Check syntax only, save time typesetting!](https://texblog.org/2011/02/02/check-syntax-only-save-time-typesetting/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,validation de syntaxe,code LaTeX,document long en latex,gros document latex
```

