# Comment utiliser le mode verbatim dans une commande ?

- Le package {ctanpkg}`fancyvrb` fournit la commande `\VerbatimFootnotes` prévue à cet effet. Voir l'exemple ci-dessous.
- Cependant, `\VerbatimFootnotes` ne suffit pas dans certaines commandes. Il faut alors utiliser `\SaveVerb` et `\UseVerb` (toujours dans le package {ctanpkg}`fancyvrb`). Par exemple :

```latex
% !TEX noedit
\DefineShortVerbatim{\|}
\SaveVerb{Verb}|\verb|
Du texte...
\marginpar{du \UseVerb{Verb} en marge}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

