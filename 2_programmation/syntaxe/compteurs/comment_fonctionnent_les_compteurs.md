# Comment redéfinir les commandes de compteur `\the(...)` ?

Chaque fois que vous demandez un nouveau compteur à LaTeX, il crée en coulisses de nombreuses commandes ainsi que la définition du compteur lui-même. Entre autres choses, `\newcounter{⟨truc⟩}` crée une commande `\the⟨truc⟩`, qui se développe en « la valeur de `⟨truc⟩` » dans votre texte mis en forme.

La définition de `\the⟨truc⟩` devrait se limiter à exprimer la valeur du compteur : c'est presque toujours une erreur d'utiliser la commande pour produire autre chose. La valeur peut raisonnablement être exprimée sous la forme d'un nombre arabe, romain ou grec, sous forme d'expression alphabétique ou même sous la forme d'une séquence (ou d'un motif de) symboles. Si vous avez besoin d'un processus de décision sur l'opportunité de redéfinir `\the⟨truc⟩`, pensez à ce qui pourrait arriver lorsque vous le faites. Voici deux exemples illustrant les difficultés possibles :

- par exemple, si vous voulez que vos numéros de section se terminent par un point, vous pouvez faire en sorte que `\thesection` se développe avec un point final. Cependant, un tel changement de `\thesection` rend la définition de `\thesubsection` étrange... Utilisez plutôt ici les techniques standard pour {doc}`ajuster la présentation des numéros des titres de sectionnement </3_composition/texte/titres/changer_la_presentation_des_numeros_de_sections>`.
- si vous voulez que le numéro de page apparaisse au bas de chaque page entouré de tirets (tel « ~ N ~ ») et que vous essayez d'y parvenir en redéfinissant `\thepage`, des problèmes surgiront de l'utilisation du numéro de page dans la table des matières (chaque numéro aura les tirets attachés), et les références `\pageref` seront également modifiées. Dans ce cas, le changement d'apparence sera mieux exécuté en redéfinissant le style de page lui-même, par exemple avec l'extension {ctanpkg}`fancyhdr`. Sur ce point, voir la question « {doc}`Comment définir les hauts et bas de page ? </3_composition/texte/pages/entetes/composer_des_en-tetes_et_pieds_de_page>` ».

______________________________________________________________________

*Source :* {faquk}`Redefining counters' \the-commands <FAQ-the-commands>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,compteurs,redéfinition
```

