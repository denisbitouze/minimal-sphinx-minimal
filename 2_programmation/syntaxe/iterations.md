# Comment programmer un traitement itératif ?

- Le package {ctanpkg}`multido` permet de faire des boucles similaires aux boucles `for` qui existent dans différents langages de programmation. Noter que TeX fournit déjà un tel mécanisme, mais le package {ctanpkg}`multido` en simplifie l'utilisation.

La commande `\multido` prend trois arguments :

- le premier définit les variables ainsi que la façon dont elles doivent évoluer;
- le deuxième spécifie le nombre d'itérations à faire;
- enfin le troisième contient le code qui doit être exécuté.

L'exemple suivant montre son fonctionnement : on affiche pour commencer une flèche de longueur 8 cm, puis, tous les centimètres (à l'aide de la variable `\i`), on trace un petit trait et on écrit la valeur du compteur `\n`. On obtient donc une flèche graduée :

```latex
% !TEX noedit
\setlength{\unitlength}{1cm}
\small

\begin{picture}(8,1)(0,-.5)
   \put(0,0){\vector(1,0){8}}
   \multido{\i=0+1, \n=0+0.25}{8}{%
     \put(\i,-.1){\line(0,1){.2}}
     \put(\i,-.2){\makebox(0,0)[t]{\n}}
   }
\end{picture}
```

```latex
\documentclass{article}
  \usepackage{multido}
  \pagestyle{empty}

\begin{document}
\setlength{\unitlength}{1cm}
\small

\begin{picture}(8,1)(0,-.5)
   \put(0,0){\vector(1,0){8}}
   \multido{\i=0+1, \n=0+0.25}{8}{%
     \put(\i,-.1){\line(0,1){.2}}
     \put(\i,-.2){\makebox(0,0)[t]{\n}}
   }
\end{picture}
\end{document}
```

:::{important}
Les noms de variables doivent avoir leur première lettre dans l'ensemble \$\\{d, D, i, I, n, N, r, R\\}\$, \$d/D\$ pour des variables de type dimension, \$i/I\$ pour des variables de type entières, \$n/N\$ pour des variables de type nombre et \$r/R\$ pour des variables de types réels. La différence entre le type nombre et le type réel, c'est que le premier utilise le package {ctanpkg}`fp` pour effectuer les calculs alors que le type réel fait une conversion des nombres en les exprimant comme des dimensions en interne (avec l'unité `sp`). Donc `Dxy`, `intx`, `ntwo` et `reel` sont des variables licites mais pas `benquoi`.
:::

- Toutefois, l'utilisation du package {ctanpkg}`multido` peut poser des problèmes avec les tableaux. Les utilisateurs d'eTeX peuvent se servir de la macro `For` de cet exemple :

```latex
% !TEX noedit
\documentclass{article}

%% Définition de \For :
\makeatletter
\newcommand*{\For}[1]
  {\noalign{\gdef\@Do##1{#1}}\@For}
\newcommand*{\@For}[3]{%
  \unless\ifnum#1 \ifnum#3>0 >\else <\fi #2
    \@Do{#1}%
    \expandafter\@For%
    \expandafter{\number\numexpr#1+#3}{#2}{#3}%
  \fi
}
\makeatother

\begin{document}
%% Utilisation de \For :
%% \For{⟨corps⟩}{⟨deb⟩}{⟨fin⟩}{⟨pas⟩}
%% #1 pour désigner le compteur de boucle
\begin{tabular}{ll}
\For{#1&#1#1\\}{0}{10}{2}
\end{tabular}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,répéter une commande,boucles en LaTeX,itérations
```

