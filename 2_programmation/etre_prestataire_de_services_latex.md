# Comment travailler comme prestataire de services LaTeX ?

:::{note}
Le contenu actuel de cette page provient du [LaTeX Ninja blog](https://latex-ninja.com/2019/02/04/planning-your-project-for-service-providers/), avec l'aimable autorisation de son auteur, Sarah Lang.

```{eval-rst}
.. todo:: Il est destiné à être adapté au style de la FAQ.
```
:::

En rédigeant la réponse à la question « [Peut-on gagner de l'argent avec LaTeX?](/1_generalites/peut_on_devenir_riche_avec_latex) », il est apparu qu'il y a beaucoup de conseils à donner sur la planification et l'ordonnancement d'un projet de prestation de service avec LaTeX. Je vais donc résumer mes expériences avec toutes sortes de « clients » (qu'il s'agisse de partenaires de projet ou autres).

## Motivation

Il ne s'agit pas de se plaindre de l'horreur de la situation, mais simplement de résumer quelques éléments à prendre en compte, qu'une personne inexpérimentée ne trouvera peut-être pas évidents. Comme l'époque où j'étais moi-même inexpérimenté n'est pas très éloignée, le processus d'apprentissage est encore assez frais et j'ai encore en tête les problèmes auxquels un débutant peut être confronté, j'espère donc être en mesure de fournir des conseils précieux.

J'espère donc pouvoir vous donner des conseils utiles. Certains de ces conseils sont tirés du billet « [Peut-on gagner de l'argent avec LaTeX?](/1_generalites/peut_on_devenir_riche_avec_latex) », alors ne vous inquiétez pas si vous avez l'impression d'avoir déjà lu certains d'entre eux. Au départ, je voulais prolonger l'ancien article, mais comme il était déjà trop long, j'ai décidé d'en faire un article à part entière. C'est également la raison pour laquelle je commencerai par la production de PDF prêts à imprimer pour une maison d'édition, puisque c'est là que j'ai commencé mes considérations dans le dernier article.

### La communication entre deux mondes

Votre travail consiste à communiquer les informations techniques de manière à ce qu'elles soient compréhensibles pour le client (probablement non technicien). Vous devez donc comprendre son langage pour en déduire ce qu'il souhaite. Ensuite, vous devez le reformuler d'une manière un peu plus technique et le « retraduire » pour vérifier que vous avez bien compris. Cette étape est nécessaire car, la plupart du temps, les exigences que vous recevez ne sont pas assez détaillées et il y a beaucoup de décisions à prendre dans le processus de planification de la mise en œuvre puis la mise en œuvre effective de ces exigences.

### Paradigmes et méthodologies de production

Dans le passé, les gens utilisaient le modèle dit « en cascade », ce qui signifie qu'ils obtenaient une fois les exigences du client et les mettaient en œuvre de la sorte, en contournant tout manque d'information qu'ils pouvaient rencontrer. Les méthodologies modernes s'orientent vers des cycles de production plus courts où l'on produit rapidement un « exemple fonctionnel minimal » afin de pouvoir vérifier si c'est bien ce que veulent les clients. S'il y a des décisions supplémentaires à prendre ou des informations manquantes, vous pouvez vérifier à nouveau avec les clients immédiatement. Vous essayez d'éviter d'implémenter des fonctionnalités inutiles que vous trouvez cool mais que le client n'a pas demandées. Pour l'instant, je m'en tiendrai à cette très brève information sur les méthodologies. J'y consacrerai peut-être un article à part entière à l'avenir.

### Les pièges de l'organisation du travail : exemple de la production d'un document prêt à imprimer

Ce n'est pas parce que vous connaissez un peu LaTeX que vous saurez nécessairement comment répondre aux besoins d'une maison d'édition. Trouvez d'abord une occasion de travailler avec eux gratuitement. Là encore, les petites maisons d'édition ou les éditeurs universitaires offrent souvent la possibilité de remettre le PDF final, ce qui revient beaucoup moins cher que de le faire mettre en page. Cela permettra au scientifique en question d'économiser beaucoup d'argent (500 à 2000 € selon la quantité de travail). Proposez à un professeur d'université de composer son livre gratuitement. Ainsi, vous aurez interagi avec l'éditeur au moins une fois, vous aurez ainsi une sorte d'une accréditation et vous saurez ce que les éditeurs veulent. Ne sous-estimez pas non plus le temps que cela peut prendre. Ce n'est pas pour rien qu'un travail comme celui-ci peut rapporter jusqu'à 2000 €. Les publications scientifiques, en particulier, font souvent l'objet de millions de modifications et de corrections avant d'être prêtes à être imprimées.

Au cours du processus, vous vous rendrez peut-être compte que produire un PDF qui correspond exactement aux critères de l'éditeur est plus difficile que vous ne le pensez. En tout cas, c'est ce qui s'est passé pour moi. J'avais utilisé LaTeX et je savais comment faire les personnalisations nécessaires pour satisfaire mes encadrants de thèse, mais l'éditeur a fini par vouloir des tonnes de changements que je n'avais jamais imaginé faire en LaTeX. Des encadrants ou collègues seront rapidement contents avec d'une sortie joliment mise en page et des références bibliographiques correctes. Les éditeurs, en revanche, veulent un aspect visuel très spécifique. Par exemple, la première chose que j'ai dû changer est la composition des notes de bas de page, dont aucun de mes encadrants ne s'était jamais soucié. Je n'avais jamais personnalisé de notes de bas de page auparavant et je n'étais même pas conscient que cela pouvait être un problème. Donc, si vous n'avez jamais fait l'une de ces choses, je vous suggère de commencer à vous documenter sur le sujet dès maintenant.

Cela représentera probablement beaucoup plus de travail que vous ne l'aviez prévu et ce travail pourrait également être assez différent de ce que vous avez l'habitude de faire avec LaTeX. Anticipez cela (!). Les maisons d'édition ont également tendance à trouver des problèmes typographiques auxquels vous n'aviez même pas songé auparavant.

### Planifier le travail

En dehors des éditeurs traditionnels, j'ai constaté que tout client est vraiment comme ça. La version du texte (avec les figures et tout le reste) que l'on vous donne n'est jamais la version finale. Vous le mettez en page et pensez avoir terminé, mais ce n'est qu'à ce moment-là qu'ils remarquent les millions de fautes de frappe et autres choses à corriger. La première « version finale » ainsi que les réunions de préparation et de consultation représentent environ la moitié du travail/des heures que vous devez prévoir (croyez-moi !). J'ai constaté qu'en général, les corrections représentent au moins autant de travail que la création de cette « première version finale », même si j'ai dû créer toute la mise en page et certains dessins en Ti*k*Z pour cette première version. Après cela, le travail de correction semble augmenter de façon exponentielle ! Ce n'est pas nécessairement une mauvaise chose si vous le savez et si vous planifiez en conséquence. Cependant, si vous pensez comme un débutant et que vous ne vous sentez pas assez sûr de vous pour dicter vos conditions et communiquer vos besoins, ils ne seront pas satisfaits. Vous risquez de vous retrouver dans une situation délicate. C'est aussi un excellent conseil en matière de relations, d'ailleurs. 😉

### Le premier « document final » qu'ils vous remettent est en fait le premier brouillon

C'est très énervant et je réfléchis encore à la manière d'éviter que cela ne se produise (à nouveau) parce que souvent, c'est juste dû à un manque de diligence de leur part. Mais bien sûr, une fois que leur document est tapé en LaTeX, vous êtes le seul à savoir encore comment faire des changements. Anticipez cela et incluez aussi, probablement (secrètement), au moins 10 heures de plus que nécessaire dans le tarif (ou l'estimation du temps nécessaires) pour les corrections (même pour les minuscules mini-projets et probablement surtout ceux-là car les gens ont tendance à être encore plus désinvoltes avec eux). Il y aura beaucoup de corrections à faire. Si vous trouvez un moyen d'éviter cela, merci de m'en informer. 😉 Jusqu'à présent, je n'ai pas eu beaucoup de succès. Les gens ne voient les coquilles ou les problèmes visuels que dans la version finale entièrement mise en page. Mais ces corrections supplémentaires pour lesquelles vous n'êtes pas payé sont souvent ce qui transforme un travail secondaire rentable en un travail qui ne l'est plus. En outre, ces corrections doivent souvent être effectuées immédiatement ou à la dernière minute. Renseignez-vous donc sur la date limite et assurez-vous d'être disponible à ce moment-là. Le mieux est probablement d'être avec eux à ce moment-là (les dernières heures avant la soumission) et de les facturer de manière préventive. Car vous finirez par devoir le faire de toute façon. En outre, plus le contenu est scientifique, plus il sera probablement stressant.

Créer un « espace sûr » pour votre travail en disant « non » : apprenez à être strict avec votre client -----------------------------------------------------------------------------------------------------

N'oubliez jamais que vous êtes le dernier maillon de la chaîne alimentaire. Vos clients vous remettront les données en retard, par exemple deux heures avant la date limite, et c'est vous qui aurez la tête sur le billot, à moins que vous n'ayez très clairement établi vos conditions à l'avance. Si vous ne voulez pas que ce travail secondaire devienne stressant, **vous** devez établir le calendrier pour eux. Ils n'ont aucune idée du temps que prendront leurs propres corrections et ajouts *totalement imprévisibles*.

Donnez-leur une *dead-line* bien avant que votre tête ne soit en jeu. Imaginez que vous obteniez une erreur LaTeX à la dernière minute et que vous ne sachiez pas comment la résoudre... Aussi, pour éviter cela, créez un exemple fonctionnel minimal avant de recevoir les données finales (ce sera à la dernière minute et bien trop tard pour commencer). Exigez un « exemple minimal fonctionnel » et travaillez dessus dès la première réunion, afin qu'ils puissent donner leur avis. Ils voudront probablement tout changer. Ou, pire encore, ils vous diront que c'est parfait et vous remercieront. La chose la plus importante à savoir est la suivante : ils auront toujours un million de corrections à apporter et le processus de correction prendra au moins autant de temps que la création de la première « version finale ». Même s'ils vous disent « c'est parfait, merci mille fois » tout de suite. Les corrections arriveront plus tard. Je n'ai pas eu un seul projet où ce n'était pas le cas. Si vous avez d'autres expériences, n'hésitez pas à m'en faire part. Mais de la part de mes collègues aussi, je n'entends rien d'autre.

Vous devez le savoir et ne laissez jamais rien vous faire croire le contraire. Moins vous recevrez de corrections ou de commentaires après la première version, plus vous en recevrez, de manière exponentielle, à mesure que vous vous rapprocherez de la « vraie » date limite du projet.

### Aider le client à découvrir ce qu'il veut vraiment

Souvent, les clients finissent par se rendre compte qu'ils n'aiment plus leur propre idée une fois qu'elle est mise en œuvre. Il se peut aussi qu'ils n'aient pas d'idée précise et que vous deviez réaliser plusieurs maquettes jusqu'à ce qu'ils soient satisfaits. Mais il ne faut pas confondre l'absence d'idée propre du client avec son acceptation heureuse du résultat que vous produisez. Une fois que vous avez trouvé une idée, ils vont probablement la mettre en pièces. Cela n'a rien d'inamical, mais c'est juste un fait qu'après que vous leur ayez servi une mise en œuvre concrète, ils réaliseront soudain exactement ce qu'ils auraient voulu.

:::{tip}
Indice : ce qu'ils veulent est probablement l'exact opposé de ce que vous venez de concevoir à la sueur de votre front.
:::

### Protégez-vous en insistant sur le fait que les fonctionnalités supplémentaires sont toutes très difficiles à mettre en œuvre (faites payer un supplément)

Je ne dis pas cela pour vous décourager mais juste pour vous donner une idée réaliste de la façon dont cela va se passer. Il en va de même pour tout type de travail où vous produisez une implémentation technique pour un « client » qui vous remettra les données à représenter. Il peut s'agir d'un projet basé sur XML dans le domaine des humanités numériques ou d'un poster à imprimer utilisant LaTeX. Par ailleurs, un autre conseil venant du monde des humanités numériques : dites toujours que tout est incroyablement difficile et prend beaucoup de temps, et que vous n'êtes pas sûr que cela puisse être fait du tout si les gens demandent des choses supplémentaires qui ne faisaient pas partie du contrat auparavant. J'avais l'habitude de détester cela et de penser que c'était un signe de « mauvais service » ou de paresse, mais c'est vraiment une précaution nécessaire. Vous devez protéger votre temps, sinon vous finirez par travailler gratuitement. Si nécessaire, faites payer un supplément. Cela signifie également que vous devez rédiger un contrat très clair indiquant ce que vous acceptez de faire et pour quel montant. N'ajoutez pas de fonctionnalités supplémentaires sans rémunération supplémentaire. Une fois que vous aurez annoncé le prix, la fonctionnalité ne sera probablement plus si importante que ça.

### Faites toujours une première ébauche avec des données réelles (au moins des bribes) dès le départ

Veillez à réaliser la « maquette » en utilisant leurs vraies données, et pas uniquement des *lorem ipsum*, si possible. Leurs données ne ressemblent sans doute pas du tout à du *lorem ipsum*. Faites-le juste après la première réunion, pour que vous ayez fait votre part. S'ils se plantent et ne vous remettent pas de données avant la date limite, cela prouve que vous avez fait votre travail et ils ne pourront pas revenir sur leur décision de vous payer.

Une certaine sécurité n'est jamais une mauvaise idée. De plus, cela permet de s'assurer que vous avez fait « la plus grande partie » du travail (peut-être le premier tiers) en avance. Rappelez-vous, si vous êtes quelqu'un qui fait toujours les choses à la dernière minute, ce travail n'est probablement pas pour vous. Les clients vous remettront probablement les données bien trop tard. Si vous attendez qu'ils vous remettent les données finales pour faire la première maquette, vous n'y arriverez jamais, car ils voudront aussi donner leur avis et faire des changements.

### Spécificités des sciences humaines

Dans le domaine des sciences humaines, il est très important que vous appreniez toutes les fonctionnalités de MS Word et que vous sachiez reproduire une apparence semblable à Word, mais en mieux. S'ils vous laissent utiliser LaTeX, vous devez d'abord prouver qu'il possède toutes les fonctionnalités de MS Word auxquelles ils sont habitués.

Comme je le déplore dans mon billet sur [l'abandon de Word](https://latex-ninja.com/2019/01/11/how-to-quit-ms-word-for-good/), tout tourne autour de MS Word dans les sciences humaines. Cela signifie que même si vous choisissez de ne pas utiliser Word dans votre vie privée, vous devrez savoir comment travailler avec lui. Et surtout, comment en tirer le meilleur parti en extrayant les données pour les traiter ensuite dans un autre format.

## Dernières réflexions

Voici donc quelques réflexions générales sur le travail en tant que « fournisseur de services ». Ces expériences proviennent à la fois d'un contrat ou d'un travail en freelance avec LaTeX et d'un travail sur un projet en humanités numériques. À un moment donné, j'écrirai probablement un article spécifique sur la gestion des projets en humanités numériques. Mais pour l'instant, je tiens à souligner à nouveau que ce billet n'a pas pour but de critiquer ou de faire fuir qui que ce soit. Je voulais simplement partager certaines de mes expériences qui pourraient être utiles à quelqu'un qui cherche à se lancer, afin qu'il puisse avoir une idée de quelques éléments à prendre en compte.

______________________________________________________________________

*Sources :*

- [Planning your project for “service providers”](https://latex-ninja.com/2019/02/04/planning-your-project-for-service-providers/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,utiliser LaTeX commercialement,faire de la prestation de services LaTeX,édition de livres,graphiste LaTeX,organiser un projet,communiquer avec un client,décrire LaTeX,arguments pour LaTeX
```

