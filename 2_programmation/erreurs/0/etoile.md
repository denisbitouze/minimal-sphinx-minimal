# Que signifie le message : « * » ? Et que dois-je faire ?

- **Message** : `*`
- **Origine** : *TeX*.

Si LaTeX s'arrête en n'affichant qu'un seul [astérisque](https://fr.wikipedia.org/wiki/Astérisque), cela signifie qu'il a atteint la fin du fichier source sans voir la requête terminant le travail (c'est-à-dire `\end{document}` en LaTeX) et qu'il attend une entrée à partir du terminal. Même s'il ne s'agit pas d'une erreur en soi, dans la plupart des cas, cela indique que quelque chose s'est particulièrement mal passé. Cela a pu se produire :

- parce que vous avez omis le `\bye` (`Plain` TeX) ou le `\end{document}` (LaTeX) ;
- parce qu'une accolade ouvrante n'a pas été suivie après divers éléments par une accolade fermante ;
- ou parce qu'en LaTeX, un environnement de type `verbatim` n'a pas été fermé, ce qui a provoqué la lecture de tout le reste du document en mode « verbatim ».

Dans le premier cas (le plus simple), vous pouvez insérer le texte manquant :

- si vous utilisez `Plain` TeX, saisissez `\end` : cela terminera la compilation ;
- si vous utilisez LaTeX, saisissez `\end{document}`.

Dans les autres cas, pour trouver la source de ce problème, vous pouvez essayer de placer un `\end{foo}`, qui donnera une erreur « `Environment... ended by...` » (indiquant quel environnement LaTeX est en cours), ou bien qui sera avalé sans réaction, auquel cas vous aurez la preuve que vous êtes effectivement dans un contexte « verbatim ». Dans ce dernier cas, il suffit d'interrompre LaTeX (en tapant Ctrl-C ou ce que le système d'exploitation demande) et de taper `x` quand apparaît l'erreur, pour terminer la compilation. En réexaminant le résultat à partir de la dernière page, la façon dont le document est composé donne généralement une indication sur l'emplacement où les ennuis commencent.

______________________________________________________________________

*Sources :*

- {faquk}`Please type a command or say \\end <FAQ-typend>`,
- <https://latex.developpez.com/faq/erreurs?page=-etoile-lt>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,astérisque,étoile,invite TeX,terminer la compilation,sortir de la compilation,sortir de LaTeX
```

