# Que signifie l'erreur : « ⟨commande⟩ allowed only in math mode » ?

- **Message** : `⟨commande⟩ allowed only in math mode`
- **Origine** : extension *amsmath*.

Cette commande ou cet environnement ne peut être utilisé qu'en mode mathématique. Il faut alors vérifier attentivement ce qui a été oublié dans le document.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=-etoile-lt>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode mathématique,mode mathématiques,commande mathématique
```

