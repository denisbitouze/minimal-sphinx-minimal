# Que signifie l'erreur : « ⟨nom⟩ undefined » ?

- **Message** : `⟨nom⟩ undefined`
- **Origine** : *LaTeX*.

Cette erreur est déclenchée lorsque `\renewcommand` est utilisé avec un `⟨nom⟩` que LaTeX ne connaît pas :

- soit `⟨nom⟩` a été mal saisi ;
- soit il faut utiliser `\newcommand`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=-etoile-lt>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,truc non défini,undefined,not defined,problème avec des commandes LaTeX
```

