# Que signifie l'erreur : « destination ... duplicate ignored » ?

- **Message** : `destination with the same identifier (name{page.⟨xx⟩}) has been already used, duplicate ignored`
- **Origine** : pdftex.

Ce message survient en raison de numéros de page en double dans votre document. Le problème est généralement simple à traiter, comme l'explique la question « {doc}`Comment utiliser hyperref avec des numéros de page répétés ? </3_composition/texte/pages/numerotation_des_pages/hyperref_et_les_numeros_de_pages_identiques>` ».

Si l'identifiant dans le message est différent, par exemple `nom{figure.1.1}`,
le problème est (souvent) dû à un problème d'interaction entre
extensions. Ainsi, l'extension {ctanpkg}`hyperref` présente avec sa
{texdoc}`documentation <hyperref>` certains de ces problèmes (et leurs
solutions), comme celui survenant avec l'environnement `equation` de l'extension
{ctanpkg}`amsmath`. Certaines extensions sont aussi tout simplement
incompatibles avec {ctanpkg}`hyperref` bien que la plupart fonctionnent
simplement en l'ignorant. Par conséquent, vous devriez charger votre extension
avant de charger {ctanpkg}`hyperref` pour qu'il procède aux modifications qu'il
juge utile et enfin utiliser à la suite votre extension. Voici un exemple :

```latex
% !TEX noedit
\usepackage{float}          % definit \newfloat
\usepackage[...]{hyperref}  % corrige \newfloat
\newfloat{...}{...}{...}
```

Une des rares exceptions à ce principe est l'extension {ctanpkg}`memhfixc`. Elle adapte {ctanpkg}`hyperref` pour son utilisation avec la classe {ctanpkg}`memoir`.

Si changer l'ordre de chargement de vos extensions ne corrige pas le problème, vous devrez {doc}`demander de l'aide </1_generalites/documentation/listes_de_discussion>`.

______________________________________________________________________

*Source :* {faquk}`pdfTeX destination ... ignored <FAQ-hyperdupdest>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,erreurs,ordre de chargement,ordre de chargement des extensions,ordre de chargement des packages,extensions,paquets,packages
```

