# Que signifie l'erreur : « `\displaybreak` cannot be applied here » ?

- **Message** : `\displaybreak cannot be applied here`
- **Origine** : package *amsmath*.

Un environnement extérieur, tel que `split`, `aligned` ou `gathered`, a créé un bloc qui ne peut pas être coupé.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=D>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur d'AMSmath,problème extension amsmath
```

