# Que signifie l'erreur : « No `\title` given » ?

- **Message** : `No \title given`
- **Origine** : *LaTeX*.

Une classe LaTeX a exécuté un `\maketitle` sans déclaration `\title`. Seule `\date` est optionnelle lorsqu'on utilise cette commande.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,page de titre,titre manquant
```

