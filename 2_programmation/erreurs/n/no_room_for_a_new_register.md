# Que signifie l'erreur : « No room for a new ⟨registre⟩ » ?

- **Message** : `No room for a new ⟨registre⟩`
- **Origine** : *TeX*.

Les extensions chargées dans le document demandent plus de registres internes que ce que TeX peut proposer. Il faut compiler le document avec ε-TeX et charger également l'extension {ctanpkg}`eTeX`.

La technologie disponible pour Donald Knuth au moment de l'écriture de TeX s'avérait particulièrement médiocre dans la gestion du stockage dynamique. Par conséquent, une grande partie du stockage utilisé dans TeX se fait par l'allocation de tableaux fixes, dans les implémentations de référence. Beaucoup de ces tableaux fixes sont extensibles dans les implémentations modernes de TeX, mais la taille des tableaux de « registres » est écrite dans la spécification comme étant 256 (généralement). Cette valeur ne doit d'ailleurs pas être changé si vous souhaitez toujours appeler le résultat TeX (voir la question « {doc}`Comment être sûr que c'est vraiment TeX ? </1_generalites/bases/verifier_la_conformite_de_son_compilateur>` »).

Si vous saturez un de ces tableaux de registres, vous obtenez un message d'erreur TeX indiquant :

```latex
% !TEX noedit
! No room for a new \⟨truc⟩.
```

Le `\⟨truc⟩` en question peut être :

- `\count`, l'objet sous-jacent à la commande `\newcounter` de LaTeX ;
- `\skip`, l'objet sous-jacent à la commande `\newlength` de LaTeX ;
- `\box`, l'objet sous-jacent à la commande `\newsavebox` de LaTeX ;
- `\dimen` ;
- `\muskip` ;
- `\toks` ;
- `\read` (limité à 16 objets) ;
- `\write` (limité à 16 objets) ;
- `\language`.

## L'évolution de cette contrainte

Les formats actuels de LaTeX sont toujours construits avec les extensions {doc}`ε-TeX </1_generalites/glossaire/qu_est_ce_que_etex>` activées, ce qui signifie qu'il y a 32768 registres disponibles pour les éléments autres que `\read` et `\write`. Pour sa part, `LuaLaTeX` étend cela plus loin avec 65536 registres pour la plupart des éléments, 256 flux `\write` mais toujours 16 flux `\read`.

Aussi, à l'heure actuelle, si vous obtenez cette erreur maintenant pour des éléments autres que les flux de fichiers, c'est parce que vous avez un ancien format LaTeX (antérieur à 2015) qui vérifie la limite d'origine de 256, même si un TeX étendu est utilisé. La mise à jour vers une version actuelle de LaTeX devrait résoudre le problème. Si vous avez vraiment utilisé 32768 registres, il est fort probable que vous ayez une erreur de programmation qui provoque une boucle pour allouer tous les registres disponibles et donc un TeX étendu ne vous aiderait pas. Vous pouvez cependant essayer avec `LuaLaTeX` qui a des limites plus grandes dans la plupart des cas.

Le nombre de flux d'écriture est limité à 16 (ou 256 pour `LuaTeX`) même avec un TeX étendu. Toutefois, l'extension {ctanpkg}`morewrites` peut fournir *l'illusion* d'un plus grand nombre de `\write` disponible.

## Des erreurs proches

Une erreur connexe est celle indiquant que le nombre de commandes `\inserts` a été dépassé. Cette commande est indirectement liée aux registres `\count`, `\dimen` et `\skip`, ce qui la limite à 256 insertions. Il est peu probable que vous obteniez cette erreur sur un format LaTeX postérieur à 2015. Cependant si vous devez utiliser un ancien format, l'extension {ctanpkg}`morefloats` augmente le nombre d'insertions disponibles pour le mécanisme des flottants.

Dans d'autres cas, une solution parfois plus simple suffit. Par exemple, dans certains exemplaires de la documentation de l'extension {ctanpkg}`epsf`, il était indiqué qu'il fallait placer la commande suivante à chaque fois qu'une image EPS était insérée.

```latex
% !TEX noedit
  \input epsf
```

En suivant cette consigne dans un document contenant plusieurs images, on arrivait systématiquement à l'erreur suivante :

```bash
! No room for a new \read .
```

En fait, à chaque fois que `epsf.tex` est chargé, il s'attribue un *nouveau* emplacement de lecture de fichier pour vérifier la figure pour sa boîte englobante... et il n'y a tout simplement pas assez de ces éléments. Dès lors, la solution est simple : remplacer la séquence de commandes

```latex
% !TEX noedit
  ...
  \input epsf
  \epsffile{...}
  ...
  \input epsf
  \epsffile{...}
```

par un simple

```latex
% !TEX noedit
  \input epsf
```

placé plutôt au début du document. Puis ajouter les commandes `\epsffile` avec un simple ajustement par la commande `\epsfxsize` si nécessaire.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`epsf gives up after a bit <FAQ-epsf>`,
- {faquk}`No room for a new "_thing_" <FAQ-noroom>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,erreur,messages d'erreur de LaTeX,register,mode eTeX,problème de mémoire,limites de LaTeX
```

