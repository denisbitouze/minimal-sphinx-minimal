# Que signifie l'erreur : « Not in outer par mode » ?

- **Message** : `Not in outer par mode`
- **Origine** : *LaTeX*.

On obtient cette erreur lorsqu'un environnement `\marginpar` ou un environnement de flottant, tel que `table` ou `figure`, se trouve à l'intérieur d'une commande ou d'un environnement produisant une boîte. Par exemple, on ne peut pas utiliser un `\marginpar` dans une note de bas de page, un flottant, un `tabular`, ou tout autre emplacement analogue (puisqu'ils produisent tous des boîtes). On doit déplacer l'objet fautif dans le texte principal.

## Plus précisément

Vous avez le message d'erreur :

```
*\mbox{\marginpar{foo}}

! LaTeX Error : Not in outer par mode.
```

Ce problème survient lorsque vous essayez mettre un élément mobile à l'intérieur d'une boîte. Les éléments mobiles, dans ce contexte, sont les environnements de flottants (`figure` ou `table`, par exemple) et les commandes `\marginpar` (notes marginales). LaTeX ne dispose tout simplement pas des mécanismes permettant à un flottant de flotter hors d'une boîte. En fait, les environnements de flottants et les `\marginpar` sont eux-mêmes définis à partir de boîtes, de sorte qu'ils ne peuvent pas être imbriqués.

Si votre erreur provient de `\marginpar`, cherchez simplement un autre moyen de placer la commande; il n'y a pas de solution générale, mais on arrive à se débrouiller au cas par cas.

Si un environnement de flottant est le coupable, il est sans doute possible d'utiliser l'option de placement `H`, fournie (par exemple) par le paquetage {ctanpkg}`float` :

```latex
% !TEX noedit
\parbox{14cm}{%
  \begin{figure}[H]
  ...
  \caption{Apparemment flottant...}
  \end{figure}%
}
```

Cet exemple n'a pas beaucoup de sens en l'état ; cependant, il est concevable de trouver des utilisations réelles (par exemple, en utilisant un paquetage tel que {ctanpkg}`algorithm2e` pour placer deux algorithmes côte à côte).

Un autre cas fréquent est celui où l'utilisateur veut insérer une figure quelque part dans un tableau :

```latex
% !TEX noedit
\begin{tabular}{|l|}
  \hline
  \begin{figure}
  \includegraphics{mon_image}
  \end{figure}
  \hline
\end{tabular}
```

une construction qui était censée dessiner un cadre autour de l'image, mais qui ne fonctionne pas. Pas plus que ça, d'ailleurs :

```latex
% !TEX noedit
\framebox{\begin{figure}
  \includegraphics{foo}
  \end{figure}%
}
```

Le problème est que l'environnement `tabular` et la commande `\framebox` empêchent l'environnement `figure` de jouer son rôle normal, qui est de flotter dans le document.

La solution consiste simplement à ne pas utiliser l'environnement `figure` ici :

```latex
% !TEX noedit
\begin{tabular}{|l|}
  \hline
  \includegraphics{mon_image}
  \hline
\end{tabular}
```

À quoi servait l'environnemnt de flottant ? Tel qu'il était utilisé dans les deux premiers exemples, il ne servait à rien. Mais peut-être vouliez-vous en fait encadrer l'image et sa légende, dans un flottant ?

Il est facile d'y parvenir, il suffit d'inverser l'ordre des environnements (ou de l'environnement `figure` et de la commande) :

```latex
% !TEX noedit
\begin{figure}
  \begin{tabular}{|l|}
    \hline
    \includegraphics{mon_image}
    \caption{Une image}
    \hline
  \end{tabular}
\end{figure}
```

Il en va de même pour les environnements `table` (ou tout autre environnement que vous auriez défini vous-même) à l'intérieur des commandes de tableaux ou de boîtes; vous devez *faire sortir l'environnement de flottant* de ces commades, d'une façon ou d'une autre.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`Not in outer par mode <FAQ-parmoderr>`,
- [LaTeX Error : Not in outer par mode](https://tex.stackexchange.com/questions/124346/latex-error-not-in-outer-par-mode).

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,note marginale,flottant dans une note
```

