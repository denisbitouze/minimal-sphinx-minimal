# Que signifie l'erreur : « No counter \`⟨nom⟩' defined » ?

- **Message** : ```` No counter `⟨nom⟩' defined ````

Le compteur `⟨nom⟩` référencé par `\setcounter`, `\addtocounter` ou dans l'argument optionnel de `\newcounter` ou de `\newtheorem` est inconnu. Il doit être déclaré au préalable avec `\newcounter`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,compteurs,compteur non défini,théorème
```

