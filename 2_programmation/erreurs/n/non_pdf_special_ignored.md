# Que signifie l'erreur : « Non-PDF special ignored! » ?

- **Message** : `Non-PDF special ignored!`
- **Origine** : *TeX*.

Il s'agit d'une erreur émise par pdfTeX : vous avez demandé à pdfTeX de produire un fichier PDF (ce qu'il fait par défaut), et il a rencontré une commande [\\special](/5_fichiers/postscript/commandes_special) dans votre document. pdfTeX s'occupe de générer sa sortie PDF lui-même, sans avoir besoin de passer par un pilote externe. Donc, dans ce mode de fonctionnement, il n'a pas besoin des commandes `\special` (qui permettent à l'utilisateur de passer des informations au pilote utilisé pour générer la sortie).

Comment cela est-il arrivé ? Actuellement, les utilisateurs de LaTeX n'utilisent pratiquement jamais les commandes `\special` par eux-mêmes --- ce sont les différentes extensions appelées qui le font pour eux, si besoin. Certaines extensions appellent ces commandes, quelle que soit la façon dont le document est compilé : {ctanpkg}`PStricks <pstricks>` en est un exemple (à sa décharge, sa raison d'être est d'écrire du code PostScript dans une séries de commandes `\special`, pour contourner les limitation des pilotes DVI). Si vous avez besoin d'utilisez {ctanpkg}`PStricks <Pstricks>` avec pdfTeX, il faudra procéder différemment. Vous pouvez essayer l'extension {ctanpkg}`pdftricks`, par exemple, qui s'occupe d'extraire votre code PStricks pour le compiler séparément du reste du document.

Si l'erreur est produite par des extensions comme {ctanpkg}`color`, {ctanpkg}`graphics` ou {ctanpkg}`hyperref`, elle sera plus facile à corriger car il est possible de leur dire dans quel mode travailler. Ces extensions ont elles-mêmes des modules qui déterminent quelles commandes `\special` (ou équivalentes) sont nécessaires pour produire l'effet demandé : le mode `pdftex` de ces extensions, par exemple, sait qu'il ne doit pas générer de commandes `\special`. Dans la plupart des cas, vous pouvez laisser le système choisir lui-même le pilote dont vous avez besoin ; dans ce cas, tout fonctionnera correctement si vous basculez de LaTeX à pdfLaTeX. Si vous avez utilisé `dvips` (et spécifié explicitement le mode `dvips`) ou `dvipdfm` (en spécifiant le mode `dvipdfm`), et que vous décidez d'essayer pdfLaTeX, vous devez penser à *retirer la spécification du mode `dvips` ou `dvipdfm`* des options de l'extension, et laisser le système choisir lui-même le bon mode.

______________________________________________________________________

*Sources :*

- {faquk}`Non-PDF special ignored! <FAQ-nonpdfsp>`
- [The PSTricks web site : PDF export](http://tug.org/PSTricks/main.cgi?file=pdf/pdfoutput),
- [Non-PDF special ignored!](https://tex.stackexchange.com/questions/101750/non-pdf-special-ignored-texlive-pstricks-osx-mac)

```{eval-rst}
.. meta::
   :keywords: LaTeX,messages d'erreur de LaTeX,mode PDF,erreur avec PStricks,DVI ou PDF
```

