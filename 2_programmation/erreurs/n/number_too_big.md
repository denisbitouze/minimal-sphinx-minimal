# Que signifie l'erreur : « Number too big » ?

- **Message** : `Number too big`
- **Origine** : *TeX*.

Un nombre a été assigné ou utilisé dans `\setcounter` ou `\addtocounter` tout en étant supérieur au maximum que TeX est capable de gérer (en l'occurrence 2147483647, ou 7FFFFFFF en hexadécimal). Cette erreur peut également survenir lorsqu'on modifie un registre de longueur avec `\setlength` ou `\addtolength`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,nombre trop grand,plus grand nombre de LaTeX,longueur trop grande
```

