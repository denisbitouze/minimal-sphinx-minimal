# Que signifie l'erreur : « You can't use \`macro parameter #' in ⟨mode⟩ mode » ?

- **Message** : `You can't use macro parameter # in ⟨mode⟩ mode`
- **Origine** : *TeX*.

TeX a trouvé un caractère `#` dans un endroit où il ne semble pas faire référence à un argument d'une commande.

:::{note}
Pour composer le symbole dièse « # », il faut utiliser la commande `\#`.
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dièse,définition de commande,comment écrire un dièse
```

