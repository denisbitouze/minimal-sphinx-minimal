# Que signifie l'erreur : « You can't use \``\spacefactor`' in math mode » ?

- **Message** : ```` You can't use `\spacefactor' in math mode ````
- **Origine** : *TeX*.

Au contexte près, cette erreur est à rapprocher de l'erreur « [You can't use \``\spacefactor`' in vertical mode](/2_programmation/erreurs/y/you_cannot_use_spacefactor_in_vertical_mode) », autrement dit une utilisation probablement erronée de la commande `\@`.

______________________________________________________________________

*Source :* {faquk}`\\spacefactor complaints <FAQ-atvert>`

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode vertical,mode horizontal,arobe,arobase
```

