# Que signifie l'erreur : « You haven't defined the language ⟨langue⟩ yet » ?

- **Message** : `You haven't defined the language ⟨langue⟩ yet`
- **Origine** : package *babel*.

De nombreuses commandes d'interfaces de {ctanpkg}`babel` vérifient que leur argument est une langue qui a été spécifiée dans la liste d'options lorsque cette extension a été chargée. Si la `⟨langue⟩` n'a pas été spécifiée, le traitement est interrompu et ce message d'erreur s'affiche.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définition de langue
```

