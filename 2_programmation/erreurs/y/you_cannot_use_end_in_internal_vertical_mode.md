# Que signifie l'erreur : « You can't use \``\end`' in internal vertical mode » ?

- **Message** : ```` You can't use `\end' in internal vertical mode ````
- **Origine** : *TeX*.

C'est l'un des messages d'erreurs de TeX les moins bien compris, puisqu'il se rapporte à la primitive TeX `\end` (terminant une compilation TeX), que TeX redéfinit pour devenir la marque d'une fin d'environnement. L'erreur signifie que la commande LaTeX `\end{document}` ou la commande `\stop` a été rencontrée pendant que LaTeX était en train de construire une boîte. Par exemple, `\begin{figure}...\stop` va produire cette erreur.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode vertical,mode horizontal,TeX et LaTeX,end en LaTeX
```

