# Que signifie l'erreur : « You can't use \``\hrule`' here except with leaders. » ?

- **Message** : ```` You can't use `\hrule' here except with leaders. ````
- **Origine** : *TeX*.

```{eval-rst}
.. todo:: Expliquer le message d'erreur.
```

______________________________________________________________________

*Sources :*

- [What does it mean : You can't use \`\\hrule' here except with leaders](https://tex.stackexchange.com/questions/528071/what-does-it-mean-you-cant-use-hrule-here-except-with-leaders)
- [Problem with generating this table](https://tex.stackexchange.com/questions/326513/problem-with-generating-this-table),

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,tableaux,hrule,hline,pointillés,points de suspension
```

