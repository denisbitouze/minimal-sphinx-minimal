# Que signifie l'erreur : « You haven't specified a language option » ?

- **Message** : `You haven't specified a language option`
- **Origine** : package *babel*.

On obtient ce message lorsque des langues non connues ont été spécifiées à {ctanpkg}`babel`. Cela signifie qu'elles ne sont ni dans la liste d'options de *babel*, ni dans la liste des options globales (ce qui est, le plus souvent, dû à une erreur de frappe). Il est probable qu'après cette erreur la compilation du document indiquera de nombreuses erreurs supplémentaires.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définition de langue,langue de babel,french,frenchb
```

