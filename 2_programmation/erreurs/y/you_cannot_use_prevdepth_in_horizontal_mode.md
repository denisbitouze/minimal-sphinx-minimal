# Que signifie l'erreur : « You can't use \``\prevdepth`' in horizontal mode » ?

- **Message** : ```` You can't use `\prevdepth' in horizontal mode ````
- **Origine** : *TeX*.

La dimension `\prevdepth` ne peut être utilisée qu'en mode vertical (c'est-à-dire entre des paragraphes).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode vertical,mode horizontal,paragraphes en latex
```

