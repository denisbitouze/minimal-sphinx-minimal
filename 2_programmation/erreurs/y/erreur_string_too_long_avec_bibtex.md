# Que signifie l'avertissement : « you've exceeded 1000, the global-string-size, for entry ⟨nom⟩ » ?

Lors de l'utilisation de `BibTeX`, ce dernier peut indiquer le message suivant :

```text
Warning--you've exceeded 1000, the global-string-size, for entry XXX
while executing--line 42 of file ma_biblio.bst
*Please notify the bibstyle designer*
```

Ce message résulte généralement d'un long résumé ou d'une longue annotation incluse dans la base de données. Cet avertissement survient généralement en raison d'un biais dans le code d'un ancien fichier de style `abstract.bst` ou de fichiers de styles qui en dérivent. Les versions plus récentes ont été corrigées et ne devraient plus poser problème. Les solutions qui suivent n'ont donc d'intérêt que si vous souhaitez travailler sur une ancienne distribution de LaTeX ou si vous rencontrez des fichiers anciens.

## Modification du fichier de style bibliographique

La solution consiste alors à faire une copie du fichier de style (en le renommant par exemple `abstract-long.bst`, tout en le plaçant dans le même répertoire que votre fichier « tex » principal si vous souhaitez aller vite) puis à le modifier comme suit. Il faut rechercher la fonction `output.nonnull`. Dans celle-ci, la première ligne est :

```latex
% !TEX noedit
{ 's :=
```

Remplacez-la par :

```latex
% !TEX noedit
{ swap$
```

Ensuite, supprimez la dernière ligne de la fonction, qui se limite à l'instruction « `s` ». Enfin, modifiez votre commande `\bibliographystyle` pour faire référence au nom du nouveau fichier.

Cette technique s'applique également à n'importe quel style de bibliographie : la même modification peut être apportée à n'importe quelle fonction `output.nonnull` similaire.

## Méthodes alternatives

Si vous hésitez à faire ce genre de changement, il y a deux autres manières de procéder :

- retirer l'entrée gênante de la base de données, afin de ne pas rencontrer la limite de `BibTeX` ;
- placer le corps de l'entrée trop long dans un fichier séparé, comme dans l'exemple suivant :

```bibtex
@article{ennuyeux,
  author =    "Fred Bavard",
  ...
  abstract =  "{\input{abstracts/long.tex}}"
}
```

De cette façon, vous faites en sorte que `BibTeX` n'ait à traiter que le nom du fichier, bien qu'il dise à LaTeX d'inclure tout le texte long.

______________________________________________________________________

*Source :* {faquk}`String too long in BibTeX <FAQ-bibstrtl>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,erreur de BibTeX,chaîne trop longue
```

