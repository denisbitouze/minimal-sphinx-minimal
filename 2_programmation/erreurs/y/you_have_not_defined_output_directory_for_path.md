# Que signifie l'erreur : « You haven't defined output directory for \`⟨chemin⟩' » ?

- **Message** : ''You haven't defined output directory for \`⟨*chemin*⟩' ''
- **Origine** : package *docstrip*.

Le fichier de configuration `docstrip.cfg` contient une déclaration pour `\BaseDirectory`, mais le `⟨chemin⟩` interne dans le script `docstrip` n'a pas de traduction en répertoire local. On doit spécifier une telle traduction avec `\DeclareDirectory` ou `\UseTDS`, comme étudié à la section 14.2.3 page 844 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,répertoire non trouvé,TDS,TeX directory structure
```

