# Que signifie l'erreur : « Paragraph ended before ⟨commande⟩ was complete » ?

- **Message** : `Paragraph ended before ⟨commande⟩ was complete`
- **Origine** : *TeX*.

Comme on l'a vu à la section A.1.2 du *LaTeX Companion*
.. todo::, les commandes définies par `\newcommand*` ou `\renewcommand*` n'acceptent pas de commande `\par` ou de ligne vide dans leurs arguments. Au cas où cela se produit, on obtient un « `Runaway argument` » ainsi que cette erreur.

:::{important}
La `⟨commande⟩` indiquée peut ne pas être celle utilisée dans le document. Par exemple, `\emph{...\par...}` indiquera la commande `\text@command` dans le message d'erreur (c'est-à-dire la commande interne appelée par `\emph`).
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=P>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,paragraphe en argument d'une commande
```

