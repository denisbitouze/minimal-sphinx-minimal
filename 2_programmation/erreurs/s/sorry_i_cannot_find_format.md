# Que signifie l'erreur : « Sorry, I can't find ⟨format⟩... » ?

- **Message** : `Sorry, I can't find ⟨format⟩...`

Si l'on obtient cette erreur, c'est que LaTeX n'a jamais débuté puisque TeX ne trouve pas le `⟨format⟩` contenant les définitions de base de LaTeX. Il s'agit d'un problème qui concerne l'installation du système TeX. Il vaut mieux dans ce cas consulter la documentation de la distribution.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,format LaTeX,format non trouvé,fichier non trouvé
```

