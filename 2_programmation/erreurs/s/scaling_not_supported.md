# Que signifie l'erreur : « Scaling not supported » ?

- **Message** : `Scaling not supported`
- **Origine** : packages *graphics/graphicx*.

On a demandé un changement de taille d'un objet avec `\resizebox` ou une commande similaire, mais le pilote graphique sélectionné ne le gère pas. LaTeX laisse l'espacement nécessaire, mais le document imprimé montrera l'image avec sa taille inchangée.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,redimensionner une image,changer la taille d'une image,pilote graphique
```

