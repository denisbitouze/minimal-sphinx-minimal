# Que signifie l'erreur : « Symbol ⟨commande⟩ not provided by font family ⟨nom⟩ » ?

- **Message** : `Symbol ⟨commande⟩ not provided by font family ⟨nom⟩`
- **Origine** : package *textcomp*.

L'extension {ctanpkg}`textcomp` utilise le codage `TS1`, qui n'est malheureusement implémenté de façon complète que par une minorité des familles de fontes utilisables avec LaTeX. L'extension compose le symbole (appelé par `⟨commande⟩`) en utilisant une famille par défaut mémorisée dans `\textcompsubstdefault`.

:::{note}
On peut remplacer l'erreur par un simple avertissement en chargeant *textcomp* avec l'option `warn`. Voir section 7.5.4 du *LaTeX Companion*

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « pour plus de détails. »
```
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage des caractères,codage TS1,symbole manquant,package textcomp
```

