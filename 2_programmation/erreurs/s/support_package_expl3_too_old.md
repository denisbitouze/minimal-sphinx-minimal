# Que signifie l'erreur : « Support package « expl3 » too old » ?

- **Message** : `Support package ‘expl3’ too old.`
- **Origine** : *LaTeX*.

Certaines extensions (plutôt récentes) sont écrites en utilisant l'environnement {doc}`LaTeX3 </1_generalites/histoire/c_est_quoi_latex3>`. Comme le développement de LaTeX3 est toujours en cours, l'auteur de l'extension ne peut pas prédire de façon certaine quelle version de LaTeX3 l'utilisateur aura installée, et si cette version sera compatible avec l'extension. Donc beaucoup d'auteurs font en sorte que le code de leur extension vérifie la version installée chez l'utilisateur, et se plaigne si elle est plus ancienne que celle de l'auteur au moment du test. Le message d'erreur est le suivant :

```
! Support package expl3 too old.
```

Si vous lisez l'«aide supplémentaire», vous aurez la solution : mettez à jour votre installation de LaTeX3. Les éléments concernés sont {ctanpkg}`l3kernel` (l'environnement de programmation lui-même, qui contient {ctanpkg}`expl3` mentionné dans le message d'erreur) et {ctanpkg}`l3packages` (extensions LaTeX3, contenant les définitions de commandes).

Ça peut paraître compliqué, mais ça ne l'est pas tant que ça. Si vous utilisez une distribution TeX moderne que vous avez installée vous-même, faites simplement sa mise à jour par Internet ; si ce n'est pas possible, installez la nouvelle version à partir des fichiers [l3kernel.tds.zip](http://mirrors.ctan.org/install/macros/latex/contrib/l3kernel.tds.zip) et [l3packages.tds.zip](http://mirrors.ctan.org/install/macros/latex/contrib/l3packages.tds.zip) sur le CTAN (dans les packages {ctanpkg}`l3kernel` et {ctanpkg}`l3packages`, respectivement).

______________________________________________________________________

*Source :* {faquk}`Support package \`expl3\` too old <FAQ-expl3-old>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors,latex3
```

