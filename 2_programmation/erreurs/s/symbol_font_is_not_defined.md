# Que signifie l'erreur : « Symbol font ⟨nom⟩ is not defined » ?

- **Message** : `Symbol font ⟨nom⟩ is not defined`

On a essayé d'utiliser la fonte de symboles `⟨nom⟩` (par exemple, comme argument d'une commande `\DeclareMathSymbol`) sans la déclarer au préalable avec `\DeclareSymbolFont`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fonte de symboles,police de symboles,police mathématique,symboles mathématiques
```

