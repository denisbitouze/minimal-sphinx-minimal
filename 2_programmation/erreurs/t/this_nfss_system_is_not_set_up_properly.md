# Que signifie l'erreur : « This NFSS system isn't set up properly » ?

- **Message** : `This NFSS system isn't set up properly`

Cette erreur survient lorsque LaTeX détecte une faute lors de la vérification des tables de substitutions de fontes [^footnote-1] au moment du `\begin{document}`. Elle signifie qu'une des déclarations `\DeclareFontSubstitution` ou `\DeclareErrorFont` [^footnote-2] est corrompue. Ces déclarations nécessitent de pointer sur des formes de fontes valides (déclarées avec `\DeclareFontShape`). On peut taper `h` pour des informations supplémentaires et il faudra en aviser le responsable du système. Si l'on est soi-même le responsable du système, il faudra se reporter à la fin de la section 7.10.5 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- <https://tex.stackexchange.com/questions/121298/this-nfss-system-isnt-set-up-properly>.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,changer la police de caractère,changer la fonte,new font selection scheme
```

[^footnote-1]: **NFSS** signifie « *new font selection scheme*. »

[^footnote-2]: La déclaration `\DeclareErrorFont` est utilisée au cours de l'installation et pointe sur une fonte (forme de fonte et taille) qui doit être utilisée lors de la détection d'une erreur. Sa valeur par défaut est *Computer Modern Roman 10pt*, qui doit être disponible sur n'importe quelle installation de TeX.

