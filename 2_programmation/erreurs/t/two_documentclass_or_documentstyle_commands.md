# Que signifie l'erreur : « Two `\documentclass` or `\documentstyle` commands » ?

- **Message** : `Two \documentclass or \documentstyle commands`
- **Origine** : *LaTeX*.

On ne peut utiliser qu'une seule de ces commandes dans un document. Cette erreur indique qu'il y en a plusieurs.

:::{note}
Une raison possible : vous avez peut-être combiné deux documents en un seul.
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fusionner deux documents LaTeX,concaténer deux documents LaTeX,multiple documentclass
```

