# Que signifie l'erreur : « The attribute ⟨attribut⟩ is unknown for language ⟨langue⟩ » ?

- **Message** : `The attribute ⟨attribut⟩ is unknown for language ⟨langue⟩`
- **Origine** : *babel*.

On a essayé d'activer un `⟨attribut⟩` d'une `⟨langue⟩` qui n'est pas défini dans le fichier de définition de langues pour cette `⟨langue⟩`. Il faut dans ce cas vérifier {texdoc}`la documentation de babel <babel>` pourr cette `⟨langue⟩`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier de langue,langue pour babel,configuration de babel
```

