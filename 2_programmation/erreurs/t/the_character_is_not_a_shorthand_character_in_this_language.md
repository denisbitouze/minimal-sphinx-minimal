# Que signifie l'erreur : « The character \`⟨car⟩' is not a shorthand character in ⟨langue⟩ » ?

- **Message** : ```` The character `⟨car⟩' is not a shorthand character in ⟨langue⟩ ````
- **Origine** : *babel*.

Cette erreur est déclenchée lorsque l'utilisateur se sert de la commande `\shorthandon` et lui passe un caractère `⟨car⟩` qui n'est pas défini comme raccourci pour la `⟨langue⟩` en cours. L'instruction est ignorée.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,caractères actifs et babel
```

