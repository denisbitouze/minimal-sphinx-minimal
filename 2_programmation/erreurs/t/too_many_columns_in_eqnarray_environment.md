# Que signifie l'erreur : « Too many columns in eqnarray environment » ?

- **Message** : `Too many columns in eqnarray environment`
- **Origine** : *LaTeX*.

L'environnement `eqnarray` autorise un maximum de trois colonnes (c'est-à-dire deux signes `&` par ligne). Pour des mathématiques sérieuses, on devrait utiliser l'extension {ctanpkg}`amsmath` décrite au chapitre 8 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision. Elle autorise des structures hors-texte plus complexes.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,alignement des équations
```

