# Que signifiel'erreur : « Two `\LoadClass` commands » ?

- **Message** : `Two \LoadClass commands`
- **Origine** : *LaTeX*.

Un classe ne peut charger qu'une seule classe au maximum. La section A.4 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.étudie en détail la façon dont les classes sont construites.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,plusieurs classes de document,charger une classe,écrire une classe
```

