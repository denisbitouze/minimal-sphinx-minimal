# Que signifie l'erreur : « Tab overflow » ?

- **Message** : `Tab overflow`
- **Origine** : *LaTeX*.

LaTeX peut accepter jusqu'à treize positions de tabulation (`\=`) à l'intérieur d'un environnement `tabbing`, et on en a utilisé un plus grand nombre. Si tous ne sont pas nécessaires en même temps, on peut résoudre le problème avec `\pushtabs` ou avec des lignes de modèle et `\kill`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,tabulations,nombre maximum de tabulations,trop de tabulations
```

