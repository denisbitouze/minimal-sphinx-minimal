# Que signifie l'erreur : « Too deeply nested » ?

- **Message** : `Too deeply nested`
- **Origine** : *LaTeX*.

LaTeX standard permet un total de six niveaux d'imbrication de listes. Ces niveaux peuvent inclure jusqu'à quatre listes de type `itemize` ou `enumerate`. Cette erreur signale que le document a dépassé une de ces limites. Le plus probable est d'avoir oublié de fermer certaines listes correctement.

Si l'on a vraiment besoin de niveaux supplémentaires, il faut recopier les définitions des environnements `list`, `itemize` et `enumerate` dans une extension privée, et modifier les constantes qui y sont codées en dur.

Pour aller un peu plus loin, ces six niveaux d'imbrication et quatre types de listes correspondent à des jeux de paramètres. En effet, il existe également différentes définitions d'étiquettes pour les environnements `enumerate` et `itemize` à leurs propres niveaux privés d'imbrication. Prenons cet exemple :

```latex
% !TEX noedit
\begin{enumerate}
\item premier élément de la première liste numérotée
  \begin{itemize}
  \item premier élément de la première liste non numérotée
    \begin{enumerate}
    \item premier élément de la deuxième liste numérotée
    ...
    \end{enumerate}
  ...
  \end{itemize}
...
\end{enumerate}
```

Dans l'exemple,

- le premier `enumerate` a des étiquettes d'un `enumerate` de premier niveau, et est indenté comme pour une liste de premier niveau ;
- le premier `itemize` a des étiquettes d'un `itemize` de premier niveau, et est indenté comme pour une liste de deuxième niveau ;
- le second `enumerate` a des étiquettes d'un `enumerate` de deuxième niveau, et est indenté comme pour une liste de troisième niveau.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`"Too deeply nested" <FAQ-toodeep>`.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,imbrication de listes,emboîtement de listes,trop de listes
```

