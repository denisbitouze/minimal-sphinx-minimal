# Que signifie l'erreur : « `\tag` not allowed here » ?

- **Message** : `\tag not allowed here`
- **Origine** : *amsmath*.

La commande `\tag` n'est permise qu'au niveau supérieur d'un environnement mathématique hors-texte. Il est généralement préférable de la placer à la fin de l'équation logique à laquelle elle appartient.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,étiquette d'équation
```

