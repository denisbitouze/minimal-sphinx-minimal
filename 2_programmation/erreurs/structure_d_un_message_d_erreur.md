# Quelle est la structure des messages d'erreur de TeX ?

Les messages d'erreur de TeX rappellent {doc}`l'époque où ce logiciel a été conçu </1_generalites/glossaire/qu_est_ce_que_tex>` (les années 1970) : ils ne sont pas très conviviaux, bien qu'ils contiennent toutes les informations que TeX peut offrir, généralement de manière assez concise.

Les rapports d'erreur de TeX ont tous la même structure :

- un message d'erreur ;
- un peu de « contexte » ;
- une invite de commande d'erreur.

## Le message d'erreur

Le message d'erreur se rapporte à ce qui pose un problème à TeX. Malheureusement, dans le cas de {doc}`formats </1_generalites/glossaire/qu_est_ce_qu_un_format>` tels que LaTeX (ou ses extensions), le problème TeX sous-jacent peut être difficile à relier au problème réel dans les commandes de « niveau supérieur ». De nombreux problèmes détectés par LaTeX se manifestent par des erreurs génériques, avec un texte d'erreur fourni par LaTeX lui-même (ou par une classe ou une extension).

## Le contexte de l'erreur

Le contexte de l'erreur est une représentation stylisée de ce que TeX faisait au moment où il a détecté l'erreur. Comme indiqué dans la question « {doc}`Comment traiter les erreurs ? </2_programmation/erreurs/interpreter_les_messages_d_erreur2>` », une extension peut indiquer à TeX la quantité de contexte à afficher, et l'utilisateur peut avoir besoin d'annuler ce que l'extension a fait. Chaque ligne de contexte est divisée au point de l'erreur. Si l'erreur s'est produite dans une commande appelée à partir de la ligne actuelle, la coupure est au point de l'appel. Si cette commande présente des arguments, la coupure apparaît après l'analyse de tous les arguments.

Voici un exemple de code fautif avec une commande (non définie) sans argument :

```latex
% !TEX noedit
\blabla et ainsi de suite
```

Il se produit l'erreur suivante dans laquelle indique que la commande n'existe pas :

```
! Undefined control sequence.
l.4 \blabla
            et ainsi de suite
```

Voici maintenant une commande appelant une commande non définie dans sa définition :

```latex
% !TEX noedit
\newcommand{\blabla}[1]{\blibli #1}
\blabla{et} ainsi de suite
```

L'erreur produite diffère un petit peu dans le sens où :

- TeX vous indique qu'une commande en appelle une autre qui pose problème ;
- le point de coupure se situe après l'argument de la commande.

```
! Undefined control sequence.
\blabla #1->\blibli
                    #1
l.5 \blabla{et}
                ainsi de suite
```

Enfin, voici le cas où la commande bien définie reçoit en argument une commande non définie :

```latex
% !TEX noedit
\newcommand{\blabla}[1]{#1 et}
\blabla{\blibli} ainsi de suite
```

Le message d'erreur est alors une nouvelle fois après la fin de l'argument de la commande qui inclut la commande fautive :

```
! Undefined control sequence.
<argument> \blibli

l.5 \blabla{\blibli}
                     ainsi de suite
```

## L'invite de commande d'erreur

L'invite de commande n'apparaît que si vous êtes en mode de compilation interactive (ce qui n'est généralement pas le cas avec les éditeurs à interface graphique où la compilation interactive est désactivée par défaut).

Elle n'accepte que les commandes à un seul caractère et la liste de ce qui est disponible peut être obtenue en tapant `?`. Une commande précieuse est `h` qui développe le message original de TeX, parfois accompagné d'un indice sur ce qu'il faut faire pour contourner le problème à court terme. Si vous tapez simplement sur la touche entrée (ou tout ce que votre système utilise pour signaler la fin d'une ligne) à l'invite, TeX tentera de continuer (souvent avec assez peu de succès).

______________________________________________________________________

*Source :* {faquk}`The structure of TeX error messages <FAQ-errstruct>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,message d'erreur,erreur,invite,structure des messages
```

