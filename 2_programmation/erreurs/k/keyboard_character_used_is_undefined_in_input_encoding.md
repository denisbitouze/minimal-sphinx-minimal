# Que signifie l'erreur : « Keyboard character used is undefined in input encoding ⟨enc⟩ » ?

- **Message** : `Keyboard character used is undefined in input encoding ⟨enc⟩`
- **Origine** : package *inputenc*.

Un nombre 8-bits rencontré dans le document ne correspond pas, avec le codage d'entrée `⟨enc⟩`, à un objet LICR (voir sections 7.5.2 et 7.11.3 du *LaTeX Companion*
.. todo::). On doit vérifier si le source est vraiment sauvegardé dans le codage indiqué.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=J>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage des caractères,problème de codage,problème de clavier
```

