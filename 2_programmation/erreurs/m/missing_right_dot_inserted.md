# Que signifie l'erreur : « Missing `\right`. inserted » ?

- **Message** : `Missing \right. inserted`
- **Origine** : *TeX*.

Une formule contient un `\left` sans le `\right` correspondant. Il convient de se rappeler que les paires de délimiteurs `\left...\right` doivent appartenir à la même « sous-formule ». Elles ne peuvent pas, par exemple, être séparées par un `&` dans un alignement ou apparaître dans des niveaux de groupes différents.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,délimiteurs mathématiques,parenthèses,accolades,brackets
```

