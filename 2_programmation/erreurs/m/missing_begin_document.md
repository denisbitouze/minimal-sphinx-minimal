# Que signifie l'erreur : « Missing `\begin{document}` » ?

- **Message** : `Missing \begin{document}`
- **Origine** : *LaTeX*.

Cette erreur survient vous demandez à LaTeX de composer un élément textuel dans le {doc}`préambule </2_programmation/syntaxe/preambule>` du document.

## Cas fréquent

La plupart du temps, cette erreur est due à une déclaration mal interprétée par LaTeX. Vous pouvez ainsi avoir :

- saisi du texte (des noms de commandes sans le caractère « `\` » par exemple) dans le préambule ;
- créé là une boîte avec `\newsavebox` en y mettant quelque chose en utilisant `\sbox` (ou similaire) ;
- oublié de mettre `\begin{document}` dans le document ;
- ou communiqué un mauvais fichier à intégrer.

Cette erreur peut également être produite par un texte sur la même ligne que `\begin{filecontents}`.

Dans tous ces cas, la solution consiste à faire une utilisation judicieuse des marqueurs de commentaires (`%`) au début d'une ligne, à déplacer des éléments, ajouter le `\begin{document}` ou faire une bascule vers le bon fichier.

:::{note}
Composer dans le préambule, à l'intérieur des déclarations `\sbox` ou `\savebox` est possible, mais il est plus prudent de placer ce type de déclarations après `\begin{document}`, car certaines extensions peuvent repousser leurs initialisations finales jusqu'à cet emplacement.
:::

## Cas d'une anomalie dans le fichier « .aux »

L'erreur peut également se produire quand le fichier « `.aux` » contient déjà une erreur. Lors de la compilation, comme il est appelé par LaTeX, il peut générer alors l'erreur. Si c'est le cas, supprimez le fichier « `.aux` » et recommencez à zéro.

## Cas de la présence d'un BOM

Toutes les choses qui peuvent apparaissent avant `\documentclass` peuvent également être problématiques puisque placées avant `\begin{document}`. De fait, les éditeurs modernes sont capables de mettre là des choses et de vous empêcher de les voir. Cela peut arriver lorsque votre document est "écrit" en {doc}`Unicode </3_composition/langues/latex_et_l_utf8>`. La norme Unicode définit des *Byte Order Marks* (BOM) ou « [indicateurs d'ordre des octets](https://fr.wikipedia.org/wiki/Indicateur_d'ordre_des_octets) » qui indiquent au programme qui lit un fichier comment les codes Unicode sont présentés. Malheureusement, LaTeX ou `pdfLaTeX` ne comprennent pas ces BOM et les considèrent comme des demandes de composition. Le message d'erreur que vous voyez ressemblera à :

```
! LaTeX Error : Missing \begin{document}.
...
l.1 <?>
       <?><?>\documentclass{article}
```

Les `<?>` représentent ici des caractères non reconnus par votre système d'exploitation. Vous pouvez repérer ce BOM en examinant les octets, par exemple avec l'application Unix `hexdump` :

```bash
$ hexdump -C ⟨fichier⟩
00000000  ef bb bf 5c 64 6f 63 75 ...
```

Le `5c 64 6f 63 75` correspond à la chaîne « `\docu` », autrement dit le début de votre document (pour vous). Les trois bytes juste avant forment le BOM.

Empêcher votre éditeur de mettre un BOM dépend, bien sûr, de l'éditeur que vous utilisez. Par exemple, si vous utilisez `GNU Emacs`, vous devez passer changer l'encodage de `utf-8-with-signature` à `utf-8`. Les instructions pour cela se trouvent sur le site [Stack Overflow](http://stackoverflow.com/questions/3859274/).

Notez bien que {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>`'' et ' {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` reconnaissent les BOM et vous permettent d'éviter ce souci.

## Cas plus rare car expérimental

Si, après avoir passé en revue toutes ces possibilités, l'erreur se reproduit, cela pourrait bien être dû à une classe ou à une extension expérimentale ayant encore des erreurs.

______________________________________________________________________

*Sources :*

- {faquk}`Missing \\begin{document} <FAQ-missbegdoc>` ;
- <https://latex.developpez.com/faq/erreurs?page=M> ;
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur dans le préambule
```

