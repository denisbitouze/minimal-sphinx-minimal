# Que signifie l'erreur : « Missing } inserted » ?

- **Message** : `Missing } inserted`
- **Origine** : *TeX*.

La structure des groupes est fautive pour le document et TeX essaie de la réparer en insérant une accolade fermante. Cette tentative peut réussir ou provoquer d'autres erreurs. Habituellement, le problème devient apparent lorsqu'on regarde la sortie.

Lorsqu'on ne peut pas déterminer la cause de cette erreur, on peut utiliser les fonctionnalités `\showgroups` ou `\tracinggroups` de eTeX, comme expliqué à la page 934 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,délimiteurs,parenthèses,accolades,accolade fermante,accolade ouvrante
```

