# Que signifie l'erreur : « Missing number, treated as zero » ?

- **Message** : `Missing number, treated as zero`
- **Origine** : *TeX*.

Cette erreur survient lorsque TeX s'attend à trouver un nombre ou une dimension et trouve autre chose. Par exemple, la commande `\value{page}`, au lieu de `\thepage`, produit cette erreur puisqu'une commande `\value` isolée fait que TeX s'attend à une assignation de bas niveau d'un compteur. En général, utiliser un registre de longueur sans fonction appropriée, comme `\setlength`, peut déclencher cette erreur. Habituellement, le problème se résout en utilisant les techniques présentées à la question « [Comment traiter les erreurs ?](/2_programmation/erreurs/interpreter_les_messages_d_erreur2) ».

On obtient également ce message lorsque `\usebox` n'est pas suivi d'un nom de boîte défini par `\newsavebox`, car, en interne, ces noms sont représentés par des nombres.

Deux erreurs spécifiques à LaTeX circulaient couramment dans les groupes de discussion et sont présentées ici.

## Les exemples du « LaTeX Companion »

Le plus courant provient de la tentative d'utilisation d'un exemple du [LaTeX Companion](/1_generalites/documentation/livres/documents_sur_latex) (première édition) qui donnait le texte d'erreur suivant :

```bash
! Missing number, treated as zero.
<to be read again>
                   \relax
l.21 \begin{Ventry}{Return values}
```

Le problème vient du fait que, dans sa première édition, les exemples de *LaTeX Companion* supposaient toujours que l'extension {ctanpkg}`calc` était chargée : bien que mentionné dans le livre, ce fait n'est pas souvent noté. Le remède consiste donc à charger l'extension {ctanpkg}`calc` dans n'importe quel document utilisant des exemples du *LaTeX Companion*.

