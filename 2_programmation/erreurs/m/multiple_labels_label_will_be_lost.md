# Que signifie l'erreur : « Multiple `\label`'s : label ⟨étiquette⟩ will be lost » ?

- **Message** : `Multiple \label's : label ⟨étiquette⟩ will be lost`
- **Origine** : package *amsmath*.

On ne peut utiliser qu'une seule commande `\label` par équation à l'intérieur des environnements hors-texte {ctanpkg}`amsmath`. Il est généralement préférable de toutes les supprimer sauf la dernière, puisque c'est la seule qui aura un effet.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,étiquettes,références
```

