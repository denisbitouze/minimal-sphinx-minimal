# Que signifie l'erreur : « Missing = inserted for `\ifdim` » ?

- **Message** : `Missing = inserted for \ifdim`
- **Origine** : *TeX*.
- La condition de bas niveau `\ifdim` n'est pas suivie d'un opérateur de comparaison entre deux longueurs.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,ifthen,ifthenelse,condition,test,comparaison de nombres,comparaison de longueurs
```

