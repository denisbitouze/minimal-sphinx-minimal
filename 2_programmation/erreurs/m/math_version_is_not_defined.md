# Que signifie l'erreur : « Math version ⟨nom⟩ is not defined » ?

- **Message** : `Math version ⟨nom⟩ is not defined`

Un alphabet mathématique ou une fonte de symboles a été assignée à une version mathématique inconnue. Soit le nom a été mal saisi, soit cette version n'a pas été déclarée (il faut éventuellement charger une extension).

Il est également possible que la version mathématique sélectionnée avec `\mathversion` soit inconnue du système.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur d'alphabet mathématique
```

