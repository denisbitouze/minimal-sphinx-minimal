# Que signifie l'erreur : « Missing p-arg in array arg » ?

- **Message** : `Missing p-arg in array arg`
- **Origine** : *LaTeX*.

Un spécificateur de colonne `p` n'est pas suivi d'une expression entre accolades (contenant sa largeur) dans l'argument d'un `tabular`, d'un `array` ou d'un `\multicolumn`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème dans un tableau,array,tabular,matrice,définition de colonne
```

