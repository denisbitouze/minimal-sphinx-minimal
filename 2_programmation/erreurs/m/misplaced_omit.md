# Que signifie l'erreur : « Misplaced `\omit` » ?

- **Message** : `Misplaced \omit`
- **Origine** : *TeX*.

La primitive TeX `\omit` est utilisée en interne pour modifier les spécifications de colonnes dans une structure alignée (par exemple, pour fusionner des colonnes avec `multicolumn` dans un `tabular`). La commande `\omit` (et donc les commandes qui l'appellent) n'est permise qu'en tout début d'entrée d'alignement (c'est-à-dire immédiatement après ```` \\ ```` ou `&`).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,array,tableau,tabular,matrice,alignement,format de colonne
```

