# Que signifie l'erreur : « Missing # inserted in alignment preamble » ?

- **Message** : `Missing # inserted in alignment preamble`
- **Origine** : *TeX*.

Un motif d'alignement spécifie la mise en forme des colonnes dans une structure d'alignement. De façon interne, TeX utilise `#` pour indiquer la partie de la colonne qui reçoit l'entrée. Sous LaTeX, cette erreur n'apparaît normalement jamais seule.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème dans un tableau,array,tabular,matrice,définition de colonne
```

