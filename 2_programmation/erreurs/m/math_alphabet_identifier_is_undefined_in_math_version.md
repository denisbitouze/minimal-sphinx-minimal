# Que signifie l'erreur : « Math alphabet identifier ⟨id⟩ is undefined in math version ⟨nom⟩ » ?

- **Message** : `Math alphabet identifier ⟨id⟩ is undefined in math version ⟨nom⟩`

L'identificateur d'alphabet mathématique `⟨id⟩` a été utilisé dans une version mathématique (`⟨nom⟩`) pour laquelle il n'est pas initialisé. Une déclaration `\SetMathAlphabet` doit être ajoutée au préambule du document pour assigner un groupe de formes de fontes à cet identificateur d'alphabet.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur d'alphabet mathématique
```

