# Que signifie l'erreur : « Misplaced `\cr` » ou « Misplaced `\crcr` » ?

- **Message** : `Misplaced \cr` ou `Misplaced \crcr`
- **Origine** : *TeX*.

`\cr` est la commande TeX de bas niveau qui termine une ligne d'une structure alignée (`\crcr` en est une variante). La commande LaTeX correspondante est ```` \\ ````. TeX pense qu'il tombe sur cette commande en dehors d'une structure d'alignement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,array,tableau,tabular,matrice,alignement,fin de ligne dans un tableau
```

