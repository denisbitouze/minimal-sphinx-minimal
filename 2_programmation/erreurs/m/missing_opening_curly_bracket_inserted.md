# Que signifie l'erreur : « Missing { inserted » ?

- **Message** : `Missing { inserted`
- **Origine** : *TeX*.

TeX pense qu'il manque une accolade ouvrante et en insère une. Cette erreur peut être due à un `}` égaré à l'intérieur d'une entrée de `tabular`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,délimiteurs,parenthèses,accolades,accolade fermante,accolade ouvrante
```

