# Que signifie l'erreur : « Encoding file \`⟨nom⟩' not found » ?

- **Message** : ```` Encoding file `⟨nom⟩' not found ````
- **Origine** : extension *fontenc*.

En demandant l'encodage `⟨nom⟩`, LaTeX essaie de charger les définitions de ce codage à partir du fichier `⟨nom⟩enc.def` (après avoir converti `⟨nom⟩` en lettres minuscules). Si ce fichier de codage n'existe pas ou ne peut être trouvé par LaTeX, on obtient ce message d'erreur.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage,codage,fichier d'encodage,fichier non trouvé
```

