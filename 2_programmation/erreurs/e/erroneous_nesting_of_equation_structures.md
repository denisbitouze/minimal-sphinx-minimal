# Que signifie l'erreur : « Erroneous nesting of equation structures ; trying to recover with \`aligned' » ?

- **Message** : ''Erroneous nesting of equation structures ; trying to recover with \`aligned' ''
- **Origine** : package *amsmath*.

Seules certaines structures hors-texte peuvent être emboîtées ; `aligned` est l'une d'entre elles et le système remplace l'environnement incorrectement emboîté par cette dernière. Ce n'est probablement pas ce qui est voulu et il faudra modifier l'environnement emboîté.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,emboîtage,emboîtement,imbrication,imbriquer des équations,aligned,aligner des équations
```

