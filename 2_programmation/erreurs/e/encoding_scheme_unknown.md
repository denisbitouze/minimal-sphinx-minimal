# Que signifie l'erreur : « Encoding scheme ⟨nom⟩ unknown » ?

- **Message** : `Encoding scheme ⟨nom⟩ unknown`

Le schéma de codage `⟨nom⟩` qui a été spécifié dans une déclaration ou dans `\fontencoding` n'est pas connu du système.

Soit il n'a pas été déclaré en utilisant `\DeclareFontEncoding`, soit le `⟨nom⟩` a été mal saisi.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage,codage,problème d'encodage
```

