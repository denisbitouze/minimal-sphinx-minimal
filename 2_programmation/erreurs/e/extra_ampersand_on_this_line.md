# Que signifie l'erreur : « Extra & on this line » ?

- **Message** : `Extra & on this line`
- **Origine** : package *amsmath*.

Cette erreur survient uniquement lorsqu'on utilise les anciens environnements d' {ctanpkg}`amsmath` non décrits dans cet ouvrage. Cette erreur indique un désastre et il faut vérifier avec beaucoup d'attention l'environnement coupable.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,et commercial,esperluette,package amsmath,tableaux
```

