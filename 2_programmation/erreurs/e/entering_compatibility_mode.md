# Que signifie l'erreur « Entering compatibility mode » ?

- **Message** : `Entering compatibility mode`
- **Origine** : *LaTeX*.

Vous lancez la compilation de votre document LaTeX, et vous voyez apparaître :

```
Entering LaTeX 2.09 COMPATIBILITY MODE
```

suivi d'une ligne d'astérisques et de `!!WARNING!!` (avertissement complet ci-après).

## Cause du problème

Cela signifie que le document n'est pas écrit dans la syntaxe LaTeX « actuelle », et qu'il n'y a aucune garantie que le résultat sera formaté correctement.

En effet, LaTeX a subit une mise à jour importante en 1994 (passage de LaTeX 2.09 à LaTeX 2ε), et la compatiblité n'a pas été entièrement conservée. Le message d'erreur indique que votre document est écrit dans une version pré-1994, alors que votre compilateur est probablement beaucoup plus récent.

## Solution

- S'il s'agit réellement d'un vieux document, qu'on vous a fourni ou que vous avez trouvé sur internet, et que vous voulez seulement le compiler pour le lire, ignorez l'erreur. La compilation montrera peut-être d'autres problèmes, mais il y a des chances que vous puissiez tout de même obtenir un fichier DVI ou PDF suffisant pour une simple lecture.
- S'il s'agit d'un nouveau document sur lequel vous venez de commencer à travailler, c'est que vous avez été induit en erreur. Vous avez sans doute suivi une très ancienne documentation de LaTeX. Vous avez écrit quelque chose comme :

```latex
% !TEX noedit
\documentstyle{article}
```

ou, plus généralement :

```latex
% !TEX noedit
\documentstyle[options]{class}
```

Ces commandes viennent (comme le dit l'avertissement) de la syntaxe LaTeX 2.09 (pré-1994) et, pour vous débarrasser de l'avertissement, vous devez utiliser la syntaxe actuelle.

La forme simple est facile à transformer. Remplacez simplement :

```latex
% !TEX noedit
\documentstyle{article}
```

par :

```latex
% !TEX noedit
\documentclass{article}
```

La seconde forme est plus compliquée, car les « options » de LaTeX 2.09 mélangeaient deux types de choses : les options de classe (comme `11pt`, `fleqn`), et les extensions à charger. Ainsi :

```latex
% !TEX noedit
\documentstyle[11pt,verbatim]{article}
```

doit devenir :

```latex
% !TEX noedit
\documentclass[11pt]{article}
\usepackage{verbatim}
```

patce que `11pt` est une option de classe, alors que {ctanpkg}`verbatim` est une extension.

Il n'y a pas de moyen simple de déterminer quelles sont les options de classe sous LaTeX 2.09 ; pour {ctanpkg}`article`, on peut citer `10pt`, `11pt`, `12pt`, `draft`, `fleqn`, `leqno`, `twocolumn` et `twoside`. Toute autre option est très certainement une extension.

Votre document peut très bien « fonctionner » après les changements ci-dessus ; si ce n'est pas le cas, vous vous allez devoir réfléchir à ce que vous voulez faire, et consulter la documentation sur comment faire avec les extensions actuellement disponibles.

Si vous êtes un débutant complet, {doc}`vous pouvez commencer par un didacticiel en ligne </1_generalites/comment_faire_ses_premiers_pas>`.

Si vous avez déjà des bases en LaTeX, cherchez {doc}`des documents sur internet </1_generalites/documentation/documents/documents_sur_latex2e>` ou {doc}`un bon livre </1_generalites/documentation/livres/documents_sur_latex>`.

Cette FAQ peut également vous être utile, car sa rédaction a commencé dans les années 1990, et qu'elle met encore parfois en parallèle les anciennes syntaxes avec les nouvelles.

## Message complet

Le message complet est très clair :

```
Entering LaTeX 2.09 COMPATIBILITY MODE
*************************************************************
!!WARNING!!    !!WARNING!!    !!WARNING!!    !!WARNING!!

This mode attempts to provide an emulation of the LaTeX 2.09
author environment so that OLD documents can be successfully
processed. It should NOT be used for NEW documents!

New documents should use Standard LaTeX conventions and start
with the \documentclass command.

Compatibility mode is UNLIKELY TO WORK with LaTeX 2.09 style
files that change any internal macros, especially not with
those that change the FONT SELECTION or OUTPUT ROUTINES.

Therefore such style files MUST BE UPDATED to use
Current Standard LaTeX : LaTeX2e.
If you suspect that you may be using such a style file, which
is probably very, very old by now, then you should attempt to
get it updated by sending a copy of this error message to the
author of that file.
*************************************************************
```

```
MODE COMPATIBILITÉ LaTeX 2.09
*************************************************************
!!ATTENTION!!     !!ATTENTION!!     !!ATTENTION!!

Ce mode tente de fournir une émulation de l'environnement
LaTeX 2.09 afin que les anciens documents puissent être
compilés. Il ne doit PAS être utilisé pour les NOUVEAUX
documents !

Les nouveaux documents doivent utiliser les conventions
LaTeX actuelles et commencer par la commande \documentclass.

Il est PEU PROBABLE que ce mode de compatibilité fonctionne
avec les fichiers de style LaTeX 2.09 qui modifient des
macros internes, et encore moins avec ceux qui modifient
la SÉLECTION DE POLICES ou les ROUTINES DE SORTIE.

De tels fichiers de style DOIVENT donc être mis à jour pour
utiliser le standard actuel de LaTeX : LaTeX2e. Si vous
pensez que vous utilisez un tel fichier de style, obsolète,
vous pouvez essayer de le faire mettre à jour en envoyant
une copie de ce message d'erreur à l'auteur son fichier.
*************************************************************
```

______________________________________________________________________

*Sources :*

- {faquk}`Entering compatibility mode <FAQ-entercompmode>`,
- [LaTeX 2ε/LaTeX 2.09](https://hopf.math.purdue.edu/doc/html/latex2elatex209.html).

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreurs,ancien document LaTeX,compatibilité,ancienne version, documentstyle vs documentclass,latex 2.09
```

