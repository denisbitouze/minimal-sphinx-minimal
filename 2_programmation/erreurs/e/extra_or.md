# Que signifie l'erreur : « Extra `\or` » ?

- **Message** : `Extra \or`
- **Origine** : *TeX*.

TeX a rencontré une primitive `\or` qui n'a pas de condition de bas niveau `\ifcase` correspondante. Le `\or` supplémentaire peut être dû à une mauvaise utilisation de `\ifthenelse`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,condition en LaTeX,problème de condition,test,ifthen,ifthenelse,opérateur logique
```

