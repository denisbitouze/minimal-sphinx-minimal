# Que signifie l'erreur : « `\end` occurred inside a group » ?

- **Message** : `\end occurred inside a group at level ⟨n⟩`
- **Origine** : *TeX*.

Ce message indique qu'une chose commencée dans le document ne s'est jamais terminée avant la fin du document lui-même. Les choses impliquées (les « groupes ») sont ce que TeX utilise pour restreindre la portée des actions et définitions : vous les voyez, par exemple, dans les commandes de sélection de police historiques {doc}`non recommandées </3_composition/texte/symboles/polices/pourquoi_ne_pas_utiliser_bf_et_it>`) : `{\ it texte}` --- si l'accolade fermante est oubliée dans une telle construction, l'effet de la commande `\it` durera jusqu'à la fin du document et vous obtiendrez alors le diagnostic.

Si TeX ne vous dit pas où se situe votre problème, vous pouvez souvent le repérer en observant le document mis en forme. Sinon, vous pouvez généralement trouver des accolades incompatibles à l'aide d'un éditeur intelligent (Emacs et `winedt` offrent par exemple cette fonctionnalité). Cependant, les groupes ne sont pas *uniquement* créés en faisant correspondre accolades ouvrantes et fermantes : d'autres commandes de regroupement sont décrites ailleurs dans cette FAQ et peuvent être également une source potentielle de groupe non fermé.

`\begin{⟨environnement⟩}` enferme le corps de l'⟨*environnement*⟩ dans un groupe et établit son propre mécanisme de diagnostic. Si vous terminez le document avant de fermer un autre environnement, vous obtenez le diagnostic LaTeX usuel :

```latex
% !TEX noedit
! LaTeX Error : \begin{truc} on input line 6 ended by \end{document}.
```

Ce message, bien qu'il ne vous dise pas dans quel ⟨*fichier*⟩ se trouvait `\begin{truc}`, est généralement suffisant pour localiser le problème. Si, en mode de compilation interactif, vous acceptez l'erreur, vous obtiendrez sans doute une ou plusieurs répétitions du message « *occurred inside a group* » avant que LaTeX ne s'arrête. L'extension {ctanpkg}`checkend` reconnaît les autres commandes `\begin{machin}` non fermées et génère un message d'erreur « *ended by* » pour chacune, plutôt que de produire le message « *occurred inside a group* », ce qui est parfois utile (si vous pensez à charger l'extension).

En l'absence de telles informations de la part de LaTeX, vous devrez utiliser une recherche binaire pour trouver le groupe incriminé. Séparez le préambule du corps de votre fichier puis divisez le reste de votre texte en deux. Compilez alors chaque moitié du texte séparément avec le préambule. Cela vous indique quelle moitié du fichier est fautive. Divisez à nouveau ce texte en deux et ainsi de suite. Le processus doit être mené avec soin (il est évidemment possible de diviser un groupe correctement écrit en le découpant au mauvais endroit) mais il permet généralement d'isoler le problème.

ε-TeX (et e-LaTeX --- LaTeX exécuté sur ε-TeX) vous donne des diagnostics supplémentaires après l'exaspérant message de TeX. Il conserve en fait les informations de la même manière que LaTeX :

```bash
(\end occurred inside a group at level 3)

==== semi simple group (level 3) entered at line 6 (\begingroup) ====
==== simple group (level 2) entered at line 5 ({) ====
==== simple group (level 1) entered at line 4 ({) ====
==== bottom level ====
```

Le diagnostic indique non seulement où le groupe a commencé, mais aussi *comment* il a commencé : `\begingroup` ou `{` (qui est un alias de `\bgroup`, les deux ne se distinguant pas au niveau du moteur TeX ).

______________________________________________________________________

*Source :* {faquk}`\\end occurred inside a group <FAQ-endingroup>`

```{eval-rst}
.. meta::
   :keywords: Tex,LaTeX,erreurs,groupe,accolades
```

