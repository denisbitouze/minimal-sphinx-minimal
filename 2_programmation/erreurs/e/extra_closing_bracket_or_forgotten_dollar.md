# Que signifie l'erreur : « Extra }, or forgotten \$ » ?

- **Message** : `Extra }, or forgotten $`
- **Origine** : *TeX*.

Cette erreur est déclenchée lorsque des délimiteurs de formules mathématiques (par exemple `$...$`, `\[...\]`) et des accolades de groupes ne sont pas correctement emboîtés. TeX pense qu'il a trouvé un `}` superflu, comme dans `$x}$`, et l'ignore. Alors que dans cet exemple la suppression de l'accolade fermante est le bon choix, ce ne serait pas le cas avec `\mbox\(a}`. Ici, un `\)` fermant a été oublié et le fait de supprimer le `}` va produire des erreurs supplémentaires.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dollar oublié,problème d'accolades,curly brackets,mode mathématique
```

