# Comment traiter les erreurs ?

TeX étant un processeur de macro (*macroprocessor*), ces messages d'erreur restent souvent difficiles à comprendre ; c'est une propriété (apparemment invariante) de ce type de programme. Knuth met en lumière le problème dans le TeX​book en suggérant que vous acquériez les compétences d'investigation d'un Sherlock Holmes des temps modernes pour traiter ces erreurs. Bien que cette approche présente un certain charme romantique, ce n'est pas une bonne solution pour un simple utilisateur de TeX et LaTeX. Cette réponse (tirée en partie d'un [article de Sebastian Rahtz](http://tug.org/TUGboat/tb16-4/tb49rahtz.pdf) dans le [TUGboat volume 16, n°4](http://tug.org/TUGboat/Contents/contents16-4.html)) propose des lignes de conduite pour gérer les rapports d'erreur de TeX et est suivie d'autres réponses sur les erreurs courantes (mais embarrassantes) que vous pourriez rencontrer.

## Lire les erreurs

Les messages d'erreur peuvent sembler énigmatiques à première vue mais ils contiennent souvent une indication sur la nature du problème. Voir la question sur {doc}`la structure des erreurs </2_programmation/erreurs/structure_d_un_message_d_erreur>` pour plus de détails.

## Lire le fichier journal

Le fichier `.log` contient souvent des indications sur des éléments que vous pourriez n'avoir pas compris, souvent des éléments qui ne sont pas présentés comme des messages d'erreur.

## Définir la quantité de contexte entourant les erreurs

Les messages d'erreur vous donnent des extraits du code TeX ou du document présents là où l'erreur « s'est produite ». Il est possible de contrôler la quantité de contexte que TeX vous communique. De nos jours, LaTeX dit à TeX de vous restituer une ligne de contexte mais vous pouvez en décider autrement en indiquant dans le préambule de votre document :

```latex
% !TEX noedit
\setcounter{errorcontextlines}{999}
```

Si vous n'êtes pas un programmeur très confiant, n'ayez pas peur de diminuer quelque peu le `999` : certaines erreurs vont générer beaucoup de texte et trouver alors les problèmes peut être un sacré défi.

## Utiliser les traces

En dernier recours, l'utilisation de trace (*tracing*) peut être une technique très performante. Lire une trace intégrale de TeX ou LaTeX demande une sacrée constitution mais, une fois que vous savez le faire, la trace peut vous mener rapidement à la source d'un problème. Vous devez avoir lu une bonne part du TeX​book (voir {doc}`Que lire sur TeX et Plain TeX ? </1_generalites/documentation/livres/documents_sur_tex>`) pour bien comprendre une trace. v La commande `\tracingall` génère la version la plus exhaustive de la trace. Elle transfère aussi le document de sortie vers le terminal interactif, ce qui constitue, quelque part, un mélange pas très heureux (dans la mesure où la sortie générée est généralement vaste, toutes les traces à l'exception des plus simples demandent à être analysée dans un éditeur de texte séparément).

L'extension LaTeX {ctanpkg}`trace` (distribuée pour la première fois avec la version de 2001 de LaTeX) fournit des mécaniques de trace plus pratiques. Sa commande `\traceon` vous restitue ce que fait la commande `\tracingall` en supprimant toutefois les parts de trace autour des zones particulières verbeuses de LaTeX lui-même. Cette extension vous fournit également :

- une commande `\traceoff` (il n'y a pas de commande « off » pour `\tracingall`) ;
- et une option d'extension (`logonly`) vous permettant de supprimer le transfert de la sortie vers le terminal.

## Ne pas paniquer !

Le meilleur conseil à donner aux personnes faisant face à des erreurs TeX reste de ne pas paniquer : la plupart des erreurs sont évidentes lorsque vous retournez à la ligne de votre code source que vous indique TeX. Si cela ne marche pas, les autres réponses données dans cette FAQ traite de certaines des erreurs les plus bizarres que vous puissiez rencontrer. Vous ne devriez donc pas avoir besoin d'appeler à l {doc}`aide du grand public </1_generalites/documentation/listes_de_discussion>` mais, si c'est le cas, pensez bien à fournir un contexte complet (voir `errorcontextlines` ci-dessus) à vos interlocuteurs.

______________________________________________________________________

*Source :* {faquk}`How to approach errors <FAQ-erroradvice>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreurs,trace,log,journal
```

