# Que signifie l'erreur : « Argument of ⟨commande⟩ has an extra } » ?

- **Message** : `Argument of ⟨commande⟩ has an extra }`
- **Origine** : *TeX*.

Une accolade droite a été utilisée à la place d'un argument obligatoire de commande (par exemple, `\mbox}`). Les commandes fragiles, lorsqu'elles sont utilisées sans `\protect` dans un argument mouvant, sont souvent cassées sous une forme qui produit cette erreur ou l'une des erreurs « `Extra...` » {doc}`vues ailleurs </2_programmation/erreurs/e/start>`.

______________________________________________________________________

Vous avez regardé votre code source et vous n'avez trouvé aucun signe d'un « `}` » mal placé... En fait, ce message est une manière un peu énigmatique de vous dire que vous avez placé une {doc}`commande fragile </2_programmation/syntaxe/c_est_quoi_la_protection>` dans un argument mouvant. Si ces termes ne vous parlent pas, {doc}`ce lien </2_programmation/syntaxe/c_est_quoi_la_protection>` vous explique ces deux concepts propres à LaTeX.

## Quelques exemples

La commande `\footnote` est un cas classique de commande fragile et la mettre dans l'argument d'une commande `\section`, dont l'argument est par défaut mouvant, va poser problème. En voici un exemple qu'on appelera ici le « cas `\section` ».

```latex
% !TEX noedit
\section{Un beau titre\footnote{je n'ai pas mieux à proposer}}
```

Nous recevons alors le message d'erreur suivant :

```latex
% !TEX noedit
! Argument of \@sect has an extra }.
```

Ceci arrive de la même manière avec les légendes (l'exemple suivant est une version simplifiée d'un exemple trouvé sur `comp.text.tex`). On appelera cet exemple le « cas `\caption` ».

```latex
% !TEX noedit
\caption{Énergie : \[e=mc^2\]}
```

Cet exemple va générer l'erreur suivante :

```latex
% !TEX noedit
! Argument of \@caption has an extra }.
```

Le cas suivant, pourtant à peine différent, va donner une réponse autre.

```latex
% !TEX noedit
\caption{Énergie : \(e=mc^2\)}
```

Ce cas va s'avérer plus ennuyeux que ceux vus auparavant : il n'y a pas d'erreur lors de la première exécution du code... mais, lors de la deuxième exécution, l'erreur apparaît lors de la génération de la liste des figures avec la commande `\listoffigures` (ou des tables avec la commande `\listoftables`) :

```latex
% !TEX noedit
! LaTeX Error : Bad math environment delimiter.
```

## Quelques solutions

### Utiliser la commande `\protect`

La solution consiste normalement à remplacer votre commande fragile par une commande robuste ou de forcer votre commande à devenir robuste en la préfixant de la commande `\protect`, ce qui donnerait dans le cas `\section` ci-dessus :

```latex
% !TEX noedit
\section{Un beau titre\protect\footnote{je n'ai pas mieux à proposer}}
```

### Ne pas utiliser de commande fragile

Toutefois, dans le cas `\section` comme dans le cas `\caption`, vous pouvez séparer l'argument mouvant en utilisant la syntaxe complète de la commande `\section[mouvant]{statique}`. Ceci donne une autre solution classique : omettre tout simplement la commande fragile de l'argument mouvant. De cette manière, le cas `\caption` serait rédigé ainsi :

```latex
% !TEX noedit
\caption[Énergie (équation d'Einstein)]{Énergie : \(E=mc^2\)}
```

### Utiliser la commande \\ensuremath avec les mathématiques

En pratique, l'insertion de mathématiques dans un argument mouvant a été traitée en LaTeX avec la commande robuste `\ensuremath` :

```latex
% !TEX noedit
\caption{Energie : \ensuremath{E=mc^2}}
```

## En conclusion

Il est donc toujours intéressant de chercher des alternatives à l'utilisation de la méthode `\protect`.

Les notes de bas de page peuvent créer des cas encore plus complexes, la question {doc}`Comment obtenir une note de bas de page dans un titre ? </3_composition/texte/pages/footnotes/inserer_une_note_de_bas_de_page_dans_un_titre>` » traite donc spécifiquement ces cas.

______________________________________________________________________

*Sources :*

- {faquk}`An extra }? <FAQ-extrabrace>`
- <https://latex.developpez.com/faq/erreurs?page=A>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,accolade en trop,erreur d'accolade,argument d'une commande,argument d'une macro
```

