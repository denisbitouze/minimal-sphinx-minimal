# Que signifie l'erreur : « A <Box> was supposed to be here » ?

- **Message** : `A <Box> was supposed to be here`
- **Origine** : *TeX*.

Cette erreur résulte de l'utilisation d'une commande de boîte, telle que `\sbox`, avec un premier argument non valide (c'est-à-dire non déclaré avec `\newsavebox`). Habituellement, on obtient d'abord l'erreur « `Missing number, treated as zero` », qui indique que TeX utilise le registre de boîte zéro.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=A>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,missing box,boîte manquante,boîyte perdue,newsavebox
```

