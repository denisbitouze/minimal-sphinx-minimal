# Que signifie l'erreur : « `\verb` illegal in command argument » ?

- **Message** : `\verb illegal in command argument`
- **Origine** : *LaTeX*.

Il n'est en général pas possible d'utiliser `\verb` (ou un environnement `verbatim`) comme argument d'une autre commande.

:::{note}
Si l'on a besoin de texte verbatim dans ces emplacements, on peut utiliser `\SaveVerb` et `\UseVerb` de l'extension {ctanpkg}`fancyvrb`, décrite à la section 3.4.3 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=V>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,verbatim en argument,commandes fragiles
```

