# Que signifie l'erreur : « `\verb` ended by end of line » ?

- **Message** : `\verb ended by end of line`
- **Origine** : *LaTeX*.

Voici les erreurs les mieux détectées : l'argument d'un `\verb` doit être placé sur une seule ligne. L'erreur signale donc que l'on a oublié le délimiteur final de l'argument ou que l'argument se trouve sur plusieurs lignes dans le source. Dans le cas d'arguments très longs, il peut être utile de les découper en plusieurs commandes `\verb` et, si nécessaire, de masquer les coupures de lignes dans le source avec un signe `%`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=V>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,verbatim,verb sur plusieurs lignes
```

