# Que signifie l'erreur : « `\LoadClass` in package file » ?

- **Message** : `\LoadClass in package file`
- **Origine** : *LaTeX*.

La commande `\LoadClass` n'est autorisée que dans les fichiers de classe (voir section A.4 du *LaTeX Companion*
.. todo::).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=L>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème de package,écrire une classe
```

