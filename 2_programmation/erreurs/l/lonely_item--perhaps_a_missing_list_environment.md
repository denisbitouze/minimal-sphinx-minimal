# Que signifie l'erreur : « Lonely `\item`--perhaps a missing list environment » ?

- **Message** : `Lonely \item--perhaps a missing list environment`

La commande `\item` n'est autorisée qu'à l'intérieur des structures de liste, et LaTeX pense que celle-ci a été trouvée en dehors d'une liste.

Contrairement à l'erreur `Something's wrong -- perhaps a missing \item` {doc}`(voir ici) </2_programmation/erreurs/s/something_is_wrong--perhaps_a_missing_item>`, le diagnostic de LaTeX est souvent correct dans ce cas.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=L>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur avec itemize,
```

