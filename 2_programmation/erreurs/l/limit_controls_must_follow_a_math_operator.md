# Que signifie l'erreur : « Limit controls must follow a math operator » ?

- **Message** : `Limit controls must follow a math operator`
- **Origine** : *TeX*.

On ne peut utiliser `\limits` et `\nolimits` qu'après un opérateur mathématique comme `\sum`. Voir tableau 8.4 page 510 du *LaTeX Companion*

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « pour une liste des opérateurs usuels. »
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=L>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,opérateur sum,opérateur intégrale,limites d'une intégrale,limites d'une somme
```

