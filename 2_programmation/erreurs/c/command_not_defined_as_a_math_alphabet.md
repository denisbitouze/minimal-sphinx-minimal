# Que signifie l'erreur : « Command ⟨nom⟩ not defined as a math alphabet » ?

- **Message** : `Command ⟨nom⟩ not defined as a math alphabet`

Cette erreur survient lorsqu'on essaie d'utiliser `\SetMathAlphabet` sur un `⟨nom⟩` qui n'a pas été déclaré précédemment par une commande `\DeclareMathAlphabet` ou `\DeclareSymbolFontAlphabet` pour être défini comme un identificateur d'alphabet mathématique.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définir un alphabet mathématique,problème d'alphabet
```

