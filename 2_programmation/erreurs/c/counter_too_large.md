# Que signifie l'erreur : « Counter too large » ?

- **Message** : `Counter too large`

Cette erreur se produit lorsqu'on essaie d'afficher une valeur de compteur avec `\fnsymbol`, `\alph` ou `\Alph` et que cette valeur est en dehors de l'intervalle autorisé pour la forme d'affichage choisie.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème de compteur,valeur trop grande,nombre trop grand
```

