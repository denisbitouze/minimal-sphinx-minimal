# Que signifie l'erreur : « `\caption` outside float » ?

- **Message** : `\caption outside float`
- **Origine** : *LaTeX*.

Une commande `\caption` a été trouvée en dehors d'un environnement de flottant tel que `figure` ou `table`. Ce message d'erreur est inactivé par certaines extensions décrites au chapitre 6 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,légende,flottant,problème de légende,où mettre la légende
```

