# Que signifie l'erreur : « Can be used only in preamble » ?

- **Message** : `Can be used only in preamble`
- **Origine** : *LaTeX*.

LaTeX a rencontré une commande ou un environnement qui doit être utilisé uniquement dans une extension ou le préambule (c'est-à-dire avant `\begin{document}`).

Cette erreur peut être également due à un second `\begin{document}`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,préambule,déclarations,usepackage,document LaTeX
```

