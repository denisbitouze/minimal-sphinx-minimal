# Que signifie l'erreur : « `\begin{⟨env⟩}` on input line ⟨num-ligne⟩ ended by `\end{⟨autre-env⟩}` » ?

- **Message** : `\begin{⟨env⟩} on input line ⟨num-ligne⟩ ended by \end{⟨autre-env⟩}`
- **Origine** : *LaTeX*.

LaTeX indique cette erreur lorsqu'il voit que l'environnement `⟨env⟩` se termine par le code de terminaison de l'environnement `⟨autre-env⟩`. La plupart du temps, cette erreur est due à l'oubli du `\end{⟨env⟩}`. Une autre possibilité pour obtenir cette erreur est d'essayer d'utiliser les environnements de type verbatim ou un environnement hors-texte {ctanpkg}`amsmath` à l'intérieur de la définition d'un environnement défini par l'utilisateur, ce qui est souvent impossible.

```{eval-rst}
.. todo:: Voir section 3.4.3 (page 168) du *LaTeX Companion* pour des solutions utilisant des environnements de type ``verbatim``.
```

S'il ne s'agit d'aucun des cas précédents et que l'on est absolument certain que tous les environnements sont convenablement emboîtés alors, quelque part entre le début de `⟨env⟩` et l'endroit où se situe l'erreur, doit se trouver une commande qui engendre un `\endgroup` sans `\begingroup` antérieur correspondant, ce qui fait croire à LaTeX que l'environnement `⟨env⟩` prend fin à cet endroit.

Pour trouver l'origine du problème, on peut déplacer la commande de fin d'environnement, en la rapprochant du départ de celui-ci, jusqu'à ce que le problème disparaisse.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=B>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème d'environnement,begin et end
```

