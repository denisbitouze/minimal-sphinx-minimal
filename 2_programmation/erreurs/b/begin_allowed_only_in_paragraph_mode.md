# Que signifie l'erreur : « `\begin{⟨env⟩}` allowed only in paragraph mode » ?

- **Message** : `\begin{⟨env⟩} allowed only in paragraph mode`
- **Origine** : extension *amsmath*.

Il existe de nombreux emplacements (à l'intérieur du mode horizontal, en mode mathématique, etc.) où il n'y a aucune sens à avoir un hors-texte mathématique. Avec {ctanpkg}`amsmath`, la totalité de ce hors-texte `⟨env⟩` est tout simplement ignorée.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=B>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode horizontal,mode paragraphe,mode mathématique
```

