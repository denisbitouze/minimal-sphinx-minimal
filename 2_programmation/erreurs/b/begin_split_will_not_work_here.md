# Que signifie l'erreur : « `\begin{split}` won't work here » ?

- **Message** : `\begin{split} won't work here`
- **Origine** : extension *amsmath*.

Soit cet environnement `split` ne se situe pas à l'intérieur d'une équation, soit on doit utiliser `aligned`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=B>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,amsmath,erreur de mode mathématique,environnement split
```

