# Que signifie l'erreur : « Infinite glue shrinkage found ⟨quelque part⟩ » ?

- **Message** : `Infinite glue shrinkage found ⟨quelque part⟩`
- **Origine** : *TeX*.

Pour couper un paragraphe en lignes ou la réserve en pages, TeX suppose qu'il n'y a pas de longueur élastique pouvant se compresser arbitrairement puisque cela signifierait que n'importe quelle quantité de matériel pourrait loger dans une seule page. Ainsi, il n'est pas permis d'écrire `\hspace{0pt minus 1fil}` dans un paragraphe ou `\vspace{0pt minus 1fil}` entre deux paragraphes. Le faire provoque cette erreur `⟨quelque part⟩` indique l'emplacement approximatif où le matériel interdit a été trouvé).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,espacement vertical,espacement horizontal,espace extensible,espace compressible
```

