# Que signifie l'erreur : « Improper `\spacefactor` » ?

- **Message** : `Improper \spacefactor`
- **Origine** : *TeX*.

Au contexte près, cette erreur est à rapprocher de l'erreur « [You can't use `\spacefactor` in vertical mode](/2_programmation/erreurs/y/you_cannot_use_spacefactor_in_vertical_mode) », autrement dit une utilisation probablement erronée de la commande `\@`.

______________________________________________________________________

*Source :* {faquk}`\\spacefactor complaints <FAQ-atvert>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreur,spacefactor
```

