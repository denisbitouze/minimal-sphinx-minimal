# Que signifie l'erreur : « Incompatible list can't be unboxed » ?

- **Message** : `Incompatible list can't be unboxed`
- **Origine** : *TeX*.

On a demandé à TeX de vider une boîte avec un contenu horizontal alors qu'il était en train de construire une liste verticale (ou *vice versa*). Soit il s'agit d'une sérieuse erreur de programmation dans une extension, soit on utilise une commande d'une façon explicitement non prévue.

Par exemple, les commandes de l'extension {ctanpkg}`soul` produisent cette erreur lorsqu'on tente de les emboîter.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mode horizontal,mode vertical,soulignement,problème avec soul
```

