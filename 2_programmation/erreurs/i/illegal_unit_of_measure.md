# Que signifie l'erreur : « Illegal unit of measure (pt inserted) » ?

- **Message** : `Illegal unit of measure (pt inserted)`
- **Origine** : *TeX*.

On obtient cette erreur lorsqu'on fait une faute de frappe sur l'unité de longueur ou qu'on l'oublie lors de la spécification de la valeur d'un paramètre de longueur. Voir section A.1.5 du *LaTeX Companion*

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « pour plus de détails. »
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,unité de mesure,unité de longueur
```

