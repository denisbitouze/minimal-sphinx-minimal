# Que signifie l'erreur : « Improper `\prevdepth` » ?

- **Message** : `Improper \prevdepth`
- **Origine** : *TeX*.

On a utilisé les commandes `\the\prevdepth` ou `\showthe\prevdepth` en dehors du mode vertical, ce qui est interdit. Cette erreur survient également lorsqu'on place un flottant (par exemple, un environnement `figure` ou `table`) à l'intérieur d'un environnement mathématique hors-texte.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,environnement mathématique,mode vertical,erreur avec un flottant
```

