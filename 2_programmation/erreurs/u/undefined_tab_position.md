# Que signifie l'erreur : « Undefined tab position » ?

- **Message** : `Undefined tab position`
- **Origine** : *LaTeX*.

Cette erreur survient lorsqu'on essaie d'atteindre une position de tabulation dans un environnement `tabbing` avec `\>`, `\+`, `\-` ou `\<`, alors que cette position de tabulation n'a pas été précédemment définie avec `\=`. Soit on a oublié le `\=`, soit on a utilisé `\+` ou `\pushtabs` en se trompant sur la position où l'on voulait effectivement se rendre.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,tabulation
```

