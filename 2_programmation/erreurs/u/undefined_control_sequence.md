# Que signifie l'erreur : « Undefined control sequence » ?

- **Message** : `Undefined control sequence`
- **Origine** : *TeX*.

Il s'agit sans doute de l'erreur LaTeX la plus fréquente, même si elle ressemble à un message d'erreur de TeX. On a utilisé un nom de commande sans définir celle-ci au préalable. Le plus souvent, on a mal saisi le nom de commande (par exemple, `\bmox` au lieu de `\mbox`). Dans ce cas, on peut répondre `i\mbox`, ce qui insère le nom correct. Plus tard, on corrigera le fichier source.

Il est également possible d'obtenir cette erreur à la suite de l'utilisation d'une commande fragile à l'intérieur d'un argument mouvant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,séquence de contrôle non définie,erreur de compilation,nom de macro
```

