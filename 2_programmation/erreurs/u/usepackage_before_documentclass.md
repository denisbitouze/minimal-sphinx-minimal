# Que signifie l'erreur : « `\usepackage` before `\documentclass` » ?

- **Message** : `\usepackage before \documentclass`
- **Origine** : *LaTeX*.

La déclaration `\usepackage` ne peut être utilisée qu'après avoir chargé la classe principale de document avec `\documentclass`. À l'intérieur d'un fichier de classe, on doit utiliser `\RequirePackage`.

:::{note}
Cela dit, il est techniquement possible de charger une extension avant une classe avec `\RequirePackage`, mais on doit l'éviter à moins de savoir ce que l'on fait.
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,structure de dociument,préambule,classe de document,packages
```

