# Que signifie l'erreur : « Use of ⟨commande⟩ doesn't match its definition » ?

- **Message** : `Use of ⟨commande⟩ doesn't match its definition`
- **Origine** : *TeX*.

Les définitions de macros de bas niveau, réalisées avec `\def` à la place de `\newcommand` et de ses amis, demandent parfois des délimiteurs d'arguments spéciaux (par exemple, le `(..)` des commandes d'image). Si `⟨commande⟩` est une commande LaTeX, il faut vérifier sa syntaxe. Sinon, il s'agit le plus souvent d'une erreur parasite due à l'utilisation d'une commande fragile dans un argument mouvant sans `\protect`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème avec une commande,définition de macro,syntaxe
```

