# Que signifie l'erreur : « UTF-8 string `\u8` : ⟨séquence-8-bits⟩ not set up for LaTeX use » ?

- **Message** : `UTF-8 string \u8 : ⟨séquence-8-bits⟩ not set up for LaTeX use`
- **Origine** : package *inputenc*.

Le caractère Unicode indiqué par la chaîne UTF-8 `⟨séquence-8-bits⟩` est inconnu de LaTeX. En supposant qu'il soit disponible avec le codage de fonte utilisé dans le document, on doit le définir avec une déclaration `\DeclareUnicodeCharacter`. Voir section 7.11.3 page 451 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage,codage de fonte,UTF-8,Unicode
```

