# Que signifie l'erreur : « Unknown option \`⟨option⟩' for package \`⟨nom⟩' » ?

- **Message** : ''Unknown option \`⟨*option*⟩' for package \`⟨*nom*⟩' ''
- **Origine** : *LaTeX*.

On a spécifié une option `⟨option⟩` pour l'extension `⟨nom⟩` qui n'est pas déclarée par celle-ci. On pourra consulter la documentation de l'extension pour connaître les options disponibles.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,option de package,option non trouvée
```

