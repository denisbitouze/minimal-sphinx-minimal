# Que signifie l'erreur : « Float(s) lost » ?

- **Message** : `Float(s) lost`

Un ou plusieurs flottants (par exemple figures ou tableaux), ou des commandes `\marginpar`, n'ont pas été composés. La raison la plus fréquente est d'avoir placé un environnement flottant ou une note marginale à l'intérieur d'un `\marginpar`, d'un environnement `minipage`, d'un autre flottant, d'une `\parbox` ou d'une `\footnote` ; LaTeX détecte ce problème très tardivement, lorsqu'il a terminé le document. Cela peut rendre difficile la localisation de l'emplacement fautif.

La meilleure solution est de diviser le document en deux de façon répétitive (par exemple en utilisant la primitive `\endinput`) jusqu'à ce que la portion produisant l'erreur soit suffisamment petite pour pouvoir la repérer (sorte de recherche par dichotomie).

Si un emboîtement incorrect n'est pas la cause principale, on peut avoir rencontré un sérieux problème de codage de l'algorithme des flottants, probablement dû au chargement de certaines extensions.

______________________________________________________________________

L'erreur

```
! LaTeX Error : Float(s) lost.
```

se produit rarement mais elle paraît toujours incroyablement mystérieure quand elle survient.

Le message indique bien ce qui se passe : un ou plusieurs flottants n'ont pas été composés, que ce soient des figures, des tables... ou même des notes marginales (dites *marginpar*). Les notes marginales sont en effet traitées par les outils comme des flottants, ce qui explique pourquoi elles se retrouvent associées à ce message d'erreur.

La raison la plus probable est que vous avez placé un flottant ou une commande `\marginpar` à l'intérieur d'un autre flottant ou d'une autre note marginale ou bien encore à l'intérieur d'un environnement `minipage`, d'une commande `\parbox` ou d'une commande `\footnote`. Notez que l'erreur peut être détectée bien après l'exécution des commandes problématiques. Aussi, les techniques de {doc}`pistage des erreurs </2_programmation/erreurs/interpreter_les_messages_d_erreur2>` doivent être utilisées.

L'auteur de ce texte a également rencontré cette erreur alors qu'il développait des commandes utilisant les mécanismes de LaTeX pour gérer les flottants. Et les personnes procédant à de telles manipulations doivent pour la plupart être capables de traiter par elles-mêmes ce type de problème...

______________________________________________________________________

*Sources :*

- {faquk}`Float(s) lost <FAQ-fllost>`,
- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,flottant perdu,problème avec les flottants,flottant dans une note de bas de page
```

