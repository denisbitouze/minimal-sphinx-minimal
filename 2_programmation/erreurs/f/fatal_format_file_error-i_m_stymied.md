# Que signifie l'erreur : « Fatal format file error; I'm stymied » ?

- **Message** : `Fatal format file error; I'm stymied`
- **Origine** : *TeX*.

TeX et LaTeX affichent souvent cette erreur lorsque vous jouez avec la configuration ou que vous venez d'installer une nouvelle version.

Le *fichier de* {doc}`format </1_generalites/glossaire/qu_est_ce_qu_un_format>` contient les commandes qui définissent le système que vous souhaitez utiliser : du plus simple {doc}`Plain TeX </1_generalites/glossaire/qu_est_ce_que_plain_tex>`) au plus compliqué, comme {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>` ou {doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>`. À partir de la commande que vous exécutez (`tex`, `latex`, etc.), TeX sait quel format vous voulez.

Ce message d'erreur (« erreur fatale de fichier de format, je suis bloqué » [^footnote-1]) signifie que TeX ne comprend pas le format demandé. Évidemment, cela se produit si le fichier de format a été corrompu, mais ce n'est généralement pas le cas. La cause la plus courante du message est qu'un nouvel exécutable de TeX a été installé dans le système. Or les formats sont lié aux exécutables. Donc, la nouvelle version de TeX que vous venez d'installer ne peut pas comprendre pas le format généré par celle que vous aviez installée l'année dernière.

Ce problème se résout donc regénérant le format. Bien sûr, cela dépend de la distribution que vous utilisez.

## Cas d'une distribution MiKTeX

Il faut aller chercher le programme `MiKTeX Options` puis lancer la mise à jour.

## Cas d'une distribution TeX Live

Il faut ici lancer l'une des deux commandes :

```bash
fmtutil --all
```

ou, en indiquant le *nom du format*,

```bash
fmtutil --byfmt nom_format
```

## Cas d'une distribution teTeX

{octicon}`alert;1em;sd-text-warning` La distribution {ctanpkg}`teTeX <tetex>` est classée comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`. Ce qui suit est informatif.

Il faut ici lancer l'une des deux commandes :

```bash
fmtutil --all
```

ou, en indiquant le *nom du format*,

```bash
fmtutil --byfmt=nom_format
```

______________________________________________________________________

*Source :* {faquk}`"Fatal format file error; I'm stymied" <FAQ-formatstymy>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,erreurs,fichier de format,problème d'installation des formats,mise à jour de LaTeX
```

[^footnote-1]: Il s'agit bien d'un « fichier de format », et non d'un « format de fichier ».

