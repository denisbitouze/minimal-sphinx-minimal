# Que signifie le message : « Font shape ⟨fonte⟩ not available » ?

- **Message** : `Font shape ⟨fonte⟩ not available`
- **Origine** : LaTeX.

LaTeX gère des tableaux des familles de polices dont il a connaissance. Ces tableaux contiennent non seulement les familles de polices mais aussi les formes et séries dans lesquelles ces familles de polices sont disponibles. Dans certains cas, ces tableaux répertorient également les tailles auxquelles LaTeX peut les utiliser.

Lorsque vous spécifiez une police, en utilisant l'une des commandes de sélection de police, LaTeX recherche la police (c'est-à-dire une police qui correspond au codage, à la famille, à la forme, à la série et à la taille que vous souhaitez) dans ses tableaux. Si la police n'est pas disponible à la taille souhaitée, vous verrez un message comme :

```latex
% !TEX noedit
LaTeX Font Warning : Font shape `OT1/cmr/m/n' in size <11.5> not available
(Font)              size <12> substituted on input line ...
```

Il y aura également un avertissement une fois que LaTeX rencontre la commande `\end{document}` :

```latex
% !TEX noedit
LaTeX Font Warning : Size substitutions with differences
(Font)              up to 0.5pt have occurred.
```

Le message vous indique que vous avez choisi une taille de police qui ne figure pas dans la liste des tailles « disponibles » pour cette police. LaTeX a donc choisi la taille de police la plus proche dont il a connaissance. En fait, vous pouvez dire à LaTeX d'autoriser généralement *n'importe quelle taille* : les restrictions viennent du temps où seules les polices bitmap étaient disponibles. Ces règles ne se sont jamais appliquées aux polices qui se présentent sous une forme redimensionnable. De fait, de nos jours, la plupart des polices qui étaient autrefois uniquement bitmap sont également disponibles sous forme redimensionnable (Adobe Type 1). Si votre installation utilise ces versions des polices *Computer Modern* ou *European Computer Modern* (EC), vous pouvez demander à LaTeX de supprimer les restrictions. Utilisez l'extension {ctanpkg}`type1cm` ou {ctanpkg}`type1ec` selon le cas.

Si la combinaison de la forme et de la série de police n'est pas disponible, LaTeX aura généralement été informé d'une combinaison de secours et la sélectionnera. Par exemple :

```latex
% !TEX noedit
LaTeX Font Warning : Font shape `OT1/cmr/bx/sc' undefined
(Font)              using `OT1/cmr/bx/n' instead on input line 0.
```

Les substitutions peuvent également se faire sans message d'avertissement dans le fichier journal. Par exemple, si vous spécifiez un encodage pour lequel il n'y a pas de version dans la famille de polices actuelle, la « famille par défaut pour l'encodage actif » est sélectionnée. Cela se produit, par exemple, si vous utilisez la commande `\textbullet`, qui est normalement extraite de la police des symboles mathématiques, qui est en codage `OMS` :

```latex
% !TEX noedit
LaTeX Font Info :    Font shape `OMS/cmr/m/n' in size <10> not available
(Font)              Font shape `OMS/cmsy/m/n' tried instead on input line ...
```

En résumé, ces messages n'indiquent pas des erreurs mais des informations sur ce que LaTeX a fait de votre texte. Vous devriez toutefois les étudier (même s'ils sont souvent peu surprenant) pour mieux appréhender vos polices.

______________________________________________________________________

*Source :* {faquk}`Warning Font shape … not available <FAQ-fontunavail>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```

