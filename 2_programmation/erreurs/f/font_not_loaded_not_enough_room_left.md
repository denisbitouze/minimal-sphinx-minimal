# Que signifie l'erreur : « Font ⟨nom-interne⟩ = ⟨externe⟩ not loaded : Not enough room left » ?

- **Message** : `Font ⟨nom-interne⟩ = ⟨externe⟩ not loaded : Not enough room left`
- **Origine** : *TeX*.

TeX ne peut charger qu'un certain nombre de fontes et il n'y a plus de place disponible pour charger `⟨externe⟩`.

```{eval-rst}
.. todo:: Pour savoir quelles sont les fontes chargées, il faut utiliser l'extension :ctanpkg:`tracefnt` décrite à la section 7.5.6 du *LaTeX Companion*.
```

Une raison possible pour un chargement excessif de fontes est l'utilisation de tailles de fontes pour lesquelles LaTeX doit calculer et charger les fontes mathématiques correspondantes.

```{eval-rst}
.. todo:: Voir section 7.10.7 du *LaTeX Companion* pour plus de détails.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,Not enough room left,nombre de fontes,police non chargée,fonte non chargée
```

