# Que signifie l'erreur : « OK (see the transcript file) » ?

- **Message** : `OK (see the transcript file)`
- **Origine** : *TeX*.

Comme pour le message précédent, il ne s'agit pas vraiment d'un message d'erreur. On a utilisé une commande de trace, telle que `\showbox` ou `\showlists`, sans rediriger également l'affichage du résultat sur le terminal.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=O>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,invite de commande LaTeX,compilation interactive
```

