# Que signifie l'erreur : « Only one # is allowed per tab » ?

- **Message** : `Only one # is allowed per tab`
- **Origine** : *TeX*.

Cette erreur indique un motif d'alignement incorrect. Avec LaTeX, ce message ne devrait jamais apparaître, sauf si l'on place une commande fragile dans un argument mouvant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=O>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dièse,une seule dièse
```

