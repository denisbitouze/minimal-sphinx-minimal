# Que signifie l'erreur : « runpopen command not allowed » ?

- **Message** : `runpopen command not allowed`
- **Origine** : *LaTeX*.

On a utilisé la commande `\input` pour {doc}`exécuter un sous-processus et récupérer sa sortie </2_programmation/compilation/write18>`, par exemple de cette façon :

```latex
% !TEX noedit
\input|"ls -1 *.data"
```

mais les mécanismes de sécurité de LaTeX ont empêché l'exécution du sous-processus.

La solution est de lancer la compilation avec l'option `-``-shell-escape` :

```bash
pdflatex --shell-escape mon_document.tex
```

:::{warning}
Cette option autorise le compilateur LaTeX à lancer n'importe quelle commande, y compris une commande qui effacerait tous les fichiers de votre disque ou qui enverrait vos données privées sur internet.

Ne l'utilisez que pour un document auquel vous faites parfaitement confiance (par exemple si vous l'avez vous-même écrit).
:::

______________________________________________________________________

*Source :*

- [LaTeX.org : I get an error of extractbb when I type latexmk](https://latex.org/forum/viewtopic.php?t=33031).

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,sous-processus,pipe,stdout,fork
```

