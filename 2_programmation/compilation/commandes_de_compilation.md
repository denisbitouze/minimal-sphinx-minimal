# Comment interagir avec la compilation ?

À partir d'un fichier source `fichier.tex`, pour générer un fichier `fichier.dvi`, il faut appliquer :

```bash
latex fichier.tex
```

Lorsqu'une compilation échoue sur une erreur, la ligne où est située l'erreur est indiquée. L'emplacement de l'erreur dans la ligne est précisé par un retour à la ligne. Une explication succincte de l'erreur est également fournie.

- La commande `?` permet alors d'avoir un menu d'aide.
- La commande `h` peut permettre d'avoir une explication plus détaillée de l'erreur sur laquelle LaTeX s'est arrêté.
- La touche *return* peut permettre de forcer la suite de la compilation.
- La commande `s` permet de visualiser les messages d'erreur suivants.
- La commande `r` permet de poursuivre la compilation sans arrêt.
- La commande `q` permet de continuer la compilation sans messages.
- La commande `i` permet d'insérer quelque chose (une balise oubliée par exemple) pour pouvoir poursuivre la compilation.
- La commande `e` permet d'éditer le fichier source.
- La commande `x` permet d'abandonner la compilation.
- Un chiffre de `1` à `9` permet d'ignorer les *x* prochains caractères du source.

Lorsque la compilation se termine normalement, elle produit un fichier `fichier.dvi` qui peut être visualisé par un utilitaire tel que :

```bash
xdvi fichier.dvi
```

A partir d'un fichier `fichier.dvi`, pour générer un fichier postscript, il faut utiliser un utilitaire tel que :

```bash
dvips fichier.dvi
```

Le fichier `fichier.ps` alors généré peut être imprimé. Par exemple :

```bash
lpr -Pimprimante fichier.ps
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,compiler son document,compiler un document LaTeX
```

