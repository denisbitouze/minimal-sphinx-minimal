# Pourquoi `\edef` ne fonctionne pas avec `\protect` ?

Les commandes LaTeX {doc}`robustes </2_programmation/syntaxe/c_est_quoi_la_protection>` sont :

- soit *naturellement robustes*, ce qui signifie qu'elles n'ont jamais besoin de `\protect` ;
- soit *auto-protégées*, ce qui signifie que `\protect` est intégrée dans leur définition.

Les commandes auto-protégées et les commandes fragiles utilisant `\protect` ne sont robustes que dans un contexte où le mécanisme `\protect` est correctement géré. De fait, le corps d'une définition `\edef` ne gère pas correctement `\protect` car `\edef` est une primitive TeX et non une commande LaTeX.

Ce problème est résolu par une commande interne LaTeX `\protected@edef` qui fait le travail de `\edef` tout en respectant le mécanisme de `\protect`. Une commande `\protected@xdef` fait un travail équivalent pour `\xdef`.

Bien sûr, ces commandes doivent être utilisées de façon particulière car elles sont internes. Sur ce point, voir « [À quoi servent \\makeatletter et \\makeatother ?](/2_programmation/macros/makeatletter_et_makeatother) ».

______________________________________________________________________

*Source :* {faquk}`« \\edef » does not work with « \\protect » <FAQ-edef>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,latex,commandes,robuste,programmation,\\edef,\\protect,protection
```

