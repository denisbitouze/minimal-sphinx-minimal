# Comment traduire des commandes LaTeX en commandes TeX ?

À plusieurs endroits de cette FAQ sont données des réponses intégrant des définitions de commandes LaTeX. Parfois, ces commandes peuvent également aider les utilisateurs de Plain TeX ou d'autres [formats](https://faq.gutenberg.eu.org/1_generalites/glossaire/qu_est_ce_qu_un_format). Cette page tente de fournir un guide approximatif pour transcrire ces définitions de commandes dans d'autres formats.

LaTeX propose des commandes qui remplacent `\def` du fait de sa philosophie générale : l'utilisateur doit être protégé de lui-même. Ainsi, l'utilisateur dispose des commandes suivantes :

- deux commandes différentes selon que la commande à définir existe (`\renewcommand`) ou pas (`\newcommand`). Si l'état de la commande s'avère différent de celui attendu par l'utilisateur, une erreur est signalée ;
- la commande `\providecommand` définit uniquement une commande si la cible n'est pas déjà définie ;
- la commande `\DeclareRobustCommand` crée une commande qui est « robuste » (c'est-à-dire qui ne se développera pas si elle est soumise à un "développement protégé" par LaTeX). Pour un utilisateur de Plain TeX, `\DeclareRobustCommand` doit être traitée comme une version non vérifiable de `\newcommand`.

Ainsi, LaTeX n'a pas d'équivalent direct de `\def`, qui ignore l'état actuel de la commande.

Les commandes LaTeX sont, par défaut, définies avec la structure `\long` ; un « `*` » optionnel entre `\newcommand` et ses arguments spécifie que la commande n'utilise pas la structure `\long`. Le « `*` » est ainsi détecté par une commande `\ifstar` qui utilise `\futurelet` pour basculer entre deux branches et qui absorbe le « `*` » : les utilisateurs de LaTeX sont encouragés à considérer le « `*` » comme une part du nom de la commande.

Les arguments d'une commande LaTeX sont spécifiés par deux arguments optionnels : un nombre d'arguments (de 0 à 9 et, si le nombre est 0, l'argument optionnel peut être omis) et une valeur par défaut pour le premier argument si celui-ci peut être facultatif. Par exemple :

```latex
% !TEX noedit
\newcommand\truc{...}
\newcommand\truc[0]{...}
\newcommand\truc[1]{...#1...}
\newcommand\truc[2][machin]{...#1...#2...}
```

Dans le dernier cas, `\truc` peut être appelé sous la forme `\truc{blabla}` qui équivaut à `\truc[machin]{blabla}` (en utilisant la valeur par défaut donnée pour le premier argument) ou sous la forme `\foo[chose]{blabla}` (avec un premier argument explicite).

La manière de coder des commandes avec des arguments optionnels est illustrée ici pour notre exemple `\truc` :

```latex
% !TEX noedit
\def\truc{\futurelet\next\@r@truc}
\def\@r@truc{\ifx\next[%
    \let\next\@x@truc
  \else
    \def\next{\@x@truc[machin]}%
  \fi
  \next
}
\def\@x@truc[#1]#2{...#1...#2...}
```

______________________________________________________________________

*Source :* {faquk}`Transcribing LaTeX command definitions <FAQ-cvtlatex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,programming
```

