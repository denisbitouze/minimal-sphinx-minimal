# Comment définir des commandes ?

TeX est un *processeur de macros* : il compose le texte au fur et à mesure en développant chaque commande (ou macro) qu'il trouve. Les commandes TeX peuvent inclure des instructions à TeX lui-même, en plus de la simple génération de texte à laquelle on peut s'attendre.

Les commandes sont *une bonne chose* car elles permettent à l'utilisateur de manipuler des documents en fonction du contexte. Par exemple, la commande `\emph{⟨texte⟩}` de LaTeX produit le ⟨*texte*⟩ en italique. Mais si elle est utilisée dans un texte déjà en italique, elle produit un texte en romain (dans les deux cas, cela fait met en valeur le texte que la commande encadre). Mieux, vous pouvez changer le comportement de cette commande en changeant sa définition, par exemple faire en sorte qu'elle produise un texte en gras, un texte en couleur ou un texte souligné. Et ce comportement sera alors appliqué à tout votre document !

## Le format des commandes

Les noms de commande débutent traditionnellement par un `\` suivi d'une séquence de lettres, qui peuvent être majuscules ou minuscules. Ils peuvent également être « n'importe quel caractère unique », ce qui permet toutes sortes de bizarreries (intégrées à la plupart des ensembles de commandes TeX, tel ''\\ '' (`\` suivi d'une espace) signifiant « insérer une espace ici »).

## Les commandes de définition

La programmation de commandes peut être parfois compliquée mais, réduite à sa plus simple expression, elle est assez intuitive :

```latex
% !TEX noedit
\def\truc{élément d'intérêt majeur}
```

Cette instruction remplace chaque commande `\truc` par le texte « élément d'intérêt majeur ». La commande `\def` est, de fait, la syntaxe Plain TeX pour définir les commandes. Pour sa part, LaTeX propose une commande `\newcommand` qui permet de prendre un peu plus de précaution pour l'utilisateur mais qui fait fondamentalement la même chose :

```latex
% !TEX noedit
\newcommand{\truc}{élément d'intérêt majeur}
```

## Les commandes avec des arguments

Les commandes peuvent avoir des *arguments*. Représentés par la notation `#` suivi d'un chiffre dans les commandes de définitions, ces derniers vont s'insérer aux emplacements indiqués lors du développement de la commande :

```latex
\def\truc#1{élément vraiment #1}
Un \truc{exceptionnel}.
```

En LaTeX, cette définition serait écrite de la manière suivante :

```latex
\newcommand{\truc}[1]{élément vraiment #1}
Un \truc{génial}.
```

Vous aurez remarqué que les arguments ci-dessus étaient entre accolades « `{` ... `}` ». Il s'agit là de la manière normale de taper des arguments, bien que TeX soit extrêmement flexible, et {doc}`vous pouvez trouver toutes sortes d'autres façons de passer des arguments </2_programmation/macros/delimiteurs_speciaux>` (si nécessaire).

## Apprendre à faire des commandes

L'écriture de commandes peut devenir rapidement très compliquée. Si vous êtes un programmeur débutant en TeX ou LaTeX, il est conseillé d'observer des exemples de code de commande ou de lire des manuels pratiques (sur {doc}`TeX </1_generalites/documentation/documents/documents_sur_tex>` ou sur {doc}`LaTeX </1_generalites/documentation/documents/documents_sur_latex2e>`). Ceci dit, plus vous voudrez modifier le comportement de base de TeX, plus vous devrez analyser des sources avancées comme le {doc}`TeXbook </1_generalites/documentation/livres/documents_sur_tex>`.

______________________________________________________________________

*Source :* {faquk}`What are (TeX) macros <FAQ-whatmacros>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,concepts
```

