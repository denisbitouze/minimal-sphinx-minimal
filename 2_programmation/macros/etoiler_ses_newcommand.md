# Étoiler ses « `\newcommand` »

Une astuce qui ne changera rien à vos documents, mais rendra votre code plus robuste en vous permettant de localiser les erreurs plus facilement. Une des erreurs les plus courantes en tapant un document LaTeX est de mal équilibrer les accolades : en particulier d'oublier l'accolade fermante à la fin de l'argument d'une macro.

Dans un cas pareil, si rien n'est prévu, TeX lira votre fichier jusqu'à la fin avant de comprendre qu'il y a un problème et qu'il ne pourra rien faire. Pour éviter cela, Don Knuth (le créateur de TeX) a prévu de distinguer deux types de macros, courtes ou longues, selon que leurs arguments peuvent ou non contenir un changement de paragraphe. Pour une raison qui m'échappe, par défaut, `\newcommand` définit des macros longues.

En utilisant `\newcommand*` pour définir votre commande, vous en faites une macro courte, ce qui est largement préférable dans la plupart des cas : ainsi, les erreurs dues à un éventuel oubli d'accolade fermante seront circonscrites à un paragraphe. En cas de problème, TeX s'en rendra compte à la fin du paragraphe (et non du document), il pourra vous signaler facilement l'emplacement de l'erreur et composer le reste du document comme si de rien n'était.

Prenez donc l'habitude d'utiliser la plupart du temps les versions étoilés de `\newcommand` et `\renewcommand` (sauf bien sûr dans des cas particuliers), cela vous épargnera quelques désagréments. (Vous pouvez vous amuser à vérifier que je suis ce conseil sur tous les exemples présentés sur cette page.)

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#ncstared>

