# À quoi servent les commandes « `\newenvironment` » et « `\renewenvironment` » ?

- Ces commandes servent à (re)définir un environnement. Ces commandes sont semblables aux commandes `\newcommand` et `\renewcommand` de la question [sur « \\newcommand » et « \\renewcommand »](/2_programmation/macros/commande_newcommand), mais définissent des environnements, et auront donc un argument obligatoire supplémentaire, qui sera le code inséré à la fin de l'environnement.

La syntaxe est similaire à celle de `\newcommand` :

```latex
% !TEX noedit
\newenvironment{⟨nom_environnement⟩}[⟨nb_args⟩][⟨defaut⟩]{⟨def_debut⟩}{⟨def_fin⟩}
```

Ici, le nom ne commence pas par une contre-oblique. Les éventuels arguments de l'environnement devront être fournis au moment du `\begin{⟨nom_environnement⟩}`. La fin de l'environnement ne peut pas prendre d'argument.

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,définir des environnements,nouvel environnement,begin...end
```

