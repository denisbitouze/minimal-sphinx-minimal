# Comment dépasser la limite des 9 arguments pour une commande ?

Si vous y réfléchissez, vous vous rendrez compte que la syntaxe de définition de commande dans TeX (avec `\def`) et LaTeX (avec `\newcommand`) a une conséquence importante :

```latex
% !TEX noedit
\def\truc#1#2 ... #9{Contenu de la commande}
```

En effet, les commandes sont intrinsèquement limitées à 9 arguments (vous ne pouvez pas mettre un `#10` qui sera lu comme `#1` suivi de `0`).

## Avec des commandes de base

Si vous devez vraiment avoir plus de 9 arguments, la voie à suivre est la suivante :

```latex
% !TEX noedit
\def\truc#1#2 ... #9{%
  \def\ArgI{{#1}}%
  \def\ArgII{{#2}}%
  ...
  \def\ArgIX{{#9}}%
  \TrucBis
}
\def\TrucBis#1#2#3{%
  % Les arguments 1 à 9 sont maintenant dans
  % \ArgI-\ArgIX.
  % Les arguments 10 à 12 sont maintenant dans
  % #1 à #3.
  Texte de la commande%
}
```

Cette technique est facilement extensible par les spécialites de TeX mais elle n'est pas vraiment recommandée.

## En définissant des arguments clé-valeur

En {doc}`définissant des arguments clé-valeur </2_programmation/macros/arguments-cle-valeur>`, on peut s'affranchir de la limite des 9 arguments tout en facilitant l'utilisation de la commande par les utilisateurs, par exemple en permettant d'écrire ceci :

```latex
% !TEX noedit
\instancefleur{espece=Primula veris,
  famille=Primulaceae,
  localisation=Coldham's Common,
  typeemplacement=Paturage,
  date=24/04/1995,
  nombre=50,
  typesol=alkaline
}
```

Le mérite d'une telle débauche de code est simple : l'utilisateur n'a pas besoin de se souvenir de l'ordre des paramètres, par exemple que le douzième argument serait `typesol`. Sans compter la lisibilité du code pour un humain !

______________________________________________________________________

*Source :* {faquk}`How to break the 9-argument limit <FAQ-moren9>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,programming
```

