# Quelles sont toutes les commandes à une seule lettre ?

:::{warning}
Cette page est extraite de la documentation de [Tralics](https://www-sop.inria.fr/marelle/tralics/).

Certaines parties sont spécifiques à *Tralics* et ne concernent pas LaTeX.
:::

## `~`

(*Tralics* version 1 description : The `~` character is a normal character, but translates into a non-breaking space. Of course, the result is a tilde in verbatim mode, and a mathematical space in math mode.) Example :

```latex
% !TEX noedit
Test tilde:~\verb=~=$a~b$.
\href{\url{a~b\~c}}{some url}
```

The XML translation is

```xml
Test tilde:&nbsp;<hi rend='tt'>~</hi><formula type='inline'>
<math xmlns='http://www.w3.org/1998/Math/MathML'>
<mrow><mi>a</mi><mspace width='1em'/><mi>b</mi></mrow></math></formula>.
<xref url='some url'>a~b~c</xref>
```

Note how `~` and `\~` are handled by the `\url` command.

In the current version of *Tralics*, the tilde character is active and defined as `\def~{\nobreakspace}`. The only purpose of the change is to make the following example work (it is file `xii` by David Carlisle).

```latex
% !TEX noedit
\let~\catcode~`76~`A13~`F1~`j00~`P2jdefA71F~`7113jdefPALLF
PA''FwPA;;FPAZZFLaLPA//71F71iPAHHFLPAzzFenPASSFthP;A$$FevP
A@@FfPARR717273F737271P;ADDFRgniPAWW71FPATTFvePA**FstRsamP
AGGFRruoPAqq71.72.F717271PAYY7172F727171PA??Fi*LmPA&&71jfi
Fjfi71PAVVFjbigskipRPWGAUU71727374 75,76Fjpar71727375Djifx
:76jelse&U76jfiPLAKK7172F71l7271PAXX71FVLnOSeL71SLRyadR@oL
RrhC?yLRurtKFeLPFovPgaTLtReRomL;PABB71 72,73:Fjif.73.jelse
B73:jfiXF71PU71 72,73:PWs;AMM71F71diPAJJFRdriPAQQFRsreLPAI
I71Fo71dPA!!FRgiePBt'el@ lTLqdrYmu.Q.,Ke;vz vzLqpip.Q.,tz;
;Lql.IrsZ.eap,qn.i. i.eLlMaesLdRcna,;!;h htLqm.MRasZ.ilk,%
s$;z zLqs'.ansZ.Ymi,/sx ;LYegseZRyal,@i;@ TLRlogdLrDsW,@;G
LcYlaDLbJsW,SWXJW ree @rzchLhzsW,;WERcesInW qt.'oL.Rtrul;e
doTsW,Wk;Rri@stW aHAHHFndZPpqar.tridgeLinZpe.LtYer.W,:jbye
```

(section-1-1)=

## `\~`

The `\~` command allows you to put a tilde accent on a letter (see also the `\tilde` command). The possibilities are given here :

\\~A \\~a \\~{\\^A} \\~{\\^a} \\~{\\u A} \\~{\\u a} \\~E \\~e \\~{\\^E} \\~{\\^e} \\~I \\~i \\~N \\~n \\~O \\~o \\~{\\=O} \\~{\\=o} \\~{\\'O} \\~{\\'o} \\~{\\"O} \\~{\\"o} \\~{\\^O} \\~{\\^o} \\~{\\H O} \\~{\\H o} \\~U \\~u \\~{\\'U} \\~{\\'u} \\~{\\H U} \\~{\\H u} \\~V \\~v \\~Y \\~y

the result is :

Ã ã Ẫ ẫ Ẵ ẵ Ẽ ẽ Ễ ễ Ĩ ĩ Ñ ñ Õ õ ȭ Ȭ Ṍ ṍ Ṏ ṏ Ỗ ỗ Ő̃ ő̃ Ũ ũ Ṹ ṹ Ű̃ ű̃ Ṽ ṽ Ỹ ỹ

(section-2-1)=

## `\$`

The `\$` command is valid in math mode and text mode. It generates a dollar sign. See also the description of the `\qquad` command. Remember that the dollar sign by itself (using default category codes) starts or finishes a math formula.

(section-3-1)=

## `\%`

The `\%` command is valid in math mode and text mode. It generates a percent sign %. See also the description of the `\qquad` command. Remember that the percent sign by itself (using default category codes) starts a comment.

(section-4-1)=

## `\&`

The `\&` command is valid in math mode and text mode. It generates a ampersand sign &. See description of the `\qquad` command. Remember that the `&` character is valid only inside arrays as a cell delimiter (see description of arrays).

(section-5-1)=

## `\!`

The `\!` command is valid in math mode and text mode. It generates a negative space of -3/18em in math mode \<mspace width='-0.166667em'/> and nothing in text mode. See description of the `\qquad` command.

## `\,` (backslash comma)

The `\,` command is valid in math mode and text mode. It generates a space of 3/18em in math mode, \<mspace width='0.166667em'/>, and a non-breaking space otherwise (this is  , as defined in isonum.ent).

See description of the `\qquad` command and `\AA` command.

(section-6-1)=

## `\>`

The `\>` command is valid in math mode. It generates a space of 4/18em. See description of the `\qquad` command.

(section-7-1)=

## `\;`

The `\;` command is valid in math mode. It generates a space of 5/18em. See description of the `\qquad` command.

(section-8-1)=

## `\{`

The `\{` command is valid in math mode and text mode. It generates an open brace, for instance \<mo>{\</mo> in math mode. See description of the `\qquad` command, and `\AA` command. It can be used as a math delimiter, see description of the `\vert` command.

Note that a single brace (without backslash), assuming default catcodes, opens a group.

(section-9-1)=

## `\}`

The `\}` command is valid in math mode and text mode. It generates an close brace, for instance \<mo>}\</mo> in math mode. See description of the `\qquad` command, and `\AA` command. It can be used as a math delimiter, see description of the `\vert` command.

Note that a single brace (without backslash) (assuming default catcodes) closes a group.

## `\+` (backslash plus)

This command `\+` is undefined in *Tralics*; it is part of the unimplemented `\settabs` mechanism.

## `\_` (backslash underscore)

The `\_` command is valid in math mode and text mode. It generates an underscore, in math mode it is \<mo>\_\</mo> See description of the \\qquad command and `\AA` command.

With defaults catcodes, a simple underscore character is valid only in math mode, and starts a subscript.

## `\-` (backslash minus)

The LaTeX kernel contains `\def\-{\discretionary{-}{}{}}`. Since hyphenation is not implemented in *Tralics*, `\-` produces no result.

The command is also redefined by the tabbing environment, which is not yet implemented.

Note that hyphens are ligatures :

```latex
% !TEX noedit
a - b -- c --- d
```

```xml
<p>a - b &ndash; c &mdash; d</p>
```

preview dashes

## `|` (vertical bar)

The `|` command it is equivalent to `\vert` as a math delimiter. See description of the `\vert` command.

## `\|` (backslash vertical bar)

The `\|` command is valid only in math mode. It is equivalent to `\Vert`. It produces the character U+2225, `\u2225`. See description of the `\vert` command.

(section-10-1)=

## `\#`

The character `#` has category 6, is described here, and cannot be use to produce a sharp sign, you must used `\#` instead. It is valid in text and in math mode; you can also use `\sharp`, that produces the musical sign character U+266F, `\u266f`. See also description of the `\AA` command.

## `\\` (backslash backslash)

The `\\` command has three meanings, depending on whether it is in text, in a text table, or in math table. It can be followed by a star (which is ignored) and an optional argument in brackets, which is a dimension. Inside a table, the `\\` indicates the end of the current row, and the optional argument specifies additional space between rows. This argument is currently ignored in math mode. Note that `\\` finished the current cell, the current row, and starts a new row and a new cell. If this is the last cell in the table, and if it is empty, then the row is removed. Said otherwise, a `\\` is ignored at the end of a table; it is however needed before a final `\hline` if you want an horizontal rule at the end of the table.

Outside a table, a `\\` specifies the end of a paragraph, and the start of a new one, which is not indented. The optional argument indicates vertical space to be added (see also the `\vskip` command). In a title, the command with its arguments is replaced by `\@headercr`, a command that produces a space by default. We give here an example of `\\` outside a table.

```latex
% !TEX noedit
text A \\* text B \\[3mm] text C
\expandafter\def\csname @headercr\endcsname{; }
\section{A\\[2mm]B}
\begin{center}
line one\\
this is the second line
\end{center}
```

This is the XML translation. The \<p> element has a noindent attribute only if it has no rend attribute.

\<p>text A\</p> \<p noindent='true'>text B\</p> \<p noindent='true' spacebefore='8.53581'>text C\</p> \<div0 id='uid1'>\<head>A; B\</head> \<p rend='center'>line one\</p> \<p rend='center'>this is the second line\</p> \</div0>

This is an example of math table.

```latex
% !TEX noedit
\begin{equation}
\begin{array}{lcl}
\dot{x} & = & Ax+g(x,u)\\[2mm]
 y & = & Cx \\
 \multicolumn{3}{l}{x\in R^n}
\end{array}
\end{equation}
```

This is the XML translation.

\<formula type='display'> \<math xmlns='<http://www.w3.org/1998/Math/MathML'>>

```
<mtable>
 <mtr>
  <mtd columnalign='left'><mover accent='true'><mi>x</mi> <mo>&dot;</mo></mover></mtd>
  <mtd><mo>=</mo></mtd>
  <mtd columnalign='left'>
   <mrow><mi>A</mi><mi>x</mi><mo>+</mo><mi>g</mi><mo>(</mo><mi>x</mi>
     <mo>,</mo><mi>u</mi><mo>)</mo></mrow>
  </mtd>
 </mtr>
 <mtr>
  <mtd columnalign='left'><mi>y</mi></mtd>
  <mtd><mo>=</mo></mtd>
  <mtd columnalign='left'><mrow><mi>C</mi><mi>x</mi></mrow></mtd>
 </mtr>
 <mtr>
  <mtd columnalign='left' columnspan='3'><mrow><mi>x</mi><mo>&Element;</mo>
      <msup><mi>R</mi> <mi>n</mi> </msup></mrow>
  </mtd>
 </mtr>
</mtable>
```

\</math> \</formula>

This is a preview. Note that the `\multicolumn` is ignored in the rendering. This has been corrected. example of `\\` in a math table

Other example.

```latex
% !TEX noedit
\begin{tabular}{|ll|rr|cc|}
\hline a&b&c&d&e&f\\
aaa&bbb&ccc&ddd&eee&fff\\
\hline
A&\multicolumn{3}{l|}{BCD}&E&F\\
\multicolumn{2}{|l}{ab}&c&d&e&f\\
\cline{1-3}\cline{6-6}
aaa&bbb&ccc&ddd&eee&fff\\\hline
\end{tabular}
```

This is the XML translation.

```xml
<table rend='inline'>
 <row top-border='true'>
  <cell halign='left' left-border='true'>a</cell>
  <cell halign='left' right-border='true'>b</cell>
  <cell halign='right'>c</cell>
  <cell halign='right' right-border='true'>d</cell>
  <cell halign='center'>e</cell>
  <cell halign='center' right-border='true'>f</cell>
 </row>
 <row>
  <cell halign='left' left-border='true'>aaa</cell>
  <cell halign='left' right-border='true'>bbb</cell>
  <cell halign='right'>ccc</cell>
  <cell halign='right' right-border='true'>ddd</cell>
  <cell halign='center'>eee</cell>
  <cell halign='center' right-border='true'>fff</cell>
 </row>
 <row top-border='true'>
  <cell halign='left' left-border='true'>A</cell>
  <cell cols='3' halign='left' right-border='true'>BCD</cell>
  <cell halign='center'>E</cell>
  <cell halign='center' right-border='true'>F</cell>
 </row>
 <row>
  <cell bottom-border='true' cols='2' halign='left' left-border='true'>ab</cell>
  <cell bottom-border='true' halign='right'>c</cell>
  <cell halign='right' right-border='true'>d</cell>
  <cell halign='center'>e</cell>
  <cell bottom-border='true' halign='center' right-border='true'>f</cell>
 </row>
 <row bottom-border='true'>
  <cell halign='left' left-border='true'>aaa</cell>
  <cell halign='left' right-border='true'>bbb</cell>
  <cell halign='right'>ccc</cell>
  <cell halign='right' right-border='true'>ddd</cell>
  <cell halign='center'>eee</cell>
  <cell halign='center' right-border='true'>fff</cell>
 </row>
</table>
```

The rendering of the tabular is not good : first, the width of the table is the width of the page (changed to 15cm in this example), and the width of each column is defined by the number of characters in it (in this example, you do not see a difference, but replacing `a` by `$a$` would be catastrophic).

You should look at the page on arrays. It contains an example similar to this one. You can see that we changed the algorithm : a `\hline` implies a bottom-border on the previous row.

example of `\\` in a normal table

(section-11-1)=

## `[, \[`

The `[` command can be used as a math delimiter. See description of the `\vert` command.

The expansion of `\[` is `$$`. It means : \`begin display math' but *Tralics* does no check.

(section-12-1)=

## `], \]`

The `]` command can be used as a math delimiter. See description of the `\vert` command.

The expansion of `\]` is `$$`. It means : \`end display math' but *Tralics* does no check. Hence `$a\]b$` and `\]x\[` are valid expressns.

(section-13-1)=

## `(, \(`

The ( command can be used as a math delimiter. See description of the \\vert command.

The expansion of `\(` is `$`. It means : \`start inline math' but *Tralics* does no check. This command is also used as an opening deliter by the ifthen package.

(section-14-1)=

## `), \)`

The `)` command can be used as a math delimiter. See description of the `\vert` command.

The expansion of `\)` is `$`. It means : \`end inline math' but *Tralics* does no check. In particular `$$ a\(\)` is valid; note also tat `\(\)` is an empty math formula. This command is also used as a closing delimiter by the {ctanpkg}`ifthen` package.

## `\<` (less than sign)

The `<` command can be used as a math delimiter. See description of the `\vert` command. Otherwise, it translates like a normal character, but is always printed as `<` in the XML output. There is one exception : the `rawxml` environment prints the convent verbatim, and the `\xmllatex` command print

## `\> (greater than sign)`
The `>` command can be used as a math delimiter. See description of the `\vert` command.

## `' (single quote, apostrophe, straight quote)`

This character `'` behaves normally in text mode; it has a special meaning when *Tralics* reads a number (see scanint); it has a special meaning in math mode. Plain TeX defines an active apostrophe character as follows

```latex
% !TEX noedit
{\catcode`\'=\active \gdef'{^\bgroup\prim@s}}
\def\prim@s{\prime\futurelet\next\pr@m@s}
\def\pr@m@s{\ifx'\next\let\nxt\pr@@@s \else\ifx^\next\let\nxt\pr@@@t
  \else\let\nxt\egroup\fi\fi \nxt}
\def\pr@@@s#1{\prim@s} \def\pr@@@t#1#2{#2\egroup}
```

The definition of LaTeX is similar, with \\expandafter instead of `\nxt`. In *Tralics*, these lines of TeX code are replaced by some lines of C++. The effect can be seen on the following example.

```latex
% !TEX noedit
{\tracingall $x' x'' x''' x'''' u_2' v'^3_4$}
```

The transcript file will hold

```
{math shift}
+stack : level + 3
...
Math : $x^{\prime } x^{\prime \prime } x^{\prime \prime \prime }
x^{\prime \prime \prime \prime } u_2^{\prime } v^{\prime 3}_4$
```

We have removed a bunch of lines of the form+stack : level'', because each opening and closing brace changes the current level (by the way, it is the dollar sign that increases the level first). The XML translation is the following.

```xml
<formula type='inline'>
 <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mrow>
   <msup><mi>x</mi> <mo>&prime;</mo></msup>
   <msup><mi>x</mi> <mrow><mo>&prime;</mo><mo>&prime;</mo></mrow> </msup>
   <msup><mi>x</mi><mrow><mo>&prime;</mo><mo>&prime;</mo><mo>&prime;</mo></mrow></msup>
   <msup><mi>x</mi> <mrow><mo>&prime;</mo><mo>&prime;</mo><mo>&prime;</mo>
     <mo>&prime;</mo></mrow></msup>
   <msubsup><mi>u</mi> <mn>2</mn> <mo>&prime;</mo></msubsup>
   <msubsup><mi>v</mi> <mn>4</mn><mrow><mo>&prime;</mo><mn>3</mn></mrow> </msubsup>
  </mrow>
 </math>
</formula>
```

## `\'` (backslash left quote)

The single quote character behaves normally, but has a special meaning when *Tralics* reads a number (see scanint). The `\'` command puts an acute accent over some letters. Do not confuse with `\acute`, which is a math-only command. Example

```latex
% !TEX ndit
\'A \'a \'{\AA} \'{\aa} \'{\AE} \'{\ae} \'{\^A} \'{\^a} \'{\u A} \'{\u a}
\'C \'c \'{\c C} \'{\c c} \'E \'e  \'{\=E}
\'{\=e} \'{\^E} \'{\^e} \'G \'g \'I \'i \\\'{\"I} \'{\"i} \'K \'k \'L \'l \'M
\'m \'N \'n \'O \'o \'{\O} \'{\o} \'{\~O} \'{\~o} \'{\=O} \'{\=o} \'{\^O}
\'{\^o} \'{\H O} \'{\H o}\\ \'P \'p \'R \'r \' S \'s
\'{\.S} \'{\.s} \'U \'u \'{\"U} \'{\"u} \'{\~U} \'{\~u} \'{\H U} \'{\H u}
\'W \'w \'Y \'y \'Z \'z
`

gives

```xml
Á á &#506; &#507; &#508; &#509; &#7844; &#7845; &#7854; &#7855;
&#262; &#263; &#7688; &#7689; É é &#7702;
&#7703; &#7870; &#7871; &#500; &#501; Í í
#7726; &#7727; &#7728; &#7729; &#313; &#314; &#7742;
&#7743; &#323; &#324; Ó ó &#510; &#511; &#7756; &#7757; &#7762; &#7763; &#7888;
&#7889; &#7898; &#7899;
&#7764; &#7765; &#340; &#341; &#346; &#347;
&#7780; &#7781; Ú ú &#471; &#472; &#7800; &#7801; &#7912; &#7913;
&#7810; &#7811; Ý ý &#377; &#378;
```

# `\" (backslash double quote)`

The double quote character behaves normally, but has a special meaning when *Tralics* reads a number (see scanint). The `\"` command puts a umlaut or diaeresis accent over some letters. Example :

```latex
% !TEX noedit
\"A \"a \"{\=A} \"{\=a} \"E \"e \"H \"h \"I \"i \"{\'I} \"{\'i} \"O \"o
\"{\=O} \"{\=o} \"{\~O} \"{\~o} \"t\\ \"U \"u \"{\=U} \"{\=u} \"{\`U} \"{\`u}
\"{\'U} \"{\'u} \"{\v U} \"{\v u} \"W \"w \"X \"x \"Y \"y
```

gives

```xml
Ä ä &#478; &#479; Ë ë &#7718; &#7719; Ï ï &#7726; &#7727; Ö ö
&#554; &#555; &#7758; &#7759; &#7831;
Ü ü &#7802; &#7803; &#475; &#476;
&#471; &#472; &#473; &#474; &#7812; &#7813; &#7820; &#7821; &#376; ÿ``

## `\=` (backslash equals)

The `\=` command generates a macro accent, similar to the `\bar` (that works in math mode only). The translation of

```latex
% !TEX noedit
\=A \=a \={\"A} \={\"a} \={\.A} \={\.a} \=\AE \=\ae \=E \= e \={\'E}={\'e}
\={\`E} \={\`e} \=G \=g \=H \=h\\
\=I \=i \={\d L} \={\d l} \= O \=o \={\k O} \={\k o} \={\"O} \={\"o} \={\~O}
\={\~o} \={\.O} \={\.o} \={\`O} \={\`o} \={\'O} \={\'o}\\ \={\d R} \={\d r}
 \=T \=t \=U \=u \={\"U} \={\"u} \=Y \=y
```

is
```xml
&#256; &#257; &#478; &#479; &#480; &#481; &#482;&#483;&#274; &#275; &#7702; &#7703;
&#7700; &#7701; &#7712; &#7713; &#294; &#295;
&#298; &#299; &#7736; &#7737; &#332; &#333; &#492; &#493; &#554; &#555; &#556;
&#557; &#560; &#561; &#7760; &#7761; &#7762; &#7763; &#7772; &#7773;
&#358; &#359; &#362; &#363; &#7802; &#7803; &#562; &#563;
```

# `\. (backslash dot)`

The `\.` command generates dot accent, similar to the `\dot` (that works in math mode only). The translation of

```latex
% !TEX noedit
\.A \.a \.{\=A} \.{\=a} \.B \.b \.C \.c \.D \.d \.E \.e \.F \.f \.G \.g \.H
\.h \.I \.L \.l \.M \.m \.N \.n \.O \.o \.{\=O} \.{\=o} \.P \.p \.R \.r \.S
\.s \.{\d S} \.{\d s} \.{\v S} \.{\v s} \.{\'S} \.{\'s} \.T \.t \.W \.w \.X
\.x \.Y \.y \.Z \.z
```

is

```xml
&#550; &#551; &#480; &#481; &#7682; &#7683; &#266; &#267; &#7690; &#7691; &#278; &#279;
&#7710; &#7711; &#288; &#289; &#7714; &#7715; &#304; &#319; &#320; &#7744; &#7745; &#7748;
&#7749; &#558; &#559; &#560; &#561; &#7766; &#7767; &#7768; &#7769; &#7776; &#7777;
&#7784; &#7785; &#7782; &#7783; &#7780; &#7781; &#7786; &#7787; &#7814; &#7815; &#7818;
&#7819; &#7822; &#7823; &#379; &#380;
```

## `\^` (backslash hat)

The `\^` command generates a circonflex accent, similar to the `\hat` command (that works in math mode only). The translation of

```latex
% !TEX noedit
\^A \^a \^{\'A} \^{\'a} \^{\`A} \^{\`a} \^{\h A} \^{\h a} \^{\~A} \^{\~a}
\^{\d A} \^{\d a} \^C \^c \^E \^e \^{\'E} \^{\'e} \^{\`E} \^{\`e} \^{\h E}
\^{\h e} \^{\~E} \^{\~e} \^{\d E} \^{\d e} \^G \^g \^H \^h \^I \^i \^J \^j
\^O \^o \^{\'O} \^{\'o}  \^{\`O} \^{\`o}  \^{\h O} \^{\h o}  \^{\~O} \^{\~o}
\^{\d O} \^{\d o}  \^S \^ s \^U \^u \^W \^w \^Y \^y \^Z \^z
```

is

```xml
Â â &#7844; &#7845; &#7846; &#7847; &#7848; &#7849; &#7850; &#7851;
&#7852; &#7853; &#264; &#265;
Ê ê &#7870; &#7871; &#7872; &#7873; &#7874;
&#7875; &#7876; &#7877; &#7878; &#7879; &#284; &#285; &#292; &#293; Î î &#308; &#309;
Ô ô &#7888; &#7889; &#7890; &#7891; &#7892; &#7893; &#7894; &#7895;
&#7896; &#7897; &#348; &#349; Û û &#372; &#373; &#374; &#375; &#7824; &#7825;
```

## ```` \` ```` (backslash backquote)

The backquote character behaves normally, but has a special meaning when
*Tralics* reads a number (see scanint). The ```` \` ```` command generates
a grave accent, similar to the \\grave command (that works in math mode
only). The translation of

```tex
% !TEX noedit
\`A \`a \`{\^A} \`{\^a} \`{\u A} \`{\u a} \`E \`e \`{\=E} \`{\=e} \`{\^E}
\`{\^e}  \`I \`i \`N \`n \\\`O \`o \`{\=O} \`{\=o} \`{\^O} \`{\^o}
\`{\H O} \`{\H o} \`U \`u \`{\"U} \`{\"u} \`{\H U} \`{\H u} \`W \`w \`Y\`y
```

is
``xml
À à &#7846; &#7847; &#7856; &#7857; È è &#7700; &#7701; &#7872;
&#7873; Ì ì &#504; &#505;
Ò ò &#7760; &#7761; &#7890; &#7891;
&#7900; &#7901; Ù ù &#475; &#476; &#7914; &#7915; &#7808; &#7809; &#7922;&#7923;
```

## `\ ` (backslash space)
The `\ ` command (backslash space) adds a space character to the XML tree. The command name is formed of a backslash followed by a space, a tabulation, a line-feed, or a carriage return. Since these characters are of catcode 10 (space), spaces after them is ignored. At the start of chapter 25 of the TeX​book, Knuth explains that the result of this command is as if a space had been given in a context where the space factor is 1000 (*Tralics* ignores `\spacefactor` and other factors that modify the space factor). In math mode, the translation is `<mspace width='4pt'/>`.

Note that `\space` produces a single space.

## `\/ (backslash slash)`

he `\/` command is assumed to insert a kern corresponding to the italic correction, if the last item on the horizontal list is a character or a ligature, or a kern of width zero in math mode. However, *Tralics* ignores such subtleties, so that this command does nothing.

(section-15-1)=

## `\*`

This command is valid in math mode only, the translation is a `\<mo>⁢&InvisibleTimes;</mo>`.

## `\@` (backslash atsign)

The LaTeX kernel contains `\def\@{\spacefactor\@m}`. However, *Tralics* ignores currently space factors, so that this command does nothing. If the at-sign character is of category \`letter', then `\@ne` is a single command; but if it is of category \`other' (normal case), it is `\@` followed by some characters, and this often gives an error in LaTeX, since the `\spacefactor` command cannot be used everywhere.

The description of `\@makeactive` is found near that of `\makeatletter`; there are som exceptions to this rule : `\@ne` is described as if it were `\one`.

______________________________________________________________________

*Source :* <https://www-sop.inria.fr/marelle/tralics/doc-symbols.html>

```{eval-rst}
.. meta::
   :keywords: commandes de LaTeX,liste des commandes,single-letter commands,commandes à un seul caractère
```

