# Que signifient certaines commandes sous forme abrégée ?

Il y a bien longtemps, lors de l'apparition de TeX, les ordinateurs avaient une mémoire extrêmement limitée et ils étaient atrocement lents (selon nos standards actuels). Lors de l'arrivée de LaTeX, la situation ne s'était guère améliorée et, même lors de l'arrivée de LaTeX, il existait toujours un réel besoin d'économiser l'espace mémoire et, de manière moindre, le temps machine.

Depuis le début, Donald E. Knuth utilisait des commandes abrégées pour accélérer les choses. LaTeX, avec les années, a largement complété la liste de Knuth. Un élément intéressant de ces « abréviations » est que, sur papier, elles paraissent plus longues que les choses qu'elles représentent ; cependant pour TeX et LaTeX, elles le sont moins...

La table qui suit liste les « abréviations » les plus courantes. Elle n'est pas complète : si la table ne vous aide pas, essayez d'analyser le {ctanpkg}`code commenté <source2e>` de LaTeX. La table donne pour chaque abréviation son *nom* et sa *valeur*, ce qui suffit pour la plupart des utilisateurs. La table donne aussi le *type* qui réprésente un concept plus épineux : au cas où, la seule réelle confusion est que les abréviations typées « définitions » sont définies avec une commande `xxxxdef`.

| Nom        | Type       | Valeur                 |
| ---------- | ---------- | ---------------------- |
| `\m@ne`    | compteur   | -1                     |
| `\p@`      | dimension  | 1pt                    |
| `\z@`      | dimension  | 0pt                    |
| `\z@skip`  | espace     | 0pt plus 0pt minus 0pt |
| `\@ne`     | définition | 1                      |
| `\tw@`     | définition | 2                      |
| `\thr@@`   | définition | 3                      |
| `\sixt@@n` | définition | 16                     |
| `\@cclv`   | définition | 255                    |
| `\@cclvi`  | définition | 256                    |
| `\@m`      | définition | 1000                   |
| `\@M`      | définition | 10000                  |
| `\@MM`     | définition | 20000                  |
| `\@vpt`    | commande   | 5                      |
| `\@vipt`   | commande   | 6                      |
| `\@viipt`  | commande   | 7                      |
| `\@viiipt` | commande   | 8                      |
| `\@ixpt`   | commande   | 9                      |
| `\@xpt`    | commande   | 10                     |
| `\@xipt`   | commande   | 10.95                  |
| `\@xiipt`  | commande   | 12                     |
| `\@xivpt`  | commande   | 14.4                   |
| `\@xviipt` | commande   | 17.28                  |
| `\@xxpt`   | commande   | 20.74                  |
| `\@xxvpt`  | commande   | 24.88                  |
| `\@plus`   | commande   | `plus`                 |
| `\@minus`  | commande   | `minus`                |

______________________________________________________________________

*Source :* {faquk}`LaTeX internal "abbreviations", etc. <FAQ-ltxabbrv>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programmation,abréviation
```

