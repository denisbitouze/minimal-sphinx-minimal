# Comment reporter l'exécution d'une commande à la fin d'une page ?

On a parfois besoin d'utiliser une page entière pour insérer du contenu, sans pour autant vouloir utiliser le mécanisme des flottants. Si vous avez l'habitude d'un traitement de texte classique, vous insérez manuellement un saut de page... mais vous risquez des ennuis quand votre document évoluera, par exemple d'avoir une page quasiment vide, parce que votre saut de page manuel aura été repoussé de quelques lignes.

- En LaTeX, le package {ctanpkg}`afterpage` fournit une solution simple : sa commande `\afterpage` s'occupe d'insérer son argument *au début de la page suivante*.

Ainsi, vous pouvez écrire là où vous le souhaitez :

```latex
% !TEX noedit
\afterpage{Je suis au top de la page!}
```

Ce texte n'apparaîtra pas à l'endroit où vous l'avez saisi, mais en haut de la page suivante.

## Utilisation avec les flottants

Si votre document contient beaucoup de flottants (figures et tableaux) de grande taille, le mécanisme d'insertion des flottants peut se retrouver saturé, et vous risquez de voir tous les flottants regroupés en fin de document, ce qui n'était sans doute pas ce que vous vouliez.

LaTeX insére les flottants quand il rencontre une commande `\clearpage`, ce qui fournit une bonne solution au problème... sauf que vous n'avez pas forcément envie d'insérer des sauts de pages supplémentaires dans votre document. Avec {ctanpkg}`afterpage`, vous pouvez écrire :

```latex
% !TEX noedit
\afterpage{\clearpage}
```

Ceci ajoute un `\clearpage` juste après le prochain saut de page automatique. Cela n'a donc pas d'effet visible sur les sauts de pages, mais force LaTeX à insérer les flottants en attente.

______________________________________________________________________

*Source :*

- [Forcing a figure to appear on the next page](https://tex.stackexchange.com/questions/52515/forcing-a-figure-to-appear-on-the-next-page),
- {texdoc}`Documentation de l'extension « afterpage » <afterpage>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,nouvelle page
```

