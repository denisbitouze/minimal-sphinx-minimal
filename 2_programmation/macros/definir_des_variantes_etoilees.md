# Définir des variantes étoilées des commandes et environnements

Vous l'avez sans doute remarqué, beaucoup d'environnements ou commandes standard de LaTeX existent sous une variante étoilée. Vous pouvez avoir envie de reprendre cette idée quand vous définissez vos commandes et arguments, et j'approuve cette envie. Voyons comment la mettre en oeuvre.

Si vous avez lu l'astuce sur le nommage, vous aurez compris que c'est immédiat pour les environnements car l'étoile est un caractère autorisé dans les noms d'environnements. Il vous suffit donc de faire `\newenvironment{myenv}` et `\newenvironment{myenv*}` avec les définitions souhaitées.

Pour les commandes, c'est plus compliqué car l'étoile ne peut pas faire partie du nom de la commande. Il y aura donc une commande, qui devra être capable de regarder si elle est ou non suivie d'une étoile et d'adapter son comportement en conséquence. Pour des raisons techniques, cette commande ne pourra pas accepter d'argument, mais pourra faire appel à des commandes qui en acceptent (voir le début de « gérer ses arguments »). Par ailleurs, on utilise une commande interne du noyau LaTeX et vous pouvez vous reportez à `\makeatletter` pour comprendre pourquoi cela implique son usage.

```latex
% !TEX noedit
\newcommand*\mycommandstared[1]{ciel #1 étoilé}
\newcommand*\mycommandunstared[1]{ciel #1 non étoilé}
\makeatletter
\newcommand\mycommand{\@ifstar{\mycommandstared}{\mycommandunstared}}
\makeatother
```

Dans cet exemple (stupide), vous pouvez alors utiliser `\mycommand` comme une commande avec un argument obligatoire et admettant une variante étoilée. Ainsi, `\mycommand{bleu}` composera « ciel bleu non étoilé » tandis que `\mycommand*{nocturne}` composera « ciel nocturne étoilé ».

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#stared>

