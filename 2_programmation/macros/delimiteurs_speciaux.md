# Comment utiliser d'autres délimiteurs que les crochets et les accolades dans une commande ?

Vous avez sans doute remarqué que certaines commandes ont leurs arguments délimités par d'autres caractères que des crochets ou des accolades. Par exemple, avec {ctanpkg}`TikZ <tikz>`, vous pouvez tracer une ligne avec la commande :

```latex
\draw (0,0) to[line to] (1,0);
```

qui utilise des parenthèses, des virgules, un point-virgule et le mot-clef `to`.

Imaginons que nous voulions définir une commande qui nous permet de mettre en forme des définitions. Si l'argument optionnel `note` est spécifié, la définition est placée dans une note de bas de page, sinon elle est mise dans le corps du texte entre crochets. Le résultat pourrait ressembler à ceci :

```latex
Les commandes de \TeX{} sont en réalité des
\emph{macros} [\textsc{macro}~ :
  chaîne de caractères que le processeur doit remplacer
  par une autre chaîne de caractère ou par une autre macro,
  éventuellement en substituant des paramètres],
ce qui ouvre des perspectives fort intéressantes.
```

On pourrait, bien sûr créer une macro avec `\newcommand` qui permettre d'écrire :

```latex
% !TEX noedit
Les commandes de \TeX{} sont en réalité des
\definir{macros}{macro}
  {chaîne de caractères que le processeur doit remplacer
  par une autre chaîne de caractère ou par une autre macro,
  éventuellement en substituant des paramètres},
ce qui ouvre des perspectives fort intéressantes.
```

Toutefois, le codage suivant serait plus expressif et faciliterait la lecture :

```latex
% !TEX noedit
Les commandes de \TeX{} sont en réalité des
\definir{macros -> macro :
  chaîne de caractères que le processeur doit remplacer
  par une autre chaîne de caractère ou par une autre macro,
  éventuellement en substituant des paramètres},
ce qui ouvre des perspectives fort intéressantes.
```

Voyons cela.

## Avec \\def

Par défaut, la primitive de TeX `\def` prend comme argument la première unité qui suit (caractère, groupe ou commande).

```latex
% !TEX noedit
\def\madef#1#2{}

\madef Le toutou % #1 = L, #2 = e

\madef {Le} {toutou} % #1 = Le, #2 = toutou
```

Cependant, on peut aussi introduire des délimiteurs dans la liste des arguments. Il peut s'agir de caractères ou de commandes (lesquelles ne seront pas interprétées). Une commande qui s'approcherait de ce que l'on souhaite pourrait donc être :

```latex
% !TEX noedit
\def\definition[#1] #2 -> #3 : #4\findedefinition{%
  \def\litteralnote{note}%
  \def\optionarg{#1}%
  \ifx\optionarg\litteralnote%
  \textit{#2}\footnote{\textsc{#3} : #4.}%
  \else%
  \textit{#2} [\textsc{#3} : #4]%
  \fi%
}
```

Comme la commande `\findedefinition` n'est pas interprétée par TeX, mais simplement utilisée comme délimiteur, elle n'a pas besoin d'être définie. Notez que les espaces sont aussi prises en compte comme délimiteurs.

On peut donc écrire :

```latex
% !TEX noedit
Les commandes de \TeX{} sont en réalité des
\definition[] macros -> macro :
  chaîne de caractères que le processeur doit remplacer
  par une autre chaîne de caractère ou par une autre macro,
  éventuellement en substituant des paramètres\findedefinition,
ce qui ouvre des perspectives fort intéressantes.
```

ou

```latex
% !TEX noedit
Les commandes de \TeX{} sont en réalité des
\definition[note] macros -> macro :
  chaîne de caractères que le processeur doit remplacer
  par une autre chaîne de caractère ou par une autre macro,
  éventuellement en substituant des paramètres\findedefinition,
ce qui ouvre des perspectives fort intéressantes.
```

On peut ensuite définir la commande `\definir` présentée ci-dessus comme suit :

```latex
% !TEX noedit
\newcommand*{\definir}[2][]{\definition[#1] #2\findedefinition}
```

ou, {doc}`si on ne veut pas que la commande TeX intermédiaire soit directement accessible par l'utilisateur </2_programmation/macros/makeatletter_et_makeatother>`, on peut tout réécrire ainsi :

```latex
% !TEX noedit
\makeatletter
\newcommand*{\definir}[2][]{\definiti@n[#1] #2\findedefiniti@n}
\def\definiti@n[#1] #2 -> #3 : #4\findedefiniti@n{%
  \def\litteralnote{note}\def\optionarg{#1}%
  \ifx\optionarg\litteralnote%
  \textit{#2}\footnote{\textsc{#3} : #4.}%
  \else%
  \textit{#2} [\textsc{#3} : #4]%
  \fi%
}
\makeatother
```

______________________________________________________________________

*Sources :*

- Victor Eijkhout, *TeX by topic*, chap. 11

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programmation,parenthèses,arguments personnalisés
```

