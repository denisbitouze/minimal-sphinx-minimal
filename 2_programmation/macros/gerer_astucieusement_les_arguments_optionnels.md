# Gérer astucieusement les arguments optionnels

Dans cette astuce, je suppose que vous connaissez déjà la syntaxe de `\newcommand`. En particulier, vous savez sans doute qu'on ne peut définir (au plus) qu'un argument optionnel et que celui-ci est nécessairement le premier. L'objet de la présente astuce est de contourner, dans certains cas, cette limitation.

Commençons par une remarque assez fondamentale sur la façon dont TeX gère les macros. D'abord, il faut bien comprendre qu'une macro n'est pas une fonction : il ne s'agit pas d'exécuter à part le code de la macro et de renvoyer un résultat, mais juste de remplacer le nom de la macro par son texte de définition. En particulier, TeX n'opère aucun contrôle sur le texte de définition d'un macro, qui peut contenir des macros non définies ou n'ayant pas le bon nombre d'arguments.

On peut alors méditer sur le cas d'une commande définie comme `\newcommand\latin{\textit}`. Techniquement, elle semble être une macro sans argument. En pratique, l'utilisateur écrira `\latin{id est}` comme si la commande admettait un argument : celui-ci sera en fait passé à `\textit`. Vous êtes maintenant prêts à comprendre l'exemple suivant.

```latex
% !TEX noedit
\newcommand*\xvec[1][0]{x_{#1},\ldots,\xvecint}
\newcommand*\xvecint[1][n]{x_{#1}}

\xvec       % donne x_0,\ldots,x_n
\xvec[1]    % donne x_1,\ldots,x_n
\xvec[1][m] % donne x_1,\ldots,x_m
\xvec[m]    % donne x_m,\ldots,x_n (attention)
```

Deux remarques :

1. Si le dernier argument est optionnel et que vous ne l'utilisez pas, votre commande va manger les espaces qui la suivent, soyez prudent hors du mode mathématique. 2. Pour des traitements plus compliqués, vous devrez utiliser des définitions emboîtées pour faire circuler les arguments entre vos différentes macros internes.

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#argh>

