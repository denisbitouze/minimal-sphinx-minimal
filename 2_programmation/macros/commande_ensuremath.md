# À quoi sert la commande « `\ensuremath` » ?

La commande `\ensuremath` permet de s'assurer que son argument sera composé en {doc}`mode mathématique </4_domaines_specialises/mathematiques/passer_en_mode_mathematique>`, quel que soit le mode courant. Cela permet de définir des commandes qui pourront être utilisées aussi bien en mode mathématique que dans du texte. Par exemple :

```latex
% !TEX noedit
\def\NN{\ensuremath{\mathbb{N}}}
```

Ainsi, cette commande est utilisable aussi bien directement en mode texte qu'en mode mathématique, ce qui simplifie son utilisation.

TeX fournit aussi la valeur booléenne `\ifmmode`, qui est vraie en mode mathématique et fausse ailleurs. On pourra donc définir la commande suivante :

```latex
% !TEX noedit
\def\ssi{\ifmmode \Leftrightarrow \else
    si, et seulement si, \fi}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,mode mathématique
```

