# Comment éviter l'espace inséré après uen environnement personnalisé ?

Vous avez écrit votre propre environnement `env`. Et voilà qu'il fonctionne bien à ceci près qu'une espace apparaît au début de la première ligne de texte composée après `\end{env}`. Cela ne se produit pourtant pas avec des environnements similaires fournis par LaTeX.

Vous pourriez imposer une restriction : que vos utilisateurs mettent toujours un signe ''% '' après l'environnement... mais les environnements LaTeX ne l'exigent pas non plus.

Le « secret » des environnements LaTeX est la présence d'un indicateur interne qui fait ignorer les espaces indésirables. Pour activer cet indicateur pour votre environnement, depuis 1996, LaTeX propose une commande utilisateur `\ignorespacesafterend`.

______________________________________________________________________

*Source :* {faquk}`There's a space added after my environment <FAQ-spaftend>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programming
```

