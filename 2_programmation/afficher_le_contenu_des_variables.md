# Comment afficher le contenu de variables ?

- On a souvent besoin d'imprimer les valeurs des variables dans le fichier `.log` ou sur le terminal. Voici trois façons d'accéder au contenu de la variable `\textheight` :

1. `\showthe\textheight`
2. `\message{La hauteur du texte est de \the\textheight}`
3. `\typeout{La hauteur du texte est de \the\textheight}`

Ces techniques utilisent les primitives TeX :

- `\the`, qui renvoie la valeur d'une variable,
- `\showthe`, qui imprime la valeur d'une variable sur le terminal et dans le journal, sur une ligne rien que pour elle,
- et `\message`, qui imprime quelque chose dans le journal.

La commande `\typeout`, quant à elle, est le mécanisme général de sortie des messages de LaTeX.

Dans chaque cas, la valeur de la variable est exprimée en points (sachant qu'il y a 72.27 point de Knuth par pouce, un point vaut donc 0,35146 mm). Pour convertir les grandeurs en unités plus usuelles, on peut utiliser le package {ctanpkg}`printlen`, beaucoup plus flexible.

Par exemple, avec {ctanpkg}`printlen`, on peut écrire :

```latex
% !TEX noedit
\newlength{\foo}
\setlength{\foo}{12pt}
\verb|\foo| vaut \printlength{\foo}.
```

pour obtenir :

```latex
\documentclass{article}
\usepackage{printlen}
\begin{document}
\thispagestyle{empty}
\newlength{\foo}
\setlength{\foo}{12pt}
\verb+\foo+ vaut \printlength{\foo}.
\end{document}
```

alors que si on précise que l'on veut des millimètres :

```latex
% !TEX noedit
\newlength{\foo}
\setlength{\foo}{12pt}
\uselengthunit{mm}
\verb|\foo| vaut \printlength{\foo} (soit \the\foo).
```

on a :

```latex
\documentclass{article}
\usepackage{printlen}
\begin{document}
\thispagestyle{empty}
\newlength{\foo}
\setlength{\foo}{12pt}
\uselengthunit{mm}
\verb|\foo| vaut \printlength{\foo} (soit \the\foo).
\end{document}
```

La macro `\rndprintlength` permet d'arrondir la valeur :

```latex
% !TEX noedit
\newlength{\foo}
\setlength{\foo}{12pt}
\uselengthunit{mm}
\verb|\foo| vaut \rndprintlength{\foo} (valeur arrondie de \printlength{\foo}).
```

donne :

```latex
\documentclass{article}
\usepackage{printlen}
\begin{document}
\thispagestyle{empty}
\newlength{\foo}
\setlength{\foo}{12pt}
\uselengthunit{mm}
\verb|\foo| vaut \rndprintlength{\foo} (valeur arrondie de \printlength{\foo}).
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`How to print contents of variables? <FAQ-printvar>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,valeur d'une variable,programmation,macros,valeur d'une longueur,unité des longueurs,commande \\the
```

