# Comment traduire ce terme anglais ?

## Un guide de traduction anglo-français pour TeX

Cette FAQ, et plus généralement les documentations autour de LaTeX, comportent de nombreux termes techniques venant des domaines de l'informatique, de l'édition numérique, de la typographie, de l'impression...

La traduction des pages en anglais de la {faquk}`FAQ anglophone < >` a révélé la difficulté d'avoir des traductions homogènes entre contributeurs. Souvent, on est aussi tenté de conserver les termes techniques anglophones, parfois passés dans le langage oral, alors que des termes français existent.

Pour faciliter les traductions, nous proposons de s'appuyer sur le guide de traduction « [Lexique anglo-français du Companion](http://www.numdam.org/item/CG_2007___49_19_0.pdf) » publié dans le [Cahier GUTenberg n°49, de 2007](http://www.numdam.org/issues/CG_2007___49/) par Jean-Côme Charpentier et Jacques André à l'occasion de la traduction du {doc}`LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>`. Seul son lexique est reproduit ci-dessous, mais n'hésitez pas à en lire la totalité : il contient de précieuses explications sur sa genèse et sur les choix de vocabulaire.

## Autres sources utiles

Si des mots sont absents du lexique, voici quelques sources complémentaires :

- Le [Vocabulaire des industries graphiques/Graphic Arts Vocabulary](http://publications.gc.ca/collections/collection_2014/tpsgc-pwgsc/S52-2-210-1993.pdf) est un dictionnaire spécialisé bilingue, édité par le Secrétariat d'État du Canada et le groupe Communication Canada en 1993 (ISBN : {isbn}`0-660-58025-X`).
- Le Wiktionnaire propose un [Lexique en anglais de l’imprimerie](https://fr.wiktionary.org/wiki/Cat%C3%A9gorie:Lexique_en_anglais_de_l%E2%80%99imprimerie).

## Le lexique par ordre alphabétique

### A

:abbreviation: abréviation
:abstract: résumé
:accent: accent
:accented character: caractère accentué
:acute accent: accent aigu
:alphabet identifier: identificateur d'alphabet
:alignment: alignement
:alternate (font): (fonte) complémentaire
:alternative: une autre possibilité, une autre solution *\[une alternative = les deux à la fois\]*
:ampersand, &: signe « & », perluète (ou esperluette)
:and/or: ou *\[en français le « ou » n'est pas exclusif\]*
:annotate: annoter
:annotation: annotation
:antislash: barre oblique inverse, contre-oblique
:apostrophe: apostrophe *\[voir [note 2](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:appendix: appendice
:a priori: a priori *\[en romain\]*
:argument: argument *\[voir **key**\]*
:arithmetic calculations: calculs arithmétiques
:array: tableau *\[comme `table`\]*
:arrow: flèche
:article: article
:assignation: affectation *\[usuel en informatique\]*
:asterisk: astérisque
:asymmetrical: asymétrique
:at sign, @: arrobas (masculin) *\[voir [note 1, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:author: auteur
:auto-completion: complétion automatique
:auxiliary file: fichier auxiliaire

### B

:background fill: remplissage du fond
:backward compatibility: compatibilité ascendante
:badness: médiocrité
:balancing columns: colonnes équilibrées
:baseline: ligne de base *\[quoique* ligne de pied *serait mieux\]*
:bibliography: bibliographie
:big-g delimiter: big-g délimiteur
:8-bit: 8-bits *\[idem pour* **16-bit**, *etc.\]*
:blackboard font: fonte du tableau *\[ou « à la craie »\]*
:blackletter: gothique
:blank page: page vide *\[*page blanche// est parfois mieux\]//
:body area: corps de la page
:body font: fonte principale
:bold font: fonte grasse
:bounding box: boîte englobante *\[*rectangle exinscrit// serait plus précis\]//
:box: boîte
:boxing formula: formule encadrée
:braces: accolades
:break: coupure
:built-in function: fonction prédéfinie

### C

:calculations: calculs
:capital letter: capitale, majuscule
:capitalization: passage en majuscule/capitale
:caption: légende
:caret: [circonflexe](https://fr.wikipedia.org/wiki/Accent_circonflexe) « ^ »
:caron: [caron](<https://fr.wikipedia.org/wiki/Caron_(diacritique)>) « ˇ »
:case: casse //\[ici *case* signifie l'état majuscule/minuscule/petites-cap; faute de meilleur équivalent en français, on garde donc *casse*\]//
:case change: changement de casse
:case sensitive: sensible à la casse
:centered: centré
:change: modification
:changebar: barre de révision
:change history: historique des changements
:chapter: chapitre
:checksum: somme de contrôle
:citation: citation
:classe: classe *\[voir **document**\]*
:code: code
:code page: page de code *\[voir **page code**\]*
:collection: collection *\[en bibliographie\]*
:colon: deux-points «: »
:color: couleur
:column: colonne
:comma: virgule « , »
:command: commande
:comment: commentaire
:commercial font: fonte commerciale *\[usage\]*
:commutative diagrams: diagrammes commutatifs
:compile error: erreur à la compilation
:composed page numbers: numéros composés de pages
:compound math symbol: symboles mathématiques composés
:computer: [ordinateur](https://fr.wikipedia.org/wiki/Ordinateur)
:computer code: programme
:conditional: conditionnel
:configuration: configuration
:consistency: cohérence
:contents: contenu, matières
:continued fraction: fraction continue
:continuous slope curve: courbe à dérivée continue
:control: commande
:counter: compteur
:core font: fonte centrale
:crop marks: [hirondelles](<https://fr.wikipedia.org/wiki/Hirondelle_(imprimerie)>), traits de coupe
:cross references: références croisées
:currencies: monnaies
:curves: courbes
:CVS: [CVS](https://fr.wikipedia.org/wiki/Concurrent_versions_system) *\[Concurrent Versions System\]*

### D

:dash: [tiret](https://fr.wikipedia.org/wiki/Tiret) //\[et non *trait d'union* qui correspond au **hyphen** anglais\]//
:dashed line: ligne discontinue, ligne en tirets
:database: base de données
:data flow: flux de données
:debugging message: message de débogage
:decimal data: données décimales
:decoration: habillage
:decorated arrow: flèches habillées
:decorative (initial, letter): (initiale, lettre) ornée
:default: défaut
:device driver: pilote de périphérique
:delimiter: délimiteur
:depth: profondeur
:description: description *\[liste\]*
:device-independant: indépendant des périphériques de sortie
:directory name: nom de répertoire
:display (characters): caractères de titraille //\[parfois *caractères d'affichage*\]//
:display: un hors-texte
:displayed equation: équation hors texte, équation en hors-texte
:display heading: titre (de sectionnement) détaché
:display language: langage de visualisation
:discardable: volatil
:document class: classe de (du) document
:document heading: en-tête de document
:documentation: documentation
:dotted line: ligne pointillée, points de conduite
:dottier accent: points en guise d'accent
:double boxes: double cadre
:double quote: double quote *\[fém. romain: voir [note 2, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*

### E

:&: & *\[voir **ampersand**\]*
:editor: rédacteur *\[bibliographie\]*
:e.g.: p. ex. //\[au long: *par exemple*\]//
:ellipse: ellipse
:ellipsis: [points de suspension](https://fr.wikipedia.org/wiki/Points_de_suspension)
:em: cadratin *\[*`em`// si unité\]//
:e-mail address: [adresse électronique](https://fr.wikipedia.org/wiki/Adresse_électronique)
:embedded: emboîté //\[et non *imbriqué*\]//
:emphasizing: mise en évidence
:empty line: ligne vide
:encapsulation: encapsulation
:encoding: codage *\[bannir « encodage » qui n'apporte rien de plus\]*
:endnote: note finale
:et al.: [et autres](https://fr.wikipedia.org/wiki/Et_al.) *\[*et alii*, bien que d'origine latine, n'est pas français\]*
:entry: entrée
:enumerate: énumération
:environment: environnement
:equality: égalité
:equals sign: signe égal « = »
:equation: équation
:error: erreur
:escape character: caractère d'échappement
:exception dictionary: dictionnaire des exceptions
:exclamation mark: point d'exclamation « ! »
:excluding file: exclusion de fichiers
:expansion: développement, expansion *\[d'une macro\]*
:expansion of expandable token: développement d'unité lexicale développable
:extension: extension *\[voir **package** *et note 5 page 2 du LC2FR\]*
:extensions supported: extensions à inclure
:external documents: documents externes

### F

:fake (to): simuler *\[p.ex. un « é » par `+`''' \]*
:families: familles *\[de fontes\]*
:FAQ: FAQ *\[Foire aux Questions\]*
:field: champ
:figure: figure
:file: fichier
:filling material: motif de remplissage *\[voir **leaders**\]*
:float: flottant
:flush left: au fer à gauche
:flush right: au fer à droite
:font: fonte *\[voir [note 2, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:font definition file: fichier de définition de fonte
:footer: pied de page
:footnote: note de bas de page
:formal rule: règle formelle
:formulas: formules
:founder: fondateur *\[bibliographie\]*
:fraction: fraction
:fragile command: commande fragile
:freely accessible, --- available: libre de droits *\[pas toujours...\]*
:frame: cadre
:FTP server: serveur [FTP](https://fr.wikipedia.org/wiki/File_Transfer_Protocol)
:full citation: citation complète
:function names: noms de fonction

### G

:gender: genre
:generic: générique
:global: global
:glossary: glossaire
:glue: ressort
:glyphe: glyphe *\[masc.\]*
:graphics: graphiques
:graphical front end: interface graphique
:graphs: graphes
:grave accent: accent grave « \` »
:greater than sign: signe plus grand que « > »
:guillemets: guillemets *\[voir [note 2,page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*

### H

:hash size errors: erreur de taille de la table de hachage
:headed lists: listes avec titres
:header: en-tête, tête *\[de page\]*
:heading: titre
:height: hauteur
:highlighting text: mettre en valeur du texte
:hints, hinting: *hints*, *hinting* *\[(ital.) -- voir [note 3, page 44](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:history command: commande d'historique
:holes: réserves, fenêtres *\[dans paragraphe\]*
:horizontal extensions: étirements horizontaux
:hyphen (-): trait d'union //\[en typographie: *division*\]//
:hyphenation: césure, coupure de mots
:hyphenation pattern: motif de césure

### I

:ibidem: *\[wpfr>Ibidem|ibidem\]\]* *\[en italique\]*
:identification: identification
:i.e.: c.-à-d. //\[au long: *c'est-à-dire*\]//
:ignored fields: champs ignorés
:image file: image
:implement (to): implémenter *\[usage informatique; « implanter » est français mais a un autre sens\]*
:implementation: implémentation *\[voir **implement**\]*
:including file: inclusion de fichier
:inclusion: inclusion
:incrementing counter: incrémenter le/un compteur
:indentation: retrait //\[mieux: *renfoncement* mais peu connu\]//
:indentation (negative): débord
:index: index
:index generation: production d'index
:input: entrée
:initial letter: initiale
:installation: installation
:input file: source, fichier d'entrée //\[dans ce contexte, on écrit *« un » source*\]//
:internal: internal
:IPA: [API](https://fr.wikipedia.org/wiki/Alphabet_phonétique_international) *\[Alphabet Phonétique International\]*
:ISBN: [ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number)
:ISSN: [ISSN](https://fr.wikipedia.org/wiki/International_Standard_Serial_Number)
:Internet resources: ressources sur internet
:interrupting displays: hors-textes entrecoupés (de textes)
:italic correction: correction d'italique
:italic shape: forme italique
:item: élément (de liste) //\[on peut garder *item* pour un élément de liste « à puce » (simple)\]//
:itemized list: liste simple *\[ou « à puce », si ça vous démange\]*

### J

:justification: justification

### K

:key: clef *\[bibliographie\]*

### L

:label: étiquette *\[même dans les listes\]*
:landscape: paysage //\[nouvel usage, mais *à l'italienne* est bien plus traditionnel\]//
:language: langage
:language dependent string: vocabulaire caractéristique de la langue *\[dans contexte de multilinguisme\]*
:language setting: commutation de langue
:large operator: grand opérateur
:last update: dernière mise à jour
:law support: domaine juridique
:layout: présentation //\[générique: *apparence visuelle*; style (typo-)graphique, hiérarchisation, etc.\]//
:layout (page): mise en page *\[au singulier\]*
:leader dots: points de conduite
:leaders: points de conduite
:leading: interlignage *\[voir [note 4, page 44](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:leading (spacing): espace de tête
:length: longueur
:less than sign: signe inférieur à « < »
:letter: lettre
:letter-shaped: genre lettre
:letterspacing: interlettrage
:license: licence
:limiting positions: positionnement des limites
:line: ligne
:list: liste
:list item: élément de liste *\[voir **item**\]*
:literate programming: programmation littérale *\[faute de mieux\]*
:log (file): (fichier) journal
:lowercase: bas-de-casse, minuscule

### M

:macro: macro
:magnification: ajustement d'échelle
:mapping: correspondance
:margin footnote: note dans la marge
:marginal note: note marginale
:mark command: commande de marque
:master script: script maître
:material: texte, contenu, matériel, motif //\[selon le contexte ; *matériau* est plus français que *matériel* mais inusuel\]//
:math font: fonte mathématique
:math symbol: symbole mathématique
:mathematical typesetting: composition des mathématiques
:matrix-like environment: environnement du type matrice
:message: message
:monetary symbol: symbole monétaire
:monospaced font: fonte à chasse variable
:motto: [épigraphe](<https://fr.wikipedia.org/wiki/Épigraphe_(littérature)>)
:moving (argument): (argument) mobile
:multilingual document: document multilingue
:multipage table: tableau sur plusieurs pages
:multiple bibliographies: bibliographies multiples
:multiple citations: citations multiples
:multiple indexes: index multiples
:multiple tables of contents: tables des matières multiples

### N

:named boxes: boîtes nommées
:named page styles: styles de page nommés
:naming: nommage *\[terme consacré pour les fontes\]*
:negated math symbol: symbole mathématique de négation
:negative indentation: débord
:nested (commands): emboîtées (commandes) *\[emboîté donne ({}), imbriqué donnerait ({)}\]*
:nesting (commands): emboîtement (de commandes)
:number-only system: méthode par numéro
:numbering: numérotage
:numbering (page): foliotage

### O

:oblique font: fonte oblique
:obsolete: périmé *\[obsolète n'est pas français\]*
:oldstyle numerals: chiffres elzéviriens
:online, on-line: en ligne
:operator: opérateur
:optional: optionnel
:options: options
:ordinary math symbol: symbole mathématique ordinaire
:ornamental boxes: boîtes habillées
:ornaments: habillage *\[au singulier\]*
:outline: contour *\[substantif à propos de glyphes\]*
:outlined font: fonte éclairée
:output encoding: codage de sortie
:output file: fichier de sortie
:output routine: routine de sortie *\[faute de mieux\]*
:oval boxes: boîtes ovales

### P

:package: extension //\[on n'utilise pas *paquetage*; voir **extension**\]//
:page break: saut de page
:page code: codage *\[même pour ceux de Microsoft ou IBM\]*
:page layout: mise en page *\[sans « s »\]*
:page number: numéro de page, folio
:page style: style de page
:paper size: format de papier
:paragraph: paragraphe
:paragraph break algorithm: algorithme de coupure de ligne pour un paragraphe
:parenthesis: parenthèses
:partial tables of contents: tables partielles des matières
:path: chemin d'accès
:pattern: motif *\[voir **hyphenation**\]*
:period: point « . »
:periods: point de suspension « ... »
:PI fonts: fontes de casseaux
:plain text file: « *plain text file* » *\[exceptionnellement\]*
:plug-in: connexion
:point: point *\[mesure\]*
:popular: courant, habituel
:preamble (document): préambule
:preamble (table): motif
:pretty-printing: impression soignée
:previewer: visualisateur
:primitive: primitive
:printer point: point imprimante
:printing: impression
:process flow: flux de traitement
:program code: code d'un (du) programme
:programs: programmes
:proofs: épreuves
:properties: options *\[d'une classe\]*
:proportional fonts: fontes à chasse proportionnelle
:punctuation: ponctuation

### Q

:question mark: point d'interrogation « ? »
:quiet mode: mode silencieux
:quote: quote *\[voir [note 2, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:quoted: échappé *\[avec caractère d'échappement\]*
:quotation: [citation](<https://fr.wikipedia.org/wiki/Citation_(littérature)>)
:quoting: mettre entre guillemets *\[voir [note 2, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*

### R

:radical: radical
:ragged left: (en) drapeau à gauche
:ragged right: (en) drapeau à droite
:raw index: index brut
:recto-verso: recto-verso
:redefining: redéfinition
:reference: référence
:referencing: référer à
:register: registre
:relation symbol: symbole de relation
:release information: information sur la version, information de version
:render (to): rendre *\[à propos d'un glyphe\]*
:rendering: le rendu *\[d'un caractère\]*
:required: obligatoire
:resizing: changement de taille ou de corps *\[fonte\]*, redimensionnement *\[graphique\]*
:resolution: définition //\[et non *résolution*\]//
:result file: fichier cible
:revision bar: barre de révision
:rigid: rigide
:rigid length: longueur fix(é)e
:robust: robuste *\[terme consacré\]*
:Roman font shape: romain *\[substantif\]*, fonte droite *\[par opposition à italique\]*
:roman numerals: [chiffres romains](https://fr.wikipedia.org/wiki/Numération_romaine)
:rotating: rotation
:rounded corners: coins arrondis
:row: ligne
:rubber length: longueur élastique //\[plutôt *ressort* (ou *glue*) en terminologie Plain TeX\]//
:rule (graphic line): [filet](<https://fr.wikipedia.org/wiki/Filet_(typographie)>)
:rule boxes: boîtes de réglure
:run-in heading: titre attaché
:running footer: pied de page courant
:running header: en-tête de page courant

### S

:sans serif fonts: fontes sans empattements
:scaling (graphic object): ajustement d'échelle, redimensionner
:scholar: érudit, savant, spécialisé (ouvrage)
:script: cursive *\[sauf fonte « script »\]*
:script: script *\[programme Shell par exemple\]*
:searching: recherche
:section: section
:sectionning XX: XX de section *\[*XX de sectionnement// quand plus générique\]//
:semicolon: point-virgule « ; »
:separator: séparateur
:series, fonts: graisse
:serifed font: fonte à empattement
:set: ensemble
:settings: composition *\[typographie\]*, initialisations *\[informatique\]*
:(language) setting: commutation de langue
:shaded font: fonte ombrée
:shadow box: boîte ombrée
:shape (font): forme (de fonte) *\[faute de mieux\]*
:short-title: titre-court *\[bibliographie\]*
:shorthands: raccourci
:shrinkability: compression
:side bearing: approches *\[et non chasse\]*
:single-byte character: caractère (à) 8-bits, sur un octet
:size (font size): corps //\[voire *taille* selon contexte\]//
:skip (interline): pas (d'interlignage)
:slanted font: fonte inclinée, fonte penchée *\[éviter oblique\]*
:slash: barre oblique
:sloped font: fonte penchée
:smashing, mathematical typesetting: aplati, comprimé
:sort: tri
:source: source *\[masculin\]*
:space: espace, espacement
:spanning: fusion *\[de cellules de tableau\]*
:stack: pile
:stand-alone: autonome *\[index\]*
:standard: standard
:stretchability: étirement
:stress: inclinaison *\[dans le contexte de fontes\]*
:stripping comments: suppression des commentaires
:strut: *strut* *\[voir [note 5, page 44](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:style file: fichier de style
:sub-structures: sous-structures
:subscript: indice, limite inférieure *\[d'une intégrale\]*
:superscript: exposant, limite supérieure *\[d'une intégrale\]*
:support (to): supporter
:support (the): (le) support
:switches (font): modificateurs (de fonte)
:symbol: symbole

### T

:table of...: table de...
:table: tableau *\[pour le flottant `table` \]*
:tag (equation): étiquette
:terminal: terminal
:text: texte
:tilde: tilde « ~ »
:title: titre
:token: unité lexicale
:tracing: trace, suivi
:trailing blanks: espaces finales
:transcript file: fichier trace
:transcription: trace
:translation: traduction
:tree structure: structure arborescente
:trimming mark: trait de coupe
:troubleshooting: débogage
:two-sided: recto-verso
:typed text: texte tapé, à chasse fixe
:typeface: caractère, famille de caractères *\[voir [note 2, page 43](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:typesetting: composition
:typewriter font: fonte à chasse fixe
:typographic: typographique
:typographical font: fonte à chasse variable

### U

:unjustified paragraph: paragraphe non justifié
:uppercase: capitale, majuscule //\[en français, *haut de casse* n'existe pas\]//
:upright font: fonte droite
:user: utilisateur

### V

:variable: variable
:variant (glyphs): variante (de glyphes)
:vector: vecteur
:verbatim: verbatim *\[en romain\]*
:verbose mode: mode verbeux, bavard
:version control: contrôle de version *\[mais voir **control**\]*
:versus, vs: opposé à, ---
:vertical: vertical
:via: via *\[en romain\]*
:visible: affichable *\[pour les caractères ASCII\]*
:volume: volume

### W

:warning message: avertissement, message d'avertissement
:weight: [graisse](<https://fr.wikipedia.org/wiki/Graisse_(typographie)>) *\[d'un caractère, d'une fonte\]*
:whatsits: élément extraordinaire *\[voir [note 6, page 44](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf)\]*
:white space: espaces, blancs
:width: chasse *\[d'un caractère\]*, largeur *\[pour une fonte globale\]*
:wrapping text around image: habiller une image par du texte
:write: écrire

### X

:x-height: hauteur d'œil

______________________________________________________________________

*Source:* [Lexique anglo-français du Companion](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf), Jacques André, Jean-Côme Charpentier. *Cahiers GUTenberg*, n{sup}`o`49 (2007).

```{eval-rst}
.. meta::
   :keywords: LaTeX,lexique de traduction,gloassaire anglais-français,dictionnaire anglais-français,traduction,documentation,LaTeX Companion,LaTeX Compagnon,traduire la FAQ,contribuer à la FAQ
```


