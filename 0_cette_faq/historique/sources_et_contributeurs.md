# Sources et contributeurs de cette FAQ

## Sources

Le contenu de cette FAQ prend ses origines dans plusieurs projets antérieurs. Les auteurs connus de ces différentes versions sont listés ci-dessous; en cas d'oubli, n'hésitez pas à corriger.

- [la première FAQ francophone](https://2001-faq-latex.fr/) initiée par Marie-Paule Kluth,
- [la FAQ fctt](http://faqfctt.fr.eu.org/) menée par Benjamin Bayart jusqu'en 2004,
- [la FAQ anglaise](https://texfaq.org/), actuellement maintenue par Joseph Wright et David Carlisle.

## Contributeurs

### Première FAQ FR

- **Marie-Paule Kluth**,
- André Allavena,
- Cyril Banderier,
- Michel Bovani,
- Antoine Chambert-Loir,
- Nicolas Corréard,
- Jean-Pierre Coulon,
- Olivier Debré,
- Eugen Dedu,
- Éric Depardieu,
- Nicolas Deschaume,
- Jean Dezert,
- Maurice Diamantini,
- Andriamasinoro Fenintsoa,
- Emmanuel Gureghian,
- Yvon Henel,
- Olivier Houix,
- Sebastien Jean,
- Arnaud Launay,
- Gilles Leborgne,
- Vincent Lefèvre,
- Stéphane Lepolozec,
- François Lesage,
- Nadine Manset,
- Jean-Baptiste Marchand,
- Dominic Mitchell,
- Julien Mudry,
- Youness Naji,
- Jean-Dominique Orvoen,
- François Pennaneac'h,
- Frédéric Petit,
- Philippe Pham,
- Françoise Pinsard,
- Stéphane Pion,
- Jean-Philippe Rey,
- Benoît Rivet,
- Denis Roegel.

```{image} /marie-paule-kluth-faq-latex-francaise.jpeg
:alt: image
:width: 200px
```

### FAQ fctt 2004

- **Benjamin Bayart**,
- Thierry Bayet,
- Prakash Countcham,
- Éric Depardieu,
- Jean-Pierre F. Drucbert,
- Mathieu Goutelle,
- Yvon Henel,
- Florence Henry,
- Loïc Joly,
- Christophe Jorssen,
- Erwan Le Pennec,
- Nicolas Markey,
- Françoise Marre-Fournier,
- Sébastien Mengin,
- Josselin Noirel,
- Paul Pichaureau,
- Jayce Piel,
- Bruno Piguet,
- Éric Streit,
- Fabien Torre,
- Thomas van Oudenhove,
- Damien Wyart.

### FAQ 2013

- Maxime Chupin,
- Yvon Henel,
- Jérémy Just,
- Manuel Pégourié-Gonnard.

### FAQ 2016

- **Jérémy Just**,
- Frédéric Darboux,
- Bastien Dumont,
- Manuel Pégourié-Gonnard,
- Yannick Tanguy.

### FAQ UK

- **Robin Fairbairns**,
- William Adams,
- Donald Arseneau,
- Rosemary Bailey,
- Barbara Beeton,
- Karl Berry,
- Giuseppe Bilotta,
- Charles Cameron,
- David Carlisle,
- François Charette,
- Paulo Cereda,
- Damian Cugley,
- Martin Garrod,
- Michael Dewey,
- Michael Downes,
- Jean-Pierre Drucbert,
- David Epstein,
- Michael Ernst,
- Thomas Esser,
- Ulrike Fischer,
- Bruno Le Floch,
- Anthony Goreham,
- Norman Gray,
- Enrico Gregorio,
- Werner Grundlingh,
- Eitan Gurari,
- William Hammond,
- John Hammond,
- John Harper,
- Gernot Hassenpflug,
- Troy Henderson,
- Hartmut Henkel,
- Stephan Hennig,
- Wilfried Hennings
- John Hobby,
- Morten Høgholm,
- Berthold Horn,
- Ian Hutchinson,
- Werner Icking,
- William Ingram,
- David Jansen,
- Alan Jeffrey,
- Regnor Jernsletten,
- Jérémy Just,
- David Kastrup,
- Oleg Katsitadze,
- Isaac Khabaza,
- Ulrich Klauer,
- Markus Kohm,
- Stefan Kottwitz,
- David Kraus,
- Ryszard Kubiak,
- Simon Law,
- Uwe Lück,
- Daniel Luecking,
- Aditya Mahajan,
- Sanjoy Mahajan,
- Diego Andres Alvarez Marin,
- Andreas Matthias,
- Steve Mayer,
- Javier Mora,
- Brooks Moses,
- Peter Moulder,
- Iain Murray,
- Vilar Camara Neto,
- Dick Nickalls,
- Ted Nieland,
- Hans Nordhaug,
- Pat Rau,
- Heiko Oberdiek,
- Piet van Oostrum,
- Scott Pakin,
- Oren Patashnik,
- Manuel Pégourié-Gonnard,
- Steve Peter,
- Susanne Raab,
- Sebastian Rahtz,
- Philip Ratcliffe,
- Chris Rowley,
- José Carlos Santos,
- Walter Schmidt,
- Hans-Peter Schröcker,
- Joachim Schrod,
- Uwe Siart,
- Maarten Sneep,
- Axel Sommerfeldt,
- Philipp Stephani,
- James Szinger,
- Nicola Talbot,
- Bob Tennent,
- Tomek Trzeciak,
- Ulrik Vieth,
- Mike Vulis,
- Chris Walker,
- Moritz Wemheuer,
- Peter Wilson,
- Joseph Wright,
- Rick Zaccone,
- Gregor Zattler,
- Reinhard Zierke.

## Remerciements

Nos remerciements vont aussi à la communauté des utilisateurs francophones de TeX/LaTeX, représentée entre autres par le forum `fr.comp.text.tex` et [la liste GUT](https://www.gutenberg.eu.org/listes).

```{eval-rst}
.. meta::
   :keywords: auteurs de la FAQ,remerciements
```


