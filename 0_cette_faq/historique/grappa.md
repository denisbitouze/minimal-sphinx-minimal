# Qu'est-ce que le/la GRAppA ?

De 2000 à 2019, une copie de la FAQ LaTeX a été hébergée par le GRAppA, *Groupe
de recherche en Apprentissage Automatique* [à
Lille](https://fr.wikipedia.org/wiki/Université_Lille-III). Elle l'est
actuellement [ici](https://2001-faq-latex.fr/), par [Fabien
Torre](https://fabien-torre.fr/).

À partir de 1996, une version HTML de la FAQ LaTeX a été produite par [Fabien
Torre](https://fabien-torre.fr/), alors en thèse au Laboratoire de Recherche en
Informatique (LRI, Orsay), et hébergée à [cette
adresse](http://www.lri.fr/Francais/Recherche/ia/stuff/FAQ-LaTeX).  Suite au
recrutement de [Fabien Torre](https://fabien-torre.fr/) à l'[université de
Lille](https://fr.wikipedia.org/wiki/Universit%C3%A9_de_Lille) en 2000, et
jusqu'en 2019, cette version de la FAQ LaTeX a été hébergée par le GRAppA
(*Groupe de Recherche en Apprentissage Automatique* de Lille)
à [cette adresse](http://www.grappa.univ-lille3.fr/FAQ-LaTeX/).


Les habitués parlent de « la FAQ GRAppA », pour désigner la version de la FAQ
communautaire datée 16 octobre 2001 (v2.28) restée à disposition à cette URL
pendant presque 20 ans, et il reste de nombreuses mentions de cette FAQ dans les
documentations disponibles sur internet et dans les livres sur LaTeX.
Cette version figée est consultable [ici](https://faqlatex.fabien-torre.fr/).

[Tout le contenu de la « FAQ GRAppA » a été intégré à la présente FAQ LaTeX](/0_cette_faq/historique/sources_et_contributeurs) et est progressivement mis à jour.

______________________________________________________________________

*Sources :*

- [Université de Lille](https://fr.wikipedia.org/wiki/Université_de_Lille),
- [Laboratoire d'informatique fondamentale de Lille](https://fr.wikipedia.org/wiki/Laboratoire_d'informatique_fondamentale_de_Lille).

```{eval-rst}
.. meta::
   :keywords: FAQ GRAppA,LaTeX,historique,ancienne FAQ LaTeX,première FAQ LaTeX,FAQ fctt,FAQ LaTeX francophone,fr.comp.text.tex
```


