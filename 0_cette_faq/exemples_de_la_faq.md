# Y a-t-il des exemples ?

- Oui, plein ! En fait, le but est d'en présenter le maximum.
- Les exemples sans affichage de résultat sont présentés comme ceci :

```latex
% !TEX noedit
Ceci est un exemple de code \LaTeX{} dont on ne présente pas le résultat.
```

- Les exemples avec affichage du résultat seront présentés comme l'exemple suivant :

```latex
% !TEX noedit
Ceci est un exemple de code \LaTeX{} dont on présente le résultat.
```

```latex
Ceci est un exemple de code \LaTeX{} dont on présente le résultat.
```

- Certains exemples montrent des documents entiers, ils sont alors mis en forme comme ici :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[latin1]{inputenc}
  \usepackage[frenchb]{babel}

%% Commentaire : fin du preambule

\begin{document}

\title{Titre de mon article}
\author{Le Monsieur \and La Madame}
\date{Le \today}
\maketitle

\begin{abstract}
Résumé de mon article, passionnant.
\end{abstract}

\tableofcontents

\section{Explications}

\texttt{fontenc}, \texttt{inputenc} sont des
packages permettant d'utiliser les accents.
\texttt{babel} sert à franciser le document.
\texttt{article} est la classe de document
utilisée.

\end{document}
```

- Certains exemples portent sur des documents entiers, mais seule une partie de la page produite est intéressante. Dans ce cas là un petit pictogramme indique quelle partie de la page est visible, comme dans l'exemple ci-dessous. C'est une reprise de l'exemple précédent, dans laquelle un zoom est fait sur le titre.

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[latin1]{inputenc}
  \usepackage[frenchb]{babel}

%% Commentaire : fin du preambule

\begin{document}

\title{Titre de mon article}
\author{Le Monsieur \and La Madame}
\date{Le \today}
\maketitle

\begin{abstract}
Résumé de mon article, passionnant.
\end{abstract}

\tableofcontents

\section{Explications}

\texttt{fontenc}, \texttt{inputenc} sont des
packages permettant d'utiliser les accents.
\texttt{Babel} sert à franciser le document.
\texttt{article} est la classe de document
utilisée.

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,code,programmation,FAQ,exemples de code LaTeX
```


