# Mentions légales

# Copyright

© 1999-2021, Groupe des mainteneurs de la FAQ LaTeX francophone.

# Sous quelle licence est publiée cette FAQ ?

- Les auteurs de la FAQ 2004 ont choisi de la publier en licence libre : la GNU Free Documentation License.

Vous pouvez faire des copies, distribuer et/ou modifier ce document selon les termes de la GNU Free Documentation License, version 1.2 ou ultérieure. La dernière version peut se trouver sur le site de GNU : <http://www.gnu.org/licenses/fdl.html>.

<https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png>

- À la reprise de la FAQ par l'association GUTenberg, le choix s'est porté sur une autre licence libre : [CC Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

Les exigences des deux licences nous semblent compatibles *dans leur but* : autoriser le partage et la réutilisation du contenu, à condition que les auteurs soient crédités et que le partage se fasse sous la même licence libre. Mais nous n'avons pas examiné la question avec l'œil du juriste. Si vous avez contribué à une version précédente de la FAQ et que ce changement de licence vous dérange, [n'hésitez pas à nous contacter.](mailto:faq@gutenberg.eu.org)

______________________________________________________________________

*Sources :*

- <https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/>
- <https://www.gnu.org/licenses/license-compatibility.fr.html>

```{eval-rst}
.. meta::
   :keywords: LaTeX,foire aux questions,frequently asked questions,questions fréquemment posées,questions fréquentes,licence,droit de reproduction,contenu,GPL,Creative Commons
```


