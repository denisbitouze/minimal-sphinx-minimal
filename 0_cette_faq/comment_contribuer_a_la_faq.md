# Comment contribuer ?

## Contenu de la FAQ

Vous pouvez tout d'abord contribuer au contenu de cete FAQ. C'est très simple :

- [créez-vous un compte](start?do=register), il vous suffit pour cela d'avoir une adresse e-mail valide,

- {doc}`jetez un œil à la syntaxe utilisée sur ce site </wiki/syntax>`,

- puis commencez à éditer les pages :

  - corrigez les éventuelles erreurs {doc}`certaines pages attendent des corrections </0_cette_faq/espace_contributeurs/questions_a_reviser>`),
  - traduisez les pages {doc}`qui seraient en anglais </0_cette_faq/espace_contributeurs/pages_a_traduire>`,
  - ajoutez des réponses aux questions ou des précisions,
  - créez de nouvelles pages pour partager votre expérience avec LaTeX.

Vous ne savez pas par où commencer ? \<randompage_link>Prenez une page au hasard et améliorez-la !\</randompage_link>

## Infrastructure

Vous avez un peu d'expérience dans la configuration et le développement de Dokuwiki ?

[Alors vous pouvez nous contacter pour nous aider.](mailto:faq@gutenberg.eu.org)

## Association GUTenberg

Enfin, vous pouvez [adhérer à l'association GUTenberg](https://www.gutenberg.eu.org/Adherer-a-l-association), qui héberge et maintient cette FAQ.

```{eval-rst}
.. meta::
   :keywords: foire aux questions LaTeX,documentation sur LaTeX,aide sur LaTeX,aide sur TeX,Association GUTenberg
```


