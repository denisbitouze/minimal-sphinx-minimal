# Bac à sable

{octicon}`alert;1em;sd-text-warning` \*\* Cette page est pensée pour faire des tests : son contenu n'est donc en aucun cas pérenne ! \*\* :-D

______________________________________________________________________

## Avec la commande de base \\fbox

Voici un format avec des dimensions exprimées en cm.

```latex
\documentclass{article}
\usepackage[width=7cm,height=1cm]{geometry}
\pagestyle{empty}
\begin{document}
Je souhaite \fbox{encadrer} un mot dans un paragraphe.
\end{document}
```

Voici le même avec une indentation pour séparer l'exemple et son résultat du reste du texte.

```latex
\documentclass{article}
\usepackage[width=7cm,height=1cm]{geometry}
\pagestyle{empty}
\begin{document}
Je souhaite \fbox{encadrer} un mot dans un paragraphe.
\end{document}
```

Voilà...

## Exemples avancés avec d'autres extensions

Les méthodes ci-dessous fournissent des cadres d'apparence très classique. D'autres extensions dessinent des cadres adaptés à des documents au look plus moderne, avec couleurs et icônes.

Certaines extensions permettent également d'encadrer des pages entières, ou des blocs de texte s'étendant sur plusieurs pages.

### L'extension fancybox

L'extension {ctanpkg}`fancybox` définit des commandes telles que `\shadowbox`, `\doublebox` et `\ovalbox`, qui fonctionnent de la même manière que `\fbox` ci-dessus :

```latex
\documentclass{article}
\usepackage[width=9cm,height=7cm]{geometry}
\usepackage{fancybox}
\pagestyle{empty}
\begin{document}

\shadowbox{Texte ombré.}
\smallskip

\doublebox{Texte doublement encadré.}
\smallskip

\ovalbox{Texte dans un cadre
  aux coins arrondis.}
\end{document}
```

### L'extension awesomebox

L'extension {ctanpkg}`awesomebox` ne dessine pas de cadres à proprement parler, mais peut mettre en valeur un bloc de texte avec une icône et un filet coloré :

```latex
\documentclass{article}
  \usepackage[width=9cm,height=7cm]{geometry}
  \usepackage{awesomebox}
  \pagestyle{empty}

\begin{document}

\notebox{Notez bien ceci !}
\smallskip

\importantbox{Lisez bien ce paragraphe
avant de passer à la suite de ce document
sinon, vous risquez de perdre votre temps
en considérations inutiles !}
\smallskip

\end{document}
```

{ctanpkg}`Awesomebox <awesomebox>` a cinq dessins de base, utilisables sous forme d'une commande ou d'un environnement :

| Pour...                 | Commande        | Environnement                                     | Exemple                                                                                                                                                           |
| ----------------------- | --------------- | ------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Une note                | `\notebox`      | `\begin{noteblock}`...`\end{noteblock}`           | {raw-latex}`\documentclass[6pt]{extarticle}\usepackage{awesomebox}\pagestyle{empty}\begin{document}\notebox{Marcel se couche de bonne heure.}\end{document}`      |
| Une suggestion          | `\tipbox`       | `\begin{tipblock}`...`\end{tipblock}`             | {raw-latex}`\documentclass[6pt]{extarticle}\usepackage{awesomebox}\pagestyle{empty}\begin{document}\tipbox{Marcel se couche de bonne heure.}\end{document}`       |
| Un avertissement        | `\warningbox`   | `\begin{warningblock}`...`\end{warningblock}`     | {raw-latex}`\documentclass[6pt]{extarticle}\usepackage{awesomebox}\pagestyle{empty}\begin{document}\warningbox{Marcel se couche de bonne heure.}\end{document}`   |
| Une mise en garde       | `\cautionbox`   | `\begin{cautionblock}`...`\end{cautionblock}`     | {raw-latex}`\documentclass[6pt]{extarticle}\usepackage{awesomebox}\pagestyle{empty}\begin{document}\cautionbox{Marcel se couche de bonne heure.}\end{document}`   |
| Une remarque importante | `\importantbox` | `\begin{importantblock}`...`\end{importantblock}` | {raw-latex}`\documentclass[6pt]{extarticle}\usepackage{awesomebox}\pagestyle{empty}\begin{document}\importantbox{Marcel se couche de bonne heure.}\end{document}` |

:::{tip}
Il est également très facile de définir vos propres boîtes avec la commande `\awesomebox`. Les icônes peuvent être choisies parmi celles de l'extension {ctanpkg}`fontawesome5`, et vous pouvez mettre des filets horizontaux avant et/ou après votre bloc de texte :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{awesomebox}

\begin{document}

\awesomebox[white][\abShortLine]{0pt}{\faGrinBeam[regular]}{pink}{Ceci va vous rendre heureux\dots}

\end{document}
```
:::

### L'extension tcolorbox

L'extension {ctanpkg}`tcolorbox` utilise {ctanpkg}`TikZ/PGF <tikz>` pour dessiner ses cadres. Si vous avez l'habitude de Ti*k*Z, vous apprécierez la syntaxe clefs-valeurs qui permet de paramétrer finement l'apparence des cadres (couleurs, formes, structures...).

À cause de ses possibilités de configuration immenses, {texdoc}`sa documentation <tcolorbox>` fait plus de 500 pages (en anglais). Mais les deux exemples ci-dessous montrent qu'il n'est vraiment pas compliqué de dessiner des cadres déjà adaptés à pas mal de circonstances, et les nombreuses illustrations de la documentation devraient vous aider à aller plus loin :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{tcolorbox}

\begin{document}

\begin{tcolorbox}
Le cadre par défaut
\end{tcolorbox}
\smallskip

\begin{tcolorbox}[colback=red!5!white,
                  colframe=red!75!black,
                  title=Cadre sexy
                 ]
Un autre cadre \textbf{plus coloré}, séparé
en deux parties, et portant un titre.
\tcblower
Demain, \textit{j'enlève le bas}.
\end{tcolorbox}

\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm,height=6cm]{geometry}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \usepackage{tcolorbox}
  \pagestyle{empty}

\begin{document}

\begin{tcolorbox}
Le cadre par défaut
\end{tcolorbox}
\smallskip

\begin{tcolorbox}[colback=red!5!white,
                  colframe=red!75!black,
                  title=Cadre sexy
                 ]
Un autre cadre \textbf{plus coloré}, séparé
en deux parties, et portant un titre.
\tcblower
Demain, \textit{j'enlève le bas}.
\end{tcolorbox}

\end{document}
```


