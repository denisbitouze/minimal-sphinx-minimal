# Déplacements de page

## Petite explication

La FAQ présente parfois un classement peu logique, ce qui peut nécessiter de déplacer des pages. Afin d'éviter de perdre des informations sur les contributions antérieures, le déplacement de page doit être traité par un script. Pour cela, il suffit de placer ci-dessous l'emplacement source et l'emplacement cible des pages à déplacer, en les séparant par le caractère « > ». Voici un exemple de la manière de les présenter.

```
https://faq.gutenberg.eu.org/0_cette_faq/espace_contributeurs/deplacements > https://faq.gutenberg.eu.org/0_cette_faq/ici/la/deplacements
```

Si vous déplacez plusieurs pages, pensez à insérer une ligne blanche entre chaque demande pour faciliter la lecture de la liste pour un humain.

Une fois le script passé, les pages traitées sont supprimées de cette page.

## Pages à déplacer

<https://faq.gutenberg-asso.fr/6_distributions/annexes/alternatives_a_tex> > <https://faq.gutenberg-asso.fr/1_generalites/bases/alternatives_a_tex>

<https://faq.gutenberg-asso.fr/6_distributions/annexes/projets_derives_de_tex> > <https://faq.gutenberg-asso.fr/1_generalites/histoire/projets_derives_de_tex>

<https://faq.gutenberg-asso.fr/5_fichiers/ecrire_dans_un_fichier> > <https://faq.gutenberg-asso.fr/2_programmation/compilation/ecrire_dans_un_fichier>

<https://faq.gutenberg-asso.fr/3_composition/flottants/positionnement> > <https://faq.gutenberg-asso.fr/3_composition/flottants/start>


