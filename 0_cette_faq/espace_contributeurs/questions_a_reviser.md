# Quelles réponses sont à réviser ?

{octicon}`alert;1em;sd-text-warning` **Page générée automatiquement. Ne pas modifier!**

Pages en attente de correction (dernière mise à jour : 2022/06/07 11:33) :

## 1. Généralités

### Comment faire ses premiers pas ?

[1_generalites:comment_faire_ses_premiers_pas](/1_generalites/comment_faire_ses_premiers_pas) (3 593 octets, FR)

- *Sans doute à compléter pour donner des sites d'explication d'installation.*

### Que lire sur les fontes ?

[1_generalites:documentation:documents:documents_sur_les_fontes](/1_generalites/documentation/documents/documents_sur_les_fontes) (552 octets, EN)

- Ajouter des références sur les fontes en français.

### Où trouver des tutoriels sur (La)TeX ?

[1_generalites:documentation:documents:tutoriels:tutoriaux](/1_generalites/documentation/documents/tutoriels/tutoriaux) (5 460 octets, FR)

- *La notion de tutoriel est ici assez vague, les présentes références mélangent des pages de liens, des exemples et des documents complets...*

### Où trouver des groupes de discussion sur (La)TeX ?

[1_generalites:documentation:listes_de_discussion:groupes_de_discussion](/1_generalites/documentation/listes_de_discussion/groupes_de_discussion) (3 378 octets, FR)

- Doit-on expliquer pour les différents clients de messagerie ?

### Y-a-t'il d'autres trucs-TeX ?

[1_generalites:glossaire:xx-tex](/1_generalites/glossaire/xx-tex) (2 916 octets, FR)

- Ajouter quelques lignes de présentation.

### Que sont LaTeX3 et le « LaTeX Project » ?

[1_generalites:histoire:c_est_quoi_latex3](/1_generalites/histoire/c_est_quoi_latex3) (4 819 octets, FR)

- Par ailleurs, si LaTeX n'existe pas en tant que tel, il existe LaTeXplus, qui est la somme d'un certain nombre d'extensions pour :latexlogo:`LaTeX` qui, mis bout à bout, font ce qu'on peut avoir de mieux à l'heure actuelle comme approximation d'une prévision de ce à quoi pourrait un jour, peut-être, ressembler LaTeX... Bien entendu, ces différents morceaux ne sont pas toujours compatibles entre eux, ni complets, ni forcément documentés. Et quand il y a une documentation, elle n'est a priori pas à jour. Et quand les morceaux sont complets, ils sont a priori buggés. Sinon, ce serait trop simple. Bref, c'est fait pour s'amuser, pour le moment. ( *manque de références sur ce paragraphe.*)

### Quelle est l'histoire de TeX et LaTeX ?

[1_generalites:histoire:histoire_de_tex_et_latex](/1_generalites/histoire/histoire_de_tex_et_latex) (3 741 octets, FR)

- [Signalons également NTS](https://en.wikipedia.org/wiki/New_Typesetting_System), un projet ambitieux qui visait d'abord à réimplémenter TeX en Java, puis l'enrichir de nouvelles fonctionnalités et n'en garder probablement que les concepts. Une version alpha a été publiée en 2000 et son développement a cessé peu après. ( *Ce texte devrait sans doute être reporté ailleurs car le projet est mort depuis un bon moment et n'a qu'un caractère historique anecdotique.*)

### Peut-on gagner de l'argent avec LaTeX ?

[1_generalites:peut_on_devenir_riche_avec_latex](/1_generalites/peut_on_devenir_riche_avec_latex) (48 943 octets, FR)

- Le contenu actuel de cette page provient du [LaTeX Ninja blog](https://latex-ninja.com/2019/02/02/earning-money-with-latex/),

## 2. Programmation

### Comment compiler un document LaTeX en ligne ?

[2_programmation:compilation:compiler_en_ligne](/2_programmation/compilation/compiler_en_ligne) (4 444 octets, EN)

- Y en a-t-il des instances ouvertes à tous sur des serveurs académiques ou associatifs ?

### Que signifie l'erreur : « '\<caractère>' invalid at this point » ?

[2_programmation:erreurs:0:invalid_at_this_point](/2_programmation/erreurs/0/invalid_at_this_point) (1 116 octets, FR)

- L'extension {ctanpkg}`calc` est chargée et la formule utilisée dans une des commandes `\setcounter`, `\setlength`, `\addtocounter` ou `\addtolength` utilise une syntaxe incorrecte du point de vue de {ctanpkg}`calc`. Voir section A.3.1 du *LaTeX Companion* pour plus de détails.

### Que signifie l'erreur : « Accent \<commande> not provided by font family \<nom> » ?

[2_programmation:erreurs:a:accent_not_provided_by_font_family](/2_programmation/erreurs/a/accent_not_provided_by_font_family) (1 296 octets, FR)

- Voir section 7.5.4 du *LaTeX Companion* pour savoir comment obtenir une autre représentation de ces accents.

### Que signifie l'erreur : « Bad \\line or \\vector argument » ?

[2_programmation:erreurs:b:bad_line_or_vector_argument](/2_programmation/erreurs/b/bad_line_or_vector_argument) (1 050 octets, FR)

- Pour le dernier cas, voir le chapitre 10 du *LaTeX Companion* pour d'autres façons de procéder.

### Que signifie l'erreur : « \\begin{\<env>} on input line \<num-ligne> ended by \\end{\<autre-env>} » ?

[2_programmation:erreurs:b:begin_env_on_input_line_nn_ended_by_end_other_env](/2_programmation/erreurs/b/begin_env_on_input_line_nn_ended_by_end_other_env) (2 166 octets, FR)

- Voir section 3.4.3 (page 168) du *LaTeX Companion* pour des solutions utilisant des environnements de type `verbatim`.

### Que signifie l'erreur : « \\caption outside float » ?

[2_programmation:erreurs:c:caption_outside_float](/2_programmation/erreurs/c/caption_outside_float) (1 006 octets, FR)

- par certaines extensions décrites au chapitre 6 du *LaTeX Companion* .

### Que signifie l'erreur : « File \`\<nom>' not found » ?

[2_programmation:erreurs:f:file_not_found](/2_programmation/erreurs/f/file_not_found) (2 183 octets, FR)

- Voir l'entrée page 917 du *LaTeX Companion* .

### Que signifie le message : « fixltx2e is not required with releases after 2015 » ?

[2_programmation:erreurs:f:fixltx2e_is_not_required](/2_programmation/erreurs/f/fixltx2e_is_not_required) (3 230 octets, FR)

- \* Donner d'autres exemples.

### Que signifie l'erreur : « Font \<nom-interne> = \<externe> not loaded : Not enough room left » ?

[2_programmation:erreurs:f:font_not_loaded_not_enough_room_left](/2_programmation/erreurs/f/font_not_loaded_not_enough_room_left) (1 496 octets, FR)

- Pour savoir quelles sont les fontes chargées, il faut utiliser l'extension {ctanpkg}`tracefnt`
- Voir section 7.10.7 du *LaTeX Companion* pour plus de détails.

### Que signifie l'erreur : « Illegal character in array arg » ?

[2_programmation:erreurs:i:illegal_character_in_array_arg](/2_programmation/erreurs/i/illegal_character_in_array_arg) (1 325 octets, FR)

- décrite au chapitre 5 du *LaTeX Companion* , en ayant oublié de la charger

### Que signifie l'erreur : « Illegal unit of measure (pt inserted) » ?

[2_programmation:erreurs:i:illegal_unit_of_measure](/2_programmation/erreurs/i/illegal_unit_of_measure) (1 033 octets, FR)

- Voir section A.1.5 du *LaTeX Companion* pour plus de détails.

### Que signifie l'erreur : « Improper \\hyphenation » ?

[2_programmation:erreurs:i:improper_hyphenation](/2_programmation/erreurs/i/improper_hyphenation) (3 150 octets, FR)

- Voir page 463 du *LaTeX Companion* pour une explication des différences de caractères dans les codages principaux.

### Que signifie l'erreur : « Keyboard character used is undefined in input encoding \<enc> » ?

[2_programmation:erreurs:k:keyboard_character_used_is_undefined_in_input_encoding](/2_programmation/erreurs/k/keyboard_character_used_is_undefined_in_input_encoding) (1 172 octets, FR)

- à un objet LICR (voir sections 7.5.2 et 7.11.3 du *LaTeX Companion* ).

### Que signifie l'erreur : « Limit controls must follow a math operator » ?

[2_programmation:erreurs:l:limit_controls_must_follow_a_math_operator](/2_programmation/erreurs/l/limit_controls_must_follow_a_math_operator) (1 050 octets, FR)

- Voir tableau 8.4 page 510 du *LaTeX Companion* pour une liste des opérateurs usuels.

### Que signifie l'erreur : « \\LoadClass in package file » ?

[2_programmation:erreurs:l:loadclass_in_package_file](/2_programmation/erreurs/l/loadclass_in_package_file) (890 octets, FR)

- (voir section A.4 du *LaTeX Companion* ).

### Que signifie l'erreur « LaTeX2e command \\usepackage in LaTeX 2.09 document » ?

[2_programmation:erreurs:l:usepackage_in_latex_2_09_document](/2_programmation/erreurs/l/usepackage_in_latex_2_09_document) (758 octets, FR)

- À rédiger.

### Que signifie l'erreur : « Missing } inserted » ?

[2_programmation:erreurs:m:missing_closing_curly_bracket_inserted](/2_programmation/erreurs/m/missing_closing_curly_bracket_inserted) (1 252 octets, FR)

- comme expliqué à la page 934 du *LaTeX Companion* .

### Que signifie l'erreur : « Missing delimiter (. inserted) » ?

[2_programmation:erreurs:m:missing_delimiter_dot_inserted](/2_programmation/erreurs/m/missing_delimiter_dot_inserted) (1 062 octets, FR)

- (voir section 8.5.3 page 508 du *LaTeX Companion* ).

### Que signifie l'erreur : « Missing \\endgroup inserted » ?

[2_programmation:erreurs:m:missing_endgroup_inserted](/2_programmation/erreurs/m/missing_endgroup_inserted) (1 141 octets, FR)

- comme expliqué à la page 934 du *LaTeX Companion* .

### Que signifie l'erreur : « Option clash for package \<nom> » ?

[2_programmation:erreurs:o:option_clash_for_package](/2_programmation/erreurs/o/option_clash_for_package) (4 962 octets, FR)

- Il existe ici un cas particulier : l'extension {ctanpkg}`fontenc` peut être chargée autant de fois que nécessaire avec différentes options. Voir section 7.5.3 page 369 du *LaTeX Companion* ).
- \* charger une extension avant `\documentclass` avec la commande `\RequirePackage`. Voir section 2.1.1 du *LaTeX Companion* pour plus de détails ;

### Que signifie l'erreur : « Paragraph ended before \<commande> was complete » ?

[2_programmation:erreurs:p:paragraph_ended_before_command_was_complete](/2_programmation/erreurs/p/paragraph_ended_before_command_was_complete) (1 397 octets, FR)

- Comme on l'a vu à la section A.1.2 du *LaTeX Companion* , les commandes définies

### Que signifie l'erreur : « \\RequirePackage or \\LoadClass in Options Section » ?

[2_programmation:erreurs:r:requirepackage_or_loadclass_in_options_section](/2_programmation/erreurs/r/requirepackage_or_loadclass_in_options_section) (1 481 octets, FR)

- Voir section A.4 du *LaTeX Companion*.

### Que signifie l'erreur : « Symbol \<commande> not provided by font family \<nom> » ?

[2_programmation:erreurs:s:symbol_not_provided_by_font_family](/2_programmation/erreurs/s/symbol_not_provided_by_font_family) (1 432 octets, FR)

- Voir section 7.5.4 du *LaTeX Companion* pour plus de détails.

### Que signifie l'erreur : « TeX capacity exceeded, \<explication> » ?

[2_programmation:erreurs:t:tex_capacity_exceeded](/2_programmation/erreurs/t/tex_capacity_exceeded) (5 627 octets, FR)

- Cette erreur est étudiée en détail à la section B.1.1 page 932 du *LaTeX Companion*.

### Que signifie l'erreur : « This NFSS system isn't set up properly » ?

[2_programmation:erreurs:t:this_nfss_system_is_not_set_up_properly](/2_programmation/erreurs/t/this_nfss_system_is_not_set_up_properly) (1 938 octets, FR)

- Si l'on est soi-même le responsable du système, il faudra se reporter à la fin de la section 7.10.5 du *LaTeX Companion* .

### Que signifie l'erreur : « Too many columns in eqnarray environment » ?

[2_programmation:erreurs:t:too_many_columns_in_eqnarray_environment](/2_programmation/erreurs/t/too_many_columns_in_eqnarray_environment) (1 102 octets, FR)

- décrite au chapitre 8 du *LaTeX Companion* .

### Que signifie l'erreur : « Too many unprocessed floats » ?

[2_programmation:erreurs:t:too_many_unprocessed_floats](/2_programmation/erreurs/t/too_many_unprocessed_floats) (5 682 octets, FR)

- ne fera que retarder l'apparition de l'erreur. Le chapitre 6 du *LaTeX Companion* indique

### Que signifiel'erreur : « Two \\LoadClass commands » ?

[2_programmation:erreurs:t:two_loadclass_commands](/2_programmation/erreurs/t/two_loadclass_commands) (949 octets, EN)

- Un classe ne peut charger qu'une seule classe au maximum. La section A.4 du *LaTeX Companion*

### Que signifie l'erreur : « Undefined color \<nom> » ?

[2_programmation:erreurs:u:undefined_color](/2_programmation/erreurs/u/undefined_color) (1 112 octets, EN)

- Pour plus de détails, voir la référence \[60\] du *LaTeX Companion*

### Que signifie l'erreur : « UTF-8 string \\u8 : \<séquence-8-bits> not set up for LaTeX use » ?

[2_programmation:erreurs:u:utf-8_string_not_set_up_for_latex_use](/2_programmation/erreurs/u/utf-8_string_not_set_up_for_latex_use) (1 223 octets, EN)

- Voir section 7.11.3 page 451 du *LaTeX Companion* .

### Que signifie l'erreur : « \\verb illegal in command argument » ?

[2_programmation:erreurs:v:verb_illegal_in_command_argument](/2_programmation/erreurs/v/verb_illegal_in_command_argument) (1 132 octets, FR)

- à la section 3.4.3 du *LaTeX Companion* .

### Que signifie l'erreur : « You can't use \`\\hrule' here except with leaders. » ?

[2_programmation:erreurs:y:you_cannot_use_hrule_here_except_with_leaders](/2_programmation/erreurs/y/you_cannot_use_hrule_here_except_with_leaders) (823 octets, EN)

- Expliquer le message d'erreur.

### Que signifie l'erreur : « You haven't defined output directory for \`\<chemin>' » ?

[2_programmation:erreurs:y:you_have_not_defined_output_directory_for_path](/2_programmation/erreurs/y/you_have_not_defined_output_directory_for_path) (1 227 octets, FR)

- comme étudié à la section 14.2.3 page 844 du *LaTeX Companion* .

### Comment travailler comme prestataire de services LaTeX ?

[2_programmation:etre_prestataire_de_services_latex](/2_programmation/etre_prestataire_de_services_latex) (16 991 octets, FR)

- Le contenu actuel de cette page provient du [LaTeX Ninja blog](https://latex-ninja.com/2019/02/04/planning-your-project-for-service-providers/),

### Comment résoudre les incompatibilités entre extensions ?

[2_programmation:incompatibilites_entre_packages](/2_programmation/incompatibilites_entre_packages) (1 567 octets, FR)

- Utiliser [la page de Freek Dijkstra](http://www.macfreek.nl/memory/LaTeX_package_conflicts) pour compléter.

### Comment mettre des caractères autres que des lettres dans les noms de commande ?

[2_programmation:macros:caracteres_non_alphabetiques_dans_les_noms_de_macros](/2_programmation/macros/caracteres_non_alphabetiques_dans_les_noms_de_macros) (4 692 octets, FR)

- *Traduction à poursuivre.*

### Comment définir une commande étoilée ?

[2_programmation:macros:commande_etoilee](/2_programmation/macros/commande_etoilee) (1 935 octets, FR)

- Fusionner avec {doc}`Comment définir une commande étoilée ? </2_programmation/macros/commande_etoilee3>`

### Commandes et environnements

[2_programmation:macros:start](/2_programmation/macros/start) (4 537 octets, FR)

- Compléter (et sans doute réorganiser) le sommaire.
- Indiquer systématiquement les solutions avec \\NewDocumentCommand.
- fusionner avec {doc}`Comment définir une commande étoilée? </2_programmation/macros/commande_etoilee>`

### Que doit contenir un fichier source ?

[2_programmation:syntaxe:que_contient_le_fichier_source](/2_programmation/syntaxe/que_contient_le_fichier_source) (3 606 octets, FR)

- Les lettres et les transparents font appel à d'autres structures particulières. *Faire les liens vers les pages traitant de ces classes.*

## 3. Composition de documents

### Comment fusionner des entrées dans la bibliographie ?

[3_composition:annexes:bibliographie:references_biblio_multiples](/3_composition/annexes/bibliographie/references_biblio_multiples) (3 545 octets, FR)

- // Ajouter l'exemple visuel (celui de la documentation de mcite est très explicite). //

### Quelles sont les alternatives à BibTeX ?

[3_composition:annexes:bibliographie:remplacer_bibtex](/3_composition/annexes/bibliographie/remplacer_bibtex) (3 580 octets, FR)

- // Ajouter des exemples de code. //

### Comment utiliser une base de données de définitions pour mes glossaires ?

[3_composition:annexes:glossaire:stocker_un_gros_fichier_de_glossaire](/3_composition/annexes/glossaire/stocker_un_gros_fichier_de_glossaire) (1 131 octets, FR)

- Ajouter un exemple plus concret.

### Comment générer plusieurs tables des matières ?

[3_composition:annexes:tables:generer_plusieurs_tables_des_matieres](/3_composition/annexes/tables/generer_plusieurs_tables_des_matieres) (974 octets, FR)

- *Ajouter un exemple.*

### Comment marquer les modifications dans un document ?

[3_composition:document:signaler_des_changements_entre_versions](/3_composition/document/signaler_des_changements_entre_versions) (5 784 octets, FR)

- *Cette information semble concerner une version ancienne de cet éditeur...*

### Comment habiller une image ou une citation avec du texte ?

[3_composition:flottants:habiller_une_image_avec_du_texte](/3_composition/flottants/habiller_une_image_avec_du_texte) (14 885 octets, FR)

- Le contenu de cette page a besoin d'une remise à jour, les choses ayant bien évolué depuis l'article de Piet van Oostrum de 1996.

### Comment modifier la commande « \\caption » ?

[3_composition:flottants:legendes:modifier_l_apparence_des_legendes](/3_composition/flottants/legendes/modifier_l_apparence_des_legendes) (5 279 octets, FR)

- *Trouver pourquoi le code ne compile pas sur le serveur alors qu'il compile sur mon ordinateur*

### Comment gérer proprement les flottants dans LaTeX ?

[3_composition:flottants:pourquoi_faire_flotter_ses_figures_et_tableaux](/3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux) (6 330 octets, FR)

- du manuel LaTeX ( *sans doute à préciser*). Mais, dans le pire des cas, les règles de LaTeX
- La signification de ces paramètres est donnée en pages 199 à 200, section C.9 du manuel LaTeX. ( *sans doute à préciser*)

### Comment supprimer l'espace en trop autour des flottants ?

[3_composition:flottants:trop_d_espace_dans_un_flottant](/3_composition/flottants/trop_d_espace_dans_un_flottant) (2 860 octets, FR)

- Lister les principaux paramètres d'espacement autour des flottants :

### Comment faire une figure animée ?

[3_composition:illustrations:animations:figures_animees](/3_composition/illustrations/animations/figures_animees) (1 197 octets)

- - Tikz/PStricks/autre

### Comment dessiner un arbre ?

[3_composition:illustrations:construire_un_arbre](/3_composition/illustrations/construire_un_arbre) (2 533 octets, FR)

- ajouter un exemple ici

### Comment changer la forme des points avec PGFplots ?

[3_composition:illustrations:graphiques:changer_la_forme_des_points_sur_un_graphique](/3_composition/illustrations/graphiques/changer_la_forme_des_points_sur_un_graphique) (2 881 octets, FR)

- Donner un exemple simple.

### Comment inclure un graphique ?

[3_composition:illustrations:graphiques:inclure_un_graphique](/3_composition/illustrations/graphiques/inclure_un_graphique) (4 493 octets, FR)

- Ajouter un exemple.
- Proposer un exemple avec {ctanpkg}`PStricks <pstricks>`.

### Comment tracer une courbe ?

[3_composition:illustrations:graphiques:tracer_une_courbe](/3_composition/illustrations/graphiques/tracer_une_courbe) (2 924 octets)

- Parler de {ctanpkg}`PGFplots <pgfplots>`.

### Comment obtenir une césure dans un mot ou groupe de mots qui contient déjà un trait d'union ?

[3_composition:langues:cesure:permettre_la_coupure_des_mots_contenant_un_trait_d_union](/3_composition/langues/cesure/permettre_la_coupure_des_mots_contenant_un_trait_d_union) (9 453 octets, FR)

- **À intégrer :** D'autres extensions sont mentionnées

### Comment franciser un document LaTeX ?

[3_composition:langues:composer_un_document_latex_en_francais](/3_composition/langues/composer_un_document_latex_en_francais) (2 092 octets, FR)

- // Commentaire à vérifier. //

### Comment composer du texte en grec moderne ou classique ?

[3_composition:langues:composer_un_document_latex_en_grec_moderne_ou_classique](/3_composition/langues/composer_un_document_latex_en_grec_moderne_ou_classique) (6 446 octets, FR)

- À compléter

### Qu'est-ce que le codage des textes ?

[3_composition:langues:multilinguisme:codage](/3_composition/langues/multilinguisme/codage) (9 566 octets, FR)

- Actualiser (notamment sur l'encodage), réduire les exemples et documenter toutes les commandes utilisées.

### Formats TeX et LaTeX spéciaux pour certaines langues ou groupes de langues

[3_composition:langues:multilinguisme:composer_textes_multilingues](/3_composition/langues/multilinguisme/composer_textes_multilingues) (958 octets, FR)

- Indiquer ce qui a encore un intérêt.

### Comment saisir du texte dans plusieurs alphabets ?

[3_composition:langues:multilinguisme:editer_textes_multilingues](/3_composition/langues/multilinguisme/editer_textes_multilingues) (6 963 octets, FR)

- et vous référer à la documentation des extensions correspondantes. *Préciser où on peut trouver les infos*
- Il serait bon que quelqu'un de compétent vérifie et actualise si besoin.

### Comment composer des textes multilingues ?

[3_composition:langues:multilinguisme:start](/3_composition/langues/multilinguisme/start) (1 915 octets, FR)

- Section très riche, mais beaucoup trop confuse ! Elle vient d'être découpée en pages plus précises. Il reste à transformer celle-ci en page chapeau avec des liens vers les autres pages.
- Beaucoup d'informations semblent obsolètes (notamment sur l'encodage) (ça commence à s'arranger). Il faudra probablement les regrouper dans une page dédiée (type « Informations historiques ») vers laquelle celle-ci renverrait.

### Où trouver une traduction de la documentation de CTeX ? (pour le chinois)

[3_composition:langues:traduction_de_la_documentation_de_ctex](/3_composition/langues/traduction_de_la_documentation_de_ctex) (21 871 octets, FR)

```
* : À poursuivre.
```

### Comment changer le style d'une ligne entière dans un tableau ?

[3_composition:tableaux:lignes:changer_la_fonte_d_une_ligne](/3_composition/tableaux/lignes/changer_la_fonte_d_une_ligne) (2 775 octets, EN)

- L'exemple ne compile pas sur le serveur.

### Comment obtenir un soulignement de plusieurs lignes de texte ?

[3_composition:texte:lignes:texte_souligne_qui_depasse_dans_la_marge](/3_composition/texte/lignes/texte_souligne_qui_depasse_dans_la_marge) (8 729 octets, FR)

- *Des problèmes entre :ctanpkg:\`xcolor\` et :ctanpkg:\`soul\` ont été mentionnés.*
- Compiler le code de cet exemple quand le serveur aura le package {ctanpkg}`Lua-UL <lua-ul>`.

### Comment bien gérer une URL s'étendant sur deux lignes ?

[3_composition:texte:lignes:url_qui_depasse_dans_la_marge](/3_composition/texte/lignes/url_qui_depasse_dans_la_marge) (2 749 octets, FR)

- // : le contenu de cette page est à valider. Il faut voir s'il faut parler ici de l'option breaklinks et de l'option hypertex (+dvips). //

### Comment modifier les espacements dans les listes ?

[3_composition:texte:listes:ajuster_l_espacement_dans_les_listes](/3_composition/texte/listes/ajuster_l_espacement_dans_les_listes) (5 979 octets, EN)

- L'extension {ctanpkg}`expdlist`, parmi ses nombreux contrôles d'apparence des listes `description`, propose un paramètre de compactage (voir la documentation). *Sans doute à compléter.*

### Comment changer l'orientation de tout ou partie d'un document ?

[3_composition:texte:pages:changer_l_orientation_d_un_document](/3_composition/texte/pages/changer_l_orientation_d_un_document) (6 512 octets, FR)

- Le programme {ctanpkg}`DocStrip <docstrip>` pourrait le faire, bien que ce ne soit pas son but premier. *À développer.*
- Si les solutions présentées par la suite permette d'avoir un élément en orientation paysage (par rotation), aucune extension actuellement disponible ne prévoit directement la composition en orientation portrait et paysage sur la même page car TeX n'est pas pensé pour faire. Si un tel comportement était une nécessité absolue, on pourrait utiliser les techniques décrites dans la question « {doc}`Comment faire couler du texte autour d'une figure ? </3_composition/flottants/habiller_une_image_avec_du_texte>` », et faire pivoter la partie à mettre en orientation paysage en utilisant les fonctions de rotation de l'extension {ctanpkg}`graphicx`. Le retour à l'orientation portrait serait un peu plus facile : la partie portrait de la page serait un flottant de bas de page à la fin de la section paysage, avec son contenu pivoté. *Un exemple pourrait être intéressant pour illustrer ce point.*

### Comment redéfinir les marges d'un document ?

[3_composition:texte:pages:modifier_les_marges_d_un_document](/3_composition/texte/pages/modifier_les_marges_d_un_document) (3 760 octets, FR)

- Il existe également l'extension {ctanpkg}`typearea`. *À compléter.*

### Comment encadrer du texte ?

[3_composition:texte:paragraphes:encadrer_du_texte](/3_composition/texte/paragraphes/encadrer_du_texte) (11 312 octets, FR)

- Détailler {ctanpkg}`nieframe`.
- Détailler {ctanpkg}`boxedminipage`.
- Détailler {ctanpkg}`bclogo`.

### Comment griser le fond d'un paragraphe ?

[3_composition:texte:paragraphes:griser_l_arriere-plan_d_un_paragraphe](/3_composition/texte/paragraphes/griser_l_arriere-plan_d_un_paragraphe) (4 163 octets, FR)

- *Ajouter des exemples compilés sur cette extension et les suivantes.*

### Comment insérer un texte sans que LaTeX le mette en forme ?

[3_composition:texte:paragraphes:texte_verbatim](/3_composition/texte/paragraphes/texte_verbatim) (4 460 octets, FR)

- *Cette extension n'est plus disponible sur le CTAN. A-t-elle été renommée ?*

### Comment obtenir des références intégrant des noms ?

[3_composition:texte:renvois:se_referer_aux_sections_par_leur_titre](/3_composition/texte/renvois/se_referer_aux_sections_par_leur_titre) (3 181 octets, EN)

- La classe {ctanpkg}`memoir` incorpore la fonctionnalité de {ctanpkg}`titleref` mais ne fonctionne pas avec {ctanpkg}`byname <smartref>` (mais des correctifs existent). // Trouver ces correctifs. //

### Quels guillemets utiliser en français ?

[3_composition:texte:symboles:caracteres:guillemets](/3_composition/texte/symboles/caracteres/guillemets) (3 317 octets, FR)

- À rédiger. Mentionner {ctanpkg}`csquotes`.

Comment obtenir le symbole « numéro » ? ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[3_composition:texte:symboles:caracteres:numero](/3_composition/texte/symboles/caracteres/numero) (2 260 octets, FR)

- // Exemple compilable à ajouter. Mise à jour du moteur LaTeX du site à faire ?//

### Comment obtenir le symbole « radioactivité » ?

[3_composition:texte:symboles:caracteres:symbole_de_radioactivite](/3_composition/texte/symboles/caracteres/symbole_de_radioactivite) (1 208 octets, EN)

- // Restitution LaTeX à obtenir.//

### Comment obtenir des symboles astronomiques ?

[3_composition:texte:symboles:caracteres:symboles_astronomiques](/3_composition/texte/symboles/caracteres/symboles_astronomiques) (4 348 octets, FR)

- // La restitution ne se fait pas pour le moment.//

## 4. Domaines spécialisés

### Comment présenter des séquences nucléiques ou protéiques ?

[4_domaines_specialises:biologie:alignements_de_sequences](/4_domaines_specialises/biologie/alignements_de_sequences) (3 080 octets, FR)

- Donner un exemple concret, y compris la procédure d'alignement.

### Comment gérer un herbier ?

[4_domaines_specialises:biologie:herbier](/4_domaines_specialises/biologie/herbier) (1 129 octets, FR)

- Ajouter un exemple de code.

### Comment écrire des nombres en toutes lettres ?

[4_domaines_specialises:commerce:ecrire_des_nombres_en_toutes_lettres](/4_domaines_specialises/commerce/ecrire_des_nombres_en_toutes_lettres) (6 134 octets, FR)

- À rédiger.

Comment préparer une présentation (avec des « diapos ») ? ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[4_domaines_specialises:diaporama:classes_pour_des_presentations](/4_domaines_specialises/diaporama/classes_pour_des_presentations) (6 613 octets, FR)

- Il existe aussi {ctanpkg}`elpres` parmi les packages actuellement maintenus.

### Par où commencer une présentation avec « Beamer » ?

[4_domaines_specialises:diaporama:premiers_pas_avec_beamer](/4_domaines_specialises/diaporama/premiers_pas_avec_beamer) (3 183 octets, FR)

- Ajouter un exemple.
- Ajouter un exemple avec des colonnes.

### Comment constuire un arbre généalogique ?

[4_domaines_specialises:economie_domestique:construire_un_arbre_genealogique](/4_domaines_specialises/economie_domestique/construire_un_arbre_genealogique) (2 249 octets, FR)

- Ajouter un exemple.

### Comment écrire des recettes de cuisine ?

[4_domaines_specialises:economie_domestique:ecrire_des_recettes_de_cuisine](/4_domaines_specialises/economie_domestique/ecrire_des_recettes_de_cuisine) (550 octets, FR)

- Ajouter un exemple.

### LaTeX peut-il faire le café ?

[4_domaines_specialises:economie_domestique:latex_et_cafe](/4_domaines_specialises/economie_domestique/latex_et_cafe) (4 616 octets, FR)

- L'exemple n'est pas bien rendu ici, car il nécessite deux compilations pour que les nœuds Ti*k*Z soient positionnés de façon absolue sur la page.

### Comment mettre en forme un calendrier ?

[4_domaines_specialises:economie_domestique:mettre_en_page_un_calendrier](/4_domaines_specialises/economie_domestique/mettre_en_page_un_calendrier) (702 octets)

- Ajouter des exmples pour ces différents packages.

### Comment corriger automatiquement un quiz ?

[4_domaines_specialises:enseignement:corriger_automatiquement_un_quiz](/4_domaines_specialises/enseignement/corriger_automatiquement_un_quiz) (1 146 octets, FR)

- Ajouter un exemple (mais {ctanpkg}`eq-pin2corr` n'est pas disponible sur TeXlive 20017).

### Comment mettre en page des exercices dont les solutions sont reportées dans un autre paragraphe ?

[4_domaines_specialises:enseignement:document_questions_reponses](/4_domaines_specialises/enseignement/document_questions_reponses) (3 303 octets, FR)

- Ajouter un exemple d'utilisation.

### Comment écrire sur des grands carreaux ?

[4_domaines_specialises:enseignement:reglure_a_grands_carreaux](/4_domaines_specialises/enseignement/reglure_a_grands_carreaux) (2 162 octets, FR)

- - À rédiger.

### Comment mettre en forme du code informatique ?

[4_domaines_specialises:informatique:inserer_du_code_source_informatique](/4_domaines_specialises/informatique/inserer_du_code_source_informatique) (13 191 octets, FR)

- Un exemple d'utilisation est donné sur
- Est-il intéressant de parler de cvt2ltx ? Je n'ai même pas réussi à le compiler.

### Comment aligner des nombres sur le séparateur décimal ?

[4_domaines_specialises:mathematiques:aligner_des_nombres_sur_la_virgule](/4_domaines_specialises/mathematiques/aligner_des_nombres_sur_la_virgule) (2 103 octets, FR)

- Toujours dans un tableau, en dehors du mode mathématique, l'extension {ctanpkg}`siunitx` (bien utile dés qu'on gère des nombre et des unités) met à disposition le style de colonne "S", aligné sur le marqueur décimal mais aussi les milliers (espace en français, virgule en anglais), comme le montre l'exemple suivant.( *L'exemple devrait être traité avec la virgule. Cependant l'option de siunitx « output-decimal-marker={,} » ne fonctionne pas pour le moment.*)
- Ajouter un exemple.

### Mathématiques

[4_domaines_specialises:mathematiques:start](/4_domaines_specialises/mathematiques/start) (2 621 octets, FR)

- // Tri à faire. //

### Fractions

[4_domaines_specialises:mathematiques:structures:fractions:start](/4_domaines_specialises/mathematiques/structures/fractions/start) (630 octets, FR)

- // Ajouter une page sur les fractions continues. //

### Comment mettre en page des théorèmes ?

[4_domaines_specialises:mathematiques:structures:mettre_en_page_des_theoremes](/4_domaines_specialises/mathematiques/structures/mettre_en_page_des_theoremes) (6 769 octets, FR)

- // Présenter amsthm. //
- *Revoir le tableau du fait des autres extensions évoquées.*

### Comment noter un vecteur ?

[4_domaines_specialises:mathematiques:structures:noter_des_vecteurs_avec_une_fleche](/4_domaines_specialises/mathematiques/structures/noter_des_vecteurs_avec_une_fleche) (2 648 octets, FR)

- L'exemple ne compile pas (pour l'instant), car l'extension {ctanpkg}`vector` doit être installée manuellement.

### Comment obtenir des flèches ?

[4_domaines_specialises:mathematiques:symboles:fleches:obtenir_des_fleches](/4_domaines_specialises/mathematiques/symboles/fleches/obtenir_des_fleches) (4 210 octets, FR)

- // À compléter avec `\looparrowleft` et `\looparrowright` qui ne s'affichent pas.//

### Comment écrire les symboles d'ensembles ?

[4_domaines_specialises:mathematiques:symboles:symboles_d_ensembles](/4_domaines_specialises/mathematiques/symboles/symboles_d_ensembles) (6 265 octets, FR)

- \* : la restitution LaTeX ne se fait pas.\*

### Comment représenter le système solaire

[4_domaines_specialises:physique:representer_le_systeme_solaire](/4_domaines_specialises/physique/representer_le_systeme_solaire) (786 octets, FR)

- L'exemple ne compile pas sur le site de la FAQ

### Je commence une thèse en sciences humaines. Est-ce que LaTeX est fait pour moi ?

[4_domaines_specialises:sciences_humaines:these_en_sciences_humaines](/4_domaines_specialises/sciences_humaines/these_en_sciences_humaines) (2 760 octets, FR)

- Ajouter du texte de description de chaque référence.

## 5. Fichiers utilisés

### Other conversions to and from (La)TeX

[5_fichiers:conversions:autres_outils_de_conversion_de_fichiers_latex](/5_fichiers/conversions/autres_outils_de_conversion_de_fichiers_latex) (6 458 octets, EN)

- If it's related to the paper [Difficulties in parsing SGML](https://dl.acm.org/doi/10.1145/62506.62520), that may be very old news.

### Comment trouver de nouvelles fontes ?

[5_fichiers:fontes:comment_trouver_une_fonte](/5_fichiers/fontes/comment_trouver_une_fonte) (3 016 octets, EN)

- Si vous n'utilisez pas LuaTeX ou XeTeX...

### Que sont les « résolutions » ?

[5_fichiers:fontes:peut_on_parler_de_resolution](/5_fichiers/fontes/peut_on_parler_de_resolution) (3 130 octets, FR)

- *Traduire*

### Fontes

[5_fichiers:fontes:start](/5_fichiers/fontes/start) (2 572 octets, FR)

- Pour pdflatex, Compléter, plize, je suis trop jeune pour comprendre !

### Comment utiliser une police ?

[5_fichiers:fontes:utiliser_une_police](/5_fichiers/fontes/utiliser_une_police) (2 569 octets, FR)

- Faire que les exemples compilent (ils fonctionnent si on les compile sur sa machine)
- Donner des exemples

### Quelles sont les extensions de noms de fichiers utilisées par LaTeX ?

[5_fichiers:formats:liste_des_extensions_de_fichiers](/5_fichiers/formats/liste_des_extensions_de_fichiers) (9 035 octets, EN)

- \* **FD** : fichier de définition de fonte. Il sert à générer le document en sortie. voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».
- \* **MF** : voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».
- \* **VF** : voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».
- *Source :* [File extensions related to LaTeX, etc.](https://tex.stackexchange.com/questions/7770/file-extensions-related-to-latex-etc) // À utiliser pour compléter la page.//

### Comment corriger les réglages d'Adobe Reader ?

[5_fichiers:pdf:imprimer_a_la_bonne_taille_avec_adobe_reader](/5_fichiers/pdf/imprimer_a_la_bonne_taille_avec_adobe_reader) (1 861 octets, FR)

- *La traduction utilise sans doute une terminologie incorrecte pour les noms des fenêtres et des boîtes de dialogues.*

### Comment sont gérés les fichiers Postscript dans LaTeX ?

[5_fichiers:postscript:support_du_postscript_dans_latex](/5_fichiers/postscript/support_du_postscript_dans_latex) (2 840 octets, EN)

- Est-ce toujours vrai ?

### Which tree to use ?

[5_fichiers:tds:la_tds3](/5_fichiers/tds/la_tds3) (3 932 octets, EN)

- Ce n'est plus vrai, MiKTeX propose maintenant de s'installer sous `C:\Users\...\AppData\Local\Programs`.

### Comment convertir du WEB en LaTeX ?

[5_fichiers:web:convertir_du_web_en_latex](/5_fichiers/web/convertir_du_web_en_latex) (642 octets, EN)

- // à rédiger //

### Langage WEB

[5_fichiers:web:start](/5_fichiers/web/start) (583 octets, FR)

- \* {doc}`Comment convertir du WEB en LaTeX ? </5_fichiers/web/convertir_du_web_en_latex>` // à rédiger //

### Conversion from (La)TeX to HTML

[5_fichiers:xml:convertir_du_latex_en_html](/5_fichiers/xml/convertir_du_latex_en_html) (5 082 octets, EN)

- \* [Pandoc](https://pandoc.org/)

## 6. Distributions et logiciels

### Existe-t-il des alternatives à TeX ?

[6_distributions:annexes:alternatives_a_tex](/6_distributions/annexes/alternatives_a_tex) (7 525 octets, FR)

- Préciser clarifier la structure de la chaîne de traitement, car le paragraphe précédent est confus.

### Quels sont les logiciels permettant de créer une bibliographie ?

[6_distributions:annexes:logiciels_pour_generer_une_bibliographie](/6_distributions/annexes/logiciels_pour_generer_une_bibliographie) (3 264 octets, FR)

- Mettre à jour en parlant de BibLaTeX.

### Logiciels de conversion de formats graphiques

[6_distributions:conversion:logiciels_de_conversion_de_formats_d_images](/6_distributions/conversion/logiciels_de_conversion_de_formats_d_images) (11 917 octets, FR)

- ajouter des exemples de lignes de commande.

### Logiciels de conversion de formats de texte

[6_distributions:conversion:logiciels_de_conversion_de_formats_de_texte](/6_distributions/conversion/logiciels_de_conversion_de_formats_de_texte) (21 825 octets, FR)

```
*
```

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision. \* Apparemment plus disponible.
```

- Non disponible sur le CTAN au 19/09/2020.
- Non disponible sur le CTAN au 19/09/2020.

### Quels sont les correcteurs orthographiques utilisables pour TeX/LaTeX ?

[6_distributions:editeurs:correcteurs_orthographiques](/6_distributions/editeurs/correcteurs_orthographiques) (7 964 octets, FR)

- *Page sans doute à reprendre posément car les différentes solutions (programmes autonomes, correcteurs intégrables à des éditeurs...) s'y mélangent.*

### Quels sont les éditeurs utilisables sous Mac OS ?

[6_distributions:editeurs:editeurs_pour_macos](/6_distributions/editeurs/editeurs_pour_macos) (3 657 octets, FR)

- *Page sans doute à revoir car certaines références semblent datées.*

### Comment visualiser des fichiers DVI ?

[6_distributions:visualisateurs:visualisateurs_dvi](/6_distributions/visualisateurs/visualisateurs_dvi) (3 569 octets, FR)

- *À compléter.*

### Comment visualiser des fichiers PDF ?

[6_distributions:visualisateurs:visualisateurs_pdf](/6_distributions/visualisateurs/visualisateurs_pdf) (5 061 octets, EN)

- *À compléter et détailler.*
- Le rendu avec des fontes issues de Metafont n'est pas terrible ( Est-ce toujours le cas?).

### Comment visualiser des fichiers Postscript ?

[6_distributions:visualisateurs:visualisateurs_postscript](/6_distributions/visualisateurs/visualisateurs_postscript) (2 411 octets, FR)

- // Il y a sans doute des solutions plus actuelles.//

## wiki

### Syntaxe de mise en page

[wiki:syntax](/wiki/syntax) (27 887 octets, FR)

- \* %% %%

```{eval-rst}
.. meta::
   :keywords: questions en attente,questions à corriger
```


