# Que contient cette FAQ ?

Cette FAQ contient :

- les réponses les plus complètes et les plus à jour possible aux questions générales et répétitives sur TeX, LaTeX, leurs acolytes, leurs convertisseurs et autres ;
- des approches thématiques, pas si souvent demandées, mais qui semblent être des sources d'informations qui feraient du bien à tout le monde. Après tout, peu de gens se demandent si le gras est plus intéressant que l'italique pour tel ou tel usage, et pourtant, ils feraient mieux de se poser la question, ils feraient moins de bêtises ;
- des réponses à des questions moins fréquentes mais non triviales et dont il vaut mieux garder une trace, au cas où ;
- des réponses anciennes ou obsolètes, pour garder une trace de ces réponses historiques en précisant à l'occasion leur caractère {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>` et l'existence de solutions plus pertinentes.

Mais, plus encore, cette FAQ ne contient pas :

- une version rédigée de votre thèse ou votre prochain article (bon courage à vous tout de même pour le rédiger) ;
- cette fichue réponse à cette satanée question que vous vous posez tous les matins face à votre miroir et que vous n'avez jamais eu le courage de creuser ou de poser à quelqu'un qui pourrait vous aider (le site [TeXnique.fr](https://texnique.fr/osqa/) peut ici vous aider). Si vous ne posez jamais la question, vous n'aurez jamais la réponse ;
- cette réponse passionnante à cette question récurrente, mais toujours ouverte, réponse que vous détenez sans avoir pensé à la mettre à la disposition de tous.

Si, au final, cela vous tracasse ou que vous constatez des manques, la solution
est de [contribuer vous aussi à cette
FAQ](/0_cette_faq/comment_contribuer_a_la_faq.md).

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation LaTeX,questions fréquemment posées,foire aux questions
```


