grep "documents à écrire : " log.txt | sed 's/documents à écrire : //g' | sed 's/, /\n/g' >  changed-source-files.txt
grep "docnames to write: "   log.txt | sed 's/docnames to write: //g'   | sed 's/, /\n/g' >> changed-source-files.txt

while IFS= read -r filename; do
    file="_build/html/$filename.html"
    echo "tex-logos-in-html.bin applied to $file"
    ./tex-logos-in-html.bin "$file" > tmp.html && mv -f tmp.html "$file";
done < changed-source-files.txt

# rm -f changed-source-files.txt log.txt
