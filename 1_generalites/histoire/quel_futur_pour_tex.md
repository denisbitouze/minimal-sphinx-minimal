# Quel est l'avenir de TeX ?

Donald Knuth a déclaré qu'il ne développe plus TeX : il ne traite désormais plus que la correction des erreurs qui lui sont remontées (et ces bugs sont rares). Cette décision fut prise peu après la publication de la version 3.0 de TeX. Depuis, à chaque correction d'erreur [^footnote-1], le numéro de version gagne une décimale, de manière à ce que ce numéro tende vers \$\\pi\$. Il vaut actuellement 3.141592653, ceci depuis janvier 2021 (voir l'[article de Donald Knuth](https://tug.org/TUGboat/tb42-1/tb130knuth-tuneup21.pdf) traduit en français dans la [Lettre Gutenberg n°43](https://www.gutenberg.eu.org/IMG/pdf/lettre43-ecran.pdf)), sachant que la version 3.14159265 datait de [janvier 2014](https://tug.org/TUGboat/tb35-1/tb109knut.pdf). Knuth a demandé à ce que TeX soit figé au numéro de version \$\\pi\$ lorsqu'il décèdera et qu'aucune autre modification ne soit faite sur le code source par la suite.

Knuth explique cette décision, et nous demande à tous de la respecter, dans un article publié initialement dans le *TUGboat* ([volume 11, numéro 4](https://tug.org/TUGboat/Articles/tb11-4/tb30knut.pdf)) et réédité dans le [journal MAPS du NTG](http://www.ntg.nl/maps/pdf/5_34.pdf). Il est fermement convaincu que la stabilité du moteur de TeX est plus importante que l'ajout de nouvelles fonctionnalités. Les éventuelles corrections de bug pourront se faire par la suite en entrée ou en sortie de TeX.

Cela n'empêche pas qu'il existe des projets de développements très innovants à partir de TeX : le plus remarquable est le {doc}`LaTeX Project </1_generalites/histoire/c_est_quoi_latex3>`. D'autres projets visent à construire un successeur au programme TeX. Le {doc}`succès de ces projets est variable </1_generalites/histoire/developpement_du_moteur_tex>` : aujourd'hui {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>`'' est le moteur standard pour la plupart des utilisateurs et `XeTeX` comme ' {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` ont tous deux abouti à des systèmes compatibles avec l'Unicode, en gardant des approches différentes sur l'extensibilité.

______________________________________________________________________

*Sources :*

- {faquk}`What is the future of TeX? <FAQ-TeXfuture>`
- [How to find out and interpret the LaTeX version number?](https://tex.stackexchange.com/questions/366586/how-to-find-out-and-interpret-the-latex-version-number)

```{eval-rst}
.. meta::
   :keywords: LaTeX,Tex,Knuth,avenir,développement,évolutions futures
```

[^footnote-1]: Une logique similaire s'applique à Metafont dont le numéro de version tend vers \$e\$ (autre constante mathématique, qui [sert de base au logarithme naturel](<https://fr.wikipedia.org/wiki/E_(nombre)>)) et vaut 2.71828182 depuis janvier 2021.

