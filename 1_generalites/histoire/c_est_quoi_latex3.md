# Que sont LaTeX3 et le « LaTeX Project » ?

## LaTeX3

LaTeX est pour le moment un nom de projet visant à définir puis à réaliser une version plus avancée de LaTeX. Dans ses versions actuelles, LaTeX souffre de quelques défauts majeurs, qui devraient se déplacer ou disparaître avec LaTeX. En voici quelques exemples :

- les limites à l'écriture de nouvelles macros. Certaines syntaxes étaient facilement accessibles, par le fonctionnement normal de `\newcommand` : un nombre quelconque d'arguments obligatoires et, si besoin, un argument optionnel. Par contre, les autres syntaxes, comme celle de `\parbox` (un argument obligatoire, la largeur, trois arguments de positionnements optionnels entre crochets, puis un dernier obligatoire pour le texte) ou comme celle des objets de l'environnement `picture` (coordonnées entre parenthèses séparées par une virgule), ne pouvaient pas être définies sans être un minimum gourou ;
- la routine de sortie (ou *output*), qui se charge de composer la page finale en faisant le collage des différents morceaux (en-têtes, pieds de page, notes, marges, colonnes, flottants, etc) est par beaucoup d'aspects trop limitée ;
- la différenciation fond/forme. Si beaucoup de commandes, comme `\chapter`, font bien ce qu'on attend d'elles, il en reste trop qui sont liées à la forme plus qu'au fond.

Certains des plus anciens documents de discussion sur les directions à prendre pour LaTeX peuvent être trouvées sur le CTAN ; d'autres articles (publiés) peuvent être trouvés sur le [site web du projet](https://www.latex-project.org/publications/), en particulier le document [The LaTeX3 Project](https://www.latex-project.org/help/documentation/ltx3info.pdf) de Frank Mittelbach et Chris Rowley.

Les codes informatiques liés à ce projet sont disponibles sur le [site Github du projet](https://github.com/latex3/) :

- [Le noyau LaTeX2e](https://github.com/latex3/latex2e) ;
- [Le dépôt de développement de LaTeX3](https://github.com/latex3/latex3).

Du code versionné par l'équipe est aussi disponible sur le CTAN, tout particulièrement :

- {ctanpkg}`l3kernel` : les conventions et éléments de programmation pour les travaux LaTeX (le langage `expl3`) ;
- {ctanpkg}`l3packages` : des extensions stables utilisant les idées de LaTeX ;
- {ctanpkg}`l3experimental` : du code plus expérimental.

Toute personne peut participer aux discussions sur le futur de LaTeX par le biais de la liste de discussion `LaTeX-L` ; certains travaux de développement (hors du projet) sont discussés sur cette liste. La souscription à la liste se fait en envoyant un message `subscribe latex-l ⟨votre_nom⟩` à l'adresse <mailto:listserv@urz.Uni-Heidelberg.de>.

Par ailleurs, si LaTeX n'existe pas en tant que tel, il existe LaTeXplus, qui est la somme d'un certain nombre d'extensions pour :latexlogo:`LaTeX` qui, mis bout à bout, font ce qu'on peut avoir de mieux à l'heure actuelle comme approximation d'une prévision de ce à quoi pourrait un jour, peut-être, ressembler LaTeX... Bien entendu, ces différents morceaux ne sont pas toujours compatibles entre eux, ni complets, ni forcément documentés. Et quand il y a une documentation, elle n'est a priori pas à jour. Et quand les morceaux sont complets, ils sont a priori buggés. Sinon, ce serait trop simple. Bref, c'est fait pour s'amuser, pour le moment.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *manque de références sur ce paragraphe.*) »
```

## Le « LaTeX Project »

Le [LaTeX Project](https://www.latex-project.org/latex3/) (projet LaTeX) ou *LaTeX3 Project* est une petite équipe de bénévoles dont le but est de produire LaTeX.

Le premier produit livré en 1994 par cette équipe fut LaTeX : il est actuellement dénommé « LaTeX » en l'absence d'autres versions. LaTeX fut pensé comme un exercice de consolidation, unifiant plusieurs sous-variantes de LaTeX en ne changeant que ce qui était absolument nécessaire. Ceci a permis à l'équipe de n'avoir à maintenir qu'une unique version de LaTeX, en parallèle du développement de LaTeX.

Toutes les informations sur cette équipe sur retrouvent sur le [site web du projet](https://www.latex-project.org/).

______________________________________________________________________

*Source :* {faquk}`The LaTeX project <FAQ-LaTeX3>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex3,latex project
```

