# Où trouver le manuel PicTeX ?

PicTeX est un ensemble de commandes de Michael Wichura pour dessiner des diagrammes et des images. Si les commandes sont disponibles gratuitement, le manuel PicTeX lui-même n'est pas gratuit. Il est désormais uniquement disponible uniquement via [Personal TeX Inc](https://pctex.com/), les fournisseurs de PCTeX. Le manuel n'est *pas* disponible électroniquement.

Cependant, il existe un {ctanpkg}`résumé des commandes PicTeX <pictex>`, excellent aide-mémoire pour ceux qui connaissent cette extension.

______________________________________________________________________

*Source :* {faquk}`The PicTeX manual <FAQ-docpictex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,dessin,picTeX
```

