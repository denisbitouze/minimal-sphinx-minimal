# Où trouver la documentation des extensions ?

Cette FAQ vous suggère régulièrement des extensions qui pourront résoudre des problèmes particuliers. Dans certains cas, la réponse fournit toute la méthode pour y arriver. Dans d'autres (souvent ceux où la solution s'avère très spécifique), comment pouvez-vous vous y prendre ? Le recours à la documentation sera nécessaire et voici les différentes méthodes de la plus générale à la moins conseillée pour trouver cette documentation.

Avant de commencer, il faut noter que les distributions elles-même sont aussi accompagnées de leur propre documentation, décrivant l'organisation des fichiers, les outils de bases, variables d'environnement, etc. Cette documentation se trouve sous plusieurs formes et/ou langues, dans des chemins qui devraient ressembler aux suivants :

```
Program Files/MiKTeX 2.5/doc/miktex/manual/index.html
Program Files/MiKTeX 2.5/doc/miktex/miktex.pdf
/usr/share/doc/texlive-doc/french/texlive-fr/live.html
/usr/share/doc/texlive-doc/english/texlive-en/live.pdf
```

## Avec texdoc ou mthelp

Si vous avez de la chance, l'extension recommandée est déjà dans votre installation. Et, si vous êtes particulièrement chanceux, votre distribution vous donne accès à la documentation de l'extension dans un format courant et facilement lisible.

Sur les distributions basées sur TeX Live, l'aide devrait être disponible en ligne de commande avec `texdoc`, comme dans :

```latex
% !TEX noedit
texdoc footmisc
```

Ceci ouvre une fenêtre montrant la {texdoc}`documentation <footmisc>` de l'extension {ctanpkg}`footmisc`.

Si `texdoc` ne trouve aucune documentation, il peut lancer un navigateur web pour consulter l'entrée de l'extension dans le catalogue CTAN. La plupart des auteurs répondent à la demande de l'équipe CTAN pour mettre à disposition la documentation des extensions. C'est de cette manière que vous trouverez le plus souvent la documentation.

Sur les systèmes MiKTeX, la même fonction est fournie par `mthelp`.

:::{note}
site [texdoc.net](http://texdoc.net) donne accès à la documentation que vous *auriez* si vous aviez *une* installation complète de TeX Live. Sur le site, vous pouvez simplement demander une extension (comme vous le demanderiez à `texdoc`) ou vous pouvez utiliser l'index de la documentation du site pour faire vos recherches.
:::

## Avec le fichier dtx

La forme la plus courante de documentation des extensions LaTeX se trouve dans le fichier `dtx` avec lequel le code est distribué (voir la question « {doc}`Que sont les fichiers de source documentée (ou fichiers « dtx ») ? </1_generalites/documentation/documents/documents_extensions/fichiers_sources_dtx>` »). De tels fichiers sont censés être traités par LaTeX lui-même mais il peut y avoir des problèmes occasionnels pour obtenir une documentation lisible. Les problèmes courants sont que l'extension elle-même est nécessaire pour traiter sa propre documentation (elle doit donc être décompressée avant le traitement) ou que le fichier `dtx` ne sera pas traité avec LaTeX. Dans ce dernier cas, le fichier `ins` produira généralement un fichier `drv` (ou de nom similaire), que vous traitez à la place avec LaTeX (il arrive que l'auteur de l'extension pense à préciser cela dans un fichier `README`.)

Pour plus d'information sur ce point, lisez la question : « {doc}`Comment générer la documentation d'une extension ? </1_generalites/documentation/documents/documents_extensions/documentation_des_packages3>` ».

## Avec la documentation séparée

Un autre forme courante de documentation se présente sous forme de fichier séparé. Cela est parfois le cas si une extension est riche conceptuellement. Une vaste documentation peut devenir encombrante dans un fichier `dtx`. En voici quelques exemples :

- la classe {ctanpkg}`memoir` ;
- les classes {ctanpkg}`KOMA-script` dont les développeurs prennent la peine de produire une documentation détaillée en allemand et en anglais ;
- l'extension {ctanpkg}`pgf` associée à Ti*k*Z. Sa documentation pourrait représenter un livre conséquent ;
- l'extension {ctanpkg}`fancyhdr` dont la documentation dérive d'un tutoriel dans une revue mathématique.

Dans ce cas, même si la documentation n'est pas identifiée séparément dans un fichier `README`, il ne devrait pas être trop difficile de la reconnaître.

Pour les francophones, certains documents ont fait l'objet de traduction. Ils se retrouvent alors souvent dans le même dossier CTAN que l'extension mais ce n'est pas systématique. Un exemple important est celui du projet [french translations](https://www.ctan.org/info/french-translations) a mis dans un dossier dédié du CTAN différentes traductions de documentation.

## Avec la documentation par commentaire dans le code de l'extension

La documentation dans l'extension peut prendre une autre forme en s'intégrant sous forme de commentaire dans le code de l'extension. Une telle documentation apparaît habituellement en tête du fichier. Un auteur plus fûté la placera après la commande `\endinput` de l'extension : en effet, cette commande marque la fin logique du fichier et LaTeX ne lit plus loin. Ainsi, une telle documentation ne coûte aucun temps de chargement.

## Avec l'appel à des amis

Si, malgré tous vos efforts, vous ne trouvez rien, il est bien possible que l'auteur ne se soit pas donné la peine de documenter son extension (éventuellement sur la base du sophisme « ce qui a été dur à coder doit être dur à comprendre »). Vous serez amené soit à vous tourner vers un utilisateur plus expérimenté, soit vers les différentes {doc}`communautés d'utilisateurs en ligne </1_generalites/documentation/listes_de_discussion>`.

## Avec... la lecture du code

Si vous arrivez ici, la situation est d'une rare gravité : vous devrez en effet lire le code de l'extension lui-même. Bon courage à vous !

______________________________________________________________________

*Sources :*

- {faquk}`Documentation of packages <FAQ-pkgdoc>`
- <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#texdoc>

. .. meta::
: ```{eval-rst}

  :keywords: LaTeX,documentation
  ```

