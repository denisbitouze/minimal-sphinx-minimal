# Comment générer la documentation d'une extension ?

En général, la documentation d'une extension est mise à disposition directement sur le [CTAN](https://www.ctan.org/) au format PDF.

Cependant, si vous trouvez une extension qui n'offre pas de documentation sur le CTAN, ou si vous avez besoin de la documentation dans un format autre que celui proposé, vous pouvez le plus souvent générer la documentation vous-même à partir de ce que vous téléchargez sur le CTAN.

## Avec le fichier dtx

Le mécanisme standard consiste simplement à exécuter LaTeX sur le {doc}`fichier de source documentée </1_generalites/documentation/documents/documents_extensions/fichiers_sources_dtx>` `dtx`, comme vous le feriez pour n'importe quel fichier LaTeX ordinaire (c'est-à-dire à plusieurs reprises jusqu'à ce que les avertissements disparaissent).

Parfois, lors de l'exécution, LaTeX se plaindra de ne pas trouver un fichier `ind` (l'index des lignes de code) et/ou un fichier `gls` (l'historique des changements et non, comme on aurait pu l'imaginer, un glossaire). Les deux types de fichiers sont traités avec des programmes spéciaux comme `makeindex`. Les commandes appropriées sont :

```latex
% !TEX noedit
makeindex -s gind extension
makeindex -s gglo -o extension.gls extension.glo
```

## Avec le fichier drv

Il se peut que, parmi les fichiers d'une extension, vous trouviez un fichier `drv`. Dans ce cas, traitez-le de préférence au fichier `dtx`. En effet, ce format `drv` aurait été une des possibilités envisagées lors des discussions sur le mécanisme source documentée de LaTeX mais il n'est pas beaucoup utilisé de nos jours.

## Avec le fichier tex annexe

Une autre pratique courante des auteurs d'extension consiste à fournir un fichier séparé nommé `extension-doc.tex` ou même simplement `manual.tex`. Si le fichier `dtx` n'aide pas, cherchez ce type d'alternatives. Ces fichiers se traitent de la même manière que n'importe quel fichier LaTeX classique.

______________________________________________________________________

*Source :* {faquk}`Generating package documentation <FAQ-install-doc>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installing
```

