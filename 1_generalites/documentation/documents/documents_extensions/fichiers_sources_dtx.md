# Que sont les fichiers de source documentée (ou fichiers « dtx ») ?

LaTeX et nombre d'extensions associées sont écrits sous forme de {doc}`programmation lettrée </5_fichiers/web/literate_programming>`, avec les sources et leur documentation placées dans le même fichier. Ce format est en fait né avant l'époque de LaTeX et des {doc}`travaux du projet LaTeX </1_generalites/glossaire/qu_est_ce_que_latex2e>`.

Un fichier de source documentée présente normalement le suffixe `dtx`.

## Utilisation des fichiers de source documentée

### Obtention de la source seule

Pour obtenir la source sans les commentaires, le fichier `dtx` doit est débarassé des commentaires avant d'être utilisé avec LaTeX. Un fichier d'installation (`ins`) est normalement fourni pour automatiser ce processus de suppression des commentaires afin d'augmenter la vitesse de chargement. Si le fichier `ins` est disponible, vous pouvez *le traiter* avec LaTeX pour produire l'extension (et, souvent, les fichiers auxiliaires).

Ce traitement devrait donner des résultats ressemblant à quelque chose comme ceci :

```latex
% !TEX noedit
Generating file(s) ./extension.sty

Processing file extension.dtx (package) -> extension.sty
File extension.dtx ended by \endinput.
Lines  processed : 2336
Comments removed : 1336
Comments  passed : 2
Codelines passed : 972
```

Les lignes « `Processing ... ended by \endinput` » (soit « Traitement ... achevé par \\endinput ») sont répétées autant de fois que le fichier `dtx` fournit de fichiers par ce mécanisme.

### Obtention de la documentation seule

Pour lire les commentaires sous forme de document mis en forme, vous pouvez exécuter LaTeX sur le fichier `dtx`. La plupart des extensions sur le CTAN fournissent déjà le document PDF résultant de ce traitement du fichier `dtx`, comme documentation.

Plusieurs extensions peuvent être incluses dans un fichier `dtx`, avec d'éventuelles parties conditionnelles. De plus, il existe des fonctionnalités pour indexer les commandes. Tout cet ensemble est trié par des directives du fichier `ins`. Des utilitaires d'indexation peuvent être nécessaires pour une sortie « complète ».

## Création de fichiers de source documentée

N'importe qui peut écrire des fichiers `dtx`. Ce format est expliqué dans le {doc}`LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>` et il existe un {ctanpkg}`tutoriel <dtxtut>` en anglais avec des fichiers `dtx` et `ins` minimaux. Certains éditeurs, comme Emacs avec {doc}`AUC-TeX </6_distributions/editeurs/introduction2>`, facilite la rédaction de ce type de fichier.

### Avec le script dtxgen

Le script Unix {ctanpkg}`dtxgen` génère un fichier basique `dtx` qui peut être utile lors du démarrage d'un nouveau projet.

### Avec le script makedtx

Une autre manière d'obtenir un fichier `dtx` consiste à écrire la documentation et le code séparément, puis à les combiner en utilisant le script Perl {ctanpkg}`makedtx`. Cette technique est intéressante car le fichier de documentation peut être utilisé séparément pour générer une sortie HTML. Il est souvent assez difficile de faire en sorte que les outils de conversion de code {doc}`LaTeX en HTML </5_fichiers/xml/convertir_du_latex_en_html>` traitent des fichiers `dtx` dans la mesure où ils utilisent un fichier de classe inhabituel.

### Avec le script sty2dtx

Le script Perl {ctanpkg}`sty2dtx` va encore plus loin : il tente de créer un fichier `dtx` à partir d'un fichier `sty` classique avec des commentaires. L'outil fonctionne bien mais, dans certaines circonstances, il peut être dérouté par des commentaires qui cherchent à structurer le document (par exemple, du matériel sous forme tabulaire, comme dans les en-têtes de fichiers de nombreuses anciennes extensions).

## Compléments

Les fichiers `dtx` ne sont plus utilisés par LaTeX après avoir été traités pour produire des fichiers `sty` ou `cls` (ou autre). Ils n'ont donc pas besoin d'être conservés avec le système de travail. Cependant, pour de nombreuses extesnions, le fichier `dtx` est la principale source de documentation, vous pouvez donc vouloir conserver les fichiers `dtx` ailleurs.

Il est à noter que l'extension {ctanpkg}`docmfp`, en étendant le modèle de l'extension {ctanpkg}`doc` à {doc}`MetaFont </1_generalites/glossaire/qu_est_ce_que_metafont>` et {doc}`MetaPost </1_generalites/glossaire/qu_est_ce_que_metapost>`, permet la distribution documentée d'extensions contenant du code pour MetaFont et MetaPost avec le code LaTeX associé.

______________________________________________________________________

*Source :* {faquk}`Documented LaTeX sources (`dtx\` files) <FAQ-dtx>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,concepts,fichier dtx,fichier ins,dtx,ins,source documentée,makedtx,dtxgen,sty2dtx
```

