# Que lire sur la typographie ?

Voici quelques documents traitant de typographie. Vous pouvez trouver par ailleurs sur le sujet une sélection de {doc}`livres </1_generalites/documentation/livres/documents_sur_la_typographie>` et de {doc}`sites web </1_generalites/documentation/sites/sites_de_typographie>`.

## En français

- Jacques André, [Petites leçons de typographie](https://jacques-andre.fr/faqtypo/lessons.pdf),
- Jean-Pierre Lacroux, [Orthotypographie](http://www.orthotypographie.fr/) (en deux tomes),
- Eddie Saudrais, [Le petit typographe rationnel](http://tex.loria.fr/typographie/saudrais-typo.pdf).

## En anglais

- Peter Wilson, {ctanpkg}`A Few Notes on Book Design <memdesign>`. Ce manuel comporte une longue introduction sur les considérations typographiques et constitue une solide base, rédigé par un auteur conscient des problèmes qui se posent aux utilisateurs de (La)TeX. Historiquement, ce document faisait partie de la {texdoc}`documentation <memoir>` de la classe {ctanpkg}`memoir` du même auteur. Il existe un [projet de traduction en français](https://github.com/jejust/memoir-fr/tree/main/memdesign-fr), auquel vous pouvez contribuer.
- Charles Hedrick, [Guidelines for Typography in NBCS](https://cgvr.cs.uni-bremen.de/links/typography.pdf) (New Brunswick Computer Services). Ce petit manuel se veut être une *checklist* sur la présentation d'un document. Une [image archivée du site de l'auteur](https://web.archive.org/web/20160422065608/http://www.nbcs.rutgers.edu/~hedrick/typography/index.html) vaut également la peine d'être lue : elle fournit entre autres des versions de ce document dans quatorze polices différentes, à des fins de comparaison, quand bien même l'auteur prend soin d'expliquer qu'il n'a pas l'ambition de supplanter des livres aussi excellents que {doc}`celui de Robert Bringhurst </1_generalites/documentation/livres/documents_sur_la_typographie>`.

Un [Vocabulaire des industries graphiques/Graphic Arts Vocabulary](http://publications.gc.ca/collections/collection_2014/tpsgc-pwgsc/S52-2-210-1993.pdf) a été édité par le Secrétariat d'État du Canada et le groupe Communication Canada en 1993 (ISBN : {isbn}`0-660-58025-X`). C'est un dictionnaire spécialisé bilingue anglais/français qui vous sera très utile pour comprendre les termes techniques utilisés dans les documents en anglais.

Enfin, le [PrintWiki](http://printwiki.org/) est une encyclopédie libre autour la filière graphique. Ce wiki s'efforce de fournir une base de connaissances complète et libre sur l'imprimerie et l'industrie de la communication graphique. Il contient plus de 7 500 articles provenant principalement de l'*Encyclopedia of Graphic Communications*, de Richard Romano ({isbn}`978-0130964229`).

## Jeux pédagogiques

- Si vous souhaitez expérimenter avec le concept de crénage, essayez le site web [Kerntype](https://type.method.ac/#). Avec des exercices à faire à l'écran, il vous permettra d'entraîner votre œil sur diverses polices de caractères.

______________________________________________________________________

*Sources :*

- {faquk}`Typography tutorials <FAQ-typo-style>`,
- [Documents sur la typographie](https://www.gutenberg.eu.org/Typographie).

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,orthotypographie,considérations typographiques,tutoriels,mise en page,composition de documents,LaTeX et typographie,imprimerie,impression au plomb
```

