# Que lire sur les fontes ?

## En français

```{eval-rst}
.. todo:: Ajouter des références sur les fontes en français.
```

## En anglais

- Il existe une FAQ `comp.fonts` disponible sur <http://www.nwalsh.com/comp.fonts/FAQ/index.html>
- La note sur les fontes Postscript {texdoc}`dans la documentation <psnfss>` de l'extension {ctanpkg}`PSNFSS <psnfss>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,Postscript,polices de caractères
```

