# Où trouver des aide-mémoires ?

## En français

- Philippe Goutet, [Aide-mémoire LaTeX](http://pgoutet.free.fr/latex/aide-memoire.pdf). Ce document accompagne un cours donné par l'auteur.

## En anglais

- Winston Chang, {ctanpkg}`LaTeX2ε Cheat Sheet <latexcheat>`. Il s'agit d'une liste d'éléments à retenir pour utiliser LaTeX. Le tout tient sur une feuille recto-verso.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation
```

