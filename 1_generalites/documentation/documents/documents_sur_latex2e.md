# Que lire sur LaTeX 2ε ?

## En français

- Marc Baudoin, [Apprends LaTeX !](http://www.babafou.eu.org/Apprends_LaTeX/Apprends_LaTeX.pdf) (2012). Il s'agit du manuel de formation de l'ENSTA, également disponible sur le {ctanpkg}`site du CTAN <apprends-latex>`.
- Benjamin Bayart, [Joli manuel pour LaTeX 2ε](https://tex.loria.fr/general/manuel2ep.pdf).
- Adrien Bouzigues, {ctanpkg}`Initiation à LaTeX - Pour débutants ou jeunes utilisateurs <guide-latex-fr>`.
- Fabien Conus et Franck Pastor, [Introduction à LaTeX sous Mac OS X](http://redac.cuk.ch/franck/article-cuk-latex-pdf/intro-latex-rev.pdf) (2009).
- Michel Goossens, [LateX2e, un aperçu](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_1994___18_1_0.pdf) (tiré du [Cahier GUTenberg n°18](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1994___18)).
- Vincent Lozano, [Tout ce que vous avez toujours voulu savoir sur LaTeX sans jamais oser le demander](https://framabook.org/tout-sur-latex/). Il s'agit ici d'un livre libre, ce principe étant détaillé sur le site de [Framabook](https://framabook.org/).
- Tobias Oetiker, {ctanpkg}`Une courte (?) introduction à LaTeX 2ε <lshort-french>` (2011).
- Florent Rougon, [Présentation rapide de LaTeX2e](http://frougon.net/writings/presentation_LaTeX/).
- Vincent Seguin, [Aide-mémoire LaTeX](http://tex.loria.fr/general/aide-memoire-latex-seguin1998.pdf) (2000).

## En anglais

- LaTeX3 Project Team, {ctanpkg}`LaTeX 2ε for authors <usrguide>`. Ce document décrit les changements entre LaTeX 2.09 et LaTeX.
- LaTeX3 Project Team, {ctanpkg}`The LaTeX 2ε sources <latex2e-help-texinfo>`. Ce document commente les fichiers source de LaTeX.
- Jon Warbrick *et al.*, [Essential LaTeX ++](https://www.researchgate.net/publication/2319358_Essential_LATEX). Ce document très pédagogique permet de réaliser un document LaTeX en quelques minutes.

```{eval-rst}
.. meta::
   :keywords: livres,documentation,LaTeX
```

