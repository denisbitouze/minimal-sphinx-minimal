# Que lire sur la programmation en LaTeX3 ?

Pour le moment, il n'y a pas de livre sur ce sujet. Et même s'il y en avait un, il aurait beaucoup de lacunes. Toutefois, il existe quelques ressources pour le moment en anglais :

- la {texdoc}`documentation <expl3>` de l'extension {ctanpkg}`expl3` donne un aperçu général des concepts ;
- la {texdoc}`documentation <interface3>` de l'extension {ctanpkg}`interface3` fournit une description des commandes complète ;
- le [site](https://www.latex-project.org/) du {doc}`projet LaTeX </1_generalites/histoire/c_est_quoi_latex3>` propose une liste de [publications](https://www.latex-project.org/publications/) ;
- Joseph Wright a écrit une courte série d'articles dans son [blog](https://www.texdev.net/) qui peut aider.

Le projet se poursuivant toujours, les documents continuent à évoluer. Certains des problèmes de conception plus larges sont discutés (toujours en anglais) sur la liste de diffusion LaTeX `latex-l`. Vous pouvez vous y abonner à cette liste en envoyant un message « `subscribe latex-l ⟨votre nom⟩` » à [listserv@urz.Uni-Heidelberg.de](mailto:listserv@urz.Uni-Heidelberg.de).

______________________________________________________________________

*Source :* {faquk}`LaTeX3 programming <FAQ-latex3-prog>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex3,programming
```

