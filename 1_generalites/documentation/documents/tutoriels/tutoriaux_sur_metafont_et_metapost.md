# Où trouver des tutoriels sur MetaFont et MetaPost ?

## En français

- Laurent Chéno, [Une introduction à MetaPost](http://pauillac.inria.fr/~cheno/metapost/metapost.pdf), 1999. Le code des exemples est [également téléchargeable](http://pauillac.inria.fr/~cheno/Metapost/index.html).
- Yves Soulet, [MetaPost raconté aux piétons](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2009___52-53_5_0.pdf), [Cahier GUTenberg n°52-53](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_2009___52-53), 2009.
- Vincent Zoonekynd, [Metapost : exemples](http://zoonek.free.fr/LaTeX/Metapost/metapost.html). Ces exemples en grand nombre sont également disponibles sur une {ctanpkg}`page du CTAN <metapost-examples>`. Bien que ces exemples ne constituent pas exactement un « didacticiel », ils constituent certainement un matériel d'apprentissage précieux.

## En anglais

- Christophe Grandsire, [The MetaFont Tutorial](http://metafont.tutorial.free.fr/).

- Hans Hagen, [MetaFun](http://www.pragma-ade.com/general/manuals/metafun-p.pdf). L'auteur propose un tutoriel qui se concentre sur l'utilisation de MetaPost dans ConTeXt. Il est déposé sur [le site](https://www.pragma-ade.com/show-man-3.htm) de son entreprise.

- Urs Oswald, [A very brief tutorial](http://www.tlhiv.org/MetaPost/tutorial/). Un tutoriel de MetaPost en ligne basé sur des exemples. Il utilise l'[outil de Troy Henderson](http://www.tlhiv.org/mppreview) pour tester des morceaux de code MetaPost, ce qui peut bien aider.

- Geoffrey Tobin, {ctanpkg}`MetaFont for Beginners <metafont-beginners>`. Il décrit le fonctionnement du système MetaFont et comment éviter certains de ses pièges potentiels (voir la question sur {doc}`l'utilisation de MetaFont </5_fichiers/fontes/utiliser_metafont>`).

- Mari Voipio, *Entry-level MetaPost*. Il s'agit d'une introduction en quatre articles :

  - [Entry-level MetaPost 1 : On the grid](https://tug.org/TUGboat/intromp/tb106voipio-grid.pdf), [TUGboat 34-1](http://tug.org/TUGboat/tb34-1/) ;
  - [Entry-level MetaPost 2 : Move it!](https://tug.org/TUGboat/intromp/tb107voipio-moveit.pdf), [TUGboat 34-2](http://tug.org/TUGboat/tb34-2/) ;
  - [Entry-level MetaPost 3 : Color](https://tug.org/TUGboat/intromp/tb108voipio-color.pdf), [TUGboat 34-3](http://tug.org/TUGboat/tb34-3/) ;
  - [Entry-level MetaPost 4 : Artful lines](https://tug.org/TUGboat/tb35-1/tb109voipio.pdf), [TUGboat 35-1](http://tug.org/TUGboat/tb35-1/).

- Peter Wilson, {ctanpkg}`Some Experiences in Running MetaFont and MetaPost <metafp>`. L'expérience de l'auteur est d'autant plus notable qu'il a conçu un certain nombre de polices « historiques » en utilisant MetaFont. Pour MetaFont, son texte présente les tests et installations de nouvelles polices MetaFont, tandis que la section MetaPost décrit comment utiliser les illustrations MetaPost dans les documents LaTeX, en mettant l'accent sur l'utilisation des polices appropriées pour le texte ou les mathématiques.

______________________________________________________________________

*Sources :*

- {faquk}`MetaFont and MetaPost tutorials <FAQ-mfptutorials>`,
- [What is Metapost/Metafont and how can I get started using it?](https://tex.stackexchange.com/questions/107489/what-is-metapost-metafont-and-how-can-i-get-started-using-it)

```{eval-rst}
.. meta::
   :keywords: LaTeX,Metafont,Metapost,illustrations,fontes,polices,dessiner des carcatères,tutoriel,didacticiel,manuel Metafont,manuel Metapost
```

