# Comment poser une question ?

Vous avez besoin de l'aide de la communauté et vous avez décidé de l'endroit où vous allez {doc}`poser votre question </1_generalites/documentation/listes_de_discussion>`. Mais comment allez-vous la tourner ?

D'excellents conseils « généraux » sur la façon de poser des questions techniques sont présentés dans un [article d'Eric Raymond](http://catb.org/~esr/faqs/smart-questions.html) (dont il existe une [traduction en français](https://www.gnurou.org/writing/smartquestionsfr/)). Rappelez-vous que personne ne vous *doit* de l'aide mais que, si vous vous exprimez bien, vous trouverez généralement quelqu'un qui se fera un plaisir de vous aider.

Alors, comment s'exprimer dans le monde de (La)TeX ? En l'absence de règles détaillées, voici quelques lignes de conduite de bon sens :

- assurez-vous de vous adresser aux bonnes personnes. Ne posez pas de questions dans un forum TeX sur les pilotes de périphériques d'imprimante pour le système d'exploitation `MacFenêtreX`. Certes, les utilisateurs de TeX ont besoin d'imprimantes... mais il y a peu de chances qu'ils se soient également spécialisés dans le système d'exploitation `MacFenêtreX` ;
- de même, évitez de poser une question dans une langue que la majorité du groupe n'utilise pas : postez votre question en basque sur « `de.comp.text.tex` » et vous risquez d'attendre bien longtemps avant d'avoir une réponse d'un allemand, expert de TeX et parlant le basque ;
- indiquez le système que vous utilisez ou avez l'intention d'utiliser. Si « Je ne peux pas installer TeX » a du sens, « J'essaye de installer la distribution `trucTeX` sur le système d'exploitation `MacFenêtreX` » fait bien mieux en donnant un contexte plus large qui aidera beaucoup ceux qui cherchent à vous répondre. La même consigne s'applique lorsque vous rencontrez des difficultés pour installer quelque chose de nouveau dans votre système : « Je veux ajouter l'extension `chouette` à ma distribution `trucTeX v12.0` sur le système d'exploitation `MacFenêtreX 2024` » ;
- si vous avez besoin de savoir comment faire quelque chose, indiquez clairement quel est votre environnement : « Je veux faire (...) en Plain TeX » ou « Je veux faire (...) en LaTeX en utilisant la classe `mystere` ». Si vous pensiez savoir comment faire et que vos tentatives ont échoué, indiquez ce que vous avez essayé : « J'ai déjà essayé d'installer l'extension `malconnue` dans le répertoire `peucourant`, et cela n'a pas fonctionné, même après avoir actualisé la base de données des noms de fichiers ».
- si quelque chose ne va pas avec (La)TeX, faites comme si vous {doc}`soumettiez un rapport de bug LaTeX </2_programmation/faire_un_rapport_de_bug>` et essayez de générer un {doc}`exemple minimal </1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal>`. Si votre exemple a besoin d'une classe locale ou d'une autre ressource qui n'est généralement pas disponible, assurez-vous d'indiquer la façon dont cette ressource peut être obtenue ;
- les figures sont une exception au point précédent. La plupart des problèmes peuvent être illustrés avec une « figure de substitution » sous la forme d'une commande `\rule{`*largeur*`}{`*hauteur*`}` (où *largeur* et *hauteur* sont les dimensions de la figure, comme par exemple `3cm`, `16pt`...). Si la vraie figure s'avère nécessaire, n'essayez pas de la poster avec votre question : mieux vaut la déposer quelque part sur le web et y faire référence ;
- soyez aussi concis que possible. Ceux qui peuvent vous aider n'ont généralement pas besoin de savoir *pourquoi* vous faites quelque chose. Ils veulent juste comprendre *ce que vous faites* et où se situe le problème.

______________________________________________________________________

*Source :* {faquk}`How to ask a question <FAQ-askquestion>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,question,documentation
```

