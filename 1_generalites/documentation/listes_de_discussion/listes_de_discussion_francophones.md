# Où trouver des listes de discussion sur (La)TeX ?

## En français

- La liste [gut@ens.fr](mailto:gut@ens.fr) de l'association GUTenberg est très
  active. Il est possible de s'y abonner sur le [site de
  l'association](https://www.gutenberg-asso.fr/listes). Des liens vers les
  archives sont également proposés.
- La liste de diffusion « typographie »,
  [typographie@irisa.fr](mailto:typographie@irisa.fr), est consacrée aux
  problèmes de composition, de typographie --- française ou étrangère --- et de
  mise en page, sans oublier les différentes techniques employées : du lettrage
  à la main à la composition numérique en passant par le plomb, du support
  papier à la page écran, du « bon usage » du Code typographique jusqu'aux
  normalisations du codage des caractères.

- Historiquement, il existait une liste de diffusion `omega@ens.fr` concernant
  Omega, une extension de TeX développée par John Plaice et Yannis Haralambous.

  :::{attention}
  Ce projet n'est {doc}`plus d'actualité. </1_generalites/histoire/developpement_du_moteur_tex>`
  :::

## En anglais

- Le TUG (international) maintient un grand nombre de [listes de diffusion anglophones](https://tug.org/mailman/listinfo) dont la plus connue est [texhax](https://tug.org/mailman/listinfo/texhax). Cette dernière correspond en effet au thème « Discussions et questions générales sur TeX » et dispose d'une [vaste archive en ligne](https://tug.org/pipermail/texhax/). Notez bien que, parmi ces listes, certaines sont réservées au développement de TeX et LaTeX et vous n'y trouverez pas forcément d'aide. N'hésitez pas à consulter les archives des listes qui vous intéressent avant d'y poster, la majorité de celles-ci étant accessible en lecture.
- Le TUG allemand, DANTE e.V., maintient la liste de diffusion des annonces du CTAN, [ctan-ann](https://lists.dante.de/mailman/listinfo/ctan-ann). Elle vous permet de suivre les évolutions des différentes extensions placées sur le CTAN à raison de 5 messages par jour en moyenne. Le site propose aussi les [archives de la liste](https://lists.dante.de/pipermail/ctan-ann/).
- L'association GUTenberg héberge la [liste de discussion internationale sur MetaFont](https://www.gutenberg.eu.org/listes) : [metafont@ens.fr](mailto:metafont@ens.fr).

## En italien

- Le TUG italien, [GuIT](https://www.guitex.org/), maintient des [forum web](https://www.guitex.org/home/en/forum/index). Le forum d'entraide est particulièrement actif.

## En espagnol

- La liste [ES-TEX@LISTSERV.REDIRIS.ES](mailto:ES-TEX@LISTSERV.REDIRIS.ES) est indépendante du TUG espagnol, [CervanTeX](http://www.cervantex.es/). Les informations pour s'y abonner sont dans la [FAQ de CervanTeX](http://www.aq.upm.es/Departamentos/Fisica/agmartin/webpublico/latex/FAQ-CervanTeX/FAQ-CervanTeX-10.html).

## En allemand

- Le TUG allemand, DANTE e.V., héberge [plusieurs listes](https://lists.dante.de/mailman/listinfo).

## En néerlandais

- Le TUG néerlandais, NTG, maintient [différents listes](https://www.ntg.nl/mail.html).

______________________________________________________________________

*Sources :*

- {faquk}`Specialist mailing lists <FAQ-maillistsstar>`,
-

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,questions sur LaTeX,liste de discussion,aide sur LaTeX,listes de diffusion,Association GUTenberg
```

