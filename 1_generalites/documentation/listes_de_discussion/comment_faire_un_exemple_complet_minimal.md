# Comment faire un « exemple minimal » ?

L'article « {doc}`Comment poser une question ? </1_generalites/documentation/listes_de_discussion/comment_poser_une_question>` » suggère de préparer un « exemple minimal » illustrant la problématique que vous rencontrez. Toutefois, si vous rencontrez un problème dans un document de deux cents pages, cette suggestion n'est sans doute pas très explicite sur la manière de procéder. Voici donc quelques méthodes et recommandations sur ce sujet.

Pour être efficace, votre exemple doit être un *exemple complet minimal* (ECM) :

- *complet* : l'exemple doit être compilable sans ajouter une seule ligne. En particulier, si vous utilisez LaTeX, il doit commencer par `\documentclass`, contenir `\begin{document}` et `\end{document}`. Evitez d'utiliser des extensions non-standard ou fournissez un lien vers celles-ci ;
- *minimal* : l'exemple ne doit pas contenir une ligne de code superflue. Ceci implique qu'il ne s'agit pas de votre document complet. Il est indispensable de supprimer de votre préambule tous les appels de packages et définitions de commandes inutiles à la reproduction du problème.

## Les deux méthodes pour produire un exemple minimal

### La méthode additive

La *méthode additive* commence par une structure de document de base : par exemple, pour LaTeX, il y aura les commandes `\documentclass`, `\begin{document}`, `\end{document}`). À cette structure de base s'ajoutent ensuite progressivement d'autres éléments.

Le premier élément à ajouter est le paragraphe encadrant le point où le problème se produit. Il peut cependant s'avérer difficile de trouver la ligne qui provoque le problème. Si le problème d'origine est une erreur (La)TeX, la question « {doc}`Quelle est la structure des erreurs TeX ? </2_programmation/erreurs/structure_d_un_message_d_erreur>` » peut vous aider.

Notez qu'il y a des choses qui peuvent mal tourner dans une partie du document en conséquence d'une erreur dans une autre partie : un cas courant est celui des problèmes dans la table des matières (du fait d'une bizarrerie dans un titre de section, ou autre) ou des problèmes dans une liste de tables ou une liste des figures (à partir d'una anomalie dans une commande `\caption`). Dans ce cas, incluez le titre de la section ou de la légende (la légende a probablement besoin de l'environnement `figure` ou `table` autour d'elle, mais *n'a pas besoin* de la figure ou de la table elle-même).

Si le fichier que vous avez ainsi créé déclenche le problème, vous avez terminé. Sinon, essayez d'ajouter progressivement des extensions utilisées dans votre document original : le mieux est d'avoir un fichier avec une seule extension mais vous pourrez peut-être constater que l'extension responsable de vos malheurs ne se chargera pas correctement à moins qu'une autre extension n'ait été chargée. Il est aussi possible que l'extension `A` échoue uniquement lorsque l'extension `B` a été chargée, ce qui arrive fréquemment.

### La méthode soustractive

La *méthode soustractive* commence avec votre document en entier puis en supprime des éléments jusqu'à ce que le fichier ne génère plus l'erreur. Est alors réintégré le dernier élément supprimé. La seule réelle difficulté est ici de déterminer quelle partie supprimer à chaque étape. N'oubliez pas d'ailleurs de supprimer toutes les extensions inutiles.

## Quelques commandes et extensions qui peuvent aider

Si vous avez ajouté plus d'une extension, placez la commande `\listfiles` dans le préambule de votre document. Ainsi, LaTeX produira une liste des extensions que vous avez utilisées avec leur numéro de version, ce qui peut être très utile pour donner des pistes pour vous aider.

La méthode additive et, dans une certaine mesure, la méthode soustractive peuvent bénéficier :

- de la classe {ctanpkg}`minimal` (qui appartient à la distribution LaTeX) qui fournit ce qu'elle indique : rien de moins que ce qui est nécessaire à LaTeX pour fonctionner ;
- de la liasse {ctanpkg}`mwe` (MWE pour *minimal working example* ou « exemple minimal fonctionnel ») qui fournit des images dans des formats utilisables dans des documents LaTeX ainsi qu'une extension {ctanpkg}`mwe` qui charge d'autres extensions telles que {ctanpkg}`blindtext` et {ctanpkg}`lipsum`, toutes deux capables de produire du [faux-texte](https://fr.wikipedia.org/wiki/Lorem_ipsum).

## Quelques conseils complémentaires

Si aucune de ces méthodes ne vous réussit, vous pourriez vous dire qu'il faut poster l'intégralité de votre document. Ne le faites pas :

- préférez la mise à disposition d'une copie sur un site sur le web. Cela reste évidemment déconseillé, surtout si le contenu de votre document est confidentiel ;
- si l'ensemble du document est nécessaire, cela peut être le signe que votre document produit en fait un débordement des capacités de LaTeX; le mieux est alors de publier le code « autour » de l'erreur et le texte intégral de l'erreur.

Toutes ces méthodes semblent demander beaucoup de travail pour préparer une simple question. Mais il s'agit ici d'être efficace :

- préparer un exemple minimal vous amène souvent à trouver la solution *par vous-même* et vous évite toute la démarche de poster votre question et d'analyser les réponses ;
- vous souhaitez en général une réponse rapide : un exemple soigneusement écrit et rapide à comprendre a bien plus de chances d'obtenir une réponse directe et pertinente. Un exemple moins bien rédigé risque d'amener à des questions complémentaires de la part de vos lecteurs... ce qui va allonger d'autant votre temps pour obtenir votre réponse.

En résumé, un bon exemple peut vous économiser une journée de votre temps, souvent pour un peu moins d'une demi-heure, pour constituer l'exemple minimal.

La plus grande partie de ce qui a été indiqué ici peut se retrouver de façon plus approfondie dans un document disponible en [allemand](https://www.minimalbeispiel.de/) comme en [anglais](https://www.minimalbeispiel.de/mini-en.pdf).

______________________________________________________________________

*Sources :*

- {faquk}`How to make a "minimum example" <FAQ-minxampl>`,
- [Réalisation d'ECM](https://texnique.fr/osqa/questions/3381/realisation-decm).

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,ECM,MWE,demander de l'aider,se faire aider,Minimal Working Example,exemple complet,exemple minimal, exemple complet minimal,simplifier son exemple
```

