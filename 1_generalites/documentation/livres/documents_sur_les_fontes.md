# Que lire sur les fontes et METAFONT ?

## En français

- Bernard Desgraupes, *METAFONT -- Guide pratique*, Vuibert, 2017, ISBN-10 2-7117-8642-0, ISBN-13 : 978-2-7117-8642-8.
- Yannis Haralambous, *Fontes et Codages*, O'Reilly, 2004, ISBN-10 2-8417-7273-X, ISBN-13 : 978-2-8417-7273-5.

## En anglais

- Alan Hoenig, *TeX Unbound : LaTeX and TeX strategies for fonts, graphics, and more*, Oxford University Press, 1998, ISBN-10 0-1950-9686-X, ISBN-13 978-0-1950-9686-6.
- Donald E. Knuth, *The METAFONTbook*, Addison Wesley, 1986, ISBN-10 0-2011-3444-6, ISBN-13 978-0-2011-3444-5.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

