# Que lire sur TeX et Plain TeX ?

## En français

- Donald E. Knuth, *Le TeX book*, Vuibert, 2017, ISBN-10 2-7117-4819-7, ISBN-13 978-2-7117-4819-8. Cette version a été traduite en français par Jean-Côme Charpentier.
- Raymond Seroul, *Le Petit Livre de TeX*, Interéditions, 1989, ISBN-10 2-7296-0233-X, ISBN-13 978-2-7296-0233-8.
- Thomas Lachand-Robert, *La Maîtrise de TeX et LaTeX*, Masson, 1995, ISBN-10 2-2258-4832-7, ISBN-13 978-2-2258-4832-2. Ce document peut être très utile à ceux qui veulent programmer, comprendre ou adapter des commandes TeX. Principalement orienté vers TeX, quelques rares et courts passages à propos de LaTeX.
- Christian Tellechea, {ctanpkg}`Apprendre à programmer en TeX <apprendre-a-programmer-en-tex>`, 2014. Orienté vers la programmation en TeX plutôt que vers la composition de documents.

## En anglais

Le site du projet LaTeX donne une [liste d'ouvrage](https://www.latex-project.org/help/books/) (avec des errata). De même, le site de macroTeX donne une [liste commentée de certains classiques](http://www.macrotex.net/texbooks/).

- Paul W. Abrahams, Karl Berry et Kathryn A. Hargreaves, *TeX for the impatient*, Addison-Wesley, 1990, ISBN-10 0-2015-1375-7. Épuisé en version papier mais disponible en {ctanpkg}`version électronique <impatient>` sur le CTAN.
- Stephan von Bechtolsheim, *TeX in Practice*, Springer Verlag, 1993, 4 volumes, ISBN-10 3-5409-7296-X pour l'ensemble. Volume 1, ISBN-10 0-3879-7595-0, volume 2 ISBN-10 0-3879-7596-9, volume 3 ISBN-10 0-3879-7597-7, et volume 4 : ISBN-10 0-3879-7598-5.
- Arvind Borde, *TeX by Example : A Beginner's Guide*, Academic Press, 1992, ISBN-10 0-12-117650-9. Épuisé.
- Malcolm Clark, *A Plain TeX Primer*, Oxford University Press, 1993, ISBN-10 0-1985-3724-7.
- Michael Doob, *TeX : Starting from Square One*, Springer Verlag, 1993, ISBN-10 3-5405-6441-1. Épuisé.
- [Victor Eijkhout](http://www.eijkhout.net/), *TeX by Topic*, Addison-Wesley, 1992, ISBN-10 0-2015-6882-9. Il est disponible aussi disponible {doc}`gratuitement </1_generalites/documentation/documents/documents_sur_tex>` sur le {ctanpkg}`CTAN <texbytopic>`.
- Donald E. Knuth, *Computers & Typesetting*, Volumes A à E, Addison-Wesley, 2001, ISBN-10 0-2017-3416-8.
- Donald E. Knuth, *Digital Typography*, CSLI and Cambridge University Press, 1999, ISBN-10 1-5758-6011-2.
- Donald E. Knuth, *The TeXbook*, Addison-Wesley, 1984, ISBN-10 0-2011-3447-0.
- David Salomon, *The advanced TeXbook*, Springer Verlag, 1995, ISBN-10 0-387-94556-3.
- Stanley Sawyer et Steven Krantz, *A TeX Primer for Scientists*, CRC Press, 1995, ISBN-10 0-849-37159-7.
- Norbert Schwarz, *Introduction to TeX*, Addison Wesley, 1989, ISBN-10 0-2015-1141-X. Épuisé.
- Raymond Seroul et Silvio Levy, *A Beginner's Book of TeX*, Springer Verlag, 1992, ISBN-10 0-3879-7562-4.
- Wynter Snow, *TeX for the Beginner*, Addison Wesley, 1992, ISBN-10 0-2015-4799-6.
- Michael D. Spivak, *The Joy of TeX*, 2{sup}`e`, AMS, 1990, ISBN-10 0-8218-2997-1. Également disponible en {ctanpkg}`version électronique <joy-of-tex>` sur le CTAN.

______________________________________________________________________

*Source :* {faquk}`Books on TeX, Plain TeX and relations <FAQ-tex-books>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,livre,TeX,quel livre sur TeX,livres sur Plain TeX
```

