# Que lire sur AMS-LaTeX ?

AMS-LaTeX étant presque incontournable en matière de mathématiques, la plupart des livres traitant de LaTeX et de mathématiques vont aborder ce sujet. On peut donc se tourner vers la question « {doc}`Que lire sur LaTeX ? </1_generalites/documentation/livres/documents_sur_latex>` »

## Ouvrages en anglais

- George Grätzer, *More Math into LaTeX : An Introduction to LaTeX and AMSLaTeX*, 5{sup}`e` édition, Springer International Publishing, 2016, ISBN-10 3-3192-3795-0, ISBN-13 978-3-3192-3795-4. Voir la description sur le [site de l'éditeur](https://www.springer.com/gp/book/9783319237954).

```{eval-rst}
.. meta::
   :keywords: LaTeX,AMS-LaTeX,livre
```

