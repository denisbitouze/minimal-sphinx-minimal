# Que lire sur LaTeX ?

## En français

- Denis Bitouzé et Jean-Côme Charpentier, *LaTeX, l'essentiel*, Pearson, 2010, ISBN-10 2-7440-7451-9, ISBN-13 978-2-7440-7451-6.
- Céline Chevalier, Walter Appel, Emmanuel Cornet, Sébastien Desreux, Jean-Julien Fleck et Paul Pichaureau, *LaTeX pour l'impatient*, 4{sup}`e` édition, H&K, 2016, ISBN-10 2-3514-1327-X, ISBN-13 978-2-3514-1327-2.
- Sébastien Combéfis, *LaTeX HowTo : Le Guide Pratique*, 2{sup}`e` édition, Lulu, 2014, ISBN-10 1-3260-0412-3, ISBN-13 978-1-3260-0412-5.
- Bernard Desgraupes, *LaTeX -- Apprentissage, guide et référence*, 2{sup}`e` édition, De Boeck, 2019, ISBN-10 2-8073-2362-6, ISBN-13 978-2-8073-2362-9. Il contient des renseignements sur le multilinguisme (babel, french, langues asiatiques, CJK, pinyin, vietnam, arabtex, hebtex...) et sur les codages, polices et création de fontes virtuelles.
- Michel Goossens, Frank Mittelbach, Johannes Braams, David Carlisle et Chris Rowley, *The LaTeX companion*, 2{sup}`e` édition, Pearson, 2005, ISBN-10 2-7440-7133-1, ISBN-13 978-2-7440-7133-1. Il s'agit ici d'une traduction.
- Vincent Gramet et Jean-Pierre Regourd, *Objectif LaTeX*, Masson, 1997, ISBN-10 : 2-2258-4993-5, ISBN-13 : 978-2-2258-4993-0.
- Vincent Lozano, *Tout ce que vous avez toujours savoir sur LaTeX sans jamais oser le demander*, In Libro Veritas, 2008, ISBN-10 2-3520-9149-7, ISBN-13 978-2-3520-9149-3.
- Nicolas Poulain, *LaTeX pour les enseignants*, Ellipses, 2020, ISBN-10 2-3400-3671-2, ISBN-13 978-2-3400-3671-0.
- Christian Rolland, *LaTeX par la pratique*, O'Reilly, 1999, ISBN-10 2-8417-7073-7, ISBN-13 978-2-8417-7073-1.

## En anglais

Le site du projet LaTeX donne une [liste d'ouvrage](https://www.latex-project.org/help/books/) (avec des errata). De même, le site de macroTeX donne une [liste commentée de certains classiques](http://www.macrotex.net/texbooks/).

- Antoni Diller, *LaTeX : Line by Line : Tips and Techniques for Document Processing*, 2{sup}`e` édition, John Wiley & Sons, 1999, ISBN-10 0-4719-7918-0, ISBN-13 : 978-0-4719-7918-0.
- Michel Goossens, Frank Mittelbach, Johannes Braams, David Carlisle et Chris Rowley, *The LaTeX companion*, 2{sup}`e` édition, Addison Wesley, 2004, ISBN-10 0-2013-6299-6, ISBN-13 978-0-2013-6299-2. Ce livre existe aussi en version numérique (EPUB, MOBI and PDF) sur le site d'[Informit](http://www.informit.com/store/latex-companion-9780133387667).
- Michel Goossens, Sebastian Rahtz, Frank Mittelbach, Denis Roegel et Herbert Voß, *The LaTeX Graphics Companion*, 2{sup}`e` édition, Addison Wesley, 2007, ISBN-10 0-3215-0892-0, ISBN-13 978-0-3215-0892-8.
- Michel Goossens, Sebastian Rahtz, Eitan M. Gurari, Ross Moore et Robert S. Sutor, *The LaTeX Web Companion*, Addison Wesley, 1999, ISBN-10 0-2014-3311-7, ISBN-13 978-0-2014-3311-1.
- George Grätzer, *First Steps in LaTeX*, Birkhäuser, 1999, ISBN-10 0-8176-4132-7, ISBN-13 978-0-8176-4132-0.
- George Grätzer, *More Math into LaTeX : An Introduction to LaTeX and AMSLaTeX*, 5{sup}`e` édition, Springer International Publishing, 2016, ISBN-10 3-3192-3795-0, ISBN-13 978-3-3192-3795-4.
- Alan Hoenig, *TeX Unbound : LaTeX and TeX strategies for fonts, graphics, and more*, Oxford University Press, 1998, ISBN-10 0-1950-9686-X, ISBN-13 978-0-1950-9686-6.
- Helmut Kopka et Patrick W. Daly, *A Guide to LaTeX, document preparation for beginners and advanced users*, 4{sup}`e` édition, Addison Wesley, 2003, ISBN-10 0-3211-7385-6, ISBN-13 978-0-3211-7385-0. Guide très complet. En particulier, il met en permanence l'accent sur ce qui est différent/spécifique entre LaTeX et LaTeX 2.09.
- Stefan Kottwitz, *LaTeX Beginner's Guide*, Packt Publishing, 2011, ISBN-10 1-8471-9986-0, ISBN-13 978-1-8471-9986-7.
- Leslie Lamport, *LaTeX, a Document Preparation System*, 2{sup}`e` édition, Addison Wesley, 1994, ISBN 0-2015-2983-1, ISBN-13 978-0-2015-2983-8.
- Leslie Lamport, *LaTeX reference manual*, Addison Wesley, 1985, ISBN-10 0-2011-5790-X, ISBN-13 978-0-2011-5790-1.
- Bernice S. Lipkin, *LaTeX for Linux : A Vade Mecum*, Springer-Verlag, 1999, ISBN-10 0-3879-8708-8, ISBN-13 978-1-4612-1462-5.
- Apostolos Syropoulos, Antonis Tsolomitis and Nick Sofroniou, *Digital Typography Using LaTeX, Incorporating some multilingual aspects, and use of Omega*, Springer-Verlag, 2002, ISBN-10 978-0-3879-5217-8, ISBN-13 978-0-3879-5217-8.
- Herbert Voß, *Typesetting Mathematics with LaTeX*, UIT Cambridge, 2010, ISBN-10 1-9068-6017-3, ISBN-13 978-1-9068-6017-2.
- Herbert Voß, *Typesetting Tables with LaTeX*, UIT Cambridge, 2011, ISBN-10 978-1-9068-6025-7, ISBN-13 978-1-9068-6025-7.
- Herbert Voß, *PSTricks : Graphics and PostScript for TeX and LaTeX*, UIT Cambridge, 2011, ISBN-10 1-9068-6013-0, ISBN-13 978-1-9068-6013-4.

Un extait du *Math into LaTeX* de George Grätzer et les fichiers d'exemple des trois *LaTeX Companions* et du *First Steps in LaTeX* sont tous disponibles sur le CTAN.

______________________________________________________________________

*Source :* {faquk}`Books on LaTeX <FAQ-latex-books>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,bibliographie en anglais
```

