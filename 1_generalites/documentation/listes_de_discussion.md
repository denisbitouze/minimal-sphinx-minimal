# Comment obtenir de l'aide en ligne ?

## Avant de demander de l'aide

Nous supposons ici que, si vous ne savez plus quoi faire, vous avez au moins :

- regardé les {doc}`réponses données dans des FAQ </1_generalites/documentation/sites/autres_faq>` ;

- regardé dans les {doc}`livres </1_generalites/documentation/livres/start>` dont vous disposez ;

- étudié des {doc}`tutoriels </1_generalites/documentation/documents/tutoriels/start>` ;

- (et éventuellement) fouillé des archives d'échanges :

  - en français, celles du groupe de discussion [fr.comp.text.tex](https://groups.google.com/g/fr.comp.text.tex) ou celle de la liste [gut](https://groups.google.com/g/gut_fr/),
  - en anglais, celles du groupe de discussion [comp.text.tex](http://groups.google.com/group/comp.text.tex) ou de la liste de discussion [texhax](https://tug.org/pipermail/texhax/).

Pensez aussi, si vous cherchez une extension ou un programme particulier, à regarder sur votre propre système : vous avez peut-être déjà ce que vous cherchez car les distributions TeX fournissent un large éventail de fichiers. Le {doc}`CTAN </1_generalites/glossaire/qu_est_ce_que_le_ctan>` peut également identifier les extensions qui pourraient aider : vous pouvez [les chercher](https://ctan.org/search/), tout comme vous pouvez le parcourir [par sujet](https://ctan.org/topics/cloud). Chaque entrée du CTAN propose une brève description de l'extension et des liens vers la documentation connue sur le net. Mieux, une grande partie de ces entrées incluent désormais de la documentation, il est donc souvent utile de consulter la page CTAN de l'extension que vous envisagez d'utiliser. D'ailleurs, dans la mesure du possible, chaque lien mis sur une extension dans le corps principal de cette FAQ a un lien vers le entrée de catalogue pertinente).

Si la recherche par historique échoue, vous pouvez alors demander de l'aide au monde entier.

## Construire un exemple complet minimal

Pour être bien aidé, il faut bien décrire votre problème. La question « {doc}`Comment poser une question ? </1_generalites/documentation/listes_de_discussion/comment_poser_une_question>` » vous donne ici des conseils généraux sur cette phase.

Dans le cas où votre code ne produit pas le résultat que vous attendez, il est indispensable d'accompagner la description de votre problème d'un exemple de code qui permette de le reproduire. Cet exemple doit être si possible complet et minimal, c'est ce qu'on appelle un « ECM » (exemple complet minimal).

La question « {doc}`Comment faire un exemple complet minimal ? </1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal>` » décrit en détail comment procéder.

## Où demander de l'aide ?

Voici les trois principales sources d'aide en ligne :

- les {doc}`listes de diffusion </1_generalites/documentation/listes_de_discussion/listes_de_discussion_francophones>` ;
- les {doc}`groupes de discussion </1_generalites/documentation/listes_de_discussion/groupes_de_discussion>` ;
- les {doc}`forums </1_generalites/documentation/sites/forums>`.

Enfin, *n'essayez surtout pas* d'envoyer un e-mail à l'équipe du {doc}`projet LaTeX </1_generalites/histoire/c_est_quoi_latex3>` ou aux responsables des distributions TeX Live ou MiKTeX pour obtenir de l'aide. Même si ces adresses concernent des utilisateurs expérimentés de TeX et LaTeX, aucun petit groupe ne peut avoir une expertise dans tous les domaines d'utilisation, de sorte que les listes et forums restent un bien meilleur pari.

______________________________________________________________________

*Sources :*

- {faquk}`Getting help online <FAQ-gethelp>`,
- [LaTeX : À l'aide !](https://fr.wikibooks.org/wiki/LaTeX/%C3%80_l%27aide_!) (wikibook)

```{eval-rst}
.. meta::
   :keywords: LaTeX,messages d'erreur,ECM,exemple complet minimal,ne marche pas,lorem ipsum dolor,documentation,forums,trouver de l'aide,get help,poser une question sur LaTeX,j'ai un problème
```

