# Qu'est-ce que le catalogue du CTAN ?

Le code de TeX et LaTeX ainsi que la plus grande partie du code des extensions et de leur documentation est distribuée via le site du [CTAN](https://ctan.org/) (pour *Comprehensive TeX Archive Network*, le Réseau des archives complètes de TeX).

La structure et l'histoire du CTAN {doc}`sont présentées sur cette page </1_generalites/glossaire/qu_est_ce_que_le_ctan>`.

Comme ceci représente des milliers de pages, le CTAN propose un menu *Browse CTAN* (« Explorer le CTAN ») avec des liens vers une liste d'extensions, une [liste de thèmes](https://www.ctan.org/topics/cloud) (qui regroupent les extensions) et une liste d'auteurs (avec leurs extensions).

:::{important}
Si vous faites une recherche à partir d'un moteur de recherche généraliste, vous aurez peut-être des réponses très redondantes.

Ceci vient du fait que le CTAN est un [réseau de sites miroirs](https://www.ctan.org/mirrors), et non un unique serveur. Donc différentes machines de par le monde proposent les mêmes fichiers, ce qui conduit à de multiples résultats similaires si le moteur de recherche n'a pas détecté que les serveurs sont des miroirs les uns des autres. C'est pourquoi le catalogue du CTAN est le moyen recommandé pour trouver des extensions.
:::

Pour un accès direct, si vous connaissez le nom d'une extension qui vous intéresse, il y a de grandes chances que son adresse web sur le CTAN soit :

```text
https://ctan.org/pkg/<nomExtension>
```

Enfin, si vous voulez être tenu au courant des actualités du CTAN, il existe une {doc}`liste de diffusion </1_generalites/documentation/listes_de_discussion/listes_de_discussion_francophones>` dédiée, [ctan-ann](https://lists.dante.de/mailman/listinfo/ctan-ann), qui communique plusieurs fois par jour les modifications qui y sont apportées.

______________________________________________________________________

*Source :* {faquk}`The CTAN catalogue <FAQ-catalogue>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,Comprehensive TeX archive network,package,extension,trouver des extensions
```

