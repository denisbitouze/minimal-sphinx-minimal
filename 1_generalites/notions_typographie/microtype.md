# Comment améliorer la qualité typographique de son document ?

Le simple ajout dans l'en-tête du document du paquet {ctanpkg}`microtype` permet d'améliorer significativement la qualité typographique du document produit.

Ce paquet a pour objet principal d'améliorer l'homogénéité du [gris typographique](https://fr.wikipedia.org/wiki/Gris_typographique), c'est-à-dire l’impression produite sur l’œil par la vision générale d’un texte. Pour cela, il a recours à deux techniques assez surprenantes, qui ont pour objectif commun de créer une très légère irrégularité qui renforce le sentiment visuel de régularité :

- La modification de la largeur de la police pour certaines lignes (nommée *font expansion* dans la documentation en anglais), qui permet de réduire l'espacement entre les mots lorsque celui-ci est trop important.
- Le dépassement de l'alignement justifié dans la marge de droite (nommé *character protrusion* dans la documentation en anglais), lorsque la justification habituelle donne l'impression d'un trou, par exemple du fait de la présence d'une ponctuation en fin de ligne ; le fait de la faire légèrement dépasser permet de placer le caractère qui précède plus à droite, ce qui limite l'impression de trou.

L'utilisation du paquet est particulièrement simple, puisqu'il suffit de le charger dans l'en-tête de son document pour activer ses réglages par défaut. Les 250 pages de {texdoc}`sa documentation <microtype>` permettent évidemment d'ajuster les options de fonctionnement selon ses goûts et ses besoins.

Avant même l'amélioration esthétique globale que `microtype` apporte, il a généralement un effet spectaculaire, en réduisant les coupures de mots nécessaires en fin de ligne, et les débordements dans la marge. Voici un exemple. À droite, nous avons juste ajouté la ligne `\usepackage{microtype}` :

**Sans {ctanpkg}`microtype`** :

```latex
% !TEX noedit
% \usepackage{microtype}
```

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.5cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  % \usepackage{microtype}
  \pagestyle{empty}
\begin{document}
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Avec {ctanpkg}`microtype`** :

```latex
% !TEX noedit
\usepackage{microtype}
```

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.5cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \usepackage{microtype}
  \pagestyle{empty}
\begin{document}
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

