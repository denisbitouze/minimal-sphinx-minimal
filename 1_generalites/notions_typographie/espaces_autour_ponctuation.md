# Faut-il mettre des espaces avant et après les symboles de ponctuation ?

Les indications qui suivent ne concernent que la typographie française :

- le point et la virgule sont toujours collés au mot précédent, et suivis d'une espace dite *justifiante*, c'est-à-dire de la même taille que les autres espaces de la ligne ;
- le point-virgule, le point d'exclamation et le point d'interrogation sont précédés d'une espace fine insécable (c'est-à-dire que le symbole de ponctuation ne doit en aucun cas se trouver en tout début de ligne), et suivis d'une espace justifiante ;
- les deux-points sont précédés d'une espace justifiante insécable, et suivis d'une espace justifiante ;
- les guillemets ouvrants sont précédés d'une espace justifiante, et suivis d'une espace justifiante insécable. Pour les guillemets fermants, l'ordre sera bien entendu le symétrique ;
- le tiret est précédé et suivi d'une espace justifiante ;
- les parenthèses et crochets ouvrants sont précédés d'une espace justifiante, et collés au mot qui les suit. L'ordre est symétrique pour les parenthèses et crochets fermants.

:::{important}
Les règles sont différentes entre le français, l'anglais, l'allemand, etc. Lorsque vous consultez des documents sur l'orthotypographie, vérifiez bien à quelle(s) langue(s) ils s'appliquent.

Certains documents *en français* peuvent être des traductions de documents étrangers dont le contenu n'a pas été adapté au français.
:::

## Y a-t-il des extensions capables de s'occuper automatiquement de l'insertion d'espaces ?

Les extensions {ctanpkg}`babel` et {ctanpkg}`polyglossia` (toutes deux avec l'option `french`) se chargent automatiquement d'ajouter ces espaces. Avec elles, on n'a donc plus à s'en occuper.

:::{warning}
Les extensions {ctanpkg}`babel` et {ctanpkg}`polyglossia` sont seulement capables d'**ajouter** des espaces manquantes ; elles ne supprimeront pas celles qui sont superflues (avant une virgule ou un point, par exemple).
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,orthotypographie,ponctuation française,espacement autour des signes de ponctuation
```

