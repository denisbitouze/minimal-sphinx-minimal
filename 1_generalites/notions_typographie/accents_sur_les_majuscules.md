# Doit-on mettre les accents sur les majuscules ?

Oui ! Au début de l'informatique, le codage sur 7 bits a obligé à réduire le nombre de caractères différents utilisables par les ordinateurs, et les majuscules accentuées ont paru accessoires aux concepteurs (américains) de l'époque. Ce n'est plus actualité. De plus, la saisie des accents sur n'importe quelle lettre est très simple en LaTeX.

Voici un exemple :

```latex
PALAIS DES CONGR\`ES
```

Une réponse plus détaillée est donnée sur le [site Druide.com](https://www.druide.com/fr/enquetes/faut-il-accentuer-les-majuscules-et-les-capitales).

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie
```

