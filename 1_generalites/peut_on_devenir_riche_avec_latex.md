# Peut-on gagner de l'argent avec LaTeX ?

:::{note}
Le contenu actuel de cette page provient du [LaTeX Ninja blog](https://latex-ninja.com/2019/02/02/earning-money-with-latex/), avec l'aimable autorisation de son auteur, Sarah Lang.
:::

```{eval-rst}
.. todo:: Ce contenu est destiné à être traduit en français et adapté au style de la FAQ.
```

## Experiences in the TeX/LaTeX community

This is the Twitter post where I asked fellow TeX lovers for their experiences to include in this post :

[@latex_ninja](https://twitter.com/latex_ninja) : Currently working on a post on “How to earn money with LaTeX / building a career with LaTeX”. I would be very happy to learn about your experiences doing paid work related to LaTeX, [#TeXLaTeX](https://twitter.com/hashtag/TeXLaTeX) community!

Now I will sum up the answers I have received. But before I do : a big thanks to the #TeXLaTeX community -- the friendliest and most helpful people ever!

### Où et comment puis-je travailler en utilisant LaTeX ?

D'après le ton général des commentaires, il semble que la communauté -- comme moi -- pense que la façon la plus confortable et la plus sûre de monétiser ses compétences en LaTeX est dans un environnement universitaire où vous seriez amené à utiliser LaTeX pour la composition, sans que ce soit forcément l'objet principal de votre poste. Vous pouvez être amené à utiliser LaTeX de manière intensive -- si vous avez de la chance et si vous recherchez les occasions de le faire -- mais vous ne dépendez pas non plus entièrement de lui.

Il est assez difficile de travailler en tant que freelance en proposant uniquement LaTeX, à moins d'avoir de bons contacts dans un environnement où la composition en TeX fait partie de la vie quotidienne. Cependant, si vous aviez la chance de trouver un tel environnement, il est probable que vous auriez beaucoup de concurrence et que les personnes qui utilisent constamment LaTeX n'auraient de toutes façons pas besoin d'aide. Du moins, pas assez souvent pour que vous puissiez vivre de ce seul service. Des entreprises comme [le-tex](https://www.le-tex.de/en/index.html) proposent non seulement LaTeX, mais aussi des services autour d'XML, InDesign ou MS Office. Il s'agit donc plutôt d'une gamme globale de services de composition et que d'une simple offre de prestations en LaTeX. Il y a cependant des indépendants qui utilisent LaTeX. Martin Sievers (du {doc}`groupe d'utilisateurs </1_generalites/gutenberg>` [allemand DANTE e.V.](https://www.dante.de/)) m'a gentiment mentionné certains d'entre eux :

Martin Sievers, [@TeX4Publication](https://twitter.com/tex4publication) Jan 19 : Companies like [@letexml](https://twitter.com/letexml) usually offer more than just LaTeX (XML, InDesign, Office). [@TeXhackse](https://twitter.com/TeXhackse), [@speedata](https://twitter.com/speedata) and [@khzimmer2](https://twitter.com/khzimmer2) could maybe tell you more on freelancing.

Il semble que les centres d'[humanités numériques](https://fr.wikipedia.org/wiki/Humanités_numériques) soient des endroits où les ouvriers utilisant LaTeX ont trouvé des environnements conviviaux où ils peuvent faire de leur passion une source d'argent (entre autres tâches). C'est assez logique. Étant donné que le développement de [XSL-FO](https://fr.wikipedia.org/wiki/XSL-FO) est arrêté depuis 2012 et que LaTeX se porte toujours aussi bien, il est normal de progressivement transformer les données des sciences humaines en LaTeX pour créer des documents imprimables. À cela s'ajoute le grand choix de possibilités et d'extensions utiles que LaTeX offre pour des besoins (à peu près) uniques aux sciences humaines (comme l'extension {ctanpkg}`reledmac` pour les apparats critiques complexes ou la composition de polices non-latines ou de caractères non-standards, par exemple).

### À quoi ressemblera probablement le travail ?

Ceux qui vivent de LaTeX disent qu'ils créent surtout des maquettes ou font du back-office. Les conseils que j'ai reçus n'étaient pas tous intuitifs pour moi, comme l'insistance sur le fait qu'il est important que vous soyez capable de discuter des solutions avec les clients. En effet, même si pour vous, la passion pour la composition et les aspects techniques de LaTeX sont au cœur de ce que vous imaginez, vous serez avant tout un prestataire de services. Un métier aux coutumes très particulières -- comme le fait que votre client a toujours raison et que vous devrez lui donner le résultat qu'il souhaite, et pas seulement vous amuser à programmer ce que vous pensez être le plus élégant (ou le plus amusant). C'est un aspect que les gens ont tendance à oublier lorsqu'ils essaient de transformer leurs passions en emplois (ou déménager sur leur lieu de vacances préféré en tant qu'expatriés). Une fois qu'il ne s'agit plus d'un simple loisir, mais d'un véritable gagne-pain (ou de votre vie quotidienne), vous pouvez avoir un ressenti très différent du passe-temps que vous aimiez. La réalité ne sera peut-être pas le rêve que vous aviez imaginé. Êtes-vous sûr d'être prêt pour cela ?

Si vous êtes conscient de ces aspects, que vous savez plus ou moins à quoi vous attendre et que vous pensez toujours que c'est une bonne idée, alors ça l'est probablement et vous devriez au moins essayer. Je vous expliquerai plus loin dans cet article la meilleure façon d'aborder la question.

En ce qui concerne la prestatation de services, le travail freelance en LaTeX ressemble beaucoup au travail typique des Humanités numériques : vous devez communiquer vos connaissances techniques de manière à ce qu'elles soient compréhensibles pour quelqu'un qui n'a peut-être pas ce bagage technique. Les clients peuvent avoir des idées ou des besoins très spécifiques, même s'ils ne sont pas conformes à ce que vous pensez être le mieux. Certains d'entre eux ne veulent pas de bonnes solutions, mais plutôt des solutions rapides, bon marché et sales. Si vous envisagez de faire du consulting LaTeX votre métier, il vaut être prêt à accepter cela. Même dans les humanités numériques, un domaine où l'on comprend généralement mieux une technologie comme LaTeX, les gens n'apprécieront peut-être pas sa beauté de la même façon que vous. La plupart des gens veulent juste que les choses soient faites, et rapidement, que l'outil soit beau ou pas.

Marei, @TeXhackse Jan 28

Another thing, Patrick also mentioned : There will be people just looking for cheap quick and dirty solutions. This is nothing a LaTeX-lover can enjoy. But this for money. Anyone will have to do this from time to time. \[...\] I work with Universities generating corporate Design templates and automation mechanisms for technical documentation or business correspondation or contracts. Cool thing is, I can choose which projects I accept.

Certains d'entre eux précisent aussi que leurs projets sont moins axés sur le travail en freelance que sur l'idée générale de l'Open Source, qui consiste à diffuser une technologie qui en vaut la peine. Certains ciblent les personnes ayant une formation technique, qui utilisent déjà beaucoup LaTeX mais qui pourraient avoir besoin d'aide pour les détails, tandis que d'autres se concentrent sur l'apport de cette formidable technologie aux personnes qui ne font pas encore partie du «groupe cible» typique. C'est également ce que je fais avec ce blog : j'essaie de fournir un contenu intéressant pour ceux qui sont déjà des utilisateurs avancés de LaTeX et qui veulent s'améliorer, mais d'un autre côté, j'essaie de fournir des bases de départ pour un public qui n'est pas encore convaincu de l'utilité de LaTeX.

La commercialisation de vos services de composition ressemble beaucoup à celle de n'importe quel type de conception graphique en freelance (je suppose). Vous aurez un public cible assez restreint, mais les gourous disent toujours qu'il faut trouver sa niche. Alors peut-être que c'est ça. J'ai personnellement gagné de l'argent en créant des affiches avec LaTeX, par exemple. Le logiciel n'était pas une obligation, j'étais libre de produire le résultat de la manière que je voulais. Mais LaTeX est ce que je fais de mieux, alors c'est ce que j'ai fait. À cet égard, c'était comme n'importe quel autre travail sous contrat. Notez que contribuer au projet LaTeX ne vous apportera probablement pas grand-chose (financièrement parlant) puisqu'il s'agit d'un projet Open-Source.

Donc, pour résumer, «travailler en utilisant LaTeX» va du développement de vos outils basés sur LaTeX, au travail en tant que consultant LaTeX en passant par la création de maquettes. Maintenant, je veux poursuivre avec quelques conseils concrets sur la façon de s'y prendre.

## Conseils pratiques

### Trouvez un job où vous pourrez utiliser LaTeX, pas un où vous êtes spécifiquement embauché pour le faire

Intégrer LaTeX dans votre travail actuel peut être relativement aisé, en fonction de votre emploi. Ou bien choisissez un poste où il sera probablement possible d'intégrer LaTeX, puis bâtissez-vous une bonne réputation et peut-être obtiendrez-vous plus de travail impliquant LaTeX et serez-vous sollicité pour travailler officiellement avec LaTeX. Il sera beaucoup plus facile d'emprunter cette voie que d'essayer de se lancer directement comme freelancer LaTeX. Et, encore plus important, ce sera beaucoup plus sûr pour vous. Je ne vous conseille pas de quitter votre job actuel pour essayer de travailler dans un domaine où vous ne savez pas encore si vous avez vraiment un marché et si la demande est suffisante pour que vous puissiez en vivre.

### Suis-je assez compétent pour effectuer un travail rémunéré avec LaTeX ?

Si vous pouvez décrocher un emploi rémunéré en le faisant, je dirais oui. N'attendez pas d'être un « expert », car c'est un terme subjectif et vous ne vous sentirez peut-être jamais expert, même dans 10 ans. Si vous voulez vous lancer, autant le faire maintenant. Au cas où vous seriez coincé dans un projet, vous pouvez toujours chercher ce dont vous avez besoin sur internet. Si vous êtes capable de chercher des informations en ligne, de les adapter à vos besoins et de les faire fonctionner, vous êtes prêt. Par ailleurs, si vous n'êtes pas encore certain de pouvoir facturer vos services, travaillez gratuitement sur 1 à 3 petits projets non critiques ni urgents. Cela vous permettra de vous constituer un portfolio et de gagner en confiance. Si les choses tournent mal, vous n'aurez pas à vous inquiéter outre mesure puisque vous n'aurez pas été payé.

De plus, je suppose que vous ne serez sans doute pas en mesure de décrocher un emploi rémunéré immédiatement. Commencer par un projet gratuit peut donc être une bonne solution. Cela vous permet également d'évaluer si le service que vous souhaitez fournir est réellement demandé. Si vous n'êtes pas sûr des compétences que vous possédez, ne proposez que ce que vous êtes sûr de pouvoir gérer et tout ira bien.

### Commencez par un projet secondaire

« Testez » votre idée d'entreprise dans le cadre d'un *side-project*. Essayez de trouver une petite tâche rémunérée à accomplir. Ainsi, une fois que vous aurez décidé de commencer à travailler réellement avec LaTeX, vous aurez déjà des clients prêts à acheter. C'est le conseil que donnent la plupart des guides sur la façon de devenir freelance ou entrepreneur, et c'est un conseil sensé. Ne quittez pas votre emploi si vous n'êtes pas sûr de pouvoir vivre de votre nouvelle idée d'entreprise. Évaluez si l'idée vaut la peine de se lancer et recueillez des preuves objectives en obtenant un travail rémunéré. Voyez si vous aimez vraiment ce travail (même si vous aimez LaTeX, vous pourriez finir par détester le faire pour de l'argent et aux conditions du client). Commencez comme une activité secondaire et ne passez à autre chose qu'une fois que c'est rentable. Lisez {isbn}`Soft Skills <9781617292392>`// de John Sonmez et le //{isbn}`Software Dev Career Guide <978-0999081419>` pour trouver plus de conseils sur la création de votre propre entreprise.

### Some sample job descriptions from Upwork

Also, to get started freelancing, think about taking a few mini jobs on online freelancing platforms. For example, I found little jobs like this doing a quick Google search :

Looking for experienced developer who can automate latex converter from multiple source.

Convert word document (Mathematics study booklet) into LaTeX using a given template. I’m looking for a competent LaTex user who knows Mathematics as well.

There are around 10 documents that need to be converted into a LaTeX template.

Template is already made for you to use.

I have uploaded a sample booklet using the template and a sample content document (Bivariate Data). Each booklet can be anywhere between 10-40 pages but some are already typed. Some are hand written. I need someone who has clean code. I require someone who can use Ti*k*Z and/or Pgf-plot so that you can draw diagrams neatly. Please don’t insert images.

Create print ready equations. Equations were created with Word’s equation editor, poor consistency. Please see a sample of an engineering book that contains many equations that were created in Word’s equation editor. I would like equations in the kindle and print file to look cleaner. I understand that there is software (GrindEQ) that can do this quickly. Ideally the whole file could be corrected at once wthe the equations remaining in their place within the text.

Help with citations in Latex. I have written an MBA thesis in Latex, using {doc}`Overleaf.com </2_programmation/compilation/compiler_en_ligne>`. I need to follow the APA citation style, but I am having a problem doing so. \[...\] I am also having a hard time indicating in my code how the citations for a website and their date of retrieval are to be shown. \[...\] **3.** While I initially, in the latex code, introduced single empty lines between each paragraph and the next, somehow these do not appear anymore when I compile my code. I must have made a mistake somewhere. I need these single empty lines back. \[...\] The deliverables are as follows : **1.** References that follow the citation style indicated in the above link. **2.** Website references are to follow a certain style. **3.** Introduction of single empty lines between each paragraph and the next. You are kindly asked to indicate which lines of code you have written to implement any changes.

## Answering questions found online

There is this [StackOverflow question](https://tex.stackexchange.com/questions/135295/how-can-i-make-some-money-out-of-my-tex-skills) on whether you can or how to earn money using LaTeX and I think there was [this one on Quora](https://www.quora.com/How-can-I-make-money-from-the-LaTeX-programming-language) where they said you couldn’t make money from LaTeX. This was the inspiration for my post, so I decided to explicitly answer it again.

### The Quora question

This is the Quora answer to the simple question :

« [How can I make money from the LaTeX programming language?](https://www.quora.com/How-can-I-make-money-from-the-LaTeX-programming-language) ». I am not entirely sure the person actually meant it because I find it highly biased and frankly, weird and totally unrealistic :

Some people do like [PRAGMA ADE](http://www.pragma-ade.nl/), except that they developped {doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>`, a better LaTeX with some [Lua](https://fr.wikipedia.org/wiki/Lua) empowered TeX engine inside, so they are real world experts about TeX and typing nice documents. Also, there are a lot of people out there that know how to type LaTeX documents, so you’ll have a lot of competition. And the LaTeX skill is not in short demand (less jobs). And typing a lot of LaTeX documents a day, will probably bore you to death at some time or create some Repetitive Stress Injury. So, I wouldn’t recommand the business model of earning money through LaTeX. But hey, it’s just my opinion. I can be wrong ^^

The commenter says correctly that some people do make money from it. But then, incorrectly states that « the LaTeX skill is not in short demand ». I find that advanced LaTeX skills are, in fact, not all that common. Sure, anybody can quickly type up a LaTeX document which will compile. But more complicated uses ? Not in my experience. Also the comment on repetitive stress injury ? Honestly ?

### Discussing the StackOverflow discussion

[Voici la question posée sur StackOverflow](https://tex.stackexchange.com/questions/135295/how-can-i-make-some-money-out-of-my-tex-skills) :

After helping a friend designing a flyer for a telecom company (for which she got decent remuneration), I find myself wondering whether there is a market for freelance TeX-related jobs, on which I could perhaps make some extra bucks for rainy days. As an amateur TeXnician with limited general knowledge of graphic design, but with a growing portfolio of decent-looking documents produced with TeX & friends :

1. Who, if anyone, should I attempt to sell my TeX skills to ?
2. Would, say, a publishing company be interested ? Or do they generally seek people with a much stronger background in TeX & friends than mine ?
3. If you are directly getting paid on account of your TeX skills, how did you get started ?

Ci-dessous, je cite certaines des réponses et les commente.

My answer is neither authoritative nor positive, so just a comment : the problem is that the major consumer of TeX, academia, consists of a lot of very proud, independent people who mostly don’t value skill in it but wouldn’t let anyone else do it for them. You will therefore not make any money from them. You might make money from their academic publishers but I doubt they need freelance work; since the authors all write their papers themselves they probably keep a TeX editor around to standardize them. I have never found a good answer to your question. --- Ryan Reich Sep 27 ’13 at 1:58

Pourquoi certaines personnes ont-elles tendance à insulter le monde universitaire tout entier sans rien savoir de lui ? Pourquoi les gens pensent-ils que c'est acceptable ? J'ai également remarqué ce discours général anti-universitaire tout récemment dans le livre {isbn}`Antifragile <978-1400067824>` que j'avais envie de lire, mais que j'ai refermé parce que j'avais l'impression d'être constamment insulté... Le monde universitaire est plutôt diversifié et tout le monde ne s'y ressemble pas. Mais si vous n'aimez pas les gens (à cause de vos préjugés par exemple) et que vous le laissez paraître, ils le remarqueront probablement et, par conséquent, ne vous traiteront pas aussi gentiment qu'ils le feraient normalement. Je travaille dans le milieu universitaire et oui, bien sûr, il y a des gens arrogants et difficiles, mais la plupart du temps, ce sont des gens très gentils qui seront heureux que vous les aidiez ! On peut trouver des personnes arrogantes dans n'importe quel domaine, et -- sans vouloir insulter qui que ce soit -- l'arrogance est en fait assez répandue chez beaucoup de spécialistes techniques, alors je ne vois pas vraiment pourquoi ils aiment porter des jugements globaux sur les universitaires alors que leur domaine n'est pas vraiment meilleur. En général, les universités sont remplies de personnes qui sont censées travailler comme une armée d'un seul homme et à qui on demande d'exceller dans des tas de compétences qui ne leur ont jamais été enseignées officiellement (de la comptabilité à la mise en page, en passant par la rédaction de demandes de subventions et, bien sûr, l'enseignement). Ils seront donc reconnaissants de recevoir de l'aide.

Au moins, j'espère apporter avec ce billet la bonne réponse à cette question, qu'il dit n'avoir jamais trouvée jusqu'à présent.

I have friends who take papers that have been TeXed by the authors, and “augment” them to work with the journal’s specific packages, etc. They make a good deal of money (30 USD/hr, 40+ hours per week). EDIT : I should mention they are employed by the journal. --- Steve D Sep 27 ’13 at 6:10

Certains mentionnent des personnes employées par des journaux qui « améliorent » ou corrigent les articles soumis et peuvent en vivre.

I’d say that wherever the end product needs to be a PDF, there’s your market. Where TeX has an edge over competition, IMO, is automatic generation of documents. Think of catalogues generated from relational databases and stuff like that. --- morbusg Sep 27 ’13 at 7:22

Oui, *voilà*! C'est tout à fait mon avis !

@RyanReich I agree with you that one cannot make money from academic stuff, but at least in my case this is due to the fact there is in general no money in academic sphere. \[...\] The market might be scientific or technical books, where TeX has its advantages. --- Pygmalion Sep 27 ’13 at 7:48

> @Pygmalion I think that many full professors have sufficient funding to hire people to do clerical work for them,

and in many cases do. However, even if they break the barrier of letting someone else write the paper for them, it is in any case easier and cheaper to make that person an undergrad and not a freelancer. --- Ryan Reich Sep 27 ’13 at 14:11

Eh bien, ici je peux fournir quelques renseignements connus des seuls initiés : il n'y a pas beaucoup d'argent dans le monde universitaire, contrairement à ce que pensent beaucoup de gens de l'extérieur. En particulier dans les sciences humaines, tout le monde est fauché et il n'y a pas d'argent. Cependant, vous découvrirez qu'après tout, il y peut y avoir un peu d'argent pour des tâches comme celle-ci, de temps en temps.

En ce qui concerne les stagiaires de premier cycle : oui, c'est sans doute vrai. 😉

You can make some money. Getting even the rates of a typical software freelancer is impossible. --- Martin Schröder Sep 27 ’13 at 7:04

Bien sûr, si vous êtes un développeur de logiciels indépendant, car les gens obtiennent des rémunérations beaucoup plus élevées pour de la programmation et de l'informatique en général. Si vous avez l'habitude de travailler dans le domaine des sciences humaines à l'université, vous serez probablement en mesure de gagner correctement votre vie et de trouver un emploi que vous aimez, si vous êtes suffisamment patient.

Les commentaires mentionnent que les services gratuits, comme [LaTeX typesetting](https://www.latextypesetting.com/), pourraient ruiner votre business (d'après ce que j'ai vu sur le site web, ce n'est même pas un service gratuit...), puisque c'est de la concurrence qui donne une valeur nulle au service. Mais je ne pense pas que ce soit vraiment un point pertinent. Bien sûr, il existe des ressources gratuites, mais une personne prête à payer ne représente pas le même client que celle qui fera le travail elle-même si on lui fournit les bons outils. Vos clients sont ceux qui ne peuvent ou ne veulent pas faire le travail eux-mêmes, alors ne vous laissez pas effrayer par ces commentaires.

I don’t consider a blunder to advertise this site : It is not my concurrent. I’m not aiming at customers with more or less good latex knowledge and the skill and the time to ask single, well phrased questions with MWE which I can answer in 10 minutes : the overhead for bookkeeping etc would eat up the gain. My customers have a lot of errors, large (often faulty) templates, no clue where to start to solve their problems or to implement some requirements and time pressure. They want continuous support over days (and also for “dumb” or localized questions). --- Ulrike Fischer Sep 27 ’13 at 7:46

Making your source code inaccessible to your customers should also be considered to prevent others from using it free of charge. (one of my business strategies) --- kiss my armpit Sep 27 ’13 at 8:04

In the LaTeX-support business it is up to impossible to keep the code away from the customer. It is also impossible to avoid that they learn how to handle latex problems themselves : that’s actually a vital part of my service. Also -- sorry -- my main aim is not to milk as much money as possible from other people. I have been helping people for free for a lot of years and I don’t intend to stop. My customers get all the code -- they can add it to the tons of free code floating around anyway and (re)use it if they have the skills to do it. --- Ulrike Fischer Sep 27 ’13 at 9:20

Les réponses d'Ulrike Fischer sont très pertinentes, bien sûr.

The only way I would assume realistic is by providing typing services to professors, but that usually requires domain-specific knowledge... So, while a nice skill to have, Latex is not something that will make you rich.

La mise en page de documents pour les enseignants-chercheurs ne nécessite pas nécessairement des connaissances spécifiques au domaine. Une idée générale de ce qui s'y passe et un certain intérêt pour le sujet ne font pas de mal, cependant. Par ailleurs, comme le suggèrent certains commentaires, vous n'écrirez pas l'article d'un chercheur et il ne considérera pas que votre travail empiète sur le sien. C'est ridicule et cela montre simplement que les commentateurs ne connaissent pas le monde universitaire. L'édition universitaire (avec l'évaluation par les pairs, etc.) est une activité qui repose beaucoup sur du travail collaboratif, et c'est une partie tout à fait normale du travail universitaire.

De plus, c'est vrai, ça ne vous rendra probablement pas riche. Le travail dans les sciences humaines non plus. Mais c'est une passion pour beaucoup de gens et le fait d'aimer son travail a une valeur en soi.

### Je réponds encore une fois à la question

Oui, vous pouvez tout à fait gagner de l'argent avec LaTeX. Mais comme pour tout autre travail, vous devez vous placer dans un environnement où il y a une demande suffisante pour le service que vous fournissez. Par conséquent, vous devez vous trouver dans un endroit où les choses sont mises en page et publiées. Votre première rencontre pourrait se faire par une maison d'édition. Mais il sera probablement difficile d'y entrer sans références professionnelles et la maison d'édition aura déjà ses propres méthodes de travail. S'ils n'utilisent pas déjà LaTeX et qu'ils ne voient pas pourquoi ils devraient changer tout leur flux de travail, ils ne vont probablement pas embaucher quelqu'un sans qualification ou expérience dans le domaine de la publication et lui confier la grande responsabilité de tout révolutionner.

Donc je recommanderais plutôt de commencer avec les universités. Des tonnes de choses sont publiées ici, qu'il s'agisse de dépliants d'information, d'affiches publicitaires, d'affiches de conférence, de présentations PowerPoint ou de livres. La demande est énorme, mais il n'y a pratiquement pas de professionnels de la composition pour y répondre. Ici, l'édition est une activité que les gens pratiquent le plus souvent en plus de leurs responsabilités professionnelles et considèrent souvent qu'elle est ennuyeuse et chronophage. Dans les universités, de nombreuses personnes n'ont jamais appris officiellement à mettre en page, éditer et publier des documents, et pourtant elles doivent constamment le faire. Beaucoup d'entre elles sont très occupées et peuvent même avoir les moyens de sous-traiter certains travaux (comme les professeurs), de sorte que vous pourriez réellement travailler pour eux au lieu de vous contenter de les consulter. Pour les autres, qui n'ont pas beaucoup de ressources à leur disposition, toute aide serait très appréciée. La plupart de ces personnes ne sont pas elles-mêmes des professionnels de la typographie. Elles ne se soucieront donc probablement pas ou ne remarqueront pas que vous n'êtes pas un professionnel ou que vous n'avez pas de qualification officielle. Donner un coup de main ici est un moyen peu risqué d'acquérir de l'expérience. Sans parler de tous les étudiants qui pourraient avoir besoin d'aide pour la composition de leur thèse. Vous devez simplement vous assurer que les gens savent quels services vous offrez. Les universités peuvent également rechercher des services de conception graphique, etc. Cela dit, ce n'est probablement pas quelque chose pour lequel vous serez engagé sans expérience préalable.

## Ciblez les « éditeurs par accident » & offrez une valeur ajoutée attractive

Vous pourriez commencer à travailler en free-lance pour aider les milliers d'«éditeurs par accident» qui hantent les universités. Dans ce cas, il sera très important qu'ils connaissent vos services (faites de la publicité sur le campus) ou qu'ils vous contactent personnellement. Si vous le demandez gentiment, des âmes charitables seront peut-être disposées à transmettre votre contact via la liste de diffusion de leur département ou de leur faculté. Il y a toujours quelqu'un qui a besoin d'aide pour de la mise en page. Vous pourriez même être compétitif par rapport à des concurrents plus expérimentés en étant simplement beaucoup moins chers (soyez quand même sûr de ne pas perdre d'argent en baissant vos tarifs).

La plupart du temps, les universitaires n'ont pas besoin des résultats parfaits qu'ils pourraient obtenir d'un « vrai » professionnel. Ils ont seulement besoin de quelque chose de correct, quelque chose qui soit juste un peu mieux que ce qu'ils auraient pu faire eux-mêmes. Dans mon expérience, les gens étaient surtout ravis de certains résultats super simples que je leur fournissais. Ils ne sont pas forcément en mesure de juger si la qualité est meilleure que celle à laquelle ils sont habitués. Le simple fait que LaTeX soit nettement *différent* de MS Word les distingue de leurs collègues, même s'ils ne sont pas *vraiment* en mesure d'apprécier ou de juger la supériorité typographique.

## Définir son profil

Lorsque vous définissez votre stratégie marketing, assurez-vous d'avoir une idée claire des services que vous êtes prêt à fournir : par exemple, les services informatiques des universités pourraient être un endroit où vous pourriez trouver un emploi si vous vous concentrez sur l'aide à l'installation et au démarrage de LaTeX, un peu comme [Troubleshooting-TeX](http://www.troubleshooting-tex.de/index-en.php) peut-être. Les universités ont également tendance à avoir leurs propres maisons d'édition, ce qui pourrait être un endroit où vous pourriez vous retrouver. L'édition universitaire peut également être intéressante si vous êtes prêt et capable de combiner votre travail LaTeX avec la correction d'épreuves, etc. Les universités techniques et les maisons d'édition utilisent fréquemment LaTeX dès qu'il s'agit de mathématiques, à cause des formules.

## Mon expérience personnelle

En fait, oui, ça m'est déjà arrivé : j'ai déjà gagné de l'argent en faisant du LaTeX. Je mettais en page des textes pour des expositions. J'ai également mis en page deux de mes propres livres (ce qui n'était pas, à proprement parler, un travail rémunéré, mais au moins la preuve que ma mise en page avait été acceptée par une maison d'édition). Je fais un peu de LaTeX dans le cadre de mes travaux en humanités numériques (affiches de conférences, extraction des données de notre dépôt, création d'une feuille de style pour les extractions en PDF de notre dépôt, création de feuilles de travail pour un projet numérique scolaire). Je fais de la mise en page pour une société scientifique dont je fais partie. J'ai enseigné un peu de LaTeX dans un cours sur l'annotation. J'aide des amis à utiliser LaTeX. Comme vous le voyez, c'est beaucoup de travail non rémunéré. Mais cela permet de se forger des références et, de temps en temps, lorsqu'il y a de l'argent disponible pour des travaux de composition, je peux en bénéficier.

## Par où commencer ?

Donc, pour répondre à la question principale, à savoir si vous pouvez gagner de l'argent en utilisant LaTeX, je dirais : oui ! Mais... ce sera plus facile dans un environnement universitaire et il vaut mieux le faire étape par étape en construisant vos références pendant que votre source de revenus vient principalement d'une autre activité. Ensuite, une fois que vous aurez fait cela (publiquement) pendant quelques années, vous pourrez peut-être le faire à temps plein. Cependant, je ne vous conseille pas de viser le plein temps très rapidement. Essayez plutôt d'obtenir un premier emploi rémunéré en rapport avec LaTeX et soyez actif dans la communauté. Assurez-vous que les gens savent que vous proposez des travaux rémunérés en LaTeX et faites-en la publicité de manière subtile. Les gens ne réalisent peut-être pas encore qu'ils pourraient avoir besoin de quelqu'un qui fait du travail LaTeX contre de l'argent. Si notre entourage ne sait même pas ce qu'est LaTeX et quels en sont les avantages, vous ne trouverez probablement pas de travail. Intéressez et sensibilisez les gens à LaTeX. Mentionnez-le dès que vous le pouvez (sans devenir ennuyeux quand même !).

Vous pourriez également être en mesure de décrocher un emploi « normal » dans le domaine de la composition ou même de la conception graphique et de le faire avec LaTeX au lieu des autres outils disponibles (du moins, c'est ce que j'ai fait). Vérifiez si les gens aiment les résultats. Faites-en la publicité. Faites de LaTeX votre niche et assurez-vous que les gens la connaissent.

Vous pouvez donner des cours sur LaTeX (dans les universités, les maisons d'édition, etc. --- la demande n'est pas énorme, mais si vous pouvez trouver votre niche de clients, c'est possible). Encore une fois, l'environnement universitaire est votre ami.

Placez-vous dans un environnement où la demande en matière de mise en page et d'édition est élevée, comme une université.

Apprenez aux doctorants à composer leur thèse. Proposez de convertir les documents MS Word en LaTeX (en même temps que la correction des erreurs éventuelles, voire la relecture si vous êtes sûr d'en être capable).

Dans les universités, des foules de gens finissent comme éditeurs alors qu'ils n'ont jamais appris à mettre quoi que ce soit en page. Ils sont généralement autodidactes et se débrouillent bien, mais vous pouvez soit répondre à leurs besoins et leur proposer de faire de la mise en page pour eux (s'ils ont le budget pour engager quelqu'un pour le faire), soit proposer des cours pour les «éditeurs par accident». Et il y en a vraiment beaucoup dans les universités, croyez-moi. La plupart d'entre eux seraient sans doute plus attirés par une initiation à Adobe Illustrator. Mais, selon la quantité de formules qu'ils ont à composer, ils pourraient être intéressés.

De nombreuses personnes ont déjà entendu parler de LaTeX, mais l'obstacle initial à franchir est trop important. Si vous pouvez faciliter cette initiation et la rendre agréable, vous pourriez vous retrouver avec des clients précieux. Vous devez juste être capable de les convaincre, pourquoi ils devraient prendre la peine d'utiliser LaTeX. Vous trouverez quelques tentatives d'explication sur mon blog. 😉 N'hésitez pas à les utiliser pour votre propre marketing !

## Mais si j'apprends à quelqu'un à le faire lui-même, ne vais-je pas perdre les rares clients dont je dispose ?

Eh bien, non. Avez-vous déjà lu {isbn}`Steal like an artist <978-2761941143>` d'Austin Kleon ? Pour le résumer très brièvement, il affirme que dans le travail créatif, nous « volons » constamment, de toutes manières. C'est ce qu'on appelle l'inspiration. Le marché de la création s'épanouit donc si nous partageons. Il ne sert à rien d'essayer de cacher votre travail ou de garder vos connaissances secrètes de peur de perdre votre autorité et/ou votre argent. Partager du contenu de valeur est essentiellement ce que recommandent aujourd'hui presque tous les gourous du développement personnel. Si vous partagez du contenu de valeur, vous montrez que vous êtes bien informé, ce qui, en retour, vous confère autorité et notoriété dans votre domaine. Et une fois que les gens sauront que vous avez de la valeur parce que vous avez prouvé, encore et encore, votre capacité à créer du contenu de qualité, ils vous solliciteront également pour des travaux rémunérés.

John Sonmez parle de «marketing entrant», qui consiste à « attirer » des clients potentiels en offrant gratuitement 90 % de votre valeur ajoutée, plutôt que d'essayer d'inciter quelqu'un à acheter un produit. Faites la publicité de vos compétences en offrant gratuitement de la valeur ajoutée et les gens viendront à vous. Il vous suffit d'être patient et persévérant. Lorsque les gens viennent à vous, vous pouvez facturer des frais plus élevés que lorsque vous essayez de faire acheter à quelqu'un un service qu'il ne demandait pas spontanément.

Inversement, personne ne voudra acheter un travail créatif que vous aurez caché pendant des années, de peur de ne rien recevoir en retour. La créativité et la capacité à améliorer ne viennent pas de la création d'une seule grande œuvre. C'est également une opinion générale dans les livres sur le développement personnel. Ils citent souvent l'exemple du professeur d'arts plastiques qui a divisé sa classe en deux groupes : l'un serait noté sur une production parfaite et l'autre serait simplement noté sur la quantité produite. Toutes les très bonnes productions provenaient du groupe « quantité ». En échouant rapidement et souvent, ils avaient affiné leurs compétences, tandis que le groupe « perfection » avait passé tout son temps à théoriser et n'avait pas l'expérience nécessaire pour produire des résultats de haute qualité.

Voici où je veux en venir : Si vous enseignez à quelqu'un, vous lui permettez de faire les choses lui-même. Les gens aiment se sentir indépendants et ils n'aiment pas que les experts cachent leur savoir pour forcer les clients à «acheter leurs compétences». Ils aiment qu'on leur permette de comprendre les bases, mais ils se rendront compte qu'ils ne sont pas encore capables de faire les choses difficiles tout seuls. Ils vous chercheront donc, vous, l'expert qui a fait preuve de compétences et d'un véritable dévouement pour les aider comme ils le souhaitent. Les clients veulent ce qui est le mieux pour eux, pas ce qui est le mieux pour vous. Ainsi, lorsque vous essayez d'inciter les clients à acheter chez vous, vous devez répondre à leurs besoins, et non essayer de les enfumer. C'est ce que l'on appelle couramment une situation « gagnant-gagnant » : vous voulez gagner de l'argent avec votre travail, les clients veulent être aidés de la meilleure façon possible (c'est-à-dire de la façon dont ils le souhaitent).

## Conditions préalables et comment démarrer

Assurez-vous d'abord que vous avez des compétences « actives » en LaTeX, c'est-à-dire que vous pouvez en faire plus que l'utilisateur moyen. Comme dans l'enseignement, il suffit d'avoir une longueur d'avance sur eux. Ensuite, vous pourrez commencer à aider les utilisateurs ayant des besoins avancés. Assurez-vous de savoir déboguer les erreurs courantes.

Si vous vous voyez plutôt comme un technicien, aidez les utilisateurs de Windows à configurer LaTeX. Cela peut facilement devenir un travail rémunéré. Vous vous insérez simplement dans la lignée des assistants techniques. De plus, essayez de faire ce travail dans les universités où les besoins en matière de composition sont importants (peut-être même en utilisant LaTeX).

Si vous souhaitez faire quelque chose dans le domaine de la «[conception graphique](https://fr.wikipedia.org/wiki/Graphisme) », apprenez {ctanpkg}`TikZ <pgf>` comme si votre vie en dépendait, car ce sera vraiment le cas.

### Rendre une copie prête pour l'impression

Ce n'est pas parce que vous connaissez un peu LaTeX que vous saurez nécessairement répondre aux besoins d'une maison d'édition. Essayez d'obtenir l'opportunité de travailler avec eux gratuitement. Là encore, les petites maisons d'édition ou les éditeurs universitaires offrent souvent la possibilité de remettre un PDF final (*prêt-à-clicher*), ce qui revient beaucoup moins cher que de le faire mettre en page par leur entremise. Cela permettra au scientifique en question d'économiser beaucoup d'argent (500 à 2000 € en fonction de la quantité de travail). Proposez à un professeur d'université de composer son livre gratuitement. Ainsi, vous aurez interagi avec l'éditeur au moins une fois, vous aurez une référence à montrer et vous saurez ce que les éditeurs veulent. Ne sous-estimez pas non plus le temps que cela va prendre. Ce n'est pas pour rien qu'un travail comme celui-ci peut rapporter jusqu'à 2000 €. Les publications scientifiques, en particulier, font souvent l'objet d'un million de modifications et de corrections avant d'être prêtes à être imprimées. Ce sera probablement beaucoup plus de travail que vous ne l'aviez prévu. Anticipez cela ! Les maisons d'édition ont également tendance à trouver des problèmes typographiques auxquels vous n'aviez jamais songé jusque-là.

:::{note}
En écrivant ces lignes, j'ai remarqué que j'aurais beaucoup plus à dire sur la façon de planifier et d'aborder le travail avec vos « clients », alors j'ai écrit un [billet supplémentaire à ce sujet](/2_programmation/etre_prestataire_de_services_latex).
:::

Dans les sciences humaines, il est très important que vous appreniez toutes les fonctions offertes par MS Word, de façon à pourvoir reproduire un aspect similaire à Word, mais en mieux. S'ils vous laissent utiliser LaTeX, vous devez d'abord prouver qu'il possède toutes les fonctionnalités de MS Word auxquelles ils sont habitués.

Au cours du processus, vous vous rendrez peut-être compte que produire un PDF qui correspond exactement aux critères de l'éditeur est plus difficile que vous ne le pensez. En tout cas, c'est ce qui s'est passé pour moi. J'avais utilisé LaTeX et je savais comment faire les personnalisations nécessaires pour satisfaire mes encadrants de thèse, mais l'éditeur a fini par exiger un million de changements que je n'avais même pas imaginé faire en LaTeX. Mes encadrants étaient contents avec une sortie joliment mise en page et des références bibliographiques correctes. Les éditeurs, en revanche, veulent une apparence très spécifique. Par exemple, la première chose que j'ai dû changer est la mise en forme des notes de bas de page, dont aucun de mes encadrants ne s'était jamais soucié. Je n'avais jamais personnalisé de notes de bas de page auparavant et je n'avais même pas conscience que cela pouvait être un problème. Donc, si vous n'avez jamais fait l'une de ces choses, je vous suggère de commencer à vous documenter sur le sujet dès maintenant (un prochain billet sur LaTeX pour l'«éditeur par accident» expliquera toutes ces choses).

## L'apprenti sorcier : comment apprendre les ficelles du métier

Si vous n'en êtes qu'au tout début de votre parcours et que vous n'avez pas encore de compétences reconnues, n'essayez pas d'« aider » un professionnel. Vous ne feriez que vous ridiculiser. Si vous voulez commencer à faire du «vrai travail» le plus rapidement possible, offrez une aide non rémunérée à des non-professionnels. Soyez honnête sur le fait que vous n'êtes pas encore un professionnel mais que vous aimeriez vraiment faire du travail gratuit pour eux afin de vous constituer un portfolio.

Apprenez d'un professionnel. Recherchez un professionnel dont vous savez qu'il utilise LaTeX et portez-vous volontaire. Faites le travail qu'il vous délèguera, essayez d'être aussi utile que possible. Écoutez très attentivement les informations privilégiées qu'ils ont recueillies au cours de leurs années d'expérience. Soyez très ouvert sur le fait que vous n'avez pas encore beaucoup de compétences mais que vous êtes prêt à apprendre. Visez un stage (probablement non rémunéré). Cette expérience sera précieuse et vous pourrez l'ajouter à la liste des références de votre CV si vous n'avez pas beaucoup de temps à perdre.

Cependant, si vous voulez vous lancer et décrocher très rapidement des opportunités de travail rémunéré, essayer de gagner de l'argent en utilisant LaTeX n'est probablement pas la bonne voie pour vous. Il s'agit plutôt d'un cheminement de carrière vers lequel vous pouvez vous diriger si vous êtes patient, et que vous vous trouvez dans un environnement adapté à cette quête.

Rejoignez un {doc}`groupe d'utilisateurs </1_generalites/gutenberg>`. Là, vous pourrez apprendre des années d'expérience des membres et peut-être avoir connaissance d'opportunités d'emplois.

## Where is LaTeX used in the real world where there could be jobs ?

### Freelancing

[Upwork](https://fr.wikipedia.org/wiki/Upwork), [Freelancer.com](https://en.wikipedia.org/wiki/Freelancer.com)

<https://www.naukri.com/latex-typesetting-jobs>

[This post](https://tex.stackexchange.com/questions/40720/latex-in-industry) discusses LaTeX in industry and gives some examples, in case you are interested in that.

### Open Access / e Publications

Many e-publication platforms use LaTeX to generate their PDFs. While I don’t know him personally, Lukas C. Bossert creates LaTeX solutions for the [German Archaeological Institute](https://fr.wikipedia.org/wiki/Institut_archéologique_allemand), for example [a citation style](http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/biblatex-contrib/archaeologie/archaeologie.pdf). So, yet again the general takeaway is that you should join an organization with a big publication output and specific needs you can help them with.

## To sum it all up

Yes, you can earn money using LaTeX.

Practical tips : Place yourself in a university environment where there is a lot of demand for typesetting but few typesetting professionals.

Practise LaTeX, learn all the most important things (citations settings, formatting footnotes and bibliographies) for scientific publishing, get credentials from a real publishing house so you can prove you are able to cater to their needs.

## References

- Austin Kleon, {isbn}`Steal like an artist <978-0761169253>`//, 2012 (édition française : //{isbn}`Voler comme un artiste <978-2761941143>`).
- John Sonmez, {isbn}`The Complete Software Developer’s Career Guide : How to Learn Your Next Programming Language, Ace Your Programming Interview, and Land The Coding Job Of Your Dreams <978-0999081419>`, 2017.
- John Sonmez, {isbn}`Soft Skills : The software developer’s life manual <978-0999081440>`, 2014.

(I will do a post on John Sonmez’s work soon, he has some really interesting advice and I highly recommend you read his books. They are also available as audiobooks which I like to listen to while riding my bike to work 😉)

Also, see these two articles concerning LaTeX consulting :

- Boris Veytsman, *TeX consulting for fun and profit*, in : TUGboat, Volume 32 (2011), No. 1, <https://tug.org/TUGboat/tb32-1/tb100veytsman.pdf>
- Amy Hendrickson, *Real Life LaTeX : Adventures of a TeX Consultant*, TeXNorthEast Conference, March 22–24, 1998. <https://tug.org/TUGboat/tb19-2/tb59amy.pdf>

______________________________________________________________________

*Sources :*

- [Earning money with LaTeX?](https://latex-ninja.com/2019/02/02/earning-money-with-latex/)
- [How can I make money from the LaTeX programming language?](https://www.quora.com/How-can-I-make-money-from-the-LaTeX-programming-language)
- [How can I make (some) \$money\$ out of my TeX skills?](https://tex.stackexchange.com/questions/135295/how-can-i-make-some-money-out-of-my-tex-skills)

```{eval-rst}
.. meta::
   :keywords: LaTeX,devenir riche sans effort,utiliser LaTeX commercialement,faire de la prestation de services LaTeX,édition de livres,graphiste LaTeX
```

