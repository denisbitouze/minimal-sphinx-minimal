---
myst:
  substitutions:
    image1: |-
        ```{image} /1_generalites/learnlatex.png
        :target: https://www.learnlatex.org/fr/
        :width: 400px
        ```
---

# Comment faire ses premiers pas ?

Cette FAQ contient à peu près toutes les notions dont vous avez besoin pour commencer à écrire vos documents avec LaTeX **mais** son organisation ne la rend pas forcément pratique pour apprendre. Voici donc quelques pistes à suivre pour bien commencer.

Avant tout, au cas où TeX et LaTeX restent pour vous un concept flou, consultez les questions « {doc}`Qu'est-ce que TeX ? </1_generalites/glossaire/qu_est_ce_que_tex>` », « {doc}`Qu'est-ce que LaTeX ? </1_generalites/glossaire/qu_est_ce_que_latex>` » et ainsi que le dossier {doc}`Bases </1_generalites/bases/start>` qui regroupe quelques questions usuelles de la part des personnes qui débutent. Par ailleurs, pour éviter des confusions par la suite, il pourra être utile de lire les questions « {doc}`Qu'est-ce qu'un moteur ? </1_generalites/glossaire/qu_est_ce_qu_un_moteur>` » et « {doc}`Qu'est-ce qu'un format de TeX ? </1_generalites/glossaire/qu_est_ce_qu_un_format>` ».

## Avec un outil dédié

Si vous souhaitez découvrir LaTeX de façon progressive et devenir rapidement efficace pour produire vos premiers documents, nous vous conseillons de suivre les 16 leçons de ce tutoriel en ligne et en français :

{{ image1 }}

Sa traduction en français a été opérée par l'[association GUTenberg](https://www.gutenberg.eu.org/).

## Avec des vidéos

Un contributeur régulier du newsgroup `fctt` [a réalisé des vidéos d'introduction à TeX](https://www.youtube.com/channel/UCt3zJLNYL8yw0V7Qyi1bI6A) qui permettent de faire ses premiers pas.

## Avec l'aide de la FAQ

Cette FAQ est une mine d'informations sur LaTeX, mais son organisation n'en fait pas un bon outil d'apprentissage. Néanmoins, elle contient plein de pointeurs vers de meilleurs documents d'initiation :

- pour LaTeX, voir « {doc}`Où trouver des introductions à LaTeX ? </1_generalites/documentation/documents/tutoriels/initiations_a_latex_sur_internet>` » ;
- pour TeX, voir « {doc}`Que lire sur TeX ? </1_generalites/documentation/documents/documents_sur_tex>` ».

Notez que cette FAQ est largement orientée vers {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>` (elle recommande donc par défaut ce format) mais vous pouvez souhaiter apprendre {doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>`, qui est un autre *format* basé sur TeX. Dans ce cas, le point de départ (en anglais) est le [wiki ConTeXt garden](http://wiki.contextgarden.net/Main_Page).

Enfin, avant de vous lancer, si vous ne disposez pas d'une installation de LaTeX, vous devez alors récupérer une *distribution* TeX appropriée à votre machine. Les possibilités sont ici décrites dans la réponse « {doc}`Quelles sont les versions de (La)TeX selon le système d'exploitation ? </6_distributions/trouver_les_sources_pour_les_differents_systemes_d_exploitation2>` ». Nous supposons ici que vous pouvez installer des choses par vous-même, ou connaissez quelqu'un qui sait y faire.

```{eval-rst}
.. todo:: *Sans doute à compléter pour donner des sites d'explication d'installation.*
```

______________________________________________________________________

*Source :* {faquk}`Getting started <FAQ-startup>`

```{eval-rst}
.. meta::
   :keywords: Apprendre LaTeX,auto-formation à LaTeX,cours LaTeX en ligne,initiations à LaTeX,commencer avec LaTeX,documentations pour débutants
```

