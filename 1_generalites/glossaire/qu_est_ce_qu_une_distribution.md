# Qu'est-ce qu'une distribution TeX ?

Une *distribution* TeX fournit une collection structurée de logiciels liés à TeX. Généralement, une distribution TeX comprend :

- un ensemble d'exécutables TeX centraux tels que `tex` et `latex` ;
- diverses polices optimisées pour une utilisation avec TeX ;
- des programmes d'aide tels que le traitement de bases de données bibliographiques BibTeX, des éditeurs, des environnements de développement intégrés, des programmes de conversion de format de fichier ;
- de nombreuses extensions LaTeX ;
- des outils de configuration ;
- et tout autre complément que le distributeur choisit d'inclure.

Les distributions TeX les plus courantes sont :

- [MacTeX](http://www.tug.org/mactex/) ;
- [MiKTeX](https://miktex.org/) (également disponible comme base de l'ensemble ProTeXt, distribué sur le DVD de TeX Live) ;
- [TeX Live](https://tug.org/texlive/) ;
- et historiquement s'y ajoutent [ozTeX](http://www.trevorrow.com/oztex/), [CMacTeX](https://www.math.tamu.edu/~tkiffe/cmactex.html) et [teTeX](https://www.tug.org/tetex/).

Certaines distributions TeX ciblent un système d'exploitation ou une architecture de processeur spécifiques tandis que d'autres fonctionnent sur plusieurs plates-formes. Les distributions TeX peuvent être {doc}`gratuites </6_distributions/trouver_les_sources_pour_les_differents_systemes_d_exploitation2>` comme [payantes](/6_distributions/implementations_commerciales).

Ce sujet est plus largement abordé dans la partie « {doc}`Distributions et logiciels </6_distributions/start>` » de cette FAQ.

______________________________________________________________________

*Source :* {faquk}`Things with « TeX » in the name <FAQ-texthings>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,glossaire,vocabulaire,distribution
```

