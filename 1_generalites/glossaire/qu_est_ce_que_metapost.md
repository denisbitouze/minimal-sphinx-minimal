# Qu'est-ce que MetaPost ?

Le système `MetaPost` (de John Hobby) implémente un langage de dessin d'images très semblable à celui de {doc}`MetaFont </1_generalites/glossaire/qu_est_ce_que_metafont>` : la différence est que `MetaPost` produit des fichiers graphiques vectoriels au lieu de bitmaps. Les formats de sortie disponibles sont PostScript et SVG. Une grande partie du code source de MetaPost vient des sources de `MetaFont`, avec la permission de Donald Knuth.

Vers 2007, `MetaPost` a été séparé en un programme frontal et une bibliothèque nommé `MPlib` (qui a ensuite été liée à {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`) par Taco Hoekwater. Cela donne à `LuaTeX` certaines capacités graphiques et constitue une alternative aux extensions de dessin telles que {doc}`TikZ et PSTricks </3_composition/illustrations/dessiner_avec_tex>`.

`MetaPost` est un langage puissant pour produire des figures pour des documents à imprimer sur des imprimantes PostScript, que ce soit directement ou par incorporation dans des documents TeX ou LaTeX. `MetaPost` est capable d'intégrer du texte et des mathématiques, balisés pour une utilisation avec TeX, dans les graphiques. Pour sa part, Donald Knuth n'utilise `MetaPost` que pour les diagrammes dans les textes qu'il écrit.

Bien que `pdfLaTeX` ne puisse généralement pas gérer des graphiques PostScript, une sortie de `MetaPost` est suffisamment simple et normée pour que `pdfLaTeX` puisse la gérer directement, en utilisant du code emprunté à {doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>`. Sur ce sujet, voir la question « {doc}`Comment intégrer des graphiques avec pdfLaTeX ? </3_composition/illustrations/inclure_une_image/inclure_un_fichier_pdf2>` ».

Il existe une liste de diffusion traitant de `MetaPost`. Vous pouvez vous y abonner par [l'interface « Mailman » du TUG](http://lists.tug.org/metapost). Le site web du TUG héberge également une [page dédiée à MetaPost](https://tug.org/metapost.html). Le document {ctanpkg}`tex-overview` vous donne également des informations (et quelques documents explicatifs de base).

______________________________________________________________________

*Sources :*

- {faquk}`What is MetaPost? <FAQ-MP>`
- [MetaPost sur Syracuse](https://melusine.eu.org/syracuse/metapost/) : Manuels & tutoriaux, Exemples (en français).

```{eval-rst}
.. meta::
   :keywords: LaTeX,dessiner avec LaTeX,MetaFont,figures,illustration,langage de dessin
```

