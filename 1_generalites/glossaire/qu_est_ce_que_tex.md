# Qu'est-ce que TeX ?

TeX est un logiciel libre de composition de document écrit par [Donald E. Knuth](https://www-cs-faculty.stanford.edu/~knuth/). Ce dernier indique dans la préface de son livre sur TeX (voir {doc}`Que lire sur TeX ? </1_generalites/documentation/livres/documents_sur_tex>`) qu'il est « destiné à la création de beaux livres --- tout particulièrement ceux qui contiennent beaucoup de mathématiques ». Ceci dit, TeX n'est pas bon *que* pour les livres mathématiques, il s'avère être un très bon système de composition de document en général.

Donald Knuth est professeur émérite d'« art de la programmation informatique » à l'Université de Stanford en Californie, aux États-Unis. Il a développé la première version de TeX en 1978 pour faire face aux révisions de sa série de livres *The Art of Computer Programming*. L'idée s'est avérée populaire et Donald Knuth a produit une deuxième version (en 1982) qui est à la base de ce que nous utilisons aujourd'hui.

Donald Knuth a développé un système de « {doc}`programmation lettrée </5_fichiers/web/literate_programming>` » pour écrire TeX : il a fourni la source de TeX en langage WEB ainsi que des outils pour transformer cette source en un élément qui peut être compilé ou un élément qui peut être imprimé. Il n'y a donc (en principe) aucun mystère sur ce que fait TeX. En outre, WEB fournit des mécanismes pour porter TeX vers de nouveaux systèmes d'exploitation et ordinateurs. Pour avoir une certaine confiance dans les portages, Donald Knuth a fourni un {doc}`test </1_generalites/bases/verifier_la_conformite_de_son_compilateur>` au moyen duquel on peut juger de la fidélité d'un système TeX. TeX et ses documents sont donc hautement portables.

Pour les programmeurs curieux du sujet, la distribution de TeX exerce une certaine fascination : elle n'a rien à voir avec la façon dont on procéderait pour un tel programme de nos jours ; pourtant, elle dure depuis plus longtemps que la plupart des autres et TeX a été porté sur de nombreuses architectures informatiques et systèmes d'exploitation --- autant d'attributs que la plupart des pratiques de programmation modernes visent. La source « lisible » retraitée du programme TeX se trouve dans la version {doc}`en structure TDS </5_fichiers/tds/la_tds>` de la distribution.

______________________________________________________________________

*Source :* {faquk}`What is TeX? <FAQ-whatTeX>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background
```

