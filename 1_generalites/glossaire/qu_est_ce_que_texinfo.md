# Qu'est-ce que Texinfo ?

Texinfo est un système de documentation qui utilise un fichier source pour produire à la fois des informations consultables de façon interactive et une sortie imprimée. Ainsi, au lieu d'écrire deux documents différents, l'un pour l'aide en ligne et l'autre pour un manuel composé, vous n'avez besoin d'écrire qu'un seul fichier source de document. Aussi, lorsque vous souhaitez modifier un document, vous n'avez besoin de réviser qu'un seul fichier. Par convention, les noms de fichiers source Texinfo se terminent par une extension « texi » ou « texinfo ».

Texinfo est un langage à commandes, ressemblant un peu à LaTeX en étant moins expressif que ce dernier. Son code est assez similaire à celui de n'importe quel langage à commandes basé sur TeX, à ceci près que ses commandes commencent par « @ » plutôt que par « \\ » (couramment utilisé dans les systèmes TeX).

Vous pouvez écrire et convertir des fichiers Texinfo en fichiers Info avec GNU Emacs et les lire en utilisant le lecteur Emacs Info. Vous pouvez également convertir les fichiers Texinfo en fichiers Info en utilisant le logiciel `makeinfo` et les lire en utilisant `info`, de sorte que vous n'êtes pas dépendant d'Emacs. Toute distribution comprend un script Perl, `texi2html`, qui convertira les sources Texinfo en HTML : le langage est en effet bien mieux adapté au HTML que ne l'est LaTeX, de sorte que les frayeurs de la {doc}`conversion de LaTeX en HTML </5_fichiers/xml/convertir_du_latex_en_html>` sont largement évitées.

Enfin, bien sûr, vous pouvez également imprimer les fichiers, ou les convertir en PDF à l'aide de `pdfTeX`.

# Comment lire une documentation au format Texinfo ?

Peut-être avez-vous déjà consulté la documentation de `dvips` en tapant (sous Linux) :

```bash
man dvips
```

et la page d'aide affichée vous a dit ☹️ :

```text
NOM
       dvips - convertit un fichier TeX DVI en PostScript

DESCRIPTION
       CETTE PAGE DE MANUEL EST OBSOLÈTE! Consultez plutôt
       la documentation Texinfo.  Vous pouvez la lire soit
       avec Emacs soit avec le programme info fournit dans
       la distribution GNU texinfo à cette adresse :
       ftp.gnu.org:pub/gnu/texinfo/texinfo*.tar.gz.
```

```text
NAME
       dvips - convert a TeX DVI file to PostScript

DESCRIPTION
       THIS  MAN  PAGE IS OBSOLETE!  See the Texinfo docu-
       mentation instead.  You can read it either in Emacs
       or  with  the  standalone  info program which comes
       with    the    GNU    texinfo    distribution    as
       ftp.gnu.org:pub/gnu/texinfo/texinfo*.tar.gz.
```

- Pour consulter l'aide en question dans sa version à jour, il vous suffit de taper dans un terminal :

```bash
info dvips
```

La documentation demandée s'affiche dans le terminal, et il est possible de naviguer avec les touches du clavier, comme dans un document hypertexte. Dans le vocabulaire de Texinfo, chaque sous-page de la documentation est un « nœud ». Pour avoir de l'aide sur les possibilités de navigation entre les nœuds, tapez `H`. Ceci devrait afficher :

```text
Raccourcis Info de base :

H           Ferme cette fenêtre d'aide.
q           Quitte complètement Info.
h           Appel du tutoriel Info.

Up          Monte d'une ligne.
Down        Descend d'une ligne.
PgUp        Recule d'une page.
PgDn        Avance d'une page.
Home        Va au début de ce nœud.
End         Va à la fin de ce nœud.

TAB         Saute au prochain hyperlien.
RET         Suit l'hyperlien sous le curseur.
l           Retourne au dernier nœud vu dans cette fenêtre.

[           Va au nœud précédent de ce document.
]           Va au nœud suivant de ce document.
p           Va au nœud précédent de ce niveau.
n           Va au nœud suivant de ce niveau.
u           Remonte d'un niveau.
t           Va au premier nœud (« top ») de ce document.
d           Va au nœud répertoire (« dir ») principal.

1...9       Choisit la première...neuvième entrée du menu de ce nœud.
0           Choisit la dernière entrée du menu de ce nœud.
m           Sélectionne une entrée de menu par son nom.
f           Suit le renvoi spécifié par son nom.
g           Va au nœud spécifié par son nom.

/           Recherche la chaîne indiquée vers l'avant.
{           Recherche l'occurrence précédente.
}           Recherche l'occurrence suivante.
i           Recherche la chaîne indiquée dans l'index et
              sélectionne le nœud correspondant à la première entrée.
I           Présente un menu des entrées d'index correspondantes.

C-g         Annule l'opération en cours.
```

______________________________________________________________________

*Sources :*

- {faquk}`What is Texinfo? <FAQ-texinfo>`
- [Texinfo](https://fr.wikipedia.org/wiki/Texinfo).

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,GNU,page de man,manuel de référence,fichier texi
```

