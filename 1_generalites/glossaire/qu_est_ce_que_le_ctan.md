# Qu'est-ce que le CTAN ?

[CTAN](https://ctan.org/) est l'acronyme de *Comprehensive TeX Archive Network* (Réseau des archives complètes de TeX), ce qui donne quelques indices :

- c'est un dépôt hébergeant la {doc}`collection complète </1_generalites/documentation/le_catalogue_du_ctan>` des ressources TeX ;
- son contenu est librement accessible par internet ;
- son contenu est répliqué sur de nombreux serveurs dans le monde entier, synchronisés entre eux, formant un « réseau ».

Le principe de ce réseau a été imaginé par un groupe de travail du {doc}`TeX Users Group </1_generalites/gutenberg>` (TUG) créé pour résoudre un problème qui se posait à l'époque : les utilisateurs devaient savoir sur quel site se trouvait telle ou telle extension TeX pour pouvoir la télécharger.

L'implémentation actuelle distingue trois type de serveurs (appelés « nœuds » du réseau) :

- le *cœur* : il s'occupe de la gestion et distribue les fichiers aux miroirs ;
- les *miroirs* : ils se synchronisent sur le cœur, et distribuent les fichiers aux utilisateurs ;
- le *sélecteur* : c'est un méta-service, qui oriente les requêtes des utilisateurs vers un miroir géographiquement proche de chez eux (en utilisant l'adresse IP pour localiser les utilisateurs et sélectionner un miroir dans la même zone géographique).

Un nœud CTAN peut parfaitement supporter d'autres fonctions, comme être également un miroir [CPAN](https://www.cpan.org/) (Perl) ou être un miroir [SourceForge](https://fr.wikipedia.org/wiki/SourceForge.net) (hébergement générique de projets open-source), ou bien encore supporter d'autres services pour la communauté LaTeX.

Parmi les fonctions assurées par le nœud central (« cœur »), on peut citer :

- les téléchargements : les utilisateurs peuvent {doc}`soumettre de nouvelles contributions ou des mises à jour </6_distributions/comment_contribuer_a_ctan>` ;
- les notifications : les changements significatifs au contenu sont signalés via la liste de diffusion [ctan-ann@dante.de](mailto:ctan-ann@dante.de) ;
- la maintenance du catalogue ;
- la [surveillance des miroirs](http://dante.ctan.org/mirmon/).

Les utilisateurs peuvent prendre contact directement avec [l'équipe de gestion du CTAN](https://ctan.org/contact).

L'accès au contenu se fait en général par [le sélecteur d'archives](http://mirror.ctan.org/) : celui-ci utilise la liste des miroirs (et le contrôle de leur bon fonctionnement) et vous rediriger vers un site miroir à la fois proche et suffisamment à jour, pour vous permettre de télécharger efficacement ce que vous voulez. En outre, cette méthode répartit la charge entre les miroirs du CTAN et lisse leur activité.

## Que trouve-t-on sur le CTAN ?

Le CTAN a une volonté d'exhautivité, concernant le monde de TeX.

Si un outil en est absent, il a, de fait, une existence marginale (très peu d'utilisateurs, risque de disparaître de la circulation, fâcherie entre l'auteur et la communauté TeX, etc.). Proposer l'ajout d'un outil dont on est l'auteur {doc}`est une chose très simple </6_distributions/comment_contribuer_a_ctan>`, et il ne faut pas hésiter à le faire.

## Quelle est l'origine du CTAN ?

Avant le développement du CTAN, un certain nombre de personnes mettaient à disposition du public des documents et des extensions TeX à télécharger, mais il n'existait pas de système bien organisé. Lors d'un débat organisé par Joachim Schrod à la conférence EuroTeX de 1991, l'idée est apparue de rassembler les différentes archives disponibles. (Joachim s'était impliqué parce qu'il dirigeait à l'époque l'un des plus grands serveurs FTP d'Allemagne et avait adapté le script [mirror.pl](https://www.ibiblio.org/catalog/items/show/4510) à son usage).

Le CTAN a été mis en place en 1992, par Rainer Schoepf et Joachim Schrod en Allemagne, Sebastian Rahtz au Royaume-Uni, et George Greenwade aux États-Unis (c'est George qui a eu l'idée du nom). La structure du site a été établie début 1992 --- essentiellement par Sebastian --- et synchronisée à partir de début 1993. Le TeX Users Group a mis en place un groupe de travail technique pour organiser le projet. Enfin, l'annonce officielle du CTAN s'est faite à la conférence EuroTeX à Aston, en 1993.

:::{tip}
Le [CPAN,](https://fr.wikipedia.org/wiki/Comprehensive_Perl_Archive_Network) *Comprehensive* **Perl** *archive network*, a été développé en suivant le modèle (et le nom) du CTAN, à partir de 1995. Puis le [CRAN](https://fr.wikipedia.org/wiki/Comprehensive_R_Archive_Network) *Comprehensive* **R** *archive network* à partir de 1997, pour le [langage R](<https://fr.wikipedia.org/wiki/R_(langage)>).
:::

Le premier nœud américain a déménagé deux fois. D'abord, après avoir été à la Sam Houston State University (Huntsville, Texas) grâce à George Greenwade, il a été transféré en 1995 à l'Université du Massachusetts à Boston sous la supervision de Karl Berry. Puis, en 1999, il a déménagé au Saint Michael's College de Colchester, dans le Vermont, où il a été maintenu par Jim Hefferon. En 2011, il a quitté la liste des principaux sites du CTAN.

Longtemps, Cambridge a hébergé le principal nœud britannique, avec le parrainage de [UK-TUG](http://uk.tug.org/) (groupe britannique des utilisateurs de TeX), sous la houlette de Robin Fairbairns. Il a fermé en 2015.

Le réseau français compte 8 à 10 miroirs, majoritairement hébergés par des instituts publics ([IBCP](https://www.ibcp.fr/) à Lyon Gerland, [IRCAM](https://www.ircam.fr/) et [ISPL](https://www.ipsl.fr/) à Paris...), des associations (notamment le groupe francophone des utilisateurs de TeX, [GUTenberg](https://ctan.gutenberg.eu.org/)) et quelques particuliers.

______________________________________________________________________

*Sources :*

- {faquk}`What is CTAN? <FAQ-ctan>`
- [What is CTAN?](https://www.ctan.org/ctan) (sur le CTAN),
- {faquk}`The CTAN catalogue <FAQ-catalogue>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,Comprehensive TeX archive network,package,extension,télécharger LaTeX,où trouver LaTeX?
```

