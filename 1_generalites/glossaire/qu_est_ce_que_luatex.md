# Qu'est-ce que LuaTeX ?

`LuaTeX` consiste en un moteur de type TeX avec un interpréteur [Lua](https://fr.wikipedia.org/wiki/Lua) intégré.

Lua est un langage de script libre, conçu de manière à pouvoir être embarqué au sein d'autres applications afin d'étendre celles-ci. Cette aptitude est utilisée dans LuaTeX : l'interpréteur Lua a accès à de nombreuses structures de données internes que le moteur TeX utilise pour la composition du document, de sorte que le programmeur peut insérer des morceaux de code Lua dans des commandes TeX ou LaTeX, tout comme il peut placer des « rappels » (*call-backs*) pour modifier le comportement de TeX lors de certaines opérations. Ceci le rend particulièrement souple.

Voici un exemple dans lequel du code Lua très simple est intégré à un document LaTeX, en argument de la macro `\directlua` :

```latex
\documentclass{article}
  \usepackage[width=8cm]{geometry}
  \pagestyle{empty}

\begin{document}
On sait compter :
\directlua{
  for x=1,12 do
    tex.print(x)
  end
}%
.
\end{document}
```

Le moteur standard {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` est entièrement rétrocompatible avec TeX. En tant que tel, il reste un système 8 bits utilisant des {doc}`métriques de polices dédiées </5_fichiers/fontes/que_sont_les_fichiers_tfm>`. En revanche, le moteur `LuaTeX` est basé sur Unicode et capable de charger des polices système standards (OpenType).

Contrairement à {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>`, la capacité à utiliser des polices système standards n'est pas intégrée au moteur lui-même mais ajoutée en utilisant du code Lua. Malgré cette différence de conception, LuaTeX et XeTeX arrivent à des résultats similaires en ce qui concerne la gestion des polices (avec des avantages différents à la clef).

Bien qu'il existe des différences voulues entre le comportement de `LuaTeX`, de TeX ou {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>`, pour la plupart des utilisateurs, `LuaTeX` peut être utilisé en remplacement des deux autres. D'ailleurs, pour la plupart des utilisateurs finaux de LaTeX, les subtilités sont transparentes, le noyau LaTeX et l'extension {ctanpkg}`fontspec` fournissant des interfaces.

Pour davantage de détails, voir le [Guide touristique de LuaLaTeX](https://github.com/jejust/lualatex-doc-fr) (en français), de Manuel Pégourié-Gonnard.

:::{note}
{doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>` Mark 4 *nécessite* `LuaTeX` mais c'est maintenant transparent pour l'utilisateur.
:::

______________________________________________________________________

*Sources :*

- {faquk}`What are XeTeX and LuaTeX? <FAQ-xetex-luatex>`
- [LuaTeX](https://fr.wikipedia.org/wiki/LuaTeX) sur Wikipedia,
- [Wiki LuaTeX](http://wiki.luatex.org/) (en anglais).

```{eval-rst}
.. meta::
   :keywords: LaTeX,UTF8,Unicode,LuaTeX,LuaLaTeX,utiliser les polices systèmes,programmation en Lua,polices OpenType
```

