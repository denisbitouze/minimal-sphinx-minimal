# Qu'est ce que XeTeX ?

[XeTeX](http://xetex.sourceforge.net/) est un moteur Unicode TeX qui peut charger les polices système directement en utilisant la bibliothèque [HarfBuzz](https://fr.wikipedia.org/wiki/HarfBuzz), qui est intégrée. Pour ce faire, la primitive `\font` et quelques autres primitives TeX ont été étendues. Pour la plupart des utilisateurs finaux de LaTeX, ces subtilités sont transparentes, le noyau LaTeX et l'extension {ctanpkg}`fontspec` fournissant des interfaces.

Le moteur standard {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` est entièrement rétrocompatible avec TeX. En tant que tel, il reste un système 8 bits utilisant des {doc}`métriques de polices dédiées </5_fichiers/fontes/que_sont_les_fichiers_tfm>`. En revanche, le moteur `XeTeX` est basé sur Unicode et capable de charger des polices système standards (OpenType). En interne, il se distingue de {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` : des résultats similaires sont atteints en utilisant des philosophies très différentes (avec des avantages différents à la clef).

Tout comme TeX, `XeTeX` ne produit pas directement de sortie PDF mais fonctionne *via* un format intermédiaire, XDV (*eXtended DVI*). Contrairement au format classique {doc}`DVI </5_fichiers/dvi/qu_est_qu_un_fichier_dvi>` produit par TeX, les fichiers XDV ne peuvent pas être visualisés directement et sont normalement convertis directement en PDF lors de l'exécution de `xetex`. Cette conversion est réalisée `xdvpdfmx`.

## Qu'est ce que XeLaTeX ?

XeLaTeX est, tout naturellement, le {doc}`format LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>` utilisé avec le moteur XeTeX.

:::{note}
« XeTeX » se prononce \[ziːtɛk\], d'après son auteur original, Jonathan Kew (voir aussi «{doc}`Comment prononcer « TeX » ou « LaTeX »? </1_generalites/bases/comment_prononcer_tex>` »).
:::

______________________________________________________________________

*Sources :*

- {faquk}`What are XeTeX and LuaTeX? <FAQ-xetex-luatex>`
- [XeTeX](https://fr.wikipedia.org/wiki/XeTeX) sur Wikipedia.
- [Interview de Jonathan Kew](https://tug.org/interviews/kew.html) (en anglais),
- [Proper pronunciation of XeTeX, XeLaTeX](https://tex.stackexchange.com/questions/274617/proper-pronunciation-of-xetex-xelatex).

```{eval-rst}
.. meta::
   :keywords: LaTeX,UTF8,Unicode,XeTeX,utiliser les polices système,OpenType
```

