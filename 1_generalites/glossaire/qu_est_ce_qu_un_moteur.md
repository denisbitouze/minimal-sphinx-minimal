# Qu'est-ce qu'un moteur ?

Dans l'univers de TeX, un moteur est un programme qui convertit un fichier source TeX en un document mis en forme.

L'exécutable original `tex`, produit à la fin des années 70 (cf. « {doc}`Qu'est-ce que TeX ? </1_generalites/glossaire/qu_est_ce_que_tex>` »), ne présente pas certaines fonctionnalités que les utilisateurs attendent de logiciels actuels. Les programmes qui ont suivi, tous qualifiés de *moteurs*, résolvent ces problèmes en augmentant le moteur TeX avec quelques fonctionnalités utiles supplémentaires.

## Les différents moteurs

### pdfTeX

- Exécutable : `pdftex`.
- TeX, qui précède le format de fichier PDF d'une décennie, produit des fichiers dans un format spécifique à TeX appelé DVI (cf. « {doc}`Qu'est-ce qu'un fichier DVI ? </5_fichiers/dvi/qu_est_qu_un_fichier_dvi>` »). En revanche, `pdfTeX` peut produire à la fois des fichiers DVI et des fichiers PDF. En mode PDF, il permet aux documents d'exploiter diverses fonctionnalités PDF telles que les hyperliens, les signets et les annotations. De plus, `pdfTeX` prend également en charge des fonctionnalités micro-typographiques avancées.
- Pour plus d'informations, voir « {doc}`Qu'est-ce que pdfTeX ? </1_generalites/glossaire/qu_est_ce_que_pdftex>` ».

### XeTeX

- Exécutable : `xetex`.
- `XeTeX` lit une entrée Unicode encodée en UTF-8 et étend la prise en charge des polices par TeX pour inclure des formats « modernes » tels que OpenType. Ceci le rend bien adapté aux textes multilingues couvrant différents systèmes d'écriture.
- Pour plus d'informations, voir « {doc}`Qu'est-ce que XeTeX? </1_generalites/glossaire/qu_est_ce_que_xetex>` ».

### LuaTeX

- Exécutable : `luatex`.
- TeX a été développé dans un langage de programmation dédié. `LuaTeX` ajoute un deuxième moteur de programmation utilisant un langage de script moderne, Lua, qui est "intégré" dans un moteur similaire à TeX. Il lit également l'encodage UTF-8 et utilise des polices OpenType.
- Pour plus d'informations, voir « {doc}`Qu'est-ce que LuaTeX? </1_generalites/glossaire/qu_est_ce_que_luatex>` ».

### ε-TeX

- Exécutable : `etex`.
- Le programme ε-TeX est une extension de l'interface de programmation de TeX. En tant que tel, il est surtout utile aux développeurs d'extensions ; il existe d'ailleurs un nombre croissant d'extensions qui nécessitent ε-TeX. En plus d'exister dans `etex`, les fonctionnalités de ε-TeX sont normalement présents dans les exécutables `pdftex`, `xetex` et `luatex`. Notez bien qu'ε-TeX, qui améliore le moteur TeX, ne doit pas être confondu avec Eplain, qui améliore le format Plain TeX.
- Pour plus d'informations, voir « {doc}`Qu'est-ce qu'ε-TeX? </1_generalites/glossaire/qu_est_ce_que_etex>` ».

## Combinaisons avec LaTeX

Etant donné que chacun des éléments ci-dessus dérive d'un moteur TeX de base, il est en principe possible de combiner chacun d'entre eux avec l'un des {doc}`formats </1_generalites/glossaire/qu_est_ce_qu_un_format>` TeX pour produire des exécutables « étendus ». Ce principe s'applique aux exécutables `pdflatex`, `xelatex` et `lualatex`. Par ailleurs, tout développement de ConTeXt utilise désormais `luatex`.

Certains de ces exécutables combinent les fonctionnalités de plusieurs moteurs TeX améliorés : par exemple, `pdftex` (dans les distributions actuelles) offre à la fois les extensions `pdfTeX` et ε-TeX dans un seul exécutable. Cet exécutable peut être proposé avec un format LaTeX (`pdflatex`) ou au format Plain TeX (`pdftex`).

______________________________________________________________________

*Source :* {faquk}`Things with « TeX » in the name <FAQ-texthings>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,glossaire,vocabulaire,moteurs
```

