# Qu'est-ce que LaTeX ?

LaTeX est un ensemble de commandes (ou *format*) de TeX, écrit à l'origine par Leslie Lamport, fournissant un système de mise en forme de documents. LaTeX permet à des balises placées dans le document source de décrire la structure du document final, de sorte que l'utilisateur peut en temps ordinaire se passe de penser à la présentation. En utilisant des classes de documents et des modules complémentaires, le même document peut produire une grande variété de mises en page différentes.

Leslie Lamport a dit que LaTeX « représente un équilibre entre fonctionnalité et facilité d'utilisation ». Cependant, cela ne doit pas cacher le conflit suivant : LaTeX *peut* répondre à la plupart des besoins des utilisateurs, mais découvrir *comment* est souvent délicat. Cette FAQ a été constituée afin d'aider à résoudre ce point.

______________________________________________________________________

*Source :* {faquk}`What is LaTeX? <FAQ-latex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background
```

