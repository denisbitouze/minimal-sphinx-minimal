# Qu'est-ce que LaTeX2ε ?

La dernière version de Leslie Lamport de LaTeX (LaTeX 2.09 datant de 1992) a été remplacée en 1994 par une nouvelle version fournie par {doc}`l'équipe du LaTeX Project </1_generalites/histoire/c_est_quoi_latex3>` : LaTeX. Cette version est maintenant celle de référence et poursuit plusieurs idées de développement vues avec LaTeX 2.09. Le petit « e » du {doc}`logo officiel </3_composition/texte/symboles/logos/logos-latex>` un epsilon à un seul trait (\$\\epsilon\$, censé indiquer en mathématique un changement minime).

LaTeX apporte plusieurs améliorations par rapport à LaTeX 2.09, mais elles restent mineures dans un souci de continuité et de stabilité. Ceci s'est fait détriment du scénario du « grand bond » que certaines personnes du projet attendaient. LaTeX continue à ce jour d'offrir un mode de compatibilité dans lequel la plupart des fichiers préparés pour une utilisation avec LaTeX 2.09 fonctionnent (bien qu'avec des performances quelque peu réduites et avec de volumineuses plaintes dans le fichier journal). Les différences entre LaTeX et LaTeX 2.09 sont décrites dans un [guide](https://ctan.org/pkg/usrguide) disponible dans chaque distribution LaTeX.

Le développement du noyau LaTeX est quelque peu limité par la nécessité de conserver la compatibilité avec un très large écosystème d'extensions. Cependant, des développements récents (tels que l'autorisation de l'entrée Unicode en standard pour les documents) démontrent qu'un tel changement est toujours possible. Un travail à plus long terme est effectué par le {doc}`LaTeX Project </1_generalites/histoire/c_est_quoi_latex3>`, dans le but d'améliorer plus substantiellement LaTeX.

______________________________________________________________________

*Source :* {faquk}`What is LaTeX2e? <FAQ-latex2e>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background,LaTeX2e,LaTeX2ε
```

