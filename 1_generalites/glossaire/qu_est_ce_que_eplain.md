# Qu'est-ce qu'Eplain ?

Le {doc}`format </1_generalites/glossaire/qu_est_ce_qu_un_format>` [Eplain](https://tug.org/eplain/) étend les définitions de {doc}`Plain TeX </1_generalites/glossaire/qu_est_ce_que_plain_tex>`. Il n'est pas destiné à fournir des « capacités de composition générique », comme le font {doc}`ConTeXt </1_generalites/glossaire/qu_est_ce_que_context>`, {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>` ou {doc}`Texinfo </1_generalites/glossaire/qu_est_ce_que_texinfo>`. Au lieu de cela, il définit des outils qui devraient être utiles quelles que soient les commandes que vous choisissez d'utiliser lorsque vous préparez votre manuscrit.

Par exemple, Eplain n'a pas de commande `\section` qui formaterait les en-têtes de section de façon appropriée, comme le fait la commande `\section` de LaTeX. La philosophie d'Eplain est de considérer que certaines personnes auront toujours bun autre vision de ce qui est « approprié ». Si vous n'aimez pas les résultats des commandes prédéfinies ou si vous essayez d'obtenir un autre format, vous constaterez peut-être qu'Eplain est fait pour vous.

Toutefois, cette règle a des exceptions : presque tout le monde souhaite disposer de fonctionnalités telles que les références croisées (avec des étiquettes), afin de ne pas avoir à mettre les numéros de page « en dur » dans le document. Les auteurs d'Eplain pensent qu'il s'agit là du seul ensemble de commandes généralement disponible qui n'impose pas son style typographique à un auteur, tout en fournissant ces fonctionnalités.

Une autre fonctionnalité utile d'Eplain est la possibilité de créer des fichiers PDF avec des hyperliens. Les commandes de références croisées peuvent implicitement générer des hyperliens avec ces références mais vous pouvez également créer des hyperliens explicites, à la fois internes (pointant vers une destination dans le document actuel) et externes (pointant vers un autre document local ou une URL).

Plusieurs extensions LaTeX fournissent des fonctionnalités qui manquent aux utilisateurs de Plain TeX, notamment la coloration et la rotation du texte fournies par l'ensemble {ctanpkg}`graphics` (avec les extensions {ctanpkg}`color` et {ctanpkg}`graphics`). Bien que l'ensemble {ctanpkg}`graphics` fournisse un « chargeur » Plain TeX pour certaines extensions, il n'est pas évident de passer des options à ces extensions sous Plain TeX. Or une grande partie des fonctionnalités des extensions est accessible via les options d'extensions... Eplain étend le chargeur afin que les options puissent être passées aux extensions comme elles le sont dans LaTeX. Les extensions suivantes sont connues pour fonctionner avec Eplain : {ctanpkg}`graphics`, {ctanpkg}`graphicx`, {ctanpkg}`color`, {ctanpkg}`autopict` (environnement d'image LaTeX), {ctanpkg}`psfrag` et {ctanpkg}`url`.

La documentation détaillée est disponible sur le [site du TUG](https://www.tug.org/eplain/doc/eplain.html) et sur {ctanpkg}`site du CTAN <eplain>`. Il existe également une [liste de diffusion](https://tug.org/mailman/listinfo/tex-eplain) (anglophone).

______________________________________________________________________

*Source :* {faquk}`What is Eplain? <FAQ-eplain>`

```{eval-rst}
.. meta::
   :keywords: TeX,LaTeX,Eplain,définition,format
```

