# Qu'est-ce que Plain TeX ?

Plain TeX (qui peut être traduit en « TeX simple ») est le premier {doc}`format </1_generalites/glossaire/qu_est_ce_qu_un_format>` de {doc}`TeX </1_generalites/glossaire/qu_est_ce_que_tex>`, écrit par Donald Knuth lui-même. Ce format contient environ 600 commandes complétant et simplifiant l'utilisation des 300 primitives de TeX.

```{eval-rst}
.. meta::
   :keywords: TeX,LaTeX,Eplain,définition,format
```

