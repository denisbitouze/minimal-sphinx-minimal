# Que sont pTeX et upTeX ?

Le programme {ctanpkg}`pTeX <ptex>` ajoute à {doc}`TeX </1_generalites/glossaire/qu_est_ce_que_tex>` des fonctionnalités pour composer des documents en japonais, notamment le support de la composition verticale. {ctanpkg}`pLaTeX2ε <platex>` y ajoute les macros nécessaires pour en faire un environnement {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>`. [Son développement se poursuit](https://github.com/texjporg/tex-jp-build).

Pour leur part, {ctanpkg}`upTeX <uptex>` et {ctanpkg}`upLaTeX2ε <uplatex>` permettent le support de l'Unicode. Ils supportent également le chinois simplifié, le chinois traditionnel et le coréen, et peuvent compiler des documents écrits en caractères latins, grecs ou cyrilliques (utilisant `\inputenc{utf8}` et l'extension {ctanpkg}`babel`).

______________________________________________________________________

*Source :* <http://www.t-lab.opal.ne.jp/tex/uptex_en.html>

```{eval-rst}
.. meta::
   :keywords: LaTeX,japonais,chinois,idéogrammes,composition verticale,Unicode,UTF-8,langues orientales,document multilingue
```

