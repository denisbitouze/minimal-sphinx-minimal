# Qu'est-ce que MetaFont ?

[MetaFont](https://fr.wikipedia.org/wiki/Metafont) a été écrit par Donald Knuth afin qu'il complète TeX. Alors que TeX définit la disposition des glyphes sur une page, MetaFont définit les formes des glyphes et les relations entre eux. MetaFont détaille les tailles des glyphes, pour le bénéfice de TeX, et crée des bitmaps qui peuvent être utilisés pour représenter les glyphes, au profit des programmes qui produiront une sortie imprimée après une exécution de TeX.

Le langage de MetaFont pour définir les polices permet l'expression de plusieurs classes de choses :

- la géométrie simple des glyphes ;
- les propriétés du moteur d'impression auquel la sortie est destinée ;
- des métadonnées qui peuvent distinguer différentes tailles dans la même police, ou les différences entre deux polices appartenant à la même famille (ou apparentée).

Donald Knuth (et d'autres) ont conçu une large gamme de polices à l'aide de MetaFont. Toutefois, la conception de polices à l'aide de MetaFont est désormais une compétence plus rare encore que celle de l'écriture de commandes TeX. C'est pour ainsi dire un art en voie de disparition : peu de nouvelles polices liées à TeX sont actuellement produites à l'aide de MetaFont. De fait, plusieurs des grandes familles de polices (conçues avec MetaFont) sont maintenant surtout converties en un autre format de police.

______________________________________________________________________

*Sources :*

- {faquk}`What is MetaFont? <FAQ-MF>`
- [The METAFONT Tutorial Page](http://metafont.tutorial.free.fr/), de Christophe Grandsire (en anglais).

```{eval-rst}
.. meta::
   :keywords: LaTeX,metafont,définition,background
```

