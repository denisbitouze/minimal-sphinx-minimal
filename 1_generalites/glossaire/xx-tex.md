# Y-a-t'il d'autres trucs-TeX ?

## Qu'est-ce que KaTeX ?

[KaTeX](https://katex.org/) est une bibliothèque logicielle destinée à afficher des formules mathématiques dans les navigateurs web. La syntaxe et la mise en forme suivent les principes de TeX mais il s'agit d'une réimplémentation en Javascript, avec `Node.js`, d'un sous-ensemble limité de fonctionnalités du moteur TeX standard.

[MathJax](https://fr.wikipedia.org/wiki/MathJax) a le même usage, et implémente un nombre plus grand de fonctionnalités que KaTeX.

Pour plus d'informations :

- [KaTeX sur Wikipedia](https://en.wikipedia.org/wiki/KaTeX) (en anglais),
- [TeX as a three-stage rocket : Cookie-cutterpage breaking](https://tug.org/TUGboat/tb36-2/tb113venkatesan.pdf), TUGboat, Volume 36 (2015), n{sup}`o`2.

## Qu'est-ce que AUCTeX ?

AUCTeX est une extension des éditeurs de texte [Emacs](https://fr.wikipedia.org/wiki/Emacs) et [XEmacs](https://fr.wikipedia.org/wiki/XEmacs), écrite en [Emacs Lisp](https://fr.wikipedia.org/wiki/Emacs_Lisp), qui propose notamment des modes pour écrire et formater des fichiers TeX et LaTeX.

AUCTeX fournit la coloration syntaxique, l'indentation, la complétion des noms de macros et environnements, la prévisualisation des mathématiques directement dans le *buffer* d'édition. Il met en évidence la structure du document (titres des sections et sous-sections), et permet de les masquer ou afficher sélectivement.

Il prend également en charge le format auto-documenté `.dtx` du projet LaTeX.

:::{note}
D'où vient le nom « AUCTeX » ?

AUCTeX est dérivé de `tex-mode.el`, développé par des étudiants du [Centre Universitaire d'Aalborg](https://fr.wikipedia.org/wiki/Université_d'Aalborg) (au Danemark), vers 1986, d'où les initiales « AUC ».

La première version publiée d'AUCTeX date de 1991.
:::

Pour plus d'informations :

- [AUCTeX sur Wikipedia](https://en.wikipedia.org/wiki/AUCTeX) (en anglais).

## Qu'est-ce que RefTeX ?

```{eval-rst}
.. todo:: Ajouter quelques lignes de présentation.
```

Pour plus d'informations :

- [RefTeX sur Wikipedia](https://en.wikipedia.org/wiki/RefTeX) (en anglais).

## Qu'est-ce que REVTeX ?

[REVTeX](https://journals.aps.org/revtex) est une collection de macros LaTeX qui est maintenue et distribuée par l'[American Physical Society](https://fr.wikipedia.org/wiki/Société_américaine_de_physique) avec des fichiers auxiliaires et un guide d'aide à l'utilisateur. REVTeX est utilisé pour soumettre des articles aux revues publiées par l'*American Physical Society* (APS), l'*American Institute of Physics* (AIP) et l'*Optical Society of America* (OSA). REVTeX est également accepté par quelques autres éditeurs techniques.

La boîte à outils complète est {ctanpkg}`disponible sur le CTAN <revtex>`.

Pour plus d'informations :

- [REVTeX sur Wikipedia](https://en.wikipedia.org/wiki/REVTeX) (en anglais).

```{eval-rst}
.. meta::
   :keywords: glossaire,dictionnaire TeX,encyclopédie de TeX,TeX-trucs,TeX-machins
```

