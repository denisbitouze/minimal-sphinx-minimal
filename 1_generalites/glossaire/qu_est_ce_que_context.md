# Qu'est-ce que ConTeXt ?

[ConTeXt](http://www.pragma-ade.com/) est un ensemble de commandes créé par Hans Hagen de Pragma ADE (pour *Advanced Document Engineering*), pour servir à l'origine d'outil de production pour Pragma (qui est une maison d'édition). [ConTeXt](https://fr.wikipedia.org/wiki/ConTeXt) est un système de production de documents basé, comme {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>`, sur {doc}`TeX </1_generalites/glossaire/qu_est_ce_que_tex>`. Alors que LaTeX éloigne l'écrivain des détails typographiques, ConTeXt adopte une approche complémentaire en fournissant des interfaces structurées pour la gestion de la typographie, y compris un support étendu pour les couleurs, les arrière-plans, les hyperliens, les présentations, l'intégration figure-texte et la compilation conditionnelle. Il donne à l'utilisateur un contrôle étendu sur le formatage tout en facilitant la création de nouvelles mises en page et styles sans apprendre le langage macro TeX. La conception unifiée de ConTeXt évite les conflits de paquets qui peuvent survenir avec LaTeX.

ConTeXt intègre également MetaFun, un sur-ensemble de {doc}`MetaPost </1_generalites/glossaire/qu_est_ce_que_metapost>` et un système puissant pour les graphiques vectoriels. MetaFun peut être utilisé comme un système autonome pour produire des figures, mais sa principale force réside dans l'amélioration des documents ConTeXt par des éléments graphiques précis.

Le développement de {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` a été conduit par ConTeXt, depuis quasiment le début du projet. Au fil du temps, ConTeXt a connu trois implémentations majeures :

- Mark II (fichiers avec l'extension `mkii`) qui fonctionne avec {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` et qui n'est plus en développement actif ;
- Mark IV (fichiers avec l'extension `mkiv`) qui fonctionne avec {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` et qui est toujours en développement ;
- [LMTX](https://wiki.contextgarden.net/LMTX), qui est Mark IV fonctionnant avec le nouveau moteur LuaMetaTex, conçu pour être plus léger, plus rapide et plus facile à faire évoluer que LuaTeX, utilisé maintenant par bien d'autres projets que ConTeXt : c'est l'implémentation proposée au [téléchargement sur le site officiel](https://wiki.contextgarden.net/Installation).

ConTeXt a une grande communauté de développeurs (bien que peut-être pas aussi grande que celle de LaTeX) et les développeurs actifs semblent avoir une énergie phénoménale. Le support est assuré *via* le [ConTeXt Garden](http://wiki.contextgarden.net/Main_Page) et *via* une [liste de discussion](https://mailman.ntg.nl/mailman/listinfo/ntg-context). Une distribution « autonome » (une distribution TeX sans commandes autres que celles basées sur ConTeXt) est disponible en tant que « [ConTeXt Standalone](http://wiki.contextgarden.net/ConTeXt_Standalone) » (également connue sous le nom de *ConTeXt Minimals* ou « Suite ConTeXt »). Elle fournit un système ConTeXt sur de nombreuses plates-formes, exécutant soit la version Mark II, soit la version Mark IV.

De fait, le CTAN *ne contient pas* la distribution principale de ConTeXt. Les utilisateurs potentiels doivent se référer au [ConTeXt Garden](http://contextgarden.net) pour plus de détails sur la distribution actuelle.

Pour une initiation plus concrète et en français, voir l'article « [Pourquoi vous devriez jeter un coup d'œil à ConTeXt](https://zestedesavoir.com/billets/2733/pourquoi-vous-devriez-jeter-un-coup-doeil-a-context/) » sur [Zeste de Savoir](https://zestedesavoir.com/).

______________________________________________________________________

*Sources :*

- {faquk}`What is ConTeXt? <FAQ-context>`
- [PRAGMA Advanced Document Engineering](https://www.pragma-ade.nl/), la maison d'édition à l'origine de ConTeXt et de LuaMetaTeX,
- [Wikilivre sur ConTeXt](https://fr.wikibooks.org/wiki/ConTeXt) (en français).

```{eval-rst}
.. meta::
   :keywords: LaTeX,ConTeXt,documentation
```

