# Pourquoi TeX n'est pas WYSIWYG ?

Le terme [WYSIWYG](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_get) (pour *what you see is what you get*, traduisible par « ce que vous voyez est ce que vous obtenez ») est un qualificatif commercial pour désigner un style particulier de traitement de texte. Les systèmes WYSIWYG se caractérisent par deux principes :

- vous tapez au clavier ce que vous voulez imprimer ;
- ce vous voyez à l'écran pendant que vous tapez est une bonne approximation de votre document imprimé.

TeX a été conçu bien avant le terme WYSIWYG, ce qui explique pour partie qu'il ne soit pas WYSIWYG. Pour la petite histoire, les premiers systèmes expérimentaux WYSIWYG fonctionnaient dans des laboratoires commerciaux près de l'endroit où Donald Knuth travaillait sur TeX.

Cependant, tout ceci date quelque peu : pourquoi, depuis lors, n'a-t-on rien fait avec TeX pour l'adapter en WYSIWYG ? À cela, deux réponses.

## Des tests sans réel impact sur le comportement des utilisateurs

Parmi les expériences de TeX WYSIWYG se détache le projet VorTeX : une paire de stations de travail Sun travaillaient en tandem, l'une gérant l'interface utilisateur tandis que l'autre composait en arrière-plan le résultat. VorTeX était assez impressionnant pour l'époque mais les deux stations de travail combinées avaient énormément moins de puissance que l'ordinateur portable moyen de nos jours. Par ailleurs, son code ne s'est pas avéré portable : il n'a même jamais fait le dernier « grand » changement de version de TeX, au tournant des années 1990, le passage à la version 3 de TeX.

## Des différences de philosophie

L'application de techniques WYSIWYG à TeX pose un problème fondamental : la complexité de TeX rend difficile l'obtention de l'équivalent d'une sortie de TeX sans l'exécuter réellement sur l'ensemble du document.

En parallèle, il existe une réelle différence conceptuelle entre le modèle d'un traitement de texte WYSIWYG et le modèle que LaTeX et ConTeXt emploient : l'idée de balisage. Elle exprime un modèle logique d'un document où chaque objet est étiqueté en fonction de ce qu'il est plutôt que de la façon dont il devrait apparaître. Ainsi, l'apparence est déduite des propriétés du type d'objet. Correctement appliqué, le balisage peut fournir une aide précieuse lorsqu'il s'agit de réutiliser des documents. Dans les faits, exprimer ce type de balisage structuré s'avère difficile pour les systèmes WYSIWYG bien connus. Cependant, le balisage *commence à apparaître dans les listes des exigences courantes* du monde commercial :

- le balisage aide à imposer un style à un document, et les utilisateurs sont de plus en plus obsédés par l'uniformité du style ;
- l'utilisation de plus en plus répandue des formats d'archivage de documents dérivés de XML l'exige.

Ces mêmes besoins doivent être relevés par les programmes basés sur TeX. Nous observons donc un certain degré de convergence des besoins des deux communautés : le futur semble être prometteur !

## Quelques « approximations » actuelles

Voici à l'heure actuelle ce qu'il y a de plus proche d'un TeX WYSIWYG :

- le logiciel [LyX](https://lyx.org) propose une interface [WYSIWYM](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_mean) (pour *what you see is what you mean*, autrement dit « ce que vous voyez est ce que vous voulez dire »), utilisant LaTeX pour la composition et un format de fichier spécifique pour les documents eux-mêmes ;
- le système commercial [Scientific Word](https://www.mackichan.com/index.html?products/sw.html~mainFrame) offre enfin sans doute la meilleure approximation disponible d'un WYSIWYG. Il peut également interagir avec un [système de calcul formel](https://fr.wikipedia.org/wiki/Système_de_calcul_formel).

______________________________________________________________________

*Source :* {faquk}`Why is TeX not a WYSIWYG system? <FAQ-notWYSIWYG>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background
```

