# Dois-je utiliser Plain TeX ou LaTeX ?

Il n'y a pas de réponse évidente à cette question. Beaucoup ne jurent que par Plain TeX et produisent en l'utilisant des documents de grande qualité (Knuth est bien entendu un bon exemple de cette école). Mais par ailleurs, beaucoup d'utilisateurs sont heureux de laisser quelqu'un d'autre prendre les décisions de mise en page à leur place, acceptant une légère perte de flexibilité au profit d'un moindre effort intellectuel.

Les controverses sur ce sujet peuvent provoquer bien des échanges animés et furieux, sans aboutir à clarifier le débat ; le mieux est probablement de découvrir ce que votre entourage utilise et de suivre leur exemple, dans l'espoir qu'ils puissent vous apporter de l'aide. Il sera toujours temps plus tard de changer de camp : ne vous tracassez pas à ce sujet.

Si vous préparez un texte pour une publication, demandez-leur ce qu'ils utilisent avant de produire votre document en suivant vos propres goûts ; de nombreuses maisons d'édition ont développé leurs propres classes Plain TeX ou LaTeX pour les revues et les livres et insistent sur la nécessité que les auteurs s'y conforment avec rigueur.

______________________________________________________________________

*Source :* {faquk}`Should I use Plain TeX or LaTeX? <FAQ-plainvltx>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background
```

