# Que veut dire « écrire en TeX » ?

TeX est un macro-processeur et offre à ses utilisateurs une puissante capacité de programmation. Pour produire un document, vous écrivez des commandes (ou macros) et du texte entrelacés les uns avec les autres. Les macros définissent un environnement dans lequel le texte doit être composé.

Cependant, le moteur TeX est assez basique et est une bête assez difficile à gérer. Reconnaissant cela (et ne voulant pas écrire lui-même les mêmes choses au début de chaque document), Knuth a fourni un paquet de commandes à utiliser avec TeX, appelé Plain TeX. Ce dernier forme donc un ensemble minimum de commandes utilisables avec TeX auquel s'ajoute certaines versions de démonstration de commandes de plus haut niveau. Quand les gens disent qu'ils « écrivent (ou programment) en TeX », ils veulent généralement dire qu'ils programment en Plain TeX.

______________________________________________________________________

*Source :* {faquk}`What's "writing in TeX"? <FAQ-plaintex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,écrire en tex
```

