# Quel est le lien entre LaTeX et Plain TeX ?

[TeX](/1_generalites/glossaire/qu_est_ce_que_tex) propose un langage de programmation (assez spécifique) et tout document plus compliqué que le trivial « [Hello world](https://fr.wikipedia.org/wiki/Hello_world) » demande d'utiliser au moins un peu de ce langage de programmation.

[Plain TeX](/1_generalites/glossaire/qu_est_ce_que_plain_tex) et {doc}`LaTeX </1_generalites/glossaire/qu_est_ce_que_latex>` sont deux ensembles de définitions de commandes (ou librairies) écrites pour compléter les commandes de TeX : de fait, lorsque l'utilisateur lance les programmes `tex` et `latex`, ces derniers font appel à TeX et chargent en complément par défaut qui leur est associée. Ces librairies sont dénommées des {doc}`formats </1_generalites/glossaire/qu_est_ce_qu_un_format>`. Les documents doivent par conséquent être programmés en tenant compte des commandes que proposent les formats Plain TeX ou LaTeX.

Plain TeX et LaTeX existent parce que la rédaction de documents avec un TeX « brut » implique beaucoup de réinventions de la roue pour traiter tel ou tel aspect de présentation pour chaque document. Ainsi, ces deux formats servent d'aides pratiques pour rendre vos document plus plaisants : LaTeX fournit d'ailleurs beaucoup d'élements et de commandes pour répondre aux besoins de mise en forme des documents.

De fait, la richesse de LaTeX lui permet d'être quasiment un sur-ensemble de Plain TeX. Cependant, certaines commandes de Plain TeX ne fonctionnent pas comme attendu lorsqu'elles sont utilisées dans un document LaTeX. Utiliser des commandes Plain TeX dans un document LaTeX est source de bugs occasionnels : le résultat est *presque* correct mais certaines choses sont mal disposées.

Ainsi, Plain TeX et LaTeX sont reliés par un parent commun et ont été construits pour être utilisés pour des travaux similaires. Mais il faut garder en tête que la programmation faite pour l'un ne fonctionnera sans doute pas bien pour l'autre.

______________________________________________________________________

*Source :* {faquk}`How does LaTeX relate to Plain TeX? <FAQ-LaTeXandPlain>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,background
```

