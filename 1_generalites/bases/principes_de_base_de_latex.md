# Quels sont les principes de base de LaTeX et pdfLaTeX ?

## Principes de LaTeX

LaTeX peut être considéré comme un langage de programmation évolué dans le sens où il s'appuie sur TeX qui est un langage de plus bas niveau. Langage de programmation signifie également que le document que l'on veut créer doit être décrit dans un fichier source (`.tex`) puis doit être compilé. Ainsi, le compilateur LaTeX prend en entrée un fichier source écrit en LaTeX et produit en sortie un fichier DVI (pour *DeVice Independent* autrement dit « indépendant du matériel »). Ce fichier peut ensuite être converti en fichier Postscript ou PDF avant d'être imprimé. Les fichiers `.dvi`, `.ps` et `.pdf` peuvent être visualisés à l'écran à l'aide de (pré)visualiseurs.

L'intérêt du format DVI est qu'il permet à TeX et LaTeX d'être indépendants du matériel qui sera utilisé pour la visualisation ou l'impression finale du document.

## Principes de pdfLaTeX

pdfLaTeX procède de la même logique que LaTeX mais le compilateur produit un fichier au format PDF. On notera que certains packages ({ctanpkg}`graphicx` et {ctanpkg}`hyperref` par exemple) proposent une option `[pdf]` ou `[pdftex]` pour une compilation avec pdfLaTeX.

:::{warning}
Certaines extensions comme {ctanpkg}`pstricks` utilisent la commande `\special` pour produire directement du code Postscript. Elles ne fonctionnent pas avec pdfLaTeX. On peut en revanche utiliser {ctanpkg}`pdftricks` avec pdfLaTeX.
:::

______________________________________________________________________

*Source :* <https://tex.stackexchange.com/questions/349/what-is-the-practical-difference-between-latex-and-pdflatex>

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,PStricks,Postscript
```

