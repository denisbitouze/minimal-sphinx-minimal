# Comment identifier une version provisoire ?

- Le paquetage {ctanpkg}`draftwatermark` permet de le signaler en mettant, dans la sortie du document, un mot (par défaut, «*draft* ») en grisé, en travers de la feuille. L'exemple ci-dessous montre le résultat d'une telle opération :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{draftwatermark}

\begin{document}
Voici une version provisoire de mon texte.
Pourriez-vous la relire et me faire part de vos
commentaires avant la fin de la semaine ?

Merci d'avance.

\paragraph{Proposition.}
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire -- Texte provisoire --
Texte provisoire

\end{document}
```

- Le paquetage {ctanpkg}`draftcopy` a les mêmes fonctionnalités de base que {ctanpkg}`draftwatermark`, mais est limité à une sortie en Postscript (pas pour les PDF).
- Le paquetage {ctanpkg}`drafthead` ajoute un en-tête contenant le mot «*draft* » (« brouillon »), ainsi que la date et l'heure de compilation du document.
- Le paquetage {ctanpkg}`prelim2e` permet d'insérer, dans le bas de la page, le nom du fichier, la date et l'heure de compilation...
- Il existe enfin la solution « bourrin » qui consiste à insérer directement le code Postscript dans le document à l'aide de la commande `\special{}` (ne fonctionne pas pour une sortie PDF). On inclura par exemple le code suivant dans le préambule :

```latex
% !TEX noedit
\special{
! userdict begin /bop-hook{
stroke
gsave 240 100 translate
65 rotate /Times-Roman findfont 220 scalefont setfont
0 0 moveto 0.9 setgray (DRAFT) show grestore
}def end}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript,brouillon,filigrane
```

