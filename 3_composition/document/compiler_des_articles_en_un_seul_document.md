# Comment constituer un recueil d'articles à partir de plusieurs documents sources ?

Cette question se pose, par exemple, si une personne prépare les comptes-rendus d'une conférence dont les articles ont été soumis en LaTeX.

Les solutions les plus simples sont les classes {ctanpkg}`combine` de Peter Wilson et {ctanpkg}`subfiles` de Federico Garcia mais de nombreuses approches ont été proposées. Chacune offre ses propres avantages. En particulier, plusieurs solutions nettement plus légères (par exemple, {ctanpkg}`includex` et {ctanpkg}`docmute`) sont bien adaptées aux documents moins formels.

## Avec la classe « combine »

La classe {ctanpkg}`combine` définit les outils (dont une commande `\import`) pour importer des documents entiers et fournit des moyens de spécifier les caractéristiques importantes de la mise en page du document, de la table des matières globale, etc. L'ensemble des fonctionnalités est assez complexe. Une extension auxiliaire, {ctanpkg}`combinet`, permet d'utiliser les commandes `\title`, `\author` et autres des documents importés et de les faire figurer dans la table des matières globale. La structure de base d'un document combiné serait :

```latex
% !TEX noedit
\documentclass[...]{combine}
...
\begin{document}
...
⟨Éléments introductifs⟩
...
\begin{papers}
% titre et auteur du premier article,
% qui iront dans la table des matières
% globale.
\coltoctitle{...}
\coltocauthor{...}
\label{art1}
\import{art1}
...
\end{papers}
...
⟨Remerciements, etc.⟩
...
\end{document}
```

## Avec la classe « subfiles »

La classe {ctanpkg}`subfiles` est utilisée dans les fichiers isolés d'un projet multi-fichiers tandis que l'extension {ctanpkg}`subfiles` sert dans le fichier parent. La structure de ce fichier parent est la suivante :

```latex
% !TEX noedit
\documentclass{⟨selon vos besoins⟩}
...
\usepackage{subfiles}
...
\begin{document}
...
\subfile{⟨nom d'un fichier enfant⟩}
...
\end{document}
```

En parallèle, les fichiers enfants appelés par le fichier parent ont pour structure :

```latex
% !TEX noedit
\documentclass[⟨nom du fichier parent⟩]{subfiles}
\begin{document}
...
\end{document}
```

Des réglages peuvent être définis pour que les fichiers enfants appelés par le fichier parent soient composés en utilisant des paramètres différents (le format de page par exemple) de ceux utilisés lorsqu'ils sont composés en tant que partie du fichier principal.

## Avec l'extension « newclude »

Les extensions {ctanpkg}`newclude` de Matt Swift, faisant partie de l'ensemble {ctanpkg}`frankenstein`, propose différents outils. Il convient de noter que {ctanpkg}`newclude` reste « en développement » depuis 1999.

L'extension vous permet d'inclure avec la commande `\includedoc` des articles complets (de la même manière que vous incluez avec `\include` des fichiers de chapitre dans un document classique). Mais elle ne tient compte que de ce qui est indiquée entre les commandes `\begin{document}` et `\end{document}`. Par conséquent, cette extension ne fait pas tout le travail à votre place : vous devez analyser les extensions utilisées dans chaque article et vous assurer qu'un ensemble cohérent est chargé dans le préambule du rapport principal.

Enfin, cette extension nécessite {ctanpkg}`moredefs`, qui fait également partie de l'ensemble {ctanpkg}`frankenstein`.

{octicon}`alert;1em;sd-text-warning` //L’extension {ctanpkg}`includex` est considérée comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`. Ce qui suit est informatif. //

Antérieurement, l'extension {ctanpkg}`includex` apportait les fonctionnalités toutes reportées dans {ctanpkg}`newclude`. Son auteur l'a classé comme obsolète même si certaines personnes semblent toujours l'utiliser.

## Avec l'extension « docmute »

L'extension {ctanpkg}`docmute` propose une boîte d'outils simple et élégante. Une fois l'extension chargée, tout ce qui se trouve entre `\documentclass` et `\begin{document}` dans tout fichier inséré par une commande `\input` ou `\include` est ignoré, puis ce fichier est traité jusqu'à son `\end{document}`. Ici encore, l'utilisateur doit s'assurer que toutes les appels aux extensions et autres configurations sont effectuées dans le document parent.

## Avec l'extension « standalone »

L'extension {ctanpkg}`standalone` (*stand-alone* signifiant « autonome ») développe les idées de {ctanpkg}`docmute`. Elle a été conçue pour répondre aux besoins des utilisateurs qui génèrent des images à partir des extensions graphiques récentes (notamment {ctanpkg}`pgf/tikz <pgf>`) pour lesquelles le temps de compilation des graphiques est tel qu'une compilation séparée est parfois souhaitable. Cette extension fournit donc un moyen de développer des graphiques de manière pratique, en les détachant du développement du document dans son ensemble. Et son intérêt pour notre présent sujet est évidente.

L'utilisateur inclut l'extension {ctanpkg}`standalone` dans le document principal et chaque sous-fichier utilise la classe {ctanpkg}`standalone`. En interne, cette classe utilise la classe {ctanpkg}`article` pour la composition en mode autonome, mais il peut être demandé d'en utiliser une autre.

La vraie différence avec l'extension {ctanpkg}`docmute` repose sur la flexibilité. Ainsi, vous pouvez demander que les préambules des documents inclus soient rassemblés, afin de pouvoir construire un bon préambule pour le document parent.

## Avec l'extension « subdocs »

Une approche de « compilation commune » est proposée par l'extension {ctanpkg}`subdocs`. Le fichier du pilote contient une commande `\subdocuments[options]{fichier1,fichier2,...}`. Ici, les arguments facultatifs fournissent des options de mise en page, telles que la présence ou pas de commandes `\clearpage` ou `\cleardoublepage` entre les différents fichiers. Chacun des sous-fichiers exécutera `\usepackage[parent]{subdocs}` pour déclarer le nom, `parent`, du fichier appelant. Chacun des sous-fichiers lit tous les fichiers « aux », de sorte que des tables des matières peuvent être produites.

## Avec l'extension « pdfpages »

Une approche complètement différente consiste à utiliser l'extension {ctanpkg}`pdfpages` et à inclure les articles soumis au format PDF dans un document PDF produit par pdfLaTeX. L'extension définit une commande `\include` qui prend des arguments similaires à ceux de la commande `\includegraphics`. Avec des mots-clés dans l'argument facultatif de la commande, vous pouvez spécifier les pages que vous souhaitez inclure à partir du fichier nommé et divers détails de la mise en page des pages incluses. Voir par exemple la question « {doc}`Comment obtenir des commentaires ou des plages de document compilables sous conditions ? </2_programmation/syntaxe/commentaires/compilation_conditionnelle_et_commentaires>` ».

______________________________________________________________________

*Source :* {faquk}`A "report" from lots of 'article's <FAQ-multidoc>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,agrégation de plusieurs documents,includex,newclude,subfiles,sous-fichier,fichier parent,combine,docmute,standalone,pdfpages,subdocs
```

