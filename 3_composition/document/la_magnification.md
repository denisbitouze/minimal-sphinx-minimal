# Comment obtenir un agrandissement de l'ensemble d'un document ?

Cet agrandissement ou *magnification* (en anglais) déforme proportionnellement toutes les hauteurs et largeurs du document, en ne modifiant en rien la position des différents éléments sur les pages (elles-mêmes agrandies). Ceci permet par exemple de passer un document pensé en format A5 à un document au format A4.

## Avec la commande \\mag

La commande `\mag` est pensée pour traiter cette tâche. Par exemple, pour agrandir de 20% le document, on tapera :

```latex
% !TEX noedit
\mag=1200
...
Tout le document est agrandi.
```

## Avec le programme dvips

Si vous cherchez à obtenir un document Postscript agrandi, alors `dvips` l'autorise avec l'option `-x nombre` où ce nombre vaut, par exemple, 1200 pour un agrandissement de 20% :

```
dvips -x 1200 -o document.ps document.dvi
```

## Avec l'extension scale

L'extension {ctanpkg}`scale` permet de modifier l'échelle d'un document avant son impression.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,Postscript,agrandissement,magnification,redimensionnement
```

