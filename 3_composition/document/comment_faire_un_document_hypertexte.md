# Comment obtenir un document hypertexte avec (La)TeX ?

Si vous voulez un faire un document hypertexte en ligne avec une source TeX ou LaTeX, il y a actuellement trois possibilités à considérer.

## Avec l'extension « hyperref »

Voici la solution la plus classique : vous partez d'une source LaTeX et utiliser `pdfTeX`, {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>`'' ou ' {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` pour produire des PDF, tout en vous servant de l'extension {ctanpkg}`hyperref` pour construire des hyperliens.

Des éléments complémentaires sont donnés à la question « {doc}`Comment obtenir des hyperliens ? </3_composition/texte/renvois/composer_des_liens_hypertexte>` ».

## Avec une conversion de LaTeX en HTML

Vous pouvez partir d'une source TeX ou LaTeX et utiliser une des nombreuses techniques pour la {doc}`traduire (plus ou moins) directement en HTML </5_fichiers/xml/convertir_du_latex_en_html>`.

## Avec « texinfo »

Vous pouvez partir d'une source {doc}`texinfo </1_generalites/glossaire/qu_est_ce_que_texinfo>` et utilisez le visualiseur `info` ou vous pouvez convertir la source {ctanpkg}`texinfo` en HTML en utilisant `texi2html`.

______________________________________________________________________

*Source :* {faquk}`Making hypertext documents from TeX <FAQ-hyper>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,hypertexte,hyperref,HTML,conversion,LaTeX pour le web
```

