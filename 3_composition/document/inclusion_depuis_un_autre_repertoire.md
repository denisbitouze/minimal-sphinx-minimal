# Pourquoi LaTeX ne veut pas inclure mon fichier ?

Vous voulez inclure un fichier avec la commande `\include{../dossier/xyz.tex}` mais LaTeX indique :

```latex
% !TEX noedit
latex : Not writing to ../bar/xyz.aux (openout_any = p).
! I can't write on file `../bar/xyz.aux'.
```

Autrement dit, il ne peut pas écrire dans le dossier que vous avez ciblé. L'erreur vient de la protection de TeX contre l'écriture dans un dossier qui n'est pas inclus dans le dossier (ou un de ses sous-dossiers associés) de votre document. Cette restriction vous protège contre les problèmes résultant de la compilation d'un document malveillant ou incorrect et donc le risque d'écrasement de données que vous souhaiteriez conserver.

Les structures de dossiers qui peuvent conduire à ce problème ressembleront à ceci :

```bash
./base/monlivre.tex
./preface/Preface.tex
./chapitre1/Intro.tex
```

Ici, le dossier `preface` n'est pas inclus dans le dossier `base`. Pour aller chercher ce dossier, vous devez remonter dans le dossier père du dossier `base`. Dès lors, l'erreur va se produire.

Mais, au fait, pourquoi veut-il y écrire ? La question « [Que fait la commande \\include ?](/3_composition/document/que_fait_vraiment_include) » en donne la raison.

Les solutions au problème tendent à être drastiques.

## En restructurant vos dossiers

La réorganisation des dossiers doit amener votre document à se trouver à la racine de votre arborescence de dossier contenant tous les éléments qui vous sont utiles :

```bash
./monlivre.tex
./monlivre/preface/Preface.tex
./monlivre/chapitre1/Intro.tex
```

## En remplaçant les commandes \\include

Si vous n'avez pas particulièrement besoin des commandes `\include` et `\includeonly`, préfèrez-leur les commandes `\input`. Vous pouvez aussi utiliser l'extension {ctanpkg}`import` détaillée dans la question « {doc}`Comment inclure des fichiers sans modifier leurs liens internes ? </3_composition/document/utiliser_des_fichiers_dans_differents_repertoires>` ».

## En modifiant le fichier texmf.cnf

Vous *pourriez* éditer le fichier `texmf.cnf` de votre système. Cette action n'est cependant pas recommandée et est laissée à ceux qui peuvent ici « s'aider eux-mêmes ».

______________________________________________________________________

*Source :* {faquk}`LaTeX won't include from other directories <FAQ-includeother>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```

