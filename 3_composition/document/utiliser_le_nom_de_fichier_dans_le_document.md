# Comment afficher le nom du fichier compilé ?

Vous souhaitez générer automatiquement un en-tête ou un pied de page intégrant le fichier en cours de traitement ? Ce n'est pas toujours trivial, voici pourquoi...

TeX conserve ce qu'il considère comme le nom de sa *tâche* (son «*job* »), dans la primitive `\jobname`. Il s'agit en général du nom du fichier passé à TeX sur la ligne de commande, dépouillé de l'éventuel nom de répertoire et de toute extension (telle que `.tex`). Mais dans deux cas, ça ne sera pas un nom de fichier :

- si aucun fichier n'a été passé (c'est-à-dire que vous utilisez TeX de manière interactive), `\jobname` a la valeur fixe `texput` (c'est aussi le nom qui est donné au fichier `log` dans ce cas);
- si vous avez utilisé l'option `-jobname=⟨nom⟩` du compilateur, `\jobname` aura la valeur `nom`, indépendamment du nom réel du fichier compilé.

Si votre document est stocké dans un unique fichier, utiliser `\jobname` est la bonne solution. Mais les documents plus importants seront souvent stockés dans un ensemble de fichiers et TeX ne fait aucune tentative pour garder trace des différents fichiers lus au cours de sa *tâche* de compilation. L'utilisateur doit donc faire le suivi lui-même et le seul moyen consiste à modifier les commandes d'entrée et à leur faire conserver les détails des noms de fichiers. Cette opération est particulièrement difficile dans le cas de Plain TeX du fait de la syntaxe particulière de la commande `\input`.

Dans le cas de LaTeX, les commandes d'entrée ont une syntaxe plus classique et les {doc}`techniques de correction </2_programmation/macros/patcher_une_commande_existante>` usuelles peuvent y être appliquées. Pour information, la commande `\input` de LaTeX est elle-même une modification de la commande Plain TeX. Nos patchs s'appliquent à la version LaTeX de la commande, utilisée sous la forme `\input{⟨fichier⟩}`. Ce type de manipulation reste cependant peu recommandé par rapport aux méthodes suivantes.

## Avec l'extension « currfile »

L'extension {ctanpkg}`currfile` fournit un moyen de garder une trace des détails du fichier courant (son nom dans `\currfilename`, son répertoire dans `\currfiledir`, ainsi que son nom de fichier sans extension et son extension). Pour y arriver, elle utilise une deuxième extension, {ctanpkg}`filehook`, qui repère les opérations sur fichiers qui utilisent `\input`, `\InputIfFileExists` et `\include`, ainsi que les chargements d'extensions et de classes.

## Avec l'extension « FiNK »

{octicon}`alert;1em;sd-text-warning` L’extension {ctanpkg}`FiNK` est classée comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`. Ce qui suit est informatif.

L'extension {ctanpkg}`FiNK` (pour *File Name Keeper*, autrement dit « gardien de nom de fichier ») garde la trace du nom et de l'extension du fichier, dans une commande `\finkfile`. {ctanpkg}`FiNK` est désormais obsolète, au profit de {ctanpkg}`currfile`, mais reste disponible pour une utilisation dans les anciens documents. En bonus, {ctanpkg}`FiNK` fournit un script en [Lisp](https://fr.wikipedia.org/wiki/Lisp), `fink.el`, qui offre un support sous Emacs avec AUCTeX.

______________________________________________________________________

*Source :* {faquk}`What's the name of this file <FAQ-filename>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,noms des fichiers chargés,nom des fichiers compilés,nom du job,exécution
```

