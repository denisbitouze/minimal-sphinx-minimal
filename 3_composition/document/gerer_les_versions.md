# Comment gérer les versions d'un document ?

## Avec un numéro de version

L'extension {ctanpkg}`vrsion` permet d'ajouter un numéro de version. Ce numéro est incrémenté à chaque compilation.

## Avec des informations de compilation

Les extensions {ctanpkg}`drafthead` et {ctanpkg}`prelim2e`, aussi mentionnés pour {doc}`l'identification d'une version provisoire </3_composition/document/identifier_une_version_provisoire>`, permettent d'inclure la date et l'heure de la compilation dans l'en-tête ou le bas de page.

## Avec des logiciels de gestion de versions

Si vous utilisez un [logiciel de gestion de versions](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions) (git, svn, etc.), des extensions dédiés permettent d'extraire automatiquement des informations du gestionnaire de version et de les inclurent dans votre document : {ctanpkg}`rcs`, {ctanpkg}`svn`, {ctanpkg}`phfsvnwatermark`, {ctanpkg}`svninfo`, {ctanpkg}`gitinfo2`, {ctanpkg}`gitver`, {ctanpkg}`latexgit`, {ctanpkg}`gitlog`.

```{eval-rst}
.. meta::
   :keywords: LaTeX, version, rcs, svn, git
```

