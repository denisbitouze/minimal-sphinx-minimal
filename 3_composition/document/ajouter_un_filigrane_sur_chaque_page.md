# Comment obtenir un filigrane sur toutes les pages ?

Il est souvent utile de placer un texte (tel que « Version provisoire » ou *draft* en anglais) en arrière-plan de chaque page d'un document. Ce type de texte est appelé [filigrane](https://fr.wikipedia.org/wiki/Filigrane) (ou *watermark* en anglais).

## Avec l'extension « draftcopy »

Pour les utilisateurs de LaTeX, une méthode simple consiste à utiliser l'extension {ctanpkg}`draftcopy` pensée pour placer le mot *draft* en filigrane. Elle peut gérer de nombreux processeurs DVI et connaît les traductions du mot anglais *draft* dans un large éventail de langues (bien que vous puissiez également choisir votre propre mot). Cependant, l'extension repose sur des commandes PostScript :

- elle échoue si vous visualisez votre document avec `xdvi` ;
- elle ne se compile pas si vous utilisez `pdfLaTeX`.

## Avec l'extension « wallpaper »

L'extension {ctanpkg}`wallpaper` se base sur {ctanpkg}`eso-pic` (voir ci-dessous). Outre les fonds de page à image unique décrits ci-dessus, l'extension fournit des fonctionnalités pour obtenir une mosaïque d'images. Toutes ses commandes viennent par paires : une pour un usage global et une pour la page courante uniquement.

## Avec l'extension « draftwatermark »

L'extension {ctanpkg}`draftwatermark` utilise {ctanpkg}`everypage` du même auteur pour fournir une interface simple pour l'ajout de filigranes textuels (de type « Version provisoire »).

## Avec l'extension « xwatermark »

L'extension {ctanpkg}`xwatermark` fournit un filigrane très flexible, avec une interface au format « clef=valeur ».

## Avec l'extension « eso-pic »

Plus riche en possibilités que les extensions vues précédemment, {ctanpkg}`eso-pic` attache un environnement `picture` à chaque page au moment où elle est finalisée. L'utilisateur peut mettre divers éléments dans cet environnement et l'extension fournit des commandes pour placer au besoin des éléments à certains points utiles, tel `text upper left` (texte en haut à gauche) ou `text center` (centre du texte).

Cette extension est basée sur {ctanpkg}`atbegshi`. Cela lui donne la possibilité de produire des « filigranes » *par-dessus* les autres éléments de la page. Cela peut être utile sur des pages où le filigrane serait autrement masqué par d'autres éléments. Alors que la commande de l'extension {ctanpkg}`atbegshi` normalement utilisée par {ctanpkg}`eso-pic` pour placer votre filigrane sous les autres éléments est `\AtBeginShipoutUpperLeft`, celle pour placer votre filigrane au-dessus des autres est `\AtBeginShipoutUpperLeftForeground`.

## Avec l'extension « everypage »

L'extension {ctanpkg}`everypage` vous permet d'ajouter des éléments à toutes les pages comme à une page particulière. Vous devez cependant développer votre propre code si vous souhaitez obtenir des résultats complexes avec cette extension.

## Avec le programme « pdftk »

Enfin, l'utilitaire `pdftk` permet d'obtenir un résultat similaire avec la commande suivante :

```bash
pdftk a.pdf background b.pdf output c.pdf
```

Le programme va créer le fichier `c.pdf` sur la base de `a.pdf` en se servant de la première page de `b.pdf` comme fond de page pour toutes les pages de `c.pdf`. Si vous avez un filigrane standard utilisé dans plusieurs fichiers, `pdftk` pourrait être intéressant.

Outil en ligne de commande, `pdftk` est disponible dans la plupart des distributions Linux, mais peut être téléchargé depuis son [site officiel](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/).

______________________________________________________________________

*Source :* {faquk}`Watermarks on every page <FAQ-watermark>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,filigrane,watermark,filigrane au-dessus du texte,filigrane au-dessous du texte
```

