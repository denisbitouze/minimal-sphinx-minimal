# Comment marquer les modifications dans un document ?

## Par des barres de modification

On a souvent besoin d'indications claires sur la façon dont un document a changé et la technique la plus courante, avec des barres de modification dans la marge, demande quelques ruses de la part du programmeur.

### Avec l'extension « backgrnd »

L'extension la plus simple qui propose d'afficher des barres de changement est {ctanpkg}`backgrnd`, de Peter Schmitt. Elle a été écrite comme une application Plain TeX qui modifie la routine de sortie (ce qui n'est pas anodin pour des utilisateurs avertis), mais cela semble fonctionner, au moins sur des documents LaTeX simples.

### Avec l'extension « changebar »

La solution spécifique à LaTeX la plus ancienne est l'extension {ctanpkg}`changebar` qui utilise des commandes `\special` liées aux pilotes que vous utilisez. Vous devez donc indiquer à l'extension le {doc}`pilote de sortie </5_fichiers/dvi/qu_est_qu_un_pilote_dvi>` utilisé (de la même manière que vous devez l'indiquer à l'extension {ctanpkg}`graphics`). La liste des pilotes disponibles est assez large mais n'inclut pas `dvipdfm`. La Parmi les plus courants, il y a :

- DVItoPS,
- DVIps,
- pdfTeX,
- XeTeX (voir {texdoc}`la documentation <changebar>` pour une liste exhaustive).

L'extension est livrée avec un script shell `chbar.sh` (pour une utilisation sur les systèmes Unix), qui compare deux documents et en génère un troisième qui est balisé avec les commandes fournies par {ctanpkg}`changebar` pour mettre en évidence les changements.

Notez que cette extension est assez fragile, et, par exemple, ne supporte pas le redimensionnement de page. En voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage[outerbars]{changebar}  % permet de positionner les barres dans la marge externe

\usepackage{babel}

\setcounter{changebargrey}{20}     % permet de configurer le niveau de gris des barres

\begin{document}
Ce texte-là est supposé ne pas avoir changé.

\begin{changebar}
Par contre ce texte-ci a été revu depuis la dernière édition.
\end{changebar}

ou encore :

ancien texte ancien texte ancien texte \cbstart
nouveau texte nouveau texte nouveau texte \cbend
ancien texte ancien texte ancien texte ancien texte.
\end{document}
```

L'éditeur du shareware `WinEDT` dispose de fonctionnalités générant des commandes {ctanpkg}`changebar` (ou autres) pour montrer les différences d'une version à une autre de votre fichier, stockées dans un dépôt `RCS` ([Revision Control System](https://fr.wikipedia.org/wiki/GNU_RCS)) --- voir https://www.winedt.org/old/Config/menus/RCS.php.

```{eval-rst}
.. todo:: *Cette information semble concerner une version ancienne de cet éditeur...*
```

### Avec l'extension « vertbars »

L'extension {ctanpkg}`vertbars` utilise très largement les techniques de l'extension {ctanpkg}`lineno` (qu'elle charge). Elle définit un environnement `vertbar` pour créer des barres. Notez, cependant, que l'environnement fait un paragraphe séparé avec son contenu : il semble préférable de l'utiliser lorsque la convention est de marquer un paragraphe entièrement modifié.

### Avec l'extension « framed »

Les barres que fournit l'extension {ctanpkg}`framed` sont un effet secondaire d'autres fonctionnalités : son environnement `leftbar` est simplement un cadre dépouillé. Cette extension s'applique également à des paragraphes entiers, comme {ctanpkg}`vertbars`.

### Avec la classe « memoir »

La classe {ctanpkg}`memoir` permet de placer des commentaires éditoriaux dans les marges, que vous pouvez évidemment utiliser pour délimiter des zones de texte modifié.

## Par un marquage des modifications

Un moyen encore plus fin pour suivre des modifications, utilisé par certains traitements de texte, consiste à produire un document qui intègre à la fois les anciennes et nouvelles versions.

### Avec l'extension « changes »

À cette fin, l'extension {ctanpkg}`changes` permet à l'utilisateur de marquer manuellement les modifications de texte, telles que les ajouts, les suppressions ou les remplacements. Le texte modifié est affiché dans une couleur différente et le texte supprimé est barré. L'extension vous permet de définir des auteurs supplémentaires et leur couleur associée; il vous permet également de définir un balisage pour les auteurs ou les annotations. La documentation montre (très clairement) comment utiliser les différentes fonctionnalités.

### Avec les scripts « latexdiff » et « latexrevise »

Le script Perl {ctanpkg}`latexdiff` peut également être utilisé pour générer ce type de balisage pour les documents LaTeX : en lui soumettant deux documents, il produit un nouveau document LaTeX dans lequel les changements sont visibles. Un exemple de sortie est intégré dans {texdoc}`sa documentation sur le CTAN <latexdiff>`.

Une fonction de révision rudimentaire est fournie par un autre script Perl, {ctanpkg}`latexrevise <latexdiff>`, qui accepte ou rejette toutes les modifications. L'édition manuelle du fichier de différences peut être utilisée pour accepter ou rejeter uniquement les modifications sélectionnées.

______________________________________________________________________

*Source :* {faquk}`Marking changed parts of your document <FAQ-changebars>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,modification,barre de modification,version,comparaison,diff de fichier LaTeX,différences entres fichiers LaTeX
```

