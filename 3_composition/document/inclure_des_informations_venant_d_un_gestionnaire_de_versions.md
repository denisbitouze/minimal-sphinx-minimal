# Comment gérer des versions en lien avec RCS, CVS, Git ou autres ?

Si vous utilisez RCS, CVS, `Subversion`, `Bazaar` ou `Git` pour la gestion de version de vos documents TeX ou LaTeX, vous pouvez avoir besoin d'outils automatiques pour insérer le numéro de version dans votre document, de manière à ce qu'il soit inclus dans le rendu du document (et pas seulement caché dans un commentaire du document source).

## Avec RCS et CVS

Le solution la plus complète pour RCS et CVS est d'utiliser l'extension {ctanpkg}`rcs`, qui vous permet de récupérer et d'afficher le contenu des informations RCS d'une manière très complète et flexible. L'extension {ctanpkg}`rcsinfo` est plus simple et suffit pour une utilisation de base, ce qui fait qu'elle a la préférence de certains ; elle se veut compatible avec {ctanpkg}`LaTeX2HTML <latex2html>`.

Si cependant vous avez besoin d'une solution qui ne passe pas par l'utilisation d'une extension ou qui fonctionne en pur TeX, alors vous pouvez utiliser la solution minimaliste qui suit :

```latex
% !TEX noedit
\def\RCS$#1 : #2 ${\expandafter\def\csname RCS#1\endcsname{#2}}
\RCS$Revision : 1.47 $ % ou n'importe quel autre champ RCS
\RCS$Date : 2014/01/28 18:17:23 $
...
\date{Revision \RCSRevision, \RCSDate}
```

## Avec Subversion

Si vous êtes un utilisateur de `Subversion`, l'extension {ctanpkg}`svn` est la plus adaptée. Elle est capable de gérer automatiquement un certain nombre d'informations concernant la version utilisée :

```latex
% !TEX noedit
\documentclass{⟨foo⟩}
...
\usepackage{svn}
\SVNdate $Date$
\author{...}
\title{...}
...
\begin{document}
\maketitle
...
\end{document}
```

Si le document source a été géré avec `Subversion`, la commande `\maketitle` utilisera automatiquement la date placée dans le champ `$Date$` de `Subversion`.

Une autre possibilité pour les utilisateurs de `Subversion` est d'utiliser l'extension {ctanpkg}`svninfo` qui possède à peu près les mêmes fonctionnalités que {ctanpkg}`svn` mais obéit à une autre logique. Elle peut, elle aussi, récupérer automatiquement la date (grâce à une option à l'appel de l'extension) et peut afficher des informations en pied de page grâce à l'extension {ctanpkg}`fancyhdr` vue à la question « {doc}`Comment définir les hauts et bas de page ? </3_composition/texte/pages/entetes/composer_des_en-tetes_et_pieds_de_page>` ».

Il est difficile de trancher entre ces deux extensions : à vous de consulter la documentation de chacune pour déterminer laquelle vous conviendra le mieux.

## Avec Git

Les extensions {ctanpkg}`gitinfo2` et {ctanpkg}`gitver` permettent de prendre en charge les documents gérés avec `Git`.

## Une solution plus générale

Il est aussi possible d'utiliser un système de script comme celui proposé par l'extension {ctanpkg}`vc` qui peut, dans certaines circonstances, se révéler plus fiable que les extensions citées ci-dessus. L'extension {ctanpkg}`vc` est compatible avec `Bazaar` (c'est la seule extension qui le soit), `Git` et `Subversion` et elle fonctionne à la fois avec LaTeX et TeX.

______________________________________________________________________

*Source :* {faquk}`Version control using RCS, CVS or the like <FAQ-RCS>`

```{eval-rst}
.. meta::
   :keywords: LaTeX, gestionnaire de révisions, versionner des fichiers LaTeX, LaTeX et Git, LaTeX et subversion, travail en commun
```

