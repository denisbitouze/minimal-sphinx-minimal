# Quelles alternatives existe-t-il aux classes standards ?

Les gens se plaisent à concocter sans cesse de nouvelles classes pour remplacer les classes standards : il a ainsi existé dans les années 80 une classe `ukart` (maintenant introuvable) pour les articles à la mode britannique, qui appelait l'extension {ctanpkg}`sober`, et changeait quelques éléments pour être conforme aux usages du Royaume-Uni comme peut le faire l'extension {ctanpkg}`babel` avec l'option `USenglish`. Il existe aussi une extension {ctanpkg}`ntgclass` proposant de remplacer les classes `article`, `report` et `book` par de nombreuses autres, conformes aux usages néerlandais (mais son développement semble avoir cessé en 1994).

:::{tip}
De façon générale, il n'est utile de développer une nouvelle classe que si la nature profonde du document diffère d'un article ou d'un livre (comme pour un poster ou une présentation de type Powerpoint). Pour personnaliser un document, il est toujours plus simple et modulaire de charger quelques extensions que de changer la classe du document.
:::

Mais deux extensions fournissent des classes qui peuvent concurrencer les classes standards : {ctanpkg}`KOMA-script <koma-script>` et {ctanpkg}`memoir`. Elles sont d'ailleurs fréquemment citées dans les réponses données par cette FAQ.

## Les classes KOMA-script

<https://framabook.org/koma-script/> L'extension {ctanpkg}`KOMA-script <koma-script>` est activement développée par [Markus Kohm](https://www.ctan.org/author/kohm). Elle fournit les classes :

- `scrartcl` pour les articles ;
- `scrreprt` pour les rapports ;
- `scrbook` pour les livres ;
- `scrlttr2` pour les lettres.

ainsi que quelques autres extensions :

- `typearea` pour calculer les largeurs des marges des pages suivant les principes du typographe renommé [Jan Tschichold](https://fr.wikipedia.org/wiki/Jan_Tschichold) ;
- `scrdate` pour accéder à la date actuelle, y compris le jour de la semaine ;
- `scrtime` pour accéder à l'heure actuelle.

Ces classes couvrent entièrement les questions importantes de composition du document et elles produisent un résultat de bonne qualité typographique. Leur documentation de référence {texdoc}`est en allemand <scrguide>`, mais elle a été progressivement {texdoc}`traduite en anglais <scrguien>`. Raymond Rochedieu en a réalisé une excellente adaptation en français, [disponible sous forme livre libre.](https://framabook.org/koma-script/)

## La classe « memoir »

L'autre classe notable est {ctanpkg}`memoir`, développée par [Peter Wilson](https://www.ctan.org/author/wilson) et actuellement maintenue par [Lars Madsen](https://www.ctan.org/author/madsen). Elle vise à remplacer directement les classes `book` et `report`, et (comme {ctanpkg}`KOMA-script <koma-script>`) elle couvre complètement tous les problèmes habituels. Son auteur la destine aux œuvres « de poésie, de fiction, de non-fiction et de mathématiques ». {texdoc}`Sa documentation (en anglais) <memman>` est très appréciée, et notamment {texdoc}`sa longue introduction <memdesign>` est régulièrement recommandée comme initiation à la typographie.

:::{note}
D'anciens tutoriels notaient des incompatibilités entre la classe `memoir` et l'extension {ctanpkg}`hyperref`. La solution préconisée était de charger un patch, `memhfixc`.

C'est maintenant inutile, `hyperref` s'occupe de le charger si besoin :

```latex
% !TEX noedit
\documentclass[...]{memoir}
...
\usepackage[...]{hyperref}
%% \usepackage{memhfixc}  <-- Ligne devenue inutile
...
\begin{document}
```
:::

______________________________________________________________________

*Sources :*

- {faquk}`Replacing the standard classes <FAQ-replstdcls>`,
- [Creating a class file vs. using packages](https://tex.stackexchange.com/questions/261094/creating-a-class-file-vs-using-packages/261115),
- [Documentation de « memoir » : projet de traduction en français](https://github.com/jejust/memoir-fr).

```{eval-rst}
.. meta::
   :keywords: LaTeX,classes standards,bonnes pratiques,classes LaTeX,mise en page
```

