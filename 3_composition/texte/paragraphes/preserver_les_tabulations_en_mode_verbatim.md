# Comment préserver les tabulations en mode verbatim ?

## Avec l'extension moreverb

L'extension {ctanpkg}`moreverb` propose un environnement `verbatimtab` qui permet de conserver des tabulations. En voici un exemple :

```latex
\documentclass{article}
\usepackage{moreverb}
\pagestyle{empty}
\begin{document}
\begin{verbatimtab}
int pattern(char *p, int n, int m)
{
        int orig = current_position();
        int new_pos;

        // Piège : la ligne suivante commence
    // par 4 espaces puis une tabulation
        goto_line(n);

        if (p && forward_search(p)
              && (current_line()<m)){
                new_pos = current_position();
                goto_char(orig);
                return(new_pos);
        }
        return(-1);
}
\end{verbatimtab}
\end{document}
```

## Avec l'extension verbasef

L'extension {ctanpkg}`verbasef` (pour *verbatim automatic segmentation of external files*) l'environnement `figure`.

## Avec des commandes de base

On peut également inclure les lignes suivantes dans le préambule du document :

```latex
% !TEX noedit
\makeatletter
{\catcode`\^^I=\active
\gdef\verbatim{
  \catcode`\^^I=\active
  \def^^I{\hspace*{4em}}%
  \@verbatim
  \frenchspacing
  \@vobeyspaces
  \@xverbatim}}
\makeatother
```

Mais le résultat sera médiocre. Ainsi dans le « piège » de l'exemple initial, LaTeX se laissera avoir et ratera son alignement.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

