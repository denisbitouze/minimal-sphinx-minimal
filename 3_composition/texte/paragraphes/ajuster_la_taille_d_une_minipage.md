# Comment optimiser la largeur d'une minipage ?

L'environnement `minipage` vous oblige à spécifier la largeur de la « page » que vous allez créer. Ceci peut être parfois gênant car vous pourriez souhaiter que LaTeX cherche à adapter cette zone afin qu'elle prenne le moins d'espace possible. Plusieurs solutions existent.

## Avec l'extension « pbox »

L'extension {ctanpkg}`pbox` définit une commande `\pbox` qui génère une boîte dont la largeur est exactement celle de la plus longue ligne qu'elle contient, en tenant compte d'une largeur maximale que vous lui donnez. Les deux commandes suivantes n'auront dès lors pas tout à fait le même comportement :

```latex
% !TEX noedit
\parbox{2cm}{Coucou !}

\pbox{2cm}{Coucou !}
```

La première produit une boîte de largeur exactement `2cm` tandis que la seconde en produit une moins large (sous réserve d'avoir une fonte peu encombrante).

L'extension fournit également :

- une commande `\settominwidth[⟨min⟩]{⟨longueur⟩}{⟨texte⟩}` qui ressemble (presque) à la commande standard `\settowidth` ;
- et une commande `\widthofpbox` analogue à la commande `\widthof`, à utiliser avec l'extension {ctanpkg}`calc`.

## Avec l'extension « eqparbox »

L'extension {ctanpkg}`eqparbox` développe l'idée de {ctanpkg}`pbox` en vous permettant de définir une série de boîtes, toutes avec la même largeur (minimisée). Notez bien qu'elle ne prend pas de paramètre de largeur maximale limite et qu'elle demande deux compilations pour que les ajustements de taille se fassent. La {texdoc}`documentation de l'extension <eqparbox>` propose un exemple présentant un *curriculum vitae* imaginaire :

```latex
% Le code est bidouillé en l'absence d'une double compilation.
\documentclass{article}
\usepackage{eqparbox}
\pagestyle{empty}
\begin{document}
\begin{tabbing}
\noindent%
\eqparbox{lieu}{\textbf{Musée D. Knuth\hspace*{1.7cm}}} \hfill \=
\eqparbox{title}{\textbf{Chef de projet\hspace*{1cm}}} \hfill \=
\eqparbox{dates}{\textbf{01/1995--présent}} \\
\noindent%
\eqparbox{lieu}{\textbf{Société L. Lamport}} \hfill \>
\eqparbox{title}{\textbf{Ingénieur}} \hfill \>
\eqparbox{dates}{\textbf{09/1992--12/1994}}
\end{tabbing}
\end{document}
```

Le code fait en sorte que, pour chacun des trois groupes (« lieu »,« titre » et « dates »), les éléments associés aient exactement la même largeur, de sorte que les lignes dans leur ensemble produisent un motif régulier tout au long de la page.

Une commande `\eqboxwidth` vous permet d'utiliser la largeur mesurée d'un groupe, ce qui permet d'obtenir d'autres effets que présente la {texdoc}`documentation de l'extension <eqparbox>` sous forme de différents exemples.

## Avec l'extension « varwidth »

L'extension {ctanpkg}`varwidth` définit un environnement `varwidth` qui répond très précisément au sujet. Vous lui donnez les mêmes paramètres que vous donneriez à `minipage` et elle adapte sa taille en fonction du contenu.

L'extension fournit également sa propre commande de texte pour mettre en forme un texte aligné à gauche, `\narrowragged`, qui rend les lignes de texte plus courtes et met plus de texte dans la dernière ligne du paragraphe, produisant ainsi des lignes avec des longueurs plus égales que ce que donne `\raggedright`.

______________________________________________________________________

*Source :* {faquk}`Automatic sizing of \\minipage <FAQ-varwidth>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,minipage,varwidth,largeur,pbox
```

