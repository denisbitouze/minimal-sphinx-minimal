# Pourquoi mon paramètre de paragraphe est-il ignoré ?

Lorsque TeX met en page du texte, il ne traite celui-ci de mot en mot ou de ligne en ligne : la plus petite unité complète qu'il formate est le paragraphe. Le paragraphe est placé dans une mémoire tampon, tel qu'il apparaît, et n'est pas touché davantage tant que le marqueur de fin de paragraphe n'est pas traité. C'est uniquement à ce moment-là que les paramètres de paragraphe ont effet. De fait, cette séquence explique souvent les erreurs qui font que les paramètres du paragraphe ne font pas ce qui était espéré (ou attendu).

Considérez l'extrait suivant :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
{\raggedleft % Justification à droite
Voici le texte à justifier à droite mais,
s'agissant du seul paragraphe concerné par cet
effet, nous pouvons terminer le groupe.}

Et voici la suite qui n'a plus besoin d'être en
justification à droite et qui redevient un texte
justifié classique pour \LaTeX.
\end{document}
```

TeX ouvre ici le groupe et impose les paramètres de réglage d'alignement à droite dans ce groupe du fait de `\raggedleft`. Il sauvegarde ensuite les quelques lignes de texte et ferme le groupe (rétablissant alors les valeurs de présentations antérieures aux modificitations opérées par `\raggedleft`). Puis TeX rencontre une ligne vierge, qu'il sait traiter comme une commande `\par`. Il compose alors le paragraphe. Cependant, comme le groupe englobant a maintenant été fermé, les réglages des paramètres ont été perdus et le paragraphe sera composé normalement...

La solution est ici simple : fermez le paragraphe à l'intérieur du groupe pour que les paramètres de réglage restent en place. Une manière appropriée de le faire consiste à remplacer les quatre dernières lignes ci-dessus par :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
{\raggedleft % Justification à droite
Voici le texte à justifier à droite mais,
s'agissant du seul paragraphe concerné par cet
effet, nous pouvons terminer le groupe.\par}
Et voici la suite qui n'a plus besoin d'être en
justification à droite et qui redevient un texte
justifié classique pour \LaTeX.
\end{document}
```

De cette façon, le paragraphe est complété alors que les paramètres de `\raggedleft` sont toujours en vigueur dans le groupe englobant.

Une autre alternative consiste à utiliser un environnement qui fait le travail approprié pour vous. Pour l'exemple ci-dessus, LaTeX en définit déjà un :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
\begin{flushright}
Voici le texte à justifier à droite mais,
s'agissant du seul paragraphe concerné par cet
effet, nous pouvons terminer le groupe.
\end{flushright}

Et voici la suite qui n'a plus besoin d'être en
justification à droite et qui redevient un texte
justifié classique pour \LaTeX.
\end{document}
```

Vous pourrez noter que l'espacement à la suite de cet environnement est différent de celui souhaité au début.

Plus généralement, il existe un certain nombre de paramètres pour lesquels TeX ne conserve qu'une valeur par paragraphe. Un d'entre eux, plutôt pénible, est l'ensemble des équivalences majuscules/minuscules, qui (assez curieusement) contraint la césure des textes mutilingues. Un autre paramètre créant régulièrement de la confusion est [\\baselineskip](/3_composition/texte/paragraphes/un_seul_baselineskip_par_paragraphe).

______________________________________________________________________

*Source :* {faquk}`Why does it ignore paragraph parameters? <FAQ-paraparam>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,\\par,paragraphe,paramètres
```

