# Comment mettre un résumé et un abstract dans un document ?

- Le package {ctanpkg}`babel` peut être utilisé, comme le montre l'exemple

```latex
% !TEX noedit
\documentclass{article}

\usepackage[T1]{fontenc}
\usepackage[english,francais]{babel}

\begin{document}
...

\selectlanguage{francais}
\begin{abstract}
Je parle français
\end{abstract}
...
\selectlanguage{english}
\begin{abstract}
I speak english
\end{abstract}

...
\end{document}
```

- Le shareware {ctanpkg}`french` (French Pro) propose les environnements `resume` et `abstract`, mais n'est pas libre de droits et n'est donc plus diffusé avec les distributions habituelles de LaTeX, il rendrait donc votre document moins portable (quelqu'un d'autre que vous ne l'aurait probablement pas).
- Autre solution qui ne permet toutefois pas d'avoir les deux textes sur la même page :

```latex
% !TEX noedit
\renewcommand{\abstractname}{Résumé}
\begin{abstract} résumé en français \end{abstract}
\renewcommand{\abstractname}{Abstract}
\begin{abstract} the same in english \end{abstract}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

