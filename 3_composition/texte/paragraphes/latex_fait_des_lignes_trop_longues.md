# Comment gérer des lignes qui débordent ?

Lorsque LaTeX construit un paragraphe, il peut faire plusieurs tentatives pour obtenir un saut de ligne correct. À chaque tentative, il exécute le même algorithme mais lui donne des paramètres différents. Vous pouvez influencer le fonctionnement de la coupure de ligne de LaTeX en ajustant ces paramètres : cette réponse traite des paramètres de « tolérance » et d'étirement. L'autre « paramètre » vital est l'ensemble des césures applicables : la question « {doc}`Pourquoi mes mots ne sont pas césurés ? </3_composition/langues/cesure/la_cesure_ne_fonctionne_pas>` » et toutes les questions liées développent ce point.

Si vous obtenez l'erreur « overfull box » (boîte qui déborde), LaTeX a tout simplement abandonné ses tentatives : les paramètres que vous lui avez donnés ne lui permettent pas de produire un résultat sans débordement. Dans ce cas, Donald Knuth a décidé que la meilleure chose à faire était de produire un avertissement et de permettre à l'utilisateur de résoudre le problème (l'alternative, passer sous silence ce problème, aurait été désagréable pour tout typographe averti). En temps normal, l'utilisateur peut presque toujours résoudre le problème en reformulant son texte --- mais ce n'est pas toujours possible. Cette réponse décrit donc les approches à adopter pour résoudre le problème, en supposant que la césure est correcte.

## Avec la commande \\linebreak

Le cas le plus simple est celui où un mot court ne parvient pas à être césuré à la fin d'une ligne. Déplacer le mot entier sur une nouvelle ligne ne fait souvent pas grande différence mais cela peut conduire à un résultat si mauvais que LaTeX le refuse par défaut. Dans ce cas, on peut *essayer* d'introduire la commande `\linebreak[n]` à l'endroit où un passage à la ligne nous paraît souhaitable, `n` étant un entier compris entre 1 et 4 : plus le nombre est élevé, plus TeX est incité à opérer le passage à la ligne, un valeur de 4 ne lui laissant pas le choix. il faut toutefois prendre garde à ce que cela ne produise pas des espacements trop larges dans la ligne que vous coupez.

Sinon, il faut ajuster les paramètres : pour ce faire, nous devons récapituler les détails des mécanismes de coupure de ligne de LaTeX.

## Avec les paramètres \\pretolerance, \\tolerance et \\emergencystretch

LaTeX procède en trois étapes :

- sa première tentative pour couper les lignes est effectuée sans même essayer de couper les mots : il considère que sa tolérance aux bizarreries de saut de ligne vaut la valeur interne `\pretolerance` et voit ce qui se passe ;
- s'il ne parvient pas à obtenir une coupure acceptable, il ajoute les points de césure autorisés et essaie à nouveau en ayant pour tolérance la valeur interne `\tolerance` ;
- si cette passe échoue également et que la valeur interne `\emergencystretch` est positive, il essaie une passe qui donne une extensibilité supplémentaire aux espaces de chaque ligne (`\emergencystretch` justement).

En principe, il existe donc trois paramètres (autres que la césure) que vous pouvez modifier : `\pretolerance`, `\tolerance` et `\emergencystretch`. Les deux premiers paramètres, traitant de la tolérance, sont de simples nombres et doivent être définies avec des primitives TeX, par exemple :

```latex
% !TEX noedit
\pretolerance=150
```

Une tolérance « infinie » est représentée par la valeur `10000` mais elle est rarement appropriée car pouvant conduire à de très mauvais sauts de ligne.

Par ailleurs, `\emergencystretch` est un registre interne à LaTeX contenant une dimension. Il peut être modifié avec `\setlength`, par exemple :

```latex
% !TEX noedit
\setlength{\emergencystretch}{3em}
```

Modifier ces paramètres peut avoir des conséquences sur la durée du traitement : chacune des passes prend du temps. Aussi, en ajouter une (en changeant `\emergencystretch`) est moins intéressant que d'en supprimer une (en changeant `\pretolerance`). Cependant, il est rare de nos jours de trouver un ordinateur suffisamment lent pour que des passes supplémentaires soient vraiment gênantes.

### La modification de \\pretolerance

Dans la pratique, `\pretolerance` n'est guère utilisé que pour manipuler l'utilisation de la césure. Plain TeX et LaTeX lui donnent pour valeur `100`. Pour supprimer le premier balayage des paragraphes, définissez `\pretolerance` sur `-1`.

### La modification de \\tolerance

Modifier `\tolerance` est souvent une bonne méthode pour ajuster l'espacement. Plain TeX et LaTeX lui attribuent la valeur `200`. La commande `\sloppy` de LaTeX lui donne la valeur de `9999`, tout comme l'environnement `sloppypar`. Cette valeur est la plus grande disponible (hors infini) et peut autoriser des pauses assez médiocres.

Il est plus satisfaisant d'apporter de petits changements à `\tolerance`, de manière incrémentielle, puis de voir comment le changement affecte le résultat. De très petites augmentations peuvent souvent suffire. Rappelez-vous que `\tolerance` est un paramètre de paragraphe : vous devez donc vous assurer qu'il est réellement appliqué comme l'explique la question « {doc}`Pourquoi mon paramètre de paragraphe est-il ignoré ? </3_composition/texte/paragraphes/parametres_non_appliques_au_paragraphe>` ». Dans la pratique, les utilisateurs de LaTeX peuvent définir un environnement pouvant englobant plusieurs paragraphes pour y appliquer leur valeur de paramètre. Voici un exemple :

```latex
% !TEX noedit
\newenvironment{tolerant}[1]{%
  \par\tolerance=#1\relax
}{%
  \par
}
```

### La modification de \\emergencystretch

Comme dit précédemment, la valeur de `\emergencystretch` est ajoutée à l'étirement supposé de chaque ligne d'un paragraphe, au cas où le paragraphe ne pourrait pas être considéré comme correct d'une autre manière. De fait, cette passe supplémentaire se produit si `\emergencystretch` est strictement supérieur à `0pt`. Supposons qu'il soit défini comme valant `3em`. Les polices *Computer Modern* contiennent généralement trois espaces dans un cadratin (un `em`), de sorte que l'indication donnée à LaTeX autoriserait jusqu'à l'équivalent de neuf espaces supplémentaires dans chaque ligne. Dans une ligne avec beaucoup d'espaces, cela pourrait être raisonnable, mais avec dans une ligne contenant uniquement trois espaces, cela pourrait conduire à étirer chaque espace jusqu'à quatre fois sa largeur naturelle. Il est donc clair que `\emergencystretch` doit être traité avec une certaine prudence.

## Avec des réglages microtypographiques

### Ceux de pdf(La)TeX

Les réglages microtypographiques proposées par pdf(La)TeX sont plus subtiles (mais plus délicates à gérer). Dans la mesure où pdf(La)TeX, le moteur par défaut pour LaTeX et ConTeXt, fonctionne dans toutes les distributions, ces réglages sont accessibles à tous. Il existe deux extensions importantes :

- le crénage marginal n'affecte que l'effet visuel de la page et n'a que peu d'effet sur la construction du paragraphe par LaTeX ;
- la déformation des caractères fonctionne comme une version plus subtile de l'astuce utilisée avec `\emergencystretch` : pdfTeX « sait » que votre police actuelle peut être étirée (ou rétrécie) dans une certaine mesure, et le fera à la volée pour optimiser le réglage d'un paragraphe. Il s'agit là d'une technique assez puissante.

### Ceux de l'extension microtype

Les réglages microtypographiques sont souvent délicats à contrôler. Toutefois, l'extension {ctanpkg}`microtype` soulage l'utilisateur du travail fastidieux de spécification des ajustements de marge et des mises à l'échelle de chaque police. Ceci se limite cependant aux polices que l'extension connaît. De manière générale, cette extension reste un bon outil et les utilisateurs qui peuvent ajouter les spécification d'ajustements pour de nouvelles polices sont évidemment les bienvenus.

______________________________________________________________________

*Source :* {faquk}`(La)TeX makes overfull lines <FAQ-overfull>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

