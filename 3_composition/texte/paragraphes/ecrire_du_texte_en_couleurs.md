# Comment écrire du texte en couleurs ?

- Voir à ce sujet le [Cahier GUTenberg numéro 16 (février 1994](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1994___16), entièrement consacré à ce problème) et l'article de \\nom{M.}\{Goossens} et \\nom{M.}\{Jouhet} dans le [Cahier GUTenberg 21 (juin 1995, pages 30-52)](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1995___21).
- Le package {ctanpkg}`xcolor` permet de colorier le texte ou le fond du document.

Utilisation de la commande `\textcolor`

```latex
% !TEX noedit
\textcolor{red}{Important}
```

- Historiquement, on utilisait le package {ctanpkg}`color` qui est de nos jours supplanté par {ctanpkg}`xcolor`.
- {ctanpkg}`pstricks` est un ensemble de macros Postscript compatibles avec Plain TeX, LaTeX, AmSTeX et AmS-LaTeX. Il comprend notamment des macros pour la colorisation, la gestion des graphiques, le dessin de camembert, d'arbres, etc.
- Voir également la question \\vref\{griser-fond-para}.

```{eval-rst}
.. meta::
   :keywords: Association GUTenberg,LaTeX,PStricks,Postscript
```

