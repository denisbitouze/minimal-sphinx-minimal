# Comment mettre en page des citations ?

Il existe deux environnements de gestion des citations :

- `quote` pour les citations courtes ;
- `quotation` pour les citations longues.

En voici un exemple :

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
\begin{quote}
L'environnement \texttt{quote} ne place pas de
retrait d'alinéa sur la première ligne de ses
paragraphes, et l'espace vertical entre ceux-ci
est supérieur à celui d'un texte standard.

La preuve !
\end{quote}

\begin{quotation}
L'environnement \texttt{quotation} place un
retrait d'alinéa sur la première ligne de ses
paragraphes, et ne rajoute pas d'espace vertical
entre ceux-ci.

La preuve !
\end{quotation}

Dans les deux cas les marges droite et gauche sont
plus importantes que celles d'un texte standard.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

