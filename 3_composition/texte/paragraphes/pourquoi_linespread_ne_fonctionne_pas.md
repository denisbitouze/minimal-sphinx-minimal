# Pourquoi la commande `\linespread` ne fonctionne pas ?

La commande `\linespread{⟨facteur⟩}` est censée multiplier le courant `\baselineskip` par le coefficient `⟨facteur⟩`. Mais, parfois, cela ne semble pas marcher.

En fait, la commande équivaut à `\renewcommand{\baselinestretch}{⟨facteur⟩}`. Ceci permet de comprendre que l'effet n'est pas immédiat : le facteur `\baselinestretch` n'est utilisé que lorsqu'une police est sélectionnée. Un simple changement de `\baselinestretch` ne change pas la police, pas plus que la commande `\fontsize{⟨taille de la fonte⟩}{⟨taille de la \baselineskip⟩}`. De fait, vous devez suivre l'une des deux solutions suivantes intégrant `\selectfont` :

```latex
% !TEX noedit
\fontsize{10}{12}%
\selectfont
```

```latex
% !TEX noedit
\linespread{1.2}%
\selectfont
```

Bien sûr, une extension telle que {ctanpkg}`setspace`, dont le travail est de gérer la ligne de base, s'occupera de tout cela, comme illustré à la question « {doc}`Comment modifier l'interligne d'un document ? </3_composition/texte/paragraphes/modifier_l_interligne>` ». Ceci dit, si vous voulez éviter {ctanpkg}`setspace`, méfiez-vous du comportement {doc}`des espaces interlignes au sein d'un paragraphe </3_composition/texte/paragraphes/un_seul_baselineskip_par_paragraphe>`.

______________________________________________________________________

*Source :* {faquk}`Why doesn't \\linespread work? <FAQ-linespread>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

