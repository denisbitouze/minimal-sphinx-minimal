# À quoi sert la commande « `\stretch` » ?

La commande `\stretch{⟨facteur⟩}` permet d'insérer une espace élastique qui va s'ajuster de manière à forcer l'occupation de toute une ligne (en mode horizontal) ou de toute une page (en mode vertical). Le paramètre ⟨facteur⟩ est appelé facteur d'élasticité, il intervient dès que plusieurs commandes `\stretch` sont appelées sur la même ligne et permet de définir la relation de proportionnalité entre ces commandes.

Ainsi, dans l'exemple ci-dessous, sur la première ligne de texte, la deuxième commande d'espacement `\hspace{\stretch{2}}` prend deux fois plus de place que la commande `\hspace{\stretch{1}}` sur la même ligne.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[body={9cm,6cm}]{geometry} % Page petite

\begin{document}
Ici\hspace{\stretch{1}} notre texte
\hspace{\stretch{2}} est peu esthétique : la
première ligne de ce paragraphe ne sera justifiée
qu'en agrandissant le premier et le troisième
blanc, le troisième étant agrandi deux fois plus
que le premier.

Un texte normal \vspace{\stretch{1}} et un texte
qui finit de remplir la ligne courante et qui se
poursuit tout en bas de la page sur la dernière
ligne. Si ce paragraphe était dans une vraie page,
le blanc nécessaire pour remplir le reste de la
page serait pris entre la première et la deuxième
ligne de ce paragraphe, en ne tenant pas compte
(ou moins compte) des autres blancs élastiques,
par exemple ceux qui sont entre deux paragraphes
ou ceux autour des titres.
\end{document}
```

```latex
\documentclass{article}
\usepackage[body={9cm,6cm}]{geometry}
\pagestyle{empty}
\begin{document}
Ici\hspace{\stretch{1}} notre texte
\hspace{\stretch{2}} est peu esthétique : la
première ligne de ce paragraphe ne sera justifiée
qu'en agrandissant le premier et le troisième blanc,
le troisième étant agrandi deux fois plus que le
premier.

Un texte normal \vspace{\stretch{1}} et un texte
qui finit de remplir la ligne courante et qui se
poursuit tout en bas de la page sur la dernière
ligne. Si ce paragraphe était dans une vraie page,
le blanc nécessaire pour remplir le reste de la
page serait pris entre la première et la deuxième
ligne de ce paragraphe, en ne tenant pas compte
(ou moins compte) des autres blancs élastiques,
par exemple ceux qui sont entre deux paragraphes
ou ceux autour des titres.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,espacement,\\stretch
```

