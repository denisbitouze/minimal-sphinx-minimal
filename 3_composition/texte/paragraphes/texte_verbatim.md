# Comment insérer un texte sans que LaTeX le mette en forme ?

L'environnement `verbatim` permet d'insérer un texte tel quel sans que LaTeX ne le traite. Il permet notamment de faire apparaître des commandes LaTeX. Il est cependant fragile et peut produire des résultats très bizarres, par exemple, il vaut mieux éviter de l'utiliser de manière inconsidérée dans des contextes curieux comme les tableaux, en argument à une autre commande, etc.

```latex
\begin{verbatim}
 La commande \LaTeX{} permet d'imprimer le logo
 LaTeX.
\end{verbatim}
```

La commande `\verb` permet de faire la même chose localement. Le premier caractère après la commande permet de marquer le début du mode `verbatim` et la prochaine occurrence de ce caractère en marque la fin. Ce peut être n'importe quel autre caractère sauf un espace, qui est ignoré après un nom de commande, une lettre, qui ferait partie du nom de commande (LaTeX croirait alors lire un appel, par exemple, à `verbX`) ou une étoile. En complément, la commande `\verb*` fonctionne comme `\verb` mais rend les espaces visibles. L'exemple suivant illustre les deux cas :

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\pagestyle{empty}
\begin{document}
La commande \verb+\verb+ permet de taper ce
qu'on veut sans l'interpréter. Par exemple
\verb,ceci cela,. On peut également
matérialiser les blancs dans la commande
\verb+\verb*+ \verb*+comme illustré ici+.
\end{document}
```

## Avec l'extension fancyvrb

L'extension {ctanpkg}`fancyvrb` permet d'écrire du texte `verbatim` encadré, en couleur... et même dans lequel certaines commandes sont interprétées. Il propose également une solution pour inclure du verbatim dans l'argument d'une commande comme le montre la question « {doc}`Comment utiliser le mode verbatim dans une note de bas de page ? </3_composition/texte/pages/footnotes/composer_en_verbatim_dans_une_note_de_bas_de_page>` ». L'exemple suivant en montre le fonctionnement.

```latex
\documentclass{article}
\usepackage{fancyvrb}
\pagestyle{empty}
\begin{document}
\fvset{frame=single,numbers=left,numbersep=3pt,
  commandchars=\\\{\},label=Exemple}
\begin{Verbatim}
Ici, je mets mon programme.
Avec des maths : \(a, b, \alpha, \beta,\ldots\)
ou de l'\textit{italique}.

\large{Pratique, non ?}
\end{Verbatim}
\end{document}
```

## Avec l'extension fvrb-ex

```{eval-rst}
.. todo:: *Cette extension n'est plus disponible sur le CTAN. A-t-elle été renommée ?*
```

L'extension {ctanpkg}`fvrb-ex` de Denis Girou, liée à {ctanpkg}`fancyvrb`, permet de mettre côte à côte ou l'un au-dessous de l'autre un passage verbatim et son aspect final.

## Avec l'extension alltt

L'extension {ctanpkg}`alltt` définit un environnement `alltt` qui ressemble à `verbatim` parce que tous les caractère particuliers de LaTeX (les accolades, les esperluettes, les pourcents, etc.) sont désactivés. Cet environnement permet donc de montrer quelque chose qui ressemble à un source, comme par exemple du code informatique, mais en le mettant en forme un minimum grâce à l'appel de certaines commandes (gras, mathématiques, symboles, etc.).

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

