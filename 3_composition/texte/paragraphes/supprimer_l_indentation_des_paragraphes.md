# Comment supprimer le retrait sur la première ligne des paragraphes ?

## Pour un seul paragraphe : « \\noindent »

- La commande `\noindent` au début du texte permet de supprimer le retrait (ou renfoncement) de la première ligne d'un paragraphe, comme le montre cet exemple :

```latex
\documentclass{article}
\usepackage[width=8cm]{geometry}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Longtemps, je me suis couché de bonne heure.
Parfois, à peine ma bougie éteinte,
mes yeux se fermaient si vite que je n’avais pas
le temps de me dire : \og{}Je m’endors.\fg{}

\medskip

\noindent Longtemps, je me suis couché de bonne heure.
Parfois, à peine ma bougie éteinte,
mes yeux se fermaient si vite que je n’avais pas
le temps de me dire : \og{}Je m’endors.\fg{}
\end{document}
```

## Pour plusieurs paragraphes (ou tout le document)

- La commande `\setlength{\parindent}{0pt}` permet de supprimer le retrait de tous les paragraphes. Pour limiter l'effet de cette commande à une série de paragraphes, mettre ces paragraphes entre accolades et insérer `\setlength{\parindent}{0pt}` après l'accolade ouvrante.
- Enfin, il existe l'extension {ctanpkg}`parskip`. Si vous la chargez sans options spéciales :

```latex
% !TEX noedit
\usepackage{parskip}
```

elle supprimera le retrait de tous les paragraphes et elle ajoutera un petit espace vertical entre les paragraphes (de hauteur `.5\baselineskip plus 2pt`). Ce petit espace vertical est utile pour la lisibilité du document, en l'absence de renfoncement pour repérer les débuts de paragraphes.

Elle a d'autres fonctions si vous lui passez des options lors de son chargement :

- `[indent=1cm]` indentera les paragraphes de `1 cm`;
- `[skip=1.5\baselineskip plus .33ex]` ajoutera un espace de `1,5 ex` (extensible à `1,83 ex`) entre les paragraphes;
- `[parfill=6em]` imposera que la dernière ligne de chaque paragraphe se termine par au moins `6 em` d'espace vide (là encore, cette méthode permet d'identifier visuellement les sauts de paragraphe en l'absence de renfoncement à gauche).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,espace en début de paragraphe,alinea,suprimer l'alinéa,première ligne des paragraphes
```

