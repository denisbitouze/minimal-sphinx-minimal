# À quoi sert la commande `\par` ?

Elle permet de marquer explicitement la fin d'un paragraphe pour permettre à LaTeX de le mettre en page. L'utilité de cette commande se justifie par le fait que LaTeX met en page un texte en le traitant paragraphe après paragraphe. Elle est, le plus souvent, utilisée dans la définition de commandes.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

