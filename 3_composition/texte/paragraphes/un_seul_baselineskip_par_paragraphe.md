# Comment contourner la limite d'un unique `\baselineskip` par paragraphe ?

La variable `\baselineskip`, mesure de l'[interlignage](https://fr.wikipedia.org/wiki/Interlignage), n'est pas, comme on pourrait l'espérer, une propriété d'une seule ligne mais une propriété d'un paragraphe entier. Par conséquent, dans un document avec une taille de fonte de `10pt` (avec une `\baselineskip` par défaut de `12pt`), un simple caractère d'une taille plus grande sera quelque peu à l'étroit dans le paragraphe. En voici un exemple :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici ce qu'il peut arriver dès lors qu'un
typographe maladroit souhaite évoquer
l'histoire avec un grand {\Huge H} !
\end{document}
```

## Avec la commande \\strut

LaTeX s'assure que ce « H » ne déborde pas sur la ligne ci-dessus. Cependant, il ne lui donne pas « de la place pour respirer », comme il le fait pour le texte à la taille standard. Autrement dit la taille du caractère (24,88 pt) est prise en compte mais pas son `\baselineskip` (30pt). Ce problème peut être résolu par un *étai*, obtenu la commande `\strut`, qui permet d'ajouter cet espace vertical manquant dans une ligne de texte. Chaque fois que vous changez la taille de la police, LaTeX redéfinit la commande `\strut` pour l'adapter à la taille choisie. Donc, pour l'exemple, nous taperions :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici ce qu'il peut arriver dès lors qu'un
typographe maladroit souhaite évoquer
l'histoire avec un grand {\Huge H\strut} !
\end{document}
```

## Avec l'environnement quote

Dès lors que votre texte de taille plus importante tient sur plusieurs lignes, les solutions passent plutôt par l'utilisation d'environnement permettant d'isoler le bloc de texte problématique au sein de votre paragraphe. L'environnement `quote` semble ici le plus adapté.

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici ce qu'il peut arriver dès lors qu'un
typographe maladroit souhaite
\begin{quote}
\Huge évoquer l'histoire avec un grand H !
\end{quote}
\end{document}
```

Le cas où le texte inséré est plus petit que le texte courant n'est pas plus heureux :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici ce qu'il peut arriver dès lors qu'un
typographe maladroit souhaite évoquer
{\footnotesize timidement et sans grandes
fioritures l'histoire avec un grand H !}
\end{document}
```

Là, la solution de la commande `\strut` n'est pas utilisable car il n'y a pas d'*étai négatif* qui rapprocherait les lignes. La solution de l'environnement `quote` sera donc privilégiée pour isoler le petit texte du reste :

```latex
\documentclass{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici ce qu'il peut arriver dès lors qu'un
typographe maladroit souhaite évoquer
\begin{quote}
\footnotesize timidement et sans grandes
fioritures l'histoire avec un grand H !
\end{quote}
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Only one \\baselineskip per paragraph <FAQ-baselinepar>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

