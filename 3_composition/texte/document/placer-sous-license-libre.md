# Comment placer mon document sous une license libre  ?

## Document source

Pour placer votre document source (`.tex`) sous une license libre, veuillez suivre les instructions données sur le site officiel de la license concernée. Voir par exemple :

- les instructions générales pour la [GPL](https://www.gnu.org/licenses/gpl-howto.fr.html) (notamment la section *L'avis de copyright*) ;
- les instructions complémentaires pour la [Gnu Free Documentation License](https://www.gnu.org/licenses/fdl-howto.fr.html) ;
- les instructions pour les licences [Creative Commons](https://creativecommons.org/about/cclicenses/).

## Document PDF

Vous pouvez choisir d'afficher une notice {doc}`dans l'en-tête ou le pied de page </3_composition/texte/pages/composer_des_en-tetes_et_pieds_de_page>`.

Pour les licences *Creative Commons*, vous pouvez {doc}`insérer manuellement les icônes </3_composition/texte/symboles/caracteres/creative_commons>` ou utiliser l'extension {ctanpkg}`doclicense`, qui propose différentes mises en forme prêtes à l'emploi.

```latex
\documentclass{article}
\usepackage{fancyhdr}
\usepackage[ type={CC},
modifier={by-nc-sa},
version={3.0},
]{doclicense}
\fancyhf{}
\fancyhead[L]{\footnotesize{Ce document est placé sous license \doclicenseNameRef — \copyright Georges Dandin}}
\begin{document}
\thispagestyle{fancy}
Voici le document sous license.
\end{document}
```

______________________________________________________________________

*Source :* [Question liée à l'affichage d'une license Creative Commons sur Stack Exchange, avec d'autres exemples](https://tex.stackexchange.com/questions/117597/inserting-a-creative-commons-licence-into-a-latex-document)

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

