# Gestion de versions avec RCS, CVS ou équivalent

Si vous utilisez RCS, CVS, `Subversion`, `Bazaar` ou `Git` pour la gestion de version de vos documents (La)TeX, vous pouvez avoir besoin d'outils automatiques pour insérer le numéro de version dans votre document, de manière à ce qu'il soit inclus dans le rendu du document (et pas seulement caché dans un commentaire du document source).

Le solution la plus complète pour RCS et CVS est d'utiliser le paquet LaTeX {ctanpkg}`rcs`, qui vous permet de récupérer et d'afficher le contenu des informations RCS d'une manière très complète et flexible. Le paquet {ctanpkg}`rcsinfo` est plus simple et suffit pour une utilisation de base, ce qui fait qu'elle a la préférence de certains ; elle se veut compatible avec {ctanpkg}`LaTeX2HTML <latex2html>`.

Si cependant vous avez besoin d'une solution qui ne passe pas par l'utilisation d'un paquet ou qui fonctionne en pur TeX, alors vous pouvez utiliser la solution minimaliste qui suit :

```latex
% !TEX noedit
\def\RCS$#1 : #2 ${\expandafter\def\csname RCS#1\endcsname{#2}}
\RCS$Revision : 1.47 $ % ou n'importe quel autre champ RCS
\RCS$Date : 2014/01/28 18:17:23 $
...
\date{Revision \RCSRevision, \RCSDate}
```

Si vous êtes un utilisateur de `Subversion`, le paquet {ctanpkg}`svn` est le plus adapté. Il est capable de gérer automatiquement un certain nombre d'informations concernant la version utilisée :

```latex
% !TEX noedit
\documentclass{⟨foo⟩}
...
\usepackage{svn}
\SVNdate $Date$
\author{...}
\title{...}
...
\begin{document}
\maketitle
...
\end{document}
```

Si le document source a été géré avec `Subversion`, la commande `\maketitle` utilisera automatiquement la date placée dans le champ `$Date$` de `Subversion`.

Une autre possibilité pour les utilisateurs de `Subversion` est d'utiliser le paquet {ctanpkg}`svninfo` qui possède à peu près les mêmes fonctionnalités que {ctanpkg}`svn` mais obéit à une autre logique.

{ctanpkg}`Svninfo <svninfo>` peut lui aussi récupérer automatiquement la date (grâce à une option à l'appel du paquet) et peut afficher des informations en pied de page grâce au {doc}`paquet « fancyhdr » </3_composition/texte/pages/composer_des_en-tetes_et_pieds_de_page>`. Il est difficile de trancher entre ces deux paquets : à vous de consulter la documentation de chacun pour déterminer lequel vous conviendra le mieux.

Il est aussi possible d'utiliser un système de script comme celui proposé par le paquet {ctanpkg}`vc` qui peut, dans certaines circonstance, se révéler plus fiable que les paquets cités ci-dessus. Le paquet {ctanpkg}`vc` est compatible avec `Bazaar`, `Git` et `Subversion` et fonctionne à ma fois avec LaTeX et TeX. Notez que {ctanpkg}`vc` est le seul paquet compatible avec les dépôts `Bazaar`.

Enfin, les paquets {ctanpkg}`gitinfo2` et {ctanpkg}`gitver` permettent de prendre en charge les documents gérés par `Git`.

______________________________________________________________________

*Source :* {faquk}`Version control using RCS, CVS or the like <FAQ-RCS>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,gestionnaire de révisions,versionner des fichiers LaTeX,LaTeX et Git,LaTeX et subversion,travail en commun
```

