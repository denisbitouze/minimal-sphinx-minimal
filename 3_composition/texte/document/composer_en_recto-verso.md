# Comment obtenir un document recto-verso ?

:::{note}
Les options de classe dont il est fait mention ici peuvent s'utiliser aussi bien avec {ctanpkg}`report` que toute autre classe implémentant le niveau de découpage par chapitres. C'est donc le cas de {ctanpkg}`book` et tous ses dérivés.
:::

## Avec LaTeX 2e

LaTeX prévoit directement les options de classe `twoside` et `openright` indiquant respectivement que l'on est en recto-verso et que l'on souhaite commencer les chapitres sur des pages de droite.

```latex
% !TEX noedit
\documentclass[twoside,openright]{report}
```

La question « {doc}`Comment supprimer les en-têtes et bas de page de pages vierges ? </3_composition/texte/pages/supprimer_l_en-tete_et_le_pied_d_une_page_blanche>` » pourra être consultée en complément sur l'option `openright`.

## Avec LaTeX 2.09

En LaTeX 2.09 il faut passer `twoside` comme option de la commande `\documentstyle`. Ensuite, pour forcer les en-têtes de chapitre à commencer sur une page impaire, il faut inclure la commande `\cleardoublepage` avant chaque début de chapitre.

```latex
% !TEX noedit
\documentstyle[twoside]{report}

\begin{document}
\cleardoublepage
\chapter{Introduction.}
        Texte.

\cleardoublepage
\chapter{Thèse.}
        Texte.

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,recto,verso
```

