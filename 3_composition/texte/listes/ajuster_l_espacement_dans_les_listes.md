# Comment modifier les espacements dans les listes ?

[Le livre de Leslie Lamport](/1_generalites/documentation/livres/documents_sur_latex) cite les divers paramètres pour la mise en page de listes (comme `\topsep`, `\itemsep` et `\parsep`) mais il ne mentionne pas qu'ils sont définis automatiquement dans les listes standard de LaTeX :

- chaque liste exécute en effet une commande `\@list⟨niveau⟩` (le ⟨niveau⟩ apparaissant comme un chiffre romain minuscule) ;
- et la commande `\@listi` est généralement réinitialisée lorsque la taille de la police est modifiée.

En conséquence, il est assez difficile pour l'utilisateur de contrôler l'espacement des listes. Bien sûr, la vraie bonne réponse est d'utiliser une classe de document conçue avec un espacement de liste plus resserré, mais de telles classes sont difficiles à trouver.

Il est à noter que certaines extensions fournissent un contrôle de l'espacement des listes, mais elles traitent rarement le cas de la séparation du texte environnant (définie par `\topsep`).

## Avec des commandes de base

Vous pouvez écrire des listes compactes avec le code suivant (s'inspirant de l'extension {ctanpkg}`mdwlist` citée ci-après) :

```latex
% !TEX noedit
\newenvironment{itemize*}%
  {\begin{itemize}%
    \setlength{\itemsep}{0pt}%
    \setlength{\parskip}{0pt}}%
  {\end{itemize}}
```

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` ne fournit pas de listes compactes, mais offre à l'utilisateur un contrôle sur l'espacement des listes en utilisant les commandes `\firmlist` et `\tightlist` (et leurs versions étoilées). Ceci est détaillé dans la section 8.6 du manuel de {ctanpkg}`memoir`.

## Avec l'extension « expdlist »

L'extension {ctanpkg}`expdlist`, parmi ses nombreux contrôles d'apparence des listes `description`, propose un paramètre de compactage (voir la documentation).

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *Sans doute à compléter.* »
```

## Avec l'extension « mdwlist »

L'extension {ctanpkg}`mdwlist` propose une commande `\makecompactlist` pour les définitions de liste par les utilisateurs. Elle s'en sert d'ailleurs pour définir des listes compactes `itemize*`, `enumerate*` et `description*`.

## Avec l'extension « paralist »

L'extension {ctanpkg}`paralist` fournit plusieurs environnements pour le compactage de listes :

- `asparaenum` présente chaque élément comme s'il s'agissait d'un paragraphe introduit par son numéro dans la liste (ce qui économise de l'espace si les éléments listés sont longs) ;
- `compactenum` donne le même genre de liste compacte que celles fournies dans {ctanpkg}`expdlist` et {ctanpkg}`mdwlist` ;
- `inparaenum` produit une liste « dans le paragraphe », c'est-à-dire sans saut de ligne entre les éléments, ce qui conduit à un large gain d'espace si les éléments listés sont courts.

L'extension modifie par ailleurs les étiquettes de l'environnement `enumerate` {doc}`comme le fait </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>` l'extension {ctanpkg}`enumerate`.

Enfin, cette extension fournit également des environnements similaires à ceux présentés pour `enumerate` pour `itemize` (`asparaitem` et autres) ou `description` (`asparadesc` et autres).

## Avec l'extension « multenum »

L'extension {ctanpkg}`multenum` offre une mise en forme plus structurée que celle de l'environnement `inparaenum` de l'extension {ctanpkg}`paralist`, pour ne pas dire une forme tabulée. Vous définissez en effet une grille fictive sur laquelle les entrées de liste doivent être disposées, et les éléments de liste respecteront ces positions dans la grille. L'effet est un peu comme celui des touches « tabulation » sur les machines à écrire traditionnelles. Cette extension a été conçue par créer des listes de réponses dans les annexes d'un livre.

## Avec l'extension « enumitem »

Les extensions {ctanpkg}`expdlist`, {ctanpkg}`mdwlist` et {ctanpkg}`paralist` offrent toutes d'autres fonctionnalités pour la configuration de la liste. Les approches décrites ci-dessous devraient être évitées si vous avez besoin de l'une de ces extensions pour un autre objectif de configuration de liste.

Afin d'obtenir plus de flexibilité (y compris la manipulation de `\topsep`), l'extension {ctanpkg}`enumitem` permet d'ajuster les paramètres de la liste en utilisant un format *`⟨clé⟩=⟨valeur⟩`*. Par exemple, pour supprimer les espaces avant et après la liste, on pourrait écrire :

```latex
% !TEX noedit
\usepackage{enumitem}
...
\begin{enumerate}[topsep=0pt, partopsep=0pt]
\item ...
\item ...
\end{enumerate}
```

De manière similaire, voici un exemple de définition de l'espacement entre les éléments et entre les paragraphes dans les éléments :

```latex
% !TEX noedit
\usepackage{enumitem}
...
\begin{enumerate}[itemsep=2pt,parsep=2pt]
\item ...
\item ...
\end{enumerate}
```

L'extension {ctanpkg}`enumitem` permet également de manipuler le format de l'étiquette d'une manière plus « basique » (et donc plus flexible) que {doc}`l'extension enumerate </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>`.

## Avec l'extension « savetrees »

Le nec plus ultra en matière de compactage (de toutes sortes) est offert par l'extension {ctanpkg}`savetrees` (littéralement « Sauvez des arbres »). Le compactage des listes fait parti des solutions retenues mais elles sont assez radicales. Si vous avez des contraintes de mise en forme, ne l'utilisez pas.

______________________________________________________________________

*Source :* {faquk}`How to adjust list spacing <FAQ-complist>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,listes,énumérations,espaces entre les items,espaces dans une liste,économiser l'espace
```

