# Comment modifier le style des listes numérotées ?

L'extension {ctanpkg}`enumerate` vous permet de contrôler le format du compteur d'énumération. Il ajoute ainsi un paramètre facultatif à l'environnement `enumerate` pour spécifier ce format. Ce paramètre contient un type d'énumération (`1` pour les chiffres arabes, `a` ou `A` pour l'énumération alphabétique et `i` ou `I` pour les chiffres romains) agrémenté d'autres éléments pour décorer l'énumération. L'exemple suivant permet d'obtenir une énumération commençant par **(a)**, **(b)**...

```latex
% !TEX noedit
\documentclass{article}
\usepackage{enumerate}

\begin{document}
Voici une liste numérotée :
\begin{enumerate}[(a)]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

```latex
\documentclass{article}
\usepackage{enumerate}
\pagestyle{empty}
\begin{document}
Voici une liste numérotée :
\begin{enumerate}[(a)]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

Cet autre exemple vous donnera une énumération commençant par **I/**, **II/**...

```latex
% !TEX noedit
\documentclass{article}
\usepackage{enumerate}

\begin{document}
Voici une liste numérotée :
\begin{enumerate}[I/]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

```latex
\documentclass{article}
\usepackage{enumerate}
\pagestyle{empty}
\begin{document}
Voici une liste numérotée :
\begin{enumerate}[I/]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

## Avec l'extension « paralist »

L'extension {ctanpkg}`paralist`, dont le but principal est {doc}`le compactage de listes </3_composition/texte/listes/ajuster_l_espacement_dans_les_listes>`, fournit les mêmes fonctionnalités simples que {ctanpkg}`enumerate` pour les environnements de type `enumerate`.

## Avec l'extension « enumitem »

Si vous avez besoin de présentations non stéréotypées, l'extension {ctanpkg}`enumitem` vous offre des fonctionnalités plus poussées et une plus grande flexibilité. Ainsi, le dernier exemple vu pour {ctanpkg}`enumerate` pourrait être réalisé avec le code suivant :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{enumitem}

\begin{document}
Voici une liste numérotée :
\begin{enumerate}[label=\Roman{*}/]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

```latex
\documentclass{article}
\usepackage{enumitem}
\pagestyle{empty}
\begin{document}
Voici une liste numérotée :
\begin{enumerate}[label=\Roman{*}/]
\item concise ;
\item claire ;
\item précise ;
\item et exemplaire.
\end{enumerate}
\end{document}
```

Notez que le caractère `*` représente dans ce cas le compteur de liste à ce niveau. Vous pouvez également modifier le format des références aux étiquettes d'éléments de liste. Dans l'exemple ci-dessous, la référence au troisième élément est mise sous la forme de chiffre romain minuscule encadré de deux tirets :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{enumitem}

\begin{document}
Voici une liste numérotée (et précise surtout au point \ref{test}) :
\begin{enumerate}[label=\Roman{*}/, ref=-\roman{*}-]
\item concise ;
\item claire ;
\item précise ; \label{test}
\item et exemplaire.
\end{enumerate}
\end{document}
```

```latex
\documentclass{article}
\usepackage{enumitem}
\pagestyle{empty}
\begin{document}
Voici une liste numérotée (et précise surtout au point -iii-) :
\begin{enumerate}[label=\Roman{*}/, ref=-\roman{*}-]
\item concise ;
\item claire ;
\item précise ; \label{test}
\item et exemplaire.
\end{enumerate}
\end{document}
```

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` inclut des fonctions qui correspondent à celles de l'extension {ctanpkg}`enumerate` et propose des fonctionnalités similaires pour les listes de l'environnement `itemize`.

______________________________________________________________________

*Source :* {faquk}`Fancy enumeration lists <FAQ-enumerate>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,listes,énumérations,chiffres romains,numéroter avec des lettres
```

