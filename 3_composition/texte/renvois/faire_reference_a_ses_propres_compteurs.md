# Comment faire référence à ses propres compteurs ?

Après avoir défini un compteur personnel (avec la commande `\newcounter`), il est possible d'y faire référence avec les commandes `\label` et `\ref`. Pour cela, il faut l'incrémenter avec la commande `\refstepcounter` au lieu de `\stepcounter`. Voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\newcounter{regle}
\setcounter{regle}{0}

\begin{document}
\section{Grands principes}
\refstepcounter{regle} Règle \theregle : j'ai raison.

\refstepcounter{regle} Règle \theregle : j'ai même toujours raison. \label{test}

La règle \ref{test} ne souffre aucune exception.
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\newcounter{regle}
\setcounter{regle}{0}
\pagestyle{empty}
\begin{document}
\section{Grands principes}
\refstepcounter{regle} Règle \theregle : j'ai raison.

\refstepcounter{regle} Règle \theregle : j'ai même toujours raison. \label{test}

La règle 2 ne souffre aucune exception.
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Making labels from a counter <FAQ-labelctr>`

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

