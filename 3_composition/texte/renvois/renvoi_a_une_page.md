# Comment obtenir un renvoi à une page ?

## Avec les commandes de base

La commande `\pageref{`*nom*`}` permet de renvoyer à la page où a été exécutée la commande `\label{`*nom*`}` correspondante.

## Avec l'extension « varioref »

L'extension {ctanpkg}`varioref` permet, lorsqu'on fait référence à la page courante ou à une de ses voisines, de modifier le style de citation, pour obtenir, par exemple, « à la page suivante ». Il faut préciser la langue voulue (par exemple, `[french]`) en argument optionnel de `\usepackage`, au moment de charger {ctanpkg}`varioref`. En voici un exemple :

```latex
% !TEX noedit
\documentclass{report}
\usepackage[francais]{babel}
\usepackage[french]{varioref}

\begin{document}
Remplir la table~\ref{tbl-add}~\vpageref{tbl-add}
\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{c|c}
      \hline
      Question & Réponse  \\
      \hline
      1 + 12 & \\
      45 + 76 & \\
    \end{tabular}
    \caption{Additions. \label{tbl-add}}
  \end{center}
\end{table}
\end{document}
```

```latex
\documentclass{report}
\usepackage[francais]{babel}
\usepackage[french]{varioref}
\pagestyle{empty}
\begin{document}
Remplir la table~1~\vpageref{tbl-add}
\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{c|c}
      \hline
      Question & Réponse  \\
      \hline
      1 + 12 & \\
      45 + 76 & \\
    \end{tabular}
    \caption{Additions. \label{tbl-add}}
  \end{center}
\end{table}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,références croisées,renvoi,page
```

