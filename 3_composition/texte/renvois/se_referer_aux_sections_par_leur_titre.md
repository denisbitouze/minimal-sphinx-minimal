# Comment obtenir des références intégrant des noms ?

Le mécanisme de références croisées de LaTeX est conçu pour le monde assez impersonnel de la publication académique, dans lequel tout a un numéro. Aussi, une extension est nécessaire si nous voulons afficher le *nom* des choses auxquelles nous faisons référence. Les deux extensions disponibles modifient les commandes de sectionnement pour obtenir ce résultat.

## Avec l'extension « titleref »

L'extension {ctanpkg}`titleref` fournit la commande `\titleref`. Il convient cependant de ne pas l'utiliser dans un document dans lequel vous devez également utiliser l'extension {ctanpkg}`hyperref`.

## Avec l'extension « byname »

L'extension {ctanpkg}`byname <smartref>` fait partie de l'ensemble {ctanpkg}`smartref` et fonctionne d'ailleurs bien avec {ctanpkg}`smartref`. Elle fonctionne (dans une certaine mesure) avec {ctanpkg}`hyperref` mais les liens qu'elle définit ne sont pas des hyperliens.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` incorpore la fonctionnalité de {ctanpkg}`titleref` mais ne fonctionne pas avec {ctanpkg}`byname <smartref>` (mais des correctifs existent).

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « // Trouver ces correctifs. // »
```

## Avec l'extension « nameref » (et « hyperref »)

L'ensemble {ctanpkg}`hyperref` comprend une extension {ctanpkg}`nameref`, qui fonctionnera seule (c'est-à-dire sans {ctanpkg}`hyperref` mais alors, bien sûr, les références ne sont pas hypertextes). Les commandes de l'extension {ctanpkg}`nameref` peuvent être utilisées dans un document de classe {ctanpkg}`memoir`.

Si vous chargez l'extension {ctanpkg}`hyperref` elle-même, alors {ctanpkg}`nameref` est automatiquement chargée. La classe {ctanpkg}`memoir` nécessite l'extension {ctanpkg}`memhfixc` lors de l'exécution avec {ctanpkg}`hyperref`, comme suit :

```latex
% !TEX noedit
\documentclass[...]{memoir}
...
\usepackage[...]{hyperref}
\usepackage{memhfixc}
```

## Avec l'extension « zref »

L'extension {ctanpkg}`zref` propose un remplacement complet des mécanismes de références croisées de LaTeX et offre en particulier une fonctionnalité de référence avec le nom :

```latex
% !TEX noedit
\usepackage[user,titleref]{zref}
...
\section{Un titre}\zlabel{sec:un}
Le nom de la section est : \ztitleref{sec:un}.
```

Chacune des extensions {ctanpkg}`titleref`, {ctanpkg}`byname <smartref>` et {ctanpkg}`nameref` définit une commande de référence avec le même nom que l'extension : `\titleref`, `\byname` et `\nameref`. L'extension {ctanpkg}`nameref` définit également une commande `\byshortnameref`, qui utilise l'argument optionnel court des commandes de chapitre et de section. Par contre, l'extension {ctanpkg}`zref`, pour du même auteur (Heiko Oberdiek), // ne définit pas // une telle variante.

______________________________________________________________________

*Source :* {faquk}`Referring to things by their name <FAQ-nameref>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,références croisées,nom,structure
```

