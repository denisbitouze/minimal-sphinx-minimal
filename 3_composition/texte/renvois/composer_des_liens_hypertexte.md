# Comment obtenir des hyperliens ?

## Avec l'extension « hyperref »

Sur ce sujet, l'extension {ctanpkg}`hyperref` se pose en référence. Elle redéfinit les commandes permettant de faire des références croisées, afin que ces références créent des liens hypertextes dans la version PDF du document.

## Avec l'extension « hyperlatex »

L'extension {ctanpkg}`hyperlatex` permet de faire des liens hypertextes. Cette extension propose également d'obtenir soit un document DVI, soit un document HTML.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

