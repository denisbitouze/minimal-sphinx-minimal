# Pourquoi ma référence croisée n'est pas correcte ?

Parfois, malgré de multiples exécutions de LaTeX, les références croisées restent tout simplement fausses. Il est ici probable que vous avez placé l'étiquette avant que les données de l'étiquette ne soient définies. Ainsi, si l'étiquette enregistre la référence d'une commande `\caption`, la commande `\label` doit apparaître *après* la commande `\caption`, ou en faire partie. Voici ici deux exemples corrects :

```latex
% !TEX noedit
\begin{figure}
  ... la figure ...
  \caption{Ma figure}
  \label{mafig}
\end{figure}
```

```latex
% !TEX noedit
\begin{figure}
  ... la figure ...
  \caption{Ma figure \label{mafig}}
\end{figure}
```

Par contre, l'exemple suivant ne fonctionnera pas bien et l'étiquette indiquera le numéro de la section (ou autre) :

```latex
% !TEX noedit
\begin{figure}
  ... la figure ...
  \label{mafig}
  \caption{Ma figure}
\end{figure}
```

Vous pouvez, avec les mêmes conséquences fâcheuses, protéger la commande `\caption` de sa commande `\label` associée, en enfermant la légende dans un environnement qui lui est propre. En voici une illustration :

```latex
% !TEX noedit
\begin{figure}
  ... la figure ...
  \caption{Ma figure}
\end{figure}
\label{mafig}
```

Ici, la commande `\label` est bien après la commande `\caption`, mais parce que l'environnement `figure` s'est fermé avant la commande `\label`, `\caption` n'est plus « visible » de `\label`.

En résumé, la commande `\label` doit être *après* la commande qui la définit (par exemple, `\caption`), et si la commande `\caption` est à l'intérieur d'un environnement, la commande `\label` doit y figurer également.

______________________________________________________________________

*Source :* {faquk}`LaTeX gets cross-references wrong <FAQ-crossref>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```

