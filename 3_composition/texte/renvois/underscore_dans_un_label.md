# Comment utiliser un tiret bas dans le texte hors du mode mathématique ?

## Problème

Par défaut, le tiret bas (`_`) se voit attribué le *catcode* 8, {doc}`qui introduit les indices en mode mathématique </2_programmation/syntaxe/catcodes/liste_des_catcodes>`. Si vous l'utilisez tel quel dans votre document, il est fort probable que vous obteniez une {doc}`erreur « Missing $ inserted » </2_programmation/erreurs/m/missing_dollar_inserted>`.

## Comment faire pour le tiret bas soit traité comme un caractère normal

### Cas où le tiret bas est considéré comme un caractère normal sans ajout d'extensions

Le tiret bas est considéré par défaut comme un caractère normal dans les arguments de :

- `\label`
- `\ref`
- `\input`
- `\include`
- `\includegraphics`
- `\begin` et `\end`
- compteurs
- paramètres de placement (comme `[t]`).

Par exemple :

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
\section{Hello World}
\label{sec_hello}
See section 1.
\end{document}
```

Si vous rencontrez quand même le message d'erreur indiqué quand vous utilisez un tiret bas dans un de ces cas, vous utilisez sans doute une extension qui modifie le comportement du tiret bas (autre que {ctanpkg}`babel`, qui ne pose pas de problèmes). Dans ce cas, vous devez appliquer l'une des solutions suivantes.

### Solution simple et généralement suffisante

L'extension {ctanpkg}`underscore` redéfinit le tiret bas comme un caractère actif qui a le comportement suivant :

- En mode mathématique, pas de changement (introduit un caractère en indice) ;
- Dans le corps du texte, affiche un tiret bas ;
- S'il est précédé du caractère `\`, affiche un tiret bas après lequel il est possible de faire une coupure de mots.

Dans la plupart des cas, cela correspond à l'effet souhaité. Cependant, la redéfinition du tiret bas par {ctanpkg}`underscore` a pour conséquence qu'il n'est plus considéré comme un caractère normal {doc}`dans les cas où il devrait être considéré comme tel </3_composition/texte/renvois/underscore_dans_un_label#cas_ou_le_tiret_bas_est_considere_comme_un_caractere_normal_sans_ajout_d_extensions>`. Ce problème est résolu en grande partie si vous chargez {ctanpkg}`babel`, ou de manière plus limitée en utilisant l'options `[strings]` de {ctanpkg}`underscore`. Pour plus de détails, veuillez vous référer à la {texdoc}`documentation <underscore>`, particulièrement p. 2.

```latex
\documentclass{article}

\usepackage[french]{babel}
\usepackage{underscore}

\pagestyle{empty}
\begin{document}
\section{Le fichier test\string_tiret-bas.pdf}
\label{sec_hello}
Nous étudions ici le fichier test\string_tiret-bas.pdf.

\section{Autre section}
Voir la section 1.
\end{document}
```

### Solution plus souple mais plus complexe

Si les effets du tiret bas ont été redéfinis par d'autres extensions avec lesquelles l'extension {ctanpkg}`underscore` interfère, on peut, au lieu de charger l'extension {ctanpkg}`underscore`, utiliser ponctuellement la commande `\string`, qui indique que le caractère qui suit immédiatement doit être traité comme un caractère normal (plus précisément, de la catégorie 12).

```latex
% !TEX noedit
\label{sec\string_hello}
\ref{sec\string_hello}
```

Comme le nom de l'étiquette est également écrit dans le fichier `.aux`, il faut assigner au tiret bas la catégorie (*catcode*) 12 lors de la lecture de ce fichier :

```latex
% !TEX noedit
\usepackage{atveryend}
\AfterLastShipout{\catcode`\_=12\relax}
```

Si l'extension en question rend le tiret bas actif avant le `\begin{document}`, alors il faut qu'il soit inactif pendant la lecture du fichier `.aux` à la fin du préambule.

```latex
% !TEX noedit
\ifnum\catcode`\_=\active
  \catcode`\_=12\relax
  \AtBeginDocument{\catcode`\_=\active}%
\fi
```

Voici le code complet de l'exemple :

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
\section{Bonjour !}
\label{hello}
Voir section 1.
\end{document}
```

______________________________________________________________________

*Sources :*

- [Putting an underscore in a « \\label »](https://tex.stackexchange.com/questions/121416/putting-an-underscore-in-a-label),
- {texdoc}`documentation de l'extension underscore <underscore>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,références,underscore,trait de soulignement, tiret bas, underscore dans les étiquettes,caractères actifs dans les étiquettes, commande \\string
```

