# Cas général

Les commandes de base pour faire des références croisées sont au nombre de trois :

- `\label{⟨nom⟩}`, qui « mémorise » la valeur du compteur courant ;
- `\ref{⟨nom⟩}` qui affiche la valeur mémorisée par la commande `\label` pour le même `⟨nom⟩` ;
- et la commande `\pageref{⟨nom⟩}` qui affiche le numéro de la page où a été exécutée la commande `\label` pour le `⟨nom⟩`.

Il est ensuite nécessaire de compiler le document deux fois, afin que toutes les références soient correctes.

Chaque ⟨nom⟩ passé en argument de ces commandes peut contenir des lettres, chiffres, et éventuellement des symboles de ponctuation. Les symboles de ponctuation peuvent cependant poser des problèmes avec les versions de {ctanpkg}`babel` antérieures à la version 3.6.

