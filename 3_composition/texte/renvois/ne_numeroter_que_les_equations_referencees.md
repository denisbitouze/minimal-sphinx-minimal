# Comment numéroter les seules équations auxquelles il est fait référence ?

En matière de numérotation d'équations, deux grandes écoles existent :

- celle qui considère que des documents qui numérotent chaque équation semblent désordonnés ;
- celle qui recommande de numéroter toutes les équations, au cas où un lecteur voudrait faire référence à une équation sur laquelle l'auteur n'a fait aucune référence croisée.

Si vous appartenez à la première école, plusieurs solutions s'offrent à vous.

## Avec les commandes de base et l'extension amsmath

En utilisant la commande `\nonumber` sur certaines équations ou en utilisant les environnements non numérotés de l'extension {ctanpkg}`amsmath` tels que `align*`, vous pouvez marquer les équations à ne pas numéroter. Toutefois, dans un article long ou complexe, cette procédure pourrait bien devenir profondément fastidieuse.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath}

\begin{document}
Voici comment ne pas numéroter la première équation :
\begin{align}
x=y+1 \nonumber \\
y=x-1
\end{align}

Voici comment ne pas numéroter les deux équations :
\begin{align*}
x=y+1  \\
y=x-1
\end{align*}
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\begin{document}
Voici comment ne pas numéroter la première équation :
\begin{align}
x=y+1 \nonumber \\
y=x-1
\end{align}

Voici comment ne pas numéroter les deux équations :
\begin{align*}
x=y+1  \\
y=x-1
\end{align*}
\end{document}
```

## Avec l'extension mathtools

L'extension {ctanpkg}`mathtools` propose une option `showonlyrefs` pour sa commande `\mathtoolsset`. Lorsque cette option est activée, seules les équations auxquelles vous faites référence sont numérotées dans la sortie finale. La consultation de la documentation de l'extension est recommandée pour avoir plus de détails sur la façon de faire des références dans ce cas.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathtools}
\mathtoolsset{showonlyrefs}

\begin{document}
Seule la deuxième équation nous intéresse \refeq{eq:b}. % Et non \ref{eq:b}.
\begin{align}
x=y+1 \label{eq:a} \\
y=x-1 \label{eq:b}
\end{align}
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Numbers for referenced equations only <FAQ-mathonlyref>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,référence,équations
```

