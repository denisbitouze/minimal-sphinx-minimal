# Comment modifier le format des étiquettes ?

Par défaut, lorsqu'une étiquette est créée, elle prend l'apparence du compteur étiqueté (comme le ferait la commande `\the⟨compteur⟩`). Ce n'est pas toujours ce dont vous avez besoin : par exemple, si vous avez imbriqué deux listes, celle de niveau 1 numérotée et celle de niveau 2 {doc}`étiquetée avec des lettres </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>`, vous pourriez vous attendre à faire référence aux éléments de la liste du niveau 2 avec, par exemple « 2(d) ». Vois trois méthodes pour le faire.

## Avec les commandes de bases

Ceci est bien sûr possible en étiquetant explicitement chaque niveau de liste puis en faisant un appel comme :

```latex
% !TEX noedit
\ref{⟨itemNiveauUn⟩}(\ref{⟨itemNiveauDeux⟩})
```

Mais ce serait à la fois fastidieux et sujet à erreur. De plus, vous construiriez une structure délicate à modifier : vous ne pourriez pas changer toutes les références aux éléments d'une liste d'un seul coup.

LaTeX dispose en fait d'une commande de formatage d'étiquette intégrée à chaque définition d'étiquette ; par défaut, elle est sans effet mais il ne tient qu'à l'utilisateur de la modifier. Pour toute étiquette *compteur*, il y a une commande interne LaTeX `\p@⟨compteur⟩` ; par exemple, une définition d'étiquette sur un élément de liste de niveau 2 est censée être effectuée à l'aide de la commande `\p@enumii{\theenumii}`. Malheureusement, la mécanique interne n'est ici pas tout à fait correcte et vous devez modifier la commande `\refstepcounter` :

```latex
% !TEX noedit
\renewcommand*\refstepcounter[1]{\stepcounter{#1}%
  \protected@edef\@currentlabel{%
    \csname p@#1\expandafter\endcsname
      \csname the#1\endcsname
  }%
}
```

Avec ce correctif, vous pouvez maintenant, par exemple, changer les étiquettes sur toutes les listes de niveau 2 en ajoutant le code suivant dans votre préambule :

```latex
% !TEX noedit
\makeatletter
\renewcommand{\p@enumii}[1]{\theenumi(#1)}
\makeatother
```

Cela ferait apparaître les étiquettes des listes énumérées de niveau 2 sous la forme « 1(a) » (et ainsi de suite). Un changement analogue fonctionne pour tout compteur utilisé dans une commande `\label`.

## Avec l'extension « fncylab »

L'extension {ctanpkg}`fncylab` fait tout ce qui précède (y compris la modification de `\refstepcounter`). Pour obtenir le résultat ci-dessus, il suffit d'utiliser la commande suivante :

```latex
% !TEX noedit
\labelformat{enumii}{\theenumi(#1)}
```

## Avec l'extension « enumitem »

Pour finir, l'exemple ci-dessus, que nous pouvons faire de plusieurs manières différentes, a été rendu obsolète par l'apparition de l'extension {ctanpkg}`enumitem`, évoquée dans la question « {doc}`Comment modifier le style des compteurs de listes numérotées ? </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>` ». Elle mérite toute votre attention.

______________________________________________________________________

*Source :* {faquk}`How to change the format of labels <FAQ-labelformat>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,étiquette,numérotation,liste
```

