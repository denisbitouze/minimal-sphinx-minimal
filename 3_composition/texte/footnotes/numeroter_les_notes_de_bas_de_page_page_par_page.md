# Comment réinitialiser le compteur de note de bas de page à chaque page ?

La solution intuitive revient ici à réinitialiser ce compteur à chaque fois que le numéro de page est incrémenté, en utilisant le {doc}`mécanisme interne de LaTeX </2_programmation/syntaxe/compteurs/compteurs_maitres_et_esclaves>`. Hélas, les endroits dans le texte où se produisent ces incrémentations sont peu prévisibles et arrivent même rarement en bas de page imprimée. Ce qui explique que les solutions naïves changeant le compteur de note de bas de page ne marchent souvent que par hasard.

De fait, réinitialiser ce compteur s'avère être une tâche complexe, nécessitant généralement des étiquettes. Dans la mesure où ce besoin peut être assez courant (par exemple lorsque les marqueurs de note sont des symboles souvent en petit nombre), quelques extensions proposent des solutions clé en main.

## Avec les extensions « perpage » et « zref-perpage »

Les extensions {ctanpkg}`perpage` et {ctanpkg}`zref-perpage <zref>` proposent un mécanisme général pour réinitialiser les compteurs à chaque page et peuvent à l'évidence être utilisé dans le cas présent. L'interface est très simple : `\MakePerPage{footnote}` pour {ctanpkg}`perpage` ou `\zmakeperpage{footnote}` pour {ctanpkg}`zref-perpage <zref>`. Si vous souhaitez réinitialiser les compteurs à une valeur autre que 1 (par exemple pour éviter quelque chose dans la liste des symboles de LaTeX), vous pouvez utiliser : `\MakePerPage[2]{footnote}` pour {ctanpkg}`perpage`) ou `\zmakeperpage[2]{footnote}` pour {ctanpkg}`zref-perpage <zref>`.

L'extension {ctanpkg}`perpage` est compacte tandis que l'extension {ctanpkg}`zref-perpage <zref>`, en tant que « module » de l'extension {ctanpkg}`zref`, est accompagnée des mécanismes de {ctanpkg}`zref` pour améliorer les commandes `\label`, `\ref` et `\pageref` de LaTeX, ce qui offre de nombreuses autres possibilités.

## Avec l'extension « footmisc »

L'extension {ctanpkg}`footmisc` propose de nombreux outils pour contrôler l'apparence des notes de bas de page et, parmi ces outils, l'option `perpage` qui répond à la question. Si vous avez d'autres modifications à faire sur les notes de bas de page, cette seule extension pourrait bien répondre à tous vos besoins.

## Avec l'extension « footnpag »

L'extension {ctanpkg}`footnpag` répond aussi à la demande et ne traite d'ailleurs que ce point. Avec la concurrence de {ctanpkg}`perpage`, il semble de moins en moins utile.

## Avec l'extension « bidi »

L'extension {ctanpkg}`bidi` (à partir de la version 34.4) dispose d'une option `perpagefootnote` qui vous permet de réinitialiser n'importe quel compteur à chaque page, en particulier le compteur des notes de bas de page.

______________________________________________________________________

*Source :* {faquk}`Footnotes numbered "per page" <FAQ-footnpp>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,compteur,note de bas de page,réinitialiser
```

