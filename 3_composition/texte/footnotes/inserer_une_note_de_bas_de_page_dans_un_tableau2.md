# Comment obtenir une note de bas de page dans un tableau ?

Cette question est traitée dans une autre section de cette FAQ : {doc}`celle portant sur les tableaux </3_composition/tableaux/notes_de_bas_de_tableau>`.

