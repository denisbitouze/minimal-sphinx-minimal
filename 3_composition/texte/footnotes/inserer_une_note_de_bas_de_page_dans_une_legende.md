# Comment obtenir une note de bas de page dans une légende ?

Mettre des notes de bas de page dans les légendes s'avère assez délicat. Elles causent des problèmes spécifiques, en plus de ceux qui s'observent :

- avec les {doc}`notes de bas de page dans les titres de section </3_composition/texte/footnotes/inserer_une_note_de_bas_de_page_dans_un_titre>` qui impactent la liste des figures/tableaux ou provoquent des {doc}`erreurs apparemment aléatoires </2_programmation/erreurs/a/argument_of_command_has_an_extra_closing_bracket>` associées à la fragilité de la commande `\footnote` ;
- avec les {doc}`notes de bas de page dans les tableaux </3_composition/tableaux/notes_de_bas_de_tableau>` qui disparaissent purement et simplement.

Heureusement, un besoin impératif de notes de bas de page dans les légendes est extrêmement rare. Aussi, si vous rencontrez des problèmes, vérifiez si vous avez vraiment ce besoin : vous pourriez ici par exemple placer du texte en bas du flottant ou placer une note de bas de page à l'endroit où vous faites référence au flottant.

## Avec l'extension threeparttable

La {doc}`mécanique proposée par l'extension </3_composition/tableaux/notes_de_bas_de_tableau>` {ctanpkg}`threeparttable` s'applique également aux notes dans les légendes et pourrait bien être préférable à toute autre solution.

## Avec l'environnement minipage

Pour cette solution, il faut :

- utiliser l'argument optionnel de la commande `\caption` sans y mettre la commande `\footnote`. Ceci évitera à la note de bas de page d'apparaître dans la « Liste des figures » ou la « Liste des tables » ;
- et placer tout le flottant dans un environnement `minipage` pour que les notes restent avec le flottant.

En voici un exemple d'application.

```latex
% !TEX noedit
\begin{figure}
  \begin{minipage}{\textwidth}
    ...
    \caption[Exemple simple]{Exemple simple\footnote{ou presque.}}
  \end{minipage}
\end{figure}
```

Cependant, en plus de cette action, il faut également gérer la tendance de la commande `\caption` à produire parfois deux fois le texte de la note de bas de page (soit un affichage en double du « ou presque. » dans l'exemple ci-dessus). Cette erreur ne se produit qu'avec des légendes nécessitant au moins la composition de deux lignes de texte (l'exemple ci-dessus serait donc épargné). Pour ce dernier problème, à la connaissance de l'auteur, il n'y a pas de solution efficace.

La documentation de l'extension {ctanpkg}`ccaption` décrit une solution de contournement hélas assez inélégante à ce problème.

______________________________________________________________________

*Source :* {faquk}`Footnotes in captions <FAQ-ftncapt>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,légende
```

