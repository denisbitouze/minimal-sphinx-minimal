# Comment éviter la renumérotation des notes de bas de page à chaque chapitre ?

Certaines classes (par exemple {ctanpkg}`book` et {ctanpkg}`report`) considèrent les notes de bas de page de chaque chapitre sont isolées des autres : la numérotation de ces notes est ainsi réinitialisée à 1 à chaque chapitre. Ceci correspond au comportement de la numérotation des équations, figures et tables, à ceci près la numérotation des notes de bas de page n'intègre pas le numéro du chapitre en cours.

Du fait de cette correspondance, la solution pour éviter ce comportement pour les notes de base de page est similaire à la {doc}`solution pour les équations, figures et tables </3_composition/texte/renvois/numeroter_les_equations_et_figures_en_continu>`. Elle requiert donc l'extension {ctanpkg}`chngcntr` pour les versions de LaTeX antérieures à 2018. Comme les numéros ne sont pas « décorés », vous pouvez utiliser la commande `\counterwithout*` et le code se limite alors à ce qui suit.

```latex
% !TEX noedit
\counterwithout*{footnote}{chapter}
```

______________________________________________________________________

*Source :* {faquk}`Not resetting footnote numbers per chapter <FAQ-run-fn-nos>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,chapitre,numérotation
```

