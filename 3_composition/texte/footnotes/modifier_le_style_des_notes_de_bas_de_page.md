# Comment modifier le style de notes de bas de page ?

L'extension {ctanpkg}`footmisc` permet de faire cela. Elle propose deux possibilités :

- l'option `symbol`, qui utilise un (petit) nombre de symboles (et qui risque donc de poser problème s'il y a trop de notes de bas de page dans le document) ;
- l'option `symbol*`, qui est semblable à `symbol` mais réinitialise le compteur à chaque page.

Dans les deux cas, il est possible de définir une liste de symboles devant être utilisés, dans l'ordre, grâce à la commande `\DefineFNsymbols`, et de les utiliser par la suite avec la commande `\setfnsymbol`. Voir l'exemple ci-dessous ainsi que {texdoc}`la documentation de footmisc <footmisc>` pour les détails.

```latex
% !TEX noedit
\documentclass{report}

\usepackage[symbol]{footmisc}
\DefineFNsymbols{messymboles}{*\dagger{**}%
                 {***}{\dagger\dagger}%
                 {\dagger\dagger\dagger}%
                 \S{\S\S}\P{\P\P}}
\begin{document}

Pour l'instant, les symboles par défaut sont
utilisés\footnote{La preuve\dots}

\setfnsymbol{messymboles}

À partir de maintenant, ce sont les symboles
définis dans  le préambule\footnote{En commençant,
par le deuxième, puisque le compteur de notes
vaut déjà $2$.}.

Les symboles sont utilisés dans l'ordre où ils
ont été définis\footnote{Voici le troisième symbole.}.

\end{document}
```

```latex
\documentclass{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={5.5cm,6cm}]{geometry}

\usepackage[symbol]{footmisc}

\DefineFNsymbols{messymboles}{*\dagger{**}%
                 {***}{\dagger\dagger}%
                 {\dagger\dagger\dagger}%
                 \S{\S\S}\P{\P\P}}

\begin{document}
\thispagestyle{empty}

Pour l'instant, les symboles par défaut sont
utilisés\footnote{La preuve\dots}

\setfnsymbol{messymboles}

À partir de maintenant, ce sont les symboles
définis dans  le préambule\footnote{En commençant,
par le deuxième, puisque le compteur de notes
vaut déjà $2$.}.

Les symboles sont utilisés dans l'ordre où ils
ont été définis\footnote{Voici le troisième symbole.}.

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,style
```

