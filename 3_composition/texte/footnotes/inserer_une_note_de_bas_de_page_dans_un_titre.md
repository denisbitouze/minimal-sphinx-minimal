# Comment mettre une note de bas de page dans un titre ?

## Sans recourir à des extensions

Deux problèmes surviennent lors de l'insertion de la commande `\footnote` dans une commande de sectionnement (`\chapter`, `\section`...) : tout d'abord, il faut éviter que la note de bas de page apparaisse dans la table des matières ou dans les en-têtes de page. L'argument optionnel des commandes de sectionnement permet d'obtenir cet effet :

```latex
% !TEX noedit
\section[Un titre]{Un titre\footnote{avec une note de bas de page}}
```

Ensuite, il faut contourner une erreur signalée à la compilation de l'exemple précédent : la commande `\footnote` ne peut être directement utilisée dans une commande de sectionnement... sauf si cette commande est précédée de la commande `\protect` :

```latex
% !TEX noedit
\section[Un titre]{Un titre\protect\footnote{avec une note de bas de page}}
```

:::{warning}
N'utiliser que `\protect\footnote` sans recourir aux arguments optionnels des commandes de sectionnement n'est pas une bonne idée. Si les tables des matières peuvent avoir des notes en bas de page, ce n'est pas le cas des en-têtes de page. De plus, il n'existe aucun mécanisme qui permette de supprimer la note de bas de page dans les en-têtes tout en les autorisant dans les tables des matières.
:::

## Avec l'extension « footmisc »

L'option `[stable]` de l'extension {ctanpkg}`footmisc` traite les deux problèmes ci-dessus : elle permet d'utiliser `\footnote` directement dans le titre (sans `\protect`), et ne l'affichera pas dans la table des matières.

______________________________________________________________________

*Source :* {faquk}`Footnotes in LaTeX section headings <FAQ-ftnsect>`

```{eval-rst}
.. meta::
   :keywords: LaTeX, note en bas de page, footmisc, titre, appel de note dans un titre
```

