# Comment obtenir le logo AMS-LaTeX ?

Actuellement, le logo d {doc}`AMS-LaTeX </1_generalites/glossaire/que_sont_ams-tex_et_ams-latex>` tend à être le suivant :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
AMS-\LaTeX
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
AMS-\LaTeX
\end{document}
```

Toutefois, une version historique est encore souvent utilisée et c'est cette dernière qui est évoquée par la suite.

## Sans extension

Cette solution revient à réutiliser *in extenso* la définition de la commande fournie par l'extension {ctanpkg}`amsmath` :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
$\cal A$\kern-.1667em\lower.5ex\hbox{$\cal M$}\kern-.125em$\cal S$-\LaTeX
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
$\cal A$\kern-.1667em\lower.5ex\hbox{$\cal M$}\kern-.125em$\cal S$-\LaTeX
\end{document}
```

## Avec l'extension amsmath

Plus simplement, l'extension {ctanpkg}`amsmath` met à disposition la commande pour écrire AMS : `\AmS`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amsmath}
\begin{document}
\AmS-\LaTeX
\end{document}
```

```latex
\documentclass{article}
\usepackage{amsmath}
\pagestyle{empty}
\begin{document}
\AmS-\LaTeX
\end{document}
```

## Avec l'extension hologo

L'extension {ctanpkg}`hologo` contient de nombreux logos associés aux logiciels liés à TeX. Elle met à disposition en particulier celui de l'AMS.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{hologo}
\begin{document}
\hologo{AmSLaTeX}
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\usepackage{hologo}
\begin{document}
\hologo{AmSLaTeX}
\end{document}
```

______________________________________________________________________

*Source :* <https://tex.stackexchange.com/questions/263449/how-to-display-the-word-ams-latex-in-latex/263451>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

