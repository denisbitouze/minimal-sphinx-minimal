# Comment obtenir les symboles phonétiques (de l'API) ?

Les symboles de l'[alphabet typographique international](https://fr.wikipedia.org/wiki/Alphabet_phonétique_international) (API) peuvent être obtenus avec deux mtéhodes différentes selon les outils que vous utilisez.

## Pour LaTeX

### Avec l'extension tipa

L'extension {ctanpkg}`tipa`, très complète, permet d’obtenir des symboles phonétiques. En complément, l'extension {ctanpkg}`engpron`, basée sur {ctanpkg}`tipa`, facilite la saisie de la prononciation de l’anglais et de l’américain.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{tipa}
\begin{document}
Voici un exemple d'écriture phonétique, celle de l'expression anglaise
\emph{science fiction} : \textipa{/""saI@ns "fIkS@n/}.
\end{document}
```

```latex
\documentclass{article}
\usepackage{tipa}
\pagestyle{empty}
\begin{document}
Voici un exemple d'écriture phonétique, celle de l'expression anglaise
\emph{science fiction} : \textipa{/""saI@ns "fIkS@n/}.
\end{document}
```

### Avec l'extension phonetic

L'extension {ctanpkg}`phonetic` fournit également un certain nombre de symboles phonétiques.

## Pour XeLaTeX

L'utilisation directe de l'unicode avec XeLaTeX et LuaLaTeX offre une autre solution : celle de saisir directement les symboles phonétiques dans son texte. L'extension {ctanpkg}`tipa` peut aussi être utilisée dans ce cas (dans la mesure où elle permet des facilités de saisie).

```latex
% !TEX noedit
\documentclass{article}
\usepackage{fontspec}
\setmainfont{Doulos SIL}
\begin{document}
Voici un exemple d'écriture phonétique, celle de l'expression anglaise
\emph{science fiction} : /ˈsaɪəns ˌfɪkʃən/.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

