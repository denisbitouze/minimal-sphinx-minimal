# Pourquoi mes caractères n'apparaissent pas ?

Vous avez tapé du texte apparemment classique et vous l'avez compilé. Le cas le plus fréquent d'erreur concernant des caractères est :

```
! Package inputenc Error : Unicode character (...)
(inputenc) not set up for use with LaTeX.
See the inputenc package documentation for explanation.
```

Vous avez utilisé des caractères qui posent problème à l'extension {ctanpkg}`inputenc`.

Mais ce n'est pas systématique : parfois, aucune erreur ne stoppe la compilation mais le résultat ne contient pas certains des caractères que vous avez tapés. Une raison probable est que la fonte que vous avez sélectionnée n'a tout simplement pas de représentation pour les caractères en question. L'exemple suivant illustre le cas :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{dsfont}
\begin{document}
Comptons : $\mathds{1,2,3}$ !
\end{document}
```

```latex
\documentclass{article}
\usepackage{dsfont}
\pagestyle{empty}
\begin{document}
Comptons : $\mathds{1,2,3}$ !
\end{document}
```

La fonte utilisée par l'extension {ctanpkg}`dsfont` dans le cas présent, *dsrom10*, ne définit pas les caractères à partir du « 2 ». Il n'y a pas ici de réel message d'erreur : vous devez lire le fichier journal, où vous trouverez de petits messages confirmant ce point :

```latex
% !TEX noedit
Missing character : There is no 2 in font dsrom10!
Missing character : There is no 3 in font dsrom10!
```

Autrement dit « Caractère manquant : il n'y a pas de 2 dans la fonte *dsrom10* ! ».

Vous pouvez aussi recevoir des messages similaires de {ctanpkg}`dvips` lorsque vous utilisez les versions OT1 et T1 des polices fournies dans l'encodage standard d'Adobe :

```latex
% !TEX noedit
dvips : Warning : missing glyph `Delta'
```

Le processus qui génère les métriques pour l'utilisation des fontes donne aussi une instruction à {ctanpkg}`dvips` pour produire ces messages, de sorte que leur non-apparition dans la sortie imprimée est moins surprenante. De nombreux glyphes fournis dans les encodages classiques, celui de Cork en particulier, ne sont pas disponibles dans les polices Adobe. Dans ces cas, le programme `dvips` produit un rectangle noir de la taille spécifiée par le fichier de police.

______________________________________________________________________

*Source :* {faquk}`Where have my characters gone? <FAQ-misschar>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```

