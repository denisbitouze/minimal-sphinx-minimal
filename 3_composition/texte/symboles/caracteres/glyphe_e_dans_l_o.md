# Comment obtenir un e dans l'o ?

L'[o-e entrelacé](https://fr.wikipedia.org/wiki/Œ) s'obtient avec la commande :

- `\oe` pour la version en minuscule ;
- `\OE` pour la version en majuscule.

Cette commande doit être suivie d'une espace (qui ne sera pas affichée, en application des règles usuelles de LaTeX) ou être accompagnée d'accolades afin d'isoler la commande. Voici un exemple d'utilisation de ces commandes avec quelques variantes sur ce point :

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{babel}
\begin{document}
Tout comme sa s\oe ur, \OE dipe disait souvent : qui vole un \oe{}uf vole un b{\oe}uf !
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{babel}
\pagestyle{empty}
\begin{document}
Tout comme sa s\oe ur, \OE dipe disait souvent : qui vole un \oe{}uf vole un b{\oe}uf !
\end{document}
```

:::{note}
Il existe aussi les commandes `\ae` et `\AE` (« æ » et « Æ »).
:::

Mais, (notamment) avec le {doc}`codage d'entrée UTF-8 </3_composition/langues/latex_et_l_utf8>` (depuis 2018, supposé par défaut), il suffit de saisir ces o-e et a-e entrelacés (`œ`, `Œ`, `æ`, `Æ`) directement dans le fichier source :

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{babel}
\begin{document}
Tout comme sa sœur, Œdipe disait souvent : qui vole un œuf vole un bœuf (et cætera) !
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{babel}
\pagestyle{empty}
\begin{document}
Tout comme sa sœur, Œdipe disait souvent : qui vole un œuf vole un bœuf (et cætera) !
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

