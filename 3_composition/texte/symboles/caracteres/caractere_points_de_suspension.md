# Comment obtenir des points de suspension dans du texte ordinaire ?

Pour obtenir un espacement correct des [points de suspension](https://fr.wikipedia.org/wiki/Points%20de%20suspension), il est recommandé d'utiliser la commande `\dots{}` (les accolades évitant toute problématique d'espace à la suite de la commande). L'exemple ci-dessous montre que, sans cette commande, les points sont plus rapprochés.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[T1]{fontenc}
\begin{document}
Ceci est un exemple\dots{} exemplaire. Et voici ce qu'il faut éviter...
\end{document}
```

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\pagestyle{empty}
\begin{document}
Ceci est un exemple\dots{} exemplaire. Et voici ce qu'il faut éviter...
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,points de suspension,symbole
```

