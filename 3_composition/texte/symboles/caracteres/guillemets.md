# Quels guillemets utiliser en français ?

- Pour écrire en français, vous utilisez normalement le package {ctanpkg}`Babel` avec l'option `french`. Dans ce cas, les commandes `\og` et `\fg` permet d'ouvrir et de fermer les guillemets (`\og` signifie simplement « Ouvrir les guillemets », et `\fg` « Fermer les guillemets »).

La commande `\frquote{⟨texte⟩}` est aussi disponible. Elle ajuste les espaces autour des guillemets, gère les imbrications et permet des citations sur plusieurs lignes :

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[width=7cm,height=7cm]{geometry}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Qu'entendez-vous par \og{}des guillemets\fg{} ?

Elle répondit : \frquote{Je ne parle pas des \frquote{chiures de mouches}, mais des vrais guillemets.}

\bigskip
Jan \bsc{Tschichold} a écrit :

\frquote{Une typographie personnelle est une typographie défectueuse. Seuls les débutants et les imbéciles peuvent l'exiger.

La typographie de la plupart des journaux est résolument arriérée.
}
\end{document}
```

La {texdoc}`documentation de Babel-French <frenchb-doc>` est en français, et mentionne les guillemets à la page 6.

:::{important}
Une autre façon de saisir des guillements français est d'utiliser `<<` et `>>`. C'est notamment la façon recommandée par l'extension {ctanpkg}`eFrench`. Mais il vous faudra insérer vous-mêmes des espaces insécables avec `~` (certains préfèrent des espaces fines avec `\,`) :

```latex
% !TEX noedit
Qu'entendez-vous par <<~des guillemets~>> ?
```

```latex
% !TEX noedit
Qu'entendez-vous par <<\,des guillemets\,>> ?
```

```latex
Qu'entendez-vous par <<~des guillemets~>> ?
```

```latex
Qu'entendez-vous par <<\,des guillemets\,>> ?
```
:::

## Et dans les autres langues ?

```{eval-rst}
.. todo:: À rédiger. Mentionner :ctanpkg:`csquotes`.
```

______________________________________________________________________

*Sources :*

- [Les guillemets](<http://www.jean-meron.fr/attachments/File/Orthotypographie.___tudes_critiques/Jean_M__ron__Les_guillemets_(14_juin_1999).PDF>), par Jean Méron (1999);
- [Guillemets in LaTeX («  »)](https://tex.stackexchange.com/questions/2681/guillemets-in-latex);
- [French quotation marks in titles](https://tex.stackexchange.com/questions/201995/french-quotation-marks-in-titles).

```{eval-rst}
.. meta::
   :keywords: LaTeX,écrire en français,guillemets,quotes,quote marks,apostrophes,guillemets simples,citation
```

