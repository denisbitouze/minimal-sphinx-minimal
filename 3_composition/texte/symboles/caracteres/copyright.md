# Comment obtenir un symbole copyright, trademark ou registered ?

LaTeX fournit les commandes :

- `\textcopyright` pour (c), symbole du [copyright](https://fr.wikipedia.org/wiki/Copyright) ;
- `\textregistered` pour (r), symbole de [marque déposée](https://fr.wikipedia.org/wiki/Symbole%20de%20marque%20déposée) (*registered trademark*) ;
- `\texttrademark` pour (tm), symbole du [trademark](https://fr.wikipedia.org/wiki/Droit%20des%20marques) ;

Un problème survient lorsque vous voulez mettre en exposant un `\textregistered` (pour qu'il ressemble un peu à un (tm)). La mise en exposant mathématique provoque des erreurs inutiles. Aussi, vous devrez utiliser la mise en exposant textuelle :

```latex
% !TEX noedit
\textsuperscript{\textregistered}
```

Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
<< L'État\texttrademark, c'est moi\textsuperscript{\textregistered} !  >> \textcopyright{} Tous droits réservés - 1655.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
<< L'État\texttrademark, c'est moi\textsuperscript{\textregistered} ! >> \textcopyright{} Tous droits réservés - 1655.
\end{document}
```

## Avec l'extension « amssymb »

L'extension {ctanpkg}`amssymb` dispose d'une commande `\circledR` pour obtenir le (r).

```latex
% !TEX noedit
\documentclass{article}
\usepackage{amssymb}
\begin{document}
Petite comparaison : \textregistered{} et \circledR.
\end{document}
```

```latex
\documentclass{article}
\usepackage{amssymb}
\pagestyle{empty}
\begin{document}
Petite comparaison : \textregistered{} et \circledR.
\end{document}
```

## Avec l'extension « textcomp »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`textcomp` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`textcomp` fournissait historiquement des variantes plus élégantes et plus stables de `\textregistered`, `\texttrademark` et `\textcopyright` pour compenser les premières versions fournies par LaTeX. En particulier, la version de `\textregistered` de {ctanpkg}`textcomp` corrigeait un comportement inadéquat du caractère de base lorsqu'il se trouvait dans un texte en gras : le « R » majuscule (en fait une petite majuscule) devenait un « r » minuscule (en l'absence de petite majuscule grasse).

______________________________________________________________________

*Source :* {faquk}`How to get copyright, trademark, etc. <FAQ-tradesyms>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole marque déposée,symbole marque commerciale,copie interdite
```

