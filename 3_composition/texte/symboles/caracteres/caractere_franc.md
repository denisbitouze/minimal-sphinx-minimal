# Comment obtenir le symbole du franc français ?

En usage courant, le [franc français](https://fr.wikipedia.org/wiki/Franc_français) n'a pas de symbole particulier, il est simplement abrégé en **F** ou **FF**. Les deux lettres **Fr** (ou leur {doc}`ligature </3_composition/texte/mots/desactiver_une_ligature>`) sont aussi utilisées, par analogie avec le [franc suisse](https://fr.wikipedia.org/wiki/Franc_suisse).

Néanmoins, le symbole **₣** a été proposé en 1988 (et refusé) et existe dans Unicode :

```
U+20A3 FRENCH FRANC SIGN
```

Vous pouvez donc aller le chercher par son code :

```latex
\documentclass{article}
  \usepackage{libertineotf}
  \pagestyle{empty}

\begin{document}
\Large ^^^^20a3 % ASCII notation
\end{document}
```

ou, mieux, définir une unité avec {ctanpkg}`siunitx` :

```latex
\documentclass{article}
  \usepackage{libertineotf}
  \usepackage{siunitx}
  \usepackage[french]{babel}
  \pagestyle{empty}

\DeclareSIUnit{\franc}{^^^^20a3}

\begin{document}
\large Question à \SI{100}{\franc}.
\end{document}
```

Et voici quelques autres exemples :

```latex
\documentclass{article}
  \usepackage{fontspec}
  \pagestyle{empty}

\begin{document}
\fontspec{FreeSerif.otf}
\Large ^^^^20a3
\end{document}
```

```latex
\documentclass{article}
  \usepackage{fontspec}
  \pagestyle{empty}

\begin{document}
\fontspec{Comfortaa-Regular.ttf}
\Large ^^^^20a3
\end{document}
```

______________________________________________________________________

*Sources :*

- [Symboles monétaires : le franc](https://fr.wikipedia.org/wiki/Symbole_monétaire#Franc),
- [French franc](https://en.wikipedia.org/wiki/French_franc) (en anglais),
- [Currency symbol : French franc](https://tex.stackexchange.com/questions/103220/currency-symbol-french-franc).

```{eval-rst}
.. meta::
   :keywords: LaTeX,franc,franc français,symbole du franc,symboles monétaires,F avec une barre,currency units
```

