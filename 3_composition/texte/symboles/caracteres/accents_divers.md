# Comment obtenir des accents ?

Le tableau suivant donnent les commandes disponibles dans LaTeX *sans extension particulière*.

| Codage      | Exemple | Nom de l'accent    |
| ----------- | ------- | ------------------ |
| `\"{o}`     | ö       | tréma              |
| `` \`{o} `` | ò       | accent grave       |
| `\^{o}`     | ô       | accent circonflexe |
| `\'{o}`     | ó       | accent aigu        |
| `\~{o}`     | õ       | tilde              |
| `\.{o}`     | ȯ       | point              |
| `\={o}`     | ō       | surligné           |
| `\H{o}`     | ő       | tréma hongrois     |
| `\b{o}`     | o̱      | souligné           |
| `\c{o}`     | o̧      | cédille            |
| `\d{o}`     | ọ       | point au-dessous   |
| `\r{o}`     | o̊      | anneau, ring       |
| `\t{oo}`    | o͡o     | tirant             |
| `\u{o}`     | ŏ       | brève              |
| `\v{o}`     | ǒ       | caron              |

:::{note}
L'ogonek ǫ peut être obtenu grâce à `\k{o}` mais, avec `pdflatex`, nécessite l'extension {ctanpkg}`fontenc` chargée avec l'option `T1`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

