# Qu'est devenu ce symbole apparemment perdu ?

Vous compilez un ancien document qui fonctionnait très bien et certaines commandes de symboles telles que `\Box` et `\lhd` semblent maintenant ne plus exister. Ces commandes étaient présentes dans le noyau de LaTeX 2.09, mais ne sont plus dans les versions récentes de LaTeX. Elles sont désormais disponibles :

- dans l'extension {ctanpkg}`latexsym <latex-base>` qui fait partie de la distribution de base de LaTeX ;
- dans l'extension {ctanpkg}`amsfonts` qui fait partie de la distribution AMS nécessitant les polices de symboles AMS.

______________________________________________________________________

*Source :* {faquk}`Missing symbol commands <FAQ-misssymb>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreur,symbole,perdu,latexsym,amsfonts
```

