# Comment obtenir des symboles astronomiques ?

Plusieurs extensions fournissent un certain nombre de ces symboles. Elles contiennent également des symboles astrologiques, mais pas votre horoscope.

## Avec l'extension starfont

L'extension {ctanpkg}`starfont` est dédiée à ce sujet et propose plus de 70 symboles (en deux versions avec et sans empattements).

```latex
% !TEX noedit
\documentclass{article}
\usepackage{starfont}

\begin{document}
Sans empattement : \Sun \Terra \Moon \Aries \Taurus \Gemini \Ceres.

Avec empattement : \starfontserif \Sun \Terra \Moon \Aries \Taurus \Gemini \Ceres.
\end{document}
```

```latex
\documentclass{article}
\usepackage{starfont}
\pagestyle{empty}
\begin{document}
Sans empattement : \Sun \Terra \Moon \Aries \Taurus \Gemini \Ceres.

Avec empattement : \starfontserif \Sun \Terra \Moon \Aries \Taurus \Gemini \Ceres.
\end{document}
```

## Avec l'extension marvosym

L'extension {ctanpkg}`marvosym` met à disposition les symboles du soleil, de la lune, des planètes du système solaire et les signes zodiacaux.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{marvosym}

\begin{document}
\Sun \Earth \Moon \Aries \Taurus \Gemini
\end{document}
```

```latex
\documentclass{article}
\usepackage{marvosym}
\pagestyle{empty}
\begin{document}
\Sun \Earth \Moon \Aries \Taurus \Gemini
\end{document}
```

## Avec l'extension mathabx

L'extension {ctanpkg}`mathabx` met à disposition des symboles utilisables en mode mathématique : ceux du soleil, de la lune (dans plusieurs phases), des planètes du système solaire et une partie seulement des signes zodiacaux.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathabx}

\begin{document}
$\Sun \Earth \rightmoon \Aries \Taurus \Gemini$
\end{document}
```

```latex
\documentclass{article}
\usepackage{mathabx}
\pagestyle{empty}
\begin{document}
$\Sun \Earth \rightmoon \Aries \Taurus \Gemini$
\end{document}
```

## Avec l'extension wasysym

L'extension {ctanpkg}`wasysym` met à disposition une trentaine de symboles.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{wasysym}

\begin{document}
\astrosun \earth \rightmoon \aries \taurus \gemini
\end{document}
```

```latex
\documentclass{article}
\usepackage{wasysym}
\pagestyle{empty}
\begin{document}
\astrosun \earth \rightmoon \aries \taurus \gemini
\end{document}
```

## Avec l'extension pifont

En section 9 de {ctanpkg}`The Comprehensive LaTeX Symbol List <comprehensive>` (« la liste complète des symboles de LaTeX ») de Scott Pakin *et al.*, une technique est présentée pour obtenir certains symboles. En passant par l'extension {ctanpkg}`pifont`, certains symboles de fontes peuvent être chargés (sous réserve de connaître alors leur numéro dans la table de caractère). L'exemple suivant donne accès à trois polices de caractères de l'extension {ctanpkg}`astro` (qu'il faut appeler avec `astrosym`) donnant accès à des symboles astronomiques et astrologiques :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{pifont}
\DeclareFontFamily{U}{astrosym}{}
\DeclareFontShape{U}{astrosym}{m}{n}{<-> astrosym}{}

\begin{document}
Première police : \Pisymbol{astrosym}{0} \Pisymbol{astrosym}{29} \Pisymbol{astrosym}{41}
\Pisymbol{astrosym}{11} \Pisymbol{astrosym}{12} \Pisymbol{astrosym}{13}  \Pisymbol{astrosym}{48}

Deuxième police : \Pisymbol{astrosym}{100} \Pisymbol{astrosym}{129} \Pisymbol{astrosym}{141}
\Pisymbol{astrosym}{111} \Pisymbol{astrosym}{112} \Pisymbol{astrosym}{113}  \Pisymbol{astrosym}{148}

Troisième police : \Pisymbol{astrosym}{200} \Pisymbol{astrosym}{229} \Pisymbol{astrosym}{241}
\Pisymbol{astrosym}{211} \Pisymbol{astrosym}{212} \Pisymbol{astrosym}{213}  \Pisymbol{astrosym}{248}
\end{document}
```

```{eval-rst}
.. todo:: // La restitution ne se fait pas pour le moment.//
```

## Avec les fontes des extensions astro et cmastro

Les deux extensions {ctanpkg}`astro` et {ctanpkg}`cmastro` mettent à disposition des fontes contenant des symboles astronomiques.

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole,astronomie,symbole astronomique
```

