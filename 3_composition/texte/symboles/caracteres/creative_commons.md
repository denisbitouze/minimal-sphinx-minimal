# Comment obtenir les symboles des licences Creative Commons ?

- Le package {ctanpkg}`ccicons` fournit tous les symboles des [licences Creative Commons](https://fr.wikipedia.org/wiki/Creative_Commons), y compris ceux avec les différentes monnaies (dollar,euro et yen) :

```latex
\documentclass{article}
\usepackage{ccicons}
\pagestyle{empty}

\begin{document}
\begin{tabular}{ll}
\verb+\ccLogo+            & \ccLogo \\
\verb+\ccAttribution+     & \ccAttribution \\
\verb+\ccShareAlike+      & \ccShareAlike \\
\verb+\ccNoDerivatives+   & \ccNoDerivatives \\
\verb+\ccNonCommercial+   & \ccNonCommercial \\
\verb+\ccNonCommercialEU+ & \ccNonCommercialEU \\
\verb+\ccNonCommercialJP+ & \ccNonCommercialJP \\
\verb+\ccZero+            & \ccZero \\
\verb+\ccPublicDomain+    & \ccPublicDomain \\
\verb+\ccPublicDomainAlt+ & \ccPublicDomainAlt \\
\verb+\ccSampling+        & \ccSampling \\
\verb+\ccShare+           & \ccShare \\
\verb+\ccRemix+           & \ccRemix \\
\verb+\ccCopy+            & \ccCopy \\
\end{tabular}
\end{document}
```

Les différentes combinaisons ont également leurs commandes spécifiques :

```latex
\documentclass{article}
\usepackage{ccicons}
\pagestyle{empty}

\begin{document}
\begin{tabular}{ll}
\verb+\ccby+       & \ccby \\
\verb+\ccbysa+     & \ccbysa \\
\verb+\ccbynd+     & \ccbynd \\
\verb+\ccbync+     & \ccbync \\
\verb+\ccbynceu+   & \ccbynceu \\
\verb+\ccbyncjp+   & \ccbyncjp \\
\verb+\ccbyncsa+   & \ccbyncsa \\
\verb+\ccbyncsaeu+ & \ccbyncsaeu \\
\verb+\ccbyncsajp+ & \ccbyncsajp \\
\verb+\ccbyncnd+   & \ccbyncnd \\
\verb+\ccbyncndeu+ & \ccbyncndeu \\
\verb+\ccbyncndjp+ & \ccbyncndjp \\
\verb+\cczero+     & \cczero \\
\verb+\ccpd+       & \ccpd \\
\end{tabular}
\end{document}
```

______________________________________________________________________

*Source :*

- [Des icônes à l’infini dans LATEX](https://la-bibliotex.fr/2019/02/10/des-icones-a-infini-dans-latex/),
- [Les licences Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,caractères,logos Creative Commons,symboles de licences Creative Commons
```

