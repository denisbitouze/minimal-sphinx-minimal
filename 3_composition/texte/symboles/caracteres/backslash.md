# Comment obtenir une contre-oblique ?

La contre-oblique « `\` », aussi nommée barre oblique inverse ou antislash, est un des {doc}`caractères réservés </3_composition/texte/symboles/caracteres/caracteres_reserves>` de LaTeX puisqu'elle indique à ce dernier que ce qui la suit est une commande. Ainsi, LaTeX considère l'expression « `LaTeX` » comme un élément du texte et « `\LaTeX` » comme une commande (existante dans le cas présent).

Pour obtenir ce symbole dans un texte, il faut donc passer par une commande dédiée :

- `\textbackslash` en mode texte (le terme anglais est bien *backslash* et non *antislash* qui est un terme forgé par des français) ;
- `\backslash` en mode mathématique.

Voici un exemple où la commande est utilisée simplement dans le texte et où elle est également utilisée dans une commande rudimentaire mettant en forme un nom de commande LaTeX :

```latex
% !TEX noedit
\documentclass{article}
\newcommand{\commande}[1]{\textbackslash #1}

\begin{document}
La contre-oblique, notée \textbackslash, débute le nom des commandes \LaTeX.
Par exemple, \commande{LaTeX} est une commande.
\end{document}
```

```latex
\documentclass{article}
\newcommand{\commande}[1]{\textbackslash #1}
\pagestyle{empty}
\begin{document}
La contre-oblique, notée \textbackslash, débute le nom des commandes \LaTeX. Par exemple, \commande{LaTeX} est une commande.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,slash,antislash,backslash,saisie du caractère antislash,barre oblique inversée
```

