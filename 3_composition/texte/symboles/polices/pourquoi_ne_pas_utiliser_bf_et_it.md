# Pourquoi éviter d'utiliser les commandes `\bf`, `\it`, etc. ?

## Les commandes historiques

Les commandes de sélection de police de LaTeX 2.09 étaient `\rm`, `\sf`, `\tt`, `\it`, `\sl`, `\em` et `\bf`. Il s'agissait de commandes modales, utilisées comme suit :

```latex
% !TEX noedit
La {\bf typographie} est l'art {\it d'assembler\/} des caractères.
```

```latex
La {\bf typographie} est l'art {\it d'assembler\/} des caractères.
```

Le changement de police était généralement inclus dans un groupe, afin de limiter son effet. L'ajout manuel de la {doc}`correction d'italique </1_generalites/notions_typographie/correction_italique>` avec la commande `\/` était nécessaire à la fin d'une section en italique.

Lors de la sortie de LaTeX à l'été 1994, ces commandes simples étaient devenues obsolètes, mais comme leur utilisation est profondément ancrée dans le cerveau des utilisateurs, les commandes elles-mêmes restèrent dans LaTeX, *avec leur saveur LaTeX 2.09*. L'obsolescence de ces commandes s'explique ainsi : chacune d'elles remplace tous les autres paramètres de police, en ne conservant que la taille. Voici une illustration de ce point :

```latex
% !TEX noedit
La typographie est {\bf\it l'art d'assembler\/} des caractères.
Et {\it\bf l'art\/} est difficile !
```

```latex
La typographie est {\bf\it l'art d'assembler\/} des caractères.
Et {\it\bf l'art\/} est difficile !
```

Dans la première phrase, la commande `\bf` est ignorée et le texte est mis en italique (et la correction d'italique à un effet réel). Dans la seconde phrase, la commande `\it` est ignorée et le texte est mis en gras (la correction d'italique ne sert à rien). Ce type d'effet persiste si des commandes LaTeX sont mélangées à ces commandes :

```latex
% !TEX noedit
La typographie est \textbf{\tt l'art d'assembler} des caractères.
```

```latex
La typographie est \textbf{\tt l'art d'assembler} des caractères.
```

Ici, la commande `\textbf` qui encadre le texte est ignorée. Le texte est alors présentée en police à chasse fixe.

Alors, pourquoi ces commandes sont obsolètes ? Tout simplement à cause des confusions qu'elles peuvent engendrer, comme l'illustre l'exemple précédent.

## Les commandes actuelles

Les commandes de fontes de LaTeX se présentent sous deux formes : les commandes modales et les commandes de bloc de texte. Le jeu de commandes modales par défaut permettent de sélectionner la graisse (`\mdseries` et `\bfseries`), la forme (`\upshape`, `\itshape`, `\scshape` et `\slshape`) et la famille (`\rmfamily`, `\sffamily` et `\ttfamily`). Une sélection de police nécessite une famille, une forme et une graisse (ainsi qu'une taille, bien sûr).

L'exemple suivant permet d'obtenir du texte en gras en fonte à chasse fixe ou de l'italique sans empattement :

```latex
% !TEX noedit
La {\ttfamily typographie} est l'art d'assembler des {\bfseries\ttfamily caractères}.
Et {\slshape\sffamily l'art\/} est difficile !
```

```latex
La {\ttfamily typographie} est l'art d'assembler des {\bfseries\ttfamily caractères}.
Et {\slshape\sffamily l'art\/} est difficile !
```

Les commandes de bloc de texte de LaTeX ont toutes la forme
`\text⟨suffixe⟩` où `⟨suffixe⟩` correspond aux deux premières lettres des
commandes modales. Ainsi ``\bfseries ```` devient `\textbf{}`, '' \\itshape''
devient `\textit{}`, et `\ttfamily` devient `\texttt{}`. Bien entendu, les
commandes de bloc peuvent être imbriquées :

```latex
% !TEX noedit
La typographie est \textit{\textbf{l'art d'assembler}} des caractères.
```

```latex
La typographie est \textit{\textbf{l'art d'assembler}} des caractères.
```

Nous obtenons ici du texte en italique gras (notez au passage que les commandes de bloc s'occupent automatiquement des corrections d'italique).

Ces commandes de bloc de texte peuvent s'imbriquer aussi avec les commandes modales LaTeX :

```latex
% !TEX noedit
La \texttt{\bfseries typographie} est l'art {\slshape \textbf{d'assembler}\/}
des caractères.
```

```latex
La \texttt{\bfseries typographie} est l'art {\slshape \textbf{d'assembler}\/}
des caractères.
```

Il faut cependant penser à mettre manuellement la correction d'italique à la fin du groupe si vous utilisez les commandes modales.

Avec les commandes de LaTeX, vous évitez sans le savoir beaucoup de bizarreries typographiques, par exemple des caractères droits et penchés à la fois ! Il reste cependant quelques cas qui peuvent être souhaités et qu'empêche mécaniquement ce système de sélection de fonte :

- vouloir une fonte italique (`\itshape`) ou penchée (`\slshape`) en petites capitales (`\scshape`). Si certains considèrent ce choix comme malheureux, de telles fontes existent. En l'occurrence, l'extension {ctanpkg}`smallcap` de Daniel Taupin permet de les obtenir pour les fontes {doc}`EC </5_fichiers/fontes/que_sont_les_fontes_ec>` et des techniques similaires peuvent être appliquées à de nombreux autres jeux de fontes ;
- vouloir une fonte italique (`\itshape`) droite (`\upshape`). Donald Knuth a mis à disposition une telle fonte et LaTeX l'utilise pour le symbole de la livre sterling « £ » dans le jeu de polices par défaut. Cette combinaison est suffisamment étrange pour que, bien qu'il y ait une fonte définie, aucune commande ne l'utilise par défaut. Si jamais vous deviez en avoir besoin, il vous faudra utiliser les commandes de sélection de police les plus simples de LaTeX :

```latex
% !TEX noedit
{\fontshape{ui}\selectfont La typographie... demande à rester raisonnable avec les caractères !}
```

```latex
{\fontshape{ui}\selectfont La typographie... demande à rester raisonnable avec les caractères !}
```

______________________________________________________________________

*Source :* {faquk}`What's wrong with \\\\bf, \\it, etc.? <FAQ-2letterfontcmd>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonte,\\bf,\\it,correction italique,police de caractères
```

