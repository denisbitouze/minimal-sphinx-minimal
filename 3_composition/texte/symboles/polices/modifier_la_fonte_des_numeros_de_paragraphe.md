# Comment modifier la police des numéros de paragraphe ?

- Pour modifier la police des numéros de paragraphe, il faut redéfinir `\@seccntformat`.

```latex
% !TEX noedit
\makeatletter
\renewcommand\@seccntformat[1]%
{\texttt{\@nameuse{the#1}\quad}}
\makeatother
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

