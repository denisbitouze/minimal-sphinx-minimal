# Comment l'italique droit peut-il exister ?

Question {doc}`inspirée de cette réponse </3_composition/texte/symboles/polices/pourquoi_ne_pas_utiliser_bf_et_it>`.

## Origine de l'italique

- [Design with FontForge : Italique](http://designwithfontforge.com/fr-FR/Italic.html),
- [Italic type](https://en.wikipedia.org/wiki/Italic_type), paragraphe **Upright italics**.

## L'italique droit en LaTeX

- Avec les commandes de LaTeX, vous évitez sans le savoir beaucoup de bizarreries typographiques, par exemple des caractères droits et italiques à la fois ! Il reste cependant quelques cas qui peuvent être souhaités et qu'empêche mécaniquement ce système de sélection de fonte :
- vouloir une fonte italique (`\itshape`) ou penchée (`\slshape`) en petites capitales (`\scshape`). Si certains considèrent ce choix comme malheureux, de telles fontes existent. En l'occurrence, l'extension {ctanpkg}`smallcap` de Daniel Taupin permet de les obtenir pour les fontes {doc}`EC </5_fichiers/fontes/que_sont_les_fontes_ec>` et des techniques similaires peuvent être appliquées à de nombreux autres jeux de fontes. Cette extension n'est pas dans les distributions courantes, mais vous pouvez toujours la télécharger depuis CTAN. Son idée est de faire des petites capitales une *famille*, au lieu d'une *forme*. Donc remplacer la commande `\scshape` par `\scfamily`. Ainsi, il devient possible de sélectionner des petites capitales grasses penchées en écrivant :

```latex
% !TEX noedit
{\bf\scfamily\slshape C'est la vie !}
```

- vouloir une fonte italique (`\itshape`) droite (`\upshape`). Donald Knuth a mis à disposition une telle fonte et LaTeX l'utilise pour le symbole de la livre sterling « £ » dans le jeu de polices par défaut. Cette combinaison est suffisamment étrange pour que, bien qu'il y ait une fonte définie, aucune commande ne l'utilise par défaut. Si jamais vous deviez en avoir besoin, il vous faudra utiliser les commandes de sélection de police les plus simples de LaTeX :

```latex
% !TEX noedit
{\fontshape{ui}\selectfont La typographie... demande à rester raisonnable avec les caractères !}
```

```latex
{\fontshape{ui}\selectfont La typographie... demande à rester raisonnable avec les caractères !}
```

______________________________________________________________________

*Sources :*

- [Design with FontForge : Italique](http://designwithfontforge.com/fr-FR/Italic.html),
- [Italic type](https://en.wikipedia.org/wiki/Italic_type), paragraphe **Upright italics**.

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonte,police italique,upright italics,romain et italique,histoire de l'italique
```

