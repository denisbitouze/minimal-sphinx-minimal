# Comment changer la police de tout un paragraphe ?

- Pour changer la fonte de tout un paragraphe, on utilisera plutôt les commandes `\rmfamily`, `\sffamily`, `\ttfamily`, `\bfseries`, `\mdseries`, `\itshape`, `\slshape`, `\upshape` et `\scshape`.

:::{note}
Si l'on utilise ces commandes sur un seul mot ou sur un groupe de mots dans un paragraphe, alors l'espace suivant une telle déclaration ne sera pas géré (il vaut mieux dans ce cas utiliser les commandes `\text`xx`{}`).
:::

Exemple :

```latex
% !TEX noedit
{\scshape BlaBla }
ou
\begin{itshape}
 blabla
\end{itshape}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

