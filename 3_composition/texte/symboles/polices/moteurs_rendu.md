# Puis-je changer le moteur de rendu pour éviter certains problèmes d'affichage des polices ?

pdfTeX, XeLaTeX et LuaLaTeX utilisent différentes bibliothèques pour le rendu graphique des polices OpenType. `fontspec` permet de sélectionner ce moteur grâce à l'option `Renderer`.

En particulier, désormais, LuaLaTeX utilise par défaut le moteur Harfbuzz. Il donne de très bons résultats la plupart du temps, mais, si vous constatez des problèmes, vous pouvez tenter d'utiliser l'un des autres moteurs disponibles. La liste en est donnée dans le manuel de `fontspec`, dans les parties VI (*Lua-TeX only font features*) et VII (*Fonts and features with XeTeX*).

L'option `Renderer` doit être passée non pas globalement à `fontspec` directement, mais à chaque police individuellement, par exemple :

```latex
% !TEX noedit
\setmainfont[Renderer=OpenType]{Libertinus Serif}
```

L'option `Renderer` permet également d'utiliser des fontes qui ne sont pas basées sur la technologie OpenType, mais sur AAT ou Graphite.

______________________________________________________________________

*Source :* {ctanpkg}`fontspec`

```{eval-rst}
.. meta::
   :keywords: rendering technology, shaping engine, fonte, aspect, visuel, bug
```

