# Pourquoi éviter d'utiliser la commande `\newfont` ?

Si tout le reste échoue, vous *pouvez* spécifier une fonte en utilisant la commande `\newfont`. Cette technique peut être tentante mais la fonte ainsi intégrée ne rentre pas dans le mécanisme de sélection de police de LaTeX. La commande `\newfont` consiste en un très léger enrobage autour de la primitive `\font` et ne convient pas du tout à LaTeX. Voici un exemple simple des problèmes que cela pose :

```latex
% !TEX noedit
\documentclass[10pt]{article}
\begin{document}
\newfont{\myfont}{cmr17 scaled 2000}
\myfont
\LaTeX
\end{document}
```

```latex
\documentclass[10pt]{article}
\pagestyle{empty}
\begin{document}
\newfont{\myfont}{cmr17 scaled 2000}
\myfont
\LaTeX
\end{document}
```

Ici, le « A » de `\LaTeX` disparaît à peu près : LaTeX choisit la taille de ce caractère selon *son* idée de la taille de la police (10pt), mais le positionne selon les dimensions de `\myfont`, soit plus de trois fois cette taille.

Et voici un autre exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[OT1]{fontenc}  % Historiquement, cet exemple fonctionnait sans cette ligne (qui force le trait).
\begin{document}
\newfont{\myfont}{ecrm1000}
{\myfont Voil\\a un exemple !}
\end{document}
``

```latex
\documentclass{article}
\usepackage[OT1]{fontenc}
\pagestyle{empty}
\begin{document}
\newfont{\myfont}{ecrm1000}
{\myfont Voil\`a un exemple !}
\end{document}
```

Cet exemple positionne un « guillemet-virgule double inférieur » (ou guillemet bas allemand) à la place de l'accent grave, sous le « a ». Cela se produit parce que `ecrm1000` est dans un {doc}`encodage </2_programmation/encodage/notion_d_encodage>` différent de celui attendu par LaTeX. Si vous utilisez l'extension {ctanpkg}`fontenc` (ici avec son option `T1` au lieu de `OT1`) pour sélectionner les polices *EC*, tous ces problèmes d'encodage fastidieux sont résolus pour vous, dans les coulisses.

Il reste cependant une circonstance où vous serez tenté d'utiliser `\newfont` : pour obtenir une taille de police qui ne rentre pas dans les tailles standard définies par Knuth, LaTeX (par défaut) ne vous permettant pas de sortir de ce cadre. Ne désespérez pas : la question « {doc}`Comment afficher des fontes de taille arbitraire ? </5_fichiers/fontes/tailles_de_fontes_arbitraires>` » vous aidera à éviter cette technique.

______________________________________________________________________

*Source :* {faquk}`What's wrong with \\newfont`? <FAQ-newfontstar>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,police,fonte,newfont
```

