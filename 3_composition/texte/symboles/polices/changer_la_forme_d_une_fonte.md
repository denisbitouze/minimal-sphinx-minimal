# Comment changer la forme d'une police ?

En LaTeX, pour un changement ponctuel de police, un certain nombre de commandes sont disponibles par défaut :

- `\textrm` pour du texte en romain ;
- `\textsf` pour du texte sans empattements (*sans serif*) ;
- `\texttt` pour du texte à chasse fixe (ou *machine à écrire*) ;
- `\textmd` pour du texte à graisse moyenne ;
- `\textbf` pour du texte gras ;
- `\textup` pour du texte droit ;
- `\textsl` pour du texte penché ;
- `\textit` pour du texte en italique ;
- `\textsc` pour du texte en petites capitales ;
- `\textnormal` pour du texte dans la police par défaut du document.

Voici un exemple :

```latex
% !TEX noedit
Un \textbf{bel} exemple.
Un \textbf{\textit{très bel}} exemple.
```

```latex
Un \textbf{bel} exemple.
Un \textbf{\textit{très bel}} exemple.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

