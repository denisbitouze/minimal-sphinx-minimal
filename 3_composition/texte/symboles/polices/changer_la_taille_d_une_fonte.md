# Comment changer la taille d'une police ?

Vous pouvez utiliser des commandes de changement de taille, dont le rôle est de modifier la taille du texte *à partir de leur exécution* :

```latex
Voici un texte d'\Large exemple. \tiny Et une suite.
```

En général, on souhaite appliquer ce changement à une petite portion du document. On applique donc ces commande à l'intérieur d'un bloc délimité par des accolades :

```latex
% !TEX noedit
Voici un texte d'{\Large exemple}. Et une suite.
```

```latex
Voici un texte d'{\Large exemple}. Et une suite.
```

Différentes commandes existent, listées ici de la plus petite taille à la plus grande :

| Commande        | Résultat                               | Commentaire                                                     |
| --------------- | -------------------------------------- | --------------------------------------------------------------- |
| `\tiny`         | {raw-latex}`\tiny Lorem ipsum`         |                                                                 |
| `\scriptsize`   | {raw-latex}`\scriptsize Lorem ipsum`   |                                                                 |
| `\footnotesize` | {raw-latex}`\footnotesize Lorem ipsum` | Son nom vient de ce qu'elle sert pour les notes de bas de page. |
| `\small`        | {raw-latex}`\small Lorem ipsum`        |                                                                 |
| `\normalsize`   | {raw-latex}`\normalsize Lorem ipsum`   | C'est la taille par défaut de votre texte.                      |
| `\large`        | {raw-latex}`\large Lorem ipsum`        |                                                                 |
| `\Large`        | {raw-latex}`\Large Lorem ipsum`        |                                                                 |
| `\LARGE`        | {raw-latex}`\LARGE Lorem ipsum`        |                                                                 |
| `\huge`         | {raw-latex}`\huge Lorem ipsum`         |                                                                 |
| `\Huge`         | {raw-latex}`\Huge Lorem ipsum`         |                                                                 |

Ces commandes sont prédéfinies en fonction de la classe de votre document.

:::{tip}
Les commandes de changement de taille du texte s'occupent automatiquement d'adapter la hauteur de l'interligne.

Mais c'est la taille courante **à la fin du paragraphe** qui détermine l'interligne du paragraphe. Il est donc important que la fin de paragraphe (ligne vide ou commande `\par`) soit **dans** le bloc dont la taille de texte est changée :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Ceci est un paragraphe de texte de taille normale,
comme vous pouvez le voir.
Son interligne est également normal.

\bigskip

{\scriptsize
Ceci est un paragraphe de texte de taille réduite,
approchez-vous un peu pour le lire.
Mais son interligne reste normal.}

\bigskip

{\scriptsize
Ceci est un paragraphe de texte de taille réduite,
approchez-vous un peu pour le lire.
Ici, son interligne est adapté au texte.\par}

\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Ceci est un paragraphe de texte de taille normale,
comme vous pouvez le voir.
Son interligne est également normal.

\bigskip

{\scriptsize
Ceci est un paragraphe de texte de taille réduite,
approchez-vous un peu pour le lire.
Mais son interligne reste normal.}

\bigskip

{\scriptsize
Ceci est un paragraphe de texte de taille réduite,
approchez-vous un peu pour le lire.
Ici, son interligne est adapté au texte.\par}

\end{document}
```
:::

## Si ces options ne vous suffisent pas...

Pour disposer de quelques tailles en plus, utilisez l'extension {ctanpkg}`moresize`. Elle vous fournira les commandes :

- `\HUGE`, encore plus grand que `\Huge`,
- `\ssmall`, intermédiaire entre `\tiny` et `\scriptsize`.

Si vous voulez contrôler précisément les tailles (en points) de vos caractères, vous pouvez utiliser la commande `\fontsize` détaillée à la question « [Pourquoi la commande « \\linespread » ne fonctionne pas ?](/3_composition/texte/paragraphes/pourquoi_linespread_ne_fonctionne_pas) ».

Enfin, l'extension {ctanpkg}`fontsize` vous ouvre la possibilité de définir très précisément la taille de base de votre document, puis vous fournit 4 fois plus de macros que celles de LaTeX pour changer la taille de caractère en cours de document. {texdoc}`Sa documentation <fontsize>` est très complète, avec des exemples.

## Pourquoi ne pas utiliser « \\scalebox » ou « \\resizebox » pour changer la taille du texte ?

- Si vous souhaitez obtenir un texte de hauteur parfaitement déterminée, vous pouvez imaginer utiliser les commandes `\scalebox` ou `\resizebox`, fournies par l'extension {ctanpkg}`graphicx`, qui modifient la taille de leur contenu.

Techniquement, ça fonctionne :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{graphicx}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\resizebox{!}{4ex}{Grand texte !}
\end{document}
```

Mais si vous mélangez différentes tailles de texte obtenues de cette façon, vous vous apercevrez que le rendu est étrange. Ici, le texte semble être en gras :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{graphicx}
  \usepackage{lmodern}
  \pagestyle{empty}

\newlength{\hauteur}
\settoheight{\hauteur}{\Huge Grand texte.}

\begin{document}
Ceci est du texte de taille normale.

\resizebox{!}{\hauteur}{Grand texte.}

À nouveau de taille normale.

\bigskip

Ceci est du texte de taille normale.

{\Huge Grand texte.}

À nouveau de taille normale.
\end{document}
```

En fait, les commandes de changement de taille ne modifient pas la taille de façon strictement géométrique. L'épaisseur des traits est également modifiée pour que les petites tailles de texte ne semblent pas dessinées avec un trait trop fin (et inversement, que les grandes tailles de texte ne semblent pas être en gras, comme ici).

Si on remet toutes les tailles de texte à la même hauteur (ici `4mm`), on peut oberver que le dessin des lettres est légèrement différent d'une taille à l'autre :

```latex
\documentclass{article}
  \usepackage{graphicx}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{ll}
\texttt{\textbackslash tiny}         & \resizebox{!}{4mm}{\tiny Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash scriptsize}   & \resizebox{!}{4mm}{\scriptsize Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash footnotesize} & \resizebox{!}{4mm}{\footnotesize Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash small}        & \resizebox{!}{4mm}{\small Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash normalsize}   & \resizebox{!}{4mm}{\normalsize Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash large}        & \resizebox{!}{4mm}{\large Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash Large}        & \resizebox{!}{4mm}{\Large Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash LARGE}        & \resizebox{!}{4mm}{\LARGE Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash huge}         & \resizebox{!}{4mm}{\huge Peux-tu m'envoyer du whisky?} \\
\texttt{\textbackslash Huge}         & \resizebox{!}{4mm}{\Huge Peux-tu m'envoyer du whisky?} \\
\end{tabular}
\end{document}
```

Ceci contribue à la qualité esthétique du document final : si dans la page, vous avez un paragraphe composé en caractères plus petits (par exemple une citation), ce paragraphe ne paraîtra pas plus clair ou plus foncé que le reste de la page (ces considérations rejoignent la notion de [gris typographique](https://fr.wikipedia.org/wiki/Gris_typographique)).

```{eval-rst}
.. meta::
   :keywords: LaTeX,taille des caractères,agrandir la police,réduire la police
```

