# Comment réaliser des changements de taille de fontes relatifs ?

## Avec l'extension « relsize »

L'extension {ctanpkg}`relsize` met à disposition une commande `\relsize{⟨n⟩}` permet d'augmenter (`⟨n⟩` positif) ou de diminuer (`⟨n⟩` négatif) la taille de la fonte par rapport à la taille courante.

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{relsize}
  \pagestyle{empty}
\begin{document}
Un \relsize{1}texte \relsize{2}de \relsize{3}%
toutes \relsize{-1}les \relsize{-4}tailles.
\end{document}
```

Quelques autres commandes sont définies :

- `\smaller` est équivalent à `\relsize{-1}` ;
- `\larger` est équivalent à `\relsize{1}` ;
- `\textsmaller{texte}` agit sur le seul *texte* ;
- `\textlarger{texte}`agit sur le seul *texte* ;
- `\mathsmaller{A}` ;
- `\mathlarger{B}`.

## Avec l'extension « scalefnt »

L'extension {ctanpkg}`scalefnt` de David Carlisle permet d'augmenter ou de diminuer, suivant un *facteur* de proportion, la taille de la police courante avec la commande `\scalefont{⟨facteur⟩}`.

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{scalefnt}
  \pagestyle{empty}
\begin{document}
La taille initiale,
\scalefont{2}le double la taille courante,
\scalefont{.75}les trois quarts la taille
courante (plus grande donc que la taille
initiale).
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tailles de polices,agrandir la police,réduire la police,taille relative
```

