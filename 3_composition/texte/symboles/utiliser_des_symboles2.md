# Comment éviter les conflits de noms de commande de symbole ?

Les polices de symboles sont souvent fournies avec une extension qui définit des commandes pour chaque symbole de la police. Bien que cela soit pratique, cela peut entraîner des difficultés, en particulier avec des conflits de noms lorsque vous chargez des extensions qui couvrent des polices qui dupliquent des symboles ou qui définissent de grands nombres de symboles. Voici quelques exemples de méthode pour éviter ce type de situation.

## Une solution générale

Cette solution évitant les conflits de noms est présentée dans la question « {doc}`Que signifie l'erreur « command already defined » ? </2_programmation/erreurs/c/command_already_defined>` ».

## La mécanique de l'extension pifont

L'extension {ctanpkg}`pifont`, initialement conçue pour utiliser la police Adobe *Zapf Dingbats*, évite ce problème. Elle vous oblige pour cela à connaître la position dans la police de tout symbole que vous souhaitez utiliser (la documentation fournit des tableaux de polices). La commande de base est ainsi `\ding{`*numéro de symbole*`}`. Cette extension fournit par ailleurs d'autres commandes pour des utilisations plus sophistiquées et vous permet également de sélectionner d'autres polices, pour un usage similaire.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{pifont}

\begin{document}
\ding{252} Voici un exemple de symbole : \ding{166}.
\end{document}
```

```latex
\documentclass{article}
\usepackage{pifont}
\pagestyle{empty}
\begin{document}
\ding{252} Voici un exemple de symbole : \ding{166}.
\end{document}
```

## La mécanique de l'extension yagusylo

L'extension {ctanpkg}`yagusylo` se décrit comme « une version étendue de {ctanpkg}`pifont` en technicolor ». Elle fournit toutes les fonctionnalités de {ctanpkg}`pifont`, mais vous permet en plus de créer vos propres noms mnémotechniques pour les symboles. Ainsi, alors que vous pouvez utiliser la commande `\yagding[`*famille*`]{`*numéro de symbole*`}[`*couleur*`]`, vous pouvez également définir les noms de symboles avec la commande `\defdingname`, puis les utiliser avec la commande `\yagding*{`*nom de symbole*`}` (le nom défini porte la famille de polices et la couleur spécifiées dans les arguments de `\defdingname`).

```latex
% !TEX noedit
\documentclass{article}
\usepackage{yagusylo}

\begin{document}
Un fleuron appelé par une commande détaillée : \yagding[fourier]{109}[black].

\defdingname[fourier][global]{109}{fleuron}[black]
Un fleuron appelé par une nouvelle commande personnelle : \yagding*{fleuron}.
\end{document}
```

```latex
\documentclass{article}
\usepackage{yagusylo}
\pagestyle{empty}
\begin{document}
Un fleuron appelé par une commande détaillée : \yagding[fourier]{109}[black].

\defdingname[fourier][global]{109}{fleuron}[black]
Un fleuron appelé par une nouvelle commande personnelle : \yagding*{fleuron}.
\end{document}
```

L'extension {ctanpkg}`yagusylo` peut être un peu compliquée mais sa documentation est claire. C'est probablement le meilleur outil à utiliser pour sélectionner et choisir des symboles parmi plusieurs familles de polices.

______________________________________________________________________

*Source :* {faquk}`Using symbols <FAQ-usesymb>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```

