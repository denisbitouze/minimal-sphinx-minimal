# Comment convertir des majuscules en minuscules et inversement ?

## Avec les commandes \\uppercase et \\lowercase

TeX fournit les deux primitives suivantes pour changer la [casse](<https://fr.wikipedia.org/wiki/Casse_(typographie)>) du texte :

- `\uppercase` pour convertir un texte en majuscules ;
- `\lowercase` pour convertir un texte en minuscules.

Elles ne sont pas très utilisées car sources de confusion :

- elles ne développent pas les commandes du texte donné en paramètre. Ainsi, le résultat de `\uppercase{abc}` est `ABC`, mais celui de `\uppercase{\abc}` est `\abc`, quelle que soit la signification de `\abc` ;
- elles appliquent un simple tableau d'équivalences entre les caractères majuscules et minuscules, ce qui fait, par exemple, qu'elles ne tiennent pas compte du sens littéral ou mathématique du texte. Un appel comme `\uppercase{Soit $y = f(x)$}` donnera `SOIT $Y = F(X)`, ce qui n'est probablement pas souhaité ;
- et elles ne traitent pas très bien les caractères non américains, par exemple `\uppercase{\ae}` est identique à `\ae` alors qu'il semblait logique d'obtenir `\AE`.

## Avec les commandes \\MakeUppercase et \\MakeLowercase

LaTeX fournit les commandes `\MakeUppercase` et `\MakeLowercase` qui corrigent le problème des caractères non américains. Elles sont d'ailleurs utilisées dans les classes standard pour produire des titres en majuscules pour les chapitres et les sections.

Malheureusement, `\MakeUppercase` et `\MakeLowercase` ne résolvent pas les autres problèmes vus avec `\uppercase` et `\lowercase`. Ainsi, par exemple, un titre où serait retenu le format majuscule alors qu'il contiendrait en argument `\begin{tabular}` ... `\end{tabular}` produirait un titre contenant la commande `\begin{TABULAR}` (probablement non définie et générant donc une erreur). La solution la plus simple à ce problème consiste à utiliser une commande définie par l'utilisateur, par exemple :

```latex
% !TEX noedit
\newcommand{\matable}{%
  \begin{tabular}
  ...
  \end{tabular}%
}
\section{Un titre avec un tableau \protect\matable{}}
```

Notez que la commande `\matable` doit être {doc}`protégée </2_programmation/syntaxe/c_est_quoi_la_protection>`, sinon elle sera développée et son résultat mis en majuscules. Vous pouvez obtenir le même résultat en le déclarant avec `\DeclareRobustCommand`, auquel cas `\protect` ne sera pas nécessaire.

## Avec l'extension textcase

L'extension {ctanpkg}`textcase`, de David Carlisle, résout un grand nombre de ces problèmes de manière transparente. Elle définit les commandes `\MakeTextUppercase` et `\MakeTextLowercase` qui reprennent les fonctionnalités de `\MakeUppercase` et `\MakeLowercase` mais sans les problèmes mentionnés ci-dessus. L'option d'extension `overload` permet de redéfinir les commandes LaTeX (*mais pas* les primitives TeX `\uppercase` et `\lowercase`). Ceci évitera les problématiques de titres indiquées ci-dessus.

______________________________________________________________________

*Source :* {faquk}`Case-changing oddities <FAQ-casechange>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,majuscules,minuscules,changement de casse
```

