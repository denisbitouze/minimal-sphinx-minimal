# Quel est le format de papier par défaut de LaTeX ?

Du fait de son origine américaine, le [format de papier](https://fr.wikipedia.org/wiki/Format_de_papier) par défaut sur les classes standard de LaTeX est le format *Letter*. Il correspond à un papier de dimension 8,5 in × 11 in, soit 215,9 mm × 279,4 mm. Ce format correspond aux déclarations suivantes (qui sont celles de l'option de classe `letterpaper`) :

```latex
% !TEX noedit
\setlength\paperheight{11in}
\setlength\paperwidth{8.5in}
```

Par comparaison, le format A4, standard pour un utilisateur européen, correspond à un papier de dimension 210 mm × 297 mm. Il peut être obtenu avec l'option de classe `a4paper` :

```latex
% !TEX noedit
\setlength\paperheight{297mm}
\setlength\paperwidth{210mm}
```

Dans la mesure où la différence de largeur est faible (5,9 mm), les coupures de lignes se trouvent souvent aux mêmes endroits dans les deux formats. En revanche, chaque page contient bien quelques lignes de plus avec le format A4.

```{eval-rst}
.. meta::
   :keywords: LaTeX,format de papier
```

