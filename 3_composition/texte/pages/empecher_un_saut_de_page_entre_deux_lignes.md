# Comment éviter un saut de page entre des lignes ?

En temps normal, un bloc de texte doit être conservé sur une même page. Mais il s'avère étonnamment difficile de réussir cela.

## De manière automatique

### Avec des commandes de base

LaTeX fournit un environnement `samepage` qui propose de traiter ce sujet. Il procède en fixant des pénalités infinies pour toutes sortes de situations de saut de page (pensez d'ailleurs à bien terminer vos paragraphes dans l'environnement, comme indiqué dans la question « {doc}`Pourquoi mon paramètre de paragraphe est-il ignoré ? </3_composition/texte/paragraphes/parametres_non_appliques_au_paragraphe>` »). Mais cet environnement a plusieurs limites :

- `samepage` ne gère pas toutes les situations ;
- si les éléments que vous essayez de garder ensemble insèrent leurs propres recommandations de saut de page, `samepage` n'a aucun pouvoir sur eux, exception faite des suggestions des éléments de liste pour insérer des sauts de page entre eux;
- si `samepage` *fonctionne*, il est capable de laisser des éléments déborder en bas de la page.

Une autre astuce pratique consiste à placer tous les éléments pertinents dans une commande `\parbox` (ou un environnement `minipage` s'il contient des éléments qui ne peuvent être utilisés dans l'argument de `\parbox`). La boîte résultante *ne se scindera pas* entre les pages... mais cela ne veut pas dire qu'elle fera proprement ce que vous souhaitez : une nouvelle fois, la boîte peut largement déborder en bas de la page.

Pourquoi aucune de ces choses évidentes ne fonctionne ? Parce que TeX ne peut pas vraiment faire la distinction entre des choses qu'il définit comme « infiniment mauvaises ». L'environnement `samepage` rend tout point de saut de page « infiniment mauvais » et les boîtes n'offrent aucune option de saut de page. Toutefois, si l'alternative est de laisser quelques centimètres « infiniment mauvais » de page blanche en bas de page, TeX ne fait rien.

Ce problème se pose d'ailleurs même si vous avez utilisé la commande `\raggedbottom` : TeX ne remarque pas ce paramètre tant qu'il ne délivre pas la page. Une approche consiste alors à définir :

```latex
% !TEX noedit
\raggedbottom
\addtolength{\topskip}{0pt plus 10pt}
```

Le `10pt` indique à la routine de sortie que la colonne de texte est extensible. Cela amènera TeX à être plus tolérant sur la nécessité de s'étirer lors de la construction de la page. Si vous recourrez à cette mesure temporairement dans votre document, il vous faudra annuler la modification de `\topskip` avec :

```latex
% !TEX noedit
\addtolength{\topskip}{0pt plus-10pt}
```

De même, il fera réinitialiser `\flushbottom`. Notez que le `10pt` n'impacte jamais réellement la page car il est neutralisé par l'extensibilité introduite par `\raggedbottom` lorsque la page est finalisée. Cependant, il pourrait bien avoir un effet si `\flushbottom` est actif.

### Avec l'extension needspace ou la classe memoir

Une alternative (suggérée par Donald Knuth dans le TeX​book) est donnée par l'extension {ctanpkg}`needspace` ou la classe {ctanpkg}`memoir`. Elles définissent toutes deux une commande `\needspace` dont l'argument lui indique quel espace est nécessaire pour garder les éléments groupés. Si l'espace n'est pas disponible, la page actuelle est effacée et les élements qui doivent être conservés ensemble sont insérés dans une nouvelle page. Par exemple, si 4 lignes de texte doivent être conservées ensemble, la séquence

```latex
% !TEX noedit
\par
\needspace{4\baselineskip}
% Les éléments qui doivent rester ensemble
⟨le texte d'une à quatre lignes⟩
% Puis la suite où vous êtes moins regardant
```

### Avec la commande \\filbreak

Une autre astuce donnée par Knuth peut servir si vous avez une séquence de petits blocs de texte qui doivent, individuellement, être conservés sur leur propre page. Insérez alors la commande `\filbreak` avant chaque petit bloc. La technique peut être utilisée dans le cas de séquences de sections LaTeX, en incorporant `\filbreak` dans la définition d'une commande (sur le principe vu dans « {doc}`Comment modifier une commande existante ? </2_programmation/macros/patcher_une_commande_existante>` »). Un patch simple et efficace peut être :

```latex
% !TEX noedit
\let\oldsubsubsection=\subsubsection
\renewcommand{\subsubsection}{%
  \filbreak
  \oldsubsubsection
}
```

Bien que l'astuce fonctionne pour des blocs de texte consécutifs, il est assez délicat de sortir d'une telle séquence à moins qu'elle ne soit interrompue par un saut de page forcé (comme `\clearpage`, qui peut être introduit par une commande `\chapter` ou la fin du document). Si la séquence n'est pas interrompue, le dernier bloc est susceptible d'être placé sur une nouvelle page, qu'il en ait besoin ou non.

## De manière manuelle

Si vous êtes prêt à accepter que tout ne peut pas être accompli de manière totalement automatique, la voie à suivre est de composer le document et de vérifier les éléments susceptibles de créer des problèmes. Dans un tel scénario, vous pouvez décider, au cas par cas, comment traiter les problèmes au dernier stade de la relecture. Là, vous pouvez modifier manuellement les sauts de page :

- en utilisant `\pagebreak` ou `\clearpage` pour ajouter un saut de page ;
- en plaçant une commande `\nopagebreak` pour supprimer les coupures malheureuses.

Vous pouvez également procéder à de petits ajustements de la géométrie de la page, en utilisant `\enlargethispage`. Supposons que vous ayez une ligne ou deux qui s'égarent, vous pourrez alors indiquer :

```latex
% !TEX noedit
\enlargethispage{2\baselineskip}
```

Deux lignes sont ajoutées ainsi à la page que vous composez. Et que cette technique puisse sembler horrible ou acceptable, elle reste un élément utile dans votre arsenal.

Notez que les commandes `\pagebreak` et `\nopagebreak` prennent un argument numérique facultatif pour ajuster la façon dont la commande doit être interprétée. Ainsi avec `\pagebreak[0]`, la commande indique qu'un saut de page peut valoir la peine d'être fait alors que `\pagebreak[4]` demande le saut de page. De même `\nopagebreak[0]` fait une suggestion, tandis que `\nopagebreak[4]` est une demande. Dans les deux commandes, la valeur par défaut de l'argument facultatif est 4.

______________________________________________________________________

*Source :* {faquk}`Preventing page breaks between lines <FAQ-nopagebrk>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,saut de page,samepage,\\parbox,\\pagebreak,\\raggedbottom,needspace
```

