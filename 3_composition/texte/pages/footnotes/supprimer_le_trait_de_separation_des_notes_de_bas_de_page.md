# Comment supprimer le trait de séparation des notes de bas de page ?

L'extension {ctanpkg}`footmisc`, avec l'option `norule`, se charge de supprimer ce trait de séparation (dénommé « filet de séparation »). En voici un exemple :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[norule]{footmisc}

\begin{document}

Donald Knuth a déclaré qu'il ne développe plus \TeX ; il ne traite désormais
plus que la correction des erreurs qui lui sont remontées\footnote{Et ces
bugs sont rares !}.

\end{document}
```

Comparez :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={7cm,3cm}]{geometry}

%\usepackage[norule]{footmisc}

\begin{document}

Donald Knuth a déclaré qu'il ne développe plus \TeX ; il ne traite désormais
plus que la correction des erreurs qui lui sont remontées\footnote{Et ces
bugs sont rares !}.

\end{document}
```

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={7cm,3cm}]{geometry}

\usepackage[norule]{footmisc}

\begin{document}

Donald Knuth a déclaré qu'il ne développe plus \TeX ; il ne traite désormais
plus que la correction des erreurs qui lui sont remontées\footnote{Et ces
bugs sont rares !}.

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,notes de bas de page,trait de séparation,filet,ligne de séparation,footnote
```

