# Comment obtenir une note de bas de page ?

La commande `\footnote` permet d'obtenir une note de bas de page de manière très simple. Elle prend un argument obligatoire, qu'elle écrira en bas de la page. Dans le texte, elle ajoute simplement un *appel de note* ou renvoi, sous forme de numéro (ou de symbole). Ce numéro est obtenu par un compteur, appelé `footnote` également. Ce numéro peut également être « forcé » en utilisant l'argument optionnel de la commande `\footnote`.

Voici un exemple de code l'utilisant :

```latex
\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\begin{document}
Voici un exemple\footnote{Cet exemple est très largement perfectible.} de note de bas de page.
\end{document}
```

La commande `\footnote` se divise en fait en deux commandes :

- la commande `\footnotemark`, qui se charge de placer le renvoi avec son numéro dans le texte ;
- et la commande `\footnotetext` qui place le texte en bas de page.

Il est parfois nécessaire d'utiliser ces deux commandes à la place de `\footnote`, comme le montre par exemple la question portant sur {doc}`les notes de bas de page dans les tableaux </3_composition/tableaux/notes_de_bas_de_tableau>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page
```

