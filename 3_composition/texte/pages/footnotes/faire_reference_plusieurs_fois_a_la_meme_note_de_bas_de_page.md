# Comment faire référence plusieurs fois à la même note de bas de page ?

Cette question est traitée dans une autre section de cette FAQ : {doc}`celle portant sur les renvois </3_composition/texte/renvois/faire_reference_plusieurs_fois_a_une_note_de_bas_de_page>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,référence
```

