# Comment obtenir plus d'une plage de notes de bas de page ?

Pouvoir disposer de plusieurs plages de notes en bas de page est un besoin courant dans les éditions critiques (ou éditions savantes) pour mettre en forme [l'apparat critique](https://fr.wikipedia.org/wiki/Apparat_critique). Ce besoin peut parfois survenir aussi dans d'autres domaines.

## Avec l'extension « reledmac »

{octicon}`alert;1em;sd-text-warning` L'extension la plus actuelle pour l'édition critique est {ctanpkg}`reledmac`, de Maïeul Rouquette. Son [développement](https://github.com/maieul/ledmac/) est très actif.

Historiquement, l'extension {ctanpkg}`EDMAC` a d'abord été développée par Dominik Wujastyk, pour TeX seulement. Son portage sous LaTeX a été fait par Peter R. Wilson et a pris le nom de {ctanpkg}`ledmac` (*LaTeX-{ctanpkg}`EDMAC`*). Une version étendue a été développée par Maïeul Rouquette, et s'est appelée {ctanpkg}`eledmac` (*extended {ctanpkg}`ledmac`*). Enfin, la réimplémentation actuelle, {ctanpkg}`reledmac`, rend obsolète toutes les précédentes.

:::{note}
{ctanpkg}`reledmac` a une syntaxe différente des implémentations plus anciennes, mais sa documentation précise comment passer de {ctanpkg}`ledmac` et {ctanpkg}`eledmac` à l'extension actuelle.
:::

## Avec l'extension « manyfoot » et les extensions liées

L'extension {ctanpkg}`manyfoot` propose aussi des plages de notes de bas de page multiples aux utilisateurs de LaTeX. Elle fournit d'ailleurs un large éventail d'options de présentation.

Deux autres extensions traitant de l'édition critique se basent sur {ctanpkg}`manyfoot` :

- {ctanpkg}`ednotes` qui inclut {ctanpkg}`manyfoot` comme mécanisme pour mettre en place plusieurs plages de notes de bas de page ;
- {ctanpkg}`bigfoot` utilise tout autant {ctanpkg}`manyfoot` pour mettre en place un système élaboré pour les notes de bas de page.

______________________________________________________________________

*Source :* {faquk}`More than one sequence of footnotes <FAQ-multfoot>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,apparat critique,noter des variantes du texte en notes de bas de page,éditiion critique,historique d'un texte
```

