# Comment mettre les notes de bas de page en fin de document ?

L'extension {ctanpkg}`endnotes` s'occupe ici de tout.

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page
```

