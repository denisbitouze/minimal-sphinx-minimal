# Comment modifier l'espace inter-colonnes ?

Il suffit de modifier la variable `\columnsep` :

```latex
% !TEX noedit
\addtolength{\columnsep}{5mm}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

