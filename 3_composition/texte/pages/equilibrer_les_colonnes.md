# Comment équilibrer les colonnes de texte en fin de document ?

L'option `twocolumn` des classes standard oblige LaTeX à présenter le texte d'un document sur deux colonnes. Cependant, la dernière page du document se termine généralement par des colonnes de longueurs différentes. Et rares sont ceux qui apprécient des colonnes déséquilibrées.

L'utilisateur doit être conscient que chacune des extensions évoquée par la suite pour résoudre ce problème est susceptible d'être perturbée en présence de flottants : si des problèmes surviennent, un réglage manuel des flottants dans le document pourra être nécessaire. C'est cette difficulté qui a conduit l'auteur de {ctanpkg}`multicol`, présenté ci-après, à supprimer les flottants à une seule colonne.

## Avec des commandes de bases

Une solution manuelle consiste à insérer la commande `\pagebreak` à l'endroit approprié sur la dernière page. Si cette solution produit le bon effet, elle plait rarement, surtout si la dernière page est composée de texte généré automatiquement (par exemple, la bibliographie ou l'index) car il est alors difficile d'insérer la commande.

## Avec l'extension « multicol »

Une des solutions au problème revient à utiliser l'extension {ctanpkg}`multicol` au lieu de l'option `twocolumn`. En effet, {ctanpkg}`multicol` équilibre par défaut les colonnes sur la dernière page. Cependant, ce choix a un coût : la routine de sortie particulière de {ctanpkg}`multicol` interdit l'utilisation de flottants au sein d'une colonne, bien qu'elle autorise toujours les flottants sur la pleine largeur de la page (par exemple, avec l'environnement `figure*`).

## Avec l'extension « flushend »

L'extension {ctanpkg}`flushend` offre une solution à ce problème avec un code un peu sulfureux car modifiant l'une des parties les plus complexes du noyau LaTeX sans déployer aucune des règles de prudence décrites pour {doc}`corriger des commandes </2_programmation/macros/patcher_une_commande_existante>`. L'extension ne change le comportement de LaTeX qu'à la fin du document où une commande dédiée permet d'équilibrer les colonnes finales. En complément, d'autres extensions liées à {ctanpkg}`flushend` fournissent des moyens pour insérer des éléments sur la pleine largeur de la page dans des documents à deux colonnes.

## Avec l'extension « balance »

L'extension {ctanpkg}`balance` modifie également la routine de sortie (de manière sans doute plus prudente que {ctanpkg}`flushend`).

## Avec l'extension « pbalance »

L'extension {ctanpkg}`pbalance` (*P* comme « poor man's balance ») essaie de résoudre les limitations des précédentes solutions concernant les flottants. Elle devrait être « sûre » (les modifications de la routine de sortie sont minimes) et fonctionner sans intervention de l'utilisateur. Cependant, elle a son propre lot d'avertissements (consultez {texdoc}`la documentation <pbalance>`). Les plus importants sont

- C'est encore une extension récente (2020) et, par conséquent, elle a été très peu testée;
- Il exige des passes LaTeX supplémentaires (jusqu'à trois dans certains cas).

______________________________________________________________________

*Source :* {faquk}`Balancing columns at the end of a document <FAQ-balance>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,équilibrer les colonnes,composition en colonnes,mise en page des colonnes
```

