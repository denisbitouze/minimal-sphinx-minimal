# Comment modifier un changement de page ?

- `\newpage` impose un changement brutal de page. Ce n'est généralement pas ce que vous voulez. Regardez les commandes suivantes.
- `\pagebreak[⟨n⟩]` où `n` représente l'autorité avec laquelle on veut changer de page. Ce paramètre est compris entre 1 et 4 :
- `1` impose une contrainte faible (« LaTeX, si tu le souhaites, tu peux insérer un saut de page ici. ») ;
- `4` impose une contrainte sévère (« LaTeX, je t'ordonne d'insérer un saut de page ici ! »).

Cette commande respecte la justification.

- `\nopagebreak[⟨n⟩]` idem pour empêcher une coupure de page.
- `\clearpage` termine la page en cours, tout comme `\newpage`, mais fait en sorte de placer tous les flottants qui seraient encore en attente, avant de commencer une nouvelle page. Cette commande est utilisée, par exemple, en fin de chapitre.
- `\cleardoublepage` fait la même chose mais force en plus un redémarrage sur une page impaire, en laissant éventuellement une page blanche. Cela permet de reprendre le texte sur une page de droite, ce qui est encore plus utilisé en fin de chapitre dans les belles éditions.
- `\enlargethispage{⟨longueur⟩}` impose à LaTeX de comprimer (⟨longueur⟩ négative) ou d'étirer (⟨longueur⟩ positive) la hauteur du contenu de la page. Ceci peut être utile pour éviter que la page suivante ne contienne trop peu de texte.

Par exemple :

```latex
% !TEX noedit
% ajoute un cm
\enlargethispage{1cm}
% supprime deux lignes dans la page
\enlargethispage{-2\baselineskip}
```

:::{note}
`\enlargethispage*{⟨lgr⟩}` donne une autorité plus grande à la commande `\enlargethispage` en l'obligeant à agir sur les espacements élastiques verticaux contenus dans la page courante.
:::

- Le fichier `block.sty`, de Jean-Pierre F. Drucbert, bricolé à partir de macros de diverses origines (dont Donald Arseneau), permet certains contrôles.

Ce n'est pas parfait, mais cela a rendu service par le passé.

```latex
\ifx\endBlock\undefined
\def\block{\begingroup%
\def\endblock{\egroup\endgroup}%
\vbox\bgroup}%
\long\def\Block{\begingroup%
\def\endBlock{\unskip\egroup\endgroup}%
\pagebreak[2]\vspace*{\parskip}\vbox\bgroup%
\par\noindent\ignorespaces}
\long\def\IBlock{\begingroup%
\def\endIBlock{\unskip\egroup\endgroup}%
\pagebreak[2]\vspace*{\parskip}\vbox\bgroup\par\ignorespaces}
\def\need#1{\ifhmode\unskip\par\fi \penalty-100 \begingroup
% preserve \dimen@, \dimen@i
   \ifdim\prevdepth>\maxdepth \dimen@i\maxdepth
      \else \dimen@i\prevdepth\fi
   \kern-\dimen@i
   \dimen@\pagegoal \advance\dimen@-\pagetotal % space left
   \ifdim #1>\dimen@
        \vfill\eject\typeout{WARNING- EJECT BY NEED}
   \fi
   \kern\dimen@i
   \endgroup}
\def\lneed#1{\need{#1\baselineskip}}
% \begin{block} ... \end{block} delimite un bloc qui restera,
%                               si possible, sur une seule page.
\long\def\TBlock{\begingroup%
\def\endTBlock{\unskip\egroup\endgroup}%
\pagebreak[2]\vspace*{\parskip}\vtop\bgroup%
\par\noindent\ignorespaces}
\else
\typeout{block.sty already loaded}
\fi
```

Vous pouvez protéger une zone contre la rupture de page. Il suffit pour cela de la placer dans un environnement `Block` ou dans un environnement `IBlock`. Dans le premier cas (`Block`), le premier paragraphe se trouvant dans la zone n'aura pas de retrait d'alinéa en première ligne, mais dans le second cas (`IBlock`), tous les paragraphes (même le premier) auront un retrait d'alinéa. Ceci est utile en particulier pour éviter de séparer un texte et un exemple qui l'accompagne. Les blocs ainsi protégés doivent, bien entendu, rester assez petits. La syntaxe est (vous pouvez remplacer `Block` par `IBlock`) :

```latex
% !TEX noedit
\begin{Block}
...
zone protégée
...
\end{Block}
```

Cette méthode, très simple, a l'inconvénient de ne pas pouvoir s'appliquer lorsque la zone à protéger doit contenir une commande de sectionnement (c'est-à-dire du même type que `section`), une note en bas de page, une note marginale ou un corps mobile (figure ou table). Dans ce cas, il faudra utiliser une des commandes du paragraphe suivant.

Vous pouvez aussi demander de changer de page (ou de colonne, si votre document est sur deux colonnes) s'il ne reste pas verticalement assez de place sur la page. Deux commandes de réservation verticale sont disponibles : `\need{⟨dimension⟩}`, dont le paramètre est une longueur, et `\lneed{⟨nombre⟩}`, dont le paramètre est le nombre de lignes équivalent à l'espace vertical demandé (avec cette forme le paramètre est plus facile à estimer).

- Le package {ctanpkg}`needspace` permet de réserver de l'espace en bas de page. S'il n'y a pas assez de place dans le restant de la page, une nouvelle est commencée. Par exemple :

```latex
% !TEX noedit
... paragraphe précédent.\par
\needspace{5\baselineskip}
% les 5 lignes suivantes seront sur une
% même page / colonne
Les cinq lignes suivantes vont parler de...
```

- Pour éviter les coupures de page (ou les coupures de colonnes avec le package {ctanpkg}`multicol`) à l'intérieur des `\item` dans une description (et d'ailleurs dans toute liste), solution simple sortie du TeX​book :

```latex
% !TEX noedit
\begin{description}\interlinepenalty 10000
```

- Lorsque l'on veut éviter qu'une liste à puces ou un paragraphe ne soit coupé par un changement de page, il suffit d'utiliser la commande `\samepage`.

______________________________________________________________________

*Source :*

- [Is it wrong to use \\clearpage instead of \\newpage?](https://tex.stackexchange.com/questions/45609/is-it-wrong-to-use-clearpage-instead-of-newpage)

```{eval-rst}
.. meta::
   :keywords: LaTeX,saut de page,nouvelle page,fin de chapitre,laisser une page blanche,sauter une page
```

