# Comment gérer des en-têtes avec des environnements verbatim multipages ?

- Pour plus d'explications sur les environnements `verbatim`, voir « {doc}`Comment écrire en mode verbatim? </4_domaines_specialises/informatique/ecrire_en_mode_verbatim>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,inclure du code dans un document,mode verbatim,\\verb,en-têtes personnalisées
```

