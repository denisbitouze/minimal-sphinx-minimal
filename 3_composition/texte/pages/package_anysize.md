# Comment utiliser « anysize » pour modifier la taille des marges ?

L'extension {ctanpkg}`anysize` est considérée comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`, et vous devriez plutôt utiliser {ctanpkg}`geometry`. C'est très facile :

```latex
% !TEX noedit
\documentclass[a4paper]{article}
  \usepackage[margin=2.5cm]{geometry}

\begin{document}
...
\end{document}
```

ou

```latex
% !TEX noedit
\documentclass[a4paper]{article}
  \usepackage[left=2.5cm,
              right=2.5cm,
              top=2.5cm,
              bottom=2.5cm
             ]{geometry}

\begin{document}
...
\end{document}
```

Vous pouvez même configurer différemment les marges sur les pages paires et impaires (en jouant sur les marges *intérieures* et *extérieures* des pages) :

```latex
% !TEX noedit
\documentclass[a4paper]{article}
  \usepackage[inner=1.0in,% Marge intérieure
              outer=0.5in,% Marge extérieure
              top=0.4in,
              bottom=0.75in,
              bindingoffset=0.2in% Marge de reliure
             ]{geometry}

\begin{document}
...
\end{document}
```

:::{tip}
Plus de détails {doc}`sur cette page </3_composition/texte/pages/configurer_la_taille_du_papier>`
:::

______________________________________________________________________

*Source :* [Why are margins unequal?](https://tex.stackexchange.com/questions/204943/why-are-margins-unequal)

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting,largeur des marges,réduire les marges,foirmat du papier
```

