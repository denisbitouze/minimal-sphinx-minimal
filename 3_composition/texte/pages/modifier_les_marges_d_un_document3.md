# Comment définir les paramètres de page manuellement ?

Si vous êtes impatient de faire les réglages par vous-même et que vous avez lu les mises en garde décrites dans la question « {doc}`Comment modifier les marges d'un document ? </3_composition/texte/pages/modifier_les_marges_d_un_document2>` », il est important que vous commenciez par vous familiariser avec les paramètres de mise en page de LaTeX dans la documentation disponible {doc}`libre </1_generalites/documentation/documents/documents_sur_latex2e>` ou {doc}`payante </1_generalites/documentation/livres/documents_sur_latex>`. Pour une bonne entrée en matière, la documentation de l'extension {ctanpkg}`layout` donne un graphique très clair sur le sujet.

LaTeX contrôle la mise en page avec un certain nombre de paramètres, qui vous permettent de modifier la distance entre les bords d'une page et les bords gauche et supérieur de votre texte, la largeur et la hauteur du texte, et le placement des autres éléments sur la page. Cependant, ils sont quelque peu complexes et il est facile de se tromper dans leurs interrelations lors de la redéfinition de la mise en page. L'extension {ctanpkg}`layout` définit une commande `\layout` qui dessine un diagramme de votre mise en page existante, avec les dimensions (mais pas leurs interrelations) affichées.

Même la modification de la hauteur et de la largeur du texte, `\textheight` et `\textwidth`, nécessite plus de soin que vous ne le pensez :

- la hauteur doit être définie pour s'adapter à un nombre entier de lignes de texte (en termes de multiples de `\baselinskip`) ;
- la largeur doit être limitée par le nombre de caractères par ligne, comme mentionné dans la question « {doc}`Comment modifier les marges d'un document ? </3_composition/texte/pages/modifier_les_marges_d_un_document2>` ».

Les marges sont contrôlées par deux paramètres : `\oddsidemargin` et `\ evensidemargin`, dont les noms proviennent de la convention selon laquelle les pages du recto, impaires (*odd*), apparaissent sur le côté droit d'une planche de deux pages et les pages du verso, paires (*even*), sur le côté gauche. Les deux paramètres se réfèrent en fait à la marge de gauche des pages concernées. Dans chaque cas, la marge de droite est déterminée implicitement, à partir de la valeur de `\textwidth` et de la largeur du papier. Dans un document en recto uniquement, correspondant à la présentation par défaut dans de nombreuses classes, telles les classes standard {ctanpkg}`article` et {ctanpkg}`report`, `\oddsidemargin` représente les deux marges.

L'« origine » (la position zéro) sur la page est à un pouce du haut du papier et à un pouce du côté gauche. Les mesures horizontales positives s'étendent ensuite en largueur sur la page et les mesures verticales positives s'étendent le long de la page. Ainsi, les paramètres `\evensidemargin`, `\oddsidemargin` et ''\\topmargin '' doivent être réglés à 1 pouce de moins que la vraie marge. Pour des marges plus proches des bords gauche et supérieur de la page que 1 pouce, les paramètres de marge doivent être définis par des valeurs négatives.

______________________________________________________________________

*Source :* {faquk}`How to set up page layout "by hand" <FAQ-marginmanual>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```

