# Quelle est la structure d'une page LaTeX ?

- La commande `\layout` du package {ctanpkg}`layout` permet de visualiser la structure d'une page et ses différents paramètres. Globalement elle est composée du corps du texte, d'un en-tête et d'un pied de page. Des marges sont également définies de chaque côté du corps du texte.

Structure d'une page LaTeX :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{layout}
\begin{document}
\layout
\end{document}
```

- Chaque classe de documents assigne des valeurs par défaut aux paramètres de mise en page. L'utilisateur peut redéfinir les valeurs de ces paramètres. Cela lui permet de créer sa propre mise en page. Cette FAQ donne un certain nombre de moyens dans ce but.

Pour les documents devant être imprimés en recto-verso, `\oddsidemargin` définit la marge gauche des pages impaires (recto), et `\evensidemargin` la marge gauche des pages paires (verso). Pour les documents simple face, la longueur `\oddsidemargin` suffit.

Les principaux paramètres d'une page sont les suivants :

- `\textheight` définit la hauteur du texte ;
- `\textwidth` définit la largeur du texte ;
- `\columnsep` définit l'espace entre les colonnes pour un document multicolonnes ;
- `\columnseprule` définit la largeur du filet qui sépare les colonnes d'un document multicolonnes (par défaut ce paramètre vaut `0pt` *i.e.* pas de filet) ;
- `\columnwidth` définit la largeur d'une colonne. Ce paramètre est calculé automatiquement par LaTeX d'après `textwidth` et `columnsep` ;
- `\linewidth` définit la longueur de la ligne courante. Ce paramètre est généralement utilisé dans des environnements qui redéfinissent les marges ;
- `\evensidemargin` définit une espace supplémentaire dans la marge gauche des pages paires des documents recto-verso ;
- `\oddsidemargin` définit cette espace pour les pages impaires d'un document recto-verso ou pour toutes les pages dans le cas d'un document recto uniquement ;
- `\footskip` définit la distance entre la dernière ligne du corps du texte et la première ligne du texte de bas de page (notes, par exemple) ;
- `\headheight` définit la hauteur de l'en-tête ;
- `\headsep` définit la distance entre la dernière ligne du texte d'en-tête et la première ligne de texte du corps du document ;
- `\topmargin` définit une espace supplémentaire au-dessus de l'en-tête ;
- `\marginparpush` définit l'espace verticale minimum entre deux notes de marge ;
- `\marginparsep` définit l'espace horizontale entre le corps du document et les notes de marge ;
- `\marginparwidth` définit la largeur des notes de marge ;
- `\paperheight` définit la hauteur du papier sur lequel le document sera imprimé ;
- `\paperwidth` définit sa largeur.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

