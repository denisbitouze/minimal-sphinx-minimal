# Comment définir les hauts et bas de page ?

:::{note}
Lorsque le haut ou bas de page défini est trop grand, on voit apparaître des messages d'erreur du style « `Overfull vbox` ». Il faut alors redimensionner la longueur correspondante.

Exemple (dans le préambule) :

```latex
% !TEX noedit
  \addtolength{\headheight}{1.5pt}
```
:::

## Avec les commandes de base

Par défaut, LaTeX offre la numérotation des pages en bas de page (style `plain`). Mais, il propose également 3 autres styles de mise en page. Il s'agit des styles :

- `empty` (hauts et bas de pages vides),
- `headings` (la numérotation des pages apparaît en haut ainsi que différentes informations suivant la classe de document), et
- `myheadings` (les commandes `\markboth` et `\markright` permettent de définir les informations qui devront apparaître dans le haut de page. `\markboth{⟨en-tête gauche⟩}{⟨en-tête droite⟩}` s'utilise pour un document recto-verso alors que `\markright{⟨en-tête⟩}` s'applique à toutes les pages d'un document en simple recto).

L'appel d'un style pour tout le document se fait avec la commande `\pagestyle{⟨style⟩}`. La commande `\thispagestyle{⟨style⟩}` permet d'appeler un style sur une page particulière.

:::{note}
Malgré une déclaration globale de style de page, il se peut que des déclarations locales de style soient également nécessaires, puisque certaines commandes LaTeX réinitialisent le style de la page sur laquelle elles apparaissent.
:::

## Avec l'extension « fancyhdr »

L'extension {ctanpkg}`fancyhdr` permet de définir des en-têtes et pieds de page. Elle est implémentée pour LaTeX et remplace l'extension {ctanpkg}`fancyheadings` pour LaTeX 2.09.

La définition des pages spéciales se fait par `\thispagestyle{⟨nom_du_style⟩}`. Le nom du style peut être « fancy », « plain » ou défini par l'utilisateur. L'application d'un style à toutes les pages (hors pages « spéciales ») se fait grâce à la commande `\pagestyle{nom_du_style}`.

Voici les principales commandes que définit {ctanpkg}`fancyhdr` :

- `fancyhf` pour initialiser les champs ;
- `fancyhead` pour remplir l'en-tête --- cette commande prend un argument facultatif indiquant la position et la page (avec l'option `twoside` : « `L` » pour la gauche (*left*), « `R` » pour la droite (*right*) et « `C` » pour le centre, « `O` » pour les pages impaires (*odd*) et « `E` » pour les pages paires (*even*) ;
- `fancyfoot` se comporte de la même façon que `fancyhead`, mais pour le pied de page ;
- `fancypagestyle` permet de définir un nouveau style d'en-têtes et pieds de page --- elle prend en premier argument le nom du style et en deuxième, la définition du style ;
- `headrulewidth` contient la largeur du filet de séparation entre l'en-tête et le reste de la page ;
- `footrulewidth` contient la largeur du filet de séparation entre le pied de page et le reste de la page.

On peut voir un exemple d'utilisation de {ctanpkg}`fancyhdr` sur l'exemple suivant :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[latin1]{inputenc}
  \usepackage[T1]{fontenc}

  \usepackage{fancyhdr}
  \usepackage[frenchb]{babel}

\fancyhf{}
\renewcommand{\headrulewidth}{0.2pt}
\renewcommand{\footrulewidth}{0.2pt}
\fancyhead[L]{\footnotesize{Un exemple d'en-têtes
     et pieds de page}}
\fancyfoot[R]{\thepage}
\fancyfoot[C]{\footnotesize{---}}
\fancyfoot[L]{\footnotesize{\textit{Les
     rédacteurs de la FAQ}}}

\begin{document}
  \thispagestyle{fancy}
  Voici une jolie page avec des jolis en-têtes
  et pieds de page bien définis, avec un petit
  filet de 0,2 points.
\end{document}
```

De plus, il est possible de demander à LaTeX d'utiliser les titres des sections ou chapitres courants dans les en-têtes. Les commandes `\sectionmark` (et `\chaptermark`, `\subsectionmark`...), `\markboth` et `\markright` s'utilisent comme dans l'exemple.

Utiliser le découpage logique dans les en-têtes et pieds de page :

```latex
% !TEX noedit
\documentclass{book}
  \usepackage[latin1]{inputenc}
  \usepackage[T1]{fontenc}

  \usepackage{fancyhdr}
  \usepackage[frenchb]{babel}

\fancyhf{}
\renewcommand{\headheight}{12.2pt}
\renewcommand{\headrulewidth}{0.2pt}
\renewcommand{\footrulewidth}{0.2pt}
\fancyhead[LE,RO]{\slshape \rightmark}
\fancyhead[LO,RE]{\slshape \leftmark}
\fancyfoot[C]{\thepage}

\begin{document}
  \thispagestyle{fancy}

\chapter{Un chapitre}

\section{Première partie}
  Voici une jolie page avec des jolis en-têtes
  et pieds de page bien définis, avec un petit
  filet de 0,2 points.
\newpage

\section{Deuxième partie}
  De plus, on voit bien que les titre du
  chapitre et de la section sont reproduits
  dans l'en-tête, page ci-contre.

\section{Troisième partie}
  Le style fancy par défaut se rapproche de
  cet exemple.
\end{document}
```

L'extension {ctanpkg}`fancyhdr` traite également la question des pages de garde (ou {doc}`non numérotées </3_composition/texte/pages/numerotation_des_pages/supprimer_les_numeros_de_pages>`), en vous permettant de définir différents styles pour ces pages et les autres pages du document.

## Avec l'extension « scrlayer-scrpage »

L'extension {ctanpkg}`scrlayer-scrpage` fournit une autre approche pour contrôler les hauts et bas de pages. Cette extension s'utilise en lien avec les classes {ctanpkg}`KOMA-script <koma-script>`, en lieu et place de l'extension {ctanpkg}`fancyhdr`.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` inclut des toutes les fonctionnalités de l'extension {ctanpkg}`fancyhdr` et dispose de plusieurs styles prédéfinis.

______________________________________________________________________

*Source :* {faquk}`Alternative head- and footlines in LaTeX <FAQ-fancyhdr>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,en-tête et pied de page
```

