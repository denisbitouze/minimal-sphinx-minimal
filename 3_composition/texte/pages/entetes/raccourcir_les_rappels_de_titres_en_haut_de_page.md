# Comment réduire les rappels de titres dans un haut ou bas de page ?

- Pour éviter que des titres trop longs n'apparaissent dans les entêtes ou les pieds de pages, il suffit de passer un titre plus court en option des commandes de structuration de document avec le package {ctanpkg}`fancyhdr`.

:::{note}
Dans ce cas, ce sont les titres courts qui apparaîtront dans la table des matières.
:::

Exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{fancyhdr}
\pagestyle{fancy}

\begin{document}
  \section[Titre résumé]{Un titre qui est beaucoup
    trop long pour les rappels}
  Intro.
  \subsection{Sous-section.}
  Texte.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

