# Comment scinder des boîtes de texte ?

En temps ordinaire, les boîtes TeX et LaTeX ne peuvent être découpées : une fois que vous avez composé quelque chose dans une boîte, il y reste, quitte à ce que la boîte dépasse sur le côté ou le bas de la page si elle ne rentre pas complètement dans la zone de composition.

Cette restriction s'avère délicate si vous souhaitez qu'une partie importante de votre texte soit encadrée (ou colorée). Heureusement, il existe des moyens de contourner le problème.

## Avec l'extension framed

L'extension {ctanpkg}`framed` fournit les environnements `framed` (encadré) et `shaded` (ombré). Tous deux mettent leur contenu une boîte encadrée (ou colorée) mais qui se scinde si nécessaire en fin de page. Ces environnements ont certaines limitations : ils perdent les notes de bas de page, les notes marginales ainsi que les entrées de titre et ne fonctionnent pas avec l'extension {ctanpkg}`multicol` ou d'autres commandes d'équilibrage de colonnes.

## Avec l'extension mdframed

L'extension {ctanpkg}`mdframed` fait le même travail, en utilisant un algorithme différent. Elle permet également de définir des environnements encadrés personnalisés, ce qui permet d'économiser des efforts considérables dans les documents comportant de nombreuses boîtes encadrées nécessitant des paramètres variés. Ses restrictions semblent sensiblement les mêmes que celles de {ctanpkg}`framed` mais la documentation des deux extensions donnent des écarts plus marqués.

## Avec l'extension boites

L'extension {ctanpkg}`boites` fournit un environnement `breakbox`. Des exemples de son utilisation peuvent être trouvés dans la {ctanpkg}`page de l'extension <boites>`. Les environnements peuvent être imbriqués et apparaître dans des environnements `multicols`. Cependant, les flottants, les notes de bas de page et les notes marginales seront perdus.

## Avec l'extension backgrnd

Pour les utilisateurs de Plain TeX, les fonctionnalités de l'extension {ctanpkg}`backgrnd` peuvent être utiles. Elle modifie la routine de sortie pour fournir des barres verticales pour marquer le texte et les commandes sont clairement marquées pour montrer où les arrière-plans colorés peuvent être introduits (ceci requiert l'extension {ctanpkg}`shade`, qui est distribué sous forme de commandes TeX et d'éléments MetaFont pour l'ombrage). L'auteur de {ctanpkg}`backgrnd` indique que l'extension fonctionne avec LaTeX 2.09 mais il y a des raisons de soupçonner qu'il pourrait être instable avec les versions actuelles de LaTeX.

## Avec la classe memoir

La classe {ctanpkg}`memoir` inclut les fonctionnalités de l'extension {ctanpkg}`framed`.

______________________________________________________________________

*Source :* {faquk}`Breaking boxes of text <FAQ-breakbox>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,boites,scinder une boîte,découper une boîte,framed,mdframed,backgrnd
```

