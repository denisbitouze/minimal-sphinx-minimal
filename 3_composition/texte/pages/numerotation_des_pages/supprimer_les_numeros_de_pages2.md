# Pourquoi ma page reste numérotée malgré le style de page « empty » ?

Si vous utilisez la commande `\pagestyle{empty}` pour retirer les numéros de page et que vous observez que certaines pages restent numérotées, vous vous trouvez probablement face à une décision de style intégrée dans les classes LaTeX standard : certaines pages spéciales devraient toujours apparaître avec le style `\pagestyle{plain}`, avec un numéro de page au centre du pied de page. Ces pages sont celles contenant une commande `\maketitle` ou, dans les classes {ctanpkg}`book` et {ctanpkg}`report`, les commandes `\chapter` ou `\part`.

La solution consiste à réémettre le style de page *après* les commandes citées ci-dessus, avec un effet limité à une seule page, comme par exemple (dans la classe {ctanpkg}`article`) :

```latex
% !TEX noedit
\maketitle
\thispagestyle{empty}
```

ou dans les classes {ctanpkg}`book` ou {ctanpkg}`report` :

```latex
% !TEX noedit
\chapter{Titre majeur}
\thispagestyle{empty}
```

Cette technique ne fonctionne pas pour la commande `\part`. Sur ce point, consultez la question « {doc}`Comment supprimer la numérotation des pages ? </3_composition/texte/pages/numerotation_des_pages/supprimer_les_numeros_de_pages>` ».

______________________________________________________________________

*Source :* {faquk}`I asked for "empty", but the page is numbered <FAQ-emptynum>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,numéro de page,numérotation des pages,empty,style de page,\\pagestyle
```

