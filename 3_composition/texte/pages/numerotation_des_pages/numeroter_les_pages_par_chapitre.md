# Comment réinitialiser la numérotation des pages à chaque chapitre ?

Pour des manuels techniques à reliure libre, l'usage consiste parfois à numéroter les pages par chapitre. En effet, si vos corrections ajoutent une page entière au chapitre, vous n'aurez qu'à redistribuer les seules pages de ce chapitre.

