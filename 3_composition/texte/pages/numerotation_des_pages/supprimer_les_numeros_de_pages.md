# Comment supprimer la numérotation des pages ?

Parfois, vous pouvez souhaiter obtenir un document ou quelques pages sans numéro de page. Voici différentes solutions.

## Avec l'extension nopageno

L'extension {ctanpkg}`nopageno` donne à la commande `\pagestyle{plain}` le même effet que `\pagestyle{empty}`. Dans les documents simples, cela supprime toute la numérotation des pages. Bien sûr, cela ne fonctionne pas si le document utilise un autre style de page que `plain`.

## Avec les commandes \\pagestyle et \\thispagestyle

Pour supprimer les numéros de page d'une séquence de pages, vous pouvez utiliser la commande `\pagestyle{empty}` au début de la séquence et restaurer le style de page d'origine à la fin. Malheureusement, vous devez toujours gérer les numéros de page sur les pages contenant une commande `\maketitle`, `\part` ou `\chapitre` (comme indiqué ci-après), puisque les classes standard les traitent différemment.

Pour supprimer le numéro de page d'une seule page, utilisez la commande `\thispagestyle{empty}` dans le texte de la page. Notez que, dans les classes standard, `\maketitle` et `\chapter` utilisent `\thispagestyle` en interne. Votre appel doit donc être effectué *après* ces commandes. Voir sur ce point la question « {doc}`Pourquoi ma page reste numérotée malgré le style de page « empty » ? </3_composition/texte/pages/numerotation_des_pages/supprimer_les_numeros_de_pages2>` ».

Malheureusement, `\thispagestyle` ne fonctionne pas pour la commande `\part` des classes {ctanpkg}`book` et {ctanpkg}`report`. Cette commande définit en effet le style de page (comme le fait `\chapter`) mais elle avance ensuite à la page suivante pour que vous n'ayez aucune possibilité de changer le style en utilisant `\thispagestyle`. Il existe là-dessus un patch manuel (trouvé sur `comp.text.tex`) :

```latex
% !TEX noedit
\makeatletter
\let\sv@endpart\@endpart
\def\@endpart{\thispagestyle{empty}\sv@endpart}
\makeatother
```

Heureusement, ce patch a maintenant été incorporé dans l'extension {ctanpkg}`nonumonpart`.

## Avec une rédéfinition de style

Il est possible de redéfinir le style `plain` en `empty` en utilisant ces commandes :

```latex
% !TEX noedit
\makeatletter
\let\ps@plain=\ps@empty
\makeatother
\AtBeginDocument{\pagestyle{plain}}
```

La commande `\AtBeginDocument` garantit que la commande mise en argument est exécutée après la commande `\begin{document}`, ce qui permet d'éviter que notre commande soit annulée par d'autres dans le préambule. Ici, sans cette commande, la suppression de numérotation ne se ferait par exemple que sur les pages de début de chapitre.

## Avec l'extension fancyhdr

Il est aussi possible, en utilisant l'extension {ctanpkg}`fancyhdr` (détaillée en question « {doc}`Comment définir les hauts et bas de page ? </3_composition/texte/pages/entetes/composer_des_en-tetes_et_pieds_de_page>` »), de redéfinir les en-têtes et pieds de page pour supprimer la numérotation (et éventuellement mettre autre chose à la place).

## Avec les classes KOMA-script et memoir

Les classes {ctanpkg}`KOMA-script <koma-script>` et {ctanpkg}`memoir` ont des styles de page dédiés aux diverses pages « spéciales ». Ainsi, dans un document de classe {ctanpkg}`KOMA-script <koma-script>`, l'absence de numérotation de la page titre s'obtiendrait avec :

```latex
% !TEX noedit
\renewcommand*{\titlepagestyle}{empty}
```

tandis que pour la classe {ctanpkg}`memoir`, il faudrait indiquer :

```latex
% !TEX noedit
\aliaspagestyle{title}{empty}
```

## Avec la commande \\pagenumbering

Une alternative (pour toutes les classes) est d'utiliser la commande suivante :

```latex
% !TEX noedit
\pagenumbering{gobble}
```

Toute tentative d'impression d'un numéro de page ne produit alors rien. Il n'y a donc aucun problème pour empêcher toute partie de LaTeX d'imprimer un numéro de page. Cependant, la commande `\pagenumbering` a pour effet secondaire de réinitialiser le numéro de page (à 1) : il est donc peu probable qu'elle soit utile autrement qu'au début d'un document.

## Avec l'extension scrpage2

L'extension {ctanpkg}`scrpage2` sépare la représentation du numéro de page (elle compose ce numéro en utilisant la commande `\pagemark`) de la construction de l'en-tête et du pied de page. Pour supprimer l'impression du numéro de page, il est donc possible d'écrire :

```latex
% !TEX noedit
\renewcommand*{\pagemark}{}
```

Notez bien qu'aucune de ces techniques de « suppression du numéro de page » n'affecte le style de page utilisé. En pratique, cela signifie qu'elles ne servent pas à moins que vous n'utilisiez `\pagestyle{plain}`.

______________________________________________________________________

*Source :* {faquk}`How to get rid of page numbers <FAQ-nopageno>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,numéro de page,suppression de numéro de page,supprimer la numérotation
```

