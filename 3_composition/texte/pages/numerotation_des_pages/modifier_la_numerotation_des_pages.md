# Comment modifier la numérotation des pages ?

- Pour changer la valeur du compteur de page, il suffit d'utiliser la commande `\setcounter{page}{numéro}`.
- Pour changer le style de numérotation, il faut modifier la commande `\thepage`. L'exemple suivant, de Piet van Oostrum, permet d'afficher un compteur comprenant, en plus du numéro de la page, le numéro du chapitre courant, et de remettre le compteur de pages à zéro à chaque nouveau chapitre :

```latex
% !TEX noedit
\makeatletter % dans le préambule
\renewcommand{\thepage}{\thechapter-\arabic{page}}
\@addtoreset{page}{chapter}
\makeatother
```

En y ajoutant le petit bout de code suivant (toujours de Piet van Oostrum), le compteur de page ne repart pas à zéro mais à un en première page de chapitre :

```latex
% !TEX noedit
\makeatletter % dans le préambule
\def\@stpelt#1{\global\csname c@#1\endcsname
               \expandafter\ifx \csname#1\endcsname
               \page
               \@ne
               \else
               \z@ \fi}
\makeatother
```

- Le code suivant, de Axel Kielhorn, permet de créer un package qui remet le compteur de page à 1 à chaque début de chapitre :

```latex
%% This is page-per-chapter-package
%% version 2.0
%%
%% Don't use it with refrep!
%% Refrep has these commands already implemented
%%
%% This version forces openright!!
%%
%% Index-commands should work in chapters and
%% appendices, they will not work as expected in
%% the preface when the pagenumbering is not arabic
%% (MakeIndex can't sort roman-numbers)
%%
%% Bugs :
%%       The index is sorted according to the
%%       pagenumber without looking at the
%%       chapternumber. I don't think MakeIndex
%%       could handle that.
%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{pagepc}[1995/05/13]

\@ifundefined{chapter}
{\PackageError{pagepc}{%
  You can't number your pages per chapter%
  \MessageBreak when you
  have no chapters
  }{%
  Use ``report'' or ``book'' instead.
  }
}%
{}

%% Reset the pagecounter to 1 at the start of a new
%% chapter
\let\ppchapter=\@chapter
\def\@chapter{%
  \if@pageperchapter\setcounter{page}{1}\fi
                    \ppchapter}

%% Force a pagebreak at the start of the appendix,
%% otherwise the number of the page right before
%% the appendix comes out wrong
\let\ppappendix=\appendix
\def\appendix{%
  \if@pageperchapter\newpage\fi\ppappendix}

\newif\if@pageperchapter \@pageperchapterfalse

%% This command enables Page-per-Chapter, it is
%% *not* on by default to allow roman pagenumbers
%% in the preface (see sample-document)
\newcommand{\pageperchapter}
    {\@pageperchaptertrue
     \@openrighttrue
     % Remember, for chapter 0 = preface
     \let\ppthepage=\thepage
     % The new number needs more space
     \renewcommand\ at pnumwidth{2.55em}
     % Here it comes :-)
     \renewcommand\thepage{%
       \ifnum \c@chapter = \z@
          \ppthepage
       \else
          \thechapter\ -- \arabic{page}
       \fi
       }
     }

%% This is a hack to make MakeIndex happy :-(
%% You can't use the |-form of an indexentry
%% because it is used to store the chapterno.
\def\@wrindex#1{%
   \ifnum \c@chapter = \z@
      \protected@write\@indexfile{}%
      {\string\indexentry{#1}{\arabic{page}}}%
      % above is wrong if pagenumbering!=arabic,
      % but I think this is better than nothing.
   \else
       \protected@write\@indexfile{}%
      {\string\indexentry{#1|ppc{%
            \thechapter}}{\arabic{page}}}%
   \fi
 \endgroup
 \@esphack
}

%% This prints the pagenumber in the index
\def\ppc#1#2{#1 -- #2}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

