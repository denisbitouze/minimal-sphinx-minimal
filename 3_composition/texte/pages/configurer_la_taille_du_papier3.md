# Comment résoudre les problèmes de dimension des pages ?

Le produit final d'une exécution de LaTeX est un document qui va être lu. Souvent, de nos jours, ce document sera lu « à l'écran », mais un document imprimé n'en reste pas moins une forme de restitution importante. Dans ce dernier cas, la bonne adaptation du document à la taille du papier est importante, si ce n'est cruciale dans le cas de l'édition d'un livre.

Malheureusement, il arrive que la sortie imprimée ne soit pas conforme aux attentes. Dans de tels cas, il faut vérifier les deux points suivants :

- si votre sortie est ouverte avec Adobe `Reader` (ou `Acrobat Reader`), il se peut que `Reader` change volontairement la taille du document. Consultez alors ici la question « {doc}`Comment corriger les réglages d'Adobe Reader ? </5_fichiers/pdf/imprimer_a_la_bonne_taille_avec_adobe_reader>` » ;
- un réglage de LaTeX produit une mauvaise taille (ou forme) de sortie : lisez « {doc}`Comment régler les dimensions des pages ? </3_composition/texte/pages/configurer_la_taille_du_papier>` ».

Une autre approche consiste à utiliser la suite {ctanpkg}`testflow` qui fournit un aperçu détaillé des problèmes potentiels avec un exemple de document et des exemples de sorties.

______________________________________________________________________

*Source :* {faquk}`The size of printed output <FAQ-papersize>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,impression,taille,dimnesion de la page
```

