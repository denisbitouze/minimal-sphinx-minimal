# Quelles sont les extensions utiles pour définir les pages et leurs dimensions ?

Il existe deux outils fiables pour ajuster les dimensions et la position des éléments imprimés sur la page : les extensions {ctanpkg}`geometry` et {ctanpkg}`zwpagelayout`. Une très large gamme d'ajustements de mise en page peut être programmée assez simplement avec l'une ou l'autre. De plus, la documentation, exhaustive, de ces extensions est de bonne qualité.

Comme d'habitude, les utilisateurs de la classe {ctanpkg}`memoir` ont des fonctions intégrées pour cette tâche et les utilisateurs des classes {ctanpkg}`KOMA-script` sont invités à utiliser une extension alternative, {ctanpkg}`typearea`. Dans les deux cas, il est difficile de dire que les utilisateurs devraient opter pour les deux entensions ci-dessus : les deux alternatives sont bonnes.

La documentation cumulée de {ctanpkg}`geometry` et de {ctanpkg}`zwpagelayout` est plutôt écrasante : apprendre toutes les fonctionnalités de l'une ou l'autre n'est sans doute pas nécessaire pour vos besoins ordinaires.

L'extension {ctanpkg}`vmargin` est un peu plus simple à utiliser : elle propose un ensemble prédéfini de formats de papier (un sur-ensemble de celui fourni dans LaTeX), de réglages pour du papier personnalisé, d'ajustements de marges et de réglages pour l'impression recto verso.

______________________________________________________________________

*Source :* {faquk}`Packages to set up page designs <FAQ-marginpkgs>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,page,geometry,zwpagelayout
```

