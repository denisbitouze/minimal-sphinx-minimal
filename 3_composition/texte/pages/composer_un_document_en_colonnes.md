# Comment obtenir un document multicolonnes ?

- L'option standard `twocolumn` permet de présenter un texte sur deux colonnes verticales :

```latex
\documentclass[twocolumn]{article}
  \usepackage[width=6cm,height=5cm]{geometry}
  \usepackage{lmodern,microtype}
  \usepackage[french]{babel}

\begin{document}
Voici un texte sur deux colonnes que \LaTeX{}
n'équilibre pas par lui-même (il remplit les
colonnes les unes après les autres). L'espace
entre les colonnes peut être modifié comme
indiqué plus loin. Une ligne de séparation des
colonnes peut également être insérée.
\end{document}
```

- Pour agir localement, on peut utiliser les commandes : `\twocolumn[⟨texte sur une colonne⟩]{⟨texte sur deux colonnes⟩}` puis `\onecolumn{⟨texte sur une colonne⟩}` ou plus généralement `\twocolumn` et `\onecolumn`.

Pour une meilleure lisibilité du source, on peut également utiliser les environnements correspondants :

```latex
\documentclass{article}
  \usepackage[width=6cm,height=8cm]{geometry}
  \usepackage{multicol}
  \usepackage{lmodern,microtype}
  \usepackage[french]{babel}
  \pagestyle{empty}

\setlength{\columnseprule}{0.5pt}

\begin{document}

\begin{multicols}{3}[Titre sur une seule colonne.]
   3~colonnes équilibrées, 3~colonnes équilibrées,
   3~colonnes équilibrées, 3~colonnes équilibrées.
\end{multicols}

\begin{multicols}{2}[\section{Titre numéroté.}]
   Blabla sur deux colonnes, c'est plus sérieux.
   C'est le style qui est généralement utilisé
   pour écrire des articles.
\end{multicols}
\end{document}
```

Pour ajouter un titre numéroté qui apparaisse sur toute la largeur de la page, il faut utiliser l'option `\section{⟨Titre⟩}` juste après `\begin{multicols}{⟨nb-col⟩}`.

:::{note}
Pour qu'une ligne de séparation apparaisse entre les colonnes, il faut utiliser : `\setlength{\columnseprule}{1pt}`.

Il est possible de redéfinir la largeur de l'espace inter-colonnes en utilisant cette commande : `\setlength{\columnsep}{30pt}`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,document en colonnes,colonnes multiples,composer un article en colonnes
```

