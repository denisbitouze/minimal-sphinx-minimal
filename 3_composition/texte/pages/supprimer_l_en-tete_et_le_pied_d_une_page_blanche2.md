# Comment obtenir des pages vraiment blanches entre des chapitres ?

Si vous utilisez les classes standards, vous devez prendre des mesures spéciales. Quant aux classes {ctanpkg}`memoir` et {ctanpkg}`KOMA-Script <Koma-Script>`, elles fournissent leurs propres méthodes pour traiter ce point.

## Avec les classes standards

Les classes {ctanpkg}`book` (par défaut) et {ctanpkg}`report` (avec l'option de classe `openright`) garantissent que chaque chapitre commence sur une page de droite (recto). Elles obtiennent ce résultat en insérant une commande `\cleardoublepage` entre les chapitres (plutôt qu'un simple `\clearpage`). La page vide ainsi créée a un en-tête normal, ce que certaines personnes n'apprécient pas.

La {texdoc}`documentation <fancyhdr>` de l'extension {ctanpkg}`fancyhdr` évoque ce problème, conseillant essentiellement la création d'une commande `\clearemptydoublepage` :

```latex
% !TEX noedit
\let\origdoublepage\cleardoublepage
\newcommand{\clearemptydoublepage}{%
  \clearpage
  {\pagestyle{empty}\origdoublepage}%
}
```

La suite logique est d'utiliser la commande ainsi obtenue en remplacement de `\cleardoublepage` dans une version corrigée de la commande `\chapitre` (vous pouvez même créer votre propre extension contenant une copie de la commande hors de la classe). Cette méthode n'est pas particulièrement difficile mais vous pouvez plus simplement écraser la définition de `\cleardoublepage` (qui n'est pas souvent utilisé par ailleurs) :

```latex
% !TEX noedit
\let\cleardoublepage\clearemptydoublepage
```

Notez que cette commande fonctionne car `\clearemptydoublepage` utilise une copie de `\cleardoublepage` : la question traitant des {doc}`patchs de commandes </2_programmation/macros/patcher_une_commande_existante>` explique ce type de méthode.

## Avec l'extension « emptypage »

L'extension {ctanpkg}`emptypage` fait automatiquement le réglage indiqué ci-dessus.

## Avec les classes « KOMA-Script »

Les classes KOMA-Script {ctanpkg}`scrbook` et {ctanpkg}`scrreprt` (qui remplacent respectivement {ctanpkg}`book` et {ctanpkg}`report`) proposent des options de classe qui contrôlent l'apparence de ces pages vides : `cleardoubleempty`, `cleardoubleplain` et `cleardoublestandard` (cette dernière utilisant alors le style de page en cours d'exécution). Les classes proposent également des commandes à modifier soi-même `\cleardoubleempty` (etc.).

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` (et l'extension {ctanpkg}`nextpage`) fournissent les commandes `\cleartooddpage` et `\cleartoevenpage`, qui prennent toutes deux un argument optionnel permettant d'obtenir des effets particuliers (sans argument, la première est équivalente à `\cleardoublepage`). La commande `\clearemptydoublepage` que nous recherchons serait réalisée par `\cleartooddpage[\thispagestyle{empty}]`. Les commandes serviront également si vous voulez avoir une mention « Page laissée vierge intentionnellement » au centre d'une page autrement vide.

______________________________________________________________________

*Source :* {faquk}`Really blank pages between chapters <FAQ-reallyblank>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,page blanche,chapitre
```

