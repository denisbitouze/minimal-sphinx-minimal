# Comment gérer un titre trop long dans un en-tête de page ?

Par défaut, les commandes de sectionnement de LaTeX permettent d'utiliser les titres de chapitre ou de section dans les en-têtes de page et autres. Mais les en-têtes de page sont positionnés dans une zone assez contrainte et les titres s'avèrent souvent trop longs y pour tenir.

## Avec les commandes de base

Les commandes de sectionnement autorisent un argument optionnel :

```latex
% !TEX noedit
\section[titre court]{titre long}
```

Si le *titre court* est présent, il sert à la fois pour la table des matières et pour l'en-tête de page.

Efficace, cette technique est devenue la réponse usuelle aux gens qui se plaignent de titres trop longs. Cependant, recourir au même texte pour la table des matières et pour les en-têtes peut également être peu satisfaisant : si les titres de vos chapitres sont vraiment très longs (comme ceux d'un [roman de Jules Verne](https://fr.wikisource.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours)), une alternative intéressante consiste à avoir une entrée de table des matières courte et une entrée plus laconique encore dans l'en-tête.

Ici, les commandes de sectionnement utilisent des commandes de « marque » pour transmettre des informations aux en-têtes de page. Par exemple, `\chapter` utilise `\chaptermark`, `\section` utilise `\sectionmark`, et ainsi de suite. Partant de cette idée, voici un exemple de structure à trois couches pour les chapitres :

```latex
% !TEX noedit
\chapter[titre court]{titre long}
\chaptermark{titre très court}
```

Les chapitres, cependant, ont la tâche facile : rares sont les livres où un en-tête de page est placé sur la page de début de chapitre (si c'est le cas, il faudra utiliser une méthode similaire à ce qui suit). Dans le cas des sections, il faut typiquement tenir compte de la nature des commandes de « marque » . L'élément qui va dans l'en-tête est la première marque sur la page (ou, à défaut de toute marque, la toute dernière marque des pages précédentes). En conséquence, la technique pour les sections est plus fastidieuse :

```latex
% !TEX noedit
\section[titre court]{titre long\sectionmark{titre très court}}
\sectionmark{titre très court}
```

Ici, le premier `\sectionmark` traite l'en-tête de la page sur laquelle la commande `\section` se positionne tandis que le second traite l'en-tête des pages suivantes. Notez qu'ici, vous avez besoin de l'argument optionnel pour `\section`, même si le *titre court* est le même texte que le *titre long*.

## Avec l'extension titlesec

L'extension {ctanpkg}`titlesec` gère les titres de sectionnement d'une manière complètement différente. Par exemple, vous pouvez utiliser l'argument facultatif des commandes de sectionnement pour les seuls en-têtes de page en chargeant l'extension avec l'option `toctitles` :

```latex
% !TEX noedit
\usepackage[toctitles]{titlesec}
```

La {texdoc}`documentation de l'extension <titlesec>` propose d'autres techniques utiles dans ce domaine.

## Avec la classe memoir

La classe {ctanpkg}`memoir` met en place un argument optionnel supplémentaire pour les commandes de chapitre et de sectionnement, par exemple :

```latex
% !TEX noedit
\section[titre court][titre très court]{titre long}
```

En conséquence, il est toujours possible pour les utilisateurs de {ctanpkg}`memoir` d'adapter le texte de l'en-tête au mieux, avec très peu de problèmes.

## Avec l'extension fancyhdr

Les problèmes de taille peuvent parfois venir du fait que LaTeX met les titres d'en-têtes entièrement en majuscules, moins économes en place. Il est ici possible de passer à un texte composé normalement en se servant de l'extension {ctanpkg}`fancyhdr`. Elle fournit une commande `\nouppercase` à utiliser dans les lignes d'en-tête (et de pied de page) pour supprimer le comportement par défaut de LaTeX. Les classes {ctanpkg}`KOMA-script` proposent par défaut des en-têtes composées sans ces majuscules systématiques.

______________________________________________________________________

*Source :* {faquk}`My section title is too wide for the page header <FAQ-runheadtoobig>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,en-tête,titre
```

