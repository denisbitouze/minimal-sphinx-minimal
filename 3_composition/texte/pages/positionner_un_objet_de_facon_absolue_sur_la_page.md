# Comment mettre des éléments à une position donnée sur une page ?

Le paradigme du monde TeX est (en gros) que l'auteur écrit un texte, puis que TeX et ses commandes décident de la façon dont tout ça est mis en page. Ce choix est parfois malheureux pour l'auteur qui, pour une raison ou une autre, doit parfois s'assurer que certaines choses apparaissent *exactement* là où il veut sur la page. Quelques extensions permettent cependant de positionner de répondre à ce besoin.

## Avec l'extension TikZ

L'extension la plus polyvalente est {ctanpkg}`TikZ <tikz>` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[showframe,paperwidth=6cm,paperheight=6cm,margin=4mm]{geometry}
  \usepackage{tikz}
  \pagestyle{empty}

\begin{document}
\begin{tikzpicture}[overlay,remember picture]
  \path (current page.north east) node[anchor=north east] {Coin superieur droit} ;
\end{tikzpicture}
\end{document}
```

:::{important}
Les **deux** options `overlay` et `remember picture` sont indispensables pour placer des points de façon absolue sur la page :

- `overlay` permet que des points soient placés en-dehors de l'image sans agrandir sa *bounding box* (autrement dit la boîte qui la délimite),
- `remember picture` stocke des références aux nœuds dans le fichier `.aux`, pour qu'elles puissent être utilisées par LaTeX à la compilation suivante (voir l'avertissement ci-contre).
:::

:::{warning}
Vous aurez besoin de deux compilations successives de votre document pour que les nœuds apparaissent, car Ti*k*Z utilise le fichier `.aux` pour les placer aux bons endroits sur la page.
:::

## Avec l'extension textpos

L'extension {ctanpkg}`textpos` permet de construire des pages à partir de « blobs » (de n'importe quoi, en fait), répartis sur toute la page, comme sur une affiche. Vous devez utiliser pour cela l'environnement `textblock` auquel vous donnez les coordonnées *(x,y)* (par rapport au coin supérieur gauche d'une page) ainsi que le contenu de ce « blob ».

Vous pouvez utiliser un repère absolu à l'aide de l'argument optionnel `[absolute]` dont l'origine est donnée à l'aide de `\textblockorigin`. Les abscisses et les ordonnées sont spécifiées avec `\TPHorizModule` et `\TPVertModule`. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}

\usepackage[absolute]{textpos}

% Unités en abscisses et en ordonnées
\setlength{\TPHorizModule}{100pt}
\setlength{\TPVertModule}{\TPHorizModule}

% Point de référence
\textblockorigin{10mm}{10mm}

\begin{document}
  \begin{textblock}{2}(1,1)
    Du texte de largeur 2 dont le coin
    haut/gauche est en (1,1).
  \end{textblock}

  \begin{textblock}{2}[0.5,0.5](3,2)
    Un autre texte de largeur 2 dont le
    centre est en (3,2).
  \end{textblock}
\end{document}
```

## Avec l'extension eso-pic

L'extension {ctanpkg}`eso-pic` définit une « image de sortie » qui couvre la page. Vous pouvez ajouter des commandes « en mode image », qui peuvent bien sûr inclure des placements de boîtes ou autres. Cette extension appelle {ctanpkg}`everyshi`, qui doit donc être aussi disponible.

______________________________________________________________________

*Sources :*

- {faquk}`Putting things at fixed positions on the page <FAQ-abspos>`,
- [TikZ full page with absolute node positioning](https://tex.stackexchange.com/questions/269844/tikz-full-page-with-absolute-node-positioning).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mixe en page,positionnement absolu sur la page,position sur la page,positionnement précis,mettre des choses à une position fixe sur la page
```

