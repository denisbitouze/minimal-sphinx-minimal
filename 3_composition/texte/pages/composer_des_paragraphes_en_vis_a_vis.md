# Comment obtenir des colonnes parallèles ?

Il est parfois nécessaire de présenter du texte en deux langues placées l'une à côté de l'autre sur une page ou sur une double page. Pour que cela soit satisfaisant, il faut généralement une sorte d'alignement entre les deux textes.

## Avec l'extension « parallel »

L'extension {ctanpkg}`parallel` répond à ce besoin en permettant la composition en deux colonnes (pas nécessairement de la même largeur) sur une page, ou sur deux pages en vis-à-vis. En voici un exemple en donnant le principe :

```latex
% !TEX noedit
\usepackage{parallel}
...
\begin{Parallel}{⟨largeur-gauche⟩}{⟨largeur-droite⟩}
  \ParallelLText{⟨texte de gauche⟩}
  \ParallelRText{⟨texte de droite⟩}
  \ParallelPar
  ...
\end{Parallel}
```

## Avec l'extension « pdfcolparallel »

L'extension {ctanpkg}`parallel` peut faire des erreurs de présentation lors de changements de couleur avec pdf(La)TeX. Heiko Oberdiek a un créé un correctif pour ce problème avec son extension {ctanpkg}`pdfcolparallel`. Elle charge {ctanpkg}`parallel` et maintient le traitement des couleurs séparé pour chaque colonne.

## Avec l'extension « parcolumns »

L'extension {ctanpkg}`parcolumns` peut (en principe) traiter n'importe quel nombre de colonnes : la documentation montre un cas avec trois colonnes. L'utilisation est assez similaire à celle de {ctanpkg}`parallel`, bien qu'il y ait bien sûr un paramètre gérant le nombre de colonnes à spécifier :

```latex
% !TEX noedit
\usepackage{parcolumns}
...
\begin{parcolumns}[⟨options⟩]{3}
  \colchunk{⟨Texte de la colonne 1⟩}
  \colchunk{⟨Texte de la colonne 2⟩}
  \colchunk{⟨Texte de la colonne 3⟩}
  \colplacechunks
  ...
\end{parcolumns}
```

Les `options` peuvent spécifier la largeur des colonnes, le placement éventuel de filets entre les colonnes, l'alignement des colonnes, etc. Une nouvelle fois, les couleurs peuvent poser problème et l'extension {ctanpkg}`pdfcolparcolumns` corrige ce point.

## Avec l'extension « reledpar »

L'extension {ctanpkg}`reledpar` est distribuée avec l'extension {ctanpkg}`reledmac`. Elle fournit un cadre parallèle soigneusement composé pour les besoins de [l'édition critique](https://fr.wikipedia.org/wiki/Apparat_critique) et permet de faire une traduction ou des notes, les deux possibilités pouvant être placées en parallèle avec le texte principal du document.

{octicon}`alert;1em;sd-text-warning` Cette extension remplace {ctanpkg}`ledpar <reledpar>` devenue {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`.

______________________________________________________________________

*Source :* {faquk}`Parallel setting of text <FAQ-parallel>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```

