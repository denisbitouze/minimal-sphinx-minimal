# Comment insérer un espace vertical dans une page ?

La commande de base est `\vspace`, qui prend en argument la hauteur de l'espace voulu :

```latex
\textit{L'espace efface le bruit.}

\vspace{1cm}
Victor Hugo.
```

:::{note}
La commande `\vspace*` force l'insertion d'un espace vertical même si ce dernier se situe sur un changement de page.
:::

Cependantans la plupart des cas, il est plus simple et préférable d'utiliser les commandes prédéfinies `\bigskip`, `\medskip` et `smallskip`. Outre qu'elles sont optimisées par les classes, elles présentent l'avantage d'être modulables en fonction des contraintes typographiques (par exemple, elles peuvent être réduites de manière ponctuelle pour éviter de terminer le paragraphe qui suit sur la première ligne de la page suivante).

Si vous avez besoin d'un espacement particulier, vous pouvez redéfinir les longueurs `\bigskipamount`, `\medskipamount` et `\smallskipamount` ou — mieux — {doc}`définir une nouvelle longueur </2_programmation/syntaxe/longueurs/manipuler_des_longueurs>`.

```latex
\newlength{\malongueurflexible}
\setlength{\malongueurflexible}{20pt plus 5pt minus 5pt}

Un paragraphe normalement espacé avec le suivant.

Longueur de \texttt{\\medskipamount} :
\the\medskipamount

\medskip

Longueur de \texttt{\\malongueurflexible} :
\the\malongueurflexible

\vspace{\malongueurflexible}

Fin du texte
```

Si vous voulez en outre indiquer que l'espace que vous avez inséré est un endroit adapté pour un changement de page (sans pour autant le forcer), vous pouvez aussi utiliser les commandes `\bigbreak`, `\medbreak` et `\smallbreak`.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

