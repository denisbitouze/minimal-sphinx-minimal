# Quelles sont les longueurs modifiables fournies directement par LaTeX pour la mise en page ?

La structure d'une page LaTeX permet de nombreux ajustements, par exemple en jouant sur les grandeurs suivantes :

- `\textwidth` permet de fixer la largeur du texte ;
- `\textheight` permet de fixer la hauteur du texte ;
- `\oddsidemargin` définit la marge gauche des pages impaires ;
- `\evensidemargin` la marge gauche des pages paires ;
- `\topskip` laisse une espace en haut de page ;
- `\footskip` laisse une espace en bas de page ;
- `\headheight` fixe la hauteur de l'en-tête ;
- `\topmargin` indique l'espace entre le haut de la feuille et l'en-tête de la page.

**Mais ne vous précipitez pas pour les modifier vous-même !**

## La bonne façon de faire : l'extension « geometry »

Le package {ctanpkg}`geometry` propose des fonctionnalités très pratiques pour définir son propre format de page sans avoir à modifier plein de ces grandeurs. Il définit de nouvelles variables de structure de la page. Il propose en outre un ensemble de formats par défaut (`a0paper`, `b5paper`, `legalpaper`...). Pour plus de détails, {doc}`voir ici </3_composition/texte/pages/configurer_la_taille_du_papier>`.

Le package {ctanpkg}`layout` produit une page d'exemple qui montre la taille relative des différentes zones de la page. Que vous utilisiez `geometry` ou que vous décidiez de vous passer de son aide, il vous guidera dans la définition de notre structure de page :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{layout}

\begin{document}
  \layout
\end{document}
```

```{image} https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/f/fc/Layout-dimensions.png
:alt: https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/f/fc/Layout-dimensions.png
:height: 720px
:width: 0px
```

## Voir aussi

Si votre installation de LaTeX utilise du format *letter* par défaut (format de papier américain), vous pouvez charger l'extension {ctanpkg}`a4` pour retrouver le format A4 usuel en Europe. Si vous trouvez les marges trop grandes, l'extension {ctanpkg}`a4wide` les réduira (à une largeur de 1 pouce), en même temps qu'elle passera en format A4.

Mais ces deux extensions ont des fonctionnalités très limitées et sont considérées comme {doc}`obsolètes </1_generalites/histoire/liste_des_packages_obsoletes>`. Il est maintenant conseillé d'utiliser {ctanpkg}`geometry` (voir ci-dessus).

:::{tip}
Si vous utilisiez {ctanpkg}`a4wide` par le passé, vous pouvez le remplacer simplement par :

```latex
% !TEX noedit
\usepackage[a4paper,margin=1in,includeheadfoot]{geometry}
```
:::

______________________________________________________________________

*Sources :*

- [Page size and margins](https://www.overleaf.com/learn/latex/page_size_and_margins).

```{eval-rst}
.. meta::
   :keywords: LaTeX,taille de page,format de papier,taille des marges,format A4,réduire les marges
```

