# Comment obtenir des onglets ?

:::{note}
Il est peu probable qu'une imprimante puisse accéder au ras de la marge. La solution consiste alors à définir un format de document plus petit et utiliser le massicot. Attention dans ce cas lors de la définition des marges (et à vos doigts).
:::

## Avec l'extension fancyhdr

Pour insérer un carré noir, décalé vers le bas à chaque nouveau chapitre, le long de la marge des pages de droite d'un document, on peut utiliser l'extension {ctanpkg}`fancyhdr`.

## Avec l'extension onglet

L'extenstion `onglet` écrite par Benjamin Bayart (fichier `onglet.sty` ci-dessous) permet d'obtenir le résultat souhaité. Elle nécessite l'extension {ctanpkg}`everyshi`.

```latex
\ProvidesPackage{onglet}[1996/07/25 B. Bayart]
\RequirePackage{everyshi}

\newcounter{maxchapter}
\newcounter{tmpa}
\newlength{\basehauteur}
\setlength{\basehauteur}{1cm}
\newlength{\ajoutdroite}
\newlength{\htcclv}
\def\concatener{%
  \setlength{\ajoutdroite}{\textheight}
  \divide\ajoutdroite by \basehauteur
  \setcounter{maxchapter}{\number\ajoutdroite}
  \setcounter{tmpa}{\value{chapter}}
  \addtocounter{tmpa}{-1}
  \divide\value{tmpa} by\value{maxchapter}
  \multiply\value{tmpa} by\value{maxchapter}
  \advance\value{tmpa} by -\value{chapter}
  \addtocounter{tmpa}{-1}
  \setlength{\ajoutdroite}{\paperwidth}
  \setlength{\htcclv}{\ht255}
  \addtolength{\ajoutdroite}{-\wd255}
  \addtolength{\ajoutdroite}{-1in}
  \addtolength{\ajoutdroite}{-1.5cm}
  \setbox255=\vbox{\hbox to \wd255{%
    \box255%\relax
    \rlap{\vbox to \htcclv{%
      \vskip-\value{tmpa}\basehauteur
      \hbox{%
        \hskip\ajoutdroite\relax
        \usebox{\laboite}%
      }%
      \vfill
    }}%
  \hfill}}%
}
\newsavebox{\laboite}
\def\faireboite{\sbox{\laboite}%
{\hbox to 1.5cm{\let\protect\relax
\huge\thechapter\hfill\vrule height 1em depth 0pt width 5mm}}}

\AtBeginDocument{\EveryShipout{\faireboite\concatener}}
```

### En suivant la méthode de Vincent Lozano

Pour son livre [Tout ce que vous avez toujours voulu savoir sur LaTeX sans jamais oser le demander](https://framabook.org/tout-sur-latex/) (livre libre, accessible à partir de ce lien), Vincent Lozano utilise une méthode dont il donne le code dans son livre en section 11.7).

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

