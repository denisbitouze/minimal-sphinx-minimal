# Comment créer une « `\subsubsubsection` » ?

Les niveaux de sectionnement numérotés de LaTeX s'arrête à la sous-sous-section, appelée par la commande `\subsubsection`. Cela reflète une décision de conception de Leslie Lamport. Après tout, qui a envie de se voir imposer un numéro à rallonge, comme « 3.2.12.6 », devant son titre ?

Dans les faits, les classes standard de LaTeX définissent des niveaux de sectionnement qui vont au-delà de `\subsubsection`, mais elles ne les composent pas comme les sections (elles ne sont pas numérotées et le texte est ajouté directement après le titre). Ces niveaux sont `\paragraph` et `\subparagraph`. Mais si vous devez utiliser des niveaux de sectionnement aussi profonds, vous pouvez faire en sorte que ces deux commandes produisent des titres numérotés, pour les utiliser comme des sous-sous-sous-sections.

## Avec l'extension « titlesec »

L'extension {ctanpkg}`titlesec` fournit un ensemble de commandes pour vous permettre d'ajuster les définitions des commandes de sectionnement. En particulier, il peut être utilisé pour transformer la composition de `\paragraph` afin qu'elle ressemble à celle de `\section` (avec la bonne numérotation).

## Avec des modifications manuelles

Si vous voulez programmer cette modification, vous constaterez que les commandes (`\section` jusqu'à `\subparagraph`) sont toutes définies par la commande interne `\@startsection`, qui prend 6 arguments et est illustrée dans la question « {doc}`Comment modifier le style des titres de sectionnement ? </3_composition/texte/titres/modifier_le_style_des_titres>` ». Aussi, il est conseillé de lire les sources LaTeX (`ltsect.dtx` dans la distribution LaTeX) et la source des extensions standard (`classes.dtx`), ou d'utiliser {doc}`The LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>`, qui traite de l'utilisation de `\@startsection`.

______________________________________________________________________

*Source :* {faquk}`How to create a \\subsubsubsection <FAQ-subsubsub>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,subsubsubsection,sous-sous-sous-section,titres de sections,paragraphes,sections,sous-sections,table des matières
```

