# Comment mettre en valeur la première lettre d'un paragraphe ?

Parfois, la première lettre d'un paragraphe est agrandie de manière à occuper plusieurs lignes de celui-ci. Ceci s'observe dans les journaux et dans des livres (le paragraphe concerné étant alors le premier de chaque chapitre). Cette lettre agrandie est appelée une [lettrine](https://fr.wikipedia.org/wiki/Lettrine) (en anglais, *initial* ou *dropped capital*).

## Avec l'extension « lettrine »

L'extension {ctanpkg}`lettrine` de Daniel Flipo propose la commande `\lettrine` avec une diversité d'options très intéressante. En voici un exemple jouant sur l'espacement par rapport à la lettrine de chaque ligne après la première.

```latex
\documentclass{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[width=8cm]{geometry}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage{lettrine}
  \pagestyle{empty}

\begin{document}
\lettrine[lines=4, slope=-0.5em, lhang=0.5,nindent=0pt]{V}{oici}
un exemple de l'utilisation de la commande \verb|\lettrine|.
Vous remarquerez que les débuts des lignes à droite de la lettre
sont décalés de manière à suivre la pente de celle-ci. Cela illustre
tout l'intérêt qu'il y a à retenir cette solution pour gérer les
lettrines.
\end{document}
```

Cette extension propose également une galerie d'exemple illustrant ses possibilités.

## Avec l'extension « dropping »

{octicon}`alert;1em;sd-text-warning` Les extensions {ctanpkg}`dropping`// et // {ctanpkg}`dropcaps` sont classées comme {doc}`obsolètes </1_generalites/histoire/liste_des_packages_obsoletes>`. Ce qui suit est informatif.

L'extension {ctanpkg}`dropping` étend l'extension {ctanpkg}`dropcaps`. Cependant, cette extension est liée avec l'ensemble des pilotes de périphériques disponibles dans les toutes premières extensions graphiques de LaTeX. Elle n'est donc pas recommandée pour des versions récentes de LaTeX.

```latex
% !TEX noedit
\documentclass[french]{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[dvips]{dropping}
  \usepackage{babel}

\begin{document}

\dropping{3}{\itshape{} Voici} un exemple de ce que permet de
faire l'extension dropping de M. Dahlgren. La commande
\verb.\dropping. peut prendre en argument un mot comme c'est
le cas ici ou une simple lettre.
\end{document}
```

## Avec l'extension « initials »

L'extension {ctanpkg}`initials` fait appel à des fontes `yinit` particulières. Son utilisation n'est donc pas recommandée.

## Avec l'extension « picins »

L'extension {ctanpkg}`picins` (et plus anciennement l {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>` {ctanpkg}`picinpar`) permet de placer une zone dans laquelle vous pouvez placer une lettre ou une image sans que le texte du paragraphe ne morde sur l'espace dédié à cet élément. Cela donne une solution détournée à la question.

## Avec une commande manuelle

Voici un exemple de commande, `\cappar`, répondant au besoin. Bien entendu, cette solution demande de mieux connaître le fonctionnement de LaTeX et ne répond pas forcément à un besoin générique.

```latex
% !TEX noedit
\font\capfont=cmbx12 at 24.87 pt% ou yinit, ou... ?
\newbox\capbox \newcount\capl \def\a{A}
\def\docappar{%
  \medbreak\noindent
  \setbox\capbox\hbox{%
    \capfont\a\hskip0.15em}%
  \hangindent=\wd\capbox%
  \capl=\ht\capbox
  \divide\capl by\baselineskip
  \advance\capl by1%
  \hangafter=-\capl%
  \hbox{%
    \vbox to8pt{%
      \hbox to0pt{\hss\box\capbox}%
      \vss
    }%
  }%
}
\def\cappar{\afterassignment\docappar%
\noexpand\let\a }

\cappar Il était une fois un petit chaperon rouge
qui avait une grand-mère qui habitait de l'autre
c\^oté de la for\^et. Un jour, alors que sa
grand-mère était malade, le petit chaperon rouge
décida de lui rendre visite...
```

:::{note}
Il est conseillé d'utiliser des polices PostScript extensibles.
:::

______________________________________________________________________

*Source :* {faquk}`Big letters at the start of a paragraph <FAQ-dropping>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,lettrine,grande lettre,agrandir une lettre,initiale
```

