# Comment obtenir une épigraphe ?

Une [épigraphe](https://fr.wikipedia.org/wiki/Épigraphe) est une citation que les auteurs mettent en tête de chapitre ou parfois en fin de chapitre, comme Donald Knuth dans le TeX​book.

Mettre en forme par vos soins ces épigraphes peut être un peu délicat mais pas impossible. Heureusement, plusieurs possibilités simples existent déjà.

## Avec l'extension « epigraph »

L'extension {ctanpkg}`epigraph` définit une commande `\epigraph` créant une simple épigraphe (en tête de chapitre) :

```latex
% !TEX noedit
\chapter{Des exemples}
\epigraph{Rien n'est plus dangereux qu'un bon conseil accompagné
d'un mauvais exemple.}{Madame de Sablé}
```

Elle fournit également un environnement `epigraphs` pour placer plus d'une épigraphe. Cet environnement fonctionne un peu comme une liste avec la commande `\qitem`.

```latex
% !TEX noedit
\begin{epigraphs}
\qitem{Le mauvais exemple est contagieux.}{Sophocle}
\qitem{Sans exemple, on ne peut rien enseigner correctement.}%
      {Columelle}
\end{epigraphs}
```

La commande `\epigraphhead` vous permet de placer votre épigraphe au-dessus du titre du chapitre. Cette commande dispose d'un argument optionnel permettant d'indiquer à quelle distance au-dessous de l'en-tête de la page l'épigraphe doit aller. Elle est exprimée sans unité, l'unité étant stockée dans la commande `\unitlength` (« 1pt » dans notre exemple) qui est aussi utilisée dans l'environnement `picture`. L'auteur de l'extension recommande « 70pt ».

```latex
% !TEX noedit
\setlength{\unitlength}{1pt}
...
\chapter{Des exemples}
\epigraphhead[50]{%
  \epigraph{Rien n'est plus dangereux qu'un bon conseil accompagné
d'un mauvais exemple.}%
           {Madame de Sablé}%
}
```

L'extension propose également diverses astuces pour ajuster la mise en page de l'en-tête de chapitre (nécessaires si vous avez une citation extrêmement longue dans une commande `\epigraphhead`), pour modifier la bibliographie, pour modifier les pages de partie générées par `\part`, et ainsi de suite. Certaines de ces suggestions et conseils vous guident d'ailleurs dans l'écriture de votre propre extension.

## Avec l'extension « quotchap »

L'extension {ctanpkg}`quotchap` redéfinit les en-têtes de chapitres et fournit un environnement `savequotes` dans lequel vous pouvez fournir une (ou plusieurs) citations à utiliser comme épigraphes. Si les fonctionnalités ne semblent pas aussi flexibles que celles de l'extension {ctanpkg}`epigraph`, elles sont probablement plus faciles à utiliser.

```latex
% !TEX noedit
\begin{savequote}[45mm]
Le mauvais exemple est contagieux.
\qauthor{Sophocle}
Sans exemple, on ne peut rien enseigner correctement.
\qauthor{Columelle}
\end{savequote}
\chapter{Des exemples}
```

L'argument optionnel valant ici `45mm` est la largeur que vous souhaitez avoir pour vos paragraphes de citation.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` offre toutes les fonctionnalités de l'extension {ctanpkg}`epigraph`.

## Avec les classes KOMA-Script

Les classes {ctanpkg}`KOMA-Script <Koma-script>` ont les commandes `\setchapterpreamble` et `\dictum` pour fournir des fonctionnalités similaires à celles présentées ici.

______________________________________________________________________

*Source :* {faquk}`Typesetting epigraphs <FAQ-epigraph>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,épigraphe,citation,début de chapitre,dédicace
```

