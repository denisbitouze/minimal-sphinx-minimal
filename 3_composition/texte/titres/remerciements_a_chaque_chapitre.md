# Comment utiliser `\thanks` dans chaque titre de chapitre ?

Pour associer des remerciements dans chaque chapitre (par exemple dans une thèse), il faut définir sa propre commande sur la base de `\footnote`. Ci-dessous est présentée une solution parmi d'autres : la commande `\thankschapter` de [Robin Fairbairns](https://www.ctan.org/author/fairbairns).

```latex
% !TEX noedit
\documentclass{report}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[francais]{babel}

\newcommand\thankschapter[2]{%
   % l'argument 1 est le titre du chapitre
   % l'argument 2 est le texte du '\thanks'
   \edef\savefootnote{\thefootnote}
   \renewcommand\thefootnote{\fnsymbol{footnote}}
   \chapter[#1]{#1\footnote[1]{#2}}
   \renewcommand\thefootnote{\arabic{footnote}}
   \setcounter{footnote}{\savefootnote}
}

\begin{document}
\thankschapter{Préface.}{\textit{Un grand merci à M.~Nigaudon
pour avoir accepter de préfacer ce livre.\\}}

Ce livre traitant de \LaTeX{}...

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

