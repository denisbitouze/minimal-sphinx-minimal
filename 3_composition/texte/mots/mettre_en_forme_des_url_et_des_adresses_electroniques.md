# Comment écrire des adresses web (ou URL) ?

Les URL ont tendance à être très longues et contiennent des caractères qui les empêchent d'être découpées facilement. Par conséquent, sans traitement spécial, elles produisent souvent des boîtes horizontales (`\hbox`) très longues et leur composition finale est alors laide. Ce qui suit présente des solutions pour arriver à mieux les gérer.

## Avec l'extension « path »

L'extension {ctanpkg}`path` définit une commande `\path`. Cette commande définit chaque caractère de césure potentielle comme un `\discretionary` et offre à l'utilisateur la possibilité de spécifier sa propre liste de caractères de césures potentielles. Cette commande est cependant {doc}`fragile </2_programmation/syntaxe/c_est_quoi_la_protection>`. Les {doc}`commandes d'Eplain </1_generalites/glossaire/qu_est_ce_que_eplain>` définissent une commande similaire `\path`. L'extension {ctanpkg}`path`, bien qu'elle fonctionne dans des situations simples, ne collabore pas bien LaTeX : malgré sa longue et honorable histoire, elle n'est plus vraiment recommandée.

## Avec l'extension « url »

L'extension {ctanpkg}`url` permet de gérer les coupures des URL un peu longues. Elle est également capable de gérer les adresses e-mail, les liens hypertextes, les noms de répertoires, etc. Elle définit une commande `\url` (et bien d'autres, y compris sa propre commande `\path`) qui donne à chaque caractère de césure potentielle un comportement assimilable à celui du mode mathématique et qui définit l'URL elle-même en mode mathématique (dans la police choisie par l'utilisateur).

Voici un exemple d'utilisation, illustrant aussi un paramétrage possible avec cette extension :

```latex
\documentclass{article}
  \usepackage{url}
  \urlstyle{sf}
  \pagestyle{empty}

\begin{document}
\url{http://hostname/~username}

\medskip
Lorsque l'on veut citer une URL dans du texte
\url{http://www.domaine.com/un/truc/long.html},
cela se passe relativement bien.
\end{document}
```

### Des commandes plus robustes

Les commandes `\path` de l'extension {ctanpkg}`path` et `\url` de l'extension {ctanpkg}`url` ne sont pas {doc}`robustes </2_programmation/syntaxe/c_est_quoi_la_protection>`. Toutefois si vous avez besoin d'une URL devant aller dans un argument mouvant, vous aurez besoin de la commande `\urldef` de l'extension {ctanpkg}`url`. Ceci donne par exemple pour rendre robuste la commande `\faqfr` :

```latex
% !TEX noedit
\urldef\faqfr\url{https://faq.gutenberg.eu.org/}
```

### La gestion des espaces

L'extension {ctanpkg}`url` ignore généralement les espaces dans les URL mais, malheureusement, certaines d'entre elles contiennent des espaces. Pour les traiter, appelez l'extension avec l'option `obeyspaces`. Deux autres options autorisent les sauts de ligne dans l'URL aux endroits où ils sont normalement supprimés pour éviter toute confusion :

- `spaces` pour autoriser les sauts au niveau des espaces (notez que cela nécessite également `obeyspaces`) ;
- `hyphens` pour autoriser les sauts après les tirets (car, l'extension ne fait *jamais* de césure « ordinaire » dans les URL).

Il est possible d'utiliser l'extension {ctanpkg}`url` en Plain TeX, avec l'aide de l'extension {ctanpkg}`miniltx` (qui a été initialement développé pour utiliser l'extension {ctanpkg}`graphics` de LaTeX dans Plain TeX). Une petit morceau de code est alors nécessaire :

```latex
% !TEX noedit
\input miniltx
\expandafter\def\expandafter\+\expandafter{\+}
\input url.sty
```

## Avec l'extension « hyperref »

L'extension {ctanpkg}`hyperref` utilise le code de {ctanpkg}`url` pour composer des liens et permet en plus de les rendre cliquables dans le document PDF.

## Avec la commande « \\discretionary »

La primitive `\discretionary` permet de dire comment couper une chaîne de caractères dans un texte : `\discretionary{Avant la coupure}{après}{s'il n'y en a pas}`

:::{note}
`\-` est défini comme `\discretionary{-}{}{}`.
:::

______________________________________________________________________

*Source :* {faquk}`Typesetting URLs <FAQ-setURL>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,adresse internet,adresse email,police tt,police machine à écrire,HTTP,HTTPS,lien web
```

