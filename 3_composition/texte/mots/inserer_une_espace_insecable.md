# Comment obtenir une espace insécable ?

Le caractère « `~` » est interprété par LaTeX et permet d'obtenir une [espace insécable](https://fr.wikipedia.org/wiki/Espace_ins%C3%A9cable). Voici un exemple d'utilisation, illustrant l'impact sur la composition de LaTeX.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{eurosym}
\begin{document}
Voici quelques cas nécessitant l'utilisation d'une espace insécable : 10 \%, 10 000 \euro{} (incorrect).

Voici quelques cas nécessitant l'utilisation d'une espace insécable~ : 10~\%, 10~000~\euro{} (correct).
\end{document}
```

```latex
\documentclass{article}
\usepackage{eurosym}
\pagestyle{empty}
\begin{document}
Voici quelques cas nécessitant l'utilisation d'une espace insécable : 10 \%, 10 000 \euro{} (incorrect).

Voici quelques cas nécessitant l'utilisation d'une espace insécable~ : 10~\%, 10~000~\euro{} (correct).
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,espace insécable
```

