# Comment insérer la date dans un document ?

Cette page traite de la question de l'affichage de [la date](https://fr.wikipedia.org/wiki/Date) courante. Une page est par ailleurs dédiée à la question « {doc}`Comment insérer l'heure dans un document ? </3_composition/texte/mots/inserer_l_heure_dans_un_document>` ».

## Sans extension

La commande `\today` permet de récupérer la date au moment de la compilation du document. Le format est cependant par défaut en anglais.

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
Nous sommes le \today.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Nous sommes le  \today.
\end{document}
```

## Avec l'extension babel

L'extension {ctanpkg}`babel` permet d'adapter le comportement de `\today` à d'autres langues.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\begin{document}
Nous sommes le \today.
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Nous sommes le  \today.
\end{document}
```

## Avec l'extension datetime2

L'extension {ctanpkg}`datetime2` fournit de nombreux outils de mise en forme de la date. L'extension {ctanpkg}`babel` doit etre chargée avant {ctanpkg}`datetime2` ; toutefois, pour bénéficier de formats spécifiques à la langue, l'option `useregional` doit être retenue. Voici un exemple montrant son utilisation, avec un cas de modification de format.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\usepackage[useregional]{datetime2}

\begin{document}
Nous sommes le \today{}.

\DTMsetdatestyle{ddmmyyyy}  % afficher les dates en format "jj mm aaaa"
\DTMsetup{datesep=/}        % ajout du séparateur "/" dans les dates
Nous sommes le \today{}.
```

```latex
\documentclass{report}
\usepackage[french]{babel}
\usepackage[useregional]{datetime2}
\pagestyle{empty}

\begin{document}
Nous sommes le \today{}.

\DTMsetdatestyle{ddmmyyyy}
\DTMsetup{datesep=/}
Nous sommes le \today{}.
\end{document}
```

Cette extension remplace {ctanpkg}`datetime` et sa documentation explique comment passer de {ctanpkg}`datetime` à {ctanpkg}`datetime2`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,date
```

