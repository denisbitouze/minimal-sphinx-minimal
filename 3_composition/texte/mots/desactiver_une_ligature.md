# Comment empêcher une ligature ?

C'est un point fort de TeX : certains couples de lettres sont automatiquement remplacés par un glyphe unique, pour des raisons esthétiques, comme on le fait en imprimerie au plomb. Par exemple quand un `f` et un `i` se suivent, le haut du `f` aurait normalement tendance à mordre sur le point du `i` : {raw-latex}`\large f\null i`, ce qui ne serait pas très beau. TeX les remplace donc par un nouveau glyphe plus joli : {raw-latex}`\large fi`. Cette fusion de glyphes est une [ligature](<https://fr.wikipedia.org/wiki/Ligature_(écriture)>).

Le même mécanisme est mis en jeu pour obtenir les caractères espagnols ¡ et ¿,
que l'on saisit ```` !` ```` et ```` ?` ````.

Dans les polices cyrilliques utilisées avec l'encodage OT2, le couple de lettres `sh` produira le glyphe ш.

## Empêcher ponctuellement une ligature

Pour empêcher une ligature, il suffit de séparer les lettres dans le code LaTeX avec quelque chose qui ne produira rien dans le fichier de sortie. Par exemple : `\/`, `{}`, `\mbox{}`, `\null`. Les codages de fontes T1 et OT1 fournissent aussi la commande `\textcompwordmark`, qui a le même but.

L'exemple ci-dessous montre la différence de résultat.

```latex
\documentclass{article}
  \usepackage{fontspec}
  \usepackage[french]{babel}
  \pagestyle{empty}

\defaultfontfeatures{
  Ligatures={
    NoCommon,
    NoRequired,
    NoContextual,
    NoHistoric,
    NoDiscretionary
  }
}
\setmainfont[
   BoldFont=lmroman10-bold.otf,
   ItalicFont=lmroman10-italic.otf,
   BoldItalicFont=lmroman10-bolditalic.otf,
   SlantedFont=lmromanslant10-regular.otf,
   BoldSlantedFont=lmromanslant10-bold.otf,
   SmallCapsFont=lmromancaps10-regular.otf
]{lmroman10-regular.otf}

\begin{document}
\Large Comment effacer efficacement les fleurs ?
\end{document}
```

À comparer à la version qui ne désactive pas les ligatures :

```latex
\documentclass{article}
  \usepackage{fontspec}
  \usepackage[french]{babel}
  \pagestyle{empty}

\setmainfont[
   BoldFont=lmroman10-bold.otf,
   ItalicFont=lmroman10-italic.otf,
   BoldItalicFont=lmroman10-bolditalic.otf,
   SlantedFont=lmromanslant10-regular.otf,
   BoldSlantedFont=lmromanslant10-bold.otf,
   SmallCapsFont=lmromancaps10-regular.otf
]{lmroman10-regular.otf}

\begin{document}
\Large Comment effacer efficacement les fleurs ?
\end{document}
```

et la version qui n'utilise pas 

`\setmainfont`

 pour charger la police :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\Large Comment effacer efficacement les fleurs ?
\end{document}
```

______________________________________________________________________

*Sources :*

- Livre *LaTeX : Apprentissage, guide et référence*, de Bernard Desgraupes.
- [LaTeX pour les linguistes](https://hal.archives-ouvertes.fr/cel-02145840/document), Thomas Pellard.
- [Removing ligatures when using fontspec](https://tex.stackexchange.com/questions/103238/removing-ligatures-when-using-fontspec).
- [When should I not use a ligature in English typesetting?](https://english.stackexchange.com/questions/50660/when-should-i-not-use-a-ligature-in-english-typesetting)

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,fi,ffi,ligature esthétique,fontspec
```

