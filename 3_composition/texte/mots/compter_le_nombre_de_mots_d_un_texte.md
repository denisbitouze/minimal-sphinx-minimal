# Comment compter le nombre de mots d'un document ?

Il arrive parfois qu'un document soit soumis à une contrainte de taille exprimée en un nombre de mots.

Une solution simple existe, basée sur un constat simple : vos relecteurs ou jurys ont peu de chances de compter tous les mots d'un document qui leur est soumis. Par conséquent, une méthode statistique peut être employée :

- trouver combien de mots il y a sur une page entière ;
- trouver combien de pages complètes compte le document (en tenant compte des divers affichages et figures insérées, ce nombre ne sera probablement pas un entier) ;
- multipliez les deux.

Cependant, si le document à soumettre doit déterminer le reste de votre vie, mieux vaut peut-être ne pas tenter ce pied de nez. Vous vous retrouvez alors face à un problème complexe si la réponse doit être précise : il faut en effet pouvoir distinguer d'une part les mots à prendre en compte et d'autre part les commandes LaTeX, qui devront être développées afin de savoir combien de mots elles engendrent. Différentes solutions, listées ci-dessous, existent et certains {doc}`éditeurs </6_distributions/editeurs/start>` proposent des traitements similaires.

## Avec une conversion en dvi

Une solution consiste à générer une sortie `dvi` puis de la convertir en texte, avec `dvi2tty`, puis compter le nombre de mots. Il est possible aussi de transformer un document postscript en texte, avec `ps2ascii`. Le compte de mot peut être ensuite effectué avec des traitements de texte simples ou avec des utilitaires dédiés. Sur Linux, cela peut être fait en ligne de commande avec « `wc` » (pour *word count*).

## Avec l'exécutable detex

Il est possible de travailler sur le source LaTeX, en utilisant le programme {ctanpkg}`detex`, qui supprime toutes les commandes LaTeX pour ne laisser que le texte. Ensuite, il ne reste qu'à compter ce qui reste. Voici, sur Linux, un exemple de shell avec la commande « `wc` » :

```bash
detex <> → ⟨fichier⟩ | wc -w
```

Il convient de noter que le programme {ctanpkg}`detex` n'est plus développé et est remplacé par [opendetex](https://github.com/pkubowicz/opendetex/).

## Avec latexcount

Le script Perl {ctanpkg}`latexcount` fait ce travail, en étant en principe assez simple à configurer (voir documentation à l'intérieur du script).

## Avec texcount

Le script Perl {ctanpkg}`texcount` va très loin avec l'heuristique de dénombrement des mots d'un fichier LaTeX. La documentation est complète et vous pouvez essayer le script en ligne via la [page officielle du script](https://app.uio.no/ifi/texcount/).

Cependant, même un traitement sophistiqué du balisage LaTeX ne peut jamais être entièrement fiable : le balisage lui-même peut ajouter ou retrancher des mots dans le texte.

## Avec l'extension wordcount

L'extension {ctanpkg}`wordcount` contient un script *Bourne shell* (donc propre à Unix) qui exécute un fichier LaTeX avec un bloc de code TeX dédié puis qui compte les indications de mot obtenues dans le fichier journal. Il s'agit probablement du calcul automatique le plus précis dont vous pourrez disposer.

______________________________________________________________________

*Source :* {faquk}`How many words have you written? <FAQ-wordcount>`

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```

