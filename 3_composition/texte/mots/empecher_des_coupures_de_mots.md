# Comment supprimer certaines césures ?

## Avec les commandes \\hyphenation et \\showhyphens

Pour agir sur un mot particulier, il faut utiliser la commande `\hyphenation`, comme illustré dans la question « {doc}`Comment définir certaines césures ? </3_composition/texte/mots/introduire_des_coupures_de_mots>` ». Par ailleurs, la commande `\showhyphens` permet de savoir quelles sont les césures possibles selon les règles de LaTeX : le résultat de cette commande apparaît dans le fichier `log`. Voici un exemple de ces deux commandes en action :

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}

\begin{document}
Un test.
\showhyphens{Un mot vraiment long : anticonstitutionnellement.}
\hyphenation{anti-constitutionnelle-ment}
\showhyphens{Un mot vraiment long : anticonstitutionnellement.}
\end{document}
```

Après compilation, le fichier `log` présente deux lignes dues à la présence des commandes `\showhyphens`. Elles montrent que la commande `\hyphenation` a modifié le comportement de LaTeX.

```
[] \OT1/cmr/m/n/10 Un mot vrai-ment long : an-ti-cons-ti-tu-tion-nel-le-ment.
[] \OT1/cmr/m/n/10 Un mot vrai-ment long : anti-constitutionnelle-ment.
```

## Avec la commande \\hbox

Moins propre mais tout aussi efficace, on peut inclure le mot à ne pas couper dans une commande `\hbox`. Cela s'applique plus généralement à une phrase qu'on voudrait ne pas couper. Il faut garder en tête que, en mettant une phrase dans une commande `\hbox`, les espaces auront leur taille naturelle (plus de compression ni d'étirement).

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}

\begin{document}
Même placée au fin fond du bout de la ligne, une boîte horizontale n'est pas coupée.

Même placée au fin fond du bout de la ligne, \hbox{une boîte horizontale n'est pas coupée}.
\end{document}
```

```latex
Même placée au fin fond du bout de la ligne, une boîte horizontale n'est pas coupée.

Même placée au fin fond du bout de la ligne, \hbox{une boîte horizontale n'est pas coupée}.
```

## Avec la commande \\hyphenpenalty

Pour empêcher LaTeX de couper les mots dans tout un document, il est possible de lui indiquer que la césure n'est pas du tout recommandée avec : `\hyphenpenalty 10000`.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\hyphenpenalty 10000

\begin{document}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{document}
```

```latex
\documentclass{report}
\usepackage[french]{babel}
\hyphenpenalty 10000
\pagestyle{empty}
\begin{document}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{document}
```

## Avec la définition de famille de fonte

De manière globale, on peut aussi supprimer les césures pour toute une famille de fonte en déclarant :

```latex
% !TEX noedit
\DeclareFontFamily{T1}{cmr}{\hyphenchar\font=-1}
```

## Avec la commande \\uchyph

Pour interdire la coupure de tous les mots commençant par une majuscule, il faut utiliser : `\uchyph=0` dans le préambule du document.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\uchyph=0

\begin{document}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.

Même placé au fin fond du bout de la ligne, un mot comme Anticonstitutionnellement peut surprendre.
\end{document}
```

```latex
\documentclass{report}
\usepackage[french]{babel}
\uchyph=0
\pagestyle{empty}
\begin{document}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.

Même placé au fin fond du bout de la ligne, un mot comme Anticonstitutionnellement peut surprendre.
\end{document}
```

## En modifiant les motifs de césures

On peut également interdire la coupure des mots d'une langue particulière dans un document multilingues en utilisant l'astuce suivante : il suffit de créer un fichier de motifs de césures vide. Par exemple pour le russe, ceci donnerait la manipulation suivante :

```latex
\patterns{}
```

Et dans le fichier `language.dat` il faut ajouter la ligne

```
russian ruhyph.tex
```

## Avec des méthodes moins autoritaires

Les deux méthodes suivantes sont moins impératives pour LaTeX. Elles servent surtout à donner un plus plus de flexibilité à ce dernier lorsqu'il traite les césures. Dans les exemples qui suivent, on verra que LaTeX va d'ailleurs pratiquer une césure.

### Avec l'environnement sloppypar

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}

\begin{document}
\begin{sloppypar}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{sloppypar}
\end{document}
```

```latex
\documentclass{report}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\begin{sloppypar}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{sloppypar}
\end{document}
```

### Avec la commande \\sloppy

La commande `\sloppy`, placée dans le préambule du document, agit sur la totalité du document.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\sloppy

\begin{document}
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{document}
```

```latex
\documentclass{report}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\sloppy
Même placé au fin fond du bout de la ligne, un mot comme anticonstitutionnellement peut surprendre.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,césure
```

