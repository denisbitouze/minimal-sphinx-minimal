# Pourquoi l'espace disparaît après une commande ?

Certains sont surpris de voir que de simples commandes font disparaître l'espace située après elles. C'est pourtant une règle fondamentale de LaTeX. Par exemple :

```latex
\LaTeX est un traitement de texte.
Il se base sur \TeX.
```

Voici quelques solutions usuelles pour ne plus avoir de mauvaises surprises.

## Avec l'ajout de « {} »

Leslie Lamport recommande d'ajouter systématiquement une paire d'accolades après la commande. Voici notre exemple corrigé :

```latex
\LaTeX{} est un traitement de texte.
Il se base sur \TeX{}.
```

Cependant, de nombreux utilisateurs trouvent que ces accolades rendent moins lisible le document source.

(avec-lajout-de-1)=

## Avec l'ajout de « \\ »

Une deuxième méthode, qui fonctionne avec LaTeX, est d'utiliser la commande ''\\ '' (une barre oblique inversée suivie d'une espace) après la commande à corriger. Elle n'a pas besoin des d'accolades après elle et elle permet ainsi de réduire à un seul le nombre de caractères supplémentaires à taper après votre commande :

```latex
\LaTeX\ est un traitement de texte.
Il se base sur \TeX.
```

## Avec l'extension « xspace »

Si un simple caractère `\` est encore de trop pour vous, l'extension {ctanpkg}`xspace` définit une commande `\xspace` qui détermine automatiquement s'il doit y avoir une espace après elle ou pas, et l'ajoute si besoin. Avec `xspace`, on pourrait donc écrire :

```latex
\documentclass{article}
  \usepackage{xspace}
  \pagestyle{empty}
\begin{document}
\LaTeX\xspace est un traitement de texte.
Il se base sur \TeX\xspace.
\end{document}
```

Vous allez dire qu'on y a plutôt perdu au change, en saisissant `\xspace` au lieu de ''\\ ''... En fait, cette extension est plutôt pensée pour l'écriture de macros personnelles qui pourront ainsi gérer correctement l'espace après elles. Dans ce cas, la commande `\xspace` doit être le dernier élément indiqué dans votre définition de commande. En voici un exemple pratique :

```latex
\documentclass{article}
  \usepackage{xspace}
  \newcommand{\latex}{\LaTeX\xspace}
  \newcommand{\tex}{\TeX\xspace}
  \pagestyle{empty}

\begin{document}
\latex est un traitement de texte. Il se base sur \tex.
\end{document}
```

Un autre exemple d'utilisation est donné au début de la question « {doc}`Comment gérer des abréviations ? </3_composition/texte/mots/definir_des_abreviations>` ».

La règle utilisée par {ctanpkg}`xspace` est plutôt simple :

- S'il n'y a pas d'accolades et d'espace après la commande, insérer une espace.
- S'il y a des accolades, ne rien faire.

Notez bien cependant que cette commande n'est pas totalement infaillible : elle ne sait gérer que les situations les plus courantes dans la mise en forme du texte. Soyez donc prudent en l'utilisant, d'autant plus qu'elle change votre façon d'écrire, ce qui peut dérouter un éventuel co-auteur (en particulier si vous créez des commandes qui utilisent ou pas `\xspace`). Ceci explique aussi pourquoi aucune commande propre à LaTeX ou à ses classes et extensions standards n'utilise `\xspace`.

______________________________________________________________________

*Sources :*

- {faquk}`Commands gobble following space <FAQ-xspace>`,
- [Always finish commands with {}](https://en.wikibooks.org/wiki/LaTeX/FAQ#Always_finish_commands_with_%7B%7D).

```{eval-rst}
.. meta::
   :keywords: LaTeX,espace,syntaxe,espace après une commande,espace qui disparaît,mots collés
```

