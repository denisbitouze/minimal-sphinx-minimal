# Comment obtenir une espace de taille donnée ?

De façon générale, LaTeX gère tout seul les espaces : il est inutile de taper plusieurs espaces de suite entre deux mots, ils seront transformés en une seule espace dans le fichier final (DVI, PS ou PDF). Vous pouvez mettre ceci à profit pour rendre votre code source plus lisible.

Inversement, si vous souhaitez insérer manuellement plus d'espace, il faudra utiliser des commandes telles que `\phantom` ou `\hspace`.

## Avec la commande \\phantom

Si la taille de l'espace souhaitée est celle d'un texte, la commande `\phantom` laisse une espace correspondant à son argument, dans la fonte courante. Il existe deux variantes, `\vphantom` (espace vertical) et `\hphantom` (espace horizontale).

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
A\phantom{BCDE}F

A\hphantom{BCDE}F

ABCDEF
\end{document}
```

## Avec la commande \\hspace

Si la taille de l'espace souhaitée est {doc}`exprimée en unités de mesure </2_programmation/syntaxe/longueurs/unites_de_mesure_de_tex>` (millimètres, `ex`...), la commande `\hspace` permet d'insérer cette espace. Voici un exemple :

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
A\hspace{2.5cm}B
\end{document}
```

:::{note}
La commande `\hspace` n'a pas d'effet en début ou en fin de ligne (elle est simplement ignorée). Si vous souhaitez qu'elle ait **toujours** un effet, même en début ou en fin de ligne, utilisez plutôt sa version étoilée, le commande `\hspace*`.

Observez ci-dessous la différence entre les deux : dans le premier cas, `\hspace{3cm}` n'insère pas d'espace entre « et » et « avec » car il se retrouve en fin de ligne, tandis que sa version avec l'étoile, `\hspace*{3cm}`, insère bien l'espace.

```latex
\fbox{\parbox{4cm}{
 Un peu de texte pour voir ce qui se passe :
 \hspace{2cm} et \hspace{3cm}
 avec encore du texte ensuite.


\bigskip

\fbox{\parbox{4cm}{
 Un peu de texte pour voir ce qui se passe :
 \hspace*{2cm} et \hspace*{3cm}
 avec encore du texte ensuite.
```
:::

## Avec la commande \\vrule

La commande `\vrule` crée une barre. Si on choisit de lui donner une épaisseur nulle, on obtient une espace. Voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
A{\vrule height 0pt depth 0pt width 2.5cm}B
\end{document}
```

```latex
\documentclass{report}
\pagestyle{empty}
\begin{document}
A{\vrule height 0pt depth 0pt width 2.5cm}B
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,espace
```

