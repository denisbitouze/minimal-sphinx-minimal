# Comment modifier l'espacement entre caractères ?

Une technique courante dans la publicité (et dans les textes dont le contenu n'a pas vraiment besoin d'être *lu*) consiste à modifier l'espace entre les lettres autrement dit l'interlettrage (ou *tracking* en anglais). En règle générale, c'est une très mauvaise idée : cela nuit à la lisibilité, en contradiction totale avec les principes de la composition. Toute police respectable que vous souhaitez utiliser devrait déjà avoir un interlettrage optimisé.

Eric Gill, grand concepteur de caractères, aurait dit : « celui qui modifie l'interlettrage d'un texte en minuscules pourrait voler des moutons ». Si cette phrase est aussi attribuée à d'autres, il faut surtout noter que voler un mouton au 19{sup}`e` siècle était un crime passible de la peine capitale en Grande-Bretagne ! Vous voilà prévenus.

Comme la remarque le suggère indirectement, modifier l'interlettrage de mots en majuscules s'avère un crime bien moins terrible. Ceci sert aussi pour mettre en évidence un texte présenté avec des polices *Fraktur* (ou similaires).

## Avec letterspacing

Des commandes simples et utilisables, en principe, avec n'importe quel ensemble de commandes TeX peuvent être trouvées dans {ctanpkg}`letterpacing`, un unique fichier `tex` dont le CTAN indique qu'il ne devrait pas servir en temps ordinaire à mettre en forme un texte.

## Avec l'extension soul

L'extension {ctanpkg}`soul` permet de régler les espacements entre les caractères, dans les mots, et entre les mots eux-mêmes avec la macro `\so` (optimisée pour une utilisation avec LaTeX mais utilisable avec Plain TeX). Les dimensions des espacements ajoutés entre les caractères, entre les mots et devant et derrière le texte interlettré peuvent être redéfinis à l'aide de la commande `\sodef`. L'extension permet, contrairement à {ctanpkg}`letterpacing`, la césure du texte si les lettres sont espacées. L'avis de Gill sur une telle manipulation n'est, de façon fort regrettable, pas connu.

## Avec l'extension tracking

L'extension {ctanpkg}`tracking` permet de jouer sur les espacements dans les mots ou les phrases pour les ajuster dans une longueur spécifiée.

## Avec l'extension microtype

L'extension de référence dans ce domaine est sans doute {ctanpkg}`microtype`. Elle utilise les capacités de micro-typographie de pdfTeX avec une commande `\textls` qui fonctionne selon les paramètres déclarés dans une commande `\SetTracking`. La fonction d'interlettrage de {ctanpkg}`microtype` augmente l'espacement naturel de la police elle-même, plutôt que d'insérer un espace entre les caractères. Normalement, augmenter l'espace interlettre détruit les ligatures ; cependant, c'est *incorrect* pour certains styles de police (par exemple, *Fraktur*), et l'extension fournit ici un moyen pour protéger les ligatures dans un texte où les lettres ont été espacées.

______________________________________________________________________

*Sources :*

- {faquk}`Changing the space between letters <FAQ-letterspace>`,
- [Approche (typographie)](<https://fr.wikipedia.org/wiki/Approche_(typographie)>).

```{eval-rst}
.. meta::
   :keywords: LaTeX,approche,interlettre,espacement des lettres,texte étiré
```

