# Comment visualiser des espaces ?

## Avec l'environnement verbatim\*

L'environnement `verbatim*` et la commande `\verb*` permettent de visualiser les espaces insérés dans un texte en les remplaçant par un caractère spécial en forme de u.

Exemple de verbatim avec des espaces visibles :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\begin{document}
\begin{verbatim*}
Voici des espaces   bien   visibles.
\end{verbatim*}
Voici \verb*+d'autres espaces + plus ciblés.
\end{document}
```

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\pagestyle{empty}
\begin{document}
\begin{verbatim*}
Voici des espaces   bien   visibles.
\end{verbatim*}
Voici \verb*+d'autres espaces + plus ciblés.
\end{document}
```

## Avec la commande \\textvisiblespace ou une commande créée pour l'occasion

LaTeX propose la commande `\textvisiblespace`.

```latex
% !TEX noedit
Je mange\textvisiblespace une\textvisiblespace pomme.
```

```latex
Je mange\textvisiblespace une\textvisiblespace pomme.
```

En TeX, c'est le caractère 32 de la fonte *cmtt*, qui est défini dans le source du TeX​book par :

```latex
% !TEX noedit
\def\]{\leavevmode\hbox{\tt\char`\ }}
```

Vous pouvez donc, pour LaTeX, définir la commande `\vs` pour obtenir ce fameux caractère.

```latex
% !TEX noedit
\def\vs{\leavevmode\hbox{\tt\char`\ }}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

