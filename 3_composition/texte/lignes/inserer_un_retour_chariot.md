# Comment insérer un retour chariot ?

Les retours chariot (fins de lignes) dans votre code-source ne sont pas directement reliés au formatage final de votre document. Il y a quelques petites choses à savoir :

- **Un** retour chariot est considéré comme une espace.
- **Deux retours chariot ou plus** marquent la fin du paragraphe en cours, entraînent donc un nouveau paragraphe avec un alinéa.

On peut donc sauter autant de lignes que l'on veut dans le texte, cela n'a aucun effet supplémentaire. Un passage à la ligne peut être forcé par `\\` ou par `\newline` mais, dans ce cas, la première ligne du nouveau paragraphe n'aura pas d'alinéa. `\\*` empêche un saut de page après le saut de ligne demandé. La commande `\par` permet de commencer un nouveau paragraphe en laissant une espace verticale plus importante. Ce nouveau paragraphe commence avec un alinéa.

:::{note}
La commande `\\` peut prendre comme paramètre une longueur `\\[6ex]` pour augmenter localement un interligne.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,fin de ligne,saut de ligne,paragraphes,fin de paragraphe
```

