# Comment bien gérer une URL s'étendant sur deux lignes ?

Lorsque vous utilisez l'extension {ctanpkg}`hyperref`, vous transformez parfois un bloc de texte pour en faire un hyperlien « actif » (lorsque l'utilisateur clique sur ce texte, le programme de lecture ouvre la cible du lien). Cette extension utilise un pilote (à l'image de l'extension {ctanpkg}`graphics`) pour déterminer comment implémenter ce type de fonctionnalité.

Si vous utilisez le pilote `dvips` (pour gérer un document PostScript), les limitations dans la façon dont `dvips` traite les commandes `\special` impliquent que l'extension {ctanpkg}`hyperref` doit empêcher les liens de se rompre à la fin de la ligne de texte. Les autres pilotes (notamment ceux pour `pdfTeX` et pour `dvipdfm`) ne souffrent pas de ce problème.

Le problème peut se produire dans un certain nombre de circonstances. Dans quelques cas, il existe des solutions de contournement :

- si vous avez une URL qui forme un hyperlien actif. Dans ce cas, l'extension {ctanpkg}`hyperref` utilise l'extension {ctanpkg}`url` pour fractionner l'URL (comme décrit dans la question « {doc}`Comment gérer des adresses web (ou URL) ? </3_composition/texte/mots/mettre_en_forme_des_url_et_des_adresses_electroniques>` »), mais le pilote `dvips` supprime alors ce fractionnement. La solution est donnée par l'extension {ctanpkg}`breakurl` qui modifie la commande `\url` pour qu'elle scinde l'URL en plusieurs morceaux, entre lesquels un saut de ligne est autorisé. Chaque groupe de morceaux, qui se termine sur une seule ligne, est converti en un seul lien cliquable ;
- si vous avez une table des matières, une liste de figures ou de tableaux, ou autre construction similaire, l'extension {ctanpkg}`hyperref` rendra normalement les titres y figurant actifs. Si le titre est trop long, il devra faire l'objet d'un retour à la ligne... que le pilote `dvips` l'empêchera. Dans ce cas, chargez {ctanpkg}`hyperref` avec l'option `linktocpage`, et seul le numéro de page sera rendu actif.

Sinon, si vous avez un long morceau de texte que vous souhaitez activer, vous n'avez actuellement pas de solution simple : vous devez réécrire votre texte ou utiliser un autre mécanisme de génération de PDF.

```{eval-rst}
.. todo:: // : le contenu de cette page est à valider. Il faut voir s'il faut parler ici de l'option breaklinks et de l'option hypertex (+dvips). //
```

______________________________________________________________________

*Source :* {faquk}`Link text doesn't break at end line <FAQ-breaklinks>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```

