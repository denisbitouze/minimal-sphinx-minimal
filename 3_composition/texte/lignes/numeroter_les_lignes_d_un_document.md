# Comment numéroter les lignes d'un document ?

## Dans du texte classique

### Avec l'extension « lineno »

L'extension {ctanpkg}`lineno` permet d'obtenir cette numérotation en modifiant la routine de sortie LaTeX. Il suffit d'insérer la commande `\linenumbers` là où on souhaite activer la numérotation. Cette méthode marche raisonnablement bien, y compris quand la taille de caractère et l'interligne sont modifiés (par exemple pour les titres), mais n'est pas sans faille ; l'utilisateur doit donc rester vigilant.

```latex
\documentclass{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[width=6.6cm,height=8cm]{geometry}
  \usepackage{lmodern}
  \usepackage{lineno}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \pagestyle{empty}

\title{À la recherche\dots}
\author{M. P.}
\date{1913}

\begin{document}
\linenumbers
\maketitle

\section{Le coucher}
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte,
mes yeux se fermaient si vite que je n'avais pas le temps de me dire :
\og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher
le sommeil m'éveillait\dots
% ; je voulais poser le volume que je croyais avoir encore
%dans les mains et souffler ma lumière.
\end{document}
```

Si on souhaite numéroter les lignes d'une partie du document seulement, on peut utiliser l'environnement `linenumbers` :

```latex
% !TEX noedit
\begin{linenumbers}
Longtemps, je me suis couché
de bonne heure...
\end{linenumbers}
```

Par défaut, la numérotation se poursuit tout le long du document, mais il est possible de la faire revenir à `1` à chaque nouvelle page, avec l'option `pagewise`.

Quelques autres options de l'extension :

- `left` pour afficher les numéros dans la marge de gauche (comportement par défaut),
- `right` pour afficher les numéros dans la marge de droite,
- `switch` pour afficher les numéros dans la marge extérieure,
- `modulo` pour numéroter toutes les cinq lignes.

La {texdoc}`documentation de « lineno » <lineno>` propose de nombreuses autres options de configuration.

Si vous placez une étiquette dans votre document avec `\linelabel{ICI}`, vous pourrez faire référence à son **numéro de ligne** avec `\ref{ICI}`.

:::{tip}
Cette fonctionnalité peut être étendue aux notes de bas de page avec l'extension {ctanpkg}`fnlineno` :

```latex
\documentclass{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[width=6.6cm,height=6.0cm]{geometry}
  \usepackage{lmodern}
  \usepackage{lineno,fnlineno}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \pagestyle{empty}

\title{À la recherche\dots}
\author{M. P.}
\date{1913}

\begin{document}
\linenumbers

\section{Le coucher}
Longtemps, je me suis couché de bonne heure\footnote{Vers 18h ou 19h,
rarement plus tard sauf en cas d'impératif mondain.}.
Parfois, à peine ma bougie éteinte, mes yeux se fermaient
si vite que je n'avais pas le temps de me dire :
\og{}Je m'endors.\fg{} Et, une demi-heure après,
la pensée qu'il était temps de chercher le sommeil m'éveillait ;
je voulais poser le volume que je croyais avoir encore
dans les mains et souffler ma lumière.
\end{document}
```
:::

### Avec l'extension « reledmac »

Les éditions critiques de textes font souvent appel à la numérotation des lignes de texte. Pour ce type de travaux, l'extension {ctanpkg}`reledmac` offre une solution complète. Cette extension remplace {ctanpkg}`edmac` et {ctanpkg}`ledmac`, devenues {doc}`obsolètes </1_generalites/histoire/liste_des_packages_obsoletes>`.

### Avec l'extension « vruler »

L'extension {ctanpkg}`vruler` évite de nombreux problèmes associés à la numérotation des lignes, en permettant de placer sur les pages une règle rectangulaire contenant la numérotation. L'effet est tout à fait correct lorsqu'il est appliqué à un texte d'apparence uniforme, mais il reste médiocre dans les textes qui impliquent des ruptures telles que les mathématiques interpolées ou les figures.

### Avec l'extension « numline »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`numline` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`numline`, à l'image de {ctanpkg}`lineno`, retraite également la routine de sortie. L'utilisateur doit donc rester prudent.

## Dans du texte en mode verbatim

### Avec les extensions « moreverb » ou « fancyvrb »

Dans ce cas particulier, les extensions {ctanpkg}`moreverb` ou {ctanpkg}`fancyvrb`, dédiées au mode verbatim, peuvent être utilisées. Voir aussi sur ce sujet « {doc}`Comment inclure un fichier en mode verbatim ? </4_domaines_specialises/informatique/inclure_un_fichier_en_mode_verbatim>` ».

### Avec la classe « memoir »

La classe {ctanpkg}`memoir` fournit également les fonctionnalités nécessaires.

______________________________________________________________________

*Source :* {faquk}`Including line numbers in typeset output <FAQ-linenos>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,ajouter les numéros de lignes,mode brouillon,numéros de lignes dans la marge
```

