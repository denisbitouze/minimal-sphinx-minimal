# Pourquoi cette ligne commence-t-elle mal ?

Cette réponse concerne deux types de problèmes :

- ceux où un astérisque au début d'une ligne ne parvient mystérieusement pas à apparaître dans le document final ;
- ceux générant une erreur de la forme :

```text
! Missing number, treated as zero.
<to be read again>
                   p
<*> [perhaps]
```

Les deux problèmes surviennent car la commande `\\` prend des arguments optionnels :

- la commande `\\*` signifie « couper la ligne ici et empêcher le saut de page après le saut de ligne » ;
- la commande `\\[⟨dimension⟩]` signifie « couper la ligne ici et ajouter un espace vertical de taille ⟨dimension⟩ à la suite ».

Et ces problèmes arrivent parce que la commande `\\` recherche le prochain élément non vide. Cette recherche ignore la fin de la ligne dans votre texte, de sorte que `\\` en vient à imaginer que vous lui donnez un « modificateur ».

## Dans le texte

Une solution évidente est de mettre l'élément en début de la nouvelle ligne entre accolades. Par exemple :

```latex
% !TEX noedit
{\ttfamily
  /* Commentaire en langage C\\
  {[Important]} à traiter\\
  {*}/
}
```

Cet exemple particulier pourrait être codé (sans aucun problème) en mode verbatim, mais ce mode déroute souvent les utilisateurs.

## Dans les mathématiques

Ce problème apparaît également en mode mathématique, dans les tableaux et autres éléments demandant la présence de la commande `\\`. Dans ce cas, la mise entre accolades à grande échelle des choses n'est *pas* une bonne idée. En lieu et place, la primitive `\relax` (qui ne fait que bloquer les recherches de cette nature) peut être utilisée. Voici un exemple :

```latex
% !TEX noedit
\begin{eqnarray}
  [a]       &=& b  \\
  \relax[a] &=& b
\end{eqnarray}
```

Cet exemple n'est toutefois pas recommandé par cette FAQ, comme indiqué à la question « {doc}`Quels sont les arguments contre l'utilisation d'« eqnarray » ? </4_domaines_specialises/mathematiques/equations/arguments_contre_eqnarray>` ».

Notez que l'extension {ctanpkg}`amsmath` modifie le comportement de la commande `\\` en mode mathématique : dans l'exemple ci-dessus, il n'y a plus besoin d'action spéciale (`\relax` ou les accolades).

______________________________________________________________________

*Source :* {faquk}`Start of line goes awry <FAQ-newlineargs>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreur,eqnarray,début de ligne,\\\\,saut de ligne
```

