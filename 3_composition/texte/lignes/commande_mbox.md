# À quoi sert la commande « `\mbox` » ?

Cette commande permet à LaTeX de considérer un ensemble de caractères, commandes, etc., comme une seule entité, et ainsi de ne pas la couper. C'est intéressant, par exemple, pour empêcher un saut de ligne indésirable, comme dans un numéro de téléphone :

```latex
% !TEX noedit
Pour votre information, mon numéro de téléphone est désormais le
\mbox{01 02 03 04 05}. Il remplace mon ancien numéro de téléphone
qui ne doit maintenant plus être utilisé.
```

```latex
Pour votre information, mon numéro de téléphone est désormais le
\mbox{01 02 03 04 05}. Il remplace mon ancien numéro de téléphone
qui ne doit maintenant plus être utilisé.
```

Une autre idée est d'utiliser l'espace insécable `~`, ce qui donne un résultat à l'espacement légèrement différent. Dans l'exemple suivant, l'espacement est moins important entre les composantes du numéro de téléphone :

```latex
% !TEX noedit
Pour votre information, mon numéro de téléphone est désormais le
01~02~03~04~05. Il remplace mon ancien numéro de téléphone qui ne
doit maintenant plus être utilisé.
```

```latex
Pour votre information, mon numéro de téléphone est désormais le
01~02~03~04~05. Il remplace mon ancien numéro de téléphone qui ne
doit maintenant plus être utilisé.
```

Il convient de faire attention à ces méthodes qui peuvent entraîner l'apparition de lignes empiétant dans les marges (comme c'est le cas dans les exemples de cette page pour la ligne donnant le numéro de téléphone).

```{eval-rst}
.. meta::
   :keywords: LaTeX,empêcher les coupures de ligne,bloc de texte,empêcher les sauts de ligne,espaces insécables
```

