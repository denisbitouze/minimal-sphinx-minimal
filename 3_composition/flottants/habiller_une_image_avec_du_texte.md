# Comment habiller une image ou une citation avec du texte ?

```{eval-rst}
.. todo:: Le contenu de cette page a besoin d'une remise à jour, les choses ayant bien évolué depuis l'article de Piet van Oostrum de 1996.
```

Il existe plusieurs extensions LaTeX qui prétendent faire cela, mais elles ont tous leurs limites car le moteur TeX lui-même n'est pas vraiment conçu pour résoudre ce genre de problème. Piet van Oostrum a [recensé les extensions disponibles](https://www.ntg.nl/maps/16/29.pdf) en 1996 et a publié ses conclusions dans [Maps, le journal du groupe néerlandais des utilisateurs de TeX](https://www.ntg.nl/maps.html), [NTG](https://www.ntg.nl/indexe.html).

Un tableau récapitulatif en bas de cette page propose également d'autres solutions.

## Avec l'extension « floatflt »

L'extension {ctanpkg}`floatflt` est une version améliorée (pour LaTeX2e) de l'antique `floatfig.sty`. Elle définit des environnements `floatingfigure` et `floatingtable`, qui permettent d'entourer la figure ou le tableau (respectivement) de texte. Sa syntaxe est :

```latex
% !TEX noedit
\begin{floatingfigure}[options]{largeur de la figure}
 % Contenu de la figure
\end{floatingfigure}
```

Les tableaux ou figures peuvent être placés à gauche ou à droite, ou alternativement sur les pages paires ou impaires d'un document recto-verso.

L'extension est compatible avec l'extension {ctanpkg}`multicol`, mais ne fonctionne pas bien dans le voisinage des environnements de listes.

Exemple complet :

```latex
\documentclass[french]{report}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{floatflt}
\usepackage{babel}

\pagestyle{empty}

\begin{document}
\begin{floatingtable}[l]{%
 \begin{tabular}{|p{3cm}|p{3cm}|}
  \hline
  package \texttt{floatfig} &
  package \texttt{floatflt} \\
  \hline
  gère uniquement les figures &
  gère les figures et les tableaux \\
  \hline
  place toujours le flottant à droite sur
  une page impaire, et à gauche sur une page
  paire &
  permet de placer le flottant à droite, à
  gauche, ou en fonction de la parité de la
  page \\
  \hline
  non utilisable en mode multicolonne &
  compatible avec le package
  \texttt{multicol} \\
  \hline
 \end{tabular}}
 \caption{Un tableau}\label{letableau}
\end{floatingtable}
Le package \texttt{floatflt} permet de choisir
le placement de la figure : à gauche (\texttt{[l]}),
à droite (\texttt{[r]}), ou \og{}à
l'extérieur\fg{} (\texttt{[p]}) pour les
documents recto-verso.
Au vu du tableau ci-contre, le package
\texttt{floatflt} devrait toujours être
préféré à son prédécesseur, sauf si
c'est pour utiliser dans un environnement de
listes. Il faut noter également que
l'environnement \texttt{floatingtable} a une
structure  différente de celle de l'environnement
\texttt{floatingfigure} : on ne spécifie pas
la longueur du flottant, mais on passe tout le
tableau en argument. Le flottant est adapté à
la longueur du tableau.
\end{document}
```

## Avec l'extension « wrapfig »

{ctanpkg}`Wrapfig <wrapfig>` a pour syntaxe :

```latex
% !TEX noedit
\begin{wrapfigure}[hauteur de la figure en nombre de lignes]{l,r,...}[décalage]{largeur}
  % Figure, légende, etc.
\end{wrapfigure}
```

Il existe un environnement `wraptable`, pour les tableaux, de syntaxe est similaire.

La hauteur peut être omise, auquel cas elle sera calculée à partir de la taille de la figure ; l'environnement utilisera la plus grande des deux valeurs suivantes : la largeur spécifiée ou la largeur réelle. Le paramètre `{l,r,`etc.`}` peut également prendre la valeur `i` (pour *inside*, intérieur) ou `o` (pour *outside*, extérieur) pour les documents recto-verso, et les majuscules `I` et `O` peuvent être utilisées pour indiquer que l'image doit flotter. Le débordement permet de décaler la figure dans la marge. La figure ou le tableau apparaîtra dans la liste des figures ou des tableaux si vous utilisez la commande `\caption`.

Les environnements ne fonctionnent pas dans les environnements de liste qui se terminent avant la fin de la figure ou du tableau, mais ils peuvent être utilisés dans une `\parbox` ou une `minipage`, et dans une mise en page en deux colonnes.

Exemple complet :

> {ctanpkg}`wrapfig` est une autre variante permettant de fondre une image dans le texte. Il est possible de faire dépasser la figure (ou le tableau) dans la marge, par exemple :

```latex
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{wrapfig}
\usepackage{array}
\usepackage{pifont}
\usepackage{babel}


\pagestyle{empty}

\begin{document}
\section{Fondre une image ou un tableau dans le texte}
\begin{wraptable}{r}[1cm]{8.5cm}
\newcommand{\p}{\ding{51}}
\newcommand{\m}{\ding{55}}
\newcommand\w{}
\newcommand\h{h}
\begin{tabular}{|l|*{11}{c|}}
\cline{2-11}
\multicolumn{1}{c|}{} &
 A & B & C & D & E & F & G & H & I & J \\ \hline
{\ttfamily picinpar} &
\p &\p &\p &\w &\p &\w &\p & \w& \w& \w\\ \hline
{\ttfamily picins} &
\p &\m &\p &\p &\w &\w &\p & \p& \w& \w\\ \hline
{\ttfamily floatfig} &
\p &\m &\m &\p &\w &\w &\w & \w& \m& \w\\ \hline
{\ttfamily floatflt} &
\p &\p &\p &\p &\w &\w &\w & \m& \w& \p\\ \hline
{\ttfamily wrapfig} &
\p &\p &\p &\p &\w &\w &\h & \m& \p& \w\\ \hline
{\ttfamily window} &
\m &\m &\p &\w &\p &\p &\p & \w& \w& \w\\ \hline
{\ttfamily flow} &
\m &\m &\p &\w &\w &\w &\p & \w& \w& \w\\ \hline
\end{tabular}
\caption{Les différents packages permettant de
fondre une image dans le texte}\label{tab}
\end{wraptable}
Le tableau ci-contre contient l'ensemble des
résultats d'un test comparant les différents
packages permettant de fondre un tableau ou une
image dans un paragraphe. Ce test a été
effectué par Piet \textsc{Van Oostrum}. Avant de commencer
à décrire la signification de chaque colonne,
je précise que le package \texttt{wrapfig}
n'est pas compatible avec un environnement de type
liste. En fait, je dis cela juste pour gagner un
peu de place pour habiller joliment mon tableau.
La signification de chaque colonne est donnée plus bas :

\end{document}
```

## Avec l'extension « picins »

L'extension {ctanpkg}`picins` fait partie d'un vaste ensemble qui permet l'inclusion d'images (par exemple, avec une ombre sous l'image, ou sous divers formats MS-DOS, etc.). La commande pour insérer une image au début d'un paragraphe est la suivante :

```latex
% !TEX noedit
\parpic(largeur,hauteur)(décalage en horizontal,décalage vertical)[Options][Position]{Image}
 % Texte du paragraphe
```

Cette extension est la seule permettant de fondre une image au milieu d'un environnement de liste. Elle permet également d'encadrer la figure par un rectangle, un ovale, une ombre, un rectangle pointillé, et on peut lui ajouter une légende qui sera incluse dans la liste des figures. Par contre, elle ne gère que les figures.

Tous les paramètres, sauf l'image elle-même, sont facultatifs. L'image peut être positionnée à gauche ou à droite, ou encadrée.

Malheureusement (pour ceux qui ne parlent pas allemand), [la documentation est en allemand](http://mirrors.ctan.org/macros/latex209/contrib/picins/mpic.dvi). Piet van Oostrum en a écrit [un résumé en anglais](http://mirrors.ctan.org/macros/latex209/contrib/picins/picins.txt).

Exemple :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \usepackage{picins}
  \pagestyle{empty}

\begin{document}
\pichskip{1cm}
Voici les avantages du package \texttt{picins} :

\begin{itemize}
\item il permet de placer des figures dans des
environnements de type \og{}liste\fg{}, contrairement
aux autres packages du même genre. Il faut
cependant placer la commande \verb+\parpic+ à
l'intérieur de l'environnement liste pour que
cela marche ;%
\parpic[ro]{\Huge F.A.Q.\ \LaTeX{}
\vrule height 2.5ex depth 1ex width 0pt
% Le \vrule, c'est pour espacer un peu le cadre
}%
\item il s'utilise de façon très simple, et
calcule le nombre de lignes à rétrécir ;
\item il permet, de manière très simple,
d'encadrer la figure. Il est également possible
de placer une légende au-dessous ou à côté
de la figure, dans ou à l'extérieur de
l'éventuel cadre.
\end{itemize}
\end{document}
```

## Avec l'extension « picinpar »

Le package {ctanpkg}`picinpar` permet encore plus de choses : en particulier, mettre du texte tout autour du flottant, pas uniquement à droite ou à gauche et pas uniquement au-dessous. Techniquement, c'est très beau, pratiquement, c'est assez difficile à lire. Les environnements que propose ce package s'appellent `figwindow` et `tabwindow`, qui prennent en argument optionnel le nombre de lignes de texte qui doivent précéder le flottant, le placement horizontal du flottant (`l`, `c`, `r`), le contenu du flottant, et la légende. Ensuite vient le texte du paragraphe. L'exemple suivant montre comment tout cela fonctionne. Noter que ce package est incompatible avec AmSLaTeX.

:::{important}
{ctanpkg}`Picinpar <picinpar>` est considéré comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`.

Il a été développé pour LaTeX 2.09. Il se trouve qu'il fonctionne encore actuellement avec `pdflatex` mais plus avec `lualatex` (voir [cet exemple](https://faq.gutenberg.eu.org/3_composition/flottants/faire_couler_le_texte_autour_d_une_figure?rev=1624906214)).
:::

## Avec l'extension « cutwin »

Toutes les solutions ci-dessus traitent des insertions dans les marges; elles sont capables de tirer parti de la primitive TeX `\parshape` qui permet d'ajuster les marges du texte d'un paragraphe, ligne par ligne (Knuth fournit un exemple d'une telle utilisation, avec du texte inscrit dans un cercle, à moitié dans la marge, au chapitre 14 du TeX​[book](/1_generalites/documentation/livres/documents_sur_tex)). Insérer une image au milieu d'un paragraphe est beaucoup plus délicat... L'extension {ctanpkg}`cutwin` est faite pour ça, comme son nom l'indique, elle « découpe une fenêtre » dans le texte. Elle requiert un ensemble de « largeurs de ligne partielles » (deux par ligne), et compose la section découpée du paragraphe ligne par ligne. Les exemples dans la documentation du paquetage sont séduisants.

## Avec l'extension « pullquote »

Une autre solution est proposée par [Ludovic Vimont](https://borntocode.fr/latex-citation-et-habillage-carre-pull-quotes/), avec le package [pullquote](http://bazaar.launchpad.net/~tex-sx/tex-sx/development/view/head:/pullquote.dtx) (non disponible sur CTAN) :

```latex
% !TEX noedit
\def\happy {
  \begin{tikzpicture}
    \clip (0,0) circle (2.7cm);
    \node (0,0) {\includegraphics[width=6cm]{happy2.jpg}};
  \end{tikzpicture}
}

...

\begin{pullquote}{shape=circular,object=\happy}
  \lipsum[1-3]
\end{pullquote}
```

## Avec l'extension « figflow » (pour Plain Tex uniquement)

Les utilisateurs de Plain TeX n'ont qu'une seule possibilité à leur disposition : {ctanpkg}`figflow` (qui ne fonctionne pas avec LaTeX). {ctanpkg}`Figflow <figflow>` ne sait insérer les figures qu'en début de paragraphe, mais il semble parfaitement fonctionnel. Sa syntaxe est

```latex
% !TEX noedit
\figflow{⟨largeur⟩}{⟨hauteur⟩}{⟨figure⟩}
```

:::{important}
L'utilisateur doit veiller à ce que les dimensions soient correctes et que la figure tienne sur une page.
:::

## Tableau récapitulatif

| \\                                                       | A           | B           | C         | D        | E   | F   | G      | H    | I   | J   |
| -------------------------------------------------------- | ----------- | ----------- | --------- | -------- | --- | --- | ------ | ---- | --- | --- |
| `picins` `floatfig` `floatflt` `wrapfig` `window` `flow` | ✓ ✓ ✓ ✓ ✕ ✕ | ✕ ✕ ✓ ✓ ✕ ✕ | ✓ ✕ ✓ ✓ ✓ | ✓ ✓ ✓ ✓✓ | ✓   | ✓   | ✓h ✓ ✓ | ✓✕ ✕ | ✕✓  | ✓   |

- **A** gestion des figures (légende, compteur, liste)
- **B** gestion des tableaux (légende, compteur, liste)
- **C** possibilité d'aligner le flottant à gauche ou à droite
- **D** possibilité d'aligner le flottant à l'extérieur, dans le cas d'un document recto-verso
- **E** possibilité de mettre du texte des deux côtés du flottant
- **F** possibilité de placer le flottant après le début du paragraphe
- **G** calcul automatique de la taille (pour `wrapfig`, seule la hauteur peut être calculée)
- **H** compatible avec un environnement de liste
- **I** fonctionne avec `twocolumn`
- **J** fonctionne avec `multicol`

______________________________________________________________________

*Sources :*

- {faquk}`Flowing text around figures <FAQ-textflow>`,
- [Citation et habillage de texte (pull quotes)](https://borntocode.fr/latex-citation-et-habillage-carre-pull-quotes/),
- [Équivalent de « float » en HTML](https://www.mathematex.fr/viewtopic.php?t=15622),
- [How to layout irregular paragraph shape?](https://tex.stackexchange.com/questions/32226/how-to-layout-irregular-paragraph-shape)
- [How to wrap text around part of a figure?](https://tex.stackexchange.com/questions/473191/how-to-wrap-text-around-part-of-a-figure)

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,flottants,mise en page,texte autour d'une figure,intégrer une image au texte,contour d'une image,habillage d'une figure,habillage d'une citation
```

