# Comment centrer une figure ou une table très large ?

Pour centrer une figure ou un tableau, la méthode classique consiste à inclure la commande `\centering` au début du code de ce flottant. Mais cela ne donne pas des résultats corrects si l'objet est plus large que la largeur du texte (valeur de `\textwidth`). En effet, dans ce cas, le flottant va déborder dans la marge droite (ce qui est en fait doublement insatisfaisant car, en plus d'être visuellement mal placé, le flottant ne sera pas placé avant la prochaine commande `\clearpage` ou similaire). Voici différentes corrections possibles selon la dimension du flottant.

## Avec un redimensionnement du flottant

Vous pouvez éviter ce problème en redimensionnant la figure ou le tableau pour l'adapter, mais cela n'est souvent pas satisfaisant pour de multiples raisons (souvent la lisibilité du flottant lui-même).

## Avec une rotation du flottant

Aussi, si l'objet est plus large que la zone imprimable de la page, vous n'avez d'autre choix que de [le faire pivoter](/3_composition/texte/pages/changer_l_orientation_d_un_document).

## Avec la commande \\makebox

Cependant, si l'objet est *juste* plus large que le zonec de texte, vous pouvez faire semblant d'avoir la bonne taille en utilisant ce code :

```latex
% !TEX noedit
\begin{figure}
  \noindent
  \makebox[\textwidth]{\includegraphics{ma-figure-large}}%
  \caption{Cette figure déborde dans les deux marges.}
\end{figure}
```

Notez ici la présence de `\noindent` : comme la commande `\makebox` débute un nouveau paragraphe, elle évite qu'il soit indenté avec `\parindent`.

______________________________________________________________________

*Source :* {faquk}`Centring a very wide figure or table <FAQ-wideflt>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,flottants,tables,centrer
```

