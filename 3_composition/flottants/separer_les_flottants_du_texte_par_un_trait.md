# Comment isoler les flottants par un filet horizontal ?

## Avec les commandes de base

Les commandes `\topfigrule` et `\botfigrule` permettent de séparer des flottants envoyés en haut ou en bas d'une page, respectivement, du reste du texte. Ainsi, si deux flottants d'une page sont envoyés en haut de la page suivante, le deuxième sera séparé du reste de la page par le contenu de la commande `\topfigure`.

```latex
% !TEX noedit
\let\topfigrule\hrule
\let\botfigrule\hrule
```

On peut aussi vouloir ajouter un peu d'espace au-dessus et au-dessous, auquel cas on écrira par exemple :

```latex
% !TEX noedit
\newcommand{\topfigrule}{%
  \vspace*{3pt}%
  \noindent\rule{\linewidth}{0.4pt}%
  \vspace{-3.4pt}%
}

\newcommand{\botfigrule}{%
  \vspace*{-3pt}%
  \noindent\rule{\linewidth}{0.4pt}%
  \vspace{2.6pt}%
}
```

## Avec l'extension « float »

L'extension {ctanpkg}`float`, mentionné à la question « {doc}`Comment définir de nouveaux flottants? </3_composition/flottants/definir_de_nouveaux_flottants>` », propose, entre autres, un style `ruled` permettant d'entourer les flottants d'un trait horizontal.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,mise en forme des images,séparer les tableaux du texte,ajouter un filet autour des flottants
```

