# Comment mettre des flottants en bas de page en mode deux colonnes ?

Vous avez spécifié l'option de placement `[htbp]` pour votre figure ou votre tableau pleine largeur, dans votre document en deux colonnes, mais ils sont toujours placés en haut de la page... Hélas, cela correspond à ce que dit la documentation : LaTeX, sans extension spécifique, n'autorise que les flottants pleine largeur en haut d'une page, ou sur une page remplie de flottants.

## Avec les commandes de base

L'article de Barbara Beeton [Placing a full-width insert at the bottom of two columns](https://tug.org/TUGboat/tb35-3/tb111beet-banner.pdf), publié dans *TUGboat* **35**(3) en 2014, propose une méthode manuelle pour atteindre le résultat souhaité : elle utilise une valeur négative dans la commande `\enlargethispage` (servant d'ordinaire à agrandir la page courante). Cet article, en anglais, est accompagné d'un exemple de code.

## Avec l'extension « nidanfloat »

L'extension {ctanpkg}`nidanfloat` permet d'utiliser l'option `[b]` pour une figure pleine largeur dans un document à deux colonnes, même sur la première page du document. Cette extension faisait initialement partie de {doc}`la suite pLaTeX </1_generalites/glossaire/ptex_et_uptex>`, dédiée à la composition du japonais. Elle est maintenant distribuée séparément car elle supporte les autres formats LaTeX. Son nom japonais est resté, *nidan-kumi* ([段組](https://ja.wikipedia.org/wiki/%E6%AE%B5%E7%B5%84)) signifiant « deux colonnes ».

:::{note}
La documentation fournie avec l'extension {ctanpkg}`nidanfloat` est en japonais, {texdoc}`mais il existe une traduction en anglais <nidanfloat-en>`.
:::

## Avec l'extension « stfloats » ou l'extension « dblfloatfix »

L'extension {ctanpkg}`stfloats`, comme l'extension « dblfloatfix » qui utilise du code de {ctanpkg}`stfloats`, améliore aussi un peu la situation et fait en sorte que LaTeX honore également l'option de placement `[b]`.

Un problème particulier avec {ctanpkg}`stfloats` et {ctanpkg}`dblfloatfix` est que le flottant apparaîtra au plus tôt sur la page suivant celle où il est spécifié. Cela a deux effets secondaires indésirables :

- il ne peut pas y avoir de flottant en bas de la première page d'un document ;
- les numéros de flottants peuvent être « mélangés » (en particulier si vous utilisez {ctanpkg}`dblfloatfix`, qui garantit que les flottants de bas de page déjà spécifiés apparaissent *avant* tout flottant simple-colonne).

Avant la version 2015 de LaTeX, les flottants en double et simple colonne étaient stockés dans des listes distinctes et pouvaient être affichés dans le mauvais ordre. Pour ces anciennes versions, l'extension {ctanpkg}`dblfloatfix` combinait les effets de {ctanpkg}`stfloats` et les {doc}`corrections à ce problème </3_composition/flottants/flottants_dans_le_desordre_en_mode_deux_colonnes>` implémentées dans {ctanpkg}`fixltx2e`.

## Complément

L'équipe de la FAQ n'a pas connaissance d'une extension permettant à LaTeX d'honorer l'option de placement `[h]` pour les flottants en double colonne, mais {ctanpkg}`midfloat` peut être utilisée pour s'approcher de ce résultat.

______________________________________________________________________

*Source :* {faquk}`Placing two-column floats at bottom of page <FAQ-2colfloat>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,placement des flottants en mode deux colonnes,placement des figures en mode deux-colonnes,document en colonnes
```

