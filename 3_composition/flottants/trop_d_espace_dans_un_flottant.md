# Comment supprimer l'espace en trop autour des flottants ?

Vous vous demandez peut-être pourquoi l'espace vertical supplémentaire est apparu dans vos environnements de flottants (`figure` ou `table`) ? Peut-être cet espace est-il arrivé là alors que vous cherchiez à *améliorer le rendu de votre document*, et vous ne vous en êtes pas rendu compte immédiatement...

Le problème vient du fait que l'environnement `center` (et ses copains `flushleft` et `flushright`) est implémenté à partir du code qui met en forme les listes de LaTeX (environnement `itemize`), or les listes sont toujours séparées de ce qui les entoure par un peu d'espace.

Si des espaces verticaux injustifiés sont apparus, c'est probablement que vous avez écrit quelque chose comme :

```latex
% !TEX noedit
\begin{figure}
 \begin{center}
   \includegraphics{...}
   \caption{...}
 \end{center}
\end{figure}
```

ou pire encore :

```latex
% !TEX noedit
\begin{figure}
 \begin{center}
   \includegraphics{...}
 \end{center}
 \caption{...}
\end{figure}
```

La solution consiste à laisser le flottant et les objets qu'il contient se positionner eux-mêmes, et à utiliser des commandes de mise en page « génériques » plutôt que leurs versions basées sur des listes :

```latex
% !TEX noedit
\begin{figure}
  \centering
  \includegraphics{...}
  \caption{...}
\end{figure}
```

(ce qui est encore plus rapide à écrire).

Ce dernier code fonctionnera avec toutes les extensions LaTeX, à l'exception de celles antérieures à {doc}`LaTeX2e </1_generalites/glossaire/qu_est_ce_que_latex2e>` (et donc {doc}`obsolètes </1_generalites/histoire/liste_des_packages_obsoletes>`), telles que {ctanpkg}`psfig` ou {ctanpkg}`epsf` --- voir la question « [Comment insérer une image ?](/3_composition/illustrations/inclure_une_image/inclure_une_image) » pour plus de détails sur la genèse de la commande `\includegraphics`.

## Et si ça ne suffit pas ?

Une fois que vous aurez retiré les espaces ajoutés par erreur avec `\begin{center}`...`\end{center}`, vous voudrez peut-être jouer sur les nombreux paramètres disponibles :

```{eval-rst}
.. todo:: Lister les principaux paramètres d'espacement autour des flottants :
```

- [Remove space after figure and before text](https://tex.stackexchange.com/questions/60477/remove-space-after-figure-and-before-text).

______________________________________________________________________

*Sources :*

- {faquk}`Extra vertical space in floats <FAQ-vertspacefloat>`,
- [Space between caption and table in TeX](https://tex.stackexchange.com/questions/292859/space-between-caption-and-table-in-tex).

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,espace blanc avant une image,espace blanc dans un flottant,espace blanc avant un tableau
```

