# Comment définir de nouveaux flottants ?

## Avec l'extension « float »

L'extension {ctanpkg}`float` permet de définir de nouveaux types de flottants. Plusieurs styles sont possibles (flottants encadrés, séparés du texte par des lignes...). Une liste des flottants de chaque type sera créée en cas de besoin... Cet exemple présente tout cela.

```latex
% !TEX noedit
\documentclass[11pt,french]{article}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{babel}

%\floatstyle{ruled} % cette commande permet par exemple que les flottants soient séparés du texte par des lignes.
\newfloat{maxime}{tbp}{lom}[section]
% 'maxime' est le nom du nouvel environnement
% 'tbp' sont les options de placement de ce flottant
% 'lom' est l'extension du fichier qui sera utilisé pour construire la liste des flottants
% 'section' est le niveau duquel dependra la numérotation des flottants
\floatname{maxime}{Maxime} % Titre de l'environnement

\begin{document}
\listof{maxime}{Adages, aphorismes et apophtegmes} % Titre de la liste des flottants.

\section{Règles de bases}
\subsection{Hommes célèbres}

Parmi les citations des hommes célèbres dans le domaine que nous étudions actuellement, il faut
retenir celle de M.~Maxime rappelée dans le cadre~\ref{max-max}.

\begin{maxime}
  M.~Maxime a dit un jour :
  \begin{quote} Chacun doit se faire ses propres raisons. \end{quote}
  \caption{Adage de M.~Maxime.}\label{max-max}
\end{maxime}
\end{document}
```

## Avec l'extension « newfloat »

L'extension {ctanpkg}`newfloat` propose également un mécanisme de création de flottant avec des options un peu plus fines. Voici une déclaration à peu près équivalente à celle ci-dessus :

```latex
% !TEX noedit
\documentclass[11pt,french]{article}
\usepackage[T1]{fontenc}
\usepackage{newfloat}
\usepackage{babel}

\DeclareFloatingEnvironment[%
  fileext=lom,                                  % extension du fichier stockant la liste de ces flottants
  listname={Adages, aphorismes et apophtegmes}, % titre de la liste de ces flottants
  name=Maxime,                                  % nom du flottant dans la légende
  placement=tbp,                                % balises de positionnement du flottant
  within=section,                               % réinitialisation du compteur à chaque chapitre
]{maxime}

\begin{document}
\listofmaxime

\section{Règles de bases}
\subsection{Hommes célèbres}

Parmi les citations des hommes célèbres dans le domaine que nous étudions actuellement, il faut
retenir celle de M.~Maxime rappelée dans le cadre~\ref{max-max}.

\begin{maxime}
  M.~Maxime a dit un jour :
  \begin{quote} Chacun doit se faire ses propres raisons. \end{quote}
  \caption{Adage de M.~Maxime.}\label{max-max}
\end{maxime}
\end{document}
```

## Avec l'extension « floatrow »

L'extension {ctanpkg}`floatrow` correspond un développement des possibilités proposées par l'extension {ctanpkg}`float` (qui est d'ailleurs appelée). Elle propose une mécanique de création de flottants très proche de celle de l'extension {ctanpkg}`float`. L'exemple ci-dessous montre que certaines commandes sont communes :

```latex
% !TEX noedit
\documentclass[11pt,french]{article}
\usepackage[T1]{fontenc}
\usepackage{floatrow}
\usepackage{babel}

\DeclareNewFloatType{maxime}%
{placement=tbp,within=section,fileext=lom}

\floatname{maxime}{Maxime} % Titre de l'environnement

\begin{document}
\listof{maxime}{Adages, aphorismes et apophtegmes} % Titre de la liste des flottants.

\section{Règles de bases}
\subsection{Hommes célèbres}

Parmi les citations des hommes célèbres dans le domaine que nous étudions actuellement, il faut
retenir celle de M.~Maxime rappelée dans le cadre~\ref{max-max}.

\begin{maxime}
  M.~Maxime a dit un jour :
  \begin{quote}
   Chacun doit se faire ses propres raisons.
  \end{quote}
  \caption{Adage de M.~Maxime.}\label{max-max}
\end{maxime}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,création de flottants
```

