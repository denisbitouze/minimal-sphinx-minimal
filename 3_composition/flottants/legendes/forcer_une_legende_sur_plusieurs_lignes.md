# Comment forcer un saut de ligne dans une légende ?

Si votre légende tient sur une seule ligne, elle sera centrée par LaTeX, et les commandes `\\` ou `\newline` n'auront pas d'effet pour insérer manuellement un saut de ligne. En revanche, si elle est plus longue qu'une ligne (sans saut de ligne manuel), elle sera composée comme un paragraphe et vous pouvez utilise `\\` à votre guise.

S'il est vraiment nécessaire d'introduire des sauts de lignes manuels dans une légende courte, l'extension {ctanpkg}`ccaption` permet d'utiliser la commande `\\` dans l'argument de `\caption`.

:::{important}
Attention : la commande `\\` est {doc}`fragile </2_programmation/syntaxe/c_est_quoi_la_protection>`, donc si vous l'utilisez dans du texte qui pourrait se retrouver dans une « Liste des figures » ou « Liste des tableaux », vous devez la protéger. Ainsi, vous écrirez :

```latex
% !TEX noedit
\caption{Titre avec un saut\protect\\ de ligne à la fois dans le corps du texte et dans la table}
\caption[Titre sans saut de ligne pour la table]{Titre avec\\ un saut de ligne dans le corps}
\caption[Titre avec un saut\protect\\ de ligne]{Titre comme vous voulez}
```
:::

Il reste en général conseillé de laisser LaTeX faire la mise en page, quitte a redéfinir la largeur que doit utiliser la légende (voir la question « [Comment modifier la commande « \\caption » ?](/3_composition/flottants/legendes/modifier_l_apparence_des_legendes) » à ce sujet).

______________________________________________________________________

*Sources :*

- [\\newline (or equivalent) inside a \\caption](https://latex.org/forum/viewtopic.php?t=3110),
- [How to add line break to caption without using caption package?](https://tex.stackexchange.com/questions/101595/how-to-add-line-break-to-caption-without-using-caption-package)

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légendes,retour à la ligne,saut de ligne dans une légende
```

