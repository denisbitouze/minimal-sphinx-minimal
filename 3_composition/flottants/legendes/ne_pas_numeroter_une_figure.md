# Comment obtenir une figure avec une légende non numérotée ?

## Avec l'extension « ccaption »

L'extension {ctanpkg}`ccaption` fournit une commande, nommée `\legend`, qui permet d'obtenir le résultat souhaité. Cette commande peut, par ailleurs, être utilisée en plus de la commande `\caption`, par exemple pour mettre une légende (numérotée) au-dessus et une autre, non-numérotée, au-dessous.

## Avec les commandes de base

Une autre solution consiste à placer les commandes suivantes dans le préambule du document (ou dans un fichier `sty`, auquel cas on enlèvera les commandes `\makeatletter` et `\makeatother` dans le code ci-dessous). Ces quelques lignes définissent une commande `\unnumberedcaption` basée sur la définition de la commande `\caption` :

```latex
% !TEX noedit
\makeatletter

\newcommand{\unnumberedcaption}%
{\@dblarg{\@unnumberedcaption\@captype}}

\newcommand{\@unnumberedcaption}{}%
\long\def\@unnumberedcaption#1[#2]#3{\par
  \addcontentsline{\csname ext@#1\endcsname}{#1}{%
    % à l'origine : \protect\numberline{\csname
    % the#1\endcsname}%
    % {\ignorespaces #2}
    \protect\numberline{}{\ignorespaces #2}%
    }%
  \begingroup
    \@parboxrestore
    \normalsize
    % à l'origine : \@makecaption{\csname
    % fnum@#1\endcsname}%
    % {\ignorespaces #3}\par
    \@makeunnumberedcaption{\ignorespaces #3}\par
  \endgroup}

% redéfinit \@makeunnumberedcaption
% (comme \@makecaption)
% pour votre propre mise en forme
\newcommand{\@makeunnumberedcaption}[1]{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{#1}%
  \ifdim \wd\@tempboxa >\hsize
    #1\par
  \else
    \global \@minipagefalse
    \hbox to\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}

% pour compatibilité avec LaTeX 2.09,
% définit \abovecaptionskip et \belowcaptionskip
\@ifundefined{abovecaptionskip}{%
  \newlength{\abovecaptionskip}%
  \setlength{\abovecaptionskip}{10pt}%
}{}
\@ifundefined{belowcaptionskip}{%
  \newlength{\belowcaptionskip}%
  \setlength{\belowcaptionskip}{0pt}%
}{}

\makeatother
```

Il faut noter que cette commande ne sera pas modifiée par les différentes extensions susceptibles de redéfinir la commande `\caption`, comme par exemple {ctanpkg}`caption`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légendes,légende non numérotée
```

