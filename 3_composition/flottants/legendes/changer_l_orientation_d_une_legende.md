# Comment changer l'orientation d'une légende ?

- Le package {ctanpkg}`rotating` fournit une commande, habilement nommée `\rotcaption`, qui permet de changer l'orientation de la légende. Voici un exemple de fonctionnement de cette commande :

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{rotating}
\usepackage{babel}

\begin{document}
Un peu de texte autour... Un peu de texte autour...
Un peu de texte autour... Un peu de texte autour...
Un peu de texte autour... Un peu de texte autour...
\begin{table}[!ht]
\begin{minipage}{1cm}
  \rotcaption{La légende}\label{test}
\end{minipage}
\centerline{%
\begin{tabular}{|c|}
\hline
Un petit tableau \\
Avec quelques lignes \\
Pour voir comment \\
est placée la légende.\\
\hline
\end{tabular}}
\end{table}
Un peu de texte autour... Un peu de texte autour...
Un peu de texte autour... Un peu de texte autour...
Un peu de texte autour... Un peu de texte autour...

Et pour finir, une référence au tableau~\ref{test}.
\end{document}
```

- Les environnements `sidewaystable` et `sidewaysfigure` définis par ce même package {ctanpkg}`rotating` changent automatiquement l'orientation de la légende, il n'y a donc rien à changer. Noter que dans ce cas, la figure sera nécessairement sur une page séparée.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

