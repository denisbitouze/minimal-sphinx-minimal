# Comment modifier l'espace entre une figure et sa légende ?

Les longueurs qui gèrent cet espace s'appelle `\abovecaptionskip` (pour l'espace placé au-dessus de la légende) et `\belowcaptionskip` (au-dessous de la légende).

Par exemple :

```latex
% !TEX noedit
\addtolength{\abovecaptionskip}{2mm}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

