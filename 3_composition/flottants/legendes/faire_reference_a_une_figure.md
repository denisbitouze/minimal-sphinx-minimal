# Comment nommer une figure ?

La commande `\caption` permet de placer une légende au-dessous (ou au-dessus) d'une figure, ainsi que de lui donner un numéro afin d'y faire référence. Dans ce dernier cas, il ne faut pas oublier de placer le `\label` *après* la commande `\caption` (puisqu'avant que cette commande soit exécutée, le numéro de figure n'est pas défini). Comme le montre l'exemple suivant, ce n'est pas très compliqué.

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
\begin{figure}[!ht]
\centerline{\framebox{ICI, UNE FIGURE}}
\caption{Ma légende}\label{fig+mafigure}
\end{figure}
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Et une référence à la figure~\ref{fig+mafigure}.
\end{document}
```

```latex
\documentclass{article}
\usepackage[total={12cm,8cm}]{geometry}
\pagestyle{empty}
\begin{document}
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
\begin{figure}[!ht]
\centerline{\framebox{ICI, UNE FIGURE}}
\caption{Ma légende}\label{fig+mafigure}
\end{figure}
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte... Du texte... Du texte... Du texte...
Et une référence à la figure~1.
\end{document}
```

Le mot « Figure » placé devant le numéro de figure peut être modifié. Sur ce point, voir la question « [Comment modifier la commande « \\caption» ?](/3_composition/flottants/legendes/modifier_l_apparence_des_legendes) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,figure,légende,caption
```

