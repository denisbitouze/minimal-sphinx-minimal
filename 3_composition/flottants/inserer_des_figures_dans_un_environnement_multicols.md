# Comment insérer des flottants dans un document multicolonne ?

L'utilisation de l'environnement suivant dans un environnement `multicols` cause des difficultés :

```latex
% !TEX noedit
\begin{figure}
  ...
\end{figure}
```

En effet, votre figure n'apparaîtra pas : les flottants sont interdits à l'intérieur de cet environnement.

Cependant, pour les flottants de type `figure` et `table`, il existe une version étoilée qui peut être incluse dans un environnement `multicols` :

```latex
% !TEX noedit
\begin{figure*}
 ...
\end{figure*}
```

&#160;ou

```latex
% !TEX noedit
\begin{table*}
 ...
\end{table*}
```

Dans ce cas, la figure ou le tableau s'étendra sur toute la largeur de la page (comme quand vous utilisez `figure*` avec l'option standard `twocolumn` de LaTeX). Petit inconvénient, tout de même : le flottant ne peut apparaître, au mieux, qu'à la page suivante.

:::{note}
Si vous utilisez l'extension {ctanpkg}`float` pour {doc}`créer de nouveaux types de flottants </3_composition/flottants/definir_de_nouveaux_flottants>`, ne soyez pas surpris : ces nouveaux types ne sont pas non plus autorisés dans l'environnement `multicols`, la commande `\newfloat` ne crée pas l'environnement étoilé correspondant.
:::

Il est possible d'avoir des figures et des tableaux sur une seule colonne, avec leur légende, en utilisant l'option de placement « `[H]` » introduite par l'extension {ctanpkg}`float`, mais vous devrez peut-être bricoler le placement parce qu'ils ne « flotteront » pas vraiment, et présenteront d'autres comportements étranges (comme le fait de déborder silencieusement de la fin de la colonne quand l'environnement `multicols` se termine).

______________________________________________________________________

*Source :* {faquk}`Floats in multicolumn setting <FAQ-mcfloat>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,figures en mode deux colonnes,mise en page en colonnes,flottants en colonnes,environnement multicols
```

