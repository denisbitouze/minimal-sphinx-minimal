# Comment placer des figures côte à côte ?

## En utilisant l'extension « subcaption »

Un exemple simple permet de montrer le fonctionnement de l'extension {ctanpkg}`subcaption`. Chaque sous-figure est représentée par une commande `\subcaptionbox`, qui permet, au minimum, d'indiquer le texte de sa légende et son contenu. Il est possible de donner une légende globale à l'ensemble constitué par les deux sous-figure en utilisant, tout simplement, une commande `\caption` à l'intérieur de l'environnement `figure`.

La {texdoc}`documentation <subcaption>` indique plus en détails comment régler la mise en forme des légendes des sous-figures, leur numérotation, la largeur des boîtes dans lesquelles sont placées chaque figure, etc... L'extension définit aussi des envirionnements `subfigure` (à ne pas confondre avec l'extension obsolète du même nom!) et `subtable`.

```latex
% !TEX noedit
\documentclass{report}
\usepackage{subcaption}

\begin{document}

Je commence, comme d'habitude, par un peu de texte.
La figure étant flottante, je vais m'arranger pour
qu'elle soit après le texte, donc plut\^ot en bas
de la page, disons.

\begin{figure}[b]
  \centering

  \subcaptionbox{La légende de la première figure\label{fig1a}}{%
    \fbox{Ceci est ma première figure}}

  \subcaptionbox{La légende de la deuxième figure\label{fig1b}}{%
    \fbox{Ceci est ma deuxième figure}}

  \caption{Une figure divisée en deux}\label{fig1}
\end{figure}

Comme j'ai demandé que la figure soit en bas de la
page, ce texte est à la suite du texte qui précède
la figure dans le source du document.

On peut faire référence soit aux sous-figures~\ref{fig1a}
et~\ref{fig1b}, ou globalement, à la figure~\ref{fig1}.

\end{document}
```

## Avec l'extension « subfigure »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`subfigure` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

Autrefois, l'extension {ctanpkg}`subfigure` servait à diviser un environnement `figure` en plusieurs sous-figures, chacune ayant sa propre légende, et étant numérotée avec un \$(a)\$, \$(b)\$... Le plus simple, pour comprendre le fonctionnement, est de regarder sur cet exemple :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{subfigure}

\begin{document}
Je commence, comme d'habitude, par un peu
de texte. La figure étant flottante, je
vais m'arranger pour qu'elle soit après
le texte, donc plutôt en bas de la page,
disons.
\begin{figure}[b]
\null\hfill
\subfigure[La légende de la première figure\label{fig1a}]{%
     \fbox{Ceci est ma première figure}}
\hfill
\subfigure[La légende de la deuxième figure\label{fig1b}]{%
     \fbox{Ceci est ma deuxième figure}}
\hfill\null
\caption{Une figure divisée en deux}\label{fig1}
\end{figure}
Comme j'ai demandé que la figure soit
en bas de la page, ce texte est à la suite
du texte qui précède la figure dans le
source du document.

On peut faire référence soit aux
sous-figures~\ref{fig1a} et~\ref{fig1b}, ou
globalement, à la figure~\ref{fig1}.

\end{document}
```

Pour simplement placer deux figures sans qu'elles aient chacune une légende et un numéro, il faudra simplement les mettre dans un environnement `minipage`, par exemple, ou dans un tableau. Il est cependant toujours possible d'utiliser {ctanpkg}`subfigure`, en supprimant l'argument optionnel qui définit la légende.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement
```

