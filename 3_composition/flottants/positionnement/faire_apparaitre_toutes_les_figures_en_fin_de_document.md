# Comment faire apparaître toutes les figures en fin de document ?

## Avec l'extension « endfloat »

L'extension {ctanpkg}`endfloat` renvoie tous les flottants en fin de document. Elle dispose d'options permettant notamment :

- de placer les tables avant les figures avec `tablesfirst` et inversement avec `figuresfirst` ;
- de ne pas afficher les tables avec `notables` ou de ne pas afficher les figures avec `nofigures` ;
- de ne pas afficher de liste des tables avec `notablist` ou de liste des figures avec `nofiglist`.

Cette extension ne fonctionne par défaut qu'avec les flottants `figure` et `table`. Si vous avez créé d'autres flottants (voir la question « [Comment définir de nouveaux flottants ?](/3_composition/flottants/definir_de_nouveaux_flottants) »), les intégrer à la logique de l'extension {ctanpkg}`endfloat` se fait avec la commmande `\DeclareDelayedFloat`. Voici un exemple (issu de la {texdoc}`documentation <endfloat>` de l'extension) avec l'extension {ctanpkg}`newfloat` :

```latex
% !TEX noedit
\usepackage{newfloat} % Une des extensions permettant de créer des flottants
\usepackage{endfloat}
\DeclareFloatingEnvironment{carte}  % La commande de création de flottant de 'newfloat'
\DeclareDelayedFloat{carte}{Cartes}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement,fin de document,endlfoat,newfloat
```

