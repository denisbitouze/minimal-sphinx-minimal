# Comment imposer un emplacement à un flottant ?

La demande est contradictoire : `figure` et `table` sont *conçus* pour flotter, et auront toujours la possibilité d'être placés loin de l'endroit où vous les avez évoqués. Par conséquent, vous avez besoin de quelque chose qui se présente comme un environnement `figure` ou `table` mais sans le comportement d'un flottant.

## Avec l'extension « float »

Le moyen le plus simple est d'utiliser l'extension {ctanpkg}`float` qui vous donne une option de placement flottant « `H` » qui empêche le flottement :

```latex
% !TEX noedit
\begin{figure}[H]
  \centering % Pour centrer la figure
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH !
  \caption{Beaucoup de bruit pour rien}
  \label{fig:aaaaah}
\end{figure}
```

Il existe une extension {ctanpkg}`here` qui permet d'obtenir la même fonctionnalité mais elle n'est pas recommandée : elle sert uniquement pour des documents anciens.

## Avec l'extension « caption » ou l'extension « capt-of »

En fait, vous n'avez pas *besoin* d'utiliser l'extension {ctanpkg}`float`. Il existe un moyen simple pour placer votre figurine à votre guise que montre le code suivant :

```latex
% !TEX noedit
\begin{center}
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH !
  \captionof{figure}{Beaucoup de bruit pour rien}
  \label{fig:aaaaah}
\end{center}
```

Ce code s'appuie sur la commande `\captionof` pour placer une légende sans bénéficier d'un flottant englobant. Cette commande peut être obtenue à partir de l'extension très simple {ctanpkg}`capt-of` ou de l'extension sophistiquée {ctanpkg}`caption`. Elle est présentée à la question « [Comment utiliser la commande « \\caption » hors d'un environnement flottant ?](/3_composition/flottants/legendes/inserer_une_legende_sans_flottant) ».

## Les limites de ces méthodes

### Gestion de l'encombrement des flottants

Quelle que soit la méthode utilisée, vous devrez gérer le cas où une figure ou un tableau soit trop grand pour la page (sous peine d'erreurs « [Overfull \\vbox](/2_programmation/erreurs/o/overfull) » et de mise en page dégradée).

### Gestion de la numérotation des flottants

Un autre problème vient de la possibilité que de tels « flottants fixes » s'insèrent entre de vrais flottants, de sorte que leurs numéros seront dans le désordre : la figure 6, non flottante, pourrait être à la page 12, tandis que la figure 5, flottante, pourrait se retrouver en page 13. Ici, mieux vaut n'avoir que des flottants ou que des « flottants fixes ».

Si vous ne pouvez suivre ce conseil, vous pouvez utiliser la commande `\MakeSorted` de l'extension {ctanpkg}`perpage`. En voici un exemple qui corrige la numérotation de vos flottants :

```latex
% !TEX noedit
\usepackage{float}
\usepackage{perpage}
\MakeSorted{figure}
\MakeSorted{table}
```

______________________________________________________________________

*Source :* {faquk}`Figure (or table) \_exactly\_ where I want it <FAQ-figurehere>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement,tables,figures
```

