# Comment modifier le nombre de figures par page ?

Plusieurs compteurs permettent de contrôler le nombre de flottants par page :

- `topnumber` permet de contrôler le nombre maximal de flottants pouvant se suivre en haut de page (avec le [placement](/3_composition/flottants/positionnement/gestion_positionnement) `t`) ;
- `bottomnumber` permet de contrôler le nombre maximal de flottants pouvant se suivre en bas de page (avec le [placement](/3_composition/flottants/positionnement/gestion_positionnement) `b`) ;
- `totalnumber` indique le nombre maximal de flottants pouvant être placés sur une même page.

En complément, la commande `\floatpagefraction` contient la proportion de flottants à partir de laquelle la page ne contiendra pas de texte. Par défaut, cette commande retourne `0.5`. Aussi, si un ou plusieurs flottants utilisent plus de la moitié de la hauteur de la page, ils seront placés sur une page ne contenant que des flottants.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement,paramètres
```

