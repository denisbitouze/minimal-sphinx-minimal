# Comment isoler un flottant sur une page à part ?

Il est parfois nécessaire de forcer un flottant à être placé seul et sans texte sur une page. Il est d'ailleurs parfois nécessaire que cela soit le cas pour *chaque* flottant. Lorsqu'un flottant ne parvient pas à « se positionner » seul et attend alors la fin d'un chapitre ou du document, une réflexe est souvent d'introduire le flottant avec la commande `\begin{`*figure ou table*`}[p!]`... mais l'indicateur de positionnement plus contraignant « `!` » n'a aucun effet sur les flottants qu'on souhaite placer seul sur des pages. Vous devez donc faire en sorte que le flottant respecte les paramètres de LaTeX. La question « {doc}`Comment gérer proprement les flottants dans LaTeX ? </3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux>` » propose quelques suggestions mais ne résout pas la question d'avoir un flottant par page.

La solution utilisant le compteur `totalnumber` (définissant le nombre total de flottants par page) ne fonctionne pas : `totalnumber` ne s'applique en effet qu'aux flottants sur les pages contenant aussi du texte. Aussi, pour permettre à n'importe quel flottant de prendre une page entière :

- définissez `\floatpagefraction` avec une petite valeur ;
- et pour vous assurer qu'il n'y ait pas plus d'un flottant par page, imposez une séparation importante entre les flottants.

Voici un exemple permettant d'obtenir cet effet :

```latex
% !TEX noedit
\renewcommand\floatpagefraction{.001}
\makeatletter
\setlength\@fpsep{\textheight}
\makeatother
```

______________________________________________________________________

*Source :* {faquk}`Floats on their own on float pages <FAQ-floatpages>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,floats,tables
```

