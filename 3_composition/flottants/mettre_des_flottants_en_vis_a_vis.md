# Comment mettre des flottants sur des pages en vis-à-vis ?

## Avec l'extension « dpfloat »

Si deux flottants sont destinés à occuper deux pages en vis à vis (ou « une double page »), dans un livre, le premier doit fatalement se trouver sur une page de gauche (*ie* sur une page paire). L'extension {ctanpkg}`dpfloat` se charge de ça. La construction à utiliser est la suivante :

```latex
% !TEX noedit
\begin{figure}[p]
  \begin{leftfullpage}
    ⟨figure du côté gauche⟩
  \end{leftfullpage}
\end{figure}
%
\begin{figure}[p]
  \begin{fullpage}
    ⟨figure du côté droit⟩
  \end{fullpage}
\end{figure}
```

Cela n'a aucun effet sur les documents qui auraient l'option de classe `oneside` (mode recto seul) :

```latex
% !TEX noedit
\documentclass[oneside]{book}% Ça ne marchera pas !
```

Pour la classe {ctanpkg}`book`, `twoside` est la valeur par défaut, donc si rien n'est précisé, {ctanpkg}`dpfloat` pourra fonctionner.

## Avec l'extension « fltpage »

Un cas particulier de la mise en forme de flottants sur deux pages consécutives : quand on veut disposer la légende d'un flottant sur la page suivante (c'est une possibilité quand on souhaite que le flottant utilise tout l'espace disponible sur la page). Vous pouvez (avec un certain nombre de manipulations) utiliser de nouveau {ctanpkg}`dpfloat`, mais l'extension {ctanpkg}`fltpage` sera plus adaptée :

```latex
% !TEX noedit
\documentclass[twoside]{article}
  \usepackage[leftFloats]{fltpage}

\begin{document}
...
\begin{FPfigure}
  \includegraphics{⟨ma grosse figure⟩}
  \caption{Oh qu'elle est belle !}
\end{FPfigure}
...
\end{document}
```

Cet exemple devrait produire une légende du style : « Fɪɢᴜʀᴇ *n (facing page)* : Oh qu'elle est belle ! »

:::{important}
Cette extension est un peu ancienne, elle se déclare comme étant une version bêta et elle ne mentionne pas de licence valide (donc elle n'est pas distribuée dans TeX Live). Elle fait le boulot, mais c'est à vos risques et périls.
:::

## Avec l'extension « caption »

Une autre solution consiste à utiliser le mécanisme de l'extension {ctanpkg}`caption` prévu pour les tableaux s'étalant sur plusieurs pages. La macro `\ContinuedFloat` apporte une petite modification à la prochaine commande `\caption`, de sorte que la commande n'incrémente pas le numéro de la légende. Cela n'a aucun effet sur le placement réel du flottant, mais cela permet d'avoir une numérotation logique :

```latex
% !TEX noedit
\begin{table}
  \caption{Un tableau}
  ...
\end{table}
...
\begin{table}\ContinuedFloat
  \caption{Un tableau (suite)}
  ...
\end{table}
```

ce qui donnera : « Tᴀʙʟᴇ 3 : Un tablea ... Tᴀʙʟᴇ 3 : Un tableau (suite) »

______________________________________________________________________

*Source :* {faquk}`Facing floats on 2-page spread <FAQ-dpfloat>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,figures,tableaux,Tᴀʙʟᴇ sur deux pages,figures sur des pages face à face
```

