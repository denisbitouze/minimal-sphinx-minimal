# Comment gérer proprement les flottants dans LaTeX ?

Les tables et les figures surprennent souvent, en *flottant* loin de l'endroit où elles ont été placées dans le code du document. Il s'agit là d'un principe parfaitement ordinaire dans une conception de document : tout logiciel de composition positionnera les figures et les tableaux là où ils enfreindront le moins possible de règles typographiques. Même si vous utilisez le spécificateur de placement « `h` » (pour *here*, c'est-à-dire « ici »), la figure ou le tableau ne sera pas positionnée « ici » si ces règles ne sont pas respectées. Ces règles elles-mêmes sont assez simples et sont listées à la page 198, section C.9 du manuel LaTeX

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *sans doute à préciser*). Mais, dans le pire des cas, les règles de :latexlogo:`LaTeX` » peuvent entraîner l'empilement des éléments flottants dans la mesure où vous obtenez un message d'erreur disant « :doc:`Too many unprocessed floats </2_programmation/erreurs/t/too_many_unprocessed_floats>` ».
```

Ce qui suit est une simple liste de points à étudier pour résoudre ces problèmes.

## Éviter les flottants non nécessaires

La question « {doc}`Comment imposer un emplacement à un flottant ? </3_composition/flottants/positionnement/forcer_la_position_d_un_flottant2>` » illustre des méthodes pour contourner la mécanique de flottement, ce qui peut correspondre à une solution efficace pour des documents courts ou des documents ne nécessitant pas un grand formalisme.

## Utiliser les bons spécificateurs de placement

La séquence de spécificateurs par défaut, « `[tbp]` », est généralement satisfaisante, mais vous pouvez raisonnablement la modifier (par exemple, en y ajoutant un « `h` »). Quoi que vous fassiez, *n'omettez* pas le « `p` », sinon cela pourrait amener LaTeX à considérer que, si vous ne pouvez pas avoir votre flottant *ici*, vous ne le voulez *nulle part*.

## Changer les paramètres de gestion des flottants

Les paramètres de LaTeX gérant le placement des flottants peuvent empêcher des placements vous semblant « raisonnables » : ils sont connus pour être assez stricts. Pour encourager LaTeX à ne pas bouger votre flottant, vous devrez peut-être relâcher ses exigences. Les paramètres les plus importants sont ici les ratios « texte sur page » et « flottant sur page » mais il est judicieux d'avoir plutôt un ensemble fixe de paramètres pour répondre à toutes les éventualités.

```latex
% !TEX noedit
\renewcommand{\topfraction}{.85}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.15}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}
```

La signification de ces paramètres est donnée en pages 199 à 200, section C.9 du manuel LaTeX.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *sans doute à préciser*) »
```

## Utiliser la commande « \\clearpage »

Votre document dispose d'endroits où vous où vous pourriez mettre une commande `\clearpage`. Si c'est le cas, faites-le. Les flottants en liste d'attente seront alors tous positionnés là. D'ailleurs, la commande `\chapter` des classes standard {ctanpkg}`book` et {ctanpkg}`report` exécute implicitement `\clearpage`, empêchant ainsi aux flottants de passer d'un chapitre à un autre.

## Utiliser l'extension « placeins »

L'extension {ctanpkg}`placeins` définit une commande `\FloatBarrier` (littéralement, une « barrière à flottant ») au-delà de laquelle les flottants ne peuvent pas aller. Vous pouvez placer des `\FloatBarrier` où vous le souhaitez mais une option de l'extension vous permet aussi de déclarer que les `\section` acquièrent cette propriété.

## Utiliser l'extension « flafter »

Si vous êtes gêné par les flottants apparaissant en haut de la page (avant qu'ils ne soient spécifiés dans votre texte), essayez l'extension {ctanpkg}`flafter <latex-base>`, qui évite ce problème en insistant sur le fait que les flottants doivent toujours apparaître *après* leur définition.

## Utiliser l'extension « afterpage »

La documentation de l'extension {ctanpkg}`afterpage` donne à titre d'exemple de son utilisation l'idée de mettre `\clearpage` *après* la page courante (traitant ainsi la liste d'attente des flottants sans pour autant causer un vilain vide dans votre texte), mais cette méthode est quelque peu fragile. Utilisez-la en dernier recours si les autres possibilités ci-dessous ne vous aident pas.

## Utiliser l'extension « morefloats »

Si vous souhaitez réellement avoir de larges blocs de flottants à la fin de chacun de vos chapitres, essayez la commande `\extrafloats`, ou pour les anciennes versions de LaTeX l'extension {ctanpkg}`morefloats`. Cela vous permet d'augmenter le nombre de flottants que LaTeX peut gérer à un instant donné (en partant de la valeur de base, 18 dans les versions anciennes de LaTeX ou 52 dans LaTeX).

Dans le cas particulier où vous utiliseriez l'extension {ctanpkg}`etex <etex-pkg>` pour augmenter le nombre de registres disponibles sur d'anciennes versions de LaTeX, vous aurez alors besoin de réserver des emplacements pour {ctanpkg}`morefloats` avec le code ressemblant à ceci :

```latex
% !TEX noedit
\usepackage{etex}
\reserveinserts{18}
\usepackage{morefloats}
```

## Utiliser l'extension « endfloat »

Si vous souhaitez avoir tous vos flottants en fin de document (par exemple pour soumettre une version provisoire d'un article), ne comptez pas sur les mécanismes usuels de LaTeX : utilisez l'extension {ctanpkg}`endfloat` pour traiter ce point.

______________________________________________________________________

*Source :* {faquk}`Moving tables and figures in LaTeX <FAQ-floats>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,placement des figures,placement des tableaux,mettre les figures en fin de document
```

