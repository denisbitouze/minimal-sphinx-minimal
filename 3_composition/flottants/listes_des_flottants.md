# Comment générer une liste des flottants ?

De la même façon que LaTeX s'occupe automatiquement de mettre à jour la table des matières de votre document, avec la simple commande `\tableofcontents`, il peut produire une « liste des figures » ou une « liste des tableaux ».

:::{note}
Pour garantir que ces tables soient correctes, il faut compiler le document au moins deux fois.
:::

## La liste des figures

La commande `\listoffigures` s'en occupe, exactement comme `\tableofcontents` génère la table des matières. Dans la liste ainsi créée se trouvent toutes les figures ayant une légende.

Par défaut, elle est titrée « List of figures », ce qui peut se corriger en utilisant les méthodes vues :

- par la question « [Comment franciser un document LaTeX ?](/3_composition/langues/composer_un_document_latex_en_francais) » ;
- par la question « [Comment changer les textes prédéfinis de LaTeX ?](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) ».

## La liste des tableaux

La commande `\listoftables`, petite sœur de `\listoffigures`, crée la liste des tableaux. Par défaut, elle est titrée « List of tables », ce qui se corrige comme vu précédemment.

## La liste des algorithmes

L'extension {ctanpkg}`algorithm` définit la commande `\listofalgorithms`, qui liste les algorithmes décrits dans le document.

## La liste de nouveaux flottants

Les extensions {ctanpkg}`float` et {ctanpkg}`floatrow` permettent, entre autres, de créer de nouveaux types de flottants et de les lister. Par exemple, la création d'un flottant nommé `extrait` permet d'utiliser la commande suivante dans laquelle vous indiquez le titre que vous souhaitez pour votre liste.

```latex
% !TEX noedit
\listof{extrait}{Liste des extraits}
```

L'extension {ctanpkg}`newfloat` donne accès à des fonctionnalités similaires. Pour un flottant nommé `extrait`, l'extension crée automatiquement une commande `\listofextrait`.

Ces différentes extensions sont présentées avec des exemples à la question « [Comment définir de nouveaux flottants ?](/3_composition/flottants/definir_de_nouveaux_flottants) ».

______________________________________________________________________

*Sources :*

- [Listez vos figures, tables, symboles et mots clefs dans votre document LaTeX](https://blog.dorian-depriester.fr/latex/listez-vos-figures-tables-symboles-et-mots-clefs-dans-votre-document-latex),
- [How to automatically create a short caption for the list of figures?](https://tex.stackexchange.com/questions/434288/how-to-automatically-create-a-short-caption-for-the-list-of-figures)

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,list of tables,liste des listes,liste des tableaux,liste des tables,table des tableaux,liste des figures,table des matières
```

