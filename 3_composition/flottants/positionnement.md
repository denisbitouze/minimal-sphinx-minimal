# Positionnement des flottants

Cette section détaille le sujet du positionnement des [flottants](/3_composition/flottants/start) dans le document.

- {doc}`Comment est géré le positionnement des flottants ? </3_composition/flottants/positionnement/gestion_positionnement>`
- {doc}`Comment imposer un emplacement à un flottant ? </3_composition/flottants/positionnement/forcer_la_position_d_un_flottant2>`
- {doc}`Comment faire apparaître toutes les figures en fin de document ? </3_composition/flottants/positionnement/faire_apparaitre_toutes_les_figures_en_fin_de_document>`
- {doc}`Comment isoler un flottant sur une page à part ? </3_composition/flottants/positionnement/faire_une_page_de_flottants>`
- {doc}`Comment lier le placement des flottants aux sections ? </3_composition/flottants/positionnement/forcer_la_position_d_un_flottant_dans_une_section>`
- {doc}`Comment placer des figures côte à côte ? </3_composition/flottants/positionnement/placer_des_figures_cote_a_cote>`
- {doc}`Comment placer des figures face à face en recto-verso ? </3_composition/flottants/positionnement/placer_des_figures_sur_deux_pages_en_vis_a_vis>`
- {doc}`Comment modifier le nombre de figures par page ? </3_composition/flottants/positionnement/modifier_le_nombre_de_flottants_par_page>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement
```

