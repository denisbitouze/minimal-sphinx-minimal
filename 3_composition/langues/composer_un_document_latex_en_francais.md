# Comment franciser un document LaTeX ?

## Avec l'extension babel

L'extension {ctanpkg}`babel` de J. Braams permet de composer des documents multilingues. Son appel sous LaTeX se fait par

```latex
% !TEX noedit
\usepackage[langue-1, langue-2, ..., langue-n]{babel}
```

Parmi les langues actuellement disponibles, on peut citer `english`, `german`, `italian`, `french`. Dans le cas d'un document multilingue avec une langue principale, cette dernière est à déclarer avec la clé `main`, par exemple :

```latex
% !TEX noedit
\usepackage[main=german,french,spanish]{babel}
```

Le changement de langue se fait via la commande `\selectlanguage`. Pour passer en espagnol, il faut indiquer :

```latex
% !TEX noedit
\selectlanguage{spanish}
```

:::{warning}
```{eval-rst}
.. todo:: // Commentaire à vérifier. //
```

Les options `[francais]` et `[frenchb]` de l'extension {ctanpkg}`babel` font appel à `frenchb` maintenue par D. Flipo, tandis que `[french]` fait appel à l'extension {ctanpkg}`french` (French Pro) de B. Gaulle lorsque ce dernier est installé (et plus particulièrement `french.ldf`).

Les versions de toutes ces extensions sont très importantes ({ctanpkg}`babel`, `frenchb`, {ctanpkg}`french`) pour une bonne coexistence et les fichiers de césure sont les mêmes pour {ctanpkg}`babel` et pour {ctanpkg}`french`.
:::

## Avec l'extension polyglossia

L'extension {ctanpkg}`polyglossia` est pensée pour traiter ce sujet avec [XeLaTeX](/1_generalites/glossaire/qu_est_ce_que_xetex) et [LuaLaTeX](1_generalites/glossaire/qu_est_ce_que_luatex). L'appel pour le français, en tant que langue principale, est :

```latex
% !TEX noedit
\usepackage{polyglossia}
\setmainlanguage{french}
```

Comme pour {ctanpkg}`babel`, le changement de langue se fait via la commande `\selectlanguage`. Ainsi, pour passer en espagnol, il faut indiquer :

```latex
% !TEX noedit
\selectlanguage{spanish}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

