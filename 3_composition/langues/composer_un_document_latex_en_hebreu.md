# Comment composer du texte en hébreu moderne ou classique ?

- Au moins deux extensions permettent ceci :
- {ctanpkg}`ArabTeX`,
- {ctanpkg}`cjhebrew`.

## Avec l'extension « ArabTeX »

Le site web primaire de l'extension {ctanpkg}`ArabTeX` est <http://www2.informatik.uni-stuttgart.de/ivi/bs/research/arab_e.htm> et est téléchargeable à l'adresse sur <ftp://ftp.informatik.uni-stuttgart.de/pub/arabtex/>. Elle permet la [vocalisation](https://fr.wikipedia.org/wiki/Diacritiques_de_l'alphabet_hébreu) mais pas les [marques de cantillation](https://fr.wikipedia.org/wiki/Cantillation).

Comme son nom l'indique, {ctanpkg}`ArabTeX` permet également d'{doc}`écrire en arabe </3_composition/langues/composer_un_document_latex_en_arabe>`, mais voici un exemple en hébreu :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{arabtex,hebtex}
  \setcode{standard}
  \sethebrew
  \pagestyle{empty}

\begin{document}
\large
Voici du texte en hébreu
\begin{arabtext}
yOm tOb
\end{arabtext}
Fin du texte en hébreu
\end{document}
```

## Avec l'extension « cjhebrew »

L'extension {ctanpkg}`cjhebrew` offre une syntaxe particulière pour saisir de courts textes en hébreu, avec la commande `\<`...`>` :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{cjhebrew}
  \pagestyle{empty}

\begin{document}
\large
Exemple :

\<’bgd>

\end{document}
```

Une syntaxe LaTeX plus classique est également offerte, avec la commande `\cjRL`, et un environnement `cjhebrew` (qui compose automatiquement dans la bonne direction, de droite à gauche) :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{cjhebrew}
  \pagestyle{empty}

\begin{document}
\large
Exemple :

\cjRL{’bgd}

\begin{cjhebrew}
’bgd
\end{cjhebrew}

\end{document}
```

:::{important}
La première commande mentionnée {texdoc}`dans la documentation <cjhebrew>` est `\textcjheb`, mais elle ne fait pas ce que vous voudrez, en général, car elle compose en hébreu « à l'envers » (de gauche à droite).
:::

### Encodage des consonnes

Voici comment écrire les consonnes de votre texte :

```latex
\documentclass{article}
  \usepackage{booktabs}
  \usepackage{cjhebrew}
  \pagestyle{empty}

\def\!#1!{\texttt{#1}}
\newcommand{\bs}{\textbackslash}

\begin{document}
\large
\begin{tabular}{ccccccccccccccc}

\toprule\midrule

\<'> & \<b> & \<g> & \<d> & \<h> & \<w> & \<z> & \<.h> &
\<.t> & \<y> & \<k|> & \<k> & \<l> & \<m|> & \<m>\\

\!'! & \!b! & \!g! & \!d! & \!h! & \!w! & \!z! & \!.h! & \!.t! &
\!y! & \!k! & \!K! & \!l! & \!m! & \!M!\\

\midrule

\<n|> & \<n> & \<s> & \<`> & \<p|> & \<p> & \<.s|> & \<.s> & \<q>
& \<r> & \</s>
& \<,s> & \<+s> & \<t> &\\

\!n! & \!N! & \!s! & \!`! & \!p! & \!P! & \!.s! & \!.S! & \!q! &
\!r! & \!/s!
& \!,s! & \!+s! & \!t! &\\

\midrule\bottomrule

\end{tabular}

\medskip

\textit{Note : \!'!~= point-virgule, \!`!~= accent grave}

\end{document}
```

Normalement, les formes finales des lettres sont automatiquement sélectionnées si besoin. Si vous avez besoin d'une forme finale *au milieu* d'un mot, vous pouvez faire suivre la lettre d'un point d'exclamation `!` ou de la commande `\endofword`.

Inversement, si vous souhaitez utiliser une forme médiane en fin de mot, faites suivre la lettre d'une barre verticale `|` ou de la commande `\zeronojoin`.

### Encodage des voyelles et accents

Les voyelles viennent *après* la consonne à laquelle elles sont associées :

```latex
\documentclass{article}
  \usepackage{booktabs}
  \usepackage{cjhebrew}
  \pagestyle{empty}

\def\!#1!{\texttt{#1}}
\newcommand{\bs}{\textbackslash}
\def\dc{\verb+\dottedcircle+}

\begin{document}
\large
\begin{tabular}{ccccccccccccccc}

\toprule\midrule

\<\dottedcircle i> & \<\dottedcircle e> & \<\dottedcircle E> &
\<\dottedcircle E:> & \<\dottedcircle a> & \</a\dottedcircle> &
\<\dottedcircle a:> & \<\dottedcircle A> & \<\dottedcircle A:> &
\<\dottedcircle o> & \<\dottedcircle u> &
\<\dottedcircle *> & \<\dottedcircle :> & \<O> & \<U>\\

\!i! & \!e! & \!E! & \!E:! & \!a! & \!/a! & \!a:! & \!A! & \!A:! &
\!o! & \!u! & \!*! & \!:! & \!O! / \!wo! & \!U!
/ \!w*!\\

\midrule

\<;> & \<--> & \<\dottedcircle> \\
\!;! & \!-\/-! & \multicolumn{6}{l}{\texttt{\bs dottedcircle}}\\

\midrule\bottomrule

\end{tabular}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,langues orientales,hébreu,écrire en hébreu avec LaTeX,arabe
```

