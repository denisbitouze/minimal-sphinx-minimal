# Comment changer les textes prédéfinis de LaTeX ?

Les classes de document de LaTeX définissent plusieurs opérations typographiques qui nécessitent un « texte prédéfini » (non fourni par l'utilisateur) parfois appelé « intitulé » ou « titre ». Initialement, avec LaTeX 2.09, ces morceaux de texte étaient intégrés directement dans les commandes de LaTeX et étaient plutôt difficiles à changer. Des commandes à « nom fixe » ont cependant été finalement introduites au profit de ceux qui souhaitaient utiliser LaTeX dans d'autres langues que l'anglais. Par exemple, la section spécifique produite par la commande `\tableofcontents` est toujours appelée `\contentsname` (ou plutôt ce que signifie `\contentsname`). La modification de ces intitulés est désormais l'une des personnalisations les plus simples qu'un utilisateur puisse faire avec LaTeX.

## Les méthodes de modification

### Sans l'extension « babel »

Les commandes associés aux textes prédéfinis sont toutes de la forme `\⟨objet⟩name`, comme le montre la table ci-dessous, et les changer se fait ainsi : placez la commande `\renewcommand{\⟨objet⟩name}{⟨nouveau texte⟩}` dans le préambule de votre document et c'est fait ! Voici un exemple classique pour les noms de chapitre :

```latex
% !TEX noedit
\documentclass{report}
\renewcommand{\chaptername}{Chapitre}
\begin{document}
\chapter{Titre du chapitre}
\end{document}
```

### Avec l'extension « babel »

Pour une personne utilisant une autre langue que l'anglais, l'utilisation de l'extension {ctanpkg}`babel` est souvent recommandée car elle offre de nombreuses fonctionnalités et subtilités de composition associées aux autres langues. De fait, lorsque {ctanpkg}`babel` sélectionne une nouvelle langue, cela garantit que les textes prédéfinis de LaTeX sont traduits de manière appropriée pour la langue en question. Malheureusement, les choix de {ctanpkg}`babel` ne sont pas toujours ceux de tout le monde et un mécanisme de rédéfinition de ces textes prédéfinis reste donc nécessaire.

Mais la méthode vue plus haut ne marche pas. En effet, chaque fois qu'une nouvelle langue est sélectionnée, {ctanpkg}`babel` réinitialise tous les textes prédéfinis aux valeurs par défaut de cette langue. En particulier, {ctanpkg}`babel` active la langue principale du document lorsque `\begin{document}` est exécutée, ce qui annule aussitôt toutes les modifications apportées aux textes prédéfinis faite dans le préambule.

Par conséquent, {ctanpkg}`babel` définit une commande pour permettre aux utilisateurs de changer les définitions de ces textes prédéfinis selon la langue : `\addto\captions⟨langue⟩`. Ici `⟨langue⟩` est l'option de langue que vous avez spécifié à {ctanpkg}`babel`. Voici un exemple pour du français :

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\addto\captionsfrench{\renewcommand{\chaptername}{Compte-rendu}}
\begin{document}
\chapter{Titre du chapitre}
\end{document}
```

### Avec l'extension « biblatex »

L'extension {ctanpkg}`biblatex` constitue une exception à la règle vue avec {ctanpkg}`babel` : elle change la manière de traiter les titres des bibliographies (`\bibname`) et des références (`\refname`). Voici un exemple montrant quelle commande placer dans le préambule pour obtenir cette rédéfinition (en mettant ici le mot « Sources » comme titre).

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage{babel}
\usepackage{biblatex}
\addbibresource{test.bib} % L'exemple suppose qu'un fichier "test.bib" existe.

\DefineBibliographyStrings{french}{%
  bibliography = {Sources},
  references = {Sources},
}

\begin{document}
\cite{test} % L'exemple suppose qu'une référence "test" existe dans le fichier ".bib".
\printbibliography
\end{document}
```

## Les différents textes prédéfinis

Les noms définis dans les classes standard LaTeX (et quelques extensions courantes) sont listés ci-dessous. Certains des noms ne sont définis que dans un sous-ensemble des classes (la classe {ctanpkg}`lettre` dispose d'un ensemble de noms qui lui est propre) : la liste indique ces spécificités, le cas échéant, dans la colonne « Contexte ».

| Commande              | Déclaration initiale | Contexte                                     | Signification                             |
| --------------------- | -------------------- | -------------------------------------------- | ----------------------------------------- |
| `\abstractname`       | Abstract             |                                              | Résumé                                    |
| `\acronymname`        | Acronyms             | Extension {ctanpkg}`glossaries`              | Acronymes                                 |
| `\alsoname`           | see also             | Extension {ctanpkg}`makeidx`                 | voir aussi                                |
| `\appendixname`       | Appendix             |                                              | Annexe                                    |
| `\bibname`            | Bibliography         | Classes {ctanpkg}`report` et {ctanpkg}`book` | Bibliographie                             |
| `\ccname`             | cc                   | Classe {ctanpkg}`letter`                     | Copie à                                   |
| `\chaptername`        | Chapter              | Classes {ctanpkg}`report` et {ctanpkg}`book` | Chapitre                                  |
| `\contentsname`       | Contents             |                                              | Table des matières                        |
| `\enclname`           | encl                 | Classe {ctanpkg}`letter`                     | P.J. (pièce jointe)                       |
| `\figurename`         | Figure               |                                              | Figure (pour les légendes de figures)     |
| `\glossaryname`       | Glossary             | Extension {ctanpkg}`glossaries`              | Glossaire                                 |
| `\headtoname`         | To                   | Classe {ctanpkg}`letter`                     | (*vide*)                                  |
| `\indexname`          | Index                |                                              | Index                                     |
| `\listfigurename`     | List of Figures      |                                              | Table des figures                         |
| `\listtablename`      | List of Tables       |                                              | Liste des tableaux                        |
| `\lstlistingname`     | Listing              | Extension {ctanpkg}`listings`                | Listing (pour les légendes de listings)   |
| `\lstlistlistingname` | Listings             | Extension {ctanpkg}`listings`                | Listings (titre de la liste des listings) |
| `\nomname`            | Nomenclature         | Extension {ctanpkg}`nomencl`                 | Liste des symboles                        |
| `\notesname`          | Notes                | Extension {ctanpkg}`endnotes`                | Notes                                     |
| `\pagename`           | Page                 | Classe {ctanpkg}`letter`                     | page                                      |
| `\prefacename`        | Preface              | Extension {ctanpkg}`babel`                   | Préface                                   |
| `\proofname`          | Proof                | Extension {ctanpkg}`amsthm`                  | Démonstration                             |
| `\partname`           | Part                 |                                              | Partie                                    |
| `\refname`            | References           | Classe {ctanpkg}`article`                    | Références                                |
| `\seename`            | see                  | Extension {ctanpkg}`makeidx`                 | voir                                      |
| `\seeonlyname`        | see                  | Classe {ctanpkg}`makeidx`                    | voir                                      |
| `\tablename`          | Table                |                                              | Table (pour les légendes de tables)       |

## Quelques cas particuliers

### Les légendes de figures et tableaux

Une redéfinition francophone usuelle pour les légendes est la suivante :

```latex
% !TEX noedit
\def\figurename{{\scshape Fig.}}%
\def\tablename{{\scshape Tab.}}%
```

### Les parties

Pour le titre des « parties » d'un document, l'affichage proposé par défaut par {ctanpkg}`babel` en français diffère de celui de LaTeX : le numéro de la partie est remplacé par un nombre ordinal placé devant le mot « Partie », ce qui donne « Première partie », « Deuxième partie », etc. Pour retrouver ce comportement sans utiliser {ctanpkg}`babel` (ou pour construire d'autres structures similaires), voici un exemple de code :

```latex
% !TEX noedit
\documentclass{report}
\makeatletter
\def\partname{\protect\@Fpt partie}%
\def\@Fpt{{\ifcase\value{part}\or Première \or Deuxième \or Troisième \or Quatrième\or
           Cinquième\or Sixième \or Septième \or Huitième \or Neuvième \or Dixième \or
           Onzième \or Douzième \or Treizième\or Quatorzième \or Quinzième \or Seizième
           \or Dix-septième \or Dix-huitième \or Dix-neuvième \or Vingtième\fi}}%
\space\def\thepart{}%
\makeatother
\begin{document}
\part{Nouvelle partie}
\end{document}
```

______________________________________________________________________

*Source :*

- {faquk}`How to change LaTeX's "fixed names" <FAQ-fixnam>`
- {faquk}`Changing the words \`babel\` uses <FAQ-latexwords>`
- <https://tex.stackexchange.com/questions/82993/how-to-change-the-name-of-document-elements-like-figure-contents-bibliogr>

```{eval-rst}
.. meta::
   :keywords: LaTeX,texte prédéfini,babel,programmation
```

