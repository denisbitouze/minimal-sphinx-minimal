# Comment définir des exceptions aux motifs de césure ?

Bien que les règles de césure de TeX soient plutôt bonnes, elles ne sont pas infaillibles : vous utiliserez parfois des mots sur lesquels TeX se trompera. Ainsi, par exemple, les règles de césure par défaut de TeX (pour l'anglais américain) ne connaissent pas le mot «*manuscript* », et comme il s'agit d'un mot long, il se peut que vous deviez le couper. Pour corriger cela, vous *pourriez* écrire les points de coupure possibles à chaque fois que vous utilisez le mot :

```latex
% !TEX noedit
... man\-u\-script ...
```

Ici, chacune des commandes `\-` est convertie en une césure, si (et seulement si) nécessaire.

Cette technique peut rapidement devenir fastidieuse : vous ne l'accepterez probablement que s'il n'y a pas plus d'un ou deux mots incorrectement coupés dans votre document. L'alternative est de définir les césures possibles dans le préambule du document. Pour ce faire, pour la césure en question, vous écrirez :

```latex
% !TEX noedit
\hyphenation{man-u-script}
```

et la césure serait définie pour l'ensemble du document. [Barbara Beeton](https://tug.org/interviews/beeton.html) publie des articles contenant des listes de ces «exceptions à la césure», dans le journal *TUGboat*; l'exemple de « man-u-script » provient d'un de ces articles.

:::{tip}
La liste complète des exceptions à la césure est {ctanpkg}`maintenue à jour sur le CTAN <hyphenex>`.

Si vous préférez lire les articles, voici la liste :

| Year | *TUGboat* issue                                                 |
| ---- | --------------------------------------------------------------- |
| 2018 | [39(2):152](https://tug.org/TUGboat/tb39-2/tb122hyf.pdf)        |
| 2018 | [39(1):5–6](https://tug.org/TUGboat/tb39-1/tb121hyf.pdf)        |
| 2015 | [36(1):5](https://tug.org/TUGboat/tb36-1/tb112hyf.pdf)          |
| 2013 | [34(2):113–114](https://tug.org/TUGboat/tb34-2/tb107hyf.pdf)    |
| 2012 | [33(1):5–6](https://tug.org/TUGboat/tb33-1/tb103hyf.pdf)        |
| 2010 | [31(3):160](https://tug.org/TUGboat/tb31-3/tb99hyf.pdf)         |
| 2008 | [29(2):239](https://tug.org/TUGboat/tb29-2/tb92hyf.pdf)         |
| 2005 | [26(1):5–6](https://tug.org/TUGboat/tb26-1/tb82hyf.pdf)         |
| 2002 | [23(3/4):247–248](https://tug.org/TUGboat/tb23-3-4/tb75hyf.pdf) |
| 2001 | [22(1/2):31–32](https://tug.org/TUGboat/tb22-1-2/tb70hyf.pdf)   |
:::

## Que faire si votre document comporte plus d'une langue ?

C'est simple : sélectionnez la langue appropriée, et faites la même chose que ci-dessus :

```latex
% !TEX noedit
\usepackage[french]{babel}
\selectlanguage{french}
\hyphenation{re-cher-cher}
```

(rien de bien compliqué ici : il s'agit de la césure « correcte » du mot, dans les tables actuelles). Cependant, il y a un problème : tout comme les mots contenant des accents introduits par une macro (comme `\'e`) ne seront pas coupés, une commande `\hyphenation` contenant des macros d'accent dans son argument produira une erreur :

```latex
% !TEX noedit
\usepackage[french]{babel}
\selectlanguage{french}
\hyphenation{r\'e-f\'e-rence}
```

nous indique que la césure est *improper*, et qu'elle sera *flushed* (jetée). Mais, tout comme la césure des mots acentués est rendue possible par l'utilisation d'un codage de police sur 8 bits, les commandes de césure sont elles aussi rendues correctes par l'utilisation de ce même codage de police sur 8 bits. Pour les motifs de césure historiques, l'encodage est {doc}`Cork </5_fichiers/fontes/que_sont_les_fontes_ec>`, de sorte que le code complet est :

```latex
% !TEX noedit
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\selectlanguage{french}
\hyphenation{r\'e-f\'e-rence}
```

Le même type de comportement s'applique à toute langue pour laquelle des polices 8 bits et des motifs de césure correspondants sont disponibles. Puisque vous devez sélectionner à la fois la langue et l'encodage de la police pour que votre document soit correctement composé, il ne devrait pas être trop contraignant de faire les bons choix avant de mettre en place les exceptions de césure.

Les variantes modernes de TeX {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` et {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`) utilisent [Unicode](https://fr.wikipedia.org/wiki/Unicode) en interne, et les distributions qui les proposent offrent également des motifs encodés en UTF-8.

Grâce au travail effectué par l'équipe chargée de la césure, l'utilisation de la césure en Unicode est faussement similaire à ce à quoi nous sommes habitués, et vous ne devriez rencontrer aucun problème.

______________________________________________________________________

*Sources :*

- {faquk}`Hyphenation exceptions <FAQ-hyphexcept>`,
- [Where can I find a list of English hyphenation exceptions?](https://tex.stackexchange.com/questions/22867/where-can-i-find-a-list-of-english-hyphenation-exceptions)

```{eval-rst}
.. meta::
   :keywords: LaTeX,césure,coupure des mots,règles de coupure des mots
```

