# Comment fonctionne la césure en TeX ?

La césure, c'est la coupure des mots en fin de ligne, comme ici dans « bou-gie » :

```latex
\documentclass{article}
  \usepackage[width=5cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
\Large
Longtemps, je me suis couché de bonne heure.
Parfois, à peine ma bougie éteinte, mes yeux
se fermaient si vite que je n’avais pas
le temps de me dire : \og{}Je m’endors.\fg{}
\end{document}
```

Elle est utilisée dans la plupart des livres et journaux, et doit suivre quelques règles pour rester discrète. Quand elle est faite à de mauvais emplacements dans les mots, elle ralentit la lecture et oblige le lecteur à revenir en arrière, par exemple ici :

```latex
\documentclass{article}
  \usepackage[width=4.6cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
\Large
Longtemps, je me su\-is couché de bonne heu\-re.
Parfois, à peine ma bo\-ugie éteinte, mes y\-eux
se fermaient si vit\-e que je n’avais pas
le temps de me dire : \og{}Je m’endors.\fg{}
\end{document}
```

Les règles de césure sont déterminées par le bon sens, mais aussi par la culture, et pour une même langue, on peut avoir des styles différents selon le pays --- par exemple, les styles de césure de l'anglais en Grande-Bretagne et aux États-Unis sont très différents. Par conséquent, un logiciel de mise en page utilisé dans le monde entier doit pouvoir adapter ses règles de césure.

Le système de césure de TeX est plutôt réputé pour sa qualité. Il a été conçu au départ par Frank Liang, dans le cadre de [sa thèse de doctorat](https://tug.org/docs/liang/). Bien qu'il soit capable de rater des points de césure qui paraîtraient sensés à un humain, il choisit rarement des points grossièrement erronés. L'algorithme prends les « candidats à la césure », c'est à dire des suites de lettres, et il les compare à un ensemble de « motifs de césure ». Les caractères qui ne sont pas des lettres ne peuvent pas apparaître dans un candidat, c'est ainsi que la primitive `\accent` de TeX empêche la césure, tout comme les signes de ponctuation (cela dit, la césure a lieu sur les caractères « envoyés à l'imprimante ». Donc le problème avec `\accent` est évité -- en LaTeX -- par l'utilisation du paquet `fontenc`, comme l'explique la page « {doc}`Pourquoi les mots accentués ne sont-ils pas coupés? </3_composition/langues/cesure/coupures_de_mots_accentues>` »)

Les ensembles de motifs de césure sont généralement dérivés de l'analyse d'une liste de césures valides. Pour TeX et LaTeX, ce processus de dérivation a été fait à l'aide de l'outil {ctanpkg}`patgen`, mais le commun des mortels n'a pas besoin de savoir faire ça.

Pour chaque langue utilisée, les motifs doivent avoir été installés pour que le moteur TeX puisse s'en servir. Pour modifier l'ensemble des motifs de césure reconnus par un système TeX ou {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>`, {doc}`une réinstallation partielle est nécessaire </3_composition/langues/utiliser_une_nouvelle_langue_avec_babel>` (notez que {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` assouplit cette contrainte).

TeX fournit deux commandes à l'utilisateur pour le contrôle de la césure : `\language` (qui sélectionne un style de césure), et `\hyphenation` (qui donne des instructions explicites au moteur de césure, généralement pour les cas particuliers ou les nouveaux mots, annulant l'effet des motifs chargés par ailleurs). L'utilisateur ordinaire de LaTeX n'a pas à se soucier de la commande `\language`, puisque l'extension {ctanpkg}`babel` s'en occupe pour lui ; l'utilisation de `\hyphenation` est expliqué {doc}`sur la page évoquant les problèmes de césure </3_composition/langues/cesure/la_cesure_ne_fonctionne_pas>`.

______________________________________________________________________

*Sources :*

- {faquk}`How does hyphenation work in TeX? <FAQ-hyphen>`
- [Coupure de mots en fin de ligne](http://www.visezjuste.uottawa.ca/pages/orthographe/coupure_des_mots.html) (Université d'Ottawa).

```{eval-rst}
.. meta::
   :keywords: LaTeX,moteur de césure,coupure de mots,motifs de césure,hyphenation patterns
```

