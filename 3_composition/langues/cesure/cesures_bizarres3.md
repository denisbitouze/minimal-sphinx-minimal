# Pourquoi les coupures de mots sont bizarres ?

Si vos mots sont coupé-s en fin de l-igne, comme ceci, avec seulemen-t des lettres isolées en début ou en fin de mot, vous avez peut-être un problème de version. Le système de césure de TeX a changé entre la version 2.9 et la version 3.0, et les macros écrites pour être utilisées avec la version 2.9 peuvent avoir cet effet quand elles sont utilisées avec un moteur de version 3.0. Si vous utilisez Plain TeX, assurez-vous que votre fichier `plain.tex` a un numéro de version supérieur ou égal à 3.0, et reconstruisez votre format.

Si vous utilisez LaTeX 2.09, le mieux est de passer à LaTeX2ε. Si, pour une raison quelconque, vous ne pouvez pas le faire, la dernière version de LaTeX 2.09 (publiée le 25 mars 1992) est toujours disponible (pour le moment du moins) et devrait résoudre ce problème.

:::{important}
La version 3.0 de TeX est sortie en 1990, LaTeX2ε en 1994... Il y a de fortes chances que l'explication ci-dessus ne vous concerne pas.
:::

Si vous utilisez LaTeX2ε, le problème vient probablement de votre fichier `hyphen.cfg`, qui doit être créé si vous utilisez un système multilingue.

Une autre source de bizarrerie peut provenir de la version 1995 des {doc}`polices de Cork </5_fichiers/fontes/que_sont_les_fontes_ec>`, qui a introduit un nouveau caractère pour le trait d'union. Dans cette version de police, les fichiers de configuration de LaTeX2ε utilisaient ce nouveau trait d'union, ce qui pouvait produire des effets bizarres avec des mots contenant explicitement un trait d'union. La version de LaTeX2ε de décembre 1995 contient des fichiers de configuration qui n'utilisent *pas* ce nouveau trait d'union, ce qui élimine cette source de problèmes. Le problème devrait donc être résolu depuis longtemps.

______________________________________________________________________

*Source :* {faquk}`Weird hyphenation of words <FAQ-weirdhyphen>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,césure,hyphenation,coupure de mots en fin de ligne,coupures de mots incorrectes,traits d'union,césure incorrecte
```

