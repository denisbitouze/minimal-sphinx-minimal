# Comment obtenir une césure dans un mot ou groupe de mots qui contient déjà un trait d'union ?

## Problème

Par défaut, TeX {doc}`coupe tout mot composé à l'endroit exact du trait d'union et uniquement à cet endroit </3_composition/langues/cesure/la_cesure_ne_fonctionne_pas>`. C'est une cause possible de {doc}`débordement </3_composition/texte/paragraphes/latex_fait_des_lignes_trop_longues>` (avertissement `overfull hbox`), comme dans l'exemple suivant :

```latex
Il fut un temps où la Russie était gouvernée depuis la ville de Saint-Pétersbourg.
Les tsars y habitaient et y gouvernaient. Puis les bolchéviks déplacèrent
le centre du pouvoir à Moscou. Aujourd'hui le pouvoir n'est plus détenu par les
bolchéviks, mais la capitale est toujours Moscou.
```

Le problème peut aussi se produire avec des groupes verbaux, comme « définissez-le » ou « observèrent-ils ».

Deux approches sont alors possibles

- Soit l'on force TeX à introduire quand même une césure ;
- Soit l'on convient qu'introduire une coupure de mots là où il y a déjà un trait d'union n'est pas du meilleur effet, auquel cas il faut trouver une autre solution.

## Forcer la césure

### Commandes de TeX

Dans ce cas, la [commande \\hyphenation](/3_composition/langues/cesure/introduire_des_coupures_de_mots#globalement) ne fonctionnera pas pour la raison décrite plus haut. Cependant, vous pouvez utiliser la [commande \\-](/3_composition/langues/cesure/introduire_des_coupures_de_mots#localement) à l'endroit désiré.

```latex
Il fut un temps où la Russie était gouvernée depuis la ville de Saint-Péters\-bourg.
Les tsars y habitaient et y gouvernaient. Puis les bolchéviks déplacèrent
le centre du pouvoir à Moscou. Aujourd'hui le pouvoir n'est plus détenu par les
bolchéviks, mais la capitale est toujours Moscou.
```

### Extensions

L'extension {ctanpkg}`babel` possède une commande `\allowhyphens` qui permet de laisser à TeX le choix des autres coupures dans le mot. Cette commande se place à gauche ou à droite du trait d'union suivant la partie où l'on souhaite autoriser la césure, soit ici :

```latex
% !TEX noedit
Saint-\allowhyphens{}Pétersbourg
```

Cette méthode a l'avantage de laisser à TeX le soin de déterminer la césure dans la deuxième partie du mot. Notez que cette commande ne fonctionne pas avec un encodage T1 ; il est peu probable que vous soyez concerné·e.

```{eval-rst}
.. todo:: **À intégrer :** D'autres extensions sont mentionnées `ici <https://stackoverflow.com/questions/2193307/how-do-i-get-latex-to-hyphenate-a-word-that-contains-a-dash>`__.
```

## Éviter d'introduire un deuxième tiret et trouver une autre solution

### Réécrire la phrase

Souvent, il suffit d'une petite reformulation pour faire disparaître le problème. Par exemple :

```latex
Autrefois la Russie était gouvernée depuis la ville de Saint-Pétersbourg.
Les tsars y habitaient et y gouvernaient. Puis les bolchéviks déplacèrent
le centre du pouvoir à Moscou. Aujourd'hui le pouvoir n'est plus détenu par les
bolchéviks, mais la capitale est toujours Moscou.
```

N'appliquez cette correction que lorsque le texte et a mise en page sont établis définitivement : sinon, il est possible que le problème se résolve de lui-même au cours de la rédaction.

### Suggérer un passage à la ligne au niveau du trait d'union

Vous pouvez suggérer à TeX de passer à la ligne au niveau du tiret existant en utilisant la [commande \\linebreak](/3_composition/texte/paragraphes/latex_fait_des_lignes_trop_longues#avec_la_commande_linebreak).

```latex
Il fut un temps où la Russie était gouvernée depuis la ville de Saint-\linebreak[4]Pétersbourg.
Les tsars y habitaient et y gouvernaient. Puis les bolchéviks déplacèrent
le centre du pouvoir à Moscou. Aujourd'hui le pouvoir n'est plus détenu par les
bolchéviks, mais la capitale est toujours Moscou.
```

Dans le cas présent, il est nécessaire de donner la valeur la plus élevée (4) à la commande `\linebreak` car elle provoque un espacement très important entre les mots de la première ligne. Cela pourrait constituer une gêne si la ligne suivante était beaucoup plus resserrée.

Notez que dans le cas où vous avez spécifié une valeur de 4 à `\linebreak`, il faudra penser à supprimer la commande si vous réécrivez le texte plus tard, sans quoi vous pourriez vous retrouver avec le résultat suivant :

```latex
La Russie était gouvernée depuis la ville de Saint-\linebreak[4]Pétersbourg.
Les tsars y habitaient et y gouvernaient. Puis les bolchéviks déplacèrent
le centre du pouvoir à Moscou. Aujourd'hui le pouvoir n'est plus détenu par les
bolchéviks, mais la capitale est toujours Moscou.
```

### Modifier une coupure de ligne précédente

Lorsque le mot qui déborde n'est pas sur la première ligne du paragraphe, il est possible de faire terminer une des lignes précédentes légèrement avant ou après l'endroit fixé par l'algorithme de TeX. Considérons cet exemple :

```latex
Aujourd'hui, nous savons tous que la capitale de la Russie est Moscou.
Cependant, encore au début du XX\textsuperscript{e} siècle,
l'empereur vivait à Saint-Pétersbourg.
Puis les bolchéviks déplacèrent le centre du pouvoir à Moscou.
```

Ici, le léger débordement est causé par le fait que TeX ne coupe pas « cependant » à la première syllabe. De ce fait, on constate également que la première ligne a un espacement entre les mots exagérément large, surtout comparé à la deuxième où les mots sont reserrés au maximum. Si l'on ne peut pas reformuler le texte, une solution envisageable est d'autoriser la césure entre les deux premières syllabes de « cependant ». Cela peut paraître, en soi, inélégant, mais ce n'est pas inacceptable, et l'on évite du même coup le débordement et une différence d'espacement trop visible entre les deux premières lignes.

```latex
Aujourd'hui, nous savons tous que la capitale de la Russie est Moscou.
Ce\-pendant, encore au début du XX\textsuperscript{e} siècle,
l'empereur vivait à Saint-Pétersbourg.
Puis les bolchéviks déplacèrent le centre du pouvoir à Moscou.
```

:::{note}
Pour les besoins de l'exemple, nous avons délibérément omis de charger {ctanpkg}`babel` ou {ctanpkg}`polyglossia` avec l'option `french` : en effet, l'algorithme de césure pour le français aurait coupé « cependant ».
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX, coupure
```

