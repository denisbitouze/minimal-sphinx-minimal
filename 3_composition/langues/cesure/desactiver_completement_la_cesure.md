# Comment désactiver complètement la coupure des mots ?

Cela peut sembler une drôle d'idée (après tout, la qualité de son algorithme de césure est l'une des forces de TeX) mais c'est parfois nécessaire.

:::{important}
Si vous utilisez couramment TeX, vous avez dû vous habituer à obtenir automatiquement des blocs de texte plutôt esthétiques. Or ceci est largement dépendant de l'utilisation de la césure... Si vous abandonnez la césure, vous risquez d'être déçu par le résultat.
:::

TeX offre quatre façons différentes de supprimer la césure (il n'y en avait que deux jusqu'à la version 3 de TeX).

## Par les pénalités de coupure de mots

Tout d'abord, on peut donner aux pénalités de césure `\hyphenpenalty` et `\exhyphenpenalty` des valeurs « infinies » (c'est-à-dire 10000, pour TeX). Cela signifie que toute césure pénalisera suffisamment la ligne qui la contiendrait pour qu'elle n'ait pas lieu. L'inconvénient de cette méthode est que le moteur TeX recalculera tout paragraphe pour lequel les césures pourraient être utiles, ce qui le ralentira.

**Césure standard** :

```latex
% !TEX noedit
%
%
```

```latex
\documentclass[11pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=5.65cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Sans césure** :

```latex
% !TEX noedit
\hyphenpenalty=10000
\exhyphenpenalty=10000
```

```latex
\documentclass[11pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=5.65cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\hyphenpenalty=10000
\exhyphenpenalty=10000

\begin{document}
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

## En utilisant une langue sans motifs de césure

On peut sélectionner une langue pour laquelle aucun motif de césure n'existe. Certaines distributions créent pour cela une langue `nohyphenation`, et le paquetage {ctanpkg}`hyphenat` utilise cette technique pour sa commande `\nohyphens`, qui compose son argument sans césure. Vous pouvez charger {ctanpkg}`hyphenat` avec la commande :

```latex
% !TEX noedit
\usepackage[none]{hyphenat}
```

pour empêcher toute césure dans un document monolingue.

Cette technique ne peut pas fonctionner dans un document dans lequel {ctanpkg}`babel` est utilisé pour sélectionner la langue, puisque {ctanpkg}`babel` s'occupe de régler les paramètres de césure en même temps qu'il change la langue.

## En jouant sur les longueurs minimales des fragments coupés

Les variables `\lefthyphenmin` et `\righthyphenmin` définissent la longueur minimale des fragments à gauche et à droite (respectivement) d'une coupure de mot, en nombre de caractères.

Les valeurs standard en français et anglais sont :

```latex
% !TEX noedit
\lefthyphenmin=2
\righthyphenmin=3
```

Si vous leur donnez une valeur plus grande que la longueur du plus long mot existant, vous désactiverez de fait la césure. La valeur généralement conseillée est 

`62`

.

{octicon}`alert;1em;sd-text-warning`

 Les trois exemples ci-dessous n'utilisent pas 

{ctanpkg}`babel`

, pour un résultat plus illustratif.

**Césure standard** :

```latex
% !TEX noedit
\lefthyphenmin=2
\righthyphenmin=3
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=2
\righthyphenmin=3

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Petites valeurs** :

```latex
% !TEX noedit
\lefthyphenmin=1
\righthyphenmin=1
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=1
\righthyphenmin=1

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Grandes valeurs** :

```latex
% !TEX noedit
\lefthyphenmin=62
\righthyphenmin=62
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=62
\righthyphenmin=62

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

:::{important}
`\left-` et `\righthyphenmin` reprennent leurs valeurs par défaut à chaque changement de langue.
:::

## Par la méthode utilisée par LaTeX en interne

Enfin, on peut supprimer la césure pour tout texte utilisant la police courante, grâce à la commande :

```latex
% !TEX noedit
\hyphenchar\font=-1
```

Ce n'est pas un moyen spécialement commode pour l'utilisateur, puisque cette commande doit être répétée pour chaque police utilisée dans le document, mais c'est ainsi que LaTeX supprime la césure dans les polices `tt` et autres polices à chasse fixe.

## Comment choisir ?

Cela dépend de ce que vous voulez réellement faire... Si le texte dont la césure doit être désactivé s'étend sur moins d'un paragraphe, votre seule possibilité est d'utiliser la langue sans césure : en effet, la valeur de la langue peut être changée en cours de paragraphe (comme la police courante), et avoir immédiatement un effet, alors que les valeurs des pénalités et des longueurs minimales des fragments ne sont utilisées qu'à la fin du paragraphe en cours pour mettre celui-ci en forme.

En revanche, si vous écrivez un document multilingue en utilisant l'extension {ctanpkg}`babel`, vous *ne pouvez pas* désactiver la césure en utilisant une langue sans césure ou les longueurs minimales des fragments, car toutes ces valeurs sont modifiées à chaque changement de langue par {ctanpkg}`babel`. Donc dans ce cas, utilisez plutôt les pénalités.

## Comment améliorer le résultat ?

Si vous vous contentez de désactiver la césure pour une bonne partie du texte, le bloc de texte aura une apparence « en dents de scie » sur la droite, et de nombreuses lignes seront trop longues. Pendant la compilation, (La)TeX vous bombardera d'avertissements `overfull/underfull boxes` (à chaque ligne trop longue ou trop courte, en fait). Pour éviter cela, vous avez deux options.

Le plus simple est d'utiliser la commande `\sloppy` (ou l'environnement correspondant `sloppypar`). Cela demande à TeX d'étirer le contenu des lignes insuffisamment remplies pour occuper tout l'espace disponible, et de couper les lignes trop pleines puis d'étirer le reste. Évidemment, le résultat ne sera pas très beau, mais il faut savoir faire des concessions.

La meilleure solution consiste à composer le texte {doc}`au fer à gauche </3_composition/texte/paragraphes/justifier_un_paragraphe_a_droite_ou_a_gauche>` (ou « en drapeau à droite »), avec la commande `\raggedright` (ou l'environnement correspondant `flushleft`), pour se débarrasser au moins des lignes trop longues; cette technique est « traditionnelle » (dans le sens où les dactylographes l'ont toujours fait) et le résultat n'est pas forcément contraire au bon goût typographique.

Vous pouvez comparer le rendu ici :

**Césure juste désactivée** :

```latex
% !TEX noedit
\lefthyphenmin=62
\righthyphenmin=62
%
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.6cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=62
\righthyphenmin=62

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Avec `\sloppy`** :

```latex
% !TEX noedit
\lefthyphenmin=62
\righthyphenmin=62
\sloppy
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.6cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=62
\righthyphenmin=62
\sloppy

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

**Avec `\raggedright`** :

```latex
% !TEX noedit
\lefthyphenmin=62
\righthyphenmin=62
\raggedright
```

```latex
\documentclass[10pt]{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=4.6cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\lefthyphenmin=62
\righthyphenmin=62
\raggedright

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{} Et, une demi-heure après, la pensée qu'il était temps de chercher le sommeil m'éveillait; je voulais poser le volume que je croyais avoir encore dans les mains et souffler ma lumière.
\end{document}
```

______________________________________________________________________

*Sources :*

- {faquk}`Stopping all hyphenation <FAQ-hyphoff>`,
- [How to prevent LaTeX from hyphenating the entire document?](https://tex.stackexchange.com/questions/5036/how-to-prevent-latex-from-hyphenating-the-entire-document)

```{eval-rst}
.. meta::
   :keywords: LaTeX,césure,coupure des mots,hyphenation,coupure en fin de ligne,désactiver la césure,désactiver la coupure des mots
```

