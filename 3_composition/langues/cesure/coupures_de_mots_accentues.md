# Comment corriger les coupures de mots accentués ?

Suivant la fonte avec laquelle on travaille (autrement dit suivant qu'elle dispose des caractères accentués ou non), il peut exister des problèmes d'interaction entre les lettres accentuées et les règles de coupure des mots. En effet, l'utilisation d'une fonte 7 bits impose que les caractères accentués soient fabriqués par la macro `accent` de TeX qui inhibe toute césure pour la suite du mot.

## Avec l'extension fontenc

En LaTeX, l'utilisation de fontes respectant la norme T1 (codage de Cork + 8 bits) permet d'éviter ces problèmes. Il faut alors faire appel à l'extension {ctanpkg}`fontenc` :

```latex
% !TEX noedit
\usepackage[T1]{fontenc}
```

Cette extension nécessite que des fontes utilisant le codage T1 (suffisamment récentes) aient été installées (fontes `ec` par exemple), ce qui est par défaut le cas sur les distributions récentes.

Il faut en outre utiliser des modèles de coupure de mots codés T1. Il existe pour cela deux fichiers disponibles sur le site du [CTAN](/1_generalites/documentation/le_catalogue_du_ctan). Il s'agit des fichiers `fr8hyph.ec` (ou mieux `f8hyph`, beaucoup plus récent) pour un codage 8 bits (fonte avec caractères accentués telle que ec) et `fr7hyph` (respectivement `f7hyph`) pour un codage 7 bits (accents TeX).

## Avec les moteurs XeTeX et LuaTeX

Avec [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex) et [LuaTeX](1_generalites/glossaire/qu_est_ce_que_luatex), un nouveau système de génération de tables de césure est en place. Pour chaque langue, une table est écrite en Unicode et des versions « 8 bits » sont générées pour une utilisation avec divers encodages de polices LaTeX. Les ensembles originaux de modèles restent sur le [CTAN](1_generalites/documentation/le_catalogue_du_ctan) et sont à réserver pour des environnements plus anciens.

## Méthodes historiques

### Avec TeX

Danc ce cas, un problème de version peut survenir : l'algorithme de coupure des mots a changé entre les versions 2.9 et 3.0. Ainsi si vous utilisez TeX V.3.0 ou plus, il faut veiller à ce que les fichiers `plain.tex` et `lplain.tex` soient également en version 3.0 ou plus.

### Avec le moteur MLTeX

Une solution consiste à utiliser MLTeX (multilingual TeX). C'est le [moteur](/1_generalites/glossaire/qu_est_ce_qu_un_moteur) TeX de Michael J. Ferguson. Il permet en particulier de gérer les coupures de mots accentués. Certaines des idées utilisées dans ce moteur ont d'ailleurs été reprises par la suite dans TeX V3.

______________________________________________________________________

*Source :* {faquk}`Accented words aren't hyphenated <FAQ-hyphenaccents>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,césure,coupure des mots
```

