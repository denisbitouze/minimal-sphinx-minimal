# Est-ce que TeX est compatible avec Unicode ?

[Unicode](https://fr.wikipedia.org/wiki/Unicode) est un schéma de codage des caractères qui a la capacité de représenter les textes de toutes les langues du monde, ainsi que des symboles importants (y compris les symboles mathématiques).

Nativement, TeX comprend les schémas de codage utilisant des octets simples (c'est-à-dire codant les caractères sur 8 bits [^footnote-1], et ne pouvant donc représenter que 256 caractères au maximum). Or Unicode, pour pouvoir représenter un très grand nombre de caractères, [peut nécessiter plusieurs octets par caractère](https://fr.wikipedia.org/wiki/UTF-8).

Pour que les applications « à l'ancienne », comme TeX ou pdfTeX, puissent comprendre un fichier en Unicode, la séquence d'octets composant chaque caractère Unicode doit être traitée par une série de macros qui fournissent un numéro de glyphe dans une police appropriée. Les macros qui lisent le flux d'entrée sont compliquées, mais pour l'utilisateur, il suffit d'utiliser l'option `utf8` quand il charge l'extension {ctanpkg}`inputenc` (présente par défaut sur toute installation de LaTeX) :

```latex
% !TEX noedit
\usepackage[utf8]{inputenc}
```

Depuis 2018, il n'y a même plus besoin d'inclure cette ligne dans le préambule des documents : c'est l'option par défaut.

Cette option ne sait traiter que les caractères Unicode qui peuvent être représentés avec les « encodages standards de LaTeX ». Le paquet séparé {ctanpkg}`ucs` fournit une couverture plus large, mais moins robuste, via l'option `utf8x` de {ctanpkg}`inputenc`.

:::{important}
L'option `utf8` est très souvent suffisante.

En règle générale, vous ne devriez jamais utiliser `utf8x` avant d'être convaincu que `utf8` ne suffit pas.
:::

Les applications « modernes », telles {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` et {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`, lisent leur entrée en utilisant par défaut des représentations UTF-8 d'Unicode. Elles utilisent également des polices TrueType ou OpenType pour leur sortie; or chacune de ces polices possède des tables qui indiquent à l'application quelle(s) partie(s) de l'espace Unicode elle couvre, ce qui permet au moteur de décider quelle police utiliser pour tel ou tel caractère (si besoin).

Tout ce ceci fait qu'UTF-8 est maintenant complètement supporté par LaTeX, et que vous n'avez plus besoin d'avoir peur de convertir vos documents anciens.

______________________________________________________________________

*Source :* {faquk}`Unicode and TeX <FAQ-unicode>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,concepts,luatex,unicode,xetex,UTF8,UTF16,UTF-8,UTF-16,encodage
```

[^footnote-1]: Jusqu'en 1989, TeX utilisait même un codage sur 7 bits, ne pouvant représenter que 128 caractères, soit juste l'alphabet anglais plus quelques caractères spéciaux. D'où {doc}`la syntaxe historique pour mettre des acccents sur les lettres </3_composition/texte/symboles/caracteres/accents_divers>` : `\'e` pour « é », etc.

