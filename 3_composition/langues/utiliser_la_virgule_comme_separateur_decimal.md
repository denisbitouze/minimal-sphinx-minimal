# Comment faire de la virgule le séparateur décimal ?

TeX utilise par défaut la convention anglophone : le point sert de séparateur entre la partie entière du nombre et sa partie décimale. Dès lors, quand TeX sert à mettre en forme un document français, il ne réagit pas tout à fait bien à la présence de la virgule comme séparateur décimal : vous noterez une petite espace placée à sa suite de cette dernière, ce qui ne donne pas une présentation conforme aux règles françaises.

## Sans extension

Une solution simple à ce problème, en mode mathématique, consiste à taper `3{,}14` au lieu de `3,14`. Bien qu'une telle technique puisse produire ponctuellement le résultat souhaité, elle ne se généralise que très mal à un document entier. Les solutions qui suivent lui sont donc préférables.

```latex
\documentclass{report}
\pagestyle{empty}
\begin{document}
Avant :
\[ 123,45 \]

Après :
\[ 123{,}45 \]
\end{document}
```

## Avec l'extension « icomma »

L'extension {ctanpkg}`icomma` garantit qu'il n'y aura plus cette espace supplémentaire après une virgule. Toutefois, si *vous* placez une espace après la virgule (comme dans `f(x, y)` où, en l'absence de l'extension, vous n'avez pas besoin de cette espace) alors le petit espace habituel après la virgule apparaît.

## Avec l'extension « ziffer »

L'extension {ctanpkg}`ziffer` est spécifiquement pensée pour de la composition en allemand. Elle couvre toutefois le besoin soulevé ici, tout en fournissant le signe double moins utilisé en allemand (et dans d'autres langues) pour représenter les centimes nuls dans un montant en devise.

## Avec l'extension « numprint »

L'extension {ctanpkg}`numprint` fournit une commande `\numprint{`*nombre*`}` qui met en forme son argument *nombre* selon les paramètres que vous lui donnez, ou selon les paramètres choisis en accord avec la langue que vous avez sélectionnée dans {ctanpkg}`babel`. Cette commande fonctionne aussi bien dans du texte que dans des mathématiques. La commande est également très flexible : elle peut regrouper les chiffres de nombres « longs ». Cependant, l'extension reste moins pratique que {ctanpkg}`icomma` ou {ctanpkg}`ziffer` si vous tapez beaucoup de chiffres.

```latex
\documentclass{report}
\usepackage{numprint}
\pagestyle{empty}
\begin{document}
\numprint{123456.78}
\end{document}
```

## Avec l'extension « siunitx »

L'extension {ctanpkg}`siunitx` reprend quelques idées de {ctanpkg}`numprint` mais répond à une question plus large : celle de la présentation de quantité dans les [unités du système international (SI)](https://fr.wikipedia.org/wiki/Syst%C3%A8me_international_d%27unit%C3%A9s). Pour le cas présent, elle propose une option permettant de gérer le choix du séparateur décimal sur l'ensemble du document : `output-decimal-marker`. En voici un exemple.

```latex
\documentclass{report}
\usepackage{numprint}
\pagestyle{empty}
\begin{document}
\numprint{123456.78}
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`The comma as a decimal separator <FAQ-dec-comma>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage,séparateur décimal
```

