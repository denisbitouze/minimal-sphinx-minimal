# Comment saisir du texte dans plusieurs alphabets ?

## Généralités

La composition du texte se fait à l'aide d'un éditeur. Cet éditeur doit permettre la saisie des caractères, glyphes des différents langages. Grâce au support de l'UTF-8, LaTeX permet maintenant de saisir les caractères exactement comme ils doivent être imprimés et non en [beta code](https://fr.wikipedia.org/wiki/Beta_Code) [^footnote-1]. Aujourd'hui, l'usage du beta code (p. ex. `<a` pour ἁ dans un environnement `greek`) semble devoir se réduire à l'insertion ponctuelle de quelques mots, sauf dans de rares cas où une extension offre des fonctionnalités reposant sur un encodage en beta code que l'on ne pourrait pas avoir avec une saisie directe des caractères (p. ex. avec {ctanpkg}`ArabLuaTeX <arabluatex>`).

Nous nous intéressons ici uniquement à la saisie directe des caractères unicodes, en envisageant plusieurs cas. Si vous souhaitez saisir du texte en beta code, vous pouvez {doc}`voir un exemple pour le grec </3_composition/langues/composer_un_document_latex_en_grec_moderne_ou_classique#exemple_complet_de_saisie_en_beta_code_avec_pdflatex>` et vous référer à la documentation des extensions correspondantes.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *Préciser où on peut trouver les infos* »
```

### Saisie de deux langues différentes

Le premier cas est celui de la saisie de deux langues différentes.

#### Éditeur de texte

La saisie des langues européennes pourra se faire avec n'importe quel éditeur généraliste à condition de disposer des fontes de caractères correspondantes. Étant donné que les {doc}`bons éditeurs </6_distributions/editeurs/start>` utilisent aujourd'hui un encodage UTF-8 par défaut, on ne devrait pas rencontrer de problèmes à partir du moment où l'on utilise une fonte unicode qui couvre la langue ciblée.

#### Clavier

Pour une saisie ponctuelle, une solution peut être utiliser un clavier virtuel en ligne, comme ceux du site [Lexilogos](https://www.lexilogos.com). Si l'on souhaite saisir de longs textes, il peut être plus pratique de changer sa disposition de clavier sur son ordinateur.

Pour les langues « alphabétiques », il suffit de changer la disposition du clavier. Une applet est disponible dans le « panel », que ce soit pour Linux, mac ou Windows.

Sous Linux, il faut installer les fontes de caractères correspondantes, et configurer l'applet de changement de disposition de clavier (KDE, Gnome) ou installer un programme indépendant de commutation de claviers. Référez-vous à la documentation de votre environnement de bureau.

Sous Windows, il faut aller dans le panneau de configuration, et ajouter les langues voulues (clavier, fontes, et locales).

Sous Mac, la procédure doit être identique.

On peut alors soit apprendre par cœur la disposition des touches sur le clavier, soit acheter un clavier multilingue, soit acheter des autocollants transparents que l'on peut coller sur les touches de notre clavier pour obtenir un clavier bilingue.

### Écrire de droite à gauche

Pour les langues comme l'hébreu ou l'arabe qui s'écrivent de droite à gauche, un éditeur généraliste moderne devrait faire l'affaire. Toutefois, il existe aussi des éditeurs spécialisés, créés généralement quand les éditeurs généralistes offraient un support limité. Peut-être certains offrent-ils encore des fonctionnalités intéressantes.

Voici quelques références d'éditeurs spécialisés pour l'hébreu, l'arabe et d'autres langues :

- `he2` : pour l'hébreu et l'anglais : <http://sourceforge.net/projects/he2> ;
- `heb` : pour l'hébreu et l'anglais.
- Pour l'hébreu, [ce site](http://www.qumran.org/ftp/local/hebrew/wordfont/files.htm) recense de nombreux autres éditeurs commerciaux ou non, ainsi que des fontes et utilitaires ;
- `Summisoft` : pour l'arabe <http://www.summitsoft.co.uk/> (Windows) ;
- `axmedit` : pour l'arabe, le farsi, le coréen et l'hébreu <http://www.langbox.com/arabic/axmedit.html> (Linux) ;

### Langues à idéogrammes

```{eval-rst}
.. todo:: Il serait bon que quelqu'un de compétent vérifie et actualise si besoin.
```

La saisie de textes en langue orientale (à idéogrammes) nécessite l'utilisation d'une « méthode de saisie ». En effet, le nombre très importants de glyphes de certaines langues asiatiques (chinois, japonais) ne permet pas d'associer une touche de clavier à chaque glyphe. Celle-ci consiste en un programme particulier qui se place entre le clavier et l'éditeur.

Ce programme capture les caractères saisis au clavier et suivant ce qui est tapé, propose un choix à partir d'une analyse phonétique, structurale, ou un mélange des deux méthodes. Ce programme s'aide de dictionnaires de mots, de dictionnaires de phrases qu'il est possible d'enrichir.

Je détaillerai un peu plus loin.

La société Suse, récemment rachetée par Novell propose un "HOWTO" (en anglais) dédié aux langues asiatiques dites CJK, avec une description précise de la configuration des différents systèmes Linux. Il y a aussi une partie sur LaTeX et tous les logiciels associés : <http://www.suse.de/~mfabian/suse-cjk/>.

En mode « langue spéciale », la séquence de touches est transmise au programme de saisie spécifique qui la transforme en une chaîne de caractère dans la langue désirée.

Il y a une touche qui permet de passer d'une langue à l'autre (ou une combinaison de touches).

L'entrée pour les langues « CJK » se fait soit en mode phonétique "latin" ou translittération, soit en mode phonétique spécifique natif : Hiragana pour le japonais, Hangul pour le coréen, Zhuyin pour le chinois. Après saisie de la partie phonétique, s'il y a plusieurs possibilités, un choix est offert à l'utilisateur.

Voici quelques programmes qui se présentent sous la forme d'un serveur, d'un client, et de dictionnaires. Ceux-ci sont disponibles sur toutes les machines Linux ou Unix.

- `xim` : <http://www.opencjk.org/~yumj/project-chinput-e.html> ;
- `uim` ;
- `kinput` ;
- `xcin` ;
- `canna`.

Sur les machines Windows, il faut aller dans Panneau-de-configuration--Options régionales et installer le chinois. Ne pas oublier d'ajouter les « locales » correspondantes. Vous pouvez ajouter le japonais par la même occasion. [|Ici, une page d'explications.](http://ccat.sas.upenn.edu/~nsivin/chinp.html)

Sur les machines Mac, il faut installer le « CLK » (Chinese Language Kit (CLK)). [Voici une page en anglais pour les détails.](http://humanum.arts.cuhk.edu.hk/~cmc/power-mag/individual/chi-ho/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeurs de texte,interface,textes en plusieurs langues
```

[^footnote-1]: Nous utilisons *beta code* au sens large, y compris pour la saisie d'autres langues que le grec.

