# Pouvez-vous donner plus de détails sur les méthodes de saisie ?

- Les méthodes de saisie sont utilisées quand le nombre de glyphes utilisés par la langue sont très supérieurs au nombre de touche d'un clavier. Ces méthodes de saisie sont donc utilisées pour toutes les langues à idéogrammes.

Le principe de saisie est le suivant : une fois l'éditeur mis dans le mode «langue à idéogrammes» l'appui d'une touche ne va pas donner un caractère, mais faire apparaître une fenêtre (menu) qui va proposer différents choix en fonction de ce qui a déjà été tapé. Il existe de nombreuses méthodes de saisie qui sont fondées soit sur la phonétique, soit sur la structure, soit une combinaison des deux, soit le codage direct des caractères.

Une bonne méthode de saisie doit se contenter d'un nombre réduit de touches, comme 26 touches par exemple (disponibles sur des claviers occidentaux).

Voyons les méthodes de saisie suivant les langues :

## Quelles sont les méthodes de saisie du chinois ?

- Je vais donner les informations trouvées sur le site : <http://seba.ulyssis.org/thesis/im.php>, simplement traduites car je ne suis en aucun cas un spécialiste des langues orientales.

### Méthodes fondées sur la phonétique

- **pinyin** : c'est la méthode officielle des Nations Unies. elle utilise les caractères trouvés sur les claviers occidentaux en association avec le « ton ». Le « ton » est l'accentuation du mot. Cette méthode est assez verbeuse : zhong + guo or zhong1 + guo2 pour 2 glyphes originaux. Plutôt que de choisir les tons dans un menu, il est possible de préciser le ton avec un chiffre à la fin du son comme écrit ci-dessus. Sur la page citée juste au-dessus, vous pourrez trouver plus de détails sur le positionnement des « tons ».
- **shuangpin** : la méthode est dérivée de pinyin, mais avec une réduction sensible du nombre de touches à taper. En général, une ou deux touches suffisent.
- **jianpin** : cette méthode est intermédiaire entre les deux précédentes. Voici un tableau de comparaison entre les 3 méthodes pour les mêmes glyphes :

```latex
\begin{tabular}{|c|c|c|c|}
\hline
Hanzi   & Pinyin & ShuangPin & JianPin  \\
\hline
glyphe & a    & a   & a \\
glyphe & ku   & ku  & ku    \\
glyphe & chu      & uu  & iu    \\
glyphe & cuan     & cc  & cuj   \\
glyphe & zhang  & ag    & ah    \\
glyphe & shuang & ih    & uuh   \\
\hline
\end{tabular}
```

- **bopomofo** : c'est une méthode développée en 1913 et modifiée de nombreuses fois ensuite. Elle est toujours d'actualité à Taïwan où elle est la méthode officielle d'apprentissage. La saisie est fondée sur des formes calligraphiques dont certaines sont dérivées de caractères d'origine chinoise. La méthode repose sur 37 symboles, 21 consonnes, 16 voyelles et 5 tons. Le grand désavantage de cette méthode est qu'elle nécessite un clavier spécifique, car les touches ne sont pas disponibles sur un clavier occidental.

### Méthodes fondées sur la structure

- **wubizixing** : dans cette méthode, chaque glyphe peut être saisi par au plus 4 appuis de touches (2 au minimum). À chaque glyphe est associé une combinaison unique d'appui de touches. Cette méthode divise le radical en 5 groupes, eux-même divisés en 5 groupes. Ils sont assignés aux touches de A à Y. La touche Z sert d'échappement. Voici un tableau de présentation :

```latex
\begin{tabular}{|c|c|c|c|c|c|}
\hline
~  & Key 1  & Key 2 & Key 3 & Key 4 &   Key 5   \\
G1 & 1 1/G  & 1 2/F & 1 3/D & 1 4/S &   1 5/A   \\
G2 & 2 1/H  & 2 2/J & 2 3/K & 2 4/L &   2 5/M   \\
G3 & 3 1/T  & 3 2/R & 3 3/E & 3 4/W &   3 5/Q   \\
G4 & 4 1/Y  & 4 2/U & 4 3/I & 4 4/O &   4 5/P   \\
G5 & 5 1/N  & 5 2/B & 5 3/V & 5 4/C &   5 5/X   \\
\hline
\end{tabular}
```

- **wubihua** : cette méthode utilise le pavé numérique (chiffres de 1 à 5 ; 6 est le caractère d'échappement). À chaque chiffre est associé une partie de base du glyphe; En fait, le dessin du glyphe est décomposé comme pour une écriture manuelle, et à chaque chiffre est associé un mouvement. La lettre T française peut être décomposée en une barre verticale et une horizontale. C'est le même principe. Il suffit d'entrer les 5 chiffres consécutifs de la constitution du glyphe suivi du caractère d'échappement (6). Voir le tableau de correspondance sur le site précité.

- **cangjie** : cette méthode divise 24 radicaux en 4 groupes assignés aux touches suivant cette liste (La traduction est certainement approximative...) :

  - *groupe philosophique* (A, B, C, D, E, F, G),
  - *groupe du trait de crayon* (H, I, J, K, L, M, N),
  - *groupe des parties du corps* (O, P, Q, R),
  - *groupe du contour du caractère* (S, T, U, V, W, Y).

- **sucheng** : méthode dérivée de cangjie, mais avec une économie de touches à taper.

- **boshiamy** : méthode développée à Taïwan. Elle utilise les 26 touches du clavier. Elle tient compte de la forme du glyphe, de sa prononciation, et de sa signification. Chaque glyphe nécessite l'appui de 2 à 4 touches du clavier.

- **dayi** : cette méthode utilise toutes les touches disponibles du clavier, ce qui est un inconvénient. Seuls 2 appuis de touches sont nécessaires pour la saisie d'un glyphe.

- **sanjiao** : cette méthode «des trois coins» est dérivée de la méthode des 4 coins, utilisée pour la recherche de glyphes dans un dictionnaire.

### Méthodes fondées sur une combinaison phonétique-structure

Pas de détails trouvés.

- **tze-loi** : ...
- **renzhi** : ...

### Méthodes de saisie directe

Pas de détails trouvés.

- **dianbaoma** : ...
- **guojiquweima** : ...
- **neima** : ...

## Quelles sont les méthodes de saisie du japonais ?

- La méthode de saisie du japonais est phonétique. On saisit phonétiquement en caractères romains, et le programme qui supporte la méthode de saisie propose le glyphe correspondant qui existe en trois versions : hiraganas, katakanas ou kanjis.

Les informations suivantes ont été trouvées sur la page suivante : <http://www.escale-japon.com/accueil.php?page=faq&select=faq>

Le japonais utilise 3 systèmes d'écriture :

- **Hiraganas** : système syllabaire qui permet d'écrire les mots d'origine japonaise, et servent aussi de fonctions grammaticales (conjugaison).
- **Katakanas** : système syllabaire qui permet d'écrire les mots d'origine étrangère.
- **Kanjis** : système identique au chinois(idéogrammes) qui associe à chaque glyphe un mot, un sens sans aucune équivoque possible.

Voir cette page qui donne quelques informations supplémentaires : <http://www.physics.wustl.edu/~alford/japanese_latex.html>

## Quelles sont les méthodes de saisie du russe ?

- Méthode de saisie du russe : (valable pour toutes les langues européennes, et celles dérivées.)

C'est une méthode simple, car il y a correspondance directe entre les touches dessinées sur le clavier et le caractère correspondant.

Pour le russe, j'ai aussi vu une méthode de translittération qui traduisait les suites de caractères romains en caractères cyrilliques (méthode phonétique). Un tel mode est disponible dans `yudit`.

## Quelle est la méthode de saisie du grec ?

- La méthode est dérivée de la méthode de saisie simple, qui associe un caractère à une touche. Le clavier peut ici se trouver en deux états : clavier romain (latin) et clavier grec. Une touche (combinaison de touches) permet de commuter d'un mode à l'autre. On utilise les touches mortes (de composition) pour certains caractères.

Voir aussi «[Comment composer du texte en grec moderne ou classique?](/3_composition/langues/composer_un_document_latex_en_grec_moderne_ou_classique) ».

## Quelles sont les méthodes de saisie du coréen ?

- Il y a environ 8500 glyphes à saisir. On utilise 3 méthodes concurremment :
- **ASCII** : pour les caractères anglais.
- **Hangul** : les glyphes sont constitués d'une consonne suivie d'une voyelle avec une finale consonnique optionnelle. Il y a une vérification de la conformité du glyphe (son en cas d'erreur).
- **Hanja** : les glyphes Hanja ne peuvent être obtenus qu'à partir des glyphes Hangul. Il y a deux modes de conversion : simple et multiple. En cas de choix multiples, une liste est proposée.

## Quelles sont les méthodes de saisie du vietnamien ?

- La méthode est dérivée de la méthode simple une touche pour un caractère, mais modifiée pour supporter les particularités de la langue. elle vérifie la conformité des caractères saisis. Il y a des « tons » qui sont appliqués à certaines voyelles (a, e, i, o, u, y, a-circumflex, e-circumflex, o-circumflex, a-breve, o-horn, or u-horn).

# Pouvez-vous donner plus de détails sur Unicode ?

- Je vous renvoie aux sites suivants pour avoir plus de détails :
- <http://www.tuteurs.ens.fr/theorie/encodages.html>,
- <http://www.unicode.org/>.

Il y a aussi des listes de diffusion consacrées à Unicode. Voir votre lecteur de *news* préféré, et chercher « Unicode ».

## Comment configurer la saisie des idéogrammes orientaux sur Windows ?

- Voir les détails d'installation et de configuration à l'URL suivante : <http://ccat.sas.upenn.edu/~nsivin/chinp.html> (en anglais).

## Comment configurer la saisie des idéogrammes orientaux sur Mac ?

- Voir les détails d'installation et de configuration à l'URL suivante : <http://www.yale.edu/chinesemac/pages/input_methods.html>.

## Comment configurer la saisie des idéogrammes orientaux sur Linux/Unix

- Voici une liste d'URL qui vous donneront tous les détails de configuration et d'installation des méthodes de saisie. Elles sont disponibles sous forme de paquetages sur toutes les distributions.
- <http://seba.studentenweb.org/thesis/linux.php>,
- <http://www.suse.de/~mfabian/suse-cjk/input.html>,
- Voir le «debian chinese HOWTO» sur le site <http://www.debian.org/>

# Quels sont les problèmes de codage des textes ?

- Maintenant que votre texte est saisi, vous voulez le traiter avec LaTeX. Auparavant, il va falloir s'assurer que le codage de votre texte sera accepté par LaTeX (problème déjà entrevu auparavant). Vous avez plusieurs possibilités suivant l'éditeur utilisé.
- Vous avez utilisé un éditeur utf-8 : là, vous avez le choix d'utiliser soit {ctanpkg}`UCS-LaTeX <ucs>`, soit {doc}`OMEGA </1_generalites/histoire/developpement_du_moteur_tex#omega_et_aleph>`. Il faut savoir que toutes les langues ne sont pas supportées par ce programme. Une facilité est apportée par le paquetage optionnel `autofe` de {ctanpkg}`UCS-LaTeX <ucs>` qui permet de passer d'une langue à l'autre sans baliser le changement.
- Vous avez utilisé un éditeur utf-8 mais les langues que vous utilisez ne sont pas traitées par {ctanpkg}`UCS-LaTeX <ucs>` ou {doc}`OMEGA </1_generalites/histoire/developpement_du_moteur_tex#omega_et_aleph>`. Il vous faut utiliser un convertisseur de codage comme `iconv`. Voir dans le chapitre utilitaires pour d'autres programmes. Parfois, votre éditeur préféré vous permet de choisir le codage de sauvegarde du fichier, qui peut être différent de celui de la saisie. Voir `Emacs` par exemple. Vous transcoderez votre texte par exemple en codage CJK et traiterez le texte avec `CJK-LaTeX`. Le principe est le même pour `Ivritex` ou `THAI-LaTeX` ou `Arab-TeX`.
- Vous avez saisi votre texte avec un éditeur spécialisé, non utf-8, mais dont le codage est connu. Alors, soit l'éditeur permet de spécifier le codage de sauvegarde, soit il faut utiliser un convertisseur.
- Vous utilisez un éditeur « propriétaire » dont le format d'enregistrement n'est pas connu, qui ne dispose pas de facilité d'exportation vers un format utilisable, changez de programme... C'est inutilisable ! (Qui a dit « Word » ?!)

## Pouvez-vous donner des exemples d'utilisation de « UCS-LaTeX » ?

## Pouvez-vous donner des exemples d'utilisation d'OMEGA ?

## Pouvez-vous donner des exemples d'utilisation de « THAI-LaTeX » ?

## Pouvez-vous donner des exemples d'utilisation de « IvriTeX » ?

## Pouvez-vous donner des exemples d'utilisation de « CJK-LaTeX » ?

```{eval-rst}
.. meta::
   :keywords: LaTeX,méthodes de saisie,langues étrangères,langues à idéogrammes
```

