# Qu'est-ce que le codage des textes ?

- Le Codage des textes est la représentation des caractères sous forme de nombres ou suite de nombres. Un des codages très connu est l'ASCII qui code les caractères sans accents sur des nombres de 0 à 127 (7 bits).

Ce codage ne permet pas de représenter les caractères accentués utilisés dans la langue française. On a donc étendu le codage à 256 (sur 8 bits), et assigné les caractères régionaux aux nombres de 128 à 256.

Chaque pays a donc assigné ses caractères locaux et il y a donc des codages sur 8 bits différents pour chaque pays.

Les codages connus commencent par ISO-8859 plus un chiffre qui caractérise le pays (ISO-8859-1 pour le latin sans le signe euro).

Les codages ont été normalisés maintenant (norme ISO).

Pour les langues asiatiques qui n'utilisent pas un alphabet fini, mais des idéogrammes, ce codage sur 8 bits est très insuffisant. Seuls 256 caractères (glyphes) peuvent être codés. Ils ont donc codé leurs glyphes sur 2 octets, ce qui permet de disposer de 65000 glyphes différents. Plusieurs codages différents ont été utilisés pour la même langue, et sont utilisés concurremment (suivant les machines, les régions, les habitudes, les programmes utilisés), ce qui pose souvent le problème de la récupération des documents quand des programmes utilisent un codage propriétaire et ne sont plus maintenus. (voir déjà le problème du codage des langues asiatiques sur `MacWord` (version 2 de `Word`) qui ne permet pas de récupérer les textes sur les nouveaux Macs avec la nouvelle version de `Word`.)

Le codage sur 8 bits ne permet que l'emploi de deux langues dont l'anglais, sauf dans les cas de langues extrêmement proches comme l'allemand et le français. Il est possible de coder les caractères et accents particuliers de ces deux langues sur les 128 nombres supplémentaires.

Pour pouvoir utiliser plusieurs langues très différentes simultanément, il faut utiliser l'unicode, qui permet de représenter toutes les langues. Le codage se fait sur plusieurs octets, et avec des codages différents (utf-8, utf-16, etc)

Voir plus loin pour les détails.

Les programmes de saisie de textes «non utf-8» doivent donc pouvoir distinguer le passage d'une langue à l'autre. Ils utilisent pour cela soit le 8{sup}`e` bit du nombre pour savoir si le caractère est régional ou pas, ou utilisent un système de balises comme le fait LaTeX.

Encore faut-il que ce balisage, codage soit documenté pour la pérennité des documents saisis.

Les éditeurs utf-8 utilisent le codage utf-8 pour représenter les glyphes, caractères, et ne nécessitent donc pas de balisage de changement de langue.

## Qu'est-ce que le codage des polices de caractères ?

- Comme le codage des textes vu au-dessus, il a fallu représenter les glyphes (dessins) des caractères sous forme de nombre (l'ordinateur ne travaille qu'avec des nombres).

Il y a donc une correspondance (bijection...) entre le codage d'un caractère de texte, et son codage dans la fonte.

Il y a des quantités de codages, et LaTeX utilise des codages particuliers (il assigne les glyphes à sa façon). Il permet en plus d'utiliser des fontes virtuelles qui sont composées de caractères piochés dans plusieurs polices.

Nous verrons plus loin comment expliquer à LaTeX quel codage est utilisé pour le texte, et quel codage est utilisé pour la fonte.

## Comment traiter les textes précédemment saisis avec LaTeX ?

```{eval-rst}
.. todo:: Actualiser (notamment sur l'encodage), réduire les exemples et documenter toutes les commandes utilisées.
```

Suivant le codage utilisé pour la saisie, il sera peut-être nécessaire de procéder à un transcodage du texte. Par exemple, `Emacs` utilise un codage propriétaire (mais documenté ! c'est un [logiciel GNU](https://fr.wikipedia.org/wiki/Projet_GNU)). Il faudra configurer le codage de sauvegarde du texte pour qu'il soit utilisable par LaTeX. Soit [utf-8](https://fr.wikipedia.org/wiki/UTF-8) pour traitement par {doc}`OMEGA </1_generalites/histoire/developpement_du_moteur_tex#omega_et_aleph>` ou `UCS-LaTeX`, ou [cjk](https://fr.wikipedia.org/wiki/Chinois,_japonais,_coréen_et_vietnamien) pour traitement par `CJK-LaTeX`.

Le paquetage {ctanpkg}`babel` permet de préciser le codage des textes avec l'option `inputenc`. Il faudra en plus expliciter le « fontencoding » pour préciser le codage de la police utilisée. Ce paquetage permet de traiter de nombreux langages. Se reporter à la documentation de {ctanpkg}`babel` pour les langues disponibles.

Voici quelques exemples d'utilisation de {ctanpkg}`babel`, `CJK-LaTeX`, etc :

### Un exemple avec « babel »

Le texte a été saisi avec `gvim` en utilisant ma locale par défaut (FR), et le codage est donc ISO-8859-1.

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[latin1]{inputenc}
  \usepackage[frenchb]{babel}
% Commentaire : fin du preambule

\begin{document}

  \title{Titre de mon article}
  \author{Le Monsieur \and La Madame}
  \date{Le \today}

  \maketitle

\begin{abstract}
  Résumé de mon article, passionnant.
\end{abstract}

\tableofcontents

\section{Explications}

\texttt{fontenc}, \texttt{inputenc}
sont des packages permettant d'utiliser
les accents.
\texttt{babel} sert à franciser le
document.

\end{document}
```

### Un exemple avec « CJK-LaTeX »

Cet exemple a été saisi avec `Emacs` en utilisant la version `mule` qui permet de saisir des textes en multiples langues. Le codage interne est spécifique à `Emacs`, mais il est possible de spécifier avec quel codage on veut que soit écrit le fichier sur le disque. Le texte `chinese.tex` a été reproduit tel quel (je ne comprends pas le chinois) et j'en ai seulement traduit la partie en anglais.

Le texte a été traité par `CJK-LaTeX` et `PDF-TeX`.

Il est difficile de montrer des exemples réels car cette FAQ est en langage national, et ne gère pas l'utf-8. Dommage !... Une solution serait d'inclure des images (exemples d'écran de travail), mais la taille de cette FAQ augmenterait d'une façon déraisonnable.

Notez la première ligne du texte qui indique à `Emacs` la nature de le codage du texte.

```latex
% !TEX noedit
%% -*- coding : emacs-mule -*-
\documentclass[12pt]{article}
  \usepackage{times}
  \renewcommand{\baselinestretch}{1.2}

\begin{document}
\Large
\begin{tabular}{l@{\hspace{5em}}l}
  texte en chinois \\
  texte en chinois \\[\medskipamount]
  chinois traditionnel & chinois simplifié \\
  chinois traditionnel & chinois simplifié \\
  chinois traditionnel & chinois simplifié \\
  chinois traditionnel & chinois simplifié \\
\end{tabular}

\bigskip
\normalsize
\noindent
Un poème de  Li\v{u} Z\=ong  (773--819), affiché
sur la partie gauche en chinois traditionnel, et
à droite en chinois simplifié.
\texttt{CJK-LaTeX} s'interface parfaitement avec
\texttt{Emacs-mule} de telle façon qu'il
est possible de saisir et d'afficher différents
types de fontes (Big5 pour les caractères
traditionnels, et GB2312 pour les caractères
simplifiés. Dans notre cas, c'est très utile, car
le caractère \emph{\v{a}i} ne fait pas partie de
la police GB2312.
J'ai donc substitué le caractère correspondant de
la fonte Big5.
Le fichier a été exporté en utilisant la fonction
\verb|cjk-write-file| fournie par le paquetage
\texttt{CJK-LaTeX}, et le résultat a été passé à
\texttt{PDF-LaTeX}.

Ici, signature en chinois\ldots

\end{document}
```

### Un exemple avec « IvriTeX »

Vous pourrez trouver des exemples dans la distribution de `IvriTeX` à l'URL suivante : <http://ivritex.sourceforge.net/>.

### Un exemple avec « THAI-LaTeX »

Vous pourrez trouver des exemples dans la distribution `THAI-LaTeX` à l'URL suivante : <http://ichris.ws/latex> ou dans votre distribution Linux adorée... Debian propose un paquetage `thai-latex`. Les exemples sont dans la section documentation.

### Un exemple avec « UCS-LaTeX »

Le texte a été saisi avec `gvim` en mode utf-8.

```latex
% !TEX noedit
\documentclass[french,8pts,a4paper]{article}

  \usepackage[notipa]{ucs}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[T3,T2A,T1]{autofe}
  \usepackage[greek,french,russian,english]{babel}


\title{Un titre}
\author{Auteur}

% commande \frt pour écrire du texte français
\newcommand\frt[1]{\foreignlanguage{french}{#1}}

% commande \rut pour écrire en russe
\newcommand\rut[1]{\foreignlanguage{russian}{#1}}

% commande grt pour écrire du texte en grec
\newcommand\grt[1]{\foreignlanguage{greek}{#1}}

% commande \phot pour écrire du texte en alphabet
% phonétique
\newcommand\phot[1]{\bgroup\fontencoding{T3}%
\selectfont\SetUnicodeOption{tipa}#1\egroup}

\begin{document}

\maketitle

\tableofcontents

\section{Je suis auteur de textes multilingues et
de dictionnaires : comment faire ?}

La composition de textes multilingues dans le cadre
d'un traitement
par \LaTeX{} doit se faire en ''UTF8''. En effet,
c'est le seul moyen pour\dots

\frt{Russe:}

Ici suit le texte en Unicode (russe)\dots

\rut{ texte en russe    }


\frt{Grec polytonique)} :

Ici suit le texte en grec :

\grt{ texte en grec }

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,inputenc,fontenc,codage des caractères,codage des polices
```

