# Comment mettre une note de bas de page dans un tableau ?

Pour des raisons trop longues à expliquer ici, il n'est pas possible d'utiliser directement la commande `\footnote` à l'intérieur d'un tableau : un appel à `\footnote` ne conserve que l'appel de note tandis que la note elle-même est perdue par l'environnement `tabular` ou ses équivalents.

Ceci correspond à la recommandation typographique classique : notes de bas de page et tables ne devraient pas être mélangées. Cependant, plusieurs solutions permettent de contourner cette recommandation. Elles sont regroupés en deux familles : celles travaillant avec des « notes de bas de table », particulièrement recommandées, et les autres d'utilisation plus ponctuelle (mais bien entendu fonctionnelles).

## Avec les méthodes utilisant des notes de bas de table

### Les extensions « threeparttable » et « threeparttablex »

L'extension {ctanpkg}`threeparttable` définit un environnement (du même nom) conçu pour faciliter la création de tables avec un titre et des notes de bas de table. Les références sont produites à l'aide de la commande `tnote`. Quant aux descriptions, elles sont précisées à l'aide de l'environnement `tablenotes`. L'exemple suivant met tout cela en pratique.

```latex
\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}

\usepackage{threeparttable}

\begin{document}

\begin{center}
 \begin{threeparttable}
  \caption{Les angles biaisés ($\beta$) pour $\fam0 Mu(H)+X_2$ et $\fam0 Mu(H)+HX$~\tnote{a}}
  \begin{tabular}{rlcc}% ou tabularx, etc.
   \hline
   &             & $\fam0 H(Mu)+F_2$      & $\fam0 H(Mu)+Cl_2$ \\
   \hline
   & $\beta$(H)  & $80,9^\circ\tnote{b}$  & $83,2^\circ$       \\
   & $\beta$(Mu) & $86,7^\circ$           & $87,7^\circ$       \\
   \hline
  \end{tabular}
  \begin{tablenotes}
   \item[a] pour la réaction d'abstraction, $\fam0 Mu+HX \rightarrow MuH+X$.
   \item[b] 1 degré${} = \pi/180$ radians.
  \end{tablenotes}
 \end{threeparttable}
\end{center}

\end{document}
```

Cette extension fonctionne bien avec le texte ordinaire et à l'intérieur de flottants. L'extension {ctanpkg}`threeparttablex` prend en plus en charge les tables `longtable`.

### L'extension « ctable »

L'extension {ctanpkg}`ctable` développe le modèle de {ctanpkg}`threeparttable` et utilise aussi les idées de {ctanpkg}`booktabs`. La commande `\ctable` effectue la totalité du travail : mettre en forme la table, placer la légende et définir les notes. La « table » peut être composée de diagrammes et un paramètre optionnel de `\ctable` permet d'avoir là un flottant traité comme une figure, plutôt que comme une table.

## Avec d'autres méthodes

### Avec l'environnement « minipage »

Une solution simple pour contourner le problème est d'inclure le tableau dans une `minipage`. Cette méthode présente trois inconvénients :

- la note n'est pas perdue, mais elle sera placée dans la `minipage`, donc juste au-dessous du tableau, et non pas en bas de la page ;
- la composition de la note, devenue une note de bas de table, n'a pas la présentation attendue ;
- `minipage` oblige à passer une largeur explicite qui peut ne pas être adaptée au tableau dont on ne connaît pas toujours la largeur finale. Un recours possible pourra être trouvé en l'environnement `varwidth` de l'extension {ctanpkg}`varwidth`, dont voici un exemple.

```latex
\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}

\usepackage{varwidth}
\setlength{\tabcolsep}{15pt}      % Tableaux aérés

\begin{document}

\begin{table}
  \centering                      % Centrer le tableau
  \renewcommand*{\footnoterule}{} % Pas de trait
  \begin{varwidth}{\linewidth}    % minipage de largeur
    \begin{tabular}{ll}           %  maximale \linewidth
      \hline
      Cépage     & Nouaison (\%) \\
      \hline
      Chasselat  & 29\footnote{En 2001} \\
      Pinot gris & 28\footnote{En 2005} \\
      Riesling   & 32\footnote{En 1997} \\
      Sylvaner   & 53\footnote{En 1999} \\
      \hline
    \end{tabular}
  \end{varwidth}
  \caption{Valeurs de nouaison pour quelques cépages}
\end{table}

\end{document}
```

### En scindant la commande « \\footnote »

La commande `\footnote` peut être décomposée en deux commandes :

- la commande `\footnotemark`, qui place le numéro de la note en exposant ;
- et `\footnotetext`, qui place le texte de la note en bas de la page. Cette dernière commande devra être exécutée en dehors du tableau.

Cette méthode simple, décrite dans le livre de Leslie Lamport, doit faire l'objet de beaucoup d'attention lorsque les notes de bas de page se multiplient et deviennent sources de confusion. Voici un exemple de cette méthode.

```latex
% !TEX noedit
\begin{tabular}{ll}
  Pierre & 01.34.65.23.12 \\
  Jean   & 06.12.43.23.21 \\
  Michel\footnotemark &   \\
  Jean-Claude & 03.23.76.45.01
\end{tabular}%
\footnotetext{Je n'ai pas son numéro.}
```

:::{note}
Notez en particulier le caractère `%` suivant le `\end{tabular}`. Si vous ne le mettez pas, la note risque d'être insérée sur la mauvaise page.
:::

Cette méthode rencontre un problème si un tableau contient plusieurs notes. Les valeurs des compteurs ne seront pas les bonnes, puisque la commande `\footnotemark` va incrémenter plusieurs fois le compteur avant que ce même compteur soit utilisé par `\footnotetext`. Une solution, pour contourner ce problème, est de mémoriser la valeur du compteur avant le tableau, et de remettre le compteur de notes à cette valeur avant d'exécuter les `\footnotetext` (qui, elles, n'incrémentent pas le compteur de notes). En voici un exemple.

```latex
% !TEX noedit
\newcounter{footnoteaux}
\setcounter{footnoteaux}{\value{footnote}}
\begin{tabular}{ll}
  Pierre  & 01.34.65.23.12\footnotemark \\
  Jean    & 06.12.43.23.21 \\
  Michel\footnotemark &    \\
  Jean-Claude & 03.23.76.45.01 \\
\end{tabular}%
\setcounter{footnote}{\value{footnoteaux}}%
\stepcounter{footnote}%
\footnotetext{Il a un répondeur.}%
\stepcounter{footnote}%
\footnotetext{Je n'ai pas son numéro.}
```

### En redéfinissant les commandes \\footmark et \\foottext

Une autre solution apportée sur [fctt](https://groups.google.com/forum/#!forum/fr.comp.text.tex) consiste à redéfinir les commandes `\footmark` et `\foottext`.

```latex
% !TEX noedit
\usepackage{ifthen}        % Utilisation des booléens
\newboolean{footmark}      % Un booléen
\newcounter{savedfootnote} % Compteur utilisé pour sauvegarder
                           %   la valeur du compteur footnote
\newcommand*{\footmark}{%
  \unless \iffootmark
    \global \footmarktrue
    \setcounter{savedfootnote}{\value{footnote}}%
  \fi
  \footnotemark
}
\newcommand*{\foottext}{%
  \iffootmark
    \global \footmarkfalse
    \setcounter{footnote}{\value{savedfootnote}}%
  \fi
  \refstepcounter{footnote}%
  \footnotetext
}
```

Les commandes s'utilisent ensuite ainsi (attention aux `%`) :

```latex
% !TEX noedit
\begin{tabular}{ll}
  Essai\footmark & Essai\footmark \\
  Essai\footmark & Essai\footmark \\
\end{tabular}%
\foottext{Une footnote}%
\foottext{Une deuxième}%
\foottext{Une troisième}%
\foottext{Une quatrième}
```

Cette méthode présente quelques restrictions :

- la méthode est inapplicable dans un flottant `table` ;
- chaque commande `footmark` doit être suivie d'une commande `\foottext` ;
- l'utilisation des arguments optionnels est déconseillée.

### Les extensions « longtable », « tabularx » et « supertabular »

Les notes au sein des tableaux produits à l'aide des extensions {ctanpkg}`longtable`, {ctanpkg}`tabularx` et {ctanpkg}`supertabular` fonctionnent mais ces extensions peuvent être moins flexibles que l'environnement de base `tabular` pour d'autres fonctionnalités.

### L'extension « mdwtab »

L'extension {ctanpkg}`mdwtab` implémente les mêmes fonctionnalités que l'extension {ctanpkg}`array` et, en prime, fait fonctionner les notes de bas de page et propose d'autres fonctionnalités pour améliorer les tableaux. Toutefois, n'envisagez cette solution que si votre document n'est pas trop complexe. En effet, {ctanpkg}`mdwtab` est incompatible avec bon nombre d'extensions.

### L'extension « tablefootnote »

L'extension {ctanpkg}`tablefootnote` fournit une commande `\tablefootnote` qui traite le sujet avec une belle sobriété.

### L'extension « footnotehyper »

L'extension {ctanpkg}`footnotehyper` fournit un environnement `savenotes` qui collecte toutes les notes de bas de page qu'il contient et les émet lorsqu'il prend fin. Ainsi, si vous placez votre environnement `tabular` dans un environnement `savenotes`, les notes de bas de page apparaîtront comme souhaité. Vous pouvez également utiliser la commande `\makesavenoteenv{tabular}` dans le préambule de votre document et les tableaux se comporteront tous comme s'ils se trouvaient dans un environnement `savenotes`.

______________________________________________________________________

*Source :* {faquk}`Footnotes in tables <FAQ-footintab>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,note de bas de page,tableau
```

