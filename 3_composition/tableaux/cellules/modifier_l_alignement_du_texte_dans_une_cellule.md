# Comment modifier la justification du texte dans une cellule ?

Il est parfois nécessaire de modifier la justification d'une cellule d'un tableau de type « `p` » ( pour « paragraphe ») cela est source de problème à la fin d'une ligne de tableau. La solution intuive ressemble souvent à :

```latex
% !TEX noedit
... & \centering blabla... \\
```

Il y a de grandes chances que se manifestent des erreurs décrites aux questions :

- « [Que signifie l'erreur : «  Misplaced \\noalign » ?](/2_programmation/erreurs/m/misplaced_noalign) » ;
- « [Que signifie l'erreur : « Extra alignment tab has been changed to \\cr » ?](/2_programmation/erreurs/e/extra_alignment_tab_has_been_changed_to_cr) ».

Ces problèmes ont pour origine la variété des sens de la commande `\\`. Dans l'environnement `tabular`, cette commande est modifiée et n'est plus compatible avec les commandes `\centering`, `\raggedright` et `\raggedleft`. Il faut ici noter que le problème ne se pose que dans la dernière cellule d'une ligne.

## Avec des commandes de base

Anciennement, la valeur réelle de la commande `\\` utilisée par l'environnement `tabular` n'était disponible que sous forme de commande interne, `\tabularnewline`. De nos jours, cette commande est publique et vous pouvez en principe l'utiliser explicitement (avec le défaut d'avoir un code un peu plus long) :

```latex
% !TEX noedit
... & \centering blabla... \tabularnewline
```

Il est également possible de modifier cette justification avec la commande `\multicolumn`. Voici un exemple :

```latex
\documentclass{article}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\begin{tabular}{||p{2cm}|}
Tous les \\
mots sont \\
alignés \\
à gauche \\
\multicolumn{1}{||r|}{sauf} \\
\og sauf \fg{}.
\end{tabular}
\end{document}
```

Notez que dans l'exemple précédent, nous avons placé les mêmes filets dans `\multicolumn` et `tabular` pour qu'il n'y ait pas de problème au niveau des raccords des cellules.

## Avec l'extension « array »

L'extension {ctanpkg}`array` fournit une commande `\arraybackslash` qui restitue à la commande `\\` sa signification correcte (dans la table). Cette commande peut être utilisée dans les spécifications du préambule du tableau :

```latex
% !TEX noedit
\begin{tabular}{... >{\centering\arraybackslash}p{50mm}}
...
```

## Avec une solution plus ancienne

Si les commandes `\tabularnewline` et `\arraybackslash` s'avéraient indisponibles, vous pouvez essayer une solution plus ancienne qui préserve le sens de la commande `\\` :

```latex
% !TEX noedit
\newcommand\sautligne[1]{\let\temp=\\%
  #1%
  \let\\=\temp
}
```

Elle s'utilise comme suit :

```latex
% !TEX noedit
... & \sautligne\centering blabla... \\
```

Elle peut être également être placée dans le préambule du tableau :

```latex
% !TEX noedit
\begin{tabular}{...>{\sautligne\centering}p{5cm}}
```

______________________________________________________________________

*Source :* {faquk}`How to alter the alignment of tabular cells <FAQ-tabcellalign>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,flottants,cellule,justification
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableau,alignement,mise en forme
```

