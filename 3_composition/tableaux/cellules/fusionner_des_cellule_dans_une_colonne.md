# Comment fusionner des cellules d'une même colonne dans un tableau ?

Certains tableaux nécessitent une cellule qui s'étendent sur plusieurs lignes. Le cas classique est celui où la colonne la plus à gauche étiquette le reste de la table. Si cette présentation peut être éventuellement traitée en utilisant une séparation diagonale comme vu à la question « {doc}`Comment diviser une cellule par une diagonale ? </3_composition/tableaux/cellules/diviser_une_cellule_en_diagonale>` », cette technique reste très limitée. Voici des solutions plus générales.

## Avec l'extension « multirow »

L'extension {ctanpkg}`multirow` vous permet de construire de telles cellules s'étendant sur plusieurs lignes. Par exemple :

```latex
\documentclass{article}
\usepackage{multirow}
\pagestyle{empty}
\begin{document}
\begin{tabular}{|c|c|}
\hline
\multirow{4}{*}{Texte multiligne}
      & Ligne 1 \\
      & Ligne 2 \\
      & Ligne 3 \\
      & Ligne 4 \\
\hline
\end{tabular}
\end{document}
```

L'extension centrera verticalement « Texte multiligne » dans la cellule étendue. Notez que les lignes qui ne contiennent pas la spécification multiligne doivent avoir des cellules vides pour laisser la place à la cellule multiligne.

Le symbole « `*` » peut être remplacé par une largeur de colonne. Dans ce cas, le contenu de la cellule multiligne peut contenir des sauts de ligne imposés :

```latex
\documentclass{article}
\usepackage{multirow}
\pagestyle{empty}
\begin{document}
\begin{tabular}{|c|c|}
\hline
\multirow{4}{25mm}{Texte%
   \\multiligne}
      & Ligne 1 \\
      & Ligne 2 \\
      & Ligne 3 \\
      & Ligne 4 \\
\hline
\end{tabular}
\end{document}
```

Un effet similaire (permettant un peu plus de sophistication) peut être obtenu en mettant un tableau plus petit qui aligne le texte dans une commande `\multirow` utilisant « `*` ».

La commande `\multirow` peut également être utilisée pour placer des textes écrits verticalement d'un côté ou de l'autre d'un tableau (à l'aide de l'extension {ctanpkg}`graphicx` qui fournit la commande `\rotatebox`) :

```latex
\documentclass{article}
\usepackage{multirow}
\usepackage{graphicx}
\pagestyle{empty}
\begin{document}
\begin{tabular}{|l|l|}
\hline
\multirow{4}{*}{%
    \rotatebox{90}{Texte}}
      & Ligne 1 \\
      & Ligne 2 \\
      & Ligne 3 \\
      & Ligne 4 \\
\hline
\end{tabular}
\end{document}
```

Cet exemple donne un texte qui va vers le haut. Pour un texte allant vers le bas, il faudrait prendre un angle de `-90`.

Pour créer une cellule s'étendant sur plusieurs lignes et plusieurs colonnes, il faut inclure la commande `\multirow` à l'intérieur de la commande `\multicolumn`, l'inverse ne fonctionnant pas :

```latex
\documentclass{article}
\usepackage{multirow}
\pagestyle{empty}
\begin{document}
\begin{tabular}{|c|c|c|}\hline
\multicolumn{2}{|c|}{
   \multirow{2}{*}{Cellules
   combinées}}
     &Haut droit\\ \cline{3-3}
\multicolumn{2}{|c|}{}
     &Centre droit\\ \hline
Bas gauche
     &Bas centre
     &Bas droit\\ \hline
\end{tabular}
\end{document}
```

L'extension {ctanpkg}`multirow` est configurée pour interagir avec l'extension {ctanpkg}`bigstrut` (abordée dans la question « {doc}`Comment gérer l'espacement interligne dans un tableau ? </3_composition/tableaux/lignes/augmenter_la_largeur_des_lignes_d_un_tableau>` »). Vous pouvez utiliser un argument facultatif de la commande `\multirow` pour dire combien de lignes de votre cellule multiligne sont ouvertes avec `\bigstrut`.

La documentation de {ctanpkg}`multirow` et de {ctanpkg}`bigstrut` se trouve, sous forme de commentaires, dans les fichiers des extensions elles-mêmes.

______________________________________________________________________

*Source :* {faquk}`Merging cells in a column of a table <FAQ-multirow>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tables,fusionner des cellules,fusionner des cases,multiligne
```

