# Comment diviser une cellule par une diagonale ?

## Avec l'extension « diagbox »

L'extension {ctanpkg}`diagbox` fournit la commande `\diagbox` qui prend deux arguments. Cette commande sépare la cellule courante en deux, par un trait diagonal du coin supérieur gauche vers le coin inférieur droit, place son premier argument *sous* ce trait et son deuxième argument *au-dessus*. En voici un exemple :

```latex
\documentclass{article}
  \usepackage{diagbox}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{|l|c|c|c|c|c|c|}
\hline
\diagbox{qui}{quoi} &
 voiture & tente & corde \\
\hline
Nico   & 1 & 1 & 1 \\ \hline
Hélène &   &   & 1 \\ \hline
Tof    &   & 1 & 1 \\ \hline
Xav    & 1 &   &   \\ \hline
\end{tabular}
\end{document}
```

Si la première ligne est trop haute, {ctanpkg}`diagbox` n'arrivera pas à s'aligner sur le coin supérieur gauche, et s'alignera uniquement sur le coin inférieur droit. Le résultat reste généralement acceptable :

```latex
\documentclass{article}
  \usepackage{diagbox}
  \usepackage{rotating}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{|l|c|c|c|}
\hline
\diagbox{qui}{quoi} &
 \begin{turn}{90}voiture\end{turn} &
 \begin{turn}{90}tente\end{turn} &
 \begin{turn}{90}corde\end{turn}
\\ \hline
Nico   & 1 & 1 & 1 \\ \hline
Hélène &   &   & 1 \\ \hline
Tof    &   & 1 & 1 \\ \hline
Xav    & 1 &   &   \\ \hline
\end{tabular}
\end{document}
```

Il est également possible de passer trois argument à `\diagbox`, pour dessiner *deux* lignes obliques :

```latex
\documentclass{article}
  \usepackage{diagbox}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{|l|c|c|c|}
\hline
\diagbox{heure}{Bureau 1}{jour} &
 Lundi & Mardi & Mercredi \\ \hline
 9h -- 12h  & occupé & occupé &  \\ \hline
 14h -- 17h &  & occupé & occupé \\ \hline
\end{tabular}
\end{document}
```

## Extension « slashbox »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`slashbox` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`slashbox` fournit la commande `\backslashbox`, qui a la même syntaxe que `\diagbox` ci-dessus, avec deux arguments. Elle est toujours disponible dans CTAN mais n'est plus distribuée dans TeX live.

L'extension {ctanpkg}`diagbox` fournit d'ailleurs les commandes compatibles `\backslashbox` et `'\slashbox` pour permettre une transition facile entre les deux extensions.

______________________________________________________________________

*Source :* {faquk}`Diagonal separation in corner cells of tables <FAQ-slashbox>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,cellule d'angle,trait en diagonale,deux traits obliques,cellule barrée
```

