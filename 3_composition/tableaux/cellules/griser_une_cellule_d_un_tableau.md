# Comment griser des cellules d'un tableau ?

## Avec l'extension « colortbl »

L'extension {ctanpkg}`colortbl` fournit la commande `\columncolor` qui définit la couleur d'une colonne. En le combinant avec la commande `\multicolumn`, il est possible de ne colorer qu'une cellule. Voici un exemple de cette méthode :

```latex
% !TEX noedit
\renewcommand\arraystretch{1.2}
\begin{center}
\begin{tabular}{|>{\columncolor[gray]{.9}}l|
    *{3}{>{\columncolor[gray]{.9}}c|}}
\hline
\multicolumn{1}{|>{\columncolor[gray]{1}}l|}{Tarifs} &
0 -  8h &  8 - 19h & 19 - 24h \\
\hline
lundi-vendredi &
\multicolumn{1}{>{\color{white}\columncolor{blue}}c|}{$0,018$~\EUR} &
\multicolumn{1}{>{\color{white}\columncolor{red}}c|}{$0,033$~\EUR} &
\multicolumn{1}{>{\color{white}\columncolor{blue}}c|}{$0,018$~\EUR}  \\
\hline
week-end \& fêtes &
\multicolumn{3}{>{\color{white}\columncolor{blue}}c|}{$0,018$~\EUR} \\
\hline
\end{tabular}
\end{center}
```

## Avec l'extension « colortab »

L'extension {ctanpkg}`colortab` permet de mettre de la couleur dans certaines parties d'un tableau. Elle n'est cependant plus maintenue et n'est pas compatible avec l'extension {ctanpkg}`longtable` par exemple.

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableau,cellule,couleur
```

