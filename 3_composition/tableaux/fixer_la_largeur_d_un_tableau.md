# Comment fixer la largeur d'un tableau ?

Fondamentalement, deux techniques sont à notre disposition pour jouer sur la largeur d'un tableau pendant sa mise en forme, pour obtenir à la fin la largeur totale désirée. On peut :

- étirer les espaces entre les colonnes ;
- étirer certaines des colonnes du tableau.

Voici comment appliquer ces deux possibilités.

## En jouant sur les espaces intercolonnes

### Avec les fonctions de base de LaTeX

Les fonctionnalités de base de LaTeX permettent uniquement d'étirer les espaces : l'environnement `tabular*` prend un argument supplémentaire qui indique la largeur totale désirée pour le tableau, sous la forme d'une longueur comme `15cm` ou `\columnwidth`. Cet argument s'écrit avant celui qui décrit la mise en forme des colonnes du tableau (avec `clpr`).

Pour que cela fonctionne, il faut également utiliser la commande `\extracolsep` dans l'argument suivant (celui qui décrit la mise en forme des colonnes), à l'intérieur d'une directive@{}''. Voici comment ça se présente :

```latex
% !TEX noedit
\begin{tabular*}{\columnwidth}{@{\extracolsep{\fill}}lllr}
```

La commande `\extracolsep` s'applique également à tous les espaces intercolonnes situés à sa droite ; si vous ne souhaitez pas que tous les espaces soient étirés, ajoutez `\extracolsep{0pt}` pour annuler l'effet de la commande initiale.

On peut également utiliser `\extracolsep{\stretch{1}}`.

:::{important}
`\extracolsep` ne doit pas être confondu avec `\tabcolsep`, qui est l'espacement de part et d'autre d'une colonne et non celui qui sépare celles-ci.
:::

```latex
\begin{tabular*}{0.5\linewidth}{
    @{\extracolsep{\stretch{1}}}
    l
    p{8cm}
    @{}
  }
  \hline
  Fonction  & Rôle \\
  \hline
  atoi()    & transforme une chaîne de caractères
              en entier \\
  fscanf()  & remplit les variables dont on donne
              l'adresse à partir d'un fichier
              (attention, d'utilisation délicate) \\
  printf()  & impression formatée d'une chaîne de
              caractères \\
  sqrt()    & racine carrée (penser à lier à la
              bibliothèque mathématique) \\
  \hline
\end{tabular*}
```

### Avec l'extension « array »

Une autre possibilité, avec l'extension {ctanpkg}`array` :

```latex
\documentclass{article}
  \usepackage[french]{babel}
  \usepackage{lmodern}
  \usepackage{array}
  \pagestyle{empty}

\begin{document}
  \begin{tabular*}{0.5\linewidth}{
      !{\extracolsep{\stretch{1}}}
      l
      p{8cm}
    }
    \hline
    Fonction  & Rôle \\
    \hline
    atoi()    & transforme une chaîne de caractères
                en entier \\
    fscanf()  & remplit les variables dont on donne
                l'adresse à partir d'un fichier
                (attention, d'utilisation délicate) \\
    printf()  & impression formatée d'une chaîne de
                caractères \\
    sqrt()    & racine carrée (penser à lier à la
                bibliothèque mathématique) \\
    \hline
  \end{tabular*}
\end{document}
```

`!{code}` est équivalent à@\{code}'' à la différence près que la séparation `\tabcolsep` n'est pas supprimée. Donc soit on supprime cette séparation des deux côtés du tableau, soit on utilise cette fonction `!{code}`.

## En jouant sur la largeur des colonnes

Dans les cas simples, il est bien sûr possible de le faire « à la main », en fixant la largeur des colonnes :

```latex
% !TEX noedit
\begin{tabular}{p{2cm}p{3cm}}
  ...
\end{tabular}
```

ou même en faisant dépendre la largeur des colonnes de la largeur de la page :

```latex
% !TEX noedit
\begin{tabular}{p{.25\textwidth}p{.33\textwidth}}
  ...
\end{tabular}
```

Mais diverses extensions ont été développées pour s'en occuper pour vous.

### Avec l'extension « tabularx »

L'extension {ctanpkg}`tabularx` définit un nouveau type de colonne : `X`. Une colonne de format `X` se comporte comme une colonne `p{⟨largeur⟩}`, mais elle est capable de s'étendre pour remplir l'espace disponible. Il n'est pas nécessaire de passer un argument `⟨largeur⟩`

S'il y a plus d'une colonne `X` dans un tableau, l'espace libre est réparti entre elles.

Voici un exemple qui donne un tableau s'étalant sur toute la largeur d'une ligne, et divisé en deux colonnes de largeur égale :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage[width=8cm]{geometry}
  \usepackage{tabularx}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{tabularx}{\linewidth}{|X|X|}
   \hline
    salut   & coucou \\
    bonjour & hello  \\
   \hline
\end{tabularx}
\end{document}
```

### Avec l'extension « tabulary »

L'extension {ctanpkg}`tabulary` (du même auteur que {ctanpkg}`tabularx`) fournit un moyen d'« équilibrer » l'espace occupé par les colonnes d'un tableau. Elle définit des spécifications de colonne `C`, `L`, `R` et `J`, qui donnent, respectivement, des versions centrée, gauche, droite et justifiée des colonnes qui se partagent l'espace en trop. L'extension examine la largeur « naturelle » de chaque colonne (c'est-à-dire la largeur qu'elle aurait sur une feuille de papier de largeur infinie) et alloue de l'espace à chaque colonne en conséquence. Elle fait quelques vérifications pour que les entrées vraiment grandes n'écrasent pas tout le reste (il y a une notion de « largeur maximale »), et pour que les entrées minuscules ne deviennent pas plus petites qu'un minimum spécifié.

Bien sûr, tout ce travail signifie que l'extension doit compiler chaque ligne plusieurs fois, donc tout ce qui produit des effets de bord (par exemple, un compteur qui serait utilisé pour compter les lignes) devient inévitablement peu fiable, et ne devrait même pas être essayé.

### Avec d'autres extensions

L'extension {ctanpkg}`ltxtable` combine les fonctionnalités des extensions {ctanpkg}`longtable` et {ctanpkg}`tabularx`. Lisez bien {texdoc}`sa documentation <ltxtable>`, car son utilisation est peu intuitive.

L'extension {ctanpkg}`easytable` fonctionne un peu comme {ctanpkg}`tabularx`.

______________________________________________________________________

*Sources :*

- {faquk}`Fixed-width tables <FAQ-fixwidtab>`,
- [Control the width of table columns (tabular) in LaTeX](https://texblog.org/2019/06/03/control-the-width-of-table-columns-tabular-in-latex/),
- [Tabular fixed width](https://tex.stackexchange.com/questions/223445/tabular-fixed-width).

```{eval-rst}
.. meta::
   :keywords: LaTeX,tables,mise en forme des tableaux,largeur fixe,tableau de la largeur de la page,flottants
```

