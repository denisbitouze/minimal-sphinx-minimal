# Comment modifier l'orientation d'un tableau ?

## Avec l'extension « graphicx »

La commande `\rotatebox` de l'extension {ctanpkg}`graphicx` permet de faire tourner une boîte et donc, en particulier, un tableau :

```latex
\documentclass{article}
\usepackage{graphicx}
\pagestyle{empty}
\begin{document}
\rotatebox{90}{%
\begin{tabular}{|c|c|}
   \hline
    salut & coucou \\
    bonjour & hello\\
   \hline
\end{tabular}}
\end{document}
```

## Avec l'extension « lscape »

L'extension {ctanpkg}`lscape` permet de passer certaines pages en mode « paysage ». Elle utilise la commande `\rotatebox` et est compatible avec l'extension {ctanpkg}`longtable` (voir la question [Comment composer un tableau s'étendant sur plus d'une page ?](/3_composition/tableaux/tableau_sur_plusieurs_pages)).

## Avec l'extension « rotating »

L'extension {ctanpkg}`rotating` permet, grâce à l'environnement `sideways`, de changer l'orientation d'un tableau :

```latex
% !TEX noedit
\begin{sideways}
\begin{tabular}{|c|c|}
\multicolumn{1}{c}{%
    \begin{turn}{60}Note\end{turn}} &
\multicolumn{1}{c}{%
    \begin{turn}{60}Quantité\end{turn}} \\
\hline
0 & 3 \\
1 & 4 \\
2 & 11 \\
3 & 15 \\
4 & 23 \\
5 & 36 \\
\hline
\end{tabular}
\end{sideways}
```

```latex
\documentclass{article}
\usepackage{rotating}
\pagestyle{empty}
\begin{document}
\begin{sideways}
\begin{tabular}{|c|c|}
\multicolumn{1}{c}{%
    \begin{turn}{60}Note\end{turn}} &
\multicolumn{1}{c}{%
    \begin{turn}{60}Quantité\end{turn}} \\
\hline
0 & 3 \\
1 & 4 \\
2 & 11 \\
3 & 15 \\
4 & 23 \\
5 & 36 \\
\hline
\end{tabular}
\end{sideways}
\end{document}
```

## Avec l'extension « rotfloat »

Si le tableau est un [flottant](/3_composition/flottants/start), l'extension {ctanpkg}`rotfloat` permet de faire tourner les flottants (légende comprise).

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants, tableaux,tourner,pivoter,rotation,paysage
```

