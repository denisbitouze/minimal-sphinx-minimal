# Comment composer un tableau de plus d'une page ?

Par défaut, un tableau est entièrement placé dans une boîte rien que pour lui : il forme donc un bloc qui ne peut être réparti sur plusieurs pages. Malheureusement, la vraie vie nous confronte souvent à des tableaux bien plus grands que ce qu'une page peut contenir...

Pour les tableaux simples (dont le format est très régulier), la solution la plus simple peut être d'utiliser l'environnement `tabbing`, qui est un peu fastidieux à mettre en place, mais qui ne force pas son conteu à apparaître sur une seule page.

L'extension {ctanpkg}`longtable` construit la table entière (par morceaux) dans un premier temps, puis utilise les informations qu'elle a écrites dans le fichier `.aux` pour obtenir le bon réglage lors des compilations ultérieures (elle parvient généralement à mettre les tableaux en forme en seulement deux passages). Comme l'extension a une vue d'ensemble du tableau au moment où elle effectue le « réglage final », la table est formatée uniformément du début à la fin, avec des largeurs de colonnes qui se correspondent bien sur les pages consécutives. {ctanpkg}`longtable` a la réputation de ne pas fonctionner avec d'autres extensions, mais il fonctionne avec {ctanpkg}`colortbl`, et son auteur fournit le paquet {ctanpkg}`ltxtable` pour remplacer (la plupart des) fonctionnalités de {ctanpkg}`tabularx` [^footnote-1] pour les tableaux longs. Attention cependant à ses contraintes d'utilisation inhabituelles : chaque tableau long doit être dans un fichier à part, et inclus par `\LTXtable{⟨largeur⟩}{⟨fichier⟩}`. Puisque les tableaux à plusieurs pages de {ctanpkg}`longtable` ne peuvent pas être mis dans des flottants, l'extension s'occupe elle-même des légendes, dans l'environnement `longtable`.

Une alternative à {ctanpkg}`ltxtable` pourrait être l'extension {ctanpkg}`ltablex`; mais elle est obsolète et n'est pas entièrement fonctionnelle. Son plus gros problème est sa capacité mémoire très limitée ({ctanpkg}`longtable` n'est pas vraiment limité, au prix d'une grande complexité de son code); {ctanpkg}`ltablex` ne peut traiter que des tableaux relativement petits, il ne semble plus maintenu; mais il est vrai que son interface utilisateur est beaucoup plus simple que {ctanpkg}`ltxtable`, donc si ses restrictions ne sont pas un problème pour vous, ça peut valoir la peine de l'essayer.

L'extension {ctanpkg}`supertabular` commence et termine un environnement `tabular` pour chaque page du tableau. Par conséquent, chaque « hauteur de page » du tableau est compilée indépendamment, et la largeur d'une même colonne peut varier sur des pages successives. Cependant, si l'homogénéité n'a pas d'importance, ou si vos colonnes sont de largeur fixe, {ctanpkg}`supertabular` a le grand avantage de faire son travail en une seule compilation.

> {ctanpkg}`longtable` et {ctanpkg}`supertabular` permettent tous deux de définir des lignes d'en-tête et de pied de tableau; {ctanpkg}`longtable` permet également de distinguer la première et la dernière ligne d'en-tête ou de pied.

Le paquetage {ctanpkg}`xtab` corrige certaines défauts de {ctanpkg}`supertabular`, et fournit également une fonction « dernier en-tête » (bien que cela détruise l'avantage de {ctanpkg}`supertabular` de fonctionner en une seule fois).

Le paquetage {ctanpkg}`stabular` fournit une « extension de `tabular` » simple à utiliser, qui permet de composer des tableaux qui s'étendent au-delà de la fin d'une page; il a aussi des fonctionnalités pratiques, mais n'a pas les capacités des paquets principaux pour régler finement les en-têtes et de pieds de pages.

______________________________________________________________________

*Source :* {faquk}`Tables longer than a single page <FAQ-longtab>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,tableaux,tableau long,tableau sur plusieurs pages
```

[^footnote-1]: voir la question « {doc}`Comment fixer la largeur d'un tableau? </3_composition/tableaux/fixer_la_largeur_d_un_tableau>` ».

