# Comment obtenir des filets partiels dans un tableau ?

La commande `\cline{⟨nb1⟩-⟨nb2⟩}` trace un filet depuis la colonne `⟨nb1⟩` (incluse) jusqu'à la colonne `⟨nb2⟩` (incluse également).

Voici un exemple, qui appelle l'extension {ctanpkg}`multirow` pour {doc}`écrire du texte sur plusieurs lignes </3_composition/tableaux/lignes/ecrire_un_texte_sur_plusieurs_lignes_dans_un_tableau>` :

```latex
\documentclass[11pt,french]{article}
\usepackage{multirow}
\usepackage{mathtools}
\usepackage{babel}
\pagestyle{empty}
\begin{document}

\[
\renewcommand\arraystretch{1.4}
%
\begin{array}{|c|c||c|c|}
 \hline
 \multirow{2}{0.5cm}{$k$} &
 \multirow{2}{0.5cm}{$p_G$} &
 \multicolumn{2}{c|}{test} \\
 \cline{3-4}
 & & \text{DADWRD} & \text{RARWRD} \\
 \hline
 2 & 1     & 90n   & 228 n \\
 3 & p_d   & 202 n & 449 n \\
 4 & p_d^2 & 424 n & 891 n \\
 5 & p_d^3 & 866 n & 1774 n \\
 \hline
\end{array}
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,filets
```

