# Comment modifier l'épaisseur des traits dans les tableaux ?

Les filets d'un tableau LaTeX ont par défaut une épaisseur de `0.4pt`; il s'agit d'une valeur prédéfinie au niveau le plus bas, qui s'applique à tous les traits (y compris ceux qui séparent les blocs de texte, notamment).

Parfois, cependant, en regardant un tableau, on voudrait faire ressortir certains filets, par exemple pour mieux séparer le contenu du tableau du corps du texte, ou pour délimiter les différentes parties du tableau. Cependant, un examen rapide de n'importe quel livre sur LaTeX ne révèle aucune technique permettant d'épaissir un filet en particulier, et une petite expérimentation montre qu'il est en effet assez difficile d'éviter qu'une modification ne « déborde » et affecte tous les filets du tableau.

Si vous lisez nos critiques sur la {doc}`mise en forme des tableaux en LaTeX </3_composition/tableaux/mes_tableaux_sont_moches>`, ailleurs dans cette FAQ, vous avez dû sentir que lors de sa conception, LaTeX a tout simplement ignoré la plupart des questions esthétiques liées aux tableaux : c'est probablement la raison pour laquelle il ne fournit rien pour modifier ponctuellement l'épaisseur des filets.

Plus précisément, si la longueur `\arrayrulewidth` affecte l'épaisseur des traits (à la fois horizontaux et verticaux) dans les environnements `tabular` et `array`, elle la modifie de façon *globale*. Par exemple :

**Épaisseur par défaut**

```latex
% !TEX noedit
%

\begin{tabular}{|lc|}
\hline
  Peintre & Années \\
\hline
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\hline
\end{tabular}
```

```latex
\begin{tabular}{|lc|}
\hline
  Peintre & Années \\
\hline
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\hline
\end{tabular}
```

**Épaisseur : `1pt`**

```latex
% !TEX noedit
\setlength{\arrayrulewidth}{1pt}

\begin{tabular}{|lc|}
\hline
  Peintre & Années \\
\hline
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\hline
\end{tabular}
```

```latex
\setlength{\arrayrulewidth}{1pt}

\begin{tabular}{|lc|}
\hline
  Peintre & Années \\
\hline
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\hline
\end{tabular}
```

Le changement est bien visible, mais remarquez que la jonction entre les trais verticaux et horizontaux se fait mal quand leur épaisseur est modifiée, tellement l'épaisseur par défaut est ancrée dans le code. La programmation au niveau utilisateur, même pointue, s'avère incapable de modifier un seul trait : il est nécessaire de se plonger dans le code (plutôt délicat) de `\hline` et `\cline` pour y arriver.

Heureusement, ce travail a déjà été fait pour la communauté : le paquet {ctanpkg}`booktabs` définit trois catégories de filets distinctes : `\toprule`, `\midrule` et `\bottomrule`, et la {texdoc}`documentation du paquet (en français) <booktabs-fr>` offre des conseils sur la façon de les utiliser. Nous vous conseillons vivement de lire attentivement cette documentation.

```latex
\documentclass{article}
  \usepackage{booktabs}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{lc}
\toprule
  Peintre & Années \\
\midrule
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\bottomrule
\end{tabular}
\end{document}
```

:::{tip}
Pour un résultat encore plus soigné, vous pouvez utiliser la commande `\cmidrule`, qui raccourcit légèrement les filets internes par rapport aux autres :

```latex
\documentclass{article}
  \usepackage{booktabs}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{lc}
\toprule
  Peintre & Années \\
\cmidrule(lr){1-2}
  Manet  & 1832--1883 \\
  Ingres & 1780--1867 \\
  Goya   & 1746--1828 \\
\bottomrule
\end{tabular}
\end{document}
```
:::

La classe {ctanpkg}`memoir` inclut le paquet {ctanpkg}`booktabs` et reprend la teneur de sa documentation dans son {texdoc}`manuel très détaillé <memoir>` (en anglais [^footnote-1]).

Notez qu'on a retiré les traits verticaux dans les derniers exemples de tableaux. Pour en connaître les raisons, consultez la {texdoc}`documentation du paquetage « booktabs » <booktabs-fr>` (encore une fois); en résumé, les traits verticaux sont à éviter, pour des raisons à la fois historiques et esthétiques. On peut toujours s'en passer, en jouant sur l'alignement du contenu des colonnes.

:::{note}
Les traits verticaux des tableaux sont de toute façon codés dans LaTeX de façon encore plus déroutante que les traits horizontaux, et leur manque de configurabilité les rend encore moins attrayants... C'est, somme toute, tant mieux pour la mise en forme de votre document.
:::

## Pour les bricoleurs

### Épaissir des traits horizontaux à la demande

On a vu que la longueur `\arrayrulewidth` définit l'épaisseur des filets de séparation horizontale *et* verticale. Cela ne permet donc de changer que *globalement* l'épaisseur des filets.

Maintenant, si, dans un tableau, on met deux filets de séparation horizontaux successifs, ceux-ci sont séparées par la longueur `\doublerulesep`. Il suffit donc de donner la bonne valeur à cette valeur pour que les deux filets soient collés, et semblent former un unique filet, plus épais. On écrira donc :

```latex
% !TEX noedit
\setlength{\doublerulesep}{\arrayrulewidth}
```

Ensuite, il suffit d'exécuter plusieurs fois consécutivement la commande `\hline` pour épaissir le filet autant que désiré.

On peut aussi, en se fondant sur la définition de `\hline`, créer une commande `\hlinewd` dont l'épaisseur sera passée en argument. Voici un exemple :

```latex
% !TEX noedit
\makeatletter
\newcommand\hlinewd[1]{%
    \noalign{\ifnum0=`}\fi\hrule \@height #1%
    \futurelet\reserved@a\@xhline}
\makeatother

\begin{tabular}{|l|r|}
\hline
lundi    & 8  \\ \hlinewd{.5\arrayrulewidth}
mardi    & 4  \\ \hlinewd{.5\arrayrulewidth}
mercredi & 9  \\ \hlinewd{.5\arrayrulewidth}
jeudi    & 13 \\ \hlinewd{.5\arrayrulewidth}
vendredi & 5  \\ \hlinewd{2\arrayrulewidth}
total    & 39 \\ \hline
\end{tabular}
```

### Épaissir des traits verticaux à la demande

Pour augmenter l'épaisseur d'un filet de séparation vertical, nous allons supprimer le filet vertical par défaut, et en insérer un « à la main », avec la commande `\vrule`. Voici comment faire :

```latex
% !TEX noedit
\begin{tabular}{|
  c<{\global\addtolength{\arrayrulewidth}{1pt}}|
  >{\global\addtolength{\arrayrulewidth}{-1pt}}c|
  c|c@{\hskip\arraycolsep\vrule width 1.4pt}}
\hline
a & b & c & d \\
\hline
a & b & c & d \\
\hline
\end{tabular}
```

:::{note}
Les packages {ctanpkg}`array` et {ctanpkg}`mdwtab` définissent un nouveau type de séparateur de colonnes, semblable à@'', mais qui ne supprime pas l'espace inter-colonnes. Le package {ctanpkg}`mdwtab` fournit en outre une commande, `\vline`, qui prend en argument optionnel l'épaisseur du filet.
:::

______________________________________________________________________

*Source :* {faquk}`The thickness of rules in LaTeX tables <FAQ-rulethk>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,flottants,modifier l'apparence des tableaux,mise en forme des tableaux,épaisseur des lignes
```

[^footnote-1]: Un [projet de traduction existe](https://github.com/jejust/memoir-fr), vous pouvez y contribuer

