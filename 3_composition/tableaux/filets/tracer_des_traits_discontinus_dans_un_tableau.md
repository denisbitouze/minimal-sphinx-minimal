# Comment tracer des filets discontinus ?

## Avec l'extension « arydshln »

L'extension {ctanpkg}`arydshln` (qui doit impérativement être chargée après l'extension {ctanpkg}`array`) fournit des commandes permettant de tracer des filets discontinus. En combinant ses commandes avec les séparateurs de colonnes fournis par {ctanpkg}`array`, il est possible d'obtenir à peu près tous les résultats souhaités.

Cette extension définit également un nouveau type de séparateur, « `:` », qui trace une ligne verticale pointillées. Par exemple :

```latex
\documentclass{report}
  \usepackage[width=8cm]{geometry}
  \usepackage{arydshln}
  \pagestyle{empty}

\begin{document}
  \setlength{\hdashlinewidth}{.5pt}
  \setlength{\hdashlinegap}{2pt}

On peut alors écrire le tableau suivant :
\[
\left[
       \begin{array}{ccc:c}
         a_1 & b_1 & c_1 & d_1 \\
         a_2 & b_2 & c_2 & d_2 \\
         a_3 & b_3 & c_3 & d_3 \\
       \hdashline
         a_4 & b_4 & c_4 & d_4 \\
       \end{array}
\right]
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme des tableaux,ligne pointillée,traits pointillés
```

