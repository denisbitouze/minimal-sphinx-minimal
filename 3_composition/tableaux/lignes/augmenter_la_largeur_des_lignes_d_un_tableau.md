# Comment améliorer l'espacement entre les lignes d'un tableau ?

Les mécanismes de TeX et LaTeX pour maintenir l'espace entre les lignes reposent sur le générateur de paragraphe de TeX, qui compare la forme des lignes consécutives et ajuste l'espace entre elles.

Ces mécanismes ne peuvent pas fonctionner exactement de la même manière lors de la construction d'une table car le constructeur de paragraphes ne voit pas les lignes elles-mêmes. En conséquence, les tableaux sont parfois composés avec des lignes trop rapprochées ou trop éloignées.

## Avec des commandes de base

Historiquement, les typographes travaillant avec des caractères en plomb ajustaient l'espacement entre les lignes d'une table à l'aide de cales (*strut* en anglais). Un utilisateur de TeX ou LaTeX peut faire exactement la même chose : la plupart des extensions proposent une commande `\strut` qui définit un espacement approprié à la taille actuelle du texte. Placer une commande `\strut` à la fin d'une ligne problématique est la solution la plus simple au problème... si elle fonctionne.

Les solutions ci-après sont spécifiques à LaTeX mais certaines peuvent être simplement traduites en commandes Plain TeX.

## Avec l'extension « array »

Si chaque ligne de votre tableau demande à être corrigée d'une même longueur, l'extension {ctanpkg}`array` fournit la commande `\extrarowheight` :

```latex
% !TEX noedit
\usepackage{array}% dans le préambule
...
\setlength{\extrarowheight}{longueur} % longueur étant à préciser : par exemple 1cm, 2.2ex...
\begin{tabular}{....}
```

## Avec l'extension « bigstrut »

Pour corriger une seule ligne qui n'est redressée par une commande `\strut`, vous pouvez utiliser une commande proche de la commande qui va à l'intérieur d'un `\strut` :

```latex
% !TEX noedit
\rule{0pt}{length}  % longueur étant à préciser : par exemple 1cm, 2.2ex...
```

L'extension {ctanpkg}`bigstrut` définit une commande que vous pouvez utiliser à cette fin :

- `\bigstrut` positionne une cale au-dessus et au-dessous de la ligne courante ;
- `\bigstrut[t]` positionne une cale au-dessus de la ligne ;
- `\bigstrut[b]` positionne une cale au-dessous de la ligne.

## Avec l'extension « tabls »

De façon un peu plus générale, l'extension {ctanpkg}`tabls` génère automatiquement une cale de taille appropriée à la fin de chaque ligne. Elle a toutefois deux inconvénients : elle ralentit la composition des tables (car elle interagit avec tout ce qui se trouve dans les tableaux) et elle peut être incompatible avec d'autres extensions.

## Avec l'extension « makecell »

L'extension {ctanpkg}`makecell` fournit une commande `\gape` qui peut être utilisée pour appliquer une cale à une seule cellule d'un tableau :

```latex
% !TEX noedit
\begin{tabular}{lll}
  ... & \gape{cell contents} & ... \\
  ...
\end{tabular}
```

La commande `\Gape` fournit la même fonctionnalité avec des arguments facultatifs : ils permettent d'affiner le réglage du haut et du bas de la ligne.

Pour ajuster chaque cellule de tableaux entiers, la commande `\setcellgapes{⟨longueur⟩}` permet ici définir une valeur d'ajustement. Un argument facultatif « `t` » ou « `b` » restreint l'ajustement respectivement au haut ou au bas de chaque cellule. Après avoir placé cette commande `\setcellgapes`, la commande `\makegapedcells` active cette correction des cellules tandis que la commande `\nomakegapedcells` la désactive.

## Avec l'extension « cellspace »

L'extension {ctanpkg}`cellspace` accomplit une tâche assez similaire en définissant un nouveau type de colonne de tableau nommée « `S` », que vous appliquez à chaque spécification de colonne. Supposons que votre table commence ainsi :

```latex
% !TEX noedit
\begin{tabular}{l l l p{3cm}}
```

Vous pourriez alors la modifier comme suit :

```latex
% !TEX noedit
\begin{tabular}{Sl Sl Sl Sp{3cm}}
```

Cette technique semble ne pas trop interférer avec les autres extensions (mais cela reste à prouver).

## Avec l'extension « booktabs »

L'extension {ctanpkg}`booktabs` est livrée avec un texte traitant de la manière dont les tableaux devraient être conçus (disponible {texdoc}`en français <booktabs-fr>`). L'auteur note en particulier que les utilisateurs de LaTeX ont trop souvent tendance à placer des filets dans leurs tableaux. Dans la mesure où la plupart des problèmes d'espacement des lignes des tableaux apparaissent lors de collisions avec des filets, la démarche de cet auteur vaut le détour.

L'extension fournit des commandes de filets pour appliquer les idées de l'auteur mais traite également la question de l'espacement entre les lignes. La version la plus récente de {ctanpkg}`booktabs` est compatible avec des extensions telles que {ctanpkg}`longtable`. Un exemple de l'utilisation de cette extension est présenté à la question « [Comment présenter un tableau comme dans les livres ?](/3_composition/tableaux/presentation_professionnelle_d_un_tableau) ».

______________________________________________________________________

*Source :* {faquk}`Spacing lines in tables <FAQ-struttab>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,floats,tables
```

