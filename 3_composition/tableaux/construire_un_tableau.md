# Comment faire un tableau ?

- L'environnement permettant de faire un tableau en mode texte s'appelle `tabular`. Il prend un argument optionnel et un argument obligatoire :

```latex
% !TEX noedit
\begin{tabular}[⟨position⟩]{⟨format⟩}
  contenu du tableau
\end{tabular}
```

L'argument obligatoire ⟨*format*⟩ décrit le format des colonnes du tableau. Cela suppose que vous connaissiez à l'avance le nombre de colonnes que vous aller produire, sinon TeX affichera un message d'erreur. Une colonne est représentée par une lettre parmi :

- `l` (pour que le texte soit aligné à gauche dans la colonne),
- `c` (pour que le texte soit centré dans la colonne) et
- `r` (pour que le texte soit aligné à droite).

Les trois types de colonne (`l`, `c` et `r`) ne passent jamais à la ligne et sont, par conséquent, adaptées aux cellules dont le contenu est court. Lorsque les cellules sont bien remplies, il faut opter pour `p{larg}` (`p` comme **p***aragraphe*) qui compose le contenu de la cellule dans un paragraphe justifié de largeur `larg`.

Une répétition `n` fois d'un motif élémentaire est facilitée par `*{n}{motif}`. `*{20}{lcc}` est équivalent à `lcclcclcc...lcc` (en tout 60 colonnes), plus concrètement l'exemple suivant montre un tableau utilisant ce genre de descriptions.

Exemple de tableau (remarquez que le second `|` fait partie de l'argument de `*`).

```latex
\begin{tabular}{|*{5}{p{1cm}|}}
\hline
0 & 1 & 2 & 3 & 4 \\
\hline
1 & 2 & 3 & 4 & 5 \\
\hline
2 & 3 & 4 & 5 & 6 \\
\hline
\end{tabular}
```

L'argument optionnel ⟨*position*⟩ définit la position verticale du tableau par rapport à la ligne courante : `t` pour **t***op* (le haut du tableau sera aligné avec la ligne courante), `c` pour **c***enter* et `b` pour **b***ottom* (le bas du tableau sera aligné avec la ligne courante). L'effet de ces options est illustré ici :

```latex
% !TEX noedit
b \begin{tabular}[b]{l} A \\ B \end{tabular}\quad
c \begin{tabular}[c]{l} A \\ B \end{tabular}\quad
t \begin{tabular}[t]{l} A \\ B \end{tabular}
```

```latex
b \begin{tabular}[b]{l} A \\ B \end{tabular}\quad
c \begin{tabular}[c]{l} A \\ B \end{tabular}\quad
t \begin{tabular}[t]{l} A \\ B \end{tabular}
```

Il existe d'autres possibilités, que nous verrons plus bas. Chaque colonne peut être séparée de sa voisine par un ou plusieurs filets verticaux, que l'on indique à l'aide du caractère `|` (la barre verticale).

À l'intérieur de l'environnement `tabular`, on remplit ligne par ligne, chaque ligne se terminant par `\\` (ou `\tabularnewline`) :

```latex
\begin{tabular}{c}
  ligne $1$ \\
  ligne $2$ \\
  $\vdots$  \\
  ligne $n$ \\
\end{tabular}
```

et à l'intérieur de chaque ligne, on remplit cellule par cellule de gauche à droite, chaque cellule étant séparée de la suivante par `&` :

```latex
% !TEX noedit
\begin{tabular}{cccc}
  cellule $1$  &  cellule $2$  &  \dots  &  cellule $m$  \\
\end{tabular}
```

```latex
\begin{tabular}{cccc}
  cellule $1$  &  cellule $2$  &  \dots  &  cellule $m$  \\
\end{tabular}
```

L'exemple suivant présente un tableau simple :

```latex
\documentclass{article}
  \usepackage{eurosym}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{|l|r|c||r|}
\hline
Article  & Prix unitaire & Quantité & Prix total \\
\hline
Arrosoir &  $19,90$~\EUR & 2 &  $39,80$~\EUR \\
Brouette & $129,00$~\EUR & 1 & $129,00$~\EUR \\
Râteau   &   $9,90$~\EUR & 1 &   $9,90$~\EUR \\
\hline
Total & & & $178,70$~\EUR \\
\hline
\end{tabular}
\end{document}
```

Pour placer des filets verticaux (traits délimitant les colonnes), il faudra insérer `|` dans l'argument `format` de l'environnement `tabular`. Placer des filet horizontaux se fait en plaçant la commande `\hline` (*horizontal line*) après le `\\`.

Par défaut, toutes les colonnes, quel que soit leur type, sont entourées par un blanc contrôlé par le paramètre `\tabcolsep`. Deux colonnes successives sont donc séparées par un espace de largeur `2 × \tabcolsep`. Dans un tableau à trois colonnes, on aurait :

```latex
\begin{center}
  \setlength{\tabcolsep}{12pt}
  \def \tempa #1{%
    \makebox[0pt][#1]{%
      \setlength{\unitlength}{\tabcolsep}%
      \raisebox{.4ex}{%
        \begin{picture}(1,0)
          \put(0.5,0){\vector(-1,0){0.5}}
          \put(0.5,0){\vector( 1,0){0.5}}
          \put(0.5,.3){\makebox[0pt]%
            {\fboxsep=1pt \fcolorbox{white}{white}%
              {$\scriptstyle \vphantom{p} x$}}}
        \end{picture}%
      }%
    }%
  }
  \def \tempb {%
    \makebox[0pt][r]{%
      \setlength{\unitlength}{2\tabcolsep}%
      \raisebox{.4ex}{%
        \begin{picture}(1,0)
          \put(0.5,0){\vector(-1,0){0.5}}
          \put(0.5,0){\vector(1,0){0.5}}
          \put(0.5,0.15){\makebox[0pt]%
            {\fboxsep=1pt \fcolorbox{white}{white}%
              {$\scriptstyle \vphantom{p} 2x$}}}
        \end{picture}%
      }%
    }%
  }
  \begin{tabular}{|c|c|c|}
    \hline
    \tempa{r}Cellule 1          &
    \tempb   Cellule 2          &
    \tempb   Cellule 3\tempa{l} \\
    \hline
  \end{tabular}
\end{center}
```

où \$x = \\verb+tabcolsep+\$.

Enfin, il est possible de séparer deux colonnes par autre chose qu'un filet vertical, en remplaçant le `|` par@{*code*}''. Dans ce cas, l'espacement entre les colonnes est supprimé, et l'argument `code` de@'' remplace la barre verticale « classique ».

- Pour faire un tableau en mode mathématique, on utilisera l'environnement `array`, dont l'utilisation est similaire. Voir la question « {doc}`Comment composer un tableau en mode mathématique? </4_domaines_specialises/mathematiques/composer_un_tableau_en_mode_mathematique>` » pour un exemple.
- L'environnement `tabbing` permet également de créer des tableaux, avec une optique assez différente. Voir la section « {doc}`tabulations </3_composition/tableaux/tabulations/composer_un_tableau_avec_des_tabulations>` » pour des informations complémentaires.

## Où placer son tableau ?

- Placé tel quel dans un paragraphe, votre tableau va s'intégrer au texte avec les règles d'alignement évoquées ci-dessus.
- Le flottant `table` défini par la plupart des classes classiques (les classes standard, et leurs clones {ctanpkg}`memoir` et les classes {ctanpkg}`KOMA-script`). Le tableau peut alors flotter (c'est-à-dire trouver la place la plus confortable) et être affublé d'une légende (commande `\caption`) accompagnée d'un numéro que l'on peut référencer par le mécanisme habituel (`\label`, `\ref`).

```latex
% !TEX noedit
\dots{}voir le tableau~\ref{tab=Un_tableau_simple}.
%
\begin{table}
  \begin{tabular}{ll}
    A & B \\
    C & D \\
  \end{tabular}
  \caption{Un tableau simple}
  \label{tab=Un_tableau_simple}
\end{table}
```

- Si l'on désire que le tableau ne flotte pas, on pourra utiliser les environnements `center`, `flushleft`, qui placeront le tableau dans un paragraphe à part avec un peu d'espace pour le séparer du texte.

```latex
% !TEX noedit
\dots{}le tableau suivant :

\begin{center}
  \begin{tabular}{ll}
    A & B \\
    C & D \\
  \end{tabular}
\end{center}
```

On perd cependant toute possibilité d'attacher une légende et de référencer le tableau. Lisez les réponses à la question « {doc}`Comment est géré le positionnement des flottants? </3_composition/flottants/positionnement>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,initiation aux tableaux,composer un tableau,environnement « tabular »,faire un tableau,construire un tableau
```

