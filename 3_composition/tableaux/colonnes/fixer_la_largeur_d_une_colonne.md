# Comment fixer la largeur d'une colonne ?

- Il suffit d'utiliser le « descripteur de colonne » `p{largeur}`. Dans ce cas, si le texte doit s'étaler sur plusieurs lignes, l'alignement vertical se fait sur la première ligne. Le package {ctanpkg}`array` définit les descripteurs `m{largeur}`, pour que le texte soit centré verticalement, et ''b\{largeur}' pour que le texte soit aligné en bas.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

