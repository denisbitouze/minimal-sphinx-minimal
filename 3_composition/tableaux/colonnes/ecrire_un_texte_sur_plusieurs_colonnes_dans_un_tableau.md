# Comment écrire un texte sur plusieurs colonnes d'un tableau ?

- La commande `\multicolumn{nb}{col}{contenu}` permet d'obtenir une cellule s'étalant sur plusieurs colonnes. Le premier argument indique le nombre de colonnes à utiliser, le deuxième argument décrit la colonne ainsi que ses séparateurs, le troisième argument contient le texte de la cellule. Voir l'exemple~\\vref{ex=multicolumn} pour la pratique.

```latex
% !TEX noedit
\renewcommand\arraystretch{1.2}
\begin{center}
\begin{tabular}{|l|*{3}{c|}}
\hline
Tarifs & 0 -  8h &  8 - 19h & 19 - 24h \\
\hline
lundi-vendredi & $0,018$~\EUR &
                 $0,033$~\EUR &
                 $0,018$~\EUR  \\
\hline
week-end \& f\^etes &
\multicolumn{3}{c|}{$0,018$~\EUR} \\
\hline
\end{tabular}
\end{center}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

