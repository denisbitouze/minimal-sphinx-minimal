# Comment changer la fonte d'une colonne ?

- Le package {ctanpkg}`array` permet d'insérer systématiquement des commandes entre le contenu d'une colonne et son séparateur. Cela se fait au moyen des « descripteurs » de colonnes `>` et `<`.

Voici comment tout cela fonctionne :

```latex
\documentclass{article}
\usepackage{array}
\begin{document}
\begin{tabular}{|>{\scshape}l|>{\(}c<{\)}|>{\itshape}r|}
\hline
Hugo       & 1831 & Les feuilles d'automne \\
Balzac     & 1832 & Le colonel Chabert     \\
Maupassant & 1887 & Le Horla               \\
\hline
\end{tabular}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme des tableaux,mise en forme d'une colonne,changer le style d'une colonne,fonte dans un tableau
```

