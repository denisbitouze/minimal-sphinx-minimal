# Comment mettre deux tableaux côte à côte ?

Sur ce sujet, la question « {doc}`Comment placer des figures côte à côte ? </3_composition/flottants/positionnement/placer_des_figures_cote_a_cote>` » donne une réponse générale.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,deux tableaux l'un à côté de l'autre,mise en page des tableaux
```

