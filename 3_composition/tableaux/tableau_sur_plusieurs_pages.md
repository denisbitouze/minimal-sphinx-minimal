# Comment composer un tableau s'étendant sur plus d'une page ?

Par défaut, un tableau est entièrement placé dans une boîte rien que pour lui : il forme donc un bloc qui ne peut être réparti sur plusieurs pages. Malheureusement, la vraie vie nous confronte souvent à des tableaux bien plus grands que ce qu'une page peut contenir...

Pour les tableaux simples (dont le format est très régulier), la solution la plus simple peut être d'utiliser l'environnement `tabbing`, qui est un peu fastidieux à mettre en place, mais qui ne force pas son contenu à apparaître sur une seule page.

## Avec l'extension « longtable »

L'extension {ctanpkg}`longtable` construit la table entière (par morceaux) dans un premier temps, puis utilise les informations qu'elle a écrites dans le fichier « `.aux` » pour obtenir le bon réglage lors des compilations ultérieures (elle parvient généralement à mettre les tableaux en forme en seulement deux passages). Comme l'extension a une vue d'ensemble du tableau au moment où elle effectue le « réglage final », la table est formatée uniformément du début à la fin, avec des largeurs de colonnes qui se correspondent bien sur les pages consécutives.

Voici un exemple :

```latex
\documentclass[10pt,french]{article}
 \usepackage[utf8]{inputenc}
 \usepackage{longtable}
 \pagestyle{empty}

\begin{document}
\begin{longtable}{|p{0.2\linewidth}|p{0.2\linewidth}|p{0.2\linewidth}|}
   \hline
   Première colonne & Deuxième & Troisième
   \endfirsthead
   \hline
   Première & Deuxième & Troisième \\
   \multicolumn{3}{|p{0.6666\linewidth}|}{Suite...}\\
   \endhead
   \hline
   \multicolumn{3}{|p{0.6666\linewidth}|}{Suite page suivante}
   \\ \hline
   \endfoot
   \hline
   \multicolumn{3}{|p{0.6666\linewidth}|}{Fin}\\
   \hline
   \endlastfoot
   \hline
   1   &     1  &        1  \\
   2   &     4  &       16  \\
   3   &     9  &       81  \\
   1   &     1  &        1  \\
   2   &     4  &       16  \\
   3   &     9  &       81  \\
   1   &     1  &        1  \\
   2   &     4  &       16  \\
   3   &     9  &       81  \\
   1   &     1  &        1  \\
   2   &     4  &       16  \\
   3   &     9  &       81  \\
   ... &    ... &      ...  \\
\end{longtable}
\end{document}
```

De façon appréciable, {ctanpkg}`longtable` permet également de distinguer la première et la dernière ligne d'en-tête ou de pied de page.

Cette extension a la réputation de ne pas fonctionner avec d'autres extensions, exception faite de {ctanpkg}`colortbl`, et son auteur fournit l'extension {ctanpkg}`ltxtable` pour remplacer (la plupart des) fonctionnalités de {ctanpkg}`tabularx` pour les tableaux longs (voir la question « {doc}`Comment fixer la largeur d'un tableau ? </3_composition/tableaux/fixer_la_largeur_d_un_tableau>` »). Cette dernière a cependant des contraintes d'utilisation inhabituelles : chaque tableau long doit être dans un fichier à part, et inclus par `\LTXtable{⟨largeur⟩}{⟨fichier⟩}`. Puisque les tableaux à plusieurs pages de {ctanpkg}`longtable` ne peuvent pas être mis dans des flottants, l'extension s'occupe elle-même des légendes, dans l'environnement `longtable`.

Une alternative à {ctanpkg}`ltxtable` pourrait être l'extension {ctanpkg}`ltablex` mais elle est obsolète et n'est pas entièrement fonctionnelle. Son plus gros problème est sa capacité mémoire très limitée ({ctanpkg}`longtable` n'est pas vraiment limité, au prix d'une grande complexité de son code) ; {ctanpkg}`ltablex` ne peut traiter que des tableaux relativement petits, il ne semble plus maintenu. Si vous ne craignez pas les expérimentations et que vous recherchez une interface utilisateur est beaucoup plus simple que celle de {ctanpkg}`ltxtable`, il peut être intéressant de l'essayer.

## Avec l'extension « supertabular »

L'extension {ctanpkg}`supertabular` commence et termine un environnement `tabular` pour chaque page du tableau que LaTeX découpe automatiquement. Par conséquent, chaque « hauteur de page » du tableau est compilée indépendamment, et la largeur d'une même colonne peut varier sur des pages successives. Cependant, si l'homogénéité n'a pas d'importance, ou si vos colonnes sont de largeur fixe, {ctanpkg}`supertabular` a le grand avantage de faire son travail en une seule compilation.

L'environnement associée à cette extension s'appelle `supertabular`. À l'intérieur de cet environnement, le contenu du tableau est géré de la même façon que dans l'environnement `tabular`. S'ajoute à cela les commandes suivantes :

- `\tablefirsthead{...}` définit le contenu de la première ligne du tableau ;
- `\tablehead{...}` définit le contenu de la première ligne qui sera insérée en cas de changement de page au milieu du tableau ;
- `\tabletail{...}` définit le contenu de la dernière ligne d'une page, en cas de changement de page au milieu du tableau ;
- `\tablelasttail{...}` définit le contenu de la dernière ligne du tableau ;
- `\topcaption{...}` et `\bottomcaption{...}` permettent de mettre une légende sur ce tableau, soit au début, soit à la fin (ces deux commandes sont incompatibles, l'utilisation des deux conduisant à ne produire que la seule légende de fin). `\tablecaption{}` place la légende à sa position « habituelle », qui est par défaut le haut du tableau.

Voici un exemple :

```latex
\documentclass[10pt,french]{article}
 \usepackage[utf8]{inputenc}
 \usepackage{supertabular}
 \pagestyle{empty}

\begin{document}
  \tablefirsthead{\hline
    \multicolumn{1}{|c}{Nombre} & \multicolumn{1}{c}{Nombre$^2$} & Nombre$^4$ & \multicolumn{1}{c|}{!Nombre} \\
    \hline%
  }
  \tablehead{\hline
    \multicolumn{4}{|l|}{\small\textsl{suite du tableau}}\\
    \hline
    \multicolumn{1}{|c}{ Nombre} & \multicolumn{1}{c}{Nombre$^2$} & Nombre$^4$ & \multicolumn{1}{c|}{Nombre!} \\
    \hline%
  }
  \tabletail{\hline
    \multicolumn{4}{|r|}{\small\textsl{Suite à la page suivante...}}\\
    \hline%
  }
  \tablelasttail{\hline}
  \topcaption{Exemple}
  \bottomcaption{Exemple de grand tableau}
%
  \begin{supertabular}{| r@{\hspace{6.5mm}}|
      r@{\hspace{5.5mm}}| r | r|}
    1   &     1  &        1  &           1    \\
    2   &     4  &       16  &           2    \\
    3   &     9  &       81  &           6    \\
    4   &    16  &      256  &          24    \\
    5   &    25  &      625  &         120    \\
    6   &    36  &     1296  &         720    \\
    7   &    49  &     2401  &        5040    \\
    8   &    64  &     4096  &       40320    \\
    9   &    81  &     6561  &      362880    \\
    ... &    ... &      ...  &         ...    \\
  \end{supertabular}
\end{document}
```

## Avec l'extension « xtab »

L'extension {ctanpkg}`xtab` corrige certaines défauts de {ctanpkg}`supertabular` et fournit une fonction « dernier en-tête » (bien que cela détruise l'avantage de {ctanpkg}`supertabular` de fonctionner en une seule fois).

## Avec l'extension « stabular »

L'extension {ctanpkg}`stabular` fournit une « extension de `tabular` » simple à utiliser, qui permet de composer des tableaux qui s'étendent au-delà de la fin d'une page. Il dispose également de fonctionnalités pratiques mais n'a pas les capacités des extensions vues précédemment pour régler finement les en-têtes et de pieds de pages.

______________________________________________________________________

*Source :* {faquk}`Tables longer than a single page <FAQ-longtab>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,tableaux,tableau long,grand tableau,tableu multi-page,tableau sur plusieurs pages
```

