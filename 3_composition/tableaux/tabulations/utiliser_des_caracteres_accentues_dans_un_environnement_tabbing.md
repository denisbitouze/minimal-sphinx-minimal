# Comment utiliser des accents dans un environnement « tabbing » ?

Afin d'obtenir des {doc}`tabulations </3_composition/tableaux/tabulations/composer_un_tableau_avec_des_tabulations>`, vous construisez un environnement `tabbing`. Mais voici que vous avez besoin d'un signe [diacritique](https://fr.wikipedia.org/wiki/Diacritique) (accent ou cédille), avec une commande comme `\'{e}`. Là, l'accent disparaît parce qu'il a été interprété comme une commande de l'environnement `tabbing`. En effet, les commandes `\'` et ```` \` ```` sont redéfinies par l'environnement `tabbing`.

En fait, pour taper des caractères accentués dans cet environnement, vous devez utiliser la commande `\a`, autrement dit `\a'{e}` au lieu de `\'{e}`. De même, il faudra utiliser `\a` pour `\` et `\a=` pour `\=`. Toute cette procédure s'avère lourde et sujette aux erreurs. Voici donc des alternatives.

## Par saisie directe

L'alternative la plus simple est d'employer un encodage contenant les caractères diacritiqués et d'utiliser un fichier de définition d'encodage approprié dans l'extension {ctanpkg}`inputenc`, ce qui évite les saisies d'accent comme `\'{e}`. Par exemple, saisissez :

```latex
% !TEX noedit
  \usepackage[latin1]{inputenc}
  ...
  \begin{tabbing}
  ...
  ... \> voilà  \> ...
```

## Avec l'extension « Tabbing »

L'extension {ctanpkg}`Tabbing` (avec un `T` majuscule) fournit un environnement `Tabbing` qui duplique `tabbing`, à ceci près que toutes les commandes à caractère unique sont remplacées par des commandes avec des noms plus longs. Ainsi `\>` devient `\TAB>`, `\=` devient `\TAB=` et ainsi de suite. L'exemple trivial ci-dessus deviendrait donc :

```latex
% !TEX noedit
\usepackage{Tabbing}
...
\begin{Tabbing}
  ...
  ... \TAB> voil\`a \TAB> ...
```

______________________________________________________________________

*Source :* {faquk}`Accents misbehave in tabbing <FAQ-tabacc>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreurs,accents,tabbing,composition des tableaux,tabulations
```

