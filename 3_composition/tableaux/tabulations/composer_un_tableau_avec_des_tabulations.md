# Comment créer un tableau avec des tabulations ?

- L'environnement `tabbing` permet de composer des tableaux de façon simple, en utilisant des tabulations pour l'alignement en colonnes. Cet environnement crée un tableau en utilisant des taquets de tabulation, que l'on place avec la commande `\=`, et sur lesquels on s'aligne avec la commande `\>`. L'exemple suivant montre un exemple simple utilisant l'environnement `tabbing` :

```latex
\documentclass{article}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
\begin{tabbing}
Article\qquad \= Prix unitaire\qquad \=
  Quantité\qquad \=  Prix total \\[3mm]
Arrosoir\> $19,90$~€  \> 2 \> $39,80$~€  \\
Brouette\> $129,00$~€ \> 1 \> $129,00$~€ \\
Rateau  \> $9,90$~€   \> 1 \> $9,90$~€   \\[3mm]
Total   \>            \>   \> $178,70$~€ \\
\end{tabbing}
\end{document}
```

Il est possible de placer des taquets avec une ligne « bidon », que l'on supprime par la suite avec la commande `\kill`.

Les taquets de tabulations du début de la ligne peuvent être « oubliés », avec la commande `\+`, puis repris en compte avec la commande `\-`. La commande `\+` agit sur toutes les lignes qui le suivent, la commande `\-` aussi. La commande `\<` permet de revenir en arrière d'une tabulation.

Il est aussi possible de « mémoriser » une ligne de tabulations, afin de la remplacer provisoirement, puis de la remettre en place. Cela se fait grâce aux commandes `\poptabs` et `\pushtabs`. Voir l'exemple avancé en fin de page.

Enfin, on peut forcer l'alignement à droite en ajoutant la commande `\`' à la
fin de la « cellule » : cette commande place le texte qui la précède dans la
colonne qui la précède (il faut donc d'abord aller à la tabulation suivante),
justifié à droite. Afin de forcer la dernière colonne à être alignée à droite,
on la fera précéder de ```` \` ````.

Exemple d'utilisation de l'environnement `tabbing` :

```latex
% !TEX noedit
\begin{tabbing}
Article \qquad \= Prix unitaire
  \=\qquad Quantit\a'e\= \qquad Prix total \kill\\
Article \>\> Prix unitaire\'\>
  Quantit\a'e\'\`Prix total \\[3mm]
Arrosoir\>\>$19,90$~\EUR \'\>2\'\`$39,80$~\EUR \\
Brouette\>\>$129,00$~\EUR\'\>1\'\`$129,00$~\EUR\\
Rateau  \>\>$9,90$~\EUR  \'\>1\'\`$9,90$~\EUR
                                          \\[3mm]
Total   \>                 \>   \`$178,70$~\EUR\\
\end{tabbing}
```

:::{important}
À l'intérieur de l'environnement `tabbing`, les commandes `\`' et ```` \` ```` n'ont donc pas leur fonction habituelle, qui est de {doc}`mettre des accents sur des caractères </3_composition/texte/symboles/caracteres/accents_divers>` (`\'e` donne « é » et ```` \`a ```` donne « à », par exemple).

Pour utiliser des caractères accentués dans un environnement `tabbing`, le plus simple est bien sûr de les saisir directement : `é`, `à`... Si ce n'est pas possible, vous pouvez utiliser `\a'` et ```` \a` ```` à la place de `\'` et ```` \` ```` : par exemple le mot « Quantité » peut être saisi comme `Quantit\a'e` dans l'exemple ci-dessus.

Plus de détails sont donnés en réponse à la question {doc}`Comment utiliser des accents dans un environnement « tabbing »? </3_composition/tableaux/tabulations/utiliser_des_caracteres_accentues_dans_un_environnement_tabbing>`
:::

Utilisation avancée de `tabbing` :

```latex
% !TEX noedit
Voici un exemple évolué avec l'environnement \texttt{tabbing} :

\begin{tabbing}
\hspace*{1.5cm}\=\hspace{1.5cm}\=\kill
\>Renault\' Clio \> 5 portes \+\+\\
                    6 CV \\
                    consommation :
\-\-\pushtabs \\
\hspace*{4cm}\=\kill
                 \>5 l/100 km sur autoroute \\
                 \>7 l/100 km en ville \\
\poptabs\>\+\>\+
                    verte \\
                         \` 10\,500 \EUR \\
     \<M\a'egane \> 5 portes \\
                    7 CV \\
                    consommation :
\-\-\pushtabs \\
\hspace*{4cm}\=\kill
                \>5 l/100 km sur autoroute \\
                \>8 l/100 km en ville \\
\poptabs\>\+\>\+
                    grise \\
                        \` 14\,000 \EUR \\
 \<Peugeot\' 406 \> 5 portes \\
                    7 CV \\
                    consommation :
\-\-\pushtabs \\
\hspace*{4cm}\=\kill
                \>6 l/100 km sur autoroute \\
                \>9 l/100 km en ville \\
\poptabs\>\+\>\+
                bleue \\
                    \` 17\,500 \EUR \\
\end{tabbing}
```

______________________________________________________________________

*Source :* [LaTeX2e unofficial reference manual : « tabbing ».](https://latexref.xyz/tabbing.html)

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,mise en forme des tableaux,alignement en colonnes,saisie des tableaux
```

