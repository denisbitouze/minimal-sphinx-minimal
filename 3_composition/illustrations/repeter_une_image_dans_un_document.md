# Comment insérer plusieurs fois une image dans un document ?

Un logo ou un filigrane affiché sur chaque page, ou toute autre image répétée de nombreuses fois dans votre document, peut rendre le fichier final du document incroyablement volumineux. Le problème est que les mécanismes par défaut d'inclusion des graphismes ajoutent l'image à chaque emplacement où elle doit être utilisée et donc, après compilation, le fichier PostScript contient autant de copies redondantes.

Les énormes fichiers PostScript sont un problème; devoir expliquer que le fichier est énorme à cause des logos en bas de page est un problème encore plus embarrassant... 🫤

:::{note}
Si votre PostScript est destiné à être converti en PDF, par un outil appelant [ghostscript](https://www.ghostscript.com/) (comme `ps2pdf`) ou par `Acrobat Distiller`, cette question ne se pose pas, car le convertisseur s'occupe de dédoublonner les objets graphiques si besoin.

pdfTeX fait le même travail lors de la compilation, en convertissant automatiquement les utilisations multiples d'une image en des références à des objets graphiques.
:::

Le tutoriel {texdoc}`Utilisation de graphiques importés <epslatex-fr>` (traduit en français depuis {texdoc}`un document Keith Reckdahl <epslatex>`) décrit une technique permettant d'éviter ce problème : en gros, on convertit l'image qui doit être répétée en une sous-routine PostScript, et on la charge sous la forme d'un fichier prologue pour `dvips`. À la place de l'image, vous chargez un fichier (avec la même *bounding box* que l'image) qui ne contient rien d'autre qu'une invocation de la sous-routine définie dans le prologue.

Cette technique (appelons-la « technique `epslatex` ») est délicate, mais elle fait le job. Plus délicate encore est l'astuce qui consiste à convertir la figure en une police Adobe Type 3 contenant un unique caractère. Cette technique est réservée aux vrais gourous, mais elle permet de gagner autant d'espace que la technique `epslatex`, avec une plus grande souplesse d'utilisation.

Pour le commun des mortels, il y a l'extension {ctanpkg}`graphicx-psmin`, de Hendri Adriaens ; il suffit de la charger *à la place* de {ctanpkg}`graphicx`, donc au lieu de :

```latex
% !TEX noedit
\usepackage[⟨options⟩]{graphicx}
```

vous écrirez :

```latex
% !TEX noedit
\usepackage[⟨options⟩]{graphicx-psmin}
```

puis, au début de votre document, vous chargerez les images à répéter :

```latex
% !TEX noedit
\loadgraphics[⟨bb⟩]{⟨liste des images⟩}
```

Chacune des images de la liste sera convertie en un « objet » à utiliser dans la sortie PostScript (c'est, en substance, une version automatisée de la technique `epslatex` décrite ci-dessus).

Après avoir chargé l'extension comme indiqué ci-dessus, chaque fois que vous appellerez `\includegraphics`, la commande vérifiera si le fichier que vous avez demandé est l'un de ceux de la liste `\loadgraphics`. Si c'est le cas, l'insertion sera convertie en un appel à l'objet plutôt que résulter en une nouvelle copie du fichier; le PostScript final sera bien sûr beaucoup plus petit.

La documentation de {ctanpkg}`graphicx-psmin` prévient que l'extensions nécessite une version de `dvips` supérieure à 5.95b, mais ça ne devrait plus être un problème, cette version ayant été publiée en 2010.

______________________________________________________________________

*Source :* {faquk}`Repeated graphics in a document <FAQ-repeatgrf>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,images répétées,inclusions multiples,taille du document
```

