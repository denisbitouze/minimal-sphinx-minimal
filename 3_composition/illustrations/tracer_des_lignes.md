# Comment tracer un filet ?

## En LaTeX, sans extension

- La commande `\rule` permet de tracer une ligne. Elle prend comme premier argument la largeur (horizontale) de la ligne, comme deuxième argument son épaisseur; elle a aussi un argument facultatif qui permet décaler la ligne sous la ligne d'écriture (on parle de ⟨*profondeur*⟩, comme pour les caractères qui ont des traits qui descendent sous la lige de base). Sa syntaxe complète est donc : `\rule[⟨profondeur⟩]{⟨largeur⟩}{⟨hauteur⟩}`

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \pagestyle{empty}
\begin{document}
\vrule height 1cm depth 5mm width .4pt
\hrule height .2pt depth .2pt width \textwidth
\end{document}
```

## Avec TikZ

Vous pouvez aussi choisir de sortir l'artillerie lourde, et d'utiliser Ti*k*Z pour tracer un filet. Sans commande `\tikz` est spécialement adaptée pour insérer un élément graphique simple dans votre document. Elle est suivie de code Ti*k*Z, et terminée par un point-virgule :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{tikz}

\begin{document}
\tikz\draw (0,0) -- (\linewidth,0pt);
\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\tikz\draw (0,0) -- (\linewidth,0pt);
\end{document}
```

Pour un simple filet, les solutions précédentes sont sûrement plus élégantes. Mais Ti*k*Z vous ouvre de nouvelles possibilités :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{tikz}
      \usetikzlibrary{snakes}
      \usetikzlibrary{decorations.footprints}
  \pagestyle{empty}
\begin{document}
\tikz\draw[double,thick,blue] (0,0) -- (\linewidth,0pt);

\tikz\draw[decorate,decoration=snake] (0,0) -- (\linewidth,0pt);

\tikz\draw[decorate,decoration=snake,shorten >= 4.2pt] (0,0) -- (\linewidth,0pt);

\tikz\draw[decorate,decoration={footprints,foot length=3ex},red] (0,0) -- (\linewidth,0pt);
\end{document}
```

Notez que la première ligne ondulée se termine par un petit trait horizontal. C'est parce que la décoration `snake` ne fonctionne que sur un nombre entier d'ondulations. Pour avoir un dessin correct dans ce cas (seconde ligne ondulée), nous avons choisi de raccourcir légèrement la ligne avec `shorten`, et tâtonné pour trouver la longueur adéquate.

______________________________________________________________________

*Source :*

- [A line of length « \\textwidth » in TikZ](https://tex.stackexchange.com/questions/58292/a-line-of-length-textwidth-in-tikz).

```{eval-rst}
.. meta::
   :keywords: LaTeX,filets,lignes,traits,tracer une ligne horizontale,tracer une ligne verticale,dessiner en LaTeX
```

