# Comment placer du texte LaTeX dans un graphique ?

Les graphiques « techniques » (tels que les diagrammes) contiennent parfois des étiquettes de texte présentant des expressions mathématiques assez complexes : il existe peu d'outils de dessin ou de graphique (tel Metapost) qui peuvent faire de telles choses.

Cette question étudie donc les méthodes pour placer des étiquettes sur les graphiques produits par tous ces *autres* outils. Notez que le terme « étiquette » doit être interprété de manière large : de nombreuses techniques présentées ici peuvent être utilisées pour placer du texte mais aussi pour dessiner sur un graphique.

## Avec l'extension « psfrag »

L'extension {ctanpkg}`psfrag` permet de gérer le cas où votre image est incluse en tant que [fichier EPS](/5_fichiers/postscript/postscript_encapsule). Ici, après avoir placé un texte unique dans votre graphique en utilisant les fonctionnalités de texte usuelles de vos outils, vous pouvez demander à {ctanpkg}`psfrag` de remplacer ce texte par du code LaTeX avec la commande suivante :

```latex
% !TEX noedit
\psfrag{texte original}{texte de remplacement}
```

Des arguments facultatifs permettent le réglage de la position, de l'échelle et de la rotation du texte de remplacement. Tous les détails sont évoqués dans la {texdoc}`documentation <psfrag>` de cette extension.

## Avec les extensions « pst-pdf », « auto-pst-pdf » ou « PDFrack »

Pour être utilisé avec `pdfLaTeX`, l'extension {ctanpkg}`psfrag`, qui travaille normalement avec des fichiers EPS, nécessite quelques traitements supplémentaires. Ce qui suit décrit deux méthodes d'adaptation possibles :

- utiliser l'extension {ctanpkg}`pst-pdf` (présentée également à la question « [Comment intégrer des images avec pdfLaTeX ?](/3_composition/illustrations/inclure_une_image/inclure_un_fichier_pdf2) ») dans un mode dédié ;
- utiliser l'extension {ctanpkg}`PDFrack <pdfrack>`.

Si l'extension {ctanpkg}`pst-pdf` peut prendre en charge les étapes supplémentaires, elle sert surtout de base à une autre extension, {ctanpkg}`auto-pst-pdf`, qui propose un paramètre pour la combiner à {ctanpkg}`psfrag`.

Par ailleurs, sous réserve que votre système accepte le shell Unix Bourne (ou équivalent), autrement dit que vous travaillez avec un système basé sur Unix, ou un équivalent tel que `cygwin` sous `Windows`, vous pouvez essayer l'extension {ctanpkg}`PDFrack <pdfrack>`. Le script qui l'accompagne vise à découper chaque figure de votre source, la passer dans un fichier LaTeX ne contenant rien d'autre que les commandes d'insertion de figure. Chacun de ces fichiers est ensuite traité par PostScript, compilé à l'aide des commandes `\psfrag` et finalement converti en PDF. Notez que l'auteur de {ctanpkg}`PDFrack <pdfrack>` considère son extension comme une astuce de programmation que comme une réelle fonctionnalité.

## Avec l'extension « psfragx »

L'extension {ctanpkg}`psfragx` va plus loin que {ctanpkg}`psfrag` : elle fournit un moyen vous permettant de placer les commandes {ctanpkg}`psfrag` dans le préambule de votre fichier EPS lui-même. Si l'extension {ctanpkg}`psfrag` dispose d'une telle commande, elle ne la recommande pas tandis que l'extension {ctanpkg}`psfragx` a amélioré le code de cette fonctionnalité. L'extension {ctanpkg}`psfragx` fournit ainsi un script `laprint` à utiliser avec `Matlab` pour produire une sortie correctement balisée. En principe, d'autres applications graphiques pourraient fournir une fonctionnalité similaire, mais apparemment aucune ne le fait.

## Avec l'éditeur « iTe »

Les personnes travaillant avec `Emacs` peuvent trouver avec l'éditeur `iTe` un outil pratique pour placer des étiquettes : il s'agit d'un éditeur graphique orienté LaTeX écrit en `Lisp Emacs`. Vous pouvez avec celui-ci créer des environnements `iteblock` contenant des graphiques et du texte, puis utiliser `iTe` pour organiser les éléments les uns par rapport aux autres.

## Avec l'extension « overpic »

L'extension {ctanpkg}`overpic` superpose un environnement `picture` sur un graphique inclus avec `\includegraphics`, ce qui se prête bien au placement facile de textes et autres sur un graphique. L'extension peut tracer une grille pour vous aider à faire votre « retraitement ». La {texdoc}`documentation <overpic>` de l'extension est accompagnée d'exemples simples.

## Avec l'extension « lpic »

L'extension {ctanpkg}`lpic` est assez similaire à {ctanpkg}`overpic` : elle définit un environnement `lpic` (qui place votre graphique pour vous). Dans cet l'environnement, vous pouvez utiliser la commande `\lbl` pour positionner le matériel LaTeX aux endroits appropriés sur le graphique.

## Avec l'extension « pinlabel »

L'extension {ctanpkg}`pinlabel` se rapproche également d' {ctanpkg}`overpic`. Sa {texdoc}`documentation <pinlabel>` explique en détail comment organiser votre étiquetage : en chargeant votre figure dans une visionneuse et en prenant des mesures à partir de celle-ci (l'extension évoque l'utilisation directe de [ghostscript](https://www.ghostscript.com/)'' ainsi que des visionneuses personnalisées telles que ''[gsview](http://www.ghostgum.com.au/) ou `gv`).

## Avec l'extension « pstricks »

L'extension {ctanpkg}`pstricks` peut bien sûr faire tout ce que {ctanpkg}`overpic`, {ctanpkg}`lpic` ou {ctanpkg}`pinlabel` peuvent faire, avec toute la flexibilité de programmation PostScript qu'il propose. Cette capacité est illustrée par l'extension {ctanpkg}`pst-layout` qui semble être un sur-ensemble de {ctanpkg}`overpic` et {ctanpkg}`lpic`.

De même, {ctanpkg}`pgf/TikZ` a toute la puissance nécessaire, mais aucune extension dédiée n'a été publiée sur le sujet.

Le site Web de {ctanpkg}`pstricks` présente plusieurs [exemples d'étiquetage](http://pstricks.tug.org/main.cgi?file=Examples/overlay) qui vous aideront à démarrer ; si {ctanpkg}`pstricks` est [bonne option pour vous](/3_composition/illustrations/dessiner_avec_tex), cette méthode vaut la peine d'être essayée.

Bien entendu, l'utilisateur confiant peut faire tout le travail dans un environnement d'image qui inclut le graphique. Les méthodes utilisant {ctanpkg}`overpic` ou {ctanpkg}`pstricks` sont tout à fait recommandables mais ne sont manifestement guère plus que des aides par rapport à ce qui est réalisable avec l'approche bricolage.

______________________________________________________________________

*Source :* {faquk}`Labelling graphics <FAQ-labelfig>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphics
```

