# Comment centrer une figure très large ?

- La première réponse qui me vient à l'esprit est d'essayer de réduire la figure, ou de la découper. En effet, même lorsqu'on aura réussi à la centrer, le résultat ne sera pas très esthétique, cela va dépasser.
- Si vraiment la réponse précédente ne convient pas, on pourra utiliser la commande `\centerline`, en lui passant en argument la commande qui inclut la figure problématique. Cela donnera donc :

```latex
% !TEX noedit

\centerline{%
  \includegraphics{figure.eps}}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

