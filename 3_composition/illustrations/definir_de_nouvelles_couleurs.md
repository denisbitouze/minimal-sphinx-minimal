# Comment définir de nouvelles couleurs ?

## Avec l'extension « xcolor »

Les couleurs sont gérées en temps normal par l'extension {ctanpkg}`xcolor`. Sa {texdoc}`documentation <xcolor>` (en anglais) fournit une excellente introduction à la théorie des couleurs.

Il existe plusieurs modèles de description des couleurs pour définir de nouvelles couleurs :

### Pour le modèle CMYK (cyan, magenta, yellow, black)

Ce modèle nommé en français [quadrichromie](https://fr.wikipedia.org/wiki/Quadrichromie) ou CMJN, pour « cyan, magenta, jaune, noir », est associé aux commandes suivantes :

```latex
% !TEX noedit
\definecolor{nouveau_nom}{cmyk}{w,x,y,z}
```

avec w,x,y,z des valeurs numériques décimales comprises entre 0 et 1 (le séparateur décimal étant le « . »).

### Pour le modèle RGB (red, green, blue)

Ce modèle nommé en français [RVB](https://fr.wikipedia.org/wiki/Rouge_vert_bleu), pour « rouge, vert, bleu », est associé aux commandes suivantes :

```latex
% !TEX noedit
\definecolor{nouveau_nom}{rgb}{x,y,z}
```

avec x,y,z des valeurs numériques décimales comprises entre 0 et 1.

### Pour le modèle HSB (hue, saturation, brightness)

Ce modèle nommé en français [TSL](https://fr.wikipedia.org/wiki/Teinte_saturation_luminosité), pour « teinte, saturation, luminosité », est associé aux commandes suivantes :

```latex
% !TEX noedit
\definecolor{nouveau_nom}{hsb}{x,y,z}
```

avec x,y,z des valeurs numériques décimales comprises entre 0 et 1.

:::{note}
Le prédécesseur de l'extension {ctanpkg}`xcolor` était {ctanpkg}`color`. Il est toujours maintenu et n'est pas encore considéré comme obsolète, mais il n'y a pas vraiment de raisons de le préférer à {ctanpkg}`xcolor`.
:::

## Avec l'extension « pstricks »

L'extension {ctanpkg}`pstricks` qui permet d'inclure des graphiques dans un document LaTeX propose également des commandes de descriptions des couleurs, très proches de celles vues ci-dessus.

(pour-le-modele-cmyk-cyan-magenta-yellow-black-1)=

### Pour le modèle CMYK (cyan, magenta, yellow, black)

```latex
% !TEX noedit
\newcmykcolor{le_nom_de_la_couleur}{w x y z}
```

avec x,y,z des valeurs numériques décimales comprises entre 0 et 1.

(pour-le-modele-rgb-red-green-blue-1)=

### Pour le modèle RGB (red, green, blue)

```latex
% !TEX noedit
\newrgbcolor{le_nom_de_la_couleur}{x y z}
```

avec x,y,z des valeurs numériques décimales comprises entre 0 et 1.

(pour-le-modele-hsb-hue-saturation-brightness-1)=

### Pour le modèle HSB (hue, saturation, brightness)

```latex
% !TEX noedit
\newhsbcolor{nom_de_la_couleur}{x y z}
```

avec x,y,z des valeurs numériques décimales comprises entre 0 et 1.

______________________________________________________________________

*Source :* [When to use the xcolor package instead of the color package?](https://tex.stackexchange.com/questions/89763/when-to-use-the-xcolor-package-instead-of-the-color-package)

```{eval-rst}
.. meta::
   :keywords: LaTeX,définir des couleurs,espaces de couleurs,modèle de couleur
```

