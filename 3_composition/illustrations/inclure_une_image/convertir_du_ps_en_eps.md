# Comment passer de PS à EPS ?

## Quelle est la différence entre ces deux formats ?

Un fichier PS (dont le nom se termine généralement par l'extension `.ps`) est un document (texte et images) codé en PostScript. [PostScript](https://fr.wikipedia.org/wiki/PostScript) lui-même est un «langage de description de page», adapté pour décrire où sont placés des objets sur une page : éléments de dessins, caractères, images...

Un fichier EPS est au format PostScript *encapsulé*, c'est à dire qu'il a été conçu pour être inclus dans un autre document. Il y a donc deux principales différences avec un fichier PS, *i.e.* au format PS :

- un fichier EPS contient au plus une page. Afin d'être inclus dans un autre document, il est important que sa taille soit précisément et facilement accessible. Elle doit (c'est d'ailleurs la seule chose qui soit obligatoire dans un fichier EPS) donc être indiquée en début de fichier, sur une ligne commençant par `%%BoundingBox:`, suivie des deux coordonnées du coin inférieur gauche, et des dimensions de la figure.
- un fichier EPS ne contient pas la commande PostScript `\showpage`. Cette commande indique en effet à l'interpréteur PS d'afficher la page courante. Si cette commande est incluse, l'interpréteur PS va afficher la page juste après avoir lu le fichier EPS, donc probablement avant la fin de la page du document englobant.

## Comment convertir l'un en l'autre ?

Pour convertir un fichier PS en EPS, il convient donc, essentiellement :

- de calculer assez précisément les dimensions de la figure PS et d'ajouter la ligne `%%BoundingBox:`,
- de désactiver la commande `\showpage`.

De nombreux outils permettent de faire cette conversion de manière automatique. L'outil spécialisé le plus connu est [ghostscript](https://fr.wikipedia.org/wiki/Ghostscript). Par exemple, la ligne de commande suivante réalisera la conversion de `figure.ps` en `figure.eps` :

```bash
gs -sDEVICE=epswrite -sOutputFile=figure.eps -q -dNOPAUSE -dBATCH -dSAFER figure.ps
```

Il existe également des programmes `ps2epsi` et `pstoeps` (et toutes les variantes possibles sur ces noms...) qui font automatiquement ces conversions.

La conversion inverse (EPS → PS) n'a généralement pas besoin d'être faite, car un fichier EPS est un fichier PS tout à fait valide. Si l'absence de commande `\showpage` pose problème, vous pouvez utiliser LaTeX pour inclure le fichier EPS dans un document PS, grâce à ce document très simple :

```latex
% !TEX noedit
\documentclass{standalone}
  \usepackage{graphicx}

\begin{document}
  \includegraphics{mon_image.eps}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript,convertir du PostScript en PostScript encapsulé,encapsuler du PostScript,Convertir de l'EPS en PS
```

