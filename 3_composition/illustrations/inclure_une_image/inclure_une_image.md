# Comment insérer une image ?

Donald Knuth, lors de la conception de la version de TeX au début des années 1980, n'avait pu déterminer aucune syntaxe standard pour décrire les graphiques dans les documents. Il avait estimé que cet état ne pouvait pas persister indéfiniment mais qu'il serait insensé de sa part de définir des primitives TeX permettant l'importation d'images. Il a donc reporté la spécification des images aux auteurs de pilotes graphiques DVI. Les documents TeX contrôleraient donc les pilotes au moyen de commandes [\\special](/5_fichiers/postscript/commandes_special).

Si vous saviez utiliser les commandes `\special` et que vous connaissiez bien les pilotes graphiques, vous pouviez donc arriver à inclure des images. Cela restait cependant une solution accessible à peu de monde. Au fil des ans, par conséquent, des extensions « d'inclusion graphique » ont vu le jour : la plupart ont été conçues pour l'inclusion d'images [PostScript encapsulés](/5_fichiers/postscript/postscript_encapsule) qui est longtemps restée le format de prédilection des utilisateurs de TeX et LaTeX. Des solutions plus générales sont arrivées par la suite.

## Avec l'extension « graphicx »

Solution incontournable, l'extension {ctanpkg}`graphicx`, basée sur l'extension {ctanpkg}`graphics` décrite plus bas, fournit une commande `\includegraphics`, dont le nom est assez explicite. Cette commande prend en argument obligatoire le nom du fichier contenant l'image :

- au format EPS {doc}`Postscript encapsulé </3_composition/illustrations/inclure_une_image/convertir_du_ps_en_eps>`) en cas de compilation avec `tex` ou `latex` ;
- aux formats JPEG, PNG ou PDF en cas de compilation avec `pdftex` ou `pdflatex`.

Par exemple :

```latex
% !TEX noedit
\includegraphics{mafigure.eps}
```

:::{note}
Certaines distributions LaTeX modifient l'environnement à l'aide de la commande `\DeclareGraphicsRule` pour pouvoir utiliser d'autres formats d'image. En fait, LaTeX effectue une conversion à la volée lors de la compilation à l'aide de la commande en dernier argument de `\DeclareGraphicsRule`. Voir le document {texdoc}`Utilisation de graphiques importés dans LaTeX2e <fepslatex>` pour plus d'informations sur cette commande.
:::

La commande `\includegraphics` peut prendre de nombreux arguments optionnels, afin de modifier la taille ou l'orientation de l'image, par exemple. Concernant les dimensions de l'image, quelques options sont décrites dans la réponse à la question « {doc}`Comment modifier la taille d'une « bounding box » ? </3_composition/illustrations/inclure_une_image/changer_la_bounding_box_d_une_image>` ». En voici quelques unes :

- `[width=...]` permet de spécifier la taille de l'image, qui sera alors agrandie ou rétrécie à la dimension voulue ;
- `[height=...]` permet de spécifier la hauteur de l'image ;
- `[keepaspectratio=false]` et `[keepaspectratio=true]` permettent, dans les cas où on a spécifié à la fois la hauteur et la largeur de l'image, de déformer ou non, respectivement, l'image. Dans le cas où on choisit de ne pas la déformer, elle sera réduite ou agrandie de telle sorte que ses dimensions soient inférieures aux valeurs données ;
- `[scale=...]` permet de préciser le facteur d'agrandissement qui doit être appliqué à l'image. Par défaut, c'est `1`, bien entendu. Pour diviser les dimensions horizontales et verticales de l'image par deux, il faut indiquer `[scale=0.5]` ;
- `[angle=...]` permet de spécifier l'angle dont doit être tournée la figure, dans le sens inverse des aiguilles d'une montre, et en degrés. La figure est alors tournée autour de son coin inférieur gauche ;
- `[origin=...]` permet de spécifier le point autour duquel l'image doit être tournée. Cette origine doit contenir une ou deux lettres appartenant à l'ensemble \$\\{\\mathtt l, \\mathtt r, \\mathtt c, \\mathtt t, \\mathtt b\\}\$, qui signifient respectivement *left*, *right*, *center*, *top* et *bottom*.

Ces options sont à séparer les unes des autres par des virgules. Par exemple :

```latex
% !TEX noedit
\includegraphics[angle=45,width=4cm,keepaspectratio=true]{mafigure.jpg}
```

L'extension {ctanpkg}`graphicx` est entièrement documentée, avec force exemples, dans le document {texdoc}`LaTeX standard graphics and color packages <grfguide>` (en anglais). L'article [Stratégies pour inclure des graphiques dans des documents en LATEX](https://www.gutenberg.eu.org/IMG/pdf/Graphiques.pdf) offre une excellente vue d'ensemble en français.

Pour les plus curieux, il faut noter que, par rapport à {ctanpkg}`graphics`, l'extension {ctanpkg}`graphicx` utilise une syntaxe de commande un peu plus sophistiquée pour sa version de la commande `\includegraphics`. Elle est également capable de produire du code PostScript plus efficace et peut obtenir des résultats tout simplement inaccessibles à {ctanpkg}`graphics`.

Enfin, si vous utilisez d'anciens [formats](/1_generalites/glossaire/qu_est_ce_qu_un_format) de TeX, l'utilisation de {ctanpkg}`graphicx` peut demander des manipulations complémentaires. Si les utilisateurs d'[Eplain](1_generalites/glossaire/qu_est_ce_que_eplain) peuvent charger directement {ctanpkg}`graphicx`, les utilisateurs de Plain TeX doivent passer par l'une des deux méthodes suivantes :

- utiliser *l'émulateur* LaTeX {ctanpkg}`miniltx` ;
- utiliser le fichier {ctanpkg}`graphicx.tex <graphics-pln>` qui permet de charger {ctanpkg}`graphicx`.

## Avec l'extension « epsfig »

L'extension {ctanpkg}`epsfig` fournit les mêmes fonctionnalités que {ctanpkg}`graphicx` mais via une commande `\psfig` (également appelée `\epsfig`), capable d'émuler le comportement de l'ancienne extension {ctanpkg}`psfig`. Elle fournit également une assistance simple pour les anciens utilisateurs du package {ctanpkg}`epsf`.

Cette extension permet d'inclure une figure exportée par `xfig` au format combiné `eps`/LaTeX avec la commande `\input`.

