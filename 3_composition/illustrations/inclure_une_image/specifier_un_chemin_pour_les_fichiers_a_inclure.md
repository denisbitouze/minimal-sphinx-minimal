# Comment spécifier un chemin pour les fichiers à inclure ?

Par défaut, les commandes graphiques telles que `\includegraphics` cherchent où que se trouvent le fichier TeX pour déterminer où se trouve le fichier graphique à intégrer. Cela peut réduire votre flexibilité si vous choisissez de conserver vos fichiers graphiques dans un répertoire commun, loin de vos sources TeX ou LaTeX.

## Avec l'extension « graphicx »

L'extension {ctanpkg}`graphicx` fournit la commande `\graphicspath` (comme les extensions {ctanpkg}`graphics` et {ctanpkg}`epsfig`), dont l'argument contiendra une suite de chemins d'accès entre accolades (chaque chemin finissant par un « / ». En voici un exemple, à inclure dans le préambule du document :

```latex
% !TEX noedit
\graphicspath{{images/grandes/}{images/petites/}}
```

Cette commande `\graphicspath` a deux particularités :

- si certains systèmes TeX ou LaTeX ne vous permettent d'utiliser que des fichiers du répertoire courant et ses sous-répertoires, `\graphicspath` n'impose, lui, aucune restriction de ce type : vous pouvez accéder à des fichiers placés n'importe où ;
- elle n'affecte pas les opérations des extensions autres que celles citées ci-dessus. Ainsi, les extensions [obsolètes](/1_generalites/histoire/liste_des_packages_obsoletes) {ctanpkg}`epsf` et {ctanpkg}`psfig` y sont insensibles.

Le léger inconvénient de la méthode `\graphicspath` est son « inefficacité ». L'extension appelle LaTeX pour chaque entrée de la liste afin qu'il la recherche, ce qui ralentit la compilation. De plus, LaTeX se souvient du nom de tout fichier qu'il lui est demandé de rechercher, perdant ainsi de la mémoire, de sorte qu'un document utilisant un très grand nombre d'entrées graphiques pourrait être parasité par un manque de mémoire... même si un tel manque de mémoire est assez improbable avec n'importe quel document ordinaire dans un système LaTeX récent.

## Avec les variables d'environnement

Il est également possible d'ajouter des chemins de dossiers contenant des fichiers d'images à la variable d'environnement `TEXINPUTS` (aussi évoquée à la question « {doc}`Comment changer le chemin recherche de fichiers de TeX ? </5_fichiers/tds/changer_le_chemin_d_acces_d_un_fichier>` »). Ce genre de modifications dépend de l'environnement utilisé. Sur un système Unix/Linux, la ligne ressemblera à ceci :

```latex
% !TEX noedit
TEXINPUTS=.:⟨chemin(s) de dossiers image(s)⟩ :
```

Sur un système Windows, le séparateur sera « `;` » plutôt que « `:` ». Le « `.` » initial est là pour s'assurer que le répertoire courant est recherché en premier. Le « `:` » de fin indique que d'autres chemins peuvent venir compléter ces valeurs.

Cette méthode a le mérite de l'efficacité (TeX fait *toutes* les recherches, ce qui va vite) mais elle reste lourde et peut s'avérer peu pratique à utiliser dans les configurations Windows.

## Avec l'extension « relinput »

L'extension {ctanpkg}`relinput` permet d'inclure un fichier dans un répertoire, et de rendre toutes les inclusions faites par ce fichier relatives au répertoire où se trouve ce dernier fichier.

Pour illustrer cela, prenons un exemple avec la hiérarchie de répertoire suivante d'un répertoire nommé `pere/`. Celui-ci contient :

- un fichier `fichier1.tex` ;

- un fichier `fichier2.tex` ;

- un sous-répertoire `fils/` qui contient :

  - un fichier `inclus.tex`,
  - un fichier `fichier2.tex`.

Le fichier `inclus.tex` contient la ligne suivante :

```latex
% !TEX noedit
\input{fichier2.tex}
```

et est inclus par le fichier `fichier1.tex` grace à la commande

```latex
% !TEX noedit
\input{fils/inclus.tex}
```

La question est de savoir quel `fichier2.tex` sera utilisé. Dans le cas présent, c'est celui du répertoire `pere/`.

Supposons maintenant que l'extension {ctanpkg}`relinput` est utilisée et que la ligne suivante est présente dans le fichier `inclus.tex` :

```latex
% !TEX noedit
\relinput{fils}{inclus.tex}
```

Supposons également que la ligne suivante est incluse dans le fichier `fichier1.tex` :

```latex
% !TEX noedit
\relinput{.}{fichier2.tex}
```

Alors c'est le fichier du répertoire `fils/` qui est utilisé. Pour inclure le fichier `fichier2.tex` du répertoire `pere/`, il suffit d'utiliser le code suivant :

```latex
% !TEX noedit
\relinput{..}{fichier2.tex}
```

## Avec l'extension « import »

Si votre document est divisé en plusieurs répertoires et que chaque répertoire a ses graphiques associés, l'extension {ctanpkg}`import` pourrait bien être ce qu'il vous faut. Voir à ce sujet la question « {doc}`Comment inclure des fichiers sans modifier leurs liens internes ? </3_composition/document/utiliser_des_fichiers_dans_differents_repertoires>` ».

______________________________________________________________________

*Source :* {faquk}`Importing graphics from "somewhere else" <FAQ-graphicspath>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fichiers,inclure des fichiers
```

