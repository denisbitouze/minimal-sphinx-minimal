# Pourquoi LaTeX n'accepte-t-il pas tous les formats d'image ?

- Voici la réponse proposée par \\nom\{Pascal}{Périchon} : « Le problème est le suivant : un fichier `dvi` + image `tiff`, `gif`, `tga`... Comment faire digérer tout ça par votre imprimante ? Le fichier `dvi` peut se faire convertir en un langage d'impression compréhensible par votre imprimante, mais vos formats d'images... Faudrait-il en plus prévoir un système de conversion de `tiff`, `gif`, `tga`... vers le langage d'impression de votre imprimante (GhostScript/GhostView propose quelques filtres, mais bon). À part quelques langages de description de pages comme PS, et un ou deux autres qui pratiquent une compatibilité ascendante quant aux versions du langage qu'ils utilisent, les autres fabricants d'imprimantes changent de langage quasiment à chaque modèle ou série d'imprimante (impossible de tenir à jour autant de convertisseurs).

Pour tout cela le langage PS (`ps` et `eps`) nous rend grandement service en nous simplifiant la vie (et pour d'autres raisons aussi). C'est peut-être un peu plus lourd à manipuler, mais plus simple pour tout le monde de convertir vos images en `eps`. De plus, si vous ne disposez pas d'imprimante PS, GhostScript\\slash GhostView se chargera d'interpréter le fichier PS (texte + images), fabriquera une bitmap à la bonne résolution et pourra l'envoyer à votre imprimante via votre pilote d'impression. Tout cela automatisé dans une bonne distribution est transparent à l'utilisateur. »

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```

