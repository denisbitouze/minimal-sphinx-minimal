# Comment inclure une capture d'écran ?

LaTeX lui-même ne permet pas de réaliser une capture d'écran, mais vous pouvez inclure n'importe quelle image au format PNG, JPEG ou PDF, y compris une capture réalisée au moyen d'un outil externe :

```latex
% !TEX noedit
\includegraphics[width=.75\linewidth]{ecran1}
```

## Comment réaliser la capture d'écran ?

De nombreux outils permettent de réaliser ce genre de choses, que ce soit sous Linux/Unix, Mac OS ou Windows.

### Sous Linux

Le plus simple est d'utiliser [The Gimp](https://fr.wikipedia.org/wiki/GIMP) (*GNU Image Manipulation Program*). C'est un logiciel de dessin très complet, assez similaire à [Adobe Photoshop](https://fr.wikipedia.org/wiki/Adobe_Photoshop) (logiciel commercial, non disponible pour Linux).

Vous devriez pouvoir l'installer à partir des paquets de votre distribution Linux.

Pour réaliser la capture d'écran, allez dans le menu *Fichier* \$\\rightarrow\$ *Créer* \$\\rightarrow\$ *Capture d'écran*. Vous pouvez ensuite choisir ce que vous voulez capturer (tout l'écran, une seule fenêtre...), puis un délai (mettez 5 secondes pour un premier essai). Quand vous cliquerez sur OK, vous aurez alors 5 secondes pour disposer vos fenêtres commes vous l'entendez, puis cliquer sur ce que vous voulez capturer. L'image s'ouvre ensuite dans *Gimp*, où vous pourrez la retailler puis l'enregistrer dans un fichier. Il ne vous restera plus qu'à l'inclure dans votre document LaTeX avec `\includegraphics`.

### Sous Mac OS

Mac OS permet de faire une capture du ou des écrans, ou bien de zones de l'écran, grâce à des [combinaisons de touches](https://support.apple.com/fr-fr/HT201361). Ceci vous créera un ou plusieurs fichiers PNG sur votre bureau, correpondants aux captures demandées :

- « Shift + Command + 3 » pour une capture complète du ou des écrans,
- « Shift + Command + 4 » pour une capture d'une zone de l'écran (à sélectionner à la souris),
- « Shift + Command + Espace + 4 » pour une capture d'une fenêtre (à sélectionner à la souris).

### Sous Windows

La touche `PrtSc` (*print screen*) en haut à droite du clavier permet de faire une copie de l'écran dans la mémoire. Ensuite, vous pourrez coller cette image dans un logiciel de dessin avec la combinaire « Ctrl + V ».

## Quel format d'image utiliser ?

Les images obtenues sont relativement petites, par rapport à une photographie en haute résolution (elles font la taille d'un écran...), mais elles montrent généralement du texte et des traits fins, qui ne supportent pas une compression avec perte de qualité. Préférez donc le format PNG au format JPG, si vous utilisez `pdf(La)TeX` ou `Lua(La)TeX`.

Si vous utilisez LaTeX pour produire un fichier DVI, il ne faudra pas oublier d'exporter le résultat en PS (ou PS encapsulé) pour l'inclure dans un document LaTeX (on peut éventuellement utiliser la commande `\DeclareGraphicsRule` [pour effectuer une conversion à la volée](/3_composition/illustrations/inclure_une_image/inclure_une_image)).

______________________________________________________________________

*Sources :*

- [Capture d'écran](https://fr.wikipedia.org/wiki/Capture_d'écran),
- [How to embed screenshots properly?](https://tex.stackexchange.com/questions/4430/how-to-embed-screenshots-properly)

```{eval-rst}
.. meta::
   :keywords: LaTeX,illustrations,screenshot,capture d'écran,copie d'écran,faire une capture d'écran avec LaTeX
```

