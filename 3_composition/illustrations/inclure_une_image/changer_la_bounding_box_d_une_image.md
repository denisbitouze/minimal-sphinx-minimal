# Comment modifier la taille d'une « bounding box » ?

Le {doc}`format EPS </5_fichiers/postscript/postscript_encapsule>` permet (et même nécessite) de spécifier la taille de la figure qu'il contient. C'est ce que l'on appelle la boîte encadrante ou *bounding box*, qui est définie au début des fichiers EPS par une ligne ressemblant à :

```postscript
%%BoundingBox : 0 0 120 250
```

Ces nombres indiquent les coordonnées du coin inférieur gauche et du coin supérieur droit de l'image, la coordonnée horizontale précédant la coordonnée verticale. Ces coordonnées sont exprimées en *points Postscript*. Un point PS vaut \$\\frac\{1}\{72}\$ pouce, alors qu'un point LaTeX vaut \$\\frac\{1}{72,27}\$ pouce...

Passons sur ces détails, et revenons à nos moutons : il est possible de forcer la taille de la *bounding box* en l'incluant avec la commande `\includegraphics` du package {ctanpkg}`graphicx`, et en utilisant l'argument optionnel « `bb = x0 y0 x1 y1` ». Ainsi, l'image occupera les dimensions demandées (en points LaTeX).

Il est possible également de spécifier la *bounding box* par rapport à celle existante, au cas où celle-ci ne commencerait pas en \$(0,0)\$, par exemple. Par exemple, en précisant « `viewport = 0 0 72 72` », on obtiendra le coin inférieur gauche de la figure, quelle que soit la *bounding box* de la figure.

Une autre possibilité est de spécifier « `trim = 10 20 30 40` », par exemple, pour supprimer 10 points à gauche, 20 points en bas, 30 points à droite et 40 points en haut de l'image.

:::{warning}
Dans tous les cas, cela ne signifie pas qu'elle sera coupée, ou retaillée aux dimensions voulues, mais que la place qui lui sera réservée dans le document sera celle qui est précisée.

En utilisant la commande `includegraphics*` ou l'option `clip`, la figure sera coupée aux dimensions demandées. La commande `\includegraphics` peut prendre également plusieurs autres arguments optionnels, pour redimensionner, tourner, couper l'image incluse. Voir la question « [Comment insérer une image ?](/3_composition/illustrations/inclure_une_image/inclure_une_image) » pour plus de détails.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,inclure une image Postscript,boîte englobante,taille d'une image
```

