# Comment limiter la largeur d'une image ?

Supposons que vous ayez des images dont la largeur peut grandement varier :

- s'ils sont moins grands que la largeur de la page, vous souhaitez les afficher à leur taille naturelle ;
- sinon, vous souhaitez réduire l'échelle de l'image afin qu'elle tienne dans la largeur de la page.

Pour obtenir ce résultat, le code de l'extension {ctanpkg}`graphics` donne une solution :

```latex
% !TEX noedit
\makeatletter
\def\maxwidth{%
  \ifdim\Gin@nat@width>\linewidth % Comparaison de la largeur de l'image et de largeur de la ligne de texte
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother
```

Ce code définit une largeur qui a les propriétés souhaitées. Cette largeur s'utilise ainsi :

```latex
% !TEX noedit
\includegraphics[width=\maxwidth]{figure}
```

Bien évidemment, ce code peut être librement adapté pour modifier la contrainte. Il suffit alors en général de remplacer `\linewidth` par une autre expression.

______________________________________________________________________

*Source :* {faquk}`Limit the width of imported graphics <FAQ-grmaxwidth>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphiques,graphics
```

