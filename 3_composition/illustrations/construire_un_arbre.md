# Comment dessiner un arbre ?

- {ctanpkg}`pst-tree` est un package du bundle PSTricks développé pour dessiner des arbres. {texdoc}`Sa documentation est très détaillée <pst-tree>`, mais voici un exeple simple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{pst-tree}
\begin{document}
\pstree[nodesep=2pt,levelsep=5ex]{\Tcircle{A}}%
                    {\pstree[arrows=->]{\Tr{B}}%
                            {\Tr{E} \Tr{F}}
                     \Tr{C}}
\end{document}
```

- Bien sûr, il est possible de faire l'équivalent avec {ctanpkg}`TikZ <tikz>`. Voir par exemple : <https://texample.net/tikz/examples/feature/trees/>.

```{eval-rst}
.. todo:: ajouter un exemple ici
```

- Le package {ctanpkg}`treesvr` peut servir à faire ce genre de chose :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{trees}
\begin{document}
\begin{picture}(100,100)
\setlength\unitlength{2mm}
\branchlabels ABC      % Valeur par défaut : 012
\root(2,10)      0.    % La racine est en (2,10)
                       % et sera désignée par 0
\branch2{16}     0:1,2.% 0 a pour fils 1 et 2;
                       % "16" est écrit au-dessus;
                       % l'espace est optionnel
                       % mais :,. obligatoires.
\leaf{4}{$u_1$}  1.    % le noeud 1 est une
                       % feuille avec 4 au-dessus
                       % et "$u_1$" à droite
\branch2{12}     2:3,7.% L'arête liant 2 et 3
                       % est la première
                       % (au-dessus), elle
                       % porte l'étiquette A
  \tbranch2{9}   3:4,5,6.
    \leaf{4}{$u_3$}4.
    \leaf{3}{$u_4$}5.
    \leaf{2}{$u_5$}6.
  \leaf{3}{$u_2$}  7.
\end{picture}
\end{document}
```

- Les packages {ctanpkg}`epic`, {ctanpkg}`eepic`, {ctanpkg}`ecltree` permettent de créer des arbres.

Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass{article}

\usepackage{epic,eepic,ecltree}

\begin{document}
\begin{bundle}{racine}
\chunk{feuille 1}
\chunk{feuille 2}
\drawwith{\dottedline{3}}
\chunk{feuille 3}
\drawwith{\drawline}
\chunk{feuille 4}
\end{bundle}
\end{document}
```

- Si aucune des solutions précédentes ne convient, jeter un coup d'œil à {ctanpkg}`qobitree` ou {ctanpkg}`treetex`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,PStricks,TikZ,arbres,graphes,Directed acyclic graph,Graphe orienté acyclique,dessin,figures,représentation
```

