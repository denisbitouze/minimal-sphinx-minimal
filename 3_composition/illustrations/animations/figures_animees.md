---
myst:
  substitutions:
    image1: |-
        ```{image} /3_composition/animations/animated_spring_on_a_sinusoid.gif
        :target: https://tex.stackexchange.com/questions/158668/nice-scientific-pictures-show-off
        ```
---
# Comment faire une figure animée ?

{{ image1 }}

Je vois des animations très démonstratives sur les sites de questions/réponses. Par exemple :

- [How to make the length of a car moving on a roller coaster track remain unchanged?](https://tex.stackexchange.com/questions/175874/how-to-make-the-length-of-a-car-moving-on-a-roller-coaster-track-remain-unchange/179059),
- [How can I draw this cycloid diagram with TikZ?](https://tex.stackexchange.com/questions/196957/how-can-i-draw-this-cycloid-diagram-with-tikz),
- [Drawing an animated circle in LaTeX](https://tex.stackexchange.com/questions/303331/drawing-an-animated-circle-in-latex),
- [Nice scientific pictures show off](https://tex.stackexchange.com/questions/158668/nice-scientific-pictures-show-off).

Comment ça marche ?

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « Tikz/PStricks/autre »
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,animations,dessin animé,illustration scientifique
```

