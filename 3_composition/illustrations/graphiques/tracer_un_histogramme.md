# Comment réaliser des diagrammes en bâtons ?

## Avec l'extension « PGFplots »

L'extension {ctanpkg}`PGFPlots <pgfplots>` vous permet de dessiner des diagrammes en bâtons, et bien d'autres encore. Basé sur {ctanpkg}`TikZ <pgf>`, elle est très polyvalente et permet toutes sortes de représentations de données. La contrepartie de cette polyvalence est que {texdoc}`sa documentation <pgfplots>` est volumineuse (mais très claire).

```latex
\documentclass{article}
\usepackage{pgfplots}
\usepackage{graphicx}
\pgfplotsset{width=7cm}
\pagestyle{empty}

\begin{document}


\begin{tikzpicture}
\begin{axis}[x tick label style={/pgf/number format/1000 sep=},ylabel=Population,enlargelimits=0.15,legend style={at={(0.5,-0.15)},anchor=north,legend columns=-1},ybar=5pt,% configures `bar shift'
    bar width=9pt,nodes near coords,point meta=y *10^-7, % the displayed number
    ]
    \addplot coordinates{(1930,50e6) (1940,33e6)(1950,40e6) (1960,50e6) (1970,70e6)};
    \addplot coordinates{(1930,38e6) (1940,42e6)(1950,43e6) (1960,45e6) (1970,65e6)
    };
    \legend{Lointain,Proche}
\end{axis}
\end{tikzpicture}

\end{document}
```

## Avec l'extension « bchart »

Si vos besoins sont très modestes, l'extension {ctanpkg}`bchart` peut être suffisante. Elle s'appuie elle aussi sur {ctanpkg}`TikZ <pgf>`.

## Avec l'extension « pstricks »

Si vous avez l'habitude d'utiliser PStricks, les extensions {ctanpkg}`pst-bar` et {ctanpkg}`bardiag` fournissent des commandes pour dessiner des diagrammes en bâtons.

