# Comment inclure un graphique ?

Imaginons que vous ayez un tableau de données, et que vous souhaitiez en faire une représentation graphique dans votre document LaTeX. Pour cela, vous n'avez besoin d'aucun logiciel externe. En dessinant votre graphique directement en LaTeX, vous aurez une intégration complète du texte et de la figure : police de caracatères identique, taille de la figure qui s'adapte à la taille du texte, couleurs identiques (si besoin).

:::{note}
Si vous avez déjà un graphique, obtenu à partir d'un autre logiciel (R, MatLab, LibreOffice, Excel...) vous pouvez simplement l'inclure avec :

```latex
% !TEX noedit
\includegraphics[width=50ex]{fichier}
```

Dans ce cas, il est préférable que le graphique soit au format PDF, mais il est également possible d'inclure du PNG ou du JPEG.

Voir la question « [Comment insérer une image ?](/3_composition/illustrations/inclure_une_image/inclure_une_image) ».
:::

## Avec l'extension « PGFplots »

L'extension {ctanpkg}`PGFplots <pgfplots>` utilise {ctanpkg}`TikZ <pgf>` pour construire le dessin, tout en fournissant une syntaxe intuitive pour décrire le résultat que vous voulez.

Elle propose un environnement `axis`, dont les paramètres modifient le style de graphique :

```{eval-rst}
.. todo:: Ajouter un exemple.
```

## Avec l'extension « TikZ » (mais sans « PGFplots »)

Si votre graphique est tellement particulier qu'il s'écarte de ce que sait faire {ctanpkg}`PGFplots <pgfplots>`, vous pouvez très bien utiliser directement {ctanpkg}`TikZ <pgf>`.

Ici, un exemple plutôt simple pour construire un repère avec des axes gradués :

```latex
\documentclass{article}
  \usepackage{tikz}
  \usepackage{verbatim}
  \pagestyle{empty}

\begin{comment}
:Title : Scatterplot
:Tags : Plots, Axes, Foreach

A scatterplot of the relationship between education and income. The plotted
data is from the `car R package`_ by John Fox. Note that the data is released
under the GPL 2 licence.

:Author : Ista Zahn

.. _car R package : http://cran.r-project.org/src/contrib/Descriptions/car.html
\end{comment}

\begin{document}
\begin{tikzpicture}[scale=.5, only marks, y=.5cm]
    %%\draw plot[mark=*,xshift=-6cm] file {data/ScatterPlotExampleData.data};
    \draw[->,xshift=-6cm] (6,0) -- coordinate (x axis mid) (17,0);
    \draw[->,xshift=-6cm] (6,0) -- coordinate (y axis mid)(6,27);
    \foreach \x in {6,8,10,12,14,16}
        \draw [xshift=-6cm](\x cm,1pt) -- (\x cm,-3pt)
            node[anchor=north] {$\x$};
    \foreach \y/\ytext in {0/0,2.5/5000,5/10000,7.5/15000,10/20000,12.5/25000}
        \draw (1pt,\y cm) -- (-3pt,\y cm) node[anchor=east] {$\ytext$};
    \node[below=5mm] at (x axis mid) {Années d'études};
    \node[left=15mm,rotate=90] at (y axis mid) {Revenus};
\end{tikzpicture}
\end{document}
```

## Avec l'extension « PStricks »

```{eval-rst}
.. todo:: Proposer un exemple avec :ctanpkg:`PStricks <pstricks>`.
```

______________________________________________________________________

*Sources :*

- [Les graphiques sous TikZ](https://blog.dorian-depriester.fr/latex/tikz/les-graphiques-sous-tikz),
- [Comment faire de beaux graphiques avec Tikz et PGFPLOTS](http://bertrandmasson.free.fr/index.php?article28/comment-faire-de-beaux-graphiques-avec-tikz-et-pgfplots),
- [Plots examples](https://texample.net/tikz/examples/tag/plots/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphique,statistiques,TikZ,barplot,histogramme,tracer une fonction,mathématiques,data science
```

