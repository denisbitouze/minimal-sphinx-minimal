# Comment générer une table des matières ?

LaTeX fournit la commande `\tableofcontents`, qui affiche la table des matières à l'endroit où elle est appelée. À chaque compilation, si la commande `\tableofcontents` est présente dans le document,

- LaTeX crée un fichier `.toc` dans lequel il place tous les renseignements nécessaires (niveau de sectionnement (par exemple un chapitre, une section), numéro, titre, page) ;
- le fichier `.toc` est par la suite inclus à l'endroit où est appelée la commande `\tableofcontents`.

En général, il est donc nécessaire de faire trois compilations pour avoir une table des matières à jour :

- la première créera le fichier `.toc` ;
- la seconde l'inclura, et modifiera donc probablement les numéros de page selon la taille de cette table des matières dans le document. Un nouveau fichier `.toc` sera généré ;
- la troisième inclura ce nouveau fichier avec les bons numéros de page.

Voici un exemple simple pour un utilisateur francophone :

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[body={8cm,20cm}]{geometry}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\tableofcontents
\contentsline {section}{\numberline {1}Qui suis-je ?}{1}%
\contentsline {section}{\numberline {2}Où vais-je ?}{1}%
\contentsline {subsection}{\numberline {2.1}Dans quel état j'erre ?}{1}%
\bigskip
Voici de grandes questions !
\section{Qui suis-je ?}
Bonne question.
\section{Où vais-je ?}
Décidément, très bonne question.
\subsection{Dans quel état j'erre ?}
Euh...
\end{document}
```

Cet exemple appelle deux commentaires :

- la présence de l'extension {ctanpkg}`babel` avec l'option `french` permettant de franciser le titre de la table des matières qui est, sinon, « Contents ». La question « [Comment changer le titre de la table des matières ?](/3_composition/annexes/tables/changer_le_titre_de_la_table_des_matieres) » propose une autre solution plus flexible ;
- la présence d'une commande d'espacement, ici `\bigskip`. Sans cela le texte qui suit la table des matières serait accolée à cette dernière. Il s'agit là d'un conseil qui n'a, bien entendu, rien d'impératif.

Voici d'ailleurs le contenu du fichier `.toc` de cet exemple (hors une ligne qu'ajoute l'extension {ctanpkg}`babel`) pour montrer ce que LaTeX génère automatiquement :

```latex
% !TEX noedit
\contentsline {section}{\numberline {1}Qui suis-je ?}{1}%
\contentsline {section}{\numberline {2}Où vais-je ?}{1}%
\contentsline {subsection}{\numberline {2.1}Dans quel état j'erre ?}{1}%
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tables des matières,table des matières
```

