# Comment faire si les numéros dépassent de la table des matières ?

LaTeX construit la table des matières, la liste des figures, des tableaux, et les éventuelles autres tables du même genre, sur la base d'une mise en page définie dans la classe. Par conséquent, elles ne s'adaptent *pas* à la taille des éléments qu'elles contiennent, comme elles le feraient si elles utilisaient un environnement `tabular` ou équivalent.

Cette implémentation peut causer des problèmes, notamment quand le document comporte des imbrications profondes de sections ou des numéros de page très grands : les numéros en question ne rentrent tout simplement pas dans l'espace qui leur est alloué dans la classe.

## Avec des changements de style de la table des matières

La question « {doc}`Comment changer le style de la table des matières ? </3_composition/annexes/tables/changer_le_style_de_la_table_des_matieres>` » propose des méthodes qui répondent à ce besoin. Par exemple, avec l'utilisation de l'extension {ctanpkg}`tocloft` :

```latex
% !TEX noedit
\setlength\cftsectionnumwidth{4em}
```

## Avec la classe « memoir »

La commande ci-dessus peut être utilisée dans les documents composés avec la classe {ctanpkg}`memoir` (par le même auteur que {ctanpkg}`tocloft`). Mais cette classe propose également un autre mécanisme pour atteindre cet objectif :

```latex
% !TEX noedit
\cftsetindents{⟨niveau⟩}{⟨indentation⟩}{⟨largeur⟩}
```

Dans le détail :

- `⟨niveau⟩` vaut `chapter`, `section` ou autre ;
- `⟨indentation⟩` spécifie la « marge » avant le début de l'entrée ;
- `⟨largeur⟩` est la largeur de la boîte dans laquelle le numéro sera inséré (elle doit donc être suffisamment grande pour le plus grand numéro, avec l'espacement nécessaire pour le séparer de ce qui le suit dans la ligne.

______________________________________________________________________

*Source :* {faquk}`Numbers too large in table of contents, etc. <FAQ-tocloftwrong>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,table of contents,table des matières,sommaire
```

