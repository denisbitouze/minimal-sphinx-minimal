# Comment changer le titre de la table des matières ?

## Avec les commandes de base

Cette technique n'est qu'un cas particulier de la méthode présentée à la question « [Comment changer les textes prédéfinis de LaTeX ?](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) ». Il suffit ainsi de redéfinir la commande `\contentsname`. Par exemple :

```latex
% !TEX noedit
\renewcommand*{\contentsname}{Plan du document}
```

## Avec l'extension « babel »

L'extension {ctanpkg}`babel` permet d'utiliser plusieurs langues. Son utilisation impose une manière particulière de redéfinir le titre de la table des matières. Par exemple pour le français :

```latex
% !TEX noedit
\addto\captionsfrench{%
  \renewcommand*{\contentsname}{Plan du document}%
}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,table des matières,titre
```

