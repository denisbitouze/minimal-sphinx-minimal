# Comment enlever le numérotation des pages de la table des matières ?

## Avec les commandes de base

Le problème est assez compliqué :

- d'une part, la table des matières est créée par l'inclusion d'un fichier généré automatiquement et il est peu souhaitable d'aller le modifier à la main ;
- d'autre part, par défaut, la commande `\tableofcontents` appelle la commande `\thispagestyle{plain}`, forçant ainsi le style `plain` (*i.e.* avec le numéro en bas de page, au centre).

Pour résoudre le second problème, l'extension {ctanpkg}`fancyhdr` permet de redéfinir les différents styles de page, en particulier le style `plain`. On inclura donc la commande suivante dans le préambule du document :

```latex
% !TEX noedit
\fancypagestyle{plain}{%
 \fancyhf{}%
 \renewcommand{\headrulewidth}{0pt}}%
```

Cela a pour effet de redéfinir *globalement* le style `plain`. S'il ne faut le redéfinir que pour la table des matières, le plus simple est d'inclure l'ensemble dans un groupe (défini par les commandes `\bgroup` et `\egroup`), en prenant soin d'ajouter un `\clearpage` après la table des matières pour que la redéfinition des en-têtes soit prise en compte pour la dernière page. Ce qui donne :

```latex
% !TEX noedit
\bgroup
\fancypagestyle{plain}{%
 \fancyhf{}%
 \renewcommand{\headrulewidth}{0pt}}%
\tableofcontents
\clearpage
\egroup
```

Le premier problème est maintenant simple à résoudre : à partir de la solution précédente, il faut ajouter le style de page devant être appliqué dans la table des matières. D'où le code suivant :

```latex
% !TEX noedit
\bgroup
\fancypagestyle{plain}{%
 \fancyhf{}%
 \renewcommand{\headrulewidth}{0pt}}%
\pagestyle{empty}
\tableofcontents
\clearpage
\egroup
```

## Avec une rédéfinition des commandes internes

Une solution touchant aux [commandes internes](/2_programmation/macros/makeatletter_et_makeatother) consiste à mettre les définitions suivantes dans le préambule du document :

```latex
% !TEX noedit
\makeatletter
\def\addcontentsline@toc#1#2#3{%
  \addtocontents{#1}%
                {\protect\thispagestyle{empty}}%
  \addtocontents{#1}%
                {\protect\contentsline{#2}{#3}%
                                   {\thepage}}}
\def\addcontentsline#1#2#3{%
  \@ifundefined{addcontentsline@#1}%
  {\addtocontents{#1}%
                 {\protect\contentsline{#2}{#3}%
                                    {\thepage}}}
  {\csname addcontentsline@#1\endcsname{#1}{#2}%
                                          {#3}}}
\makeatother
```

Cela ajoute simplement la commande `\thispagestyle{empty}` avant chaque entrée de la table des matières (ce qui peut sembler un peu plus « violent » que la solution précédente).

```{eval-rst}
.. meta::
   :keywords: LaTeX,tables des matières,table des matières,pagination,numérotation de page
```

