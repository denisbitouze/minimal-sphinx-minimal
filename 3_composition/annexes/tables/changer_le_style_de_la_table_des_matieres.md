# Comment changer le style de la table des matières ?

La mise en forme des entrées de la table des matières est contrôlée par un certain nombre de commandes internes (présentées à la section 2.3 du {doc}`The LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>`), notamment les commandes :

- `\@pnumwidth` qui contrôle l'espace laissé pour les numéros de page ;
- `\@tocrmarg` qui contrôle l'indentation de la marge de droite ;
- `\@dotsep` qui contrôle la séparation des points dans les pointillés ;
- et la série des commandes nommées `\l@xxx`, où `xxx` est le nom d'un niveau de sectionnement (comme `chapter`, `section`...), qui contrôlent la mise en forme du titre correspondant, y compris l'espace pour les numéros de section.

Toutes ces commandes internes peuvent être redéfinies individuellement pour obtenir l'effet souhaité, en suivant les consignes de la question « [À quoi servent « \\makeatletter » et « \\makeatother » ?](/2_programmation/macros/makeatletter_et_makeatother) ».

Néanmoins, ce travail sur des commandes internes peut être évité en utilisant différentes extensions.

## Avec l'extension « tocloft »

L'extension {ctanpkg}`tocloft` fournit un ensemble de commandes pouvant modifier facilement la mise en forme de la table des matières, autant pour les différentes dimensions utilisées que pour le style du texte.

L'exemple ci-dessous montre comment cette extension permet d'enlever les points de suspension des sous-sections et de décaler le numéro des sections et sous-sections. Il passe par la redéfinition de certaines variables proposées par l'extension :

```latex
\documentclass[8pt]{extarticle}
\usepackage[width=6cm]{geometry}
\usepackage{tocloft}
\usepackage[frenchb]{babel}

\renewcommand{\cftsecafterpnum}{%
                 \cftparfillskip}
\renewcommand{\cftsubsecleader}{%
                 \hfill}
\renewcommand{\cftsubsecafterpnum}{%
                 \cftparfillskip}
\pagestyle{empty}
\begin{document}
\part{Vocabulaire}
\section{Ga}
\subsection{Bu}
\subsection{Zo}
\section{Meu}
\bigskip % On triche pour afficher la table des matières théorique.
\section*{Table des matières}
\contentsline {part}{I\hspace {1em}Vocabulaire}{1}%
\contentsline {section}{\numberline {1}Ga}{1}%
\contentsline {subsection}{\numberline {1.1}Bu}{1}%
\contentsline {subsection}{\numberline {1.2}Zo}{1}%
\contentsline {section}{\numberline {2}Meu}{1}%
\end{document}
```

Comme les mêmes mécanismes sont utilisés pour la liste des figures et la liste des tableaux, la mise en forme de ces tables peut être contrôlée de la même façon.

## Avec l'extension « titletoc »

L'extension {ctanpkg}`titletoc` permet également de modifier la mise en page de la table des matières. Elle est peut-être un peu moins conviviale que {ctanpkg}`tocloft`, mais offre plus de possibilités. Elle est distribuée avec l'extension {ctanpkg}`titlesec` qui permet de redéfinir le style des commandes de sectionnement. C'est pour cela que la documentation de {ctanpkg}`titletoc` se trouve à la fin de celle de {ctanpkg}`titlesec`.

L'exemple ci-dessous montre comment {ctanpkg}`titletoc` permet d'afficher toutes les sous-sous-sections de la table des matières sans retour à la ligne.

```latex
% Pseudocode
\documentclass[8pt]{extarticle}
  \usepackage[width=6cm]{geometry}
  \usepackage{titletoc}
  \usepackage[frenchb]{babel}
  \pagestyle{empty}

\newcommand*{\subsectionbreak}{%
    \space}
\titlecontents*{subsubsection}[2cm]
    {$\bullet$\space \itshape\small}
    {}{}{,~\thecontentspage}[ --- ][]

\begin{document}
\section{Ga}
  \subsection{Bu}
    \subsubsection{Bu long}
    \subsubsection{Bu court}
  \subsection{Zo}
    \subsubsection{Zo rare}
\section{Meu}

\section*{Table des matières}
\contentsline {section}{\numberline {1}Ga}{1}%
\contentsline {subsection}{\numberline {1.1}Bu}{1}%
\contentsline {subsubsection}{\numberline {1.1.1}Bu long}{1}%
\contentsline {subsubsection}{\numberline {1.1.2}Bu court}{1}%
\contentsline {subsection}{\numberline {1.2}Zo}{1}%
\contentsline {subsubsection}{\numberline {1.2.1}Zo rare}{1}%
\contentsline {section}{\numberline {2}Meu}{1}%
\contentsfinish
\end{document}
```

## Avec l'extension « etoc »

L'extension {ctanpkg}`etoc` offre une flexibilité similaire, ainsi que la possibilité d'avoir tes tables des matières multicolonnes et des encadrés autour des tables (et autres).

## Avec les classes « KOMA-Script »

Les classes {ctanpkg}`KOMA-Script` fournissent si besoin une structure variable pour la table des matières et calculent automatiquement l'espace nécessaire pour les numéros.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` inclut toutes les fonctionnalités de {ctanpkg}`tocloft` (elles sont toutes deux du même auteur).

______________________________________________________________________

*Source :* {faquk}`The format of the Table of Contents, etc. <FAQ-tocloft>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,table des matières,sommaire,table of contents,structure du document,sections,titres,paragraphes
```

