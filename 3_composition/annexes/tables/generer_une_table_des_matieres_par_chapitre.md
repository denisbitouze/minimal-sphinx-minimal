# Comment obtenir une table des matières par partie, chapitre ou section ?

Cette mise en forme particulière de tables des matières réduites dite « mini-tables » s'obtient avec l'extension {ctanpkg}`minitoc`. Cette extension prend également en charge les mini-listes de tableaux et de figures. Toutefois, le cas des mini-bibliographies pose un problème différent, développé en question « [Comment faire des bibliographies séparées par chapitre ?](/3_composition/annexes/bibliographie/bibliographies_par_chapitre) ».

Dans la pratique, l'extension génère un fichier auxiliaire (`aux`) pour chaque chapitre afin de le traiter dans le chapitre par la suite. Chaque nouvelle table des matières créée est ensuite stockée dans un fichier `.mtc⟨n⟩` où `⟨n⟩` est le numéro absolu de la section où apparaît ladite table des matières. Voici un exemple d'utilisation :

```latex
% !TEX noedit
\usepackage{minitoc}
...
\begin{document}
...
\dominitoc \tableofcontents
\dominilof \listoffigures
...
\chapter{Que commence le texte}
\minitoc \mtcskip \minilof
...
```

Dans cet exemple, les commandes :

- `\dominitoc` et `\dominilof` indiquent à LaTeX de constituer les fichiers auxiliaires pour les mini-tables et les mini-listes de figure. Ces commandes s'appellent respectivement avant la commande `\tableofcontents` et la commande `listoffigures` ;
- `\minitoc` et `\minilof` intègrent respectivement les mini-tables et les mini-listes de figure dans le document ;
- `\mtcskip` ajoute un espace vertical.

L'extension propose également la commande `\faketableofcontents` qui peut remplacer la commande `\tableofcontents` afin que la table des matières générale n'apparaisse pas. Par ailleurs, elle définit le compteur `\minitocdepth` qui permet de modifier la profondeur des mini-tables.

L'extension {ctanpkg}`babel` ne connaît pas l'extension {ctanpkg}`minitoc` mais cette dernière permet de prendre en charge d'autres langues que l'anglais. Par opposition, l'extension {ctanpkg}`hyperref` reconnaît {ctanpkg}`minitoc` et traite les mini-tables de la même manière que les tables des matières classiques.

______________________________________________________________________

*Source :* {faquk}`Table of contents, etc., per chapter <FAQ-minitoc>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,table des matières,mini-table,minitoc,toc,aux
```

