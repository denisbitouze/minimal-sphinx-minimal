# Comment référencer l'index dans la table des matières ?

Ce sujet est abordé de manière générale à la question « [Comment ajouter une entrée dans la table des matières ?](/3_composition/annexes/tables/ajouter_une_entree_a_une_table_des_matieres) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,index,table des matières
```

