# Comment changer le style de certains mots indexés ?

Le symbole@'' dans la commande `\index` permet d'indiquer de répondre à ce besoin. Voici un exemple :

```latex
% !TEX noedit
\index{Sport@\textbf{Sport}}
```

Dans ce cas, la partie qui se trouve avant le «@'' » sert pour le tri de l'index et ce qui se trouve après correspond à ce qui sera affiché dans l'index. Ainsi, dans notre exemple, le mot « Sport » écrit en gras sera mis dans l'index à l'endroit où doit se trouver le terme `Sport`. C'est très utile notamment pour les mots commençant par une lettre accentuée, puisque pour `makeindex`, « équitation » ne commence pas par un « `e` » mais par un *symbole*.

Cet exemple montre comment combiner cela avec {doc}`la hiérarchisation de l'index </3_composition/annexes/index/construire_un_index_hierarchique>` :

```latex
% !TEX noedit
\documentclass{report}
  \usepackage[francais]{babel}
  \usepackage{makeidx}

  \makeindex

\begin{document}
\chapter{Sports.}
Le sport\index{Sport} c'est fantastique !
Mes sports préférés sont :
\begin{itemize}
\item l'escalade \index{Sport@\textbf{Sport}!%
       Escalade@\textsf{Escalade}}
   et surtout les sorties en falaise ;
\item l'équitatio \index{Sport@\textbf{Sport}!%
       Equitation@\textsf{\'Equitation}}
   et en particulier les disciplines de dressage%
\index{Sport@\textbf{Sport}!%
       Equitation@\textsf{\'Equitation}!%
       Dressage@\textsf{Dressage}}
   et de complet \index{Sport@\textbf{Sport}!%
       Equitation@\textsf{\'Equitation}!%
       Complet@\textsf{Complet}} :
\item le judo\index{Sport@\textbf{Sport}!Judo@\textsf{Judo}}.
\end{itemize}

\clearpage

\printindex

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,index,construire un index,ajouter un index,personnaliser un index
```

