# Comment changer le style d'un numéro de page de référence ?

Un petit exemple vaut mieux qu'un long discours :

```latex
% !TEX noedit
\index{Sport|textbf}
```

Une telle entrée dans l'index verra son numéro de page mis en gras. Bien entendu, n'importe quelle commande peut être utilisée ici, à condition :

- qu'elle prenne un seul argument ;
- et qu'on lui supprime sa contre-oblique initiale `\`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,
```

