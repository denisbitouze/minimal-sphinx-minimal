# Comment générer un index ?

Par défaut, un index s'obtient en utilisant l'extension {ctanpkg}`makeidx` et des programmes de traitement d'index. L'extension définit en particulier la précieuse commande `\printindex` décrite ci-dessous et redéfinit quelques commandes présentes dans LaTeX.

## Les commandes à placer dans le document

Dans les faits, pour générer un index, trois commandes sont nécessaires :

- `\makeindex` à mettre dans le préambule pour indiquer à LaTeX qu'il doit stocker, dans un fichier `.idx`, les entrées de l'index. Sans cette commande, l'index sera vide ;
- `\index{⟨mot⟩}` permet d'ajouter l'entrée ⟨mot⟩ dans l'index. Elle se trouve donc à de multiples reprises dans le corps du texte et permet, par le biais de certains symboles d'obtenir des effets particuliers (voir ici les questions « [Comment changer le style de certains mots indexés ?](/3_composition/annexes/index/changer_le_style_de_certains_mots_indexes) », « [Comment changer le style d'un numéro de page de référence ?](/3_composition/annexes/index/changer_le_style_des_pages_de_reference) » et « [Comment construire un index hiérarchique ?](/3_composition/annexes/index/construire_un_index_hierarchique) ») ;
- `\printindex` place l'index dans le document à l'endroit où cette commande est disposée. Cette commande a principalement pour rôle d'inclure le fichier `.ind` contenant l'index trié.

Voici un exemple minimaliste de cette utilisation :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\usepackage{makeidx}
\makeindex
\begin{document}

Il ne se passe pas un jour\index{jour} sans que je pense à cette mélodie\index{mélodie}.

\printindex

\end{document}
```

## L'appel à un programme de génération d'index

Le fichier `.idx` contient vos entrées d'index (avec leur numéro de page) telles qu'elles arrivent au fur et à mesure dans votre document. Elles sont dès lors sans mise en forme et non triées. La résolution de ces deux problèmes passe par l'utilisation d'un des programmes évoqués à la question « [Quels sont les programmes générateurs d'index ?](/3_composition/annexes/index/generateurs_d_index) ». Ces programmes lisent le fichier `.idx` et le transforment en `.ind` qui contient vos entrées ordonnées mises en forme.

Voici le cas du programme `makeindex`. Pour créer l'index de votre fichier `mondoc.tex`, vous exécuteriez (après avoir compilé votre document LaTeX une première fois) :

```bash
makeindex mondoc.idx
```

C'est lors d'une compilation consécutive de votre document `mondoc.tex` que vous pourrez observer l'index.

Dans le cas de `makeindex`, il est possible d'indiquer un fichier de style d'index (fichier `.ist`) par l'option `-s`. Dans le cas où vous disposeriez d'un fichier de style `monstyle.ist`, vous exécuteriez :

```bash
makeindex -s monstyle.ist mondoc.idx
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,index
```

