# Comment faire référence à une autre entrée dans l'index ?

Pour cela, il faut détourner la méthode permettant de changer le style des pages de référence telle que décrire à la question « [Comment changer le style d'un numéro de page de référence ?](/3_composition/annexes/index/changer_le_style_des_pages_de_reference) » et utiliser la commande `\see` de l'extension {ctanpkg}`makeidx`. Par exemple :

```latex
% !TEX noedit
\index{Varappe|see{Escalade}}
```

Dans ce cas, la commande `\see`, qui prend deux arguments, « avalera » le numéro de page et affichera un renvoi vers le mot passé en premier argument. Par ailleurs, l'absence du caractère `\` devant `see` ci-dessus s'explique par le fait que c'est le programme générateur d'index qui va l'ajouter.

```{eval-rst}
.. meta::
   :keywords: LaTeX,index,entrée,référence
```

