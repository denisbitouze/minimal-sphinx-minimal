# Comment rappeler certains mots dans un haut de page ?

L'extension {ctanpkg}`fancyhdr` permet d'afficher le premier mot de la page en haut à gauche et le dernier mot en haut à droite, à la manière d'un dictionnaire.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

