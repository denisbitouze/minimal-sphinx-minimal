# Quels sont les programmes générateurs d'index ?

La génération d'un index avec LaTeX conduit à utiliser des programmes triant la sortie que génère LaTeX, cette sortie triée étant inclue dans le document lors de l'exécution suivante.

Plusieurs programmes sont ici disponibles.

## Le programme makeindex

Le programme le plus connu, présent sur toute distribution de LaTeX et disponible sur les principaux systèmes d'exploitation, est {ctanpkg}`makeindex`. Il peut être utilisé avec certains anciens [formats](/1_generalites/glossaire/qu_est_ce_qu_un_format) comme [Eplain](/1_generalites/glossaire/qu_est_ce_que_eplain) et [TeX](/1_generalites/glossaire/qu_est_ce_que_tex).

Il a l'avantage d'être facilement configurable, à l'aide de fichiers d'extension `.ist` mais n'est pas bien pensé pour traiter d'autres ordres de tri que l'ordre ASCII canonique.

Sa {texdoc}`documentation <makeindex>` est une bonne source d'informations sur la façon de créer votre propre index.

## Le programme idxtex

Le programme {ctanpkg}`idxtex`, écrit par Richard L. Aurbach, facilite la création d'index. Il est couplé à `GloTeX`, qui est l'équivalent permettant de créer des glossaires.

## Le programme texindex

Le programme {ctanpkg}`texindex` est un petit script shell Unix utilisant les programmes `sed` et `awk`.

## L'autre programme texindex

Le système [Texinfo](/1_generalites/glossaire/qu_est_ce_que_texinfo) fournit également un programme `texindex` dont la source est disponible dans la distribution `texinfo`. L'extension {ctanpkg}`ltxindex` donne accès aux commandes permettant d'utiliser `texindex`.

## Le programme xindy

Le programme {ctanpkg}`xindy`, disponible sur sa page [SourceForge](http://sourceforge.net/projects/xindy/), est né des difficultés à obtenir une version multilingue de `makeindex`. Il a été conçu pour succéder à `makeindex` par une équipe qui comprenait le mainteneur actuel de `makeindex`.

Écrit en Lisp, il est considéré comme un générateur d'index très général (*i.e.* pas seulement pour LaTeX). Il résout de nombreuses lacunes de `makeindex`, y compris les difficultés d'ordre de classement dans différentes langues (telle la gestion des accents), et il est facilement paramétrable.

Dans la pratique, `xindy` fonctionne avec une entrée LaTeX encodée Unicode (UTF-8). Une application distincte, `texindy`, traite le code source LaTeX "standard" et transmet son résultat « nettoyé » à `xindy`.

On trouvera une série d'articles introductifs et des conseils pratiques [sur le blog de Maïeul Rouquette](https://geekographie.maieul.net/169), ainsi que la documentation de référence en anglais [sur le site officiel](http://www.xindy.org/documentation.html).

______________________________________________________________________

*Source :* {faquk}`Generating an index in (La)TeX <FAQ-makeindex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX
```

