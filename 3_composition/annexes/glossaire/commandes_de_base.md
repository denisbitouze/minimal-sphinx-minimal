# Comment obtenir un glossaire ?

Un glossaire, c'est à peu près la même chose qu'un index. Et, comme pour l'index, LaTeX ne fournit pas par défaut les outils nécessaires pour obtenir un glossaire. Le passage par des extensions est obligatoire.

## Avec l'extension « nomencl »

L'extension {ctanpkg}`nomencl` permet de générer des glossaires de la même façon que l'on génère des index :

- `\makenomenclature` dans le préambule, pour indiquer qu'un glossaire doit être constitué ;
- `\nomenclature[préfixe]{nom}{description}` pour ajouter une entrée ;
- `\printnomenclature` à l'endroit où le glossaire doit être composé.

Tout comme les index, le glossaire doit être « compilé » pour que les entrées soient triées et mises en page correctement. Avec `makeindex` on exécutera :

```bash
makeindex -s nomencl.ist -o fichier.nls fichier.nlo
```

Le `fichier.nlo` a, théoriquement, été créé par LaTeX à la compilation. Bien entendu, `makeindex` n'est pas la seule possibilité, par exemple, le programme [xindy](/3_composition/annexes/index/generateurs_d_index) peut le remplacer.

Voici un exemple de document :

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{nomencl}
\pagestyle{empty}
\makenomenclature
\begin{document}

\section{La FAQ}
Ceci est la FAQ\nomenclature{FAQ}{(Foire Aux Questions) Réponses aux questions les plus
fréquemment posées.} du groupe FCTT\nomenclature{FCTT}{Groupe \texttt{fr.comp.text.tex}.}.
Elle contient en particulier une explication sur les glossaires.

\begin{thenomenclature}
\nomgroup{A}
  \item [{FAQ}]\begingroup (Foire Aux Questions) Réponses aux questions les plus fréquemment posées.\nomeqref {0}\nompageref{1}
  \item [{FCTT}]\begingroup Groupe \texttt{fr.comp.text.tex}.\nomeqref {0}\nompageref{1}

\end{thenomenclature}
\end{document}
```

## Avec l'extension « glossaries »

L'extension {ctanpkg}`glossaries` est probablement la plus complète sur le sujet. Elle permet d'obtenir des glossaires multiples, des listes d'acronymes, une gestion de différentes langues.

En voici un exemple simple d'utilisation pour un glossaire en français. Cet exemple évite de passer par `makeindex` ou [xindy](/3_composition/annexes/index/generateurs_d_index) mais il n'est recommandé pour des documents complexes, longs ou nécessitant des caractères hors de l'alphabet latin.

```latex
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{babel}
\usepackage{glossaries}
\newglossaryentry{ex}{name={exemple},description={illustration d'un concept}}
\pagestyle{empty}
\begin{document}
Voici un bel \gls{ex}.
\section*{Glossaire}
\textbf{exemple} illustration d'un concept. 1
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,glossaires,nomencl,glossaries
```

