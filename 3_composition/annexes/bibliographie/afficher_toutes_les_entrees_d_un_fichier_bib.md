# Comment afficher toutes les entrées d'un fichier « .bib » ?

Le travail de `BibTeX` ou `BibLaTeX` consiste à sélectionner uniquement les entrées de votre base de données bibliographique (fichier « `.bib` ») citées dans votre document. Mais parfois, par exemple pour lire et corriger votre base de données, vous voulez faire le contraire : obtenir un document avec tout le contenu de la base.

## Avec les commandes de base

Vous pouvez alors utiliser la commande `\nocite` avec une étoile pour argument :

```latex
% !TEX noedit
\nocite{*}
```

Ainsi, `BibTeX` inclut toutes les références de chaque fichier « `.bib` » listé dans votre déclaration `\bibliography{...}`, de sorte qu'après avoir exécuté les programmes `latex`, `bibtex` puis `latex`, vous aurez un document contenant l'ensemble de la liste.

Si le style bibliographique ne trie pas les entrées, celles-ci seront incluses dans leur ordre d'apparition dans le fichier « `.bib` ». De cette façon,

- si `\nocite{*}` est placée au début du document, les entrées de la bibliographie seront dans l'ordre du fichier « `.bib` » ;
- si cette commande est placée à la fin du document, les entrées qui n'étaient pas citées par ailleurs sont ajoutées après les entrées citées dans le document.

Notez que LaTeX *ne produit pas* d'avertissement « `Citation ... undefined` » ou « `There were undefined references` » quand ils arrivent à cause de `\nocite{*}`. Si vous exécutez LaTeX et `BibTeX` manuellement, cela ne vous posera aucun souci mais cela risque d'empêcher des scripts automatiques de déterminer si une nouvelle exécution de LaTeX est nécessaire.

## Cas de bases volumineuses

{octicon}`alert;1em;sd-text-warning` *Ce qui suit a un caractère historique du fait de l'évolution des capacités de mémoire des ordinateurs rendant ces solutions peu utiles.*

Avec une base de données bibliographique volumineuse, il peut arriver que `BibTeX`, utilisé avec `\nocite{*}`, sature la mémoire de la machine. Des extensions ont été développées pour contourner le problème :

- {ctanpkg}`biblist` a été écrit pour être utilisé sous LaTeX 2.09 mais semble encore fonctionner ;
- {ctanpkg}`listbib` est plus moderne.

Chose intéressante, ces deux extensions fournissent leurs propres fichiers de style bibliographique « `.bst` », ce qui permet de restituer chaque entrée de la bibliographie sous forme d'entrées plus détaillées intégrant en particulier la « [clé unique](/3_composition/annexes/bibliographie/construire_un_fichier_bibtex) ».

______________________________________________________________________

*Sources :*

- {faquk}`Listing all your BibTeX entries <FAQ-nocitestar>`
- <https://tex.stackexchange.com/questions/482570/generate-list-of-references-from-bib-file>

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,cite all,citer toutes les références,bibliographie complète,lister toutes les références biblio
```

