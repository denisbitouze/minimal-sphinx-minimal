# Quelles sont les alternatives à BibTeX ?

Initialement, {ctanpkg}`BibTeX` était un programme de création de bibliographies de documents techniques anglais. Par la suite, pour rendre `BibTeX` plus flexible, des mécanismes permettant une utilisation multilingue lui ont été apportés, tandis qu'une extension (`bibtex8`) a permis de traiter des codes de caractères 8 bits (facilitant d'autant plus l'utilisation multilingue). En parallèle, des fichiers de style `BibTeX` spécialisés ont été développés pour permettre de traiter des articles non techniques.

Ces extensions des fonctionnalités de `BibTeX` ne parviennent cependant à cacher deux défauts de ce logiciel :

- il utilise un langage de programmation dont les mécanismes ne sont pas familiers à la plupart des programmeurs actuels. Aussi, ce langage s'avère difficile à apprendre et, en l'absence d'occasions nombreuses pour apprendre à utiliser ce langage, peu le maîtrisent. Il évolue donc peu ou pas.
- `BibTeX` ne traite en totalité que le sujet de la manipulation de bases de données bibliographiques. Il ne gère que partiellement le sujet de la composition des résultats à afficher avec les fichiers de style, laissant une bonne part du travail à LaTeX et ses extensions dédiées.

Deux approches alternatives se distinguent ici par leur disponibilité et leur utilisation.

## L'extension « biblatex » et le programme Biber

L'extension {ctanpkg}`biblatex` propose de créer des bibliographies avec un contrôle de la mise en forme intégralement géré par le code LaTeX, plutôt que par un fichier de style `BibTeX`. Cela signifie que :

- chaque document peut avoir son propre style de présentation ;
- avec la mise à disposition des différentes données bibliographiques dans l'exécution de LaTeX, une gamme beaucoup plus large de résultats bibliographiques est possible. Ceci est particulièrement important pour appliquer des styles de citation propres aux sciences humaines ;
- enfin, cette extension intègrant de nombreuses idées des systèmes existants axés sur `BibTeX`, cela permet une cohérence (et une compabilité) des interfaces.

Si l'extraction des données des fichiers `.bib` en lien avec {ctanpkg}`biblatex` *peut* être réalisée en utilisant `BibTeX`, cette solution reste limitée. L'extension {ctanpkg}`biblatex` a été co-développée avec le programme `Biber`. Ce dernier, compatible avec l'Unicode, génère des fichiers répondant aux exigences de {ctanpkg}`biblatex`, peut extraire des données à partir de fichiers `.bib`, peut trier les entrées selon une large gamme de critères de langue, et plus encore. L'utilisation de `Biber` est donc fortement recommandée dans ce cadre.

## L'extension « amsrefs »

L'extension {ctanpkg}`amsrefs` utilise un fichier `.bib` transformé pour qu'il se présente sous forme de commandes LaTeX. Elle fournit d'ailleurs un style `BibTeX` qui effectue la transformation, de sorte qu'un document source LaTeX contenant une commande `\nocite{*}` permet à `BibTeX` de produire une base de données bibliographique utilisable par {ctanpkg}`amsrefs`.

```{eval-rst}
.. todo:: // Ajouter des exemples de code. //
```

______________________________________________________________________

*Source :* {faquk}`Replacing the BibTeX-LaTeX mechanism <FAQ-biblatex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,BibLaTeX,biber
```

