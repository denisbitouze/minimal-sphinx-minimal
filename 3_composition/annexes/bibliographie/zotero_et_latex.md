# Comment utiliser LaTeX avec Zotero ?

[Zotero](https://www.zotero.org/) est un logiciel de gestion de références gratuit, libre et open source. Ses principaux atouts techniques reposent sur l'intégration au navigateur web, la possibilité de synchronisation des données depuis plusieurs ordinateurs, la génération de citations (notes et bibliographies) dans un texte rédigé depuis les logiciels [LibreOffice](https://fr.libreoffice.org/), [Microsoft Word](https://www.microsoft.com/fr-fr/microsoft-365/word), [NeoOffice](https://www.neooffice.org/neojava/fr/index.php) ou [OpenOffice.org Writer](https://www.openoffice.org/fr/). [Zoho Writer](https://www.zoho.com/fr/writer/?src=zoho-home&ireft=ohome) ne dispose pas encore de cette fonctionnalité mais sa mise à disposition est bien envisagée.

Zotero ne fait pas partie de l'écosystème de LaTeX mais il s'y intègre facilement en se positionnant comme générateur de fichier `BibTeX`.

L'article « [Zotero et LaTeX](https://zotero.hypotheses.org/762) » de Raphaël Grolimund détaille les principes de LaTeX et de Zotero et explique la manière de les utiliser de concert.

______________________________________________________________________

*Sources :*

- [Zotero](https://fr.wikipedia.org/wiki/Zotero),
- [Rédiger sa bibliographie de thèse avec Zotero et LaTeX](https://ist.blogs.inrae.fr/technologies/2015/07/01/rediger-sa-bibliographie-de-these-avec-zotero-et-latex/), sur le site de l'INRAe.

```{eval-rst}
.. meta::
   :keywords: bibliographie,bibtex,Zotero,base de données,GUI
```

