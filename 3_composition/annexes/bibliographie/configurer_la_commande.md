# Comment configurer la commande « `\cite` » ?

## Avec l'extension « cite »

L'extension {ctanpkg}`cite` permet de configurer proprement la commande `\cite`. Il s'occupe automatiquement de {doc}`grouper les références multiples </3_composition/annexes/bibliographie/grouper_des_references_bibligraphiques_multiples>`.

## Avec l'extension « natbib »

L'extension {ctanpkg}`natbib` permet de disposer de plusieurs styles de citation : par exemple, avec les noms des auteurs et les années ou juste les auteurs, au moyen de plusieurs commandes ayant des arguments optionnels. Il est fourni avec des styles bibliographiques adaptés (`plainnat`, `unsrtnat`, `abbrnat`) qui reproduisent les styles standards de `BibTeX`.

Les styles produits avec {ctanpkg}`custom-bib` sont compatibles avec {ctanpkg}`natbib`.

## Avec l'extension « overcite »

De son côté, l'extension {ctanpkg}`overcite` modifie le style des citations : au lieu de « bidule \[5,6,7\] », elle restitue « bidule⁵⁻⁷ ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,style de références bibliographiques
```

