# Comment changer de langue dans une bibliographie ?

- \\label\{traduirebiblio} Il existe des versions francisées des styles bibliographiques, disponibles sur <https://www.ctan.org/%7Bbiblio/bibtex/contrib/bib-fr%7D>. Ces versions traduisent principalement le \\og and \\fg{} en \\og et \\fg{}, les noms des mois et quelques autres petites choses. Cependant, elles ne changent pas les règles typographiques appliquées au document, notamment les règles de césure.
- Les éléments d'une bibliographie étant des paragraphes, on peut modifier localement les règles typographiques à appliquer pour chaque entrée. Voir à ce sujet le paragraphe~\\vref\{babel}.
- Le package {ctanpkg}`mlbib` permet de gérer des bibliographies multilingues. Voir <https://www.ctan.org/%7Bmacros/latex/contrib/mlbib/%7D>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```

