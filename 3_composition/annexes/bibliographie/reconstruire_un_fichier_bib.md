# Comment reconstruire un fichier « .bib » ?

Peut-être avez-vous perdu le fichier « `.bib` » à partir duquel vous avez produit votre document, ou bien un correspondant vous a envoyé un document sans fichier « `.bib` ». Ou encore, vous avez fait l'erreur d'écrire un gros document sans utiliser `BibTeX`...

Le script Perl {ctanpkg}`tex2bib` permet de reconstituer des fichiers « `.bib` » à partir des environnements `thebibliography`, à condition que l'original (qu'il soit généré automatiquement ou manuellement) ne s'écarte pas trop des styles « classiques ».

La documentation du script se trouve dans le fichier `tex2bib.readme`. Sa syntaxe est très simple, en ligne de commande :

```bash
tex2bib -i mon_fichier_d_entree.tex -o biblio_restauree.bib
```

(l'option `-k` permet de regénérer les clefs de la bibliographie)

:::{important}
Il est conseillé de bien vérifier la sortie du script.

Bien qu'il ne détruise généralement pas d'informations, il a tendance à mal les étiqueter.
:::

______________________________________________________________________

*Source :* {faquk}`Reconstructing \`.bib\` files <FAQ-makebib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,retrouver un fichier ".bib",reconstruire un fichier BibTeX,PDF to BibTeX,LaTeX to BibTeX,bibliographie perdue
```

