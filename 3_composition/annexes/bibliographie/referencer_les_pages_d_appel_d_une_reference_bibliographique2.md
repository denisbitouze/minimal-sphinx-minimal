# Comment obtenir des liens de la bibliographie vers les références dans le texte ?

Un lien (ou au moins une référence de page) permettant de passer d'un ouvrage de la bibliographie à son évocation dans le texte principal d'un document est souvent utile dans les documents volumineux. Voici deux extensions qui permettent d'obtenir cette fonctionnalité, illustrée en tenant compte du fichier de références bibliographiques suivant :

```latex
@Book{complot,
author = {Machiavel, Nicolas},
title = {Le complot pour les nuls},
publisher = {{\'E}ditions Dubois},
year = {1522}
}

@Article{vu,
author = {Londres, Albert and Rochefort, Henri},
title = {Machiavel n'avait rien vu},
journal = {Le quotidien d'hier},
year = {1912}
}

@Book{titrer,
author = {Escher, Maurits Cornelis},
title = {Comment mal titrer un livre, y compris celui-ci},
publisher = {{\'E}ditions du labyrinthe},
year = {1956},
volume = {13}
}
```

## Avec l'extension « backref »

L'extension {ctanpkg}`backref` fait partie de l'ensemble {ctanpkg}`hyperref` et prend en charge les hyperliens de la bibliographie vers les zones de texte des commandes `\cite`.

Cette extension n'agrége pas les listes de pages (« 5, 6, 7 » apparaît comme tel, plutôt que « 5--7 ») mais elle ne répète pas la référence à une page contenant plusieurs fois une référence.

Voici un exemple (où est affichée la seule page de la bibliographie) :

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
\begin{thebibliography}{Mac22}

\bibitem[Esc56]{titrer}
Maurits~Cornelis {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe, 1956. pages 1

\bibitem[LR12]{vu}
Albert {\scshape Londres} et Henri {\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912. pages 1, 2

\bibitem[Mac22]{complot}
Nicolas {\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522. pages 1, 2, 3

\end{thebibliography}
\end{document}
```

## Avec l'extension « citeref »

L'extension {ctanpkg}`citeref` est plus ancienne et repose sur un code plutôt simple : elle produit uniquement une liste de pages où figure chaque référence après chacune des références de la bibliographie. Elle n'interagit pas bien avec d'autres extensions jouant sur les références (par exemple, {ctanpkg}`cite`), ce qui reflète son ancienneté. Elle dérive en effet d'une extension LaTeX 2.09.

Tout comme {ctanpkg}`backref`, elle n'agrége pas les listes de pages mais ne répète pas la référence à une page contenant plusieurs fois une référence.

Voici un exemple (où est affichée la seule page de la bibliographie) :

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
\begin{thebibliography}{Mac22}
\bibitem[Esc56]{titrer}
Maurits~Cornelis {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe, 1956. [1]

\bibitem[LR12]{vu}
Albert {\scshape Londres} et Henri {\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912. [1, 2]

\bibitem[Mac22]{complot}
Nicolas {\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522. [1, 2, 3]

\end{thebibliography}
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`References from the bibliography to the citation <FAQ-backref>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,bibliographies,liens
```

