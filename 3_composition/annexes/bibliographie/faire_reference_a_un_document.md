# Comment faire référence à une entrée bibliographique ?

Cela se fait avec la commande `\cite`. Cette commande prend un argument obligatoire, la « [clé unique](/3_composition/annexes/bibliographie/construire_un_fichier_bibtex) » de l'entrée citée dont la valeur figure dans votre fichier bibliographique « `.bib` » (la mécanique entière est détaillée à la question « [Comment générer une bibliographie ?](3_composition/annexes/bibliographie/construire_une_bibliographie) »). Elle accepte aussi un argument optionnel, qui permet de préciser, par exemple, à quelle partie du document il est fait référence.

Voici un exemple d'utilisation de `\cite` :

```latex
% !TEX noedit
\documentclass{article}

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[francais]{babel}

\begin{document}
Pour plus de renseignements sur la bibliographie,
consulter~\cite[chapitre 13]{Companion}.

\begin{thebibliography}{MMM99}

\bibitem[GMS94]{Companion}
M. Goossens, F. Mittelbach et A. Samarin,
\textit{The \LaTeX{} Companion},
Addison-Wesley, 1994.

\end{thebibliography}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```

