# Multiple bibliographies ?

If you're thinking of multiple bibliographies tied to some part of your document (such as the chapters within the document), please see [bibliographies per chapter](/3_composition/annexes/bibliographie/bibliographies_par_chapitre).

For more than one bibliography, there are three options.

The {ctanpkg}`multibbl` package offers a very simple interface : you use a command `\newbibliography` to define a bibliography "tag". The package redefines the other bibliography commands so that each time you use any one of them, you give it the tag for the bibliography where you want the citations to appear. The `\bibliography` command itself also takes a further extra argument that says what title to use for the resulting section or chapter (i.e., it patches [\\refname and \\bibname](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) in a {ctanpkg}`babel`-safe way). So one might write :

```latex
% !TEX noedit
\usepackage{multibbl}
\newbibliography{bk}
\bibliographystyle{bk}{alpha}
\newbibliography{art}
\bibliographystyle{art}{plain}
...
\cite[pp.~23--25]{bk}{milne:pooh-corner}
...
\cite{art}{einstein:1905}
...
\bibliography{bk}{book-bib}{References to books}
\bibliography{art}{art-bib}{References to articles}
```

(Note that the optional argument of `\cite` appears *before* the new tag argument, and that the `\bibliography` commands may list more than one `bib` file --- indeed all `\bibliography` commands may list the same set of files.)

The `\bibliography` data goes into files whose names are `⟨tag-name⟩.aux`, so you will need to run

```latex
% !TEX noedit
bibtex bk
bibtex art
```

after the first run of LaTeX, to get the citations in the correct place.

The {ctanpkg}`multibib` package allows you to define a series of "additional topics", each of which comes with its own series of bibliography commands. So one might write :

```latex
% !TEX noedit
\usepackage{multibib}
\newcites{bk,art}%
         {References from books,%
          References from articles}
\bibliographystylebk{alpha}
\bibliographystyleart{plain}
...
\citebk[pp.~23--25]{milne:pooh-corner}
...
\citeart{einstein:1905}
...
\bibliographybk{book-bib}
\bibliographyart{art-bib}
```

Again, as for {ctanpkg}`multibbl`, any `\bibliography...` command may scan any list of `bib` files.

BibTeX processing with {ctanpkg}`multibib` is much like that with {ctanpkg}`multibbl`; with the above example, one needs :

```latex
% !TEX noedit
bibtex bk
bibtex art
```

Note that, unlike {ctanpkg}`multibbl`, {ctanpkg}`multibib` allows a simple, unmodified bibliography (as well as the "topic" ones).

The {ctanpkg}`bibtopic` package allows you separately to cite several different bibliographies. At the appropriate place in your document, you put a sequence of `btSect` environments (each of which specifies a bibliography database to scan) to typeset the separate bibliographies. Thus, one might have a file `diss.tex` containing :

```latex
% !TEX noedit
\usepackage{bibtopic}
\bibliographystyle{alpha}
...
\cite[pp.~23--25]{milne:pooh-corner}
...
\cite{einstein:1905}
...
\begin{btSect}{book-bib}
\section{References from books}
\btPrintCited
\end{btSect}
\begin{btSect}[plain]{art-bib}
\section{References from articles}
\btPrintCited
\end{btSect}
```

Note the different way of specifying a bibliographystyle : if you want a different style for a particular bibliography, you may give it as an optional argument to the `btSect` environment.

Processing with BibTeX, in this case, uses `aux` files whose names are derived from the name of the base document. So in this example you need to say :

```latex
% !TEX noedit
bibtex diss1
bibtex diss2
```

There is also a command `\btPrintNotCited`, which gives the rest of the content of the database (if nothing has been cited from the database, this is equivalent to LaTeX standard `\nocite{*}`).

However, the *real* difference from {ctanpkg}`multibbl` and {ctanpkg}`multibib` is that selection of what appears in each bibliography section is determined in {ctanpkg}`bibtopic` by what's in the `bib` files.

An entirely different approach is taken by the {ctanpkg}`splitbib` package. You provide a `category` environment, in the preamble of your document, for each category you want a separate citation list for. In each environment, you list the `\cite` keys that you want listed in each category. The `\bibliography` command (or, more precisely, the `thebibliography` environment it uses) will sort the keys as requested. (Keys not mentioned in a `category` appear in a "misc" category created in the sorting process.) A code example appears in the package documentation (a PDF file in the CTAN directory, which you can browse to, from the link, below).

______________________________________________________________________

*Source :* {faquk}`Multiple bibliographies? <FAQ-multbib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,plusieurs bibliographies,bibliographies multiples
```

