# Comment créer mon style de bibliographie ?

Il est bien sûr *possible* d'écrire son propre style de bibliographie : les styles standard sont souvent distribués sous une forme comportant de nombreux commentaires, et le langage dans lequel ils sont écrits est {doc}`documenté dans la distribution BibTeX </1_generalites/documentation/documents/documentation_sur_bibtex>`. Cependant, il faut admettre que ce langage est assez obscur, et on ne recommanderait pas à quiconque n'est pas un programmeur chevronné de se lancer dans l'écriture de son propre style. Cela dit, apporter des changements mineurs à un style existant est à la portée de tout le monde, et peut être une façon de commencer à apprendre le langage en question.

Si votre style n'est pas trop exotique, vous pouvez probablement éviter de le programmer vous-même, en utilisant les fonctionnalités de l'extension {ctanpkg}`custom-bib`. Elle contient un fichier `makebst.tex`, qui vous propose un menu pour produire un fichier d'instructions, que vous pouvez ensuite utiliser pour générer votre propre fichier `bst`. Cette technique ne permet pas de développer des styles BibTeX complètement nouveaux, mais les « styles maîtres » de {ctanpkg}`custom-bib` offrent déjà beaucoup plus que ce qui vient par défaut avec BibTeX.

Une alternative, de plus en plus souvent recommandée, consiste à utiliser [biblatex](/3_composition/annexes/bibliographie/remplacer_bibtex). {ctanpkg}`BibLaTeX <biblatex>` offre de nombreux possibilités pour ajuster la mise en forme donnée par les styles bibliographiques « de base », ainsi qu'une collection de styles apportés par une communauté des contributeurs. Cette collection s'agrandit progressivement et est maintenant comparable à ce qui est disponible pour BibTeX. Il n'y a pas de `custom-biblatex`; si vous êtes débutant, vous devrez trouver une autre façon de créer votre premier style pour {ctanpkg}`BibLaTeX <biblatex>`. Mais le langage est beaucoup plus simple à appréhender que celui de BibTeX.

______________________________________________________________________

*Source :* {faquk}`Creating a bibliography style <FAQ-custbib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,références bibliographiques,mise en page de la bibliographie,format de la bibliographie
```

