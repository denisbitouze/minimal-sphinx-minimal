# Comment faire apparaître les entrées bibliographiques dans le texte ?

Il s'agit d'une exigence courante pour les journaux de sciences humaines. Parfois, l'entrée doit figurer dans le texte courant du document, tandis que d'autres styles demandent que l'entrée apparaisse dans une note de bas de page.

## Comment avoir les entrées bibliographiques dans le texte courant ?

C'est notamment utile pendant la rédaction d'un article, au moment de vérifier que les bonnes références sont insérées au bon endroit.

Voici les différentes possibilités :

- L'extension {ctanpkg}`bibentry`, qui impose quelques restrictions sur le format de l'entrée que votre fichier `.bst` génère, mais qui est par ailleurs peu exigeant pour le style de bibliographie.
- L'extension {ctanpkg}`inlinebib`, qui requiert que vous utilisiez son fichier `inlinebib.bst`. {ctanpkg}`Inlinebib <inlinebib>` a en fait été conçu pour les citations en note de bas de page : son utilisation attendue est que vous placiez une citation directement comme argument d'une commande `\footnote`.
- L'extension {ctanpkg}`jurabib`, qui a été conçue à l'origine pour les documents juridiques allemands, et qui offre des possibilités très complètes pour la manipulation des citations. Cette extension est fournie avec quatre styles de bibliographie : `jurabib.bst`, `jhuman.bst` et deux styles suivant les recommandations du [Chicago Manual of Style](https://fr.wikipedia.org/wiki/The_Chicago_Manual_of_Style).

Plus précisément, l'extension {ctanpkg}`bibentry` propose les commandes suivantes :

- `\bibentry`, prend en argument la clef de la référence à citer (comme la commande `\cite`),
- `\nobibliography`, qui prend en argument le fichier `.bib` dans lequel les références sont définies (comme la commande `\bibliography`) et
- `\bibliographystyle`, qui prend en argument le fichier de style bibliographique à utiliser.

Si vous utilisez `\bibentry` et `\nobibliography` à la place de `\cite` et `\bibliography` (respectivement), chaque référence bibliographique apparaîtra dans le corps du texte, de façon complète, à l'endroit de l'appel. Inversement, il n'y aura pas de liste de références en fin de document.

:::{important}
La commande `\nobibliography` doit être utilisée **avant** le premier appel à `\bibentry`.
:::

## Comment avoir les entrées bibliographiques en notes de bas de page ?

Voici les principales possibilités disponibles :

- L'extension {ctanpkg}`footbib`, et
- Les extensions {ctanpkg}`jurabib` et {ctanpkg}`inlinebib`, de nouveau.

:::{tip}
{ctanpkg}`Jurabib <jurabib>` utilise les notes de bas de page standards de LaTeX, alors que {ctanpkg}`footbib` crée sa propre séquence de notes de bas de page. Par conséquent, dans un document qui a d'autres notes de bas de page, il semble préférable d'utiliser {ctanpkg}`jurabib` (ou bien sûr {ctanpkg}`inlinebib`), pour éviter la confusion entre les notes de bas de page à proprement parler et les citations de bas de page.
:::

L'extension {ctanpkg}`usebib` offre une « boîte à outils », qui permet à l'utilisateur d'insérer dans le texte exactement ce dont il a besoin, au lieu d'une citation complète. La commande à utiliser, qui met la citation en forme, est `\usebibdata{⟨clef⟩}{⟨valeur⟩}` ; elle compose l'élément `⟨valeur⟩` à partir de la `⟨clef⟩` de la bibliographie ; l'utilisateur met ensuite en forme l'entrée comme il le souhaite --- on pourrait imaginer construire complètement sa propre bibliographie à partir de cette commande, mais cela serait évidemment fastidieux.

______________________________________________________________________

*Sources :*

- {faquk}`Putting bibliography entries in text <FAQ-bibinline>`,
- [Using bibentry to insert reference in text and omit the list of references at the end](https://tex.stackexchange.com/questions/142985/using-bibentry-to-insert-reference-in-text-and-omit-the-list-of-references-at-th),
- [A LaTeX package to place bibliography entries in text](https://gking.harvard.edu/files/bibentry2.pdf).

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,références bibliographiques,citations,références bibliographiques en notes de pied de page,vérifier les références bibliographiques
```

