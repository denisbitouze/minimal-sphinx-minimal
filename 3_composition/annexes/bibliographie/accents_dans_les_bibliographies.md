# Comment obtenir des accents dans une bibliographie ?

`BibTeX` a non seulement tendance à jouer par défaut avec la casse des lettres, mais il transforme également des commandes d'accent en espacement : le code « `ma\~nana` » apparaît comme « ma nana » (!).

La solution est similaire à celle du problème vu à la question « [Comment conserver les majuscules dans les titres ?](/3_composition/annexes/bibliographie/conserver_les_majuscules_dans_les_titres) » : mettez la séquence gênante entre accolades, comme « `{\~n}` », dans notre exemple.

______________________________________________________________________

*Source :* {faquk}`Accents in bibliographies <FAQ-bibaccent>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```

