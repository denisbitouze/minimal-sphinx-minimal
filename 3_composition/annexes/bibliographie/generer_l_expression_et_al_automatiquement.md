# Comment générer l'expression « et al.» automatiquement ?

Lorsqu'un document ayant une longue liste d'auteurs est mis en bibliographie, l'usage consiste à ne pas mettre la liste complète de ces auteurs, mais seulement les quatre premiers, puis *et al.* ([« et les autres »](https://fr.wikipedia.org/wiki/Et_al.) en latin). Dans certains domaines, en français, on n'utilise pas l'expression latine, mais « et collab. » ou « et coll. » (pour *collaborateurs*).

Si le style bibliographique que vous utilisez ne vous propose pas cette mécanique, vous pouvez alors copier votre fichier de style, le renommer (c'est ce fichier renommé que vous appelerez dans votre code LaTeX et l'éditer comme suit. Il faut modifier la fonction `format.names` (qui est généralement toujours semblable à celle ci-dessous) et ajouter un test sur le nombre d'auteurs :

```bibtex
FUNCTION {format.names}
{ 's :=
  #1 'nameptr :=
  s num.names$ 'numnames :=
  numnames 'namesleft :=
  { namesleft #0 > }
  { s nameptr "{ff~}{vv~}{ll}{, jj}" format.name$
    't :=
    nameptr #1 >
    { namesleft #1 >
      { ", " * t * }
      { numnames #2 >
          { "," * }
          'skip$
        if$
        t "others" =
          { " et~al." * }
          { " and " * t * }
        if$
      }
      if$
    }
    't
  if$
  nameptr #1 + 'nameptr :=
  namesleft #1 - 'namesleft :=
% ------------------------------
% (debut des ajouts)
% si à ce stade on se retrouve avec des noms à placer
% et que le pointeur nameptr est égal à 4 alors
% il faut :
% 1. ajouter un "et~al."
% 2. ne pas prendre en compte les auteurs suivants :
%    on met donc namesleft à 0.
%
    nameptr #4 =  namesleft #0 >  and
    {  " \emph{et~al.}" *
       #0 'namesleft := }
    'skip$
    if$
%
% NB : pour mettre 3 noms au lieu de 4, on mettra #3
% au lieu de #4 ci-dessus.
% (fin des ajouts)
% ------------------------------
}
  while$
}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,liste d'auteurs,tronquer la lste des auteurs,et al.,collaborateurs,bibliographie trop longue
```

