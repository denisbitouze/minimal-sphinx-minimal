# Comment générer une bibliographie ?

Il existe ici deux possibilités pour placer une bibliographie dans un document :

- soit en la saisissant directement dans le document ;
- soit en créant un fichier contenant les références bibliographiques (un fichier d'extension « `.bib` ») qui sera traité par une chaîne de compilation dédiée utilisant `BibTeX`.

Dans les deux cas, la commande `\cite` permet de citer une référence de la bibliographie dans le corps document. Toutefois, dans le cas particulier de la chaîne avec `BibTeX`, cette commande indique qu'il faut afficher la référence souhaitée dans la bibliographie, si elle existe. Toujours dans le cas de la chaîne avec `BibTeX`, pour inclure une référence dans la bibliographie sans y faire référence explicitement dans le corps du document, il faut utiliser la commande `\nocite`.

## Avec les commandes de base

La méthode « directe » consiste à utiliser l'environnement `thebibliography`. C'est un environnement semblable à `itemize`, chaque entrée étant précédée d'un `\bibitem`. En voici un exemple :

```latex
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}

\begin{document}

Les livres~\cite{Lamport} et~\cite{Companion} sont deux bons bouquins sur \LaTeX.

\begin{thebibliography}{MMM99}

\bibitem[Lam99]{Lamport}
L. Lamport,
\textit{\LaTeX : A Document Preparation System},
Addison-Wesley, 1994.

\bibitem[GMS94]{Companion}
M. Goossens, F. Mittelbach et A. Samarin,
\textit{The \LaTeX{} Companion},
Addison-Wesley, 1994.

\end{thebibliography}
\end{document}
```

La commande `\bibitem` a, en général, un argument obligatoire et un argument optionnel. L'argument obligatoire est la *clef interne*, c'est-à-dire le « nom » à utiliser dans le document pour faire référence à cette entrée. L'argument optionnel est la clé que LaTeX utilisera dans le document. Ce qui suit le `\bibitem` est le contenu de la référence bibliographique. C'est du code LaTeX normal.

L'environnement `thebibliography` a un argument obligatoire, qui indique la taille du retrait à prévoir dans la liste. L'argument devra donc être, pour des raisons esthétiques, la plus longue clé apparaissant dans la bibliographie.

## Avec le programme BibTeX

La méthode précédente a l'inconvénient de ne pas être automatique : il faut recréer la liste des références pour chaque document. {ctanpkg}`BibTeX <bibtex>` permet de créer une fois pour toutes une liste de références bibliographiques et de lister, automatiquement et de manière configurable, les références utilisées dans le document.

### Le fichier des références bibliographiques

L'automatisation passe par la constitution d'un fichier (ou de plusieurs fichiers) d'extension « `.bib` » contenant les références bibliographiques mises sous une forme cadrée, chaque référence étant composée d'une liste de données sous la forme « `champ = "valeur"` » séparées par des virgules. Pour notre exemple de début de page, ce fichier aurait la forme suivante :

```bibtex
@book{Lamport,
  title     = "\LaTeX : A Document Preparation
              System",
  author    = "Lamport, Leslie",
  publisher = "Addison-Wesley",
  year      = 1994
}

@book{Companion,
  title     = "\LaTeX{} Companion",
  author    = "Goossens, Michel and Mittelbach,
              Frank and Samarin, Alexander",
  publisher = "Addison-Wesley",
  year      = 1994
}
```

La question « [Comment construire un fichier de références bibliographiques (.bib) ?](/3_composition/annexes/bibliographie/construire_un_fichier_bibtex) » développe ce sujet.

### Le fichier de style bibliographique

Afin de pouvoir simplement passer d'un style bibliographique à un autre, ces styles sont contenus dans un fichier d'extension « `.bst` ». Il existe de nombreux fichiers de ce type, les plus courants étant `abbrv`, `alpha`, `apalike`, `plain` et `unsrt` (qui existent aussi en version francisée : `alpha-fr`, `plain-fr`, etc.).

Les principales caractéristiques de ces styles sont présentées à la question « [Comment choisir un style de bibliographie ?](/3_composition/annexes/bibliographie/choisir_un_style_de_bibliographie) ».

### Les commandes à placer dans le document principal

Pour inclure une bibliographie dans un document, il faut utiliser la commande `\bibliography` qui indique également à LaTeX qu'il doit placer la bibliographie à l'endroit du texte où se trouve cette commande. Mais, comme vu ci-dessus, on doit donc définir le style voulu et le(s) fichier(s) contenant les références bibliographiques. Cela se fait respectivement avec les commandes `\bibliographystyle` et `\bibliography` (encore elle). Par exemple :

```latex
% !TEX noedit
\bibliographystyle{alpha}
\bibliography{mabiblio,bibliofac,commun}
```

Notez ici l'absence d'espaces après les virgules dans la commande `\bibliography` (qui permet dans le cas présent d'aller chercher les données des fichiers `mabiblio.bib`,`bibliofac.bib` et `commun.bib`).

### La chaîne de compilation et d'utilisation de BibTeX

À la compilation, plusieurs passes sont nécessaires. Les voici toutes détaillés, sachant que certaines pourront être à répéter, par exemple si la bibliographie elle-même contient des références (dans ce cas, les deux premières étapes ci-dessous seront répétées autant de fois que nécessaire).

#### Première compilation

Lors de la première compilation de votre document par LaTeX, les différentes commandes ajoutent de l'information au fichier auxiliaire (« `.aux` ») à destination de `BibTeX` :

- la commande `\bibliographystyle` place une note indiquant le style de la bibliographie ;
- chaque commande `\cite` place une note indiquant quelle référence bibliographique est appelée ;
- la commande `\bibliography` écrit une note indiquant quel est le fichier « `.bib` » qui doit être utilisé.

Notez qu'à ce stade, LaTeX ne « résout » aucune des références : à chaque commande `\cite`, il vous avertit de la présence d'une référence non définie, et lorsque le document sera terminé, il indique la présence générale de références indéfinies.

#### Utilisation de BibTeX

Il faut alors appeler `BibTeX` et le faire traiter votre fichier auxiliaire :

```bash
bibtex monfichier
```

Vous n'avez pas à préciser ici l'extension du fichier (« `.aux` »). Si vous indiquez `bibtex monfichier.aux`, `BibTeX` tentera aveuglément de traiter `monfichier.aux.aux`.

`BibTeX` scanne alors le fichier auxiliaire :

- il cherche le style de bibliographie qu'il doit utiliser et « compile » ce style ;
- il note les références souhaitées ;
- il cherche les fichiers bibliographiques dont il a besoin et les parcourt en faisant correspondre les références aux entrées de la bibliographie ;
- il trie les entrées qui ont été citées (si le style de bibliographie spécifie qu'elles doivent être triées) ;
- il constitue un fichier « `.bbl` » contenant le résultat de ces travaux, à savoir un environnement `thebibliography` et la liste des `\bibitem` demandés.

#### Deuxième compilation

Cette deuxième compilation de votre document par LaTeX va permettre d'inclure le contenu du fichier « `.bbl` » à l'endroit où se trouve la commande `\bibliography`.

Une nouvelle fois, LaTeX vous avertit que chaque référence bibliographique est indéfinie. Cependant, lorsqu'il rencontre chaque commande `\bibitem` dans le fichier, il note la définition de ces références.

#### Troisième compilation

Cette dernière compilation permet à LaTeX de pouvoir faire proprement toutes les références croisées à la bibliographie.

Si, lors de l'édition, vous modifiez l'une des références ou en ajoutez de nouvelles, vous devez suivre tout le processus de compilation décrit ci-dessus pour que le document se stabilise à nouveau. Ces quatre exécutions obligatoires de LaTeX rendent le traitement d'un document avec une bibliographie plus fastidieux que dans le cas où vous traitez des [références croisées](/3_composition/texte/renvois/start) ou dans celui où vous insérez une [table des matières](3_composition/annexes/tables/generer_une_table_des_matieres). Cependant, nombre de programmes d'édition de document LaTeX propose des raccourcis pour exécuter la totalité de cette chaîne de compilation.

______________________________________________________________________

*Source :* {faquk}`Normal use of BibTeX from LaTeX <FAQ-usebibtex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,références bibliographique,compiler une bibliographie,fichier bib
```

