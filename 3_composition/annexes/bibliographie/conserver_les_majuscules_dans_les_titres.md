# Comment conserver les majuscules dans les titres ?

Certains styles bibliographiques, notamment les {doc}`styles standards de BibTeX </3_composition/annexes/bibliographie/choisir_un_style_de_bibliographie>`, ont la mauvaise habitude de passer les titres des ouvrages cités en minuscules (sauf, bien sûr, la première lettre).

Si, avec un tel style, on cite :

```latex
% !TEX noedit
  title = "La FAQ du groupe fr.comp.text.tex"
```

alors on obtient en bibliographie le résultat : « La faq du groupe fr.comp.text.tex ».

Ça n'est pas déraisonnable au vu des usages courants en anglais (les règles proviennent du *Chicago Manual of Style*), mais c'est gênant pour les acronymes, les formules chimiques, les noms propres, etc.

Pour éviter ce problème, on utilise le fait que `BibTeX` ne touche pas les lettres qui sont dans des accolades. On codera donc, par exemple, ainsi :

```bibtex
title = {Programme de {TF1} pour ce soir},
title = {Analyse de l'{ADN} du {Brontosaure}},
```

Parfois, `BibTeX` change la casse d'une seule lettre de manière inappropriée. Peu importe : la technique peut être appliquée à des lettres uniques, comme ici :

```bibtex
title = {Te{X}niques et astuces},
```

Mais les accolades peuvent empêcher le crénage entre les lettres, c'est pourquoi il est généralement préférable d'entourer les mots entiers et pas seulement les lettres individuelles d'accolades pour les protéger.

:::{note}
Dans le cas des formules chimiques, une bonne façon de faire est d'utiliser le package {ctanpkg}`mhchem`, qui fournit la commande `\ce` (**c***hemical* **e***quation*) pour mettre en forme les formules. Elle s'occupera également de protéger son contenu contre les modifications par `BibTeX` :

```bibtex
title = {Noyade dans un verre d'\ce{H2O}},
```

(le package {ctanpkg}`chemformula` permet la même chose avec sa commande `\ch`).
:::

Si votre maquette de document demande un style de capitalisation différent, vous devriez choisir un style de bibliographie qui n'applique pas les règles par défaut de `BibTeX`.

:::{warning}
Ce n'est *pas* une bonne idée de mettre un titre entier entre accolades, comme dans

```bibtex
title = {{TeXniques and tips}},
```

même si cela garantit que la capitalisation n'est pas modifiée. Votre base de données `BibTeX` doit rester une base de données à usage général, et non se plier aux exigences d'un document ou d'un style bibliographique particulier, ou à la façon dont vous pensez aujourd'hui. Vous pourrez avoir plus tard à utiliser un style `BibTeX` différent, avec des règles de capitalisation différentes.
:::

Vous trouverez plus d'informations sur le sujet dans {doc}`la documentation sur BibTeX </1_generalites/documentation/documents/documentation_sur_bibtex>`.

______________________________________________________________________

*Sources :*

- {faquk}`Capitalisation in BibTeX <FAQ-capbibtex>`,
- <https://tex.stackexchange.com/questions/10772/bibtex-loses-capitals-when-creating-bbl-file>,
- <https://tex.stackexchange.com/questions/7288/preserving-capitalization-in-bibtex-titles/7289>,
- <https://tex.stackexchange.com/questions/301785/how-to-write-chemical-formulas-in-the-title-field-of-bib-file>,
- [Distinction entre capitale et majuscule](https://fr.wikipedia.org/wiki/Capitale_et_majuscule).

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,majuscules,capitales,titres
```

