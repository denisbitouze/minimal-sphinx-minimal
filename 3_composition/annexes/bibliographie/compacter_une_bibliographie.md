# Comment compacter une bibliographie ?

Par défaut, les bibliographies de LaTeX sont plutôt aérées, avec des espaces généreux entre les entrées. Cela les rend faciles à lire, mais peut donner l'impression de gaspiller l'espace de la page. Plusieurs solutions existent pour réduire cet espacement.

## Avec les méthodes liées aux listes

Les bibliographies sont, en interne, implémentées comme des listes, donc {doc}`tout ce qui peut compacter les listes </3_composition/texte/listes/ajuster_l_espacement_dans_les_listes>` s'applique également aux bibliographies.

## Avec l'extension « natbib »

Si l'extension {ctanpkg}`natbib` fonctionne dans votre cas, la solution est relativement simple. Il faut ajouter au préambule de votre document le code suivant :

```latex
% !TEX noedit
\usepackage{natbib}
\setlength{\bibsep}{0.0pt}
```

:::{important}
Si vous utilisez un style de bibliographie spécial, il se peut que {ctanpkg}`natbib` ne donne pas le résultat voulu.
:::

## Avec l'extension « compactbib »

L'extension {ctanpkg}`compactbib` a un effet similaire. Son but premier est de produire deux bibliographies, et il semble empêcher l'utilisation de `BibTeX` (bien que la {texdoc}`documentation <compactbib>` de l'extension, contenue dans le fichier « `.sty` » lui-même, ne soit pas particulièrement claire).

## Avec des commandes de base

En temps normal, l'extension {ctanpkg}`mdwlist` sait faire le travail mais elle ne fonctionne pas ici parce qu'elle crée une liste portant un nom différent, alors que le nom `\thebibliography` est intégré dans LaTeX et `BibTeX`. Par conséquent, il faut plutôt {doc}`corriger la macro sous-jacente </2_programmation/macros/patcher_une_commande_existante>` :

```latex
% !TEX noedit
\let\oldbibliography\thebibliography
\renewcommand{\thebibliography}[1]{%
  \oldbibliography{#1}%
  \setlength{\itemsep}{0pt}%
}
```

## Avec l'extension « savetrees », indirectement

L'extension {ctanpkg}`savetrees` effectue la correction voulue, parmi une pléthore d'autres visant à économiser de l'espace. Vous pouvez donc, en théorie, désactiver toutes ses autres fonctionnalités et lui demander de vous fournir *uniquement* une bibliographie compressée.

______________________________________________________________________

*Source :* {faquk}`Reducing spacing in the bibliography <FAQ-compactbib>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,mise en forme de la bibliographie,compacter la bibliographie,réduire les blancs
```

