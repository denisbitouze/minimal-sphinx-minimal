# Comment faire des références croisées avec BibTeX ?

Cette opération est possible avec le champ `crossref` de `BibTeX`. Voici un
exemple : test

```bibtex
% !TEX noedit
@inbook{Companion-biblio,
  crossref  = "Companion",
  chapter   = 13,
  pages     = "371--420"
}
@inbook{Companion-math,
  crossref  = "Companion",
  chapter   = 8,
  pages     = "215--257"
}
@book{Companion,
  title     = "\LaTeX{} Companion",
  author    = "Goossens, Michel and Mittelbach,
               Frank and Samarin, Alexander",
  publisher = "Addison-Wesley",
  year      = 1994
}
```

De cette façon, tous les champs pertinents pour une entrée de type `inbook` et non définis dans l'entrée `Companion-biblio` sont « hérités » de l'entrée `Companion`.

Trois remarques cependant :

- l'ordre est important dans la mesure où l'entrée contenant le champ `crossref` doit toujours être placéee *avant* l'entrée référencée ;
- s'il est fait référence à `Companion-biblio` dans le document, mais pas à `Companion`, alors l'entrée `Companion` ne sera pas listée dans la bibliographie. Par contre, s'il est également fait référence à `Companion-math`, alors l'entrée `Companion` sera automatiquement listée. Ce comportement peut être modifié avec l'option `-min-crossrefs` de `BibTeX` ;
- les références croisées ne peuvent être emboitées.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,références croisées
```

