# Comment convertir une bibliographie en HTML ?

## Avec un export direct à la compilation, par « noTeX »

Le style de bibliographie {ctanpkg}`noTeX <notex-bst>` offre ici une solution astucieuse. Il produit un fichier « `.bbl` » qui est en fait une série de paragraphes HTML de classe `noTeX`, et qui peut donc être inclus directement dans un fichier HTML :

```html
<P CLASS=noTeX>
...
</P>
```

Il est possible de personnaliser votre bibliographie afin que son contenu traité par {ctanpkg}`noTeX <notex-bst>` soit différent de celui présenté lorsqu'elle est traitée de manière ordinaire par LaTeX.

## Avec des scripts de conversion

### Le script bib2xhtml

Une solution complète est offerte par {ctanpkg}`bib2xhtml` (version mise à jour de `bib2html`). Son utilisation fait intervenir en fait une des versions modifiées des styles `BibTeX` courants qu'il fournit. La sortie ainsi produite doit être ensuite traitée à l'aide d'un script Perl.

### Le script bibhtml

> {ctanpkg}`bibhtml` prend la même approche que {ctanpkg}`bib2xhtml`.

### Le script bbl2html

Un convertisseur plus conventionnel est le script `awk` {ctanpkg}`bbl2html`, qui traduit le fichier « `.bbl` » que vous avez généré en HTML.

## Avec Pandoc

Enfin, il est également possible d'utiliser [Pandoc](https://pandoc.org/), mais il faudra le faire tourner sur le fichier « `.tex` » incluant votre bibliographie, et non directement le fichier « `.bib` » :

```bash
pandoc test.tex -o output.html --bibliography ma_biblio.bib
```

Le fichier LaTeX peut être très simple, si vous voulez juste convertir la bibliographie :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
 \nocite{*}
 \bibliographystyle{abbrv} % ou tout autre style de bibligraphie
 \bibliography{ma_biblio.bib}
\end{document}
```

## Avec des convertisseurs en ligne

Il existe des convertisseurs en ligne, basés sur [la bibliothèque bibTeX-js](https://github.com/pcooksey/bibtex-js). En voici un fonctionnel : <http://people.irisa.fr/Francois.Schwarzentruber/bibtextohtml/>.

______________________________________________________________________

*Sources :*

- {faquk}`Making HTML of your bibliography <FAQ-htmlbib>`,
- [Bibtex to HTML/Markdown/etc., using Pandoc](https://tex.stackexchange.com/questions/171793/bibtex-to-html-markdown-etc-using-pandoc),
- [How to display references on html using bibtex format](http://gewhere.github.io/bibtex-js).

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,HTML,script,export
```

