# Comment changer le format des références numériques dans la bibliographie ?

Par défaut, LaTeX fait en sorte que les entrées de la bibliographie ressemblent à ceci :

```latex
[1] Pierre, Paul et al.  Journal. 2004.

[2] Pierre, Paul et al. Journal. 2003.
```

Mais vous pourriez préférer quelque chose comme :

```latex
1. Pierre, Paul et al.  Journal. 2004.

2. Pierre, Paul et al. Journal. 2003.
```

## Avec des commandes de base

Si vous ne souhaitez pas utiliser d'extension particulière, les commandes LaTeX suivantes, dans le préambule de votre document, vous permettront d'obtenir cette modification :

```latex
% !TEX noedit
\makeatletter
\renewcommand*{\@biblabel}[1]{\hfill#1.}
\makeatother
```

## Avec des extensions

Ceci peut, sinon, être obtenu avec de nombreuses extensions « génériques » pour `BibTeX`. Par exemple, avec l'extension {ctanpkg}`natbib`, il suffit d'indiquer :

```latex
% !TEX noedit
\renewcommand{\bibnumfmt}[1]{#1.}
```

______________________________________________________________________

*Source :* {faquk}`Format of numbers in the bibliography <FAQ-formbiblabel>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,références numériques,numéros des citation,numéros avec BibTeX
```

