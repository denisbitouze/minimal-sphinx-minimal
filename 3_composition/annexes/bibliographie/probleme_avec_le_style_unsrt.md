# Comment gérer le style bibliographique « unsrt » avec des tables des matières ?

En utilisant le style bibliographique {ctanpkg}`unsrt <bibtex>`, vous vous attendez à ce que votre bibliographie ne soit *pas* triée : les entrées de la bibliographie apparaissent alors dans l'ordre dans lequel elles sont apparues pour la première fois dans votre document.

Cependant, si vous avez la malchance d'avoir besoin d'une citation bibliographique dans un [titre de sectionnement](/3_composition/texte/titres/start) et que vous avez également une table des matières, les citations qui apparaissent maintenant dans la table des matières perturberont l'ordre « naturel » produit par le style {ctanpkg}`unsrt <bibtex>`. La situation sera similaire avec des citations dans des légendes de figure (ou de table) et que vous avez une liste de figures (ou de tables).

## Avec des suppressions de fichier

Il existe une méthode manuelle assez simple pour résoudre le problème, dès lors que le document est stable :

1. supprimez le fichier auxiliaire (d'extension `aux`) et tous les fichiers « `.toc` », « `.lof` » ou « `.lot` » ;
2. exécutez LaTeX ;
3. exécutez `BibTeX` ;
4. exécutez LaTeX aussi souvent que nécessaire pour que le document reste stable.

## Avec l'extension « notoccite »

Ce qui paraît ici simple peut devenir fastidieux lorsque vous aurez des erreurs dans votre version de départ contient des erreurs.

Heureusement, l'extension {ctanpkg}`notoccite` est là pour éviter que les citations présentes dans la table des figures (et consorts) n'interviennent dans l'ordre de numérotation. Essayez de compiler les fichiers sources `notoccitebib.bib` et `notoccitetex.tex` en guise d'illustration.

```bibtex
@Misc{DOC,
          author =       {Toto},
           title =       {Blabla}
}
@Misc{DOC2,
          author =       {Titi},
           title =       {Blabla, 2nd édition}
}
```

```latex
\documentclass{book}

 \usepackage{notoccite}
 \bibliographystyle{unsrt}
 \usepackage{multido}

\begin{document}
 \tableofcontents

 Cf.~\cite{DOC}.

\chapter{Voir \cite{DOC2}}
 \multido{}{600}{blabla }

\bibliography{notoccitebib}
\end{document}
```

:::{important}
Dans le fichier d'exemple ci-dessus, on a utilisé des références en majuscules qui restent inchangées dans les en-têtes. Mais, si les références sont en minuscules, il faudra utiliser l'extension {ctanpkg}`textcase`.
:::

______________________________________________________________________

*Source :* {faquk}`Table of contents rearranges unsrt ordering <FAQ-bibtocorder>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,style,unsrt,titre de sectionnement
```

