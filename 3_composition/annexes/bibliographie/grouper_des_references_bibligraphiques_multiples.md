# Comment grouper des références bibliographiques multiples ?

Si vous passez à LaTeX quelque chose comme `\cite{fred,joe,harry,min}`, vous allez obtenir les références dans l'ordre de leur entrée : « \[2,6,4,3\] », ce qui n'est pas très beau. Bien sûr, on pourrait s'amuser à trier les références dans la commande `\cite`, mais c'est du travail manuel, qui devra éventuellement être refait si le fichier « `.bib` » change, et le resultat restera médiocre : « \[2,3,4,6\] ».

## Avec l'extension « cite »

L'extension {ctanpkg}`cite` trie les références par ordre numérique et détecte les séquences, ce qui donnera : « \[2–4,6\] », plus élégant.

## Avec l'extension « natbib »

L'extension {ctanpkg}`natbib`, avec les options `numbers` et `sort&compress`, fait la même chose quand il est utilisé avec ses propres styles bibliographiques (`plainnat.bst` et `unsrtnat.bst`).

## Avec l'extension « BibLaTeX »

L'extension {ctanpkg}`BibLaTeX <biblatex>` a un style intégré, `numeric-comp`, pour obtenir ce résultat.

______________________________________________________________________

*Source :* {faquk}`Sorting and compressing citations <FAQ-citesort>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,références bibliographiques,bibliographies,références numériques,trier et compacter les références bibliographiques,ordonner les références
```

