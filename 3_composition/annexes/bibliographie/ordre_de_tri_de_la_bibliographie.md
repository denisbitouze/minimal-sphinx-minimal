# Comment spécifier un tri dans une bibliographie ?

## Avec le fichier de style bibliographique

Dans de nombreux fichiers de style bibliographique, le tri est spécifié par défaut.

Il existe aussi de nombreux fichiers de style bibliographique où aucun tri n'est fait, tel `unsrt.bst`. Dans ce cas, les entrées sont affichées dans l'ordre où elles sont citées dans le document. Pour forcer un certain ordre, une solution peut consister à utiliser un tel style et, au début du document, citer les entrées avec la commande `\nocite` dans l'ordre voulu.

## Avec le programme « bibtool »

Dans le même ordre d'idée, utiliser `\nocite{*}` permet de citer dans l'ordre où les entrées sont placées dans le fichier « `.bib` ». Ici, l'outil {ctanpkg}`bibtool` permet de trier un fichier « `.bib` » suivant différents critères. Par exemple, pour lister le contenu d'une bibliographie, classé par année de parution, il faudrait exécuter une ligne de commande similaire à celle-ci :

```bash
bibtool -s -- 'sort.format={%d(year)}' biblio.bib -o bibliotriee.bib
```

Il suffit ensuite de compiler le fichier suivant :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
\nocite{*}
\bibliographystyle{unsrt}
\bibliography{bibliotriee}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```

